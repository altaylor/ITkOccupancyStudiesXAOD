#!/bin/bash
#grab job number from input
pdgCode=$1
pileup=$2

AltasSetup="/afs/cern.ch/atlas/software/dist/AtlasSetup"
asetup="source $AtlasSetup/scripts/asetup.sh"


# setup root, python, and InDetUpgradePerformanceAnalysis
echo $asetup dev,r5,64 --testarea=~/testarea/dev
$asetup dev,r5,64 --testarea=~/testarea/dev

echo $PATH

jobpath=pileup${pileup}
logfile=run${pdgCode}_${pileup}.log
mjobbasedir=/afs/cern.ch/user/a/aschaeli/scratch0/utopia

echo "Have " ${pdgCode} " and " ${pileup}" : " ${logfile}

mkdir -p  ${mjobbasedir}/${jobpath}
cd  ${mjobbasedir}/${jobpath}

cp /afs/cern.ch/user/a/aschaeli/testarea/dev/InnerDetector/InDetPerformance/InDetUpgradePerformanceAnalysis/share/samples.txt .

upg_perf_analyze.py ${pdgCode} ${pileup} > ${logfile} 2>&1 

