import ROOT as R
import os
import PyROOTUtils
import AtlasStyle

R.gROOT.SetBatch()

confs = [
    "slhc_ttbar_mu140_sigZ75",
    "slhc_piplus_mu130to150_sigZ75",
    "slhc_ttbar_mu80_sigZ50",
    "slhc_jetjet_JZ7W_mu0_sigZ75",
    "slhc_jetjet_JZ4W_mu0_sigZ75",
    "slhc_jetjet_JZ2W_mu0_sigZ75",  
    "slhc_piplus_mu0_sigZ50",
    #"slhc_piplus_mu0_sigZ75",
    #"slhc_minbias_sigZ50",
]

dets = ["sct", "pixel", "sct_reticule", "pixel_400um", "pixel_800um"]

tag = "all_27May"

names_confs = {
    #"slhc_piplus_mu0_sigZ50" : "#pi^{+} #mu=0 #sigmaz=50",
    #"slhc_piplus_mu0_sigZ75" : "#pi^{+} #mu=0 #sigmaz=75",
    #"slhc_jetjet_JZ2W_mu0_sigZ75" : "dijet p_{t} 80-200 GeV #mu=0 #sigmaz=75",  
    #"slhc_jetjet_JZ4W_mu0_sigZ75" : "dijet p_{t} 500-1000 GeV #mu=0 #sigmaz=75",
    #"slhc_jetjet_JZ7W_mu0_sigZ75" : "dijet p_{T}>2 TeV #mu=0 #sigmaz=75",
    #"slhc_piplus_mu130to150_sigZ75" : "#pi^{+} #mu=130-150 #sigmaz=75",
    #"slhc_ttbar_mu140_sigZ75" : "t#bar{t} #mu=140 #sigmaz=75",
    #"slhc_ttbar_mu80_sigZ50" : "t#bar{t} #mu=80 #sigmaz=50",
    #"slhc_minbias_sigZ50" : "minbias",
    "slhc_piplus_mu0_sigZ50" : "#pi^{+} #mu=0 ",
    "slhc_piplus_mu0_sigZ75" : "#pi^{+} #mu=0",
    "slhc_jetjet_JZ2W_mu0_sigZ75" : "dijet p_{t} 80-200 GeV #mu=0",  
    "slhc_jetjet_JZ4W_mu0_sigZ75" : "dijet p_{t} 500-1000 GeV #mu=0",
    "slhc_jetjet_JZ7W_mu0_sigZ75" : "dijet p_{T}>2 TeV #mu=0",
    "slhc_piplus_mu130to150_sigZ75" : "#pi^{+} #mu=130-150",
    "slhc_ttbar_mu140_sigZ75" : "t#bar{t} #mu=140",
    "slhc_ttbar_mu80_sigZ50" : "t#bar{t} #mu=80 ",
    "slhc_minbias_sigZ50" : "minbias",
}


lines_confs = {
    "slhc_piplus_mu0_sigZ50" : 1,
    "slhc_piplus_mu0_sigZ75" : 1,
    "slhc_jetjet_JZ2W_mu0_sigZ75" : 1,
    "slhc_jetjet_JZ4W_mu0_sigZ75" : 1,
    "slhc_jetjet_JZ7W_mu0_sigZ75" : 1,
    "slhc_piplus_mu130to150_sigZ75" :4,
    "slhc_ttbar_mu140_sigZ75" : 3,
    "slhc_ttbar_mu80_sigZ50" : 2,
    "slhc_minbias_sigZ50" : 1,
}

plot_dir= "stats_plots/"+tag

if not os.path.exists(plot_dir):
    os.mkdir(plot_dir)
   
for det in dets:
    if not os.path.exists(plot_dir+"/"+det):
        os.mkdir(plot_dir+"/"+det)


rfs = {}
for det in dets:
    rfs[det] = {}
    for conf in confs:
        rfname = "plots/"+tag+"/"+conf+"_"+det+"/stats.root"
        rfs[det][conf] = R.TFile(  rfname )
        if not rfs[det][conf]:
            print rf,"not found"

def plot_stats( detector, statname, label):
    graphs = {}
    leg = PyROOTUtils.Legend(0.3,0.9, textSize=0.035)
    leg2 = PyROOTUtils.Legend(0.6,0.9, textSize=0.035)
    ymax=0; ymin=99999

    for (conf, iConf) in zip(confs, range(len(confs))):

        graph_raw  = rfs[detector][conf].Get( statname ) 
        if not graph_raw:
            raise RuntimeError("Graph not found " + statname + " in " + rfs[detector][conf].GetName() ) 
        graph = PyROOTUtils.Graph( graph_raw )
        if iConf>3:
            leg2.AddEntry(graph, names_confs[conf], "lp")
        else:
            leg.AddEntry(graph, names_confs[conf], "lp")
        if graph.argminY() < ymin: ymin = graph.argminY()
        if graph.argmaxY() > ymax: ymax = graph.argmaxY()
        print ymin, ymax
        graphs[conf] = graph
    c = R.TCanvas()
    for conf, graph, iGraph in zip(graphs.keys(), graphs.values(), range(len(graphs))):
        PyROOTUtils.format_hist( graph, ymin=ymin-(ymax-ymin)*0.1, ymax=ymax+(ymax-ymin)* 0.4, 
                                ytitle = label, xtitle = "Layer", 
                                marker_style=PyROOTUtils.pretty_marker(iGraph), 
                                line_colour=PyROOTUtils.pretty_colour(iGraph), 
                                marker_colour=PyROOTUtils.pretty_colour(iGraph), 
                                line_style=lines_confs[conf],
                                title="",
                                xmin=-0.5, xmax=5.5 )
        if iGraph==0: 
            graph.Draw("ALPE")
        else:
            graph.Draw("LPE")
    leg.Draw()
    leg2.Draw()
    c.SaveAs( plot_dir + "/" + detector + "/" + statname + ".eps")
    c.SaveAs( plot_dir + "/" + detector + "/" + statname + ".pdf")

for det in ["sct", "pixel"]:
    plot_stats( det, "sensor_mean_clus_mean", "<Mean clusters on sensor>")
    plot_stats( det, "sensor_max_clus_mean", "<Max clusters on sensor>")
    plot_stats( det, "sensor_mean_hits_mean", "<Mean hits on sensor>")
    plot_stats( det, "sensor_max_hits_mean", "<Max hits on sensor>")
    plot_stats( det, "sensor_mean_flu_mean", "<Mean fluency on sensor>")
    plot_stats( det, "sensor_max_flu_mean", "<Max fluency on sensor>")
    plot_stats( det, "sensor_mean_flu_clus_mean", "<Mean clusters/area on sensor>")
    plot_stats( det, "sensor_max_flu_clus_mean", "<Max clusters/area on sensor>")
    plot_stats( det, "sensor_mean_occ_mean", "<Mean occupancy on sensor> (%)")
    plot_stats( det, "sensor_max_occ_mean", "<Max occupancy on sensor> (%)")

    plot_stats( det, "chip_mean_clus_mean", "<Mean clusters on chip>")
    plot_stats( det, "chip_max_clus_mean", "<Max clusters on chip>")
    plot_stats( det, "chip_mean_hits_mean", "<Mean hits on chip>")
    plot_stats( det, "chip_max_hits_mean", "<Max hits on chip>")
    plot_stats( det, "chip_mean_flu_mean", "<Mean fluency on chip>")
    plot_stats( det, "chip_max_flu_mean", "<Max fluency on chip>")
    plot_stats( det, "chip_mean_flu_clus_mean", "<Mean clusters/area on chip>")
    plot_stats( det, "chip_max_flu_clus_mean", "<Max clusters/area on chip>")
    plot_stats( det, "chip_mean_occ_mean", "<Mean occupancy on chip> (%)")
    plot_stats( det, "chip_max_occ_mean", "<Max occupancy on chip> (%)")

plot_stats( "sct_reticule", "chip_mean_clus_mean", "<Mean clusters on reticule>",)
plot_stats( "sct_reticule", "chip_max_clus_mean", "<Max clusters on reticule>")
plot_stats( "sct_reticule", "chip_mean_hits_mean", "<Mean hits on reticule>")
plot_stats( "sct_reticule", "chip_max_hits_mean", "<Max hits on reticule>")
plot_stats( "sct_reticule", "chip_mean_flu_mean", "<Mean fluency on reticule>")
plot_stats( "sct_reticule", "chip_max_flu_mean", "<Max fluency on reticule>")
plot_stats( "sct_reticule", "chip_mean_flu_clus_mean", "<Mean clusters/area on reticle>")
plot_stats( "sct_reticule", "chip_max_flu_clus_mean", "<Max clusters/area on reticle>")
plot_stats( "sct_reticule", "chip_mean_occ_mean", "<Mean occupancy on reticle> (%)")
plot_stats( "sct_reticule", "chip_max_occ_mean", "<Max occupancy on reticle> (%)")


plot_stats( "pixel_400um", "chip_mean_clus_mean", "<Mean clusters in 400\\mum\\times400\\mum area>",)
plot_stats( "pixel_400um", "chip_max_clus_mean", "<Max clusters in 400\\mum\\times400\\mum area>")
plot_stats( "pixel_400um", "chip_mean_hits_mean", "<Mean hits in 400\\mum\\times400\\mum area>")
plot_stats( "pixel_400um", "chip_max_hits_mean", "<Max hits in 400\\mum\\times400\\mum area>")
plot_stats( "pixel_400um", "chip_mean_flu_mean", "<Mean fluency in 400\\mum\\times400\\mum area>")
plot_stats( "pixel_400um", "chip_max_flu_mean", "<Max fluency in 400\\mum\\times400\\mum area>")
plot_stats( "pixel_400um", "chip_mean_flu_clus_mean", "<Mean clusters/area in 400\\mum\\times400\\mum>")
plot_stats( "pixel_400um", "chip_max_flu_clus_mean", "<Max clusters/area in 400\\mum\\times400\\mum>")
plot_stats( "pixel_400um", "chip_mean_occ_mean", "<Mean occupancy in 400\\mum\\times400\\mum> (%)")
plot_stats( "pixel_400um", "chip_max_occ_mean", "<Max occupancy in 400\\mum\\times400\\mum> (%)")

plot_stats( "pixel_800um", "chip_mean_clus_mean", "<Mean clusters in 800\\mum\\times800\\mum area>",)
plot_stats( "pixel_800um", "chip_max_clus_mean", "<Max clusters in 800\\mum\\times800\\mum area>")
plot_stats( "pixel_800um", "chip_mean_hits_mean", "<Mean hits in 800\\mum\\times800\\mum area>")
plot_stats( "pixel_800um", "chip_max_hits_mean", "<Max hits in 800\\mum\\times800\\mum area>")
plot_stats( "pixel_800um", "chip_mean_flu_mean", "<Mean fluency in 800\\mum\\times800\\mum area>")
plot_stats( "pixel_800um", "chip_max_flu_mean", "<Max fluency in 800\\mum\\times800\\mum area>")
plot_stats( "pixel_800um", "chip_mean_flu_clus_mean", "<Mean clusters/area in 800\\mum\\times800\\mum>")
plot_stats( "pixel_800um", "chip_max_flu_clus_mean", "<Max clusters/area in 800\\mum\\times800\\mum>")
plot_stats( "pixel_800um", "chip_mean_occ_mean", "<Mean occupancy in 800\\mum\\times800\\mum> (%)")
plot_stats( "pixel_800um", "chip_max_occ_mean", "<Max occupancy in 800\\mum\\times800\\mum> (%)")
