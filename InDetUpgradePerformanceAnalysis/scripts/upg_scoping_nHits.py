#!/usr/bin/env python

# Import the analysis package!
from InDetUpgradePerformanceAnalysis import SingleAnalysis, InDetUpgradeAnalysis, TreeHouse, Addition, DerivedBranch

# Import cut definitions
# see python/StandardCuts.py
from InDetUpgradePerformanceAnalysis.StandardCuts import *

# Import custom binning definitions
# see python/CustomBinning.py
# By default, all resoltuion/efficiency plots are binned as a function
# of |eta| with 10 bins from 0->2.5
# Alternative options are defined in python/CustomBinning.py
import InDetUpgradePerformanceAnalysis.CustomBinning

# Import observable definitions - define the things we want to plot
# (reolsutions, efficiencies etc)
# see python/StandardObservableDefinitions.py
from InDetUpgradePerformanceAnalysis.ScopingDefinitions import HitsResolutionObservables

# Add derived branches definitions to calculate e.g. the nAllHoles / nAllHits variables
# see python/DerviedBranches.py
from InDetUpgradePerformanceAnalysis.DerivedBranches import DerivedBranches

# Get the configuration: input file(s), output file, number of events to process, pdgId etc
import InDetUpgradePerformanceAnalysis.Configuration as Configuration
options=Configuration.GetOptions()

pdgId=13
folder = "Zmumu"
label = "Zmumu; prompt muons only"
outfile=options.outfile
infiles=options.inputfiles
minProb=options.minProb
maxEvents = options.maxEvents

# Now set up the analysis....
ana=InDetUpgradeAnalysis()
ana.SetOutputFileName(outfile)
ana.SetChain(options.treename)
ana.SetMaxEvents(maxEvents)

# Add input files
for f in infiles:
    print 'adding',f
    ana.AddFile(f)

# Set up a "SingleAnalysis" job. Each SingleAnalysis job corresponds
# to one run of the event loop. We add spepcific instances of the observable classes
# defined in python/StandardObservableDefinitions.py e..g with different pT cuts
sa=SingleAnalysis("Hits",TreeHouse.Current())
sa.AddStep(InDetUpgradePerformanceAnalysis.CONTROL)

# Add derived branches to calculate e.g. the nAllHoles / nAllHits variables
# These are used in cuts so have to add them before anything else
sa.Add(DerivedBranches(add_dead_holes=True))


# Resolution observables using RMS to get resolution 
sa.Add(HitsResolutionObservables(pdgId,None, minProb, folder, label, minPt=4e3))
#sa.Add(HitsResolutionObservables(pdgId,None, minProb, folder+"_extendedEta", label, extended_eta=True, minPt=4e3))

# Add to the main analysis
ana.Add(sa)

# Processing is now handed over to C++ to do all the heavy lifting
try:
    ana.Initialize()
    ana.Execute()
    ana.Finalize()
except KeyboardInterrupt:
    print('%s got KeyboardInterrupt' % name)
