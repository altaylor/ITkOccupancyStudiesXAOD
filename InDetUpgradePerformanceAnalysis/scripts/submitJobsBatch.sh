#!/bin/bash
# based on script by Andreas Korn

myvar=1

ninputfiles=`ls -1 /afs/cern.ch/user/g/gencomm/w0/korn/user09.AndreasKorn.CosmicDigi.test3/*RDO|wc -l`
while [ $myvar -le ${ninputfiles} ]
do
    echo "submitting " ${myvar}
    /usr/bin/bsub -q 8nh  runJob.sh $myvar 
    myvar=$(( $myvar + 1 ))
done
