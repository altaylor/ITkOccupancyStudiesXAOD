#!/usr/bin/env python

import sys, glob, os

usage = """Script for making nHits plots for the scoping document\n
    upg_scoping_plot_nHits.py <tag> [<preliminary=True>]
where <tag> should match the outfilelabel argument passed to upg_scoping_nHits.py"""

# Option parsing
if len(sys.argv)<2:
    print usage
    sys.exit()
tag = sys.argv[1]
if len(sys.argv)>2: public=bool(int(sys.argv[2]))
else: public=False

print "public=",public

from InDetUpgradePerformanceAnalysis.PlotMaker import *
from InDetUpgradePerformanceAnalysis.ScopingPlotting import *
import ROOT as R
R.gROOT.SetBatch()


def make_hits_plot(layout, path, extended_eta=False):

    def get_plots(rf, path):
        gr_all = rf.Get(path+"/trk_nAllHits_bias_etaSingleSided")
        gr_sct = rf.Get(path+"/trk_nSCTHits_bias_etaSingleSided")
        gr_pix = rf.Get(path+"/trk_nPixHits_bias_etaSingleSided")
        gr_all_res = rf.Get(path+"/trk_nAllHits_res_etaSingleSided")
        gr_sct_res = rf.Get(path+"/trk_nSCTHits_res_etaSingleSided")
        gr_pix_res = rf.Get(path+"/trk_nPixHits_res_etaSingleSided")
        return (gr_all, gr_sct, gr_pix, gr_all_res, gr_sct_res, gr_pix_res)

    # Set errors as resolutions, truncate VF_Silver and
    # fudge VF_Silver_m10
    def modify_gr(gr, gr_res, gr_g, gr_res_g, layout):
        eta, hits, res = R.Double(), R.Double(), R.Double()
        eta_g, hits_g, res_g = R.Double(), R.Double(), R.Double()
        for iP in range(gr.GetN()):
            gr_res.GetPoint(iP, eta, res)
            if (layout=="VF_Silver" or layout=="VF_Silver_m10") and eta>3.2:
                gr.SetPointError(iP, gr.GetErrorX(iP), 0)
                gr.SetPoint(iP, eta, 0)
            elif layout=="VF_Silver_m10" and eta>2.7:
                gr_res_g.GetPoint(iP, eta_g, res_g)
                gr_g.GetPoint(iP, eta_g, hits_g)
                gr.SetPointError(iP, gr.GetErrorX(iP), res_g)
                gr.SetPoint(iP, eta, hits_g)
            else:
                gr.SetPointError(iP, gr.GetErrorX(iP), res)

    file = get_filename(layout, tag)
    if not os.path.exists(file):
        print "file not found", file
        return
    c = R.TCanvas()
    rf = R.TFile(file)

    gr_all, gr_sct, gr_pix, gr_all_res, gr_sct_res, gr_pix_res = get_plots(rf, path)

    if "m10" in layout: gold_name = "VF_Gold_m10"
    else: gold_name = "VF_Gold"
    rf_gold = R.TFile(get_filename(gold_name, tag))
    gr_all_g, gr_sct_g, gr_pix_g, gr_all_res_g, gr_sct_res_g, gr_pix_res_g = get_plots(rf_gold, path)

    modify_gr(gr_all, gr_all_res, gr_all_g, gr_all_res_g, layout)
    modify_gr(gr_pix, gr_pix_res, gr_pix_g, gr_pix_res_g, layout)
    modify_gr(gr_sct, gr_sct_res, gr_sct_g, gr_sct_res_g, layout)
    modify_gr(gr_all_g, gr_all_res_g, None, None, "VF_Gold")

    h = R.TH1D("hframe", "hframe", 1, 0, 4.0)
    
    h.SetMaximum(26)
    h.GetXaxis().SetTitle("|#eta|")
    h.GetYaxis().SetTitle("<Hits on track>")
    h.Draw()

    gr_sct.SetLineColor(R.kGreen+2)
    gr_sct.SetMarkerStyle(22)
    gr_sct.SetMarkerColor(R.kGreen+2)

    gr_pix.SetLineColor(R.kMagenta+2)
    gr_pix.SetMarkerColor(R.kMagenta+2)
    gr_pix.SetMarkerStyle(23)

    gr_all.SetLineColor(R.kBlue+2)
    gr_all.SetMarkerColor(R.kBlue+2)

    if not "Gold" in layout:
        gr_all_g.SetLineColor(R.kGray)
        gr_all_g.Draw("L")

    gr_all.Draw("LP")
    gr_sct.Draw("LP")
    gr_pix.Draw("LP")

    AtlasUtil.AtlasLabel(0.2,0.86,1,text="Atlas", preliminary=False, public=public, simulation=True)
    AtlasUtil.DrawText(0.2,0.8,text=get_legend(layout)+" Layout", size=0.045)
    if "Gold" in layout: leg = R.TLegend(0.62, 0.70, 0.92, 0.88)
    else:leg = R.TLegend(0.62, 0.65, 0.92, 0.9)
    leg.SetBorderSize(0)
    leg.SetFillStyle(0)
    if not "Gold" in layout: leg.AddEntry(gr_all_g, "Total Hits (Reference)", "l")
    leg.AddEntry(gr_all, "Total Hits", "lp")
    leg.AddEntry(gr_pix, "Pixel Hits", "lp")
    leg.AddEntry(gr_sct, "Strip Hits", "lp")
    leg.Draw()

    #tag = layout
    #if outlabel: tag += "_" + outlabel
    path = "plots"
    if public: path+="_public"
    if not os.path.exists(path): os.system("mkdir -p "+path)
    c.SaveAs(path+"/"+layout+"_hits.eps")
    c.SaveAs(path+"/"+layout+"_hits.C")

def make_holes_plot(layout, path, extended_eta=False):
    file = get_filename(layout, tag)
    if not os.path.exists(file):
        print "file not found", file
        return
    c = R.TCanvas()
    rf = R.TFile(file)
    gr_all = rf.Get(path+"/trk_nAllHoles_bias_etaSingleSided")
    gr_sct = rf.Get(path+"/trk_nSCTHoles_bias_etaSingleSided")
    gr_pix = rf.Get(path+"/trk_nPixHoles_bias_etaSingleSided")
    if extended_eta:
        h = R.TH1D("hframe", "hframe", 1, 0, 4.0)
    else:
        h = R.TH1D("hframe", "hframe", 1, 0, 2.7)
    h.SetMaximum(0.2)
    h.GetXaxis().SetTitle("|#eta|")
    h.GetYaxis().SetTitle("<Holes on track>")
    h.Draw()
    gr_sct.SetLineColor(R.kBlue)
    gr_pix.SetLineColor(R.kRed)
    gr_sct.SetMarkerColor(R.kBlue)
    gr_pix.SetMarkerColor(R.kRed)
    gr_sct.SetMarkerStyle(22)
    gr_pix.SetMarkerStyle(23)
    gr_all.Draw("LP")
    gr_sct.Draw("LP")
    gr_pix.Draw("LP")

    AtlasUtil.AtlasLabel(0.2,0.86,1,text="Atlas", preliminary=preliminary, public=public, simulation=True)
    AtlasUtil.DrawText(0.2,0.8,text=get_legend(layout)+" Layout", size=0.045)
    leg = R.TLegend(0.2, 0.62, 0.4, 0.78)
    leg.SetBorderSize(0)
    leg.SetFillStyle(0)
    leg.AddEntry(gr_all, "Total Holes", "lp")
    leg.AddEntry(gr_pix, "Pixel Holes", "lp")
    leg.AddEntry(gr_sct, "Strip Holes", "lp")
    leg.Draw()

    tag = layout
    #if options.outlabel: tag += "_" + options.outlabel
    path = "plots/"+tag
    if public: path+="_public"
    if not os.path.exists(path): os.system("mkdir -p "+path)
    c.SaveAs(path+"/holes.eps")
    c.SaveAs(path+"/holes.C")

def make_hits_holes_dead_plot(layout, path, extended_eta=False):
    file = get_filename(layout, tag)
    if not os.path.exists(file):
        print "file not found", file
        return
    c = R.TCanvas()
    rf = R.TFile(file)
    gr_all = rf.Get(path+"/trk_nAllHits_bias_etaSingleSided")
    gr_sct = rf.Get(path+"/trk_nSCTHits_bias_etaSingleSided")
    gr_pix = rf.Get(path+"/trk_nPixHits_bias_etaSingleSided")
    gr_hitsholes_all = rf.Get(path+"/trk_nAllHitsHoles_bias_etaSingleSided")
    gr_hitsholes_sct = rf.Get(path+"/trk_nSCTHitsHoles_bias_etaSingleSided")
    gr_hitsholes_pix = rf.Get(path+"/trk_nPixHitsHoles_bias_etaSingleSided")
    gr_hitsholesdead_all = rf.Get(path+"/trk_nAllHitsHolesDead_bias_etaSingleSided")
    gr_hitsholesdead_sct = rf.Get(path+"/trk_nSCTHitsHolesDead_bias_etaSingleSided")
    gr_hitsholesdead_pix = rf.Get(path+"/trk_nPixHitsHolesDead_bias_etaSingleSided")
    if extended_eta:
        h = R.TH1D("hframe", "hframe", 1, 0, 4.0)
    else:
        h = R.TH1D("hframe", "hframe", 1, 0, 2.7)
    h.SetMaximum(26)
    h.GetXaxis().SetTitle("|#eta|")
    h.GetYaxis().SetTitle("<Hits on track>")
    h.Draw()
    gr_sct.SetLineColor(R.kBlue)
    gr_pix.SetLineColor(R.kRed)
    gr_sct.SetMarkerColor(R.kBlue)
    gr_pix.SetMarkerColor(R.kRed)
    gr_hitsholes_sct.SetLineColor(R.kBlue)
    gr_hitsholes_pix.SetLineColor(R.kRed)
    gr_hitsholes_sct.SetMarkerColor(R.kBlue)
    gr_hitsholes_pix.SetMarkerColor(R.kRed)
    gr_hitsholes_all.SetMarkerColor(R.kBlack)
    gr_hitsholesdead_sct.SetLineColor(R.kBlue)
    gr_hitsholesdead_pix.SetLineColor(R.kRed)
    gr_hitsholesdead_sct.SetMarkerColor(R.kBlue)
    gr_hitsholesdead_pix.SetMarkerColor(R.kRed)
    gr_hitsholesdead_all.SetMarkerColor(R.kBlack)
    gr_sct.SetMarkerStyle(22)
    gr_pix.SetMarkerStyle(23)
    gr_hitsholes_sct.SetMarkerStyle(22)
    gr_hitsholes_pix.SetMarkerStyle(23)
    gr_hitsholesdead_sct.SetMarkerStyle(22)
    gr_hitsholesdead_pix.SetMarkerStyle(23)

    gr_hitsholes_sct.SetLineStyle(2)
    gr_hitsholes_pix.SetLineStyle(2)
    gr_hitsholes_all.SetLineStyle(2)
    gr_hitsholesdead_sct.SetLineStyle(7)
    gr_hitsholesdead_pix.SetLineStyle(7)
    gr_hitsholesdead_all.SetLineStyle(7)
    
    gr_all.Draw("LP")
    gr_sct.Draw("LP")
    gr_pix.Draw("LP")

    gr_hitsholes_all.Draw("Le0")
    gr_hitsholes_sct.Draw("Le0")
    gr_hitsholes_pix.Draw("Le0")

    gr_hitsholesdead_all.Draw("LX")
    gr_hitsholesdead_sct.Draw("LX")
    gr_hitsholesdead_pix.Draw("LX")

    AtlasUtil.AtlasLabel(0.2,0.86,1,text="Atlas", preliminary=False, public=public, simulation=True)
    AtlasUtil.DrawText(0.2,0.8,text=get_legend(layout)+" Layout", size=0.045)
    leg = R.TLegend(0.2, 0.62, 0.39, 0.78)
    leg.SetBorderSize(0)
    leg.SetFillStyle(0)
    leg.AddEntry(gr_all, "Total Hits", "lp")
    leg.AddEntry(gr_pix, "Pixel Hits", "lp")
    leg.AddEntry(gr_sct, "Strip Hits", "lp")
    leg.Draw()

    leg2 = R.TLegend(0.59, 0.65, 0.92, 0.93)
    leg2.SetBorderSize(0)
    leg2.SetFillStyle(0)
    leg2.AddEntry(gr_hitsholesdead_all, "Hits+Holes+Dead", "l")
    leg2.AddEntry(gr_hitsholes_all, "Hits+Holes", "l")
    leg2.AddEntry(gr_all, "Hits", "l")
    leg2.Draw()

    tag = layout
    #if options.outlabel: tag += "_" + options.outlabel
    path = "plots/"+tag
    if public: path += "_public"
    if not os.path.exists(path): os.system("mkdir -p "+path)
    c.SaveAs(path+"/hitsholesdead.eps")


def main():
    make_hits_plot( "VF_Gold_m10", "Zmumu", True)
    make_hits_plot( "VF_Silver_m10", "Zmumu", True)
    make_hits_plot( "VF_Gold", "Zmumu", True)
    make_hits_plot( "VF_Silver", "Zmumu", True)
    make_hits_plot( "Bronze_m10", "Zmumu", True)
    make_hits_plot( "Bronze", "Zmumu", True)


if __name__ == "__main__":
    main()
