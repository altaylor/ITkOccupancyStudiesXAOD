#!/bin/bash
#grab job number from input
jobnumber=$1
shift

#source ~akorn/COSMIC_RECOTEST/cmthome/setup.sh
asetup dev,r5,64 -testarea=~/testarea/dev


ninputfiles=`ls -1 /afs/cern.ch/user/g/gencomm/w0/korn/user09.AndreasKorn.CosmicDigi.test3/*RDO|wc -l`
jobinputfile=`ls -1 /afs/cern.ch/user/g/gencomm/w0/korn/user09.AndreasKorn.CosmicDigi.test3/*RDO|head -${jobnumber}|tail -1`

echo "Have " ${ninputfiles} " files, use file " ${jobnumber}" : " ${jobinputfile}

mjobbasedir=/afs/cern.ch/user/g/gencomm/w0/korn/ESD2
mkdir -p  ${mjobbasedir}/${jobnumber}
cp /afs/cern.ch/user/g/gencomm/w0/korn/ESD2/CosmicSimulationRecoSetup_jamie.py /afs/cern.ch/user/g/gencomm/w0/korn/ESD2/${jobnumber}/
cd /afs/cern.ch/user/g/gencomm/w0/korn/ESD2/${jobnumber}

Reco_trf.py inputRDOFile=${jobinputfile} maxEvents=5000 preInclude=RecExCommon/RecoUsefulFlags.py,CosmicSimulationRecoSetup_jamie.py,RecExCommission/RecExCommissionRepro.py,RecJobTransforms/UseOracle.py beamType=cosmics outputESDFile=myESD_${jobnumber}.pool.root HIST=myMergedMonitoring_${jobnumber}.root outputTAGComm=myTAGCOMM_${jobnumber}.root outputCBNT=myCommission_${jobnumber}.ntuple.root preExec="DQMonFlags.monType.set_Value_and_Lock('BSall')" geometryVersion=ATLAS-GEO-07-00-00 --ignoreunknown

