#!/usr/bin/env python

# Import the analysis package!
from InDetUpgradePerformanceAnalysis import SingleAnalysis, InDetUpgradeAnalysis, TreeHouse, AllCuts, ObservableGroup, AbsSelector, RangeCut, MinCut, MaxCut, CONTROL

from InDetUpgradePerformanceAnalysis.ScopingDefinitions import *

# Import custom binning definitions
# see python/CustomBinning.py
# By default, all resoltuion/efficiency plots are binned as a function
# of |eta| with 10 bins from 0->2.5
# Alternative options are defined in python/CustomBinning.py
from InDetUpgradePerformanceAnalysis import CustomBinning

# Add derived branches definitions to calculate e.g. the nAllHoles / nAllHits variables
# see python/DerviedBranches.py
from InDetUpgradePerformanceAnalysis.DerivedBranches import DerivedBranches

# Get the configuration: input file(s), output file, number of events to process, pdgId etc
import InDetUpgradePerformanceAnalysis.Configuration as Configuration
options=Configuration.GetOptions()

# Define the basic track cuts. These are extended by TrackResolutionCuts
class BasicTrackCuts(AllCuts):
    def __init__(self, nHits, nHoles, no_cuts=False, noIP=False):
        AllCuts.__init__(self)
        if "VF_Silver" in options.layout:
            self.Add(RangeCut("trk_eta",-3.2,3.2)) # mm
        if no_cuts: return
        self.Add(MinCut("trk_pt",1000))  # MeV
        if not noIP: self.Add(RangeCut("trk_d0_wrtPV",-1.0,1.0)) # mm
        if not noIP: self.Add(RangeCut("trk_z0_wrtPV",-150.0,150.0)) # mm
        if nHits>0:
            self.Add(MinCut("trk_nAllHits",nHits))
        if nHoles>=0:
            self.Add(MaxCut("trk_nPixHoles",nHoles+1))


pdgId=13
folder = "Zmumu"
label = "Zmumu; prompt muons only"
outfile=options.outfile
infiles=options.inputfiles
minProb=options.minProb
maxEvents = options.maxEvents

# Now set up the analysis....
ana=InDetUpgradeAnalysis()
ana.SetOutputFileName(outfile)
ana.SetChain(options.treename)
ana.SetMaxEvents(maxEvents)

# Add input files
for f in infiles:
    print 'adding',f
    ana.AddFile(f)

# Set up a "SingleAnalysis" job. Each SingleAnalysis job corresponds
# to one run of the event loop. We add spepcific instances of the observable classes
# defined in python/StandardObservableDefinitions.py e..g with different pT cuts
sa_EfficiencyFakesHits=SingleAnalysis("EfficienciesFakesHits",TreeHouse.Current())
sa_EfficiencyFakesHits.Add(DerivedBranches())
sa_EfficiencyFakesHits.AddStep(CONTROL)
nHoles=1
for nHits in [7, 9, 11]:
    folder = "effFakeMatchProb_"+str(nHits)+"hits_1holes"
    sa_EfficiencyFakesHits.Add(EfficiencyObservables(pdgId, None, folder, "Efficciency "+folder, BasicTrackCuts(nHits, nHoles), minPt=4e3))
    sa_EfficiencyFakesHits.Add(FakeRateObservables(folder,  "Fakes "+folder, BasicTrackCuts(nHits, nHoles), add_prob=True))
    folder = "matchProb_"+str(nHits)+"hits_1holes_mu"
    sa_EfficiencyFakesHits.Add(MatchProbObservables(folder, folder, TrackResolutionCuts(0, BasicTrackCuts(nHits, nHoles), pdgId=pdgId, truthMinPt=4e3)))
# Add to the main analysis
ana.Add(sa_EfficiencyFakesHits)

# And finally.... run the job!
# Processing is now handed over to C++ to do all the heavy lifting
try:
    ana.Initialize()
    ana.Execute()
    ana.Finalize()
except KeyboardInterrupt:
    print('%s got KeyboardInterrupt' % name)
