#!/usr/bin/env python

import InDetUpgradePerformanceAnalysis as UpgPerfAna
from InDetUpgradePerformanceAnalysis import *

from InDetUpgradePerformanceAnalysis.geometry import AtlasDetector
from InDetUpgradePerformanceAnalysis.sensorinfo import getSensorInfo

from InDetUpgradePerformanceAnalysis.occupancy import SCTDisks, SCTBarrel

# Get the configuration: input file(s), output file, number of events to process, pdgId etc
import InDetUpgradePerformanceAnalysis.Configuration as Configuration
options=Configuration.GetOptions(True)

class PixelOccupancyObservables(ObservableGroup):
    def __init__(self,path,title):
        ObservableGroup.__init__(self,title)
        self.SetPath(path)
        self.Add(PixelBarrelOccupancy())
        self.Add(PixelDiskOccupancy())
        
class SCTOccupancyObservables(ObservableGroup):
    def __init__(self,path,title):
        ObservableGroup.__init__(self,title)
        self.SetPath(path)
        self.Add(SCTDiskOccupancy())
        self.Add(SCTBarrelOccupancy())


#utopia=AtlasDetector('ATLAS-SLHC-01-00-00')
#sensor=getSensorInfo(utopia)

#cartigny=AtlasDetector('ATLAS-SLHC-01-02-01')
#sensor=getSensorInfo(cartigny)

itkLoi=AtlasDetector('ATLAS-SLHC-01-03-00')
sensor=getSensorInfo(itkLoi)

ana=InDetUpgradeAnalysis()
ana.SetOutputFileName(options.outfile)
ana.SetChain(options.treename)
ana.SetMaxEvents(options.maxEvents)
for f in options.inputfiles:
    ana.AddFile(f)
ana.AddSensor(sensor)

print "Chain entries", ana.GetChain().GetEntries()
print "maxEvents", options.maxEvents



#c=ana.GetChain()
#c.Print()
sa=SingleAnalysis("OccupancyAnalysis",TreeHouse(ana.GetChain()))
sa.SetMaxEvents(options.maxEvents)
#sa.Add(DerivedBranch("pixClus_r","sqrt(pixClus_x*pixClus_x+pixClus_y*pixClus_y)"))
#sa.Add(PixelOccupancyObservables('pixel_occupancy','PixelOccupancyObservables'))
sa.Add(SCTOccupancyObservables('sct_occupancy','SCTOccupancyObservables'))
sa.Add(DumpSensorInfo())
sa.Add(Distribution("mcVx_z", 500, -250, 250))
sa.Add(Distribution("pixClus_z", 500, -250, 250))

# Define cuts for plotting truth particle pT
truth_cuts = AllCuts()
truth_cuts.Add(MaxCut("mc_gen_barcode", int(200e3)))
truth_cuts.Add(AbsSelector("mc_charge", 1))

og = ObservableGroup("gen_particles")
og.SetCuts(truth_cuts)
og.Add(Distribution("mc_gen_pt", 200, 0, 100e3))
og.Add(Distribution("mc_gen_eta", 100, -10, 10))
og.Add(Distribution("mc_gen_type", 1000, -500, 500))
og.SetPath("gen_particles")
sa.Add(og)

sa.AddStep(UpgPerfAna.EVENT)
ana.Add(sa)

ana.Initialize()
ana.Execute()
ana.Finalize()
