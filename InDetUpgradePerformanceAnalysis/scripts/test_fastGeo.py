#!/usr/bin/env python

import InDetUpgradePerformanceAnalysis as UpgPerfAna
from InDetUpgradePerformanceAnalysis import *
from InDetUpgradePerformanceAnalysis.geometry_fastGeo import AtlasDetector
from InDetUpgradePerformanceAnalysis.sensorinfo_fastGeo import getSensorInfo

from ROOT import SensorInfoBase

itkLoi=AtlasDetector('ATLAS-P2-ITK-05') #ExtBrl32 -- ITK extended barrel @ 3.2                                                            
#itkLoi=AtlasDetector('ATLAS-P2-ITK-06') #ExtBrl4 -- ITK extended barrel @ 4.0                                                            
#itkLoi=AtlasDetector('ATLAS-P2-ITK-07') #IExtBrl4 -- ITK extended barrel, inclined layout @ 4.0                                          
#itkLoi=AtlasDetector('ATLAS-P2-ITK-08') #InclBrl4 -- ITK fully inclined layout

sensor=getSensorInfo(itkLoi)

# this is for the SCT Barrel

nLayer = len(sensor.sctlayer)
doubleStripLength = False 

testCounter = 0 
for i in range(nLayer):

    print 'layer i is...', i
    
    nModule = sensor.sctlayer[i].nmodule*sensor.sctlayer[i].nsegment
    area = sensor.sctlayer[i].area

    if doubleStripLength and i < 3:
        nModule /= 2
        area *= 2

    nMax = nModule/2
    nMin = -nMax
    hasZero = (nModule%2!=0)
    d = { }

    #Loop over number of sensors along Z (=nnodule*nsegment), phi symmetry?
    for m in range(nMin,nMax+1):
        if m!=0 or hasZero:
            # set the number of strips per sensor.
            # perModuleEta gives the number of strips per sensor summed over phi and accounts for both sides..
            # per Module gives the number of strips per sensor..
            print ' total number of readout over all phi ', sensor.sctlayer[i].perModuleEta
            print ' total number of pixels in a single sensor ', sensor.sctlayer[i].perModule
            
            testCounter += 1
            #d[m]=Counters(tot_pix=sensor.sctlayer[i].perModuleEta)

            #layerInfo->getValue("perModule")

# this is for the SCT DISKS

#number of read out channels for each ring is either 40960 or 81920

nDisks = len(sensor.sctdisk)
print 'number of disks ' , nDisks

for i,pd in enumerate(sensor.sctdisk):
        for j,ring in enumerate(pd.rings):
                print 'indices ', i , j 
                print 'testing ', ring.total





#nlayer = sensor->getValue("nsctlayer");



#from InDetUpgradePerformanceAnalysis.occupancy import SCTDisks, SCTBarrel

# Get the configuration: input file(s), output file, number of events to process, pdgId etc
#import InDetUpgradePerformanceAnalysis.Configuration as Configuration
#options=Configuration.GetOptions(True)



class PixelOccupancyObservables(ObservableGroup):
    def __init__(self,path,title):
        ObservableGroup.__init__(self,title)
        self.SetPath(path)
        #self.Add(PixelBarrelOccupancy(True))
        self.Add(PixelBarrelOccupancy())
        
        self.Add(PixelDiskOccupancy())
        
class SCTOccupancyObservables(ObservableGroup):
    def __init__(self,path,title):
        ObservableGroup.__init__(self,title)
        self.SetPath(path)
        self.Add(SCTDiskOccupancy())
        self.Add(SCTBarrelOccupancy())


#itkLoi=AtlasDetector('ATLAS-P2-ITK-05') #ExtBrl32 -- ITK extended barrel @ 3.2
#itkLoi=AtlasDetector('ATLAS-P2-ITK-06') #ExtBrl4 -- ITK extended barrel @ 4.0
#itkLoi=AtlasDetector('ATLAS-P2-ITK-07') #IExtBrl4 -- ITK extended barrel, inclined layout @ 4.0
itkLoi=AtlasDetector('ATLAS-P2-ITK-08') #InclBrl4 -- ITK fully inclined layout

sensor=getSensorInfo(itkLoi)

ana=InDetUpgradeAnalysis()
ana.SetOutputFileName(options.outfile)
ana.SetChain(options.treename)
ana.SetMaxEvents(options.maxEvents)
for f in options.inputfiles:
    ana.AddFile(f)
ana.AddSensor(sensor)

print "Chain entries", ana.GetChain().GetEntries()
print "maxEvents", options.maxEvents



#c=ana.GetChain()
#c.Print()
sa=SingleAnalysis("OccupancyAnalysis",TreeHouse(ana.GetChain()))
sa.SetMaxEvents(options.maxEvents)

sa=SingleAnalysis("OccupancyAnalysis",TreeHouse(ana.GetChain()))
sa.SetMaxEvents(options.maxEvents)
sa.Add(DerivedBranch("pixClus_r","sqrt(pixClus_x*pixClus_x+pixClus_y*pixClus_y)"))
sa.Add(PixelOccupancyObservables('pixel_occupancy','PixelOccupancyObservables'))
sa.Add(SCTOccupancyObservables('sct_occupancy','SCTOccupancyObservables'))
sa.Add(DumpSensorInfo())
sa.Add(Distribution("mcVx_z", 500, -250, 250))
sa.Add(Distribution("pixClus_z", 500, -250, 250))
# Define the basic track cuts for hits plots
cluster_cuts_pos = AllCuts()
cluster_cuts_pos.Add(MinCut("pixClus_z", 800))
cluster_cuts_neg = AllCuts()
cluster_cuts_neg.Add(MaxCut("pixClus_z", -800))


sa.Add(DerivedBranch("pixClus_r","sqrt(pixClus_x*pixClus_x+pixClus_y*pixClus_y)"))
sa.Add(PixelOccupancyObservables('pixel_occupancy','PixelOccupancyObservables'))
sa.Add(SCTOccupancyObservables('sct_occupancy','SCTOccupancyObservables'))
sa.Add(DumpSensorInfo())
sa.Add(Distribution("TruthVerticesAux.z", 500, -250, 250))
sa.Add(Distribution("PixelClustersAux.globalZ", 500, -250, 250))

# Define the basic track cuts for hits plots
cluster_cuts_pos = AllCuts()
cluster_cuts_pos.Add(MinCut("PixelClustersAux.globalZ", 800))
cluster_cuts_neg = AllCuts()
cluster_cuts_neg.Add(MaxCut("PixelClustersAux.globalZ", -800))


# Haave to define these AFTER making the derived branch
# A neater way than this is to define them as classes
# which only get initiated when they're being added
# see upg_perf_analyze.py

og = ObservableGroup("pixClusters_negZ")
og.SetCuts(cluster_cuts_neg)
og.Add(Distribution("pixClus_x", 350, 0, 350))
og.Add(Distribution("pixClus_r", 350, 0, 350))
og.Add(Distribution("pixClus_y", 350, 0, 350))
og.Add(Distribution("pixClus_z", 180*4, -1800, 1800))
og.SetPath("pixHits_negZ")
sa.Add(og)

og = ObservableGroup("pixClusters_posZ")
og.SetCuts(cluster_cuts_pos)
og.Add(Distribution("pixClus_x", 350, 0, 350))
og.Add(Distribution("pixClus_r", 350, 0, 350))
og.Add(Distribution("pixClus_y", 350, 0, 350))
og.Add(Distribution("pixClus_z", 180*4, -1800, 1800))
og.SetPath("pixHits_posZ")
sa.Add(og)



og = ObservableGroup("pixClusters_negZ")
og.SetCuts(cluster_cuts_neg)
og.Add(Distribution("PixelClustersAux.globalX", 350, 0, 350))
#og.Add(Distribution("pixClus_r", 350, 0, 350))
og.Add(Distribution("PixelClustersAux.globalY", 350, 0, 350))
og.Add(Distribution("PixelClustersAux.globalZ", 180*4, -1800, 1800))
og.SetPath("pixHits_negZ")
sa.Add(og)

og = ObservableGroup("pixClusters_posZ")
og.SetCuts(cluster_cuts_pos)
og.Add(Distribution("PixelClustersAux.globalX", 350, 0, 350))
#og.Add(Distribution("pixClus_r", 350, 0, 350))
og.Add(Distribution("PixelClustersAux.globalY", 350, 0, 350))
og.Add(Distribution("PixelClustersAux.globalZ", 180*4, -1800, 1800))
og.SetPath("pixHits_posZ")
sa.Add(og)



sa.AddStep(UpgPerfAna.EVENT)
ana.Add(sa)

sa=SingleAnalysis("SecondaryAnalysis",TreeHouse(ana.GetChain()))
sa.SetMaxEvents(5000)  # less statistics needed
sa.SetPath("occupancy_maps")
sa.Add(PixelOccupancy2D())
ana.Add(sa)


sa=SingleAnalysis("SecondaryAnalysis",TreeHouse(ana.GetChain()))
sa.SetMaxEvents(5000)  # less statistics needed
sa.SetPath("occupancy_maps")
sa.Add(Occupancy2D())
ana.Add(sa)



ana.Initialize()
ana.Execute()
ana.Finalize()

'''
