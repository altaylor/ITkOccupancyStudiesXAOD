#!/usr/bin/env python

import sys, glob, os

usage = """Script for making efficiency plots for the scoping document\n
    upg_scoping_plot_varHits_eff.py <tag> [<preliminary=True>]
where <tag> should match the outfilelabel argument passed to upg_scoping_varHits_eff.py"""

# Option parsing
if len(sys.argv)<2:
    print usage
    sys.exit()
tag = sys.argv[1]
if len(sys.argv)>2: public=bool(int(sys.argv[2]))
else: public=False

print "public=",public

from InDetUpgradePerformanceAnalysis.PlotMaker import *
from InDetUpgradePerformanceAnalysis.ScopingPlotting import *
import ROOT as R
R.gROOT.SetBatch()

def make_comparison_plots( path, label ):

    pmEff = PlotMaker()
    pmFr = PlotMaker()
    pmTr = PlotMaker()
    pmEff_pi = PlotMaker()

    for layout in [ "VF_Gold", "VF_Gold_m10", "VF_Silver", "Silver_m10", "Bronze", "Bronze_m10"]:
        infile = get_filename(layout, tag)

        if not os.path.exists(infile):
            print ""
            print "WARNING: Cannot find file: "+infile
            #print "         Will skip plot making for pdgId=%i, mu=%i" % (pdgId, mu)
            print ""
            continue

        xmax = 2.7
        #if "VF_Silver" in layout: xmax = 2.7
        if "VF_Gold" in layout: xmax = 4.0
        pmEff.AddPath(infile, path+"_muon",  legend= get_legend(  layout ),  
                linecolor=layout_color[layout], linestyle=layout_linestyle[layout], markerstyle=layout_markerstyle[layout], 
                xerr=True, )
        #kill_beyond=xmax 
        pmFr.AddPath(infile, path+"_incl",  legend= get_legend(  layout ),  
                linecolor=layout_color[layout], linestyle=layout_linestyle[layout], markerstyle=layout_markerstyle[layout], 
                xerr=True, kill_beyond=xmax )
        pmTr.AddPath(infile, path+"_incl",    legend= get_legend(  layout ),  
                linecolor=layout_color[layout], linestyle=layout_linestyle[layout], markerstyle=layout_markerstyle[layout], 
                xerr=True, kill_below=0.01)
        pmEff_pi.AddPath(infile, path+"_pion" , legend= get_legend(  layout ),  
                linecolor=layout_color[layout], linestyle=layout_linestyle[layout], markerstyle=layout_markerstyle[layout], 
                xerr=True)

    # Muon efficiencies
    pmEff.AddGraph('efficiencyPlot_etaSingleSidedMC', ytitle="Efficiency", legendloc='bottomright2', ymin=0., ymax=1.2, xtitle="|#eta|")
    pmEff.AddGraph('efficiencyPlot_varEtaSingleSidedMC', ytitle="Efficiency", legendloc='bottommid3', ymin=0., ymax=1.2, xrange=(0,4), xtitle="|#eta|")
    #pmEff.AddGraph('efficiencyPlot_truthPt', ytitle="Efficiency", legendloc='bottomright2', ymin =0.65, ymax=1.09, xtitle="p_{T} [GeV]", xrange=(5,100), scale_x=0.001, kill_below=0.5)
    pmEff.AddGraph('efficiencyPlot_varTruthPt', ytitle="Efficiency", legendloc='bottomright2', ymin=0.5, ymax=1.15, xtitle="p_{T} [GeV]", xrange=(0,100), scale_x=0.001, kill_below=0.)
    pmEff.AddGraph('efficiencyPlot_genVtx', ytitle="Efficiency", legendloc='topright2', ymin=0.7, ymax=1.2, xrange=(170,230), xtitle="nVtx_{mc}")

    # Exclusive fakes
    pmFr.AddGraph('frPlot_etaSingleSided', ytitle="Mis-reconstructed track fraction", legendloc='topright2', ymin =0.0001, ymax=0.08, log=True, xtitle="|#eta|")
    pmFr.AddGraph('frPlot_varEtaSingleSided', ytitle="Mis-reconstructed track fraction", legendloc='topright2', ymin =0.0001, ymax=0.08, log=True, xtitle="|#eta|")

    # Inclusive fakes
    range_mcVtx = (170,230)
    range_nTrk = (100, 1500)
    ytitle_nTrk = "nTrk^{reco}"
    xtitle_nVtx = "nVtx_{MC}"
    xtitle_nTrk = "nTrack_{reco}"
    ytitle_ratio = "nTrk_{reco}/nTrk_{MC}"
    ytitle_ratioNorm = "1/#epsilon #times nTrk^{reco}/nTrk^{truth}"
    pmTr.AddGraph("recoToTruthMatchedTrackRatio_numMcVx", xtitle=xtitle_nVtx, ytitle=ytitle_ratioNorm, xrange=range_mcVtx, ymax=1.17, legendloc="topright2")
    pmTr.AddGraph("recoTracks_numMcVx", ytitle=ytitle_nTrk, xtitle=xtitle_nVtx, xrange=range_mcVtx, legendloc="topright2", ymin=400, ymax=1800)
    pmTr.AddGraph("recoToTruthTrackRatio_numMcVx", xtitle=xtitle_nVtx, ytitle=ytitle_ratio, xrange=range_mcVtx, ymin=0.5, ymax=1.05, legendloc="topright2")
    pmTr.AddGraph("recoToTruthTrackRatio_nRecoTrack", xtitle=xtitle_nTrk, ytitle=ytitle_ratio, xrange=range_nTrk, ymax=1.6, legendloc="topright2")
    pmTr.AddGraph("recoToTruthMatchedTrackRatio_nRecoTrack", xtitle=xtitle_nTrk, ytitle=ytitle_ratioNorm, xrange=range_nTrk, ymax=1.2, legendloc="topright2")

    # Pion Efficiencies
    pmEff_pi.AddGraph('efficiencyPlot_etaSingleSidedMC', ytitle="Efficiency", legendloc='bottomright2', ymin =0.0, ymax=1.2, xtitle="|#eta|")
    pmEff_pi.AddGraph('efficiencyPlot_varEtaSingleSidedMC', ytitle="Efficiency", legendloc='bottommid3', ymin =0.0, ymax=1.2, xtitle="|#eta|")
    pmEff_pi.AddGraph('efficiencyPlot_varTruthPtPion', ytitle="Efficiency", legendloc='topfarright', ymin =0.5, ymax=1.2, xtitle="p_{T} [GeV]", xrange=(1,30), scale_x=0.001, kill_before=2.5e3, kill_beyond=30e3)

    pmFr.AddRatio("ratio", (0.9,1.1))
    # create plots
    pms = [ pmEff, pmTr, pmFr, pmEff_pi ]
    for pm in pms:
        pm.MakePlots( )
        pm.MakeLegend()
        pm.MakeAtlasLabel(preliminary=False, public=public)
    pmEff.MakeLayoutLabel( "Muon, <#mu>=190-210"+label )
    pmFr.MakeLayoutLabel( "<#mu>=190-210"+label )
    pmTr.MakeLayoutLabel( "<#mu>=190-210"+label )
    pmEff_pi.MakeLayoutLabel( "Pion, <#mu>=190-210"+label )

    # save plots into files
    directory = 'plots/%s/%s' % (tag+"_"+path, "incl")
    if public: directory+= "_public"
    pmTr.SavePlots(directory)
    pmFr.SavePlots(directory)
    directory = 'plots/%s/%s' % (tag+"_"+path, "muon")
    if public: directory+= "_public"
    pmEff.SavePlots(directory)
    directory = 'plots/%s/%s' % (tag+"_"+path, "pion")
    if public: directory+= "_public"
    pmEff_pi.SavePlots(directory)

def main():
    make_comparison_plots( "Zmumu_max1Holes", "")

if __name__ == "__main__":
    main()
