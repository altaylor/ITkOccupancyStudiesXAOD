#!/bin/bash

NAME=$1
CMD=$2
QUEUE=${3:-8nh}
TEST=${4:-0}

echo "Submitting job"
echo "  NAME=${NAME}"
echo "  CMD=${CMD}"
echo "  QUEUE=${QUEUE}"
echo "  TEST=${TEST}"

if [ $# -lt 2 ]
then
  echo "Usage: batch_sub.sh <name> <command>"
  exit
fi

mkdir -p logs
mkdir -p scripts

LOGFILE=logs/$NAME.log.txt
SCRIPT=$PWD/scripts/runBatch_$NAME.sh

cat <<EOF > $SCRIPT
cd ${TestArea}
source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh
asetup 20.6.0,,here
cd $PWD
$CMD
EOF
chmod +x ${SCRIPT}

CMD="bsub -o ${LOGFILE} -q ${QUEUE} -n 2 -R span[hosts=1],rusage[mem=2500] -J ${NAME} ${SCRIPT} "
echo "$CMD"
if [ $TEST -eq 0 ]; then
  echo "Submitting " $TEST
  $CMD
fi
