#!/usr/bin/env python

# Import the analysis package!
from InDetUpgradePerformanceAnalysis import SingleAnalysis, InDetUpgradeAnalysis, TreeHouse, CONTROL

# Import cut and observable definitions for the scoping analysis
from InDetUpgradePerformanceAnalysis.ScopingDefinitions import *

# Import custom binning definitions
# Alternative options are defined in python/CustomBinning.py
import InDetUpgradePerformanceAnalysis.CustomBinning

# Add derived branches definitions to calculate e.g. the nAllHoles / nAllHits variables
# see python/DerviedBranches.py
from InDetUpgradePerformanceAnalysis.DerivedBranches import DerivedBranchesScoping

# Get the configuration: input file(s), output file, number of events to process, pdgId etc
import InDetUpgradePerformanceAnalysis.Configuration as Configuration
options=Configuration.GetOptions()

outfile=options.outfile
infiles=options.inputfiles
minProb=options.minProb
maxEvents = options.maxEvents

# Now set up the analysis....
ana=InDetUpgradeAnalysis()
print ana
ana.SetOutputFileName(outfile)
ana.SetChain(options.treename)
ana.SetMaxEvents(maxEvents)

# Add input files
for f in infiles:
    print 'adding',f
    ana.AddFile(f)

#extended_eta = "VF" in options.layout
sa=SingleAnalysis("Zmumu",TreeHouse.Current())
sa.Add(DerivedBranchesScoping())
sa.AddStep(CONTROL)
for maxHoles in [1,]:
    folder = "Zmumu_max{0}Holes_incl".format(maxHoles)
    folder_mu = "Zmumu_max{0}Holes_muon".format(maxHoles)
    folder_pi = "Zmumu_max{0}Holes_pion".format(maxHoles)
    ## Muon resolutions vs eta
    sa.Add(ResolutionObservables(folder_mu, "Resolution_muon_maxHoles"+str(maxHoles), minProb, 
                                 [get_binning(), CustomBinning.SingleSidedVariableEtaBinning()],
                                 VariableEtaTrackCuts(maxHoles),
                                 fit=False, pdgId=13, truthMinPt=4e3 ))
    # Muon resolutions vs pT
    sa.Add(ResolutionObservables(folder_mu+"_pt", "ResolutionPt_muon_maxHoles"+str(maxHoles), minProb, 
                                 [CustomBinning.VariablePtBinning2(), ],
                                 VariableEtaTrackCuts(maxHoles),
                                 fit=False, pdgId=13, truthMinPt=1e3, truthMaxEta=1.0 ))
    # Pion resolutions vs eta
    sa.Add(ResolutionObservables(folder_pi, folder_pi+"_Resolution", minProb, 
                                 [get_binning(), CustomBinning.SingleSidedVariableEtaBinning()],
                                 VariableEtaTrackCuts(maxHoles),
                                 fit=False, pdgId=211, truthMinPt=4e3 ))
    ## Pion resolutions vs pT
    sa.Add(ResolutionObservables(folder_pi+"_pt", folder_pi+"_ResolutionPt", minProb, 
                                 [CustomBinning.VariablePtBinningPion(), ],
                                 VariableEtaTrackCuts(maxHoles),
                                 fit=False, pdgId=211, truthMinPt=1e3, truthMaxEta=2.7))
ana.Add(sa)
# And finally.... run the job!
# Processing is now handed over to C++ to do all the heavy lifting
try:
    ana.Initialize()
    ana.Execute()
    ana.Finalize()
except KeyboardInterrupt:
    print('%s got KeyboardInterrupt' % name)
