#!/bin/bash

NAME=$1
CMD=$2
FILES=$3
QUEUE=${4:-8nh}
TEST=${5:-0}
MAXFILES=${6:-999999}

echo "Submitting job"
echo "  NAME=${NAME}"
echo "  CMD=${CMD}"
echo "  QUEUE=${QUEUE}"
echo "  TEST=${TEST}"

if [ $# -lt 2 ]
then
  echo "Usage: batch_sub.sh <name> <command>"
  exit
fi

mkdir -p logs
mkdir -p scripts

LOGFILE=logs/$NAME.log.txt
SCRIPT=$PWD/scripts/runBatch_$NAME.sh

cat <<EOF > $SCRIPT
pwd
SCRATCH=\$PWD
mkdir infiles
for f in \`head -$MAXFILES $FILES\`;
do
  xrdcp \$f infiles/
done
find \$PWD/infiles -name "*root*" > files.txt
cd ${TestArea}
source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh
asetup 20.6.0,,here
cd \$SCRATCH
$CMD
cp *root $PWD
EOF
chmod +x ${SCRIPT}

CMD="bsub -o ${LOGFILE} -q ${QUEUE} -R rusage[mem=2500] -J ${NAME} ${SCRIPT} "
echo "$CMD"
if [ $TEST -eq 0 ]; then
  echo "Submitting " $TEST
  $CMD
fi
