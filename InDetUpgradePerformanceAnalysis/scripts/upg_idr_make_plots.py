#!/usr/bin/env python

from InDetUpgradePerformanceAnalysis.PlotMaker import *
import ROOT as R
R.gROOT.SetBatch()

import optparse, glob, os
usage = "Script for making plot for the IDR"
optP = optparse.OptionParser(usage=usage,conflict_handler="resolve")
optP.add_option('--infilelabel', action='store', dest='infilelabel',
                default='InDetUpPerfPlots', type='string', help='input file name label - should be the same as specified in --outfilelabel in upg_idr_analyze.py')
optP.add_option('--layout', action='store', dest='layout',
                default='ITk LoI', type='string', help='Name of the layout - used on plot label')
optP.add_option('--extendedEta', action='store_true', dest='extended_eta',
                default=False, help='Use extended eta range upt to |eta|<4 - for VF layouts')
optP.add_option('--outlabel', action='store', dest='outlabel',
                default='', type='string', help='')
optP.add_option('--preliminary', action='store_true', dest='preliminary',
                default=False, help='Add Preliminary label (rather than internal)')
(options, args) = optP.parse_args()

print options

pdgId_labels = {11: "Electron", 13: "Muon", 211: "Pion", 0:""}
pdgId_style = {11: 20, 13: 21, 211: 22}
pdgId_color = {11: R.kGreen, 13: R.kBlue, 211: R.kRed}
pdgId_yrange = {11: (0.7, 1.09), 13: (0.8, 1.09), 211: (0.7, 1.09) }

mu_style = {0: 20, 80: 21, 140: 22, 200:23}
mu_color = {0: R.kBlack, 80:R.kGreen, 140: R.kBlue, 200: R.kRed}

def get_dir(pt):
    dir = "pt%i" % (pt,)
    if options.extended_eta: 
        dir += "_extendedEta"
    return dir

def get_filename(pdgId):
    filename = "%s_pdgId%i.root" % (options.infilelabel, pdgId)
    return filename

def get_legend_pu(pt, mu):
    return 'p_{T}=%i GeV, <#mu>=%i' % (pt, mu)

def get_legend(pt, pdgId, mu):
    if pdgId:
        return 'p_{T}=%i GeV %ss' % (pt, pdgId_labels[pdgId])
    else:
        return 'p_{T}=%i GeV' % (pt,)

def make_plots(pdgId, mu, ):

    pm = PlotMaker()

    infile = get_filename(pdgId)

    if not os.path.exists(infile):
        print ""
        print "WARNING: Cannot find file: "+infile
        print "         Will skip plot making for pdgId=%i, mu=%i" % (pdgId, mu)
        print ""
        return

    #pm.AddPath(infile, get_dir(5),  legend= get_legend(  5, pdgId, mu),  linecolor=R.kBlue, markerstyle=23, xerr=False, pt=5)
    #pm.AddPath(infile, get_dir(15), legend= get_legend( 15, pdgId, mu),  linecolor=R.kRed, markerstyle=22, xerr=False, pt=15)
    #pm.AddPath(infile, get_dir(50), legend= get_legend( 50, pdgId, mu),  linecolor=R.kGreen, markerstyle=21, xerr=False, pt=50)
    #pm.AddPath(infile, get_dir(100),legend= get_legend(100, pdgId, mu),  linecolor=R.kBlack, markerstyle=20, xerr=False, pt=100)
    pm.AddPath(infile, get_dir(5),  legend= get_legend(  5, 0, mu),  linecolor=R.kBlue, markerstyle=23, xerr=False, )
    pm.AddPath(infile, get_dir(15), legend= get_legend( 15, 0, mu),  linecolor=R.kRed, markerstyle=22, xerr=False,  )
    pm.AddPath(infile, get_dir(50), legend= get_legend( 50, 0, mu),  linecolor=R.kGreen, markerstyle=21, xerr=False,)
    pm.AddPath(infile, get_dir(100),legend= get_legend(100, 0, mu),  linecolor=R.kBlack, markerstyle=20, xerr=False,)

    add_graphs(pm, pdgId)

    # create plots
    pm.MakePlots( )
    pm.MakeLegend()
    pm.MakeAtlasLabel(preliminary=options.preliminary)
    pm.MakeLayoutLabel( options.layout )
    pm.MakeParticleLabel( "%ss, <#mu>=%i" % (pdgId_labels[pdgId], mu,) )

    # save plots into files
    tag =options.infilelabel
    if options.outlabel: tag += "_" + options.outlabel
    directory = 'plots/%s/%s' % (tag, pdgId_labels[pdgId])
    pm.SavePlots(directory)

def add_graphs(pm, pdgId):

    binning = "etaSingleSided"
    binning_eff = "etaSingleSidedMC"

    # Resolutions
    pm.AddGraph('trk_d0_mc_perigee_d0_res_'+binning)
    pm.AddGraph('trk_z0_mc_perigee_z0_res_'+binning, )
    pm.AddGraph('trk_theta_mc_perigee_theta_res_'+binning, legendloc='topfarright')
    pm.AddGraph('trk_phi_mc_perigee_phi_res_'+binning, )
    pm.AddGraph('trk_qoverpt_mc_qoverpt_res_'+binning, ytitle='p_{T} #times #sigma q/p_{T}', legendloc='topleft', scale=True)

    pm.AddGraph('trk_qoverpt_mc_qoverpt_tailFrac_'+binning, ytitle='Fraction outside #pm 2#sigma', legendloc='topleft', scale=True, ymax=1.2)

    # Hits n Holes
    #pm.AddGraph('trk_nAllHits_bias_'+binning, legendloc='topleft', ytitle='<No. Track Hits>')
    #pm.AddGraph('trk_nAllHoles_bias_withOutliers_'+binning, legendloc='topleft', ytitle='<No. Track Holes>')
    #pm.AddGraph('trk_nPixHits_bias_'+binning, legendloc='topleft', ytitle='<No. Pixel Hits>')
    #pm.AddGraph('trk_nPixHoles_bias_withOutliers_'+binning, legendloc='topleft', ytitle='<No. Pixel Holes>')
    #pm.AddGraph('trk_nSCTHits_bias_'+binning, legendloc='topleft', ytitle='<No. Strip Hits>')
    #pm.AddGraph('trk_nSCTHoles_bias_withOutliers_'+binning, legendloc='topleft', ytitle='<No. Strip Holes>')

    # Efficiencies
    (ymin, ymax) = pdgId_yrange[pdgId]
    pm.AddGraph('efficiencyPlot_'+binning_eff, ytitle="Efficiency", legendloc='bottomleft', ymin =ymin, ymax=ymax)

def make_hits_plot(file, path):
    if not os.path.exists(file):
        return
    c = R.TCanvas()
    rf = R.TFile(file)
    gr_all = rf.Get(path+"/trk_nAllHits_bias_etaSingleSided")
    gr_sct = rf.Get(path+"/trk_nSCTHits_bias_etaSingleSided")
    gr_pix = rf.Get(path+"/trk_nPixHits_bias_etaSingleSided")
    if options.extended_eta:
        h = R.TH1D("hframe", "hframe", 1, 0, 4.0)
    else:
        h = R.TH1D("hframe", "hframe", 1, 0, 2.7)
    h.SetMaximum(26)
    h.GetXaxis().SetTitle("|#eta|")
    h.GetYaxis().SetTitle("<Hits on track>")
    h.Draw()
    gr_sct.SetLineColor(R.kBlue)
    gr_pix.SetLineColor(R.kRed)
    gr_sct.SetMarkerColor(R.kBlue)
    gr_pix.SetMarkerColor(R.kRed)
    gr_sct.SetMarkerStyle(22)
    gr_pix.SetMarkerStyle(23)
    gr_all.Draw("LP")
    gr_sct.Draw("LP")
    gr_pix.Draw("LP")

    AtlasUtil.AtlasLabel(0.2,0.86,1,text="Atlas", preliminary=options.preliminary, public=False, simulation=True)
    AtlasUtil.DrawText(0.2,0.8,text=options.layout+" Layout", size=0.045)
    leg = R.TLegend(0.2, 0.62, 0.4, 0.78)
    leg.SetBorderSize(0)
    leg.SetFillStyle(0)
    leg.AddEntry(gr_all, "Total Hits", "lp")
    leg.AddEntry(gr_pix, "Pixel Hits", "lp")
    leg.AddEntry(gr_sct, "Strip Hits", "lp")
    leg.Draw()

    tag =options.infilelabel
    if options.outlabel: tag += "_" + options.outlabel
    c.SaveAs("plots/"+tag+"/hits.eps")

def make_pu_comparison_plots( pdgId, ):

    pm = PlotMaker()
    infile = get_filename(pdgId)
    nF=0

    for mu in [0,80,140,200]:

        this_infile = infile.replace("pilup140", "pilup"+str(mu))

        if not os.path.exists(this_infile):
            print ""
            print "WARNING: Cannot find file: "+this_infile
            print "         Will skip plot making for pdgId=%i, mu=%i" % (pdgId, mu)
            print ""
            continue

        pm.AddPath(this_infile, get_dir(5),  legend= get_legend_pu(  5, mu),  linecolor=mu_color[mu], markerstyle=mu_style[mu], xerr=False, linestyle=R.kSolid)
        #pm.AddPath(this_infile, get_dir(100),legend= get_legend(100, pdgId, mu),  linecolor=pdgId_color[pdgId], markerstyle=pdgId_style[pdgId], xerr=False, linestyle=R.kDashed, )
        nF+=1

    if nF==0:
        return

    add_graphs( pm, pdgId)

    # create plots
    pm.MakePlots( )
    pm.MakeLegend()
    pm.MakeAtlasLabel(preliminary=options.preliminary)
    pm.MakeLayoutLabel( options.layout )
    pm.MakeParticleLabel( "%ss" % (pdgId_labels[pdgId], ) )

    # save plots into files
    tag =options.infilelabel
    if options.outlabel: tag += "_" + options.outlabel
    directory = 'plots/%s/pu_comparisons/%s' % (tag, pdgId_labels[pdgId])
    pm.SavePlots(directory)

def make_comparison_plots( mu, ):

    pm = PlotMaker()

    for pdgId in [11, 13, 211]:
        infile = get_filename(pdgId)

        if not os.path.exists(infile):
            print ""
            print "WARNING: Cannot find file: "+infile
            print "         Will skip plot making for pdgId=%i, mu=%i" % (pdgId, mu)
            print ""
            continue

        pm.AddPath(infile, get_dir(5),  legend= get_legend(  5, pdgId, mu),  linecolor=pdgId_color[pdgId], markerstyle=pdgId_style[pdgId], xerr=False, linestyle=R.kSolid)
        pm.AddPath(infile, get_dir(100),legend= get_legend(100, pdgId, mu),  linecolor=pdgId_color[pdgId], markerstyle=empty_marker(pdgId_style[pdgId]), xerr=False, linestyle=R.kDashed, )

    add_graphs( pm, 11)

    # create plots
    pm.MakePlots( )
    pm.MakeLegend()
    pm.MakeAtlasLabel(preliminary=options.preliminary)
    pm.MakeLayoutLabel( options.layout )
    pm.MakeParticleLabel( "<#mu>=%i" % (mu,) )

    # save plots into files
    tag =options.infilelabel
    if options.outlabel: tag += "_" + options.outlabel
    directory = 'plots/%s/%s' % (tag, "comparison")
    pm.SavePlots(directory)

def empty_marker(filled_marker):
    if filled_marker<23:
        return filled_marker+4
    else:
        return 32

def main():
    make_plots(11, 140)
    make_plots(13, 140)
    make_plots(211, 140)
    make_comparison_plots( 140 )
    make_pu_comparison_plots( 13 )
    make_pu_comparison_plots( 11 )
    if options.extended_eta:
        make_hits_plot( get_filename(13), "pt100_extendedEta")
    else:
        make_hits_plot( get_filename(13), "pt100")

if __name__ == "__main__":
    main()
