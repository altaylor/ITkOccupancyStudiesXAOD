#!/usr/bin/env python

import ROOT
import os

from ROOT import TFile, TCanvas, TMultiGraph, TLegend, TGaxis

mysettings={'trk_d0_mc_perigee_d0_res' : {
    'title':'d0',
    'xtitle':'#eta',
    'ytitle':'d0',
    'yrange':(0., 0.015),
    'legendloc':'topcenter'
    },
          'trk_z0_mc_perigee_z0_res': {
    'title':'z0',
    'xtitle':'#eta',
    'ytitle':'sigma z0',
    'yrange':(0., 0.015),
    'legendloc':'topcenter'
    },
          'trk_phi_mc_perigee_phi_res': {
    'title':'phi',
    'xtitle':'#eta',
    'ytitle':'sigma phi',
    'yrange':(0., 0.015),
    'legendloc':'topcenter'
    },
          'trk_theta_mc_perigee_theta_res': {
    'title':'theta',
    'xtitle':'#eta',
    'ytitle':'sigma theta',
    'yrange':(0., 0.015),
    'legendloc':'topright'
    },
          'trk_pt_mc_gen_pt_res': {
    'title':'pt',
    'xtitle':'#eta',
    'ytitle':'sigma pt',
    'yrange':(0., 0.015),
    'legendloc':'topcenter'
    },
          'trk_z0_mc_perigee_z0_mc_perigee_theta_res': {
    'title':'z0*sin theta',
    'xtitle':'#eta',
    'ytitle':'sigma z0*sin theta',
    'yrange':(0., 0.015),
    'legendloc':'topright'
    },
          'trk_qoverp_mc_perigee_qoverp_mc_perigee_theta_res': {
    'title':'q/p',
    'xtitle':'#eta',
    'ytitle':'sigma(q / p)',
    'yrange':(0., 0.015),
    'legendloc':'topcenter'
    },
          'effPlot': {
    'title':'Efficiency',
    'xtitle':'#eta',
    'ytitle':'Efficiency',
    'yrange':(0.,1.01),
    'legendloc':'topleft'
    },
      'frPlot': {
    'title':'Fake Rate',
    'xtitle':'#eta',
    'ytitle':'Fake Rate',
    'yrange':(0.,0.015),
    'legendloc':'topcenter'
    }       
     }

class MyRootStyle :
    def __init__(self):
        from ROOT import gROOT, gStyle
        gROOT.SetStyle('Plain')
        TGaxis.SetMaxDigits(3)
        gStyle.SetPadTickX(1)
        gStyle.SetPadTickY(1)
        #gStyle.SetTextFont(132)
        #gStyle.SetTitleFont(132)
        #gStyle.SetLabelFont(132)
        #gStyle.SetLegendFont(12)

class PlotMaker :
    def __init__(self):
        self.graphs=[]
        self.canvas={}
        self.legends={}
        self.mgraphs={}
        self.frames={}

        self.defaultoptions={ 'linecolor' : ROOT.kBlack,
                              'markerstyle' : 20
                              }
        self.filepath=[]
        self.files=[]
        self.Initialize()
        self.settings=mysettings

    def AddGraph(self,gr,opt=None,**kwargs):
        if opt==None:
            opt=self.defaultoptions.copy()
        for k,v in kwargs.iteritems():
            print k,v
            opt[k]=v
        self.graphs.append( (gr,opt) )

    def Initialize(self):
        # TODO: automatize 
        self.AddGraph('trk_d0_mc_perigee_d0_res')
        self.AddGraph('trk_z0_mc_perigee_z0_res')
        self.AddGraph('trk_phi_mc_perigee_phi_res')
        self.AddGraph('trk_theta_mc_perigee_theta_res')
        self.AddGraph('trk_pt_mc_gen_pt_res')
        self.AddGraph('trk_z0_mc_perigee_z0_mc_perigee_theta_res')
        self.AddGraph('trk_qoverp_mc_perigee_qoverp_mc_perigee_theta_res')
        self.AddGraph('effPlot')
        self.AddGraph('frPlot')
        
        
    def AddPath(self,fname,pathname,**kwargs) :
        ltitle=pathname
        for k,v in kwargs.iteritems():
            print k,v
            if k=='legend':
                ltitle=v
        self.filepath.append( (fname,pathname,ltitle,kwargs) )

    def GetLegend(self,gr):
        if self.legends.has_key(gr):
            leg=self.legends[gr]
        else:
            legendloc=self.settings[gr]['legendloc']
            if legendloc=='topcenter':
                yys=(0.31,0.7,0.79,0.9)
            else:
                yys=(0.1,0.7,0.48,0.9)
            leg=TLegend(*yys)
            leg.SetFillStyle(0)
            leg.SetBorderSize(0)
                                     
            self.legends[gr]=leg

        return leg

    def MakePlots(self):
        for fname,pathname,ltitle,fopt in self.filepath:
            print fname,pathname
            f=TFile.Open(fname)
            print fname,':',
            print fopt
            self.files.append(f)
            for gr,opt in self.graphs:
                f.cd(pathname)
                graph=f.Get(pathname+'/'+gr)
                kwargs=opt.copy()
                kwargs.update(fopt)
                print kwargs
                self.UpdateGraph(graph,**kwargs)

                if self.mgraphs.has_key(gr):
                    mgraph=self.mgraphs[gr]
                else:
                    mgraph=TMultiGraph()
                    self.mgraphs[gr]=mgraph

                print graph
                leg=self.GetLegend(gr)
                    
                leg.AddEntry(graph,ltitle,"ep")
                mgraph.Add(graph)

              
        # draw multigraph
        for gr,opt in self.graphs:
            print gr
            c1=TCanvas()
            self.canvas[gr]=c1
            mgraph=self.mgraphs[gr]
            mgraph.Draw('Apl')
            c1.Modified()
            c1.Update()
            frame=mgraph.GetHistogram()
            self.frames[gr]=frame
            self.ApplySettings(gr,frame)

    def MakeLegend(self):                
        # draw legend
        for gr,opt in self.graphs:
            print gr
            if self.canvas.has_key(gr):
                c1=self.canvas[gr]
                leg=self.legends[gr]
                c1.cd()
                leg.Draw()
                c1.Modified()
                c1.Update()
            else:
                print 'ERROR: something wrong in MakeLegend'

        

    def ApplySettings(self,gr,frame):
        opt=self.settings[gr]
        for k,v in opt.iteritems():
            print k,v
            if k=='title':
                frame.SetTitle(v)
            elif k=='xtitle':
                frame.SetXTitle(v)
            elif k=='ytitle':
                frame.SetYTitle(v)

    def UpdateGraph(self,graph,**kwargs):
        print 'update graph'
        for k,v in kwargs.iteritems():
            print k,v
            if k=='linecolor':
                graph.SetLineColor(v)
                graph.SetMarkerColor(v)
            elif k=='markerstyle':
                graph.SetMarkerStyle(v)
                
        
        pass
                    
    def GetFrame(self,key):
        return self.frames[key]

    def SavePlots(self,directory):
        if not os.path.isdir('./'+directory):
            os.mkdir(directory)
        for gr,opt in self.graphs:
             if self.canvas.has_key(gr):
                c1=self.canvas[gr]
                c1.SaveAs(directory+'/'+gr+'.png')
                c1.SaveAs(directory+'/'+gr+'.eps')

if __name__ == '__main__':
    style=MyRootStyle()
    pm = PlotMaker()
    directory = 'plots'
    infile = 'OUT.root'
    from sys import argv
    if len(argv)>=3:
        infile=argv[1]
        directory=argv[2]
    pm.AddPath(infile,'pt5',legend='pt=5 GeV')
    pm.AddPath(infile,'pt15',legend='pt=15 GeV',linecolor=ROOT.kRed)
    pm.AddPath(infile,'pt50',legend='pt=50 GeV',linecolor=ROOT.kGreen)
    pm.AddPath(infile,'pt100',legend='pt=100 GeV',linecolor=ROOT.kBlue)

    # create plots
    pm.MakePlots()
    pm.MakeLegend()

    # fine tuning
    f=pm.GetFrame('trk_d0_mc_perigee_d0_res')
    f.SetTitle("d0 resolution")

    # save plots into files
    pm.SavePlots(directory)

