#Skeleton joboption for a simple analysis job

theApp.EvtMax=10                                         #says how many events to run over. Set to -1 for all events

import AthenaPoolCnvSvc.ReadAthenaPool                   #sets up reading of POOL files (e.g. xAODs)
svcMgr.EventSelector.InputCollections=[""]   #insert your list of input files here

algseq = CfgMgr.AthSequencer("AthAlgSeq")                #gets the main AthSequencer
#algseq += CfgMgr.MyPackageAlg()                                 #adds an instance of your alg to it

algseq += CfgMgr.MyAlg()   




include("AthAnalysisBaseComps/SuppressLogging.py")       #Optional include to suppress as much athena output as possible
