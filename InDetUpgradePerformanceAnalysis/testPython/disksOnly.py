#!/usr/bin/env python

# initialize & import various packages...


#import InDetUpgradePerformanceAnalysis as UpgPerfAna
#from InDetUpgradePerformanceAnalysis import *
#from InDetUpgradePerformanceAnalysis.geometry_fastGeo import AtlasDetector
#from InDetUpgradePerformanceAnalysis.sensorinfo_fastGeo import getSensorInfo


'''
from geometry_fastGeo import AtlasDetector 
from sensorinfo_fastGeo import getSensorInfo 

from ROOT import SensorInfoBase


'''






# Set up ROOT and RootCore:
import sys
import ROOT
import math
ROOT.gROOT.Macro( '$ROOTCOREDIR/scripts/load_packages.C' )

from ROOT import *
ROOT.gROOT.LoadMacro("AtlasStyle.C")

import itertools

# Initialize the xAOD infrastructure: 
if(not ROOT.xAOD.Init().isSuccess()): print "Failed xAOD.Init()"

from ROOT import TLorentzVector 
import ROOT as r
outputHists = [ ] 
outfile=ROOT.TFile.Open("OutputFile.root","RECREATE")
ROOT.gStyle.SetOptStat(False) #turn off stats boxes

fileName = "/afs/cern.ch/work/a/altaylor/ITK_DATA/InclinedBarrel/minbias.root"
treeName = "CollectionTree" # default when making transient tree anyway

f = ROOT.TFile.Open(fileName)
# Make the "transient tree":
t = ROOT.xAOD.MakeTransientTree( f, treeName)

#Choose Layout.

#itkLoi=AtlasDetector('ATLAS-P2-ITK-05') #ExtBrl32 -- ITK extended barrel @ 3.2                                                                    
#itkLoi=AtlasDetector('ATLAS-P2-ITK-06') #ExtBrl4 -- ITK extended barrel @ 4.0                                                                     
#itkLoi=AtlasDetector('ATLAS-P2-ITK-07') #IExtBrl4 -- ITK extended barrel, inclined layout @ 4.0                                                   
#itkLoi=AtlasDetector('ATLAS-P2-ITK-08') #InclBrl4 -- ITK fully inclined layout
#sensor=getSensorInfo(itkLoi)




#---------------------------------end of import packages.

# this is for the SCT Disks only
# ring.total gives the number of read out channels for each ring.
# number of read out channels for each ring is either 40960 or 81920

'''

nDisks = len(sensor.sctdisk)
print 'number of disks ' , nDisks

# counters. set up counters for each ring. there are 18 rings in each disk.
# these counters are to store the number of hits over N Events in each ring. 

diskCounter = [  ] 

for i,pd in enumerate(sensor.sctdisk):
        for j,ring in enumerate(pd.rings):
                print 'indices ', i , j 
                print 'testing ', ring.total
                diskCounter.append(ring.total)
'''


# Start the event loop.                                                                                       
print( "Number of input events: %s" % t.GetEntries() )
for entry in xrange(0,500):
    
    t.GetEntry( entry )
    #print( "Processing run #%i, event #%i" % ( t.EventInfo.runNumber(), t.EventInfo.eventNumber() ) )
        
    for i in xrange(t.SCT_Clusters.size()):

        sctCluster = t.SCT_Clusters.at(i)

        if sctCluster.auxdata("bec") is not 0:
                print sctCluster.auxdata("bec")
                
                

        '''
        
        sctClus_x.push_back(sctCluster.globalX())
        sctClus_y.push_back(sctCluster.globalY())
        sctClus_z.push_back(sctCluster.globalZ())
        sctClus_bec.push_back(sctCluster.auxdata("bec"))
        sctClus_eta_module.push_back(sctCluster.auxdata("eta_module"))
        sctClus_layer.push_back(sctCluster.auxdata("layer"))
        sctClus_detElementId.push_back(sctCluster.auxdata("detectorElementID"))
        sctClus_size.push_back(sctCluster.rdoIdentifierList().size())

        '''





        
pass #end event loop

'''
'''

ROOT.xAOD.ClearTransientTrees()

outfile.cd()

for i in outputHists:
    i.Write()
    i.Draw()
    name = str(i.GetName())+".pdf"
    #name = str(i.GetName())+".png"
    print name

    
    #ROOT.c1.SaveAs('histo_'+str(i).split('"')[1]+'.png') #get name of histogram from ROOT object info and save
    #ROOT.c1.SaveAs(str(i.GetName())+'.png"+'.png')
    #ROOT.c1.SaveAs(str(i.GetName()))
    ROOT.c1.SaveAs(name, ".pdf")
    #ROOT.c1.SaveAs(name, ".png");
    #ROOT.c1.SaveAs(name, ".pdf")
    

outfile.Write()
outfile.Close()

#        mycanvas->SaveAs("mcmatchedWboson.pdf", "pdf");



print 'Done.'

#sys.exit(0)

