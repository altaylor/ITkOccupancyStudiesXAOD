#!/usr/bin/env python
import os

from InDetUpgradePerformanceAnalysis import Configuration
found=os.path.isfile(Configuration.dbfile())
    
if not found:
    print 'dbfile',Configuration.dbfile(),'not found'
    raise Exception()

try:
    import sqlite3
    cnx = sqlite3.connect(Configuration.dbfile(),check_same_thread = False)
except:
    import sqlite
    cnx = sqlite.connect(Configuration.dbfile(),check_same_thread = False)
    


# open connection
cur=cnx.cursor()

class AtlasDetector:
    def __init__(self,version='ATLAS-IBL-01-00-00'):
        self.atlas = AtlasDetNode.root(version)
        self.nodelist = {}

    def findVersions(self,pattern):
        cur.execute('select TAG_NAME,TAG_COMMENT from HVS_TAG2NODE where NODE_ID=0 and TAG_NAME like ?  order by TAG_NAME',(pattern,))
        versions=cur.fetchall()
        self.dict={}
        for version,comment in versions:
            self.dict[version]=comment
        return self.dict.keys()

    def getComment(self,version):
        try:
            return self.dict[version]
        except KeyError:
            print version,'not found use findVersion first'
            return None

    def fetchNode(self,name,root):
        for node in root.children:
            if node.name.upper() == name:
                print 'found',node
                return node
            if node.version != None and node.version.upper() == name:
                print 'version found',node
                return node
            if node.isBranch:
                res = self.fetchNode(name,node)
                if res!= None:
                    return res;
        return None

    def __getitem__(self,name):
        if self.nodelist.has_key(name):
            return self.nodelist[name]
        node=self.fetchNode(name.upper(),self.atlas)
        if node != None and not node.isBranch:
            node.fetchData()
        self.nodelist[name]=node
        return node
        

class AtlasDetNode:

    @classmethod
    def root(cls,version='ATLAS-IBL-01-00-00',fetchChildren=False):
        cur.execute('select NODE_ID,TAG_ID from HVS_TAG2NODE where TAG_NAME = ? ',(version,))
        id,versionId=cur.fetchone()

        cur.execute('select NODE_NAME, PARENT_ID , BRANCH_FLAG , NODE_COMMENT from HVS_NODE where NODE_ID=?',(id,))
        name,parentID,isBranch,comment = cur.fetchone()
        #(0, u'ATLAS', None, 1, None)

        print 'found', version

        obj = cls(id,name,parentID,isBranch,comment,fetchChildren=False)
        obj.version=version
        obj.versionId=versionId
        obj.fetchChildren()
        return obj    


    def __init__(self,id,name,parent,isBranch=False,comment=None,fetchChildren=False):
        self.id=id
        self.name=name
        self.parentID=parent
        self.isBranch=isBranch
        self.comment=comment
        self.parent=None
        self.version=None
        self.versionId=None
        self.children=[]
        self.data=None
        if fetchChildren:
            self.fetchChildren(True)
    
    def fetchChildren(self,recursive=False):
        if self.isBranch:
            cur.execute('select * from HVS_NODE where PARENT_ID=? order by NODE_ID',(self.id,))
            res = cur.fetchall()
            self.children=[AtlasDetNode(*r) for r in res]
            if self.versionId != None:
                for c in self.children:
                    c.updateVersion(self.versionId)
                
            for c in self.children:
                c.fetchChildren(recursive)
        else:
            self.data=AtlasDetData(self.name,self.versionId)

    def updateVersion(self,parentVersion):
        
        cur.execute('select CHILD_TAG from HVS_LTAG2LTAG where PARENT_TAG = ? and CHILD_NODE = ?',(parentVersion,self.id))
        res=cur.fetchone()
        if res != None:
            self.versionId,=res

            cur.execute('select TAG_NAME from HVS_TAG2NODE where TAG_ID = ? ',(self.versionId,))
            self.version,=cur.fetchone()

    def fetchData(self):
        self.data.fetchData()
        return self.data

    def __getitem__(self,key):
        return self.data[key]

    def select(self,**kwarg):
        return self.data.select(**kwarg)

    def __str__(self):
        if self.version != None:
            return self.version
        return self.name

    def __repr__(self):
        version=''
        if self.version!=None:
            version=' - '+self.version
        
        if self.isBranch:
            return 'AtlasDetNode('+str(self.id)+','+self.name+','+str(self.isBranch)+')'+version
        else :
            return 'AtlasDetNode('+str(self.id)+','+self.name+')'+version


class AtlasDetData:
    def __init__(self,name,versionId):
        self.sdata=None
        self.name=name
        self.versionId=versionId

    def fetchData(self):
        if self.versionId != None:
            xxx_tag_id = self.name+'_TAG_ID'
            xxx_data_id = self.name+'_DATA_ID'
            xxx_data2tag = self.name+'_DATA2TAG'
            xxx_data = self.name+'_DATA'

            cmd = 'select * from %s where %s in ( select %s from %s where %s = ?)' %(xxx_data,xxx_data_id,xxx_data_id,xxx_data2tag,xxx_tag_id)
            cur.execute(cmd,(self.versionId,))
            self.data=cur.fetchall()
            self.desc=[d[0] for d in cur.description]

    def __str__(self):
        s='##'+self.name+'\n'
        for row in self.data:
            s+= '*** NEXT ***\n'
            for d,v in zip(self.desc,row):
                s+= d+'='+str(v)+'\n'
        return s

    def select(self,**kwarg):
        self.sdata=self.get(**kwarg)
        return self

    def __getitem__(self,key):
        if self.sdata !=None:
            try:
                return self.sdata[key.upper()]
            except:
                print 'value',key,'not found'
        return self.get(key)

    def get(self,*args,**kwargs):
        #print 'args',len(args)
        for key in args:
            if len(self.data)==1:
                d=dict(zip(self.desc, self.data[0]))
                try:
                    return d[key]
                except KeyError:
                    print key,'not found'
                    return None

        #print 'kwargs',len(kwargs)
        rows=[]
        values=[]
        if len(kwargs.keys())==1 and kwargs.keys()[0]=='ID':
            j=kwargs.values()[0]
        else:
            for key,value in kwargs.items():
                try:
                    i=self.desc.index(key.upper())
                    rows.append(zip(*self.data)[i])
                    values.append(value)
                except (ValueError,AttributeError):
                    print key,'=',value,' not found'
                    return {}

            j=list(zip(*rows)).index(tuple(values))
            if j<0:
                values = [ v.upper() for v in values ]
                j=list(zip(*rows)).index(tuple(values))

        if j>=0 and j<len(self.data):    
            return dict(zip(self.desc, self.data[j]))
        return {}

    def _old_get(self,*args,**kwargs):
        #print 'args',len(args)
        for key in args:
            if len(self.data)==1:
                d=dict(zip(self.desc, self.data[0]))
                try:
                    return d[key]
                except KeyError:
                    print key,'not found'
                    return None
        #print 'kwargs',len(kwargs)
        for key,value in kwargs.items():
            try:
                i=self.desc.index(key.upper())
                j=list(zip(*self.data)[i]).index(value)
                if j<0:
                    j=list(zip(*self.data)[i]).index(value.upper())
                if j>=0:    
                    return dict(zip(self.desc, self.data[j]))
            except (ValueError,AttributeError):
                print key,'=',value,' not found'
                return {}
            
        return {}



def simpleTest():
    cur.execute('select * from HVS_NODE where PARENT_ID=0 order by NODE_ID')
    # cur.description
    # NODE_ID NODE_NAME PARENT_ID BRANCH_FLAG NODE_COMMENT

    res = cur.fetchall()
    for r in res:
        print AtlasDetNode(*r)

    cur.execute("select * from HVS_TAG2NODE where TAG_NAME like 'ATLAS-IBL%' ")
    res = cur.fetchall()
    for r in res:
        print r
    

    # close connection
    cnx.commit()

#simpleTest()

def newTest():
    global atlas, pixRO
    atlas=AtlasDetNode.root('ATLAS-IBL-01-00-00')
    atlas.fetchChildren()
    pixRO=atlas.children[0].children[0].children[15]
    pixRO.getData()


if __name__=='__main__':
    atlas=AtlasDetector('ATLAS-IBL-01-00-00')

    pixModule=atlas['PixelModule']
    pixRO=atlas['PixelReadout']


    oldRO=AtlasDetNode.root('PixelReadout-02')

    g=atlas['pixellayer']
    print g.select(layer=2)['moduletype']
    print g.select(layer=0)['moduletype']
    print g['moduletype']


#cur.close()

