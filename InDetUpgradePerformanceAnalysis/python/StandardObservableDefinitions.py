from StandardCuts import *
import CustomBinning
from InDetUpgradePerformanceAnalysis  import ObservableGroup, Efficiency, FakeRate, DiffObservable, Z0SinThetaObs, QOverPtObs, Distribution, SingleObservable, PullObservable
import ROOT

class DiffObservableFitted(ROOT.DiffObservable(float)):
    def __init__(self, var, mcvar, binning=None, fractional=False):
        if binning==None:
            binning = CustomBinning.SingleSidedEtaBinning()
        ROOT.DiffObservable(float).__init__(self, var, mcvar, binning, fractional)
        self.DoFit(True)

class PullObservableFitted(ROOT.PullObservable(float)):
    def __init__(self, var, errvar, mcvar, binning=None):
        if binning==None:
            binning = CustomBinning.SingleSidedEtaBinning()
        ROOT.PullObservable(float).__init__(self, var, errvar, mcvar, binning)


# Definition of a set of resolution observables, but with Gaussian fitting
# Defined as a class so we can instantiate it multiple times for different pt/pdgid/matching-probability
class ResolutionObservables(ObservableGroup):
    def __init__(self,pdg, pt, prob, path, title,charge=None,fit=True, extended_eta=False, minPt=None):
        ObservableGroup.__init__(self,title)
        self.SetPath(path)
        # Use either a gaussian fit or the RMS to get the resolutions
        if fit:
            DiffObs = DiffObservableFitted
            PullObs = PullObservableFitted
        else:
            DiffObs = DiffObservable
            PullObs = PullObservable
        if extended_eta:
            binning = CustomBinning.SingleSidedEtaBinningExtended()
        else:
            binning = CustomBinning.SingleSidedEtaBinning()
        # Set cuts
        # Apply the TrackResolutionCuts defined above
        self.SetCuts(TrackResolutionCuts(pdg,pt,prob, minPt=minPt))
        # Resolutions
        self.Add(DiffObs( "trk_d0","mc_perigee_d0", binning))
        self.Add(DiffObs( "trk_z0","mc_perigee_z0", binning))
        self.Add(DiffObs( "trk_phi","mc_perigee_phi", binning))
        self.Add(DiffObs( "trk_theta","mc_perigee_theta", binning))
        #self.Add(DiffObs( "trk_z0SinTheta","mc_z0SinTheta", binning))
        ## Pulls
        self.Add(PullObs( "trk_d0","trk_d0_err", "mc_perigee_d0", binning))
        self.Add(PullObs( "trk_z0","trk_z0_err", "mc_perigee_z0", binning))
        self.Add(PullObs( "trk_theta","trk_theta_err", "mc_perigee_theta", binning))
        self.Add(PullObs( "trk_phi","trk_phi_err", "mc_perigee_phi", binning))

# Hit "resolution" obeservables - used for calculating the means and widths of the nHits distributions in bins
# Needs to be sepearate as dont apply track hits cuts
# Defined as a class so we can instantiate it multiple times for different pt/pdgid/matching-probability
class HitsResolutionObservables(ObservableGroup):
    def __init__(self,pdg, pt, prob, path, title,charge=None, extended_eta=False, minPt=None):
        ObservableGroup.__init__(self,title)
        self.SetPath(path)
        if extended_eta:
            binning = CustomBinning.SingleSidedEtaBinningExtended()
        else:
            binning = CustomBinning.SingleSidedEtaBinning()
        self.SetCuts(TrackCutsForHitsPlots(pdg,pt,prob,minPt))
        ## Hits
        self.Add(SingleObservable( "trk_nAllHits", binning))
        self.Add(SingleObservable( "trk_nAllHoles", binning))
        self.Add(SingleObservable( "trk_nSCTHits", binning))
        self.Add(SingleObservable( "trk_nSCTHoles", binning))
        self.Add(SingleObservable( "trk_nSCTDeadSensors", binning))
        self.Add(SingleObservable( "trk_nAllDeadSensors", binning))
        #self.Add(SingleObservable( "trk_nSCTHitsHoles", binning))
        #self.Add(SingleObservable( "trk_nSCTHitsHolesDead", binning))
        #self.Add(SingleObservable( "trk_nPixHitsHoles", binning))
        #self.Add(SingleObservable( "trk_nPixHitsHolesDead", binning))
        #self.Add(SingleObservable( "trk_nAllHitsHoles", binning))
        #self.Add(SingleObservable( "trk_nAllHitsHolesDead", binning))
        self.Add(SingleObservable( "trk_nPixHits", binning))
        self.Add(SingleObservable( "trk_nPixelDeadSensors", binning))
        self.Add(SingleObservable( "trk_nPixHoles", binning))


# Definition the set of momentum resolutions observables
# These have to be defined separately to remove th reco track pT cut
# which biases the resolution measuremnt
# Defined as a class so we can instantiate it multiple times for different pt/pdgid/matching-probability
class MomentumResolutionObservables(ObservableGroup):
    def __init__(self,pdg, pt, prob, path, title,charge=None,fit=True, extended_eta=False, minPt=None):
        ObservableGroup.__init__(self,title)
        self.SetPath(path)
        # Use either a gaussian fit or the RMS to get the resolutions
        if fit:
            DiffObs = DiffObservableFitted
            PullObs = PullObservableFitted
        else:
            DiffObs = DiffObservable
            PullObs = PullObservable
        if extended_eta:
            binning = CustomBinning.SingleSidedEtaBinningExtended()
        else:
            binning = CustomBinning.SingleSidedEtaBinning()
        # Set cuts
        # Apply the TrackResolutionCuts defined above
        self.SetCuts(TrackResolutionCuts(pdg,pt,prob, trackPtCut=False, minPt=minPt))
        self.Add(PullObs( "trk_qoverp","trk_qoverp_err", "mc_perigee_qoverp", binning))

        #qoverpt = DiffObs( "trk_qoverpt","mc_qoverpt", binning, True )
        #qoverpt.SetMaxRangeFinal(2.5)
        #if pdg==11: qoverpt.DoubleGaussianFit()
        #self.Add( qoverpt )

        qoverp = DiffObs( "trk_qoverp","mc_perigee_qoverp", binning, True )
        qoverp.SetMaxRangeFinal(2.5)
        if pdg==11: qoverp.DoubleGaussianFit()
        self.Add( qoverp )

        self.Add(DiffObs( "trk_pt","mc_gen_pt", binning, True))
        self.Add(QOverPtObs( "trk_pt","mc_perigee_qoverp", "mc_perigee_theta", binning))

# Definition of a set of efficiency observables
# Defined as a class so we can instantiate it multiple times for different pt/pdgid/matching-probability
class EfficiencyObservables(ObservableGroup):
    def __init__(self, pdg, pt, path, title, extended_eta=False, minPt=None):
        ObservableGroup.__init__(self,title)
        self.SetPath(path)
        # Selects only truth particles with same pdgid with pt+/-1 GeV, with eta/d0/z0 cuts 
        self.SetCuts(EfficiencyTruthParticleCuts(pdg,pt,extended_eta, minPt=minPt))
        if extended_eta:
            binning = CustomBinning.SingleSidedEtaBinningMCExtended()
        else:
            binning = CustomBinning.SingleSidedEtaBinningMC()
        eff=Efficiency( binning )
        #eff=Efficiency( binning )
        # Only count tracks passing quality cuts: d0/z0/pt/nHits   
        eff.SetCuts(BasicTrackCuts())
        self.Add(eff)
                     
# Definition of a set of efficiency observables - plotted as a function of deltaR to the nearest jet
# Defined as a class so we can instantiate it multiple times for different pt/pdgid/matching-probability
# Only works for BJetD3PD
class EfficiencyObservablesDeltaRJet(ObservableGroup):
    def __init__(self,pdg, pt, path, title):
        ObservableGroup.__init__(self,title)
        self.SetPath(path)
        # Selects only truth particles with same pdgid with pt+/-1 GeV, with eta/d0/z0 cuts 
        self.SetCuts(EfficiencyTruthParticleCuts(pdg,pt))
        eff=Efficiency("mc_gen_eta", "mc_gen_pt", "trk_mc_index" , "trk_mc_probability" , "trk_eta", 2, 10e3)
        # Only count tracks passing quality cuts: d0/z0/pt/nHits   
        eff.SetCuts(BasicTrackCuts())
        self.Add(eff)
                     
# Definition of a set of efficiency observables - plotted as a function of jet pT of the nearest jet
# Defined as a class so we can instantiate it multiple times for different pt/pdgid/matching-probability
# Only works for BJetD3PD
class EfficiencyObservablesJetPt(ObservableGroup):
    def __init__(self,pdg, pt, path, title):
        ObservableGroup.__init__(self,title)
        self.SetPath(path)
        # Selects only truth particles with same pdgid with pt+/-1 GeV, with eta/d0/z0 cuts 
        self.SetCuts(EfficiencyTruthParticleCuts(pdg,pt))
        eff=Efficiency("mc_gen_eta", "mc_gen_pt", "trk_mc_index" , "trk_mc_probability" , "trk_eta", 3, 10e3)
        # Only count tracks passing quality cuts: d0/z0/pt/nHits   
        eff.SetCuts(BasicTrackCuts())
        self.Add(eff)

# Definition of a set of fake rate plots
# Defined as a class so we can instantiate it multiple times for different pt/pdgid/matching-probability
class FakeRateObservables(ObservableGroup):
    def __init__(self,pdg, pt, path, title):
        ObservableGroup.__init__(self,title)
        self.SetPath(path)
        self.SetCuts(FakeRakeTrackSelection(pdg,pt))
        fr = FakeRate(CustomBinning.SingleSidedEtaBinning())
        self.Add(fr)
                     
# Definition of a set of tracj hits plots
# Defined as a class so we can instantiate it multiple times for different pt/pdgid/matching-probability
class TrackHitsObservables(ObservableGroup):
    def __init__(self, pdg, pt, prob, path, title):
        ObservableGroup.__init__(self,title)
        self.SetPath(path)
        self.SetCuts(TrackCutsForHitsPlots( pdg, pt, prob))
        # Arguments are:
        # variable_name, nBins, minBin, maxBin
        self.Add(Distribution("trk_nPixHits", 25, 0, 25))
        self.Add(Distribution("trk_nSCTHits", 25, 0, 25))
        self.Add(Distribution("trk_nPixHoles", 25, 0, 25))
        self.Add(Distribution("trk_nSCTHoles", 25, 0, 25))
        # These don't exist in the D3PD. We will add a "DerivedBranch" to
        # calculate them on the fly (see below)
        self.Add(Distribution("trk_nAllHits", 25, 0, 25))
        self.Add(Distribution("trk_nAllHoles", 25, 0, 25))

