# @file:    InDetUpgradePerformanceAnalysis/python/MyPyAlg.py
# @purpose: <put some purpose here>
# @author:  <put your name here>

__doc__     = 'some documentation here'
__version__ = '$Revision: 623437 $'
__author__  = '<put your name here>'

import AthenaCommon.SystemOfUnits as Units
import AthenaPython.PyAthena as PyAthena
from AthenaPython.PyAthena import StatusCode

from geometry_fastGeo import AtlasDetector
from sensorinfo_fastGeo import getSensorInfo
from occupancy import Counters

#numpy arrays used for tgraph in root
import ROOT
from ROOT import *
import numpy as np

class MyPyAlg (PyAthena.Alg):


    'put some documentation here'
    def __init__(self, name='MyPyAlg', **kw):
        ## init base class
        kw['name'] = name
        super(MyPyAlg, self).__init__(**kw)

        #self.ntuple_name = kw.get('ntuple_name','/ostream/ntuples/ntup')
        #itkLoi=AtlasDetector('ATLAS-P2-ITK-05') #ExtBrl32 -- ITK extended barrel @ 3.2                                                         
        #itkLoi=AtlasDetector('ATLAS-P2-ITK-06') #ExtBrl4 -- ITK extended barrel @ 4.0                                                          
        itkLoi=AtlasDetector('ATLAS-P2-ITK-07') #IExtBrl4 -- ITK extended barrel, inclined layout @ 4.0                                         
        #itkLoi=AtlasDetector('ATLAS-P2-ITK-08') #InclBrl4 -- ITK fully inclined layout
        sensor=getSensorInfo(itkLoi)

        # Set up counter holders, for the etaModuleIDs.
        
        self.CounterHolders = { }
        nLayer = len(sensor.sctlayer)
        print ' n layer is ', nLayer 
        doubleStripLength = False

        self.eventCounter = 0 

        #loop over the layers..

        for i in range(nLayer):
            print 'layer i is...', i

            #nModule = sensor.sctlayer[i].nmodule*sensor.sctlayer[i].nsegment
            # a factor of 2 which isn't understood yet is added to nModule
            nModule = 2*sensor.sctlayer[i].nmodule*sensor.sctlayer[i].nsegment

            print ' number of etaModules IDs in a layer is there ? ' , nModule 
            area = sensor.sctlayer[i].area

            if doubleStripLength and i < 3:
                nModule /= 2
                area *= 2

            nMax = nModule/2
            nMin = -nMax
            hasZero = (nModule%2!=0)
            d = { }

            # loop over the etaModuleIDs. note that in the xAOD they are are -ve -> +ve. 

            for m in range(nMin,nMax+1):
                #not certain on this if statement.. 
                if m!=0 or hasZero:
                    # i is the layer, m is etaModuleID. create tuple.

                    #use dict to associate layer, etaModuleID and counter together. 
                    self.CounterHolders[(i,m)] = Counters()
                    self.CounterHolders[(i,m)].tot_pix = sensor.sctlayer[i].perModuleEta
                    
                    # set the number of strips per sensor.
                    # perModuleEta gives the number of strips per sensor summed over phi and accounts for both sides
                    #print ' total number of readout over all phi ', sensor.sctlayer[i].perModuleEta
                    #print ' total number of pixels in a single sensor ', sensor.sctlayer[i].perModule

        return

    def initialize(self):
        self.msg.info('==> initialize...')

        self.hsvc = PyAthena.py_svc('THistSvc/THistSvc')
        myHist = TH1D("myHist","myHist",10,0,10)
        anotherHist = TH1D("anotherHist","anotherHist",10,0,10)
        self.hsvc.regHist("/MYSTREAM/myHist",myHist)

        return StatusCode.Success

    def execute(self):
        # loop over all counters and increment event. used for averaging at the end. 
        for item in self.CounterHolders:
            self.CounterHolders[item].eventIncrement()

        self.eventCounter += 1
        print ' event number ' , self.eventCounter 

            
        # loop over all clusters that are in the barrel
        for sctCluster in self.evtStore["SCT_Clusters"]:

            #ensure its in the barrel. 
            
            if sctCluster.auxdata("bec") == 0:
                
                etaModuleID = sctCluster.auxdata("eta_module")
                layer = sctCluster.auxdata("layer")

                x = sctCluster.globalX()
                y = sctCluster.globalY()
                z = sctCluster.globalZ()
                s = sctCluster.rdoIdentifierList().size()

                self.CounterHolders[(layer,etaModuleID)].add(x,y,z,s,sctCluster.auxdata("detectorElementID"))

                if etaModuleID > abs(56):
                    print ' Warning!!, issue with code !!!!! ' 

        return StatusCode.Success

    def finalize(self):

        #InDetTrackTree = TTree("InDetTrackTree", "InDetTrackTree")

        myHist2 = TH1D("myHist2","myHist2",10,0,10)
        self.hsvc.regHist("/MYSTREAM/myHist2",myHist2)

        sct_barrel_map2D = TH2D("sct_occ_map2d","occupancies in percent (200 pileup)",240,-3100.,3100.,160,0.,1100.)
        self.hsvc.regHist("/MYSTREAM/sct_occ_map2d",sct_occ_map2d)

        myTree = TTree("myTree","myTree")
        self.hsvc.regTree("/MYSTREAM/myTree",myTree)

        # < mu > = 200

        Occupancy_0 = ROOT.vector('float')()
        z_0 = ROOT.vector('float')()
        etaModuleID0 = ROOT.vector('float')()

        Occupancy_1 = ROOT.vector('float')()
        z_1 = ROOT.vector('float')()
        etaModuleID1 = ROOT.vector('float')()
        
        Occupancy_2 = ROOT.vector('float')()
        z_2 = ROOT.vector('float')()
        etaModuleID2 = ROOT.vector('float')()
        
        Occupancy_3 = ROOT.vector('float')()
        z_3 = ROOT.vector('float')()
        etaModuleID3 = ROOT.vector('float')()

        # 2D Occupancy Maps.
        # Hold on to all R/Z/Occupancy
        Occupancy_A= ROOT.vector('float')()
        z_A = ROOT.vector('float')()
        r_A = ROOT.vector('float')()

        myTree.Branch("Occupancy_0",Occupancy_0)
        myTree.Branch("z_0",z_0)
        myTree.Branch("etaModuleID0",etaModuleID0)
        
        myTree.Branch("Occupancy_1",Occupancy_1)
        myTree.Branch("z_1",z_1)
        myTree.Branch("etaModuleID1",etaModuleID1)
        
        myTree.Branch("Occupancy_2",Occupancy_2)
        myTree.Branch("z_2",z_2)
        myTree.Branch("etaModuleID2",etaModuleID2)
        
        myTree.Branch("Occupancy_3",Occupancy_3)
        myTree.Branch("z_3",z_3)
        myTree.Branch("etaModuleID3",etaModuleID3)

        myTree.Branch("Occupancy_A",Occupancy_A)
        myTree.Branch("z_A",z_A)
        myTree.Branch("r_A",r_A)

        pileUpFactor = 200
        # do for loop over counter holders, key[0] = layer, key[1] = etaModuleID. value = Counter. 
        
        for key, value in self.CounterHolders.items():

            Occupancy_A.push_back(value.occupancy()*pileUpFactor*100)
            z_A.push_back(value.z())
            r_A.push_back(value.r())

            sct_barrel_map2D.Fill(value.z(),value.r(),value.occupancy()*pileUpFactor*100)

            if value.occupancy != 0.0 :

                #print ' occupancy ' , value.occupancy()
                # factor of 100 to change occupancy into percent!

                if value.occupancy()*pileUpFactor*100 > 1.0:

                    print ' occupancy in percent! ' , value.occupancy()*pileUpFactor*100
                    print value.sum_evnt
                    print value.sum_clussize
                    print value.tot_pix
        
                    
            if key[0] == 0:
                Occupancy_0.push_back(value.occupancy()*pileUpFactor*100)
                z_0.push_back(value.z())
                etaModuleID0.push_back(key[1])

            if key[0] == 1:
                Occupancy_1.push_back(value.occupancy()*pileUpFactor*100)
                z_1.push_back(value.z())
                etaModuleID1.push_back(key[1])
                
            if key[0] == 2:
                Occupancy_2.push_back(value.occupancy()*pileUpFactor*100)
                z_2.push_back(value.z())
                etaModuleID2.push_back(key[1])

            if key[0] == 3:
                Occupancy_3.push_back(value.occupancy()*pileUpFactor*100)
                z_3.push_back(value.z())
                etaModuleID3.push_back(key[1])

        myTree.Fill()

        self.msg.info('==> finalize...')
        return StatusCode.Success

    # class MyPyAlg
