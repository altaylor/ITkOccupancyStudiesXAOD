from InDetUpgradePerformanceAnalysis import DerivedBranch, ObservableGroup

class DerivedBranches(ObservableGroup):
    def __init__(self, add_dead_holes=False):
        ObservableGroup.__init__(self,"DerivedBranches")
        self.SetRunLoop(False);
        if add_dead_holes:
            self.Add(DerivedBranch("trk_nSCTHitsHoles","trk_nSCTHits+trk_nSCTHoles"))
            self.Add(DerivedBranch("trk_nPixHitsHoles","trk_nPixHits+trk_nPixHoles"))
            self.Add(DerivedBranch("trk_nAllHitsHoles","trk_nPixHits+trk_nSCTHits+trk_nPixHoles+trk_nSCTHoles"))
            self.Add(DerivedBranch("trk_nAllPixHolesDead","trk_nPixHoles+trk_nPixelDeadSensors"))
            self.Add(DerivedBranch("trk_nAllSCTHolesDead","trk_nSCTHoles+trk_nSCTDeadSensors"))
            self.Add(DerivedBranch("trk_nPixHitsHolesDead","trk_nPixHits+trk_nPixHoles+trk_nPixelDeadSensors"))
            self.Add(DerivedBranch("trk_nSCTHitsHolesDead","trk_nSCTHits+trk_nSCTHoles+trk_nSCTDeadSensors"))
            self.Add(DerivedBranch("trk_nAllHitsHolesDead","trk_nPixHits+trk_nSCTHits+trk_nPixHoles+trk_nSCTHoles+trk_nPixelDeadSensors+trk_nSCTDeadSensors"))
        self.Add(DerivedBranch("trk_nAllDeadSensors","trk_nPixelDeadSensors+trk_nSCTDeadSensors"))
        self.Add(DerivedBranch("trk_nAllHitsDead","trk_nPixHits+trk_nSCTHits+trk_nPixelDeadSensors+trk_nSCTDeadSensors"))
        self.Add(DerivedBranch("trk_nAllHits","trk_nPixHits+trk_nSCTHits"))
        self.Add(DerivedBranch("trk_nAllHoles","trk_nPixHoles+trk_nSCTHoles"))
        self.Add(DerivedBranch("trk_z0SinTheta","trk_z0/TMath::CosH(trk_eta)"))
        #self.Add(DerivedBranch("mc_z0SinTheta","mc_perigee_z0*TMath::Sin(mc_perigee_theta)"))
        self.Add(DerivedBranch("trk_qoverpt","trk_qoverp*TMath::CosH(trk_eta)"))
        #self.Add(DerivedBranch("mc_qoverpt", "mc_perigee_qoverp/TMath::Sin(mc_perigee_theta)"))
        self.Add(DerivedBranch("trk_d0_err", "TMath::Sqrt(trk_cov_d0)"))
        self.Add(DerivedBranch("trk_z0_err", "TMath::Sqrt(trk_cov_z0)"))
        self.Add(DerivedBranch("trk_theta_err", "TMath::Sqrt(trk_cov_theta)"))
        self.Add(DerivedBranch("trk_phi_err", "TMath::Sqrt(trk_cov_phi)"))
        self.Add(DerivedBranch("trk_qoverp_err", "TMath::Sqrt(trk_cov_qoverp)"))

class DerivedBranchesRecoOnly(ObservableGroup):
    def __init__(self):
        ObservableGroup.__init__(self,"DerivedBranches")
        self.SetRunLoop(False);
        self.Add(DerivedBranch("trk_nAllHits","trk_nPixHits+trk_nSCTHits"))
        self.Add(DerivedBranch("trk_nAllHoles","trk_nPixHoles+trk_nSCTHoles"))
        self.Add(DerivedBranch("trk_z0SinTheta","trk_z0/TMath::CosH(trk_eta)"))
        #self.Add(DerivedBranch("mc_z0SinTheta","mc_perigee_z0*TMath::Sin(mc_perigee_theta)"))
        self.Add(DerivedBranch("trk_qoverpt","trk_qoverp*TMath::CosH(trk_eta)"))
        #self.Add(DerivedBranch("mc_qoverpt", "mc_perigee_qoverp/TMath::Sin(mc_perigee_theta)"))
        self.Add(DerivedBranch("trk_d0_err", "TMath::Sqrt(trk_cov_d0)"))
        self.Add(DerivedBranch("trk_z0_err", "TMath::Sqrt(trk_cov_z0)"))
        self.Add(DerivedBranch("trk_theta_err", "TMath::Sqrt(trk_cov_theta)"))
        self.Add(DerivedBranch("trk_phi_err", "TMath::Sqrt(trk_cov_phi)"))
        self.Add(DerivedBranch("trk_qoverp_err", "TMath::Sqrt(trk_cov_qoverp)"))

class DerivedBranchesScoping(ObservableGroup):
    def __init__(self):
        ObservableGroup.__init__(self,"DerivedBranches")
        self.SetRunLoop(False);
        self.Add(DerivedBranch("trk_nAllHits","trk_nPixHits+trk_nSCTHits"))
        #self.Add(DerivedBranch("trk_nAllHoles","trk_nPixHoles+trk_nSCTHoles"))
        #self.Add(DerivedBranch("trk_z0SinTheta","trk_z0/TMath::CosH(trk_eta)"))
        #self.Add(DerivedBranch("mc_z0SinTheta","mc_perigee_z0*TMath::Sin(mc_perigee_theta)"))
        #self.Add(DerivedBranch("trk_qoverpt","trk_qoverp*TMath::CosH(trk_eta)"))
        #self.Add(DerivedBranch("mc_qoverpt", "mc_perigee_qoverp/TMath::Sin(mc_perigee_theta)"))
        #self.Add(DerivedBranch("trk_d0_err", "TMath::Sqrt(trk_cov_d0)"))
        #self.Add(DerivedBranch("trk_z0_err", "TMath::Sqrt(trk_cov_z0)"))
        #self.Add(DerivedBranch("trk_theta_err", "TMath::Sqrt(trk_cov_theta)"))
        #self.Add(DerivedBranch("trk_phi_err", "TMath::Sqrt(trk_cov_phi)"))
        #self.Add(DerivedBranch("trk_qoverp_err", "TMath::Sqrt(trk_cov_qoverp)"))
