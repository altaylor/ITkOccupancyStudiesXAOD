#!/usr/bin/env python

#import InDetUpgradePerformanceAnalysis as UpgPerfAna
#from InDetUpgradePerformanceAnalysis import *
from geometry_fastGeo import AtlasDetector
from sensorinfo_fastGeo import getSensorInfo

#from ROOT import SensorInfoBase

itkLoi=AtlasDetector('ATLAS-P2-ITK-05') #ExtBrl32 -- ITK extended barrel @ 3.2                                                            
#itkLoi=AtlasDetector('ATLAS-P2-ITK-06') #ExtBrl4 -- ITK extended barrel @ 4.0                                                            
#itkLoi=AtlasDetector('ATLAS-P2-ITK-07') #IExtBrl4 -- ITK extended barrel, inclined layout @ 4.0                                          
#itkLoi=AtlasDetector('ATLAS-P2-ITK-08') #InclBrl4 -- ITK fully inclined layout

sensor=getSensorInfo(itkLoi)

# this is for the SCT Barrel

nLayer = len(sensor.sctlayer)
doubleStripLength = False 

testCounter = 0 
for i in range(nLayer):

    print 'layer i is...', i
    
    nModule = sensor.sctlayer[i].nmodule*sensor.sctlayer[i].nsegment
    area = sensor.sctlayer[i].area

    if doubleStripLength and i < 3:
        nModule /= 2
        area *= 2

    nMax = nModule/2
    nMin = -nMax
    hasZero = (nModule%2!=0)
    d = { }

    #Loop over number of sensors along Z (=nnodule*nsegment), phi symmetry?
    for m in range(nMin,nMax+1):
        if m!=0 or hasZero:
            # set the number of strips per sensor.
            # perModuleEta gives the number of strips per sensor summed over phi and accounts for both sides..
            # per Module gives the number of strips per sensor..
            print ' total number of readout over all phi ', sensor.sctlayer[i].perModuleEta
            print ' total number of pixels in a single sensor ', sensor.sctlayer[i].perModule
            
            testCounter += 1
            #d[m]=Counters(tot_pix=sensor.sctlayer[i].perModuleEta)

            #layerInfo->getValue("perModule")

# this is for the SCT DISKS

#number of read out channels for each ring is either 40960 or 81920

nDisks = len(sensor.sctdisk)
print 'number of disks ' , nDisks

for i,pd in enumerate(sensor.sctdisk):
        for j,ring in enumerate(pd.rings):
                print 'indices ', i , j 
                print 'testing ', ring.total



