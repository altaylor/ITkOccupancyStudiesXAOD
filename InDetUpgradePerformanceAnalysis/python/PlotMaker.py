import ROOT as R
import os, math
from ROOT import TFile, TCanvas, TMultiGraph, TLegend, TGaxis
import AtlasUtil, AtlasStyle

mysettings={
    
          'trk_d0_mc_perigee_d0_res_etaSingleSided' : {
    'title':'d0',
    'xtitle':'|#eta|',
    'ytitle':'#sigma d_{0} (mm)',
    'legendloc': ['topleft', 'topleft2'],
    },
          'trk_d0_mc_perigee_d0_res_etaDoubleSided' : {
    'title':'d0',
    'xtitle':'#eta',
    'ytitle':'#sigma d_{0} (mm)',
    'legendloc': ['topcenter', 'topleft2'],
    },
          'trk_d0_mc_perigee_d0_res_phi' : {
    'title':'d0',
    'xtitle':'#phi',
    'ytitle':'#sigma d_{0} (mm)',
    'legendloc': ['midleft', 'topleft2'],
    },
          'trk_d0_mc_perigee_d0_res_genVtx' : {
    'title':'d0',
    'xtitle':'No. Generated Vertices',
    'ytitle':'#sigma d_{0} (mm)',
    'legendloc': ['midleft', 'topleft2'],
    },
          'trk_d0_mc_perigee_d0_res_nHits' : {
    'title':'d0',
    'xtitle':'No. Track Hits',
    'ytitle':'#sigma d_{0} (mm)',
    'legendloc': ['topcenter', 'topleft2'],
    },
          'trk_z0_mc_perigee_z0_res_etaSingleSided': {
    'title':'z0',
    'xtitle':'|#eta|',
    'ytitle':'#sigma z_{0} (mm)',
    'legendloc': ['topleft', 'topleft2'],
    },
          'trk_phi_mc_perigee_phi_res_etaSingleSided': {
    'title':'phi',
    'xtitle':'|#eta|',
    'ytitle':'#sigma #phi (rad)',
    'legendloc': ['topleft', 'topleft2'],
    },
          'trk_theta_mc_perigee_theta_res_etaSingleSided': {
    'title':'theta',
    'xtitle':'|#eta|',
    'ytitle':'#sigma #theta (rad)',
    'legendloc': 'topfarright',
    },
          'trk_pt_mc_gen_pt_res_etaSingleSided': {
    'title':'pt',
    'xtitle':'|#eta|',
    'ytitle':'#sigma p_{T} ',
    'legendloc':'topleft'
    },
          'trk_z0_mc_perigee_z0_mc_perigee_theta_res_etaSingleSided': {
    'title':'z0*sin theta',
    'xtitle':'|#eta|',
    'ytitle':'#sigma z_{0}#sin #theta ',
    'legendloc':'topright'
    },
          'trk_qoverp_mc_perigee_qoverp_mc_perigee_theta_res_etaSingleSided': {
    'title':'q/p',
    'xtitle':'|#eta|',
    'ytitle':'p_{T} #times #sigma q/p_{T}',
    'legendloc': ['topleft', 'topleft2'],
    },
          # vs eta variable
          'trk_d0_mc_perigee_d0_res_varEtaSingleSided' : {
    'title':'d0',
    'xtitle':'|#eta|',
    'ytitle':'#sigma d_{0} (mm)',
    'legendloc': ['topleft', 'topleft2'],
    },
          'trk_z0_mc_perigee_z0_res_varEtaSingleSided': {
    'title':'z0',
    'xtitle':'|#eta|',
    'ytitle':'#sigma z_{0} (mm)',
    'legendloc': ['topleft', 'topleft2'],
    },
          'trk_phi_mc_perigee_phi_res_varEtaSingleSided': {
    'title':'phi',
    'xtitle':'|#eta|',
    'ytitle':'#sigma #phi (rad)',
    'legendloc': ['topleft', 'topleft2'],
    },
          'trk_theta_mc_perigee_theta_res_varEtaSingleSided': {
    'title':'theta',
    'xtitle':'|#eta|',
    'ytitle':'#sigma #theta (rad)',
    'legendloc': 'topfarright',
    },
          'trk_pt_mc_gen_pt_res_varEtaSingleSided': {
    'title':'pt',
    'xtitle':'|#eta|',
    'ytitle':'#sigma p_{T} ',
    'legendloc':'topleft'
    },
          'trk_z0_mc_perigee_z0_mc_perigee_theta_res_varEtaSingleSided': {
    'title':'z0*sin theta',
    'xtitle':'|#eta|',
    'ytitle':'#sigma z_{0}#sin #theta ',
    'legendloc':'totopright'
    },
          'trk_qoverp_mc_perigee_qoverp_mc_perigee_theta_res_varEtaSingleSided': {
    'title':'q/p',
    'xtitle':'|#eta|',
    'ytitle':'p_{T} #times #sigma q/p_{T}',
    'legendloc': ['topleft', 'topleft2'],
    },
          'efficiencyPlot': {
    'title':'Efficiency',
    'xtitle':'|#eta|',
    'ytitle':'Efficiency',
    'yrange':(0.501,1.01),
    'legendloc':'topleft'
    },
          # vs pT
          'trk_d0_mc_perigee_d0_res_varTrkPt' : {
    'title':'d0',
    'xtitle':'p_{T} [GeV]',
    'ytitle':'#sigma d_{0} (mm)',
    'legendloc': 'topfarright',
    'scale_x' : 0.001
    },
          'trk_z0_mc_perigee_z0_res_varTrkPt': {
    'title':'z0',
    'xtitle':'p_{T} [GeV]',
    'ytitle':'#sigma z_{0} (mm)',
    'legendloc': 'topfarright',
    'scale_x' : 0.001
    },
          'trk_phi_mc_perigee_phi_res_varTrkPt': {
    'title':'phi',
    'xtitle':'p_{T} [GeV]',
    'ytitle':'#sigma #phi (rad)',
    'legendloc': 'topfarright',
    'scale_x' : 0.001
    },
          'trk_theta_mc_perigee_theta_res_varTrkPt': {
    'title':'theta',
    'xtitle':'p_{T} [GeV]',
    'ytitle':'#sigma #theta (rad)',
    'legendloc': 'topfarright',
    'scale_x' : 0.001
    },
          'trk_pt_mc_gen_pt_res_varTrkPt': {
    'title':'pt',
    'xtitle':'p_{T} [GeV]',
    'ytitle':'#sigma p_{T} ',
    'legendloc':'topleft',
    'scale_x' : 0.001
    },
          'trk_z0_mc_perigee_z0_mc_perigee_theta_res_varTrkPt': {
    'title':'z0*sin theta',
    'xtitle':'p_{T} [GeV]',
    'ytitle':'#sigma z_{0}#sin #theta ',
    'legendloc':'topfarright',
    'scale_x' : 0.001
    },
          'trk_qoverp_mc_perigee_qoverp_mc_perigee_theta_res_varTrkPt': {
    'title':'q/p',
    'xtitle':'p_{T} [GeV]',
    'ytitle':'p_{T} #times #sigma q/p_{T}',
    'legendloc': 'topleft',
    'scale_x' : 0.001
    },
          'efficiencyPlot': {
    'title':'Efficiency',
    'xtitle':'p_{T} [GeV]',
    'ytitle':'Efficiency',
    'yrange':(0.501,1.01),
    'legendloc':'topleft',
    'scale_x' : 0.001
    },
          # vs pT
          'trk_d0_mc_perigee_d0_res_varTrkPtPion' : {
    'title':'d0',
    'xtitle':'p_{T} [GeV]',
    'ytitle':'#sigma d_{0} (mm)',
    'legendloc': 'topfarright',
    'scale_x' : 0.001
    },
          'trk_z0_mc_perigee_z0_res_varTrkPtPion': {
    'title':'z0',
    'xtitle':'p_{T} [GeV]',
    'ytitle':'#sigma z_{0} (mm)',
    'legendloc': 'topfarright',
    'scale_x' : 0.001
    },
          'trk_phi_mc_perigee_phi_res_varTrkPtPion': {
    'title':'phi',
    'xtitle':'p_{T} [GeV]',
    'ytitle':'#sigma #phi (rad)',
    'legendloc': 'topfarright',
    'scale_x' : 0.001,
    },
          'trk_theta_mc_perigee_theta_res_varTrkPtPion': {
    'title':'theta',
    'xtitle':'p_{T} [GeV]',
    'ytitle':'#sigma #theta (rad)',
    'legendloc': 'topfarright',
    'scale_x' : 0.001,
    },
          'trk_pt_mc_gen_pt_res_varTrkPtPion': {
    'title':'pt',
    'xtitle':'p_{T} [GeV]',
    'ytitle':'#sigma p_{T} ',
    'legendloc':'topleft',
    'scale_x' : 0.001,
    },
          'trk_z0_mc_perigee_z0_mc_perigee_theta_res_varTrkPtPion': {
    'title':'z0*sin theta',
    'xtitle':'p_{T} [GeV]',
    'ytitle':'#sigma z_{0}#sin #theta ',
    'legendloc':'topright',
    'scale_x' : 0.001,
    },
          'trk_qoverp_mc_perigee_qoverp_mc_perigee_theta_res_varTrkPtPion': {
    'title':'q/p',
    'xtitle':'p_{T} [GeV]',
    'ytitle':'p_{T} #times #sigma q/p_{T}',
    'legendloc': 'topleft',
    'scale_x' : 0.001,
    },
          'efficiencyPlot': {
    'title':'Efficiency',
    'xtitle':'|#eta|',
    'ytitle':'Efficiency',
    'yrange':(0.501,1.01),
    'legendloc':'topleft',
    },
      'frPlot': {
    'title':'Fake Rate Test',
    'xtitle':'#|eta|',
    'ytitle':'Fake-Rate',
    'legendloc':'topcenter'
    },
    'default': {
        'legendloc':'topright'
    }
    }

class MyRootStyle :
    def __init__(self):
        from ROOT import gROOT, gStyle
        gROOT.SetStyle('Plain')
        #axis.SetNoExponent(kTRUE).
        TGaxis.SetMaxDigits(3)
        gStyle.SetPadTickX(1)
        gStyle.SetPadTickY(1)
        gStyle.SetOptTitle(0)
        gStyle.SetOptStat(0)
        #gStyle.SetTextFont(132)
        #gStyle.SetTitleFont(132)
        #gStyle.SetLabelFont(132)
        #gStyle.SetLegendFont(12)

class RootFileParser(object):
    def __init__(self,fname):
        f=TFile.Open(fname)
        self.graphs={}
        self.histos={}
        self.parse(f,'')

    def parse(self,rdir,path):
        l=rdir.GetListOfKeys()
        for i in l:
            name=path+i.GetName()
            o=rdir.Get(i.GetName())
            otype= o.ClassName()
            if otype=='TDirectoryFile':
                self.parse(o,path+i.GetName()+'/')
            elif otype in ['TGraphErrors','TGraph','TMultiGraph'] :
                self.graphs[name]=o
            elif otype in ['TH2D','TH1D','TH1F'] :
                self.histos[name]=o
            else:
                print 'ERROR Type not recognised',otype

class PlotMaker :
    def __init__(self):
        self.graphs=[]
        self.graphnames={}
        self.histos=[]
        self.histonames={}
        self.canvas={}
        self.legends={}
        self.mgraphs={}
        self.frames={}
        self.path_graphs={}
        self.ratio_graphs = []
        self.ratio_mgrs = []

        self.defaultoptions={ 'linecolor' : R.kBlack,
                              'markerstyle' : 20
                              }
        self.filepath=[]
        self.files=[]
        self.Initialize()
        self.settings=mysettings
        self.labels_y = 0.2
        self.force_eta=False
        self.add_ratio = False
        self.ratio_label = ""
        self.ratio_yrange = None

        self.locations={ 
            'topcenter':(0.4,0.59,0.6,0.82),
            'topcenter2':(0.61,0.7,0.81,0.85),
            'topleft':(0.2,0.57,0.35,0.8),
            'bottomleft':(0.2,0.2,0.35,0.45),
            'bottomright3':(0.77,0.2,0.90,0.45),
            'bottomright2':(0.70,0.2,0.8,0.45),
            'bottomright':(0.60,0.2,0.85,0.45),
            'bottommid':(0.55,0.2,0.8,0.45),
            'bottommid2':(0.45,0.2,0.70,0.45),
            'bottommid3':(0.42,0.20,0.65,0.45),
            'topright':(0.48,0.65,0.9,0.9),
            'topfarright':(0.7,0.65,0.80,0.88),
            'topright2':(0.67,0.65,0.9,0.9),
            'midleft':(0.2,0.5,0.35,0.7),
            'topleft2':(0.36,0.7,0.56,0.85),
            'right':(0.52,0.4,0.9,0.7),
            'right2':(0.67,0.45,0.9,0.7),
            'right2a':(0.45,0.58,0.60,0.73),
            'right2b':(0.60,0.58,0.75,0.73),
            'right2c':(0.75,0.55,0.9,0.77),
            'topleft2a':(0.14,0.62,0.34,0.87),
            'topleft2b':(0.27,0.62,0.47,0.87),
            'topleft2c':(0.40,0.62,0.60,0.87),
            'topright2a':(0.44,0.62,0.64,0.87),
            'topright2b':(0.57,0.62,0.77,0.87),
            'topright2c':(0.70,0.62,0.90,0.87),
            'ratioright':(0.65,0.42,0.90,0.62),
            'ratiomid':(0.55,0.42,0.8,0.62)
            }


        self.xtitles={
            'etaSingleSided' : '|#eta|',
            'etaDoubleSided' : '#eta',
        }

    def AddRatio(self, label, yrange=None):
        self.add_ratio = True
        self.ratio_label = label
        self.ratio_yrange = yrange
            
    def AddGraph(self,gr,**kwargs):
        if not self.settings.has_key(gr):
            self.settings[gr]=self.settings['default'].copy()
        self.settings[gr].update(kwargs)
        self.graphs.append( (gr, self.settings[gr]) )
        grname = gr
        if kwargs.has_key('name'):
            grname+="_"+kwargs['name']
        self.graphnames[gr]=grname

    def AddHisto(self,hn,**opt):
        if not self.settings.has_key(hn):
            self.settings[hn]=self.settings['default'].copy()
        self.settings[hn].update(opt)
        self.histos.append( (hn,opt) )
        self.graphnames[hn] = hn
        #self.histonames[hn]=None

    def Initialize(self):
        pass
    
    def InitializeStandardPlots(self):

        #for binning in ["etaSingleSided", ]:
        for binning in ["etaSingleSided", "etaDoubleSided", "phi"]:
            self.AddGraph('trk_d0_mc_perigee_d0_res_'+binning)
            self.AddGraph('trk_z0_mc_perigee_z0_res_'+binning)
            self.AddGraph('trk_phi_mc_perigee_phi_res_'+binning)
            self.AddGraph('trk_theta_mc_perigee_theta_res_'+binning)
            self.AddGraph('trk_pt_mc_gen_pt_res_'+binning)
            self.AddGraph('trk_z0_mc_perigee_z0_mc_perigee_theta_res_'+binning)
            self.AddGraph('trk_qoverp_mc_perigee_qoverp_mc_perigee_theta_res_'+binning)
        #self.AddGraph('efficiencyPlot')
        #self.AddGraph('frPlot')
        
        
    def AddPath(self,fname,pathname,**kwargs) :
        #ltitle=pathname
        ltitle=None
        for k,v in kwargs.iteritems():
            if k=='legend':
                ltitle=v
        self.filepath.append( (fname,pathname,ltitle,kwargs) )

    def CalcLocation(self,loc):
        if type(loc)==list and len(loc)<=3:
            return [self.CalcLocation(l) for l in loc]
        else:
            if type(loc)==tuple and len(loc)==4:
                yys=loc
            elif type(loc)==str and self.locations.has_key(loc):
                yys=self.locations[loc]
            else:
                print 'WARNING legendloc',loc, 'not found, using default'
                yys=(0.1,0.7,0.48,0.9)
            return yys
        return None

    def GetLegend(self,gr,**kwargs):
        if self.legends.has_key(gr):
            legs=self.legends[gr]
        else:
            if kwargs.has_key('legendloc'):
                legendloc=kwargs['legendloc']
            else:
                legendloc=self.settings[gr]['legendloc']
            legendloc=self.CalcLocation(legendloc)
            yys=legendloc
            if type(legendloc)==list:
                yys=legendloc[0]
            legs=[TLegend(*yys)]
            legs[0].SetFillStyle(0)
            legs[0].SetFillColor(10)
            legs[0].SetBorderSize(0)
            legs[0].SetTextSize(0.04)

            if kwargs.has_key('ltitle') :
                ltitles=kwargs['ltitle']
                if type(ltitles)!=list:
                    ltitles=[ltitles]
                for i,lt in enumerate(ltitles):
                    if i>=len(legs):
                        if type(legendloc)==list and len(legendloc)>i:
                            yys=legendloc[i]
                        legs.append(TLegend(*yys))
                        legs[i].SetFillStyle(0)
                        legs[i].SetFillColor(10)
                        legs[i].SetBorderSize(0)
                        legs[i].SetTextSize(0.04)
                        
                    legs[i].SetHeader(lt)

            self.legends[gr]=legs

        return legs

    def MakePlots(self):
        for fname,pathname,ltitle,fopt in self.filepath:
            print '========================================================='
            print " Making plots for "+fname+" "+pathname
            print '========================================================='
            f=TFile.Open(fname)
            if not f or f.IsZombie() or not f.IsOpen():
                raise RuntimeError("File not found %s"% (fname,))
            self.files.append(f)
            for gr,opt in self.graphs:
                fullpath = pathname+'/'+gr
                print fullpath
                f.cd(pathname)
                graph=f.Get(fullpath)
                if not graph:
                    raise RuntimeError("Graph not found in input file "+fullpath)
                if not pathname in self.path_graphs.keys(): self.path_graphs[pathname] = []
                self.path_graphs[pathname].append(graph)
                kwargs=opt.copy()
                kwargs.update(fopt)
                if isinstance(graph,TMultiGraph):
                    #self.UpdateGraph(graph,**kwargs)                    
                    self.mgraphs[gr]=graph
                    legs=self.GetLegend(gr,**kwargs)
                    n=len(graph.GetListOfGraphs())/len(legs)
                    
                    for i,g in enumerate(graph.GetListOfGraphs()):
                        if ltitle: legs[i/n].AddEntry(g,g.GetTitle(),"lp")
                    graph.maximum = max([ g.GetHistogram().GetMaximum() for g in graph.GetListOfGraphs() ])                    
                else:
                    print fname, graph, kwargs
                    self.UpdateGraph(graph,**kwargs)
                    if self.mgraphs.has_key(gr):
                        mgraph=self.mgraphs[gr]
                    else:
                        mgraph=TMultiGraph()
                        mgraph.maximum=0
                        mgraph.graphs = []
                        self.mgraphs[gr]=mgraph

                    legs=self.GetLegend(gr,**kwargs)

                    if ltitle: legs[0].AddEntry(graph,ltitle,"lp")
                    gmax = graph_max( graph)
                    if gmax > mgraph.maximum: mgraph.maximum = gmax
                    mgraph.Add(graph)
                    mgraph.graphs.append(graph)
                    
            # Add histograms
            self.histonames[pathname+fname] = {}
            for hn,opt in self.histos:
                # Unique set of opts for this histo, histo opts+path opts
                kwargs = opt.copy()
                kwargs.update(fopt)
                if pathname:
                    h=f.Get(pathname+"/"+hn)
                else:
                    h=f.Get(hn)
                self.UpdateGraph(h,**kwargs)
                self.histonames[pathname+fname][hn]=h
                legs=self.GetLegend(hn,**kwargs)
                legs[0].AddEntry(h,ltitle,"l")
                # Need to set this for later
                opt['2d'] = (h.GetDimension()==2)
                #for command,param in opts:
                    #if hasattr(h,command):
                        #getattr(h,command)

        # draw multigraph
        for gr,opt in self.graphs:
            c1=TCanvas()
            if self.add_ratio:
                p1 = R.TPad("","",0,0.35,1,1)
                p1.SetBottomMargin(0.03)
                p1.Draw()
                p2 = R.TPad("","",0,0.0,1,0.34)
                p2.Draw()
                p2.SetBottomMargin(0.30)
                p2.SetTopMargin(0.)
                p2.SetGridy()
                p1.cd()
                c1.p1 = p1
                c1.p2 = p2
            self.canvas[gr]=c1
            mgraph=self.mgraphs[gr]
            mgraph.SetMaximum( mgraph.maximum * 1.3 )
            if self.add_ratio:
                p2.cd()
                self.DrawRatio(mgraph, **self.settings[gr])
                p1.cd()
            if 'nolines' in opt and opt['nolines']==True: mgraph.Draw('Ap')
            else: mgraph.Draw('Apl')
            if self.add_ratio:
                mgraph.GetXaxis().SetLabelOffset(10)
                mgraph.GetYaxis().SetTitleOffset(0.92)
                mgraph.GetYaxis().SetTitleSize(0.06)
                mgraph.GetXaxis().SetTitleSize(0.06)
                mgraph.GetYaxis().SetLabelSize(0.06)
                mgraph.GetXaxis().SetLabelSize(0.06)
            c1.Modified()
            c1.Update()
            frame=mgraph.GetHistogram()
            self.frames[gr]=frame
            self.ApplySettings(gr,frame,opt)
            if 'xrange' in self.settings[gr]:
                mgraph.GetXaxis().SetLimits(*self.settings[gr]['xrange'])
            if "log" in self.settings[gr].keys() and self.settings[gr]['log']: 
                if self.add_ratio: p1.SetLogy()
                else: c1.SetLogy()
            c1.Modified()
            c1.Update()

        # draw histograms 
        for hn, opt in self.histos:
            if 'drawopt' in opt.keys():
                drawopt = opt['drawopt']
            else:
                drawopt = ''
            if opt['2d']:
                for fname,pathname,ltitle,fopt in self.filepath:
                    c1=TCanvas()
                    h = self.histonames[pathname+fname][hn]
                    self.canvas[hn+'_'+pathname]=c1
                    h.Draw('colz'+drawopt)   
                    self.frames[hn+'_'+pathname]=h
                    self.ApplySettings(hn,h)
            else:
                c1=TCanvas()
                c1.SetTopMargin(0.05)
                self.canvas[hn]=c1
                same = ""
                hmax=0
                for fname,pathname,ltitle,fopt in self.filepath:
                    h = self.histonames[pathname+fname][hn]
                    h.Draw(drawopt+same)   
                    if h.GetMaximum()>hmax: hmax=h.GetMaximum()
                    if not hn in self.frames.keys(): self.frames[hn]=h
                    same = "same"
                self.frames[hn].SetMaximum(hmax*1.1)
                if "log" in self.settings[hn].keys() and self.settings[hn]['log']: 
                    c1.SetLogy()
                self.ApplySettings(hn,self.frames[hn],opt)

    def DrawRatio(self, mgraph, **kwargs):
        mgr = R.TMultiGraph()
        graphs = []
        g0 = mgraph.graphs[0]
        mgr = R.TMultiGraph()
        for g in mgraph.graphs[1:]:
            gr = g.Clone(g.GetName()+"_ratio")
            for iP in range(gr.GetN()):
                x, y = R.Double(), R.Double()
                x0, y0 = R.Double(), R.Double()
                gr.GetPoint(iP, x, y)
                g0.GetPoint(iP, x0, y0)
                yerr = gr.GetErrorY(iP)
                xerr = gr.GetErrorX(iP)
                yerr0 = g0.GetErrorY(iP)
                yr = y/y0
                if y>0: yrerr = yr * math.sqrt( (yerr/y)**2 + (yerr0/y0)**2)
                else: yerr=0
                gr.SetPoint(iP, x, yr)
                gr.SetPointError(iP, xerr, yrerr)
            mgr.Add(gr, "lp")
            self.ratio_graphs.append(gr)
            #self.UpdateGraph(gr,**kwargs)
        mgr.Draw("Alp")
        mgr.GetYaxis().SetTitle(self.ratio_label)
        mgr.GetYaxis().SetTitleSize(0.1)
        mgr.GetXaxis().SetTitleSize(0.1)
        mgr.GetYaxis().SetLabelSize(0.1)
        mgr.GetXaxis().SetLabelSize(0.1)
        mgr.GetYaxis().SetTitleOffset(0.6)
        mgr.GetXaxis().SetTitle(kwargs["xtitle"])
        mgr.GetYaxis().SetNdivisions(4,5,0)
        if self.ratio_yrange:
            mgr.GetYaxis().SetRangeUser(self.ratio_yrange[0], self.ratio_yrange[1])
        if 'xrange' in kwargs.keys():
            mgr.GetXaxis().SetLimits(*kwargs['xrange'])
        self.ratio_mgrs.append(mgr)
           
    def MakeAtlasLabel(self, preliminary=False, public=False):
        #draw label
        for gr,opt in self.graphs+self.histos:
            if self.canvas.has_key(gr):
                c1=self.canvas[gr]
                c1.cd()
                if self.add_ratio:
                    ltop = 0.89
                else:
                    ltop = 0.86
                AtlasUtil.AtlasLabel(self.labels_y, ltop, 1, text="Atlas", preliminary=preliminary, public=public, simulation=True)
                c1.Modified()
                c1.Update()
            else:
                print 'ERROR: something wrong in MakeAtlasLabel'
           
    def MakeLayoutLabel(self, layout):
        #draw label
        for gr,opt in self.graphs+self.histos:
            if self.canvas.has_key(gr):
                c1=self.canvas[gr]
                c1.cd()
                if self.add_ratio: ltop=0.84
                else: ltop=0.815
                AtlasUtil.DrawText(self.labels_y, ltop, text=layout+" ", size=0.04)
                c1.Modified()
                c1.Update()
            else:
                print 'ERROR: something wrong in MakeAtlasLabel'
     
    def MakeParticleLabel(self, particle_label):
        #draw label
        for gr,opt in self.graphs+self.histos:
            if self.canvas.has_key(gr):
                c1=self.canvas[gr]
                c1.cd()
                legend_pos = self.settings[gr]['legendloc']
                if type(legend_pos) == list:
                    legend_pos = legend_pos[0]
                if  self.locations[legend_pos][0] >0.5:
                    leg_x, leg_y = self.locations[legend_pos][0], self.locations[legend_pos][1]
                    #place label 10 units below legend
                    AtlasUtil.DrawTextOneLine(leg_x,leg_y-0.04,text=particle_label)
                else:
                    leg_x, leg_y = self.locations[legend_pos][0], self.locations[legend_pos][3]
                    #place label 10 units above legend
                    AtlasUtil.DrawTextOneLine(leg_x,leg_y+0.02,text=particle_label)
                c1.Modified()
                c1.Update()
            else:
                print 'ERROR: something wrong in MakeParticleLabel'
   
    def MakeLegend(self):                
        # draw legend
        for gr,opt in self.graphs+self.histos:
            if self.canvas.has_key(gr):
                c1=self.canvas[gr]
                c1.cd()
                legs=self.legends[gr]
                for leg in legs:
                    leg.Draw()
                c1.Modified()
                c1.Update()
            else:
                print 'ERROR: something wrong in MakeLegend'

    def ApplySettings(self,gr,frame,opt=None):
        if not opt: opt=self.settings[gr]
        if not opt.has_key('Xtitle'): self.SetXtitle(opt,gr)
        for k,v in opt.iteritems():
            if k=='title':
                frame.SetTitle(v)
            elif k=='xtitle' and v:
                frame.SetXTitle(v)
            elif k=='ytitle' and v:
                frame.SetYTitle(v)
            elif k=='yrange':
                yaxis=frame.GetYaxis()
                yaxis.SetRangeUser(v[0],v[1])
            elif k=='ymin':
                print "set min",v
                frame.SetMinimum(v)
            elif k=='ymax':
                frame.SetMaximum(v)
            #elif k=='xrange':
                #xaxis=frame.GetXaxis()
                #xaxis.SetLimits(v[0],v[1])
                ##xaxis.SetRangeUser(v[0],v[1])

    def SetXtitle(self, opt, gr):
        for binning_name, xtitle in self.xtitles.iteritems():
            if binning_name in gr:
                opt['xtitle'] = xtitle

    def UpdateGraph(self,graph,**kwargs):
        for k,v in sorted(kwargs.items()):
            if k=='linecolor':
                graph.SetLineColor(v)
                graph.SetMarkerColor(v)
            elif k=='markerstyle':
                graph.SetMarkerStyle(v)
            elif k=='linestyle':
                graph.SetLineStyle(v)
            elif k=='linewidth':
                graph.SetLineWidth(v)
            elif k=='unitNorm':
                if v: graph.Scale(1./graph.Integral())
            elif k=='kill_below':
                self.RemovePointsBelowMin(graph, v)
            elif k=='kill_beyond':
                self.RemovePointsPast(graph, v)
            elif k=='kill_before':
                self.RemovePointsBefore(graph, v)
            elif k=='xerr' and not v:
                if hasattr(graph, "SetPointError"):
                    if hasattr(graph, 'GetN'): [ graph.SetPointError( bin, 0, graph.GetErrorY(bin) ) for bin in xrange(graph.GetN()) ]
            elif k=='scale' and "pt" in kwargs.keys():
                scale_graph(graph, kwargs["pt"])
            elif k=='scale_x':
                scale_graph_x(graph, v)
            elif k=='rebin':
                try: graph.Rebin(v)
                except: "can't rebin object", graph

        
        pass

    def RemovePointsBelowMin(self, graph, ymin):
        iP=0
        while iP<graph.GetN():
            x, y, = R.Double(), R.Double()
            graph.GetPoint(iP, x, y)
            if y<ymin or math.isnan(y):
                graph.RemovePoint(iP)
            else:
                iP+=1

    def RemovePointsPast(self, graph, xmax):
        print "Removing points beyond",xmax
        iP=0
        while iP<graph.GetN():
            x, y, = R.Double(), R.Double()
            graph.GetPoint(iP, x, y)
            if x>xmax or math.isnan(y):
                graph.RemovePoint(iP)
            else:
                iP+=1

    def RemovePointsBefore(self, graph, xmin):
        print "Removing points before",xmin
        iP=0
        while iP<graph.GetN():
            x, y, = R.Double(), R.Double()
            graph.GetPoint(iP, x, y)
            if x<xmin or math.isnan(y):
                graph.RemovePoint(iP)
            else:
                iP+=1
                    
    def GetFrame(self,key):
        return self.frames[key]

    def _savePlot(self,key,directory):
        dd=[directory]
        for d in self.graphnames[key].split('/'):
            pd='/'.join(dd)
            if not os.path.isdir(pd):
                os.makedirs(pd)
            dd.append(d)

        if self.canvas.has_key(key):
            c1=self.canvas[key]
            c1.SaveAs('/'.join(dd)+'.png')
            c1.SaveAs('/'.join(dd)+'.eps')
            c1.SaveAs('/'.join(dd)+'.C')

    def SavePlots(self,directory):
        for gr,opt in self.graphs:
            self._savePlot(gr,directory)

        for hn,opt in self.histos:
            self._savePlot(hn,directory)


def graph_max( graph ):
  """ Get the maximum Y point of the graph. """
  gmax = 0
  for i in range( 0, graph.GetN() ):
     p = ( R.Double(), R.Double() )
     graph.GetPoint( i, p[0], p[1] )
     if p[1] > gmax: gmax = p[1]
  return gmax

def scale_graph(graph, sf):
  print "SCALING ", sf
  for i in range( 0, graph.GetN() ):
     p = ( R.Double(), R.Double() )
     graph.GetPoint( i, p[0], p[1] )
     graph.SetPoint( i, p[0], p[1] *sf )

def scale_graph_x(graph, sf):
  for i in range( 0, graph.GetN() ):
     p = ( R.Double(), R.Double() )
     graph.GetPoint( i, p[0], p[1] )
     graph.SetPoint( i, p[0]*sf, p[1] )
     graph.SetPointError( i, graph.GetErrorX(i)*sf, graph.GetErrorY(i))
