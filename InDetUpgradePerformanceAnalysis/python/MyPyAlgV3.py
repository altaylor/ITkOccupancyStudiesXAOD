# @purpose: <put some purpose here>
# @author:  <put your name here>

__doc__     = 'some documentation here'
__version__ = '$Revision: 623437 $'
__author__  = '<put your name here>'

import AthenaCommon.SystemOfUnits as Units
import AthenaPython.PyAthena as PyAthena
from AthenaPython.PyAthena import StatusCode

import math 

from geometry_fastGeo import AtlasDetector
from sensorinfo_fastGeo import getSensorInfo
from occupancy import Counters

#numpy arrays used for tgraph in root
import ROOT
from ROOT import *
import numpy as np


class MyPyAlgV2 (PyAthena.Alg):
    'put some documentation here'
    def __init__(self, name='MyPyAlgV2', **kw):
        ## init base class
        kw['name'] = name
        super(MyPyAlgV2, self).__init__(**kw)

        #itkLoi=AtlasDetector('ATLAS-P2-ITK-05') #ExtBrl32 -- ITK extended barrel @ 3.2                                                         
        #itkLoi=AtlasDetector('ATLAS-P2-ITK-06') #ExtBrl4 -- ITK extended barrel @ 4.0                                                          
        itkLoi=AtlasDetector('ATLAS-P2-ITK-07') #IExtBrl4 -- ITK extended barrel, inclined layout @ 4.0                                         
        #itkLoi=AtlasDetector('ATLAS-P2-ITK-08') #InclBrl4 -- ITK fully inclined layout
        sensor=getSensorInfo(itkLoi)

        self.eventCounter = 0 

        # Set up counter holders, 

        self.CounterHolders = { }

        # Pixel Barrel Occupancy.....
        #loop over the layers..

        nLayer = len(sensor.layer)
        for i in range(nLayer):
            print 'layer i is...', i

            nModule = sensor.layer[i].nmodule
            '''
            if doubleStripLength and i < 3:
                nModule /= 2
                area *= 2


            pixClus_x.push_back(pixelCluster.globalX())
            pixClus_y.push_back(pixelCluster.globalY())
            pixClus_z.push_back(pixelCluster.globalZ())
            pixClus_bec.push_back(pixelCluster.auxdata("bec"))
            pixClus_eta_module.push_back(pixelCluster.auxdata("eta_module"))
            pixClus_layer.push_back(pixelCluster.auxdata("layer"))
            pixClus_detElementId.push_back(pixelCluster.auxdata("detectorElementID"))

            pixClus_size.push_back(pixelCluster.rdoIdentifierList().size())


            '''

            nMax = nModule/2
            nMin = -nMax
            hasZero = (nModule%2!=0)
            d = { }
            # loop over the etaModuleIDs. note that in the xAOD they are are -ve -> +ve. 
            for m in range(nMin,nMax+1):
                #not certain on this if statement.. 
                if m!=0 or hasZero:
                    # i is the layer, m is etaModuleID. create tuple.
                    print ' i , m ' , i , m 

                    #use dict to associate layer, etaModuleID and counter together. 
                    self.CounterHolders[(i,m)] = Counters()
                    self.CounterHolders[(i,m)].tot_pix = sensor.sctlayer[i].perModuleEta
                    

        return

    def initialize(self):
        self.msg.info('==> initialize...')

        self.hsvc = PyAthena.py_svc('THistSvc/THistSvc')
        myHist = TH1D("myHist","myHist",10,0,10)
        anotherHist = TH1D("anotherHist","anotherHist",10,0,10)
        self.hsvc.regHist("/MYSTREAM/myHist",myHist)


        
        return StatusCode.Success

    def execute(self):
        # loop over all counters and increment event. used for averaging at the end. 
        for item in self.CounterHolders:
            self.CounterHolders[item].eventIncrement()

        self.eventCounter += 1
        print ' event number ! ' , self.eventCounter 

        # loop over all clusters that are in the barrel
        for sctCluster in self.evtStore["SCT_Clusters"]:

            # ensure its in the end caps. bec is equal to +/- 2. 
            if sctCluster.auxdata("bec") != 0:
                
                etaModuleID = sctCluster.auxdata("eta_module")
                layer = sctCluster.auxdata("layer")

                x = sctCluster.globalX()
                y = sctCluster.globalY()
                z = sctCluster.globalZ()
                s = sctCluster.rdoIdentifierList().size()

                print ' r and z of the sctCluster ! ' , math.sqrt(x*x + y*y), ' and.. ' , z  

                #print ' data, layer, etaModuleID, bec etc ' , layer, etaModuleID, sctCluster.auxdata("bec")
                # note that layer, labels the disk and etaModuleID labels the ring.
                if sctCluster.auxdata("bec") == 2:
                    setk = 0
                else:
                    setk = 1 
                

                self.CounterHolders[(layer,etaModuleID,setk)].add(x,y,z,s,sctCluster.auxdata("detectorElementID"))
                self.CounterHolders[(layer,etaModuleID,setk)].setbec(sctCluster.auxdata("bec"))
        
        return StatusCode.Success

    def finalize(self):
        self.msg.info('==> finalize...')
        #test vector..
        # here we make a vector which is of the form Occupancy [Disk][etaModuleID][Occupanc]]

        myTree = TTree("myTree","myTree")
        self.hsvc.regTree("/MYSTREAM/myTree",myTree)
        
        sct_disk_map2D = TH2D("sct_disk_map2d","occupancies in percent (200 pileup)",240,-3100.,3100.,160,0.,1100.)
        self.hsvc.regHist("/MYSTREAM/sct_disk_map2d",sct_disk_map2d)


        DiskInfo = ROOT.vector('std::vector<float>')()
        myTree.Branch("DiskInfo",DiskInfo)
        pileUpFactor = 200 

        # keys give the layer/'DISK' and etaModuleID/'ring' 
        for key, value in self.CounterHolders.items():

            #print ' testing key ' , key[2] 
            #print ' testing... avg z and r ' , value.z(), value.r()

            sct_disk_map2D.Fill(value.z(),value.r(),value.occupancy()*pileUpFactor*100)

            tester = ROOT.vector('float')()
            # get DiskID / BEC / etaID / r / occupancy
            tester.push_back(key[0])
            tester.push_back(value.bec)
            tester.push_back(key[1])
            tester.push_back(value.r())
            tester.push_back(value.occupancy()*pileUpFactor*100)
            DiskInfo.push_back(tester)

            if value.bec == 0:
                print ' warning , problem in code !!!!!! ' , value.bec

        

        


        '''

        Disk_0 = ROOT.vector('std::vector<float>')()
        Disk_1 = ROOT.vector('std::vector<float>')()
        Disk_2 = ROOT.vector('std::vector<float>')()
        Disk_3 = ROOT.vector('std::vector<float>')()
        Disk_4 = ROOT.vector('std::vector<float>')()
        Disk_5 = ROOT.vector('std::vector<float>')()
        
        myTree.Branch("Disk_0",Disk_0)
        myTree.Branch("Disk_1",Disk_1)
        myTree.Branch("Disk_2",Disk_2)
        myTree.Branch("Disk_3",Disk_3)
        myTree.Branch("Disk_4",Disk_4)
        myTree.Branch("Disk_5",Disk_5)

        pileUpFactor = 200 

        # keys give the layer and etaModuleID
        for key, value in self.CounterHolders.items():

            tester = ROOT.vector('float')()
            # get BEC / etaID / r / occupancy
            tester.push_back(value.bec)
            tester.push_back(key[1])
            tester.push_back(value.r())
            tester.push_back(value.occupancy()*pileUpFactor*100)

            if key[0] == 0:
                Disk_0.push_back(tester)

            if key[0] == 1:
                Disk_1.push_back(tester)
            
            if key[0] == 2:
                Disk_2.push_back(tester)
            
            if key[0] == 3:
                Disk_3.push_back(tester)
            
            if key[0] == 4:
                Disk_4.push_back(tester)

            if key[0] == 5:
                Disk_5.push_back(tester)

        '''

        myTree.Fill()

            

            




        return StatusCode.Success



