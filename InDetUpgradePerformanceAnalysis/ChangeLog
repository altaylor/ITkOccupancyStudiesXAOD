2016-04-07  Robert Harrington <roberth -at- cern.ch>
	* Added functionality to double the strip length for innermost layers
	* Added sensorinfo_fastGeo and geometry_fastGeo for XML-based
	geometries

2015-08-25  Nick Edwards <nedwards -at- cern.ch>
	* Check in additional occupancy classes for pixel endcap per-event occupancy
	studies:
		- src/PixelDiskPerEventOccupancy.cpp
		- InDetUpgradePerformanceAnalysis/PixelDiskPerEventOccupancy.h
		- Fixes to other occupancy classes and to scripts
	* Small changes to scoping scripts for final plots
	* Tagging InDetUpgradePerformanceAnalysis-00-03-00

2015-08-25  Nick Edwards <nedwards -at- cern.ch>
	* Fix up python/__init__.py to work in RootCore
	* Tagging InDetUpgradePerformanceAnalysis-00-02-02

2015-08-25  Nick Edwards <nedwards -at- cern.ch>
	* Fix up python/DerivedBranches.py and python/StandardObservableDefinitions.py 
	  to work together and make a reasonable number of hits plots
	* Update cmt/precompile.RootCore and cmt/Makefile.RootCore to work with
	latest RootCore
	* Tagging InDetUpgradePerformanceAnalysis-00-02-01

2015-08-25  Nick Edwards <nedwards -at- cern.ch>
	* Add scripts used for scoping analysis:
		- scripts/upg_scoping_*
		- python/ScopingDefinitions.py
		- python/ScopingPlotting.py
	* Modify python/__init__.py to allow running in root6 based athena
	* Tagging InDetUpgradePerformanceAnalysis-00-02-00 

2015-01-15  Nick Edwards <nedwards -at- cern.ch>
	* Fix missing dictionary for DerivedBranch in cmt compilation
	* Fix bug where resolution plots code crashes if no events pass cuts
	* Remove script/upg_idr_analyze.py as all functionality folded into
	  script/upg_analyze.py and there is no need for two scripts.
	* Improved README with example instructions
	* Add file_lists for LoI samples on EOS used in the IDR in share/
	* Add 2D occupancy plots script
		  - scripts/upg_make_occ_plots_2d.py
	* Fix python/PlotMaker.py to work with the occupancy plots again
	* Tagging InDetUpgradePerformanceAnalysis-00-01-04

2013-10-14  Nick Edwards <nedwards -at- cern.ch>
	* Fix missing dictionary for DerivedBranch in cmt compilation
	* Fix bug where resolution plots code crashes if no events pass cuts

2013-05-29  Nick Edwards <nedwards -at- cern.ch>
	* Merge some changes from Nick Styles's version of the code:
	    - Allow option to divide by pT in ResolutionObservables
	    - Add genTracks plot in Efficieny.cpp
	    - Set X errors to be bin width in Efficiency.cpp
	* Restructure constructors of ResolutionObservable to simplify (UI
	  via derived classes has *not* changed).
	* Add optional third option to upg_perf_analyze.py: maxevents

2012-02-26  Chiara Debenedetti <chiara.debenedetti -at- cern.ch>
	* added preliminary jet-track matching as a cut class

2012-02-22  Chiara Debenedetti <chiara.debenedetti -at- cern.ch>
	* added script from Nick to allow to switch to JetTagD3PDs
	
2012-01-05  Nick Styles <nicholas.styles -at- desy.de>

	* added Overloaded contructor methods to allow different d3pd branch names (ie for btagging d3pds)

2011-10-27  Andreas Schaelicke <andreas.schaelicke -at- cern.ch>

	* added track files resolution plots package in src and share
