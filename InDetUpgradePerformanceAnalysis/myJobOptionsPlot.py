# import some useful constants for message level
import AthenaCommon.Constants as Lvl

# for job configuration: the application manager object
# as the name implies, there is only one application manager
from AthenaCommon.AppMgr import theApp

# for job configuration: the object to hold all services
# note: the real object is 'ServiceMgr' but we give it a shorter alias 'svcMgr'
from AthenaCommon.AppMgr import ServiceMgr as svcMgr

import AthenaPoolCnvSvc.ReadAthenaPool                   #sets up reading of POOL files (e.g. xAODs)

# using this we can run over all xAOD in a directory !!

#from glob import glob
#svcMgr.EventSelector.InputCollections = glob("/afs/cern.ch/work/a/altaylor/ITK_DATA/InclinedBarrel/*.root")
#svcMgr.EventSelector.InputCollections=["/afs/cern.ch/work/a/altaylor/ITK_DATA/InclinedBarrel/tester.root"]   #insert your list of input files here

from glob import glob

#svcMgr.EventSelector.InputCollections = glob("/afs/cern.ch/work/a/altaylor/ITK_DATA/InclinedBarrel/mc15_14TeV.119995.Pythia8_A2MSTW2008LO_minbias_inelastic_low.recon.DAOD_IDTRKVALID.e1133_s2846_s2850_r7878/*.root")

svcMgr.EventSelector.InputCollections = glob("/afs/cern.ch/work/a/altaylor/ITK_DATA/InclBrl4/NoThin/*.root")   


# for job configuration: the object to hold the sequence of algorithms to
# be run in turn
from AthenaCommon.AlgSequence import AlgSequence
job = AlgSequence()

## schedule for 10 events
theApp.EvtMax = 50

svcMgr += CfgMgr.THistSvc()
svcMgr.THistSvc.Output += ["MYSTREAM DATAFILE='Disk.root' OPT='RECREATE'"]

#algseq = CfgMgr.AthSequencer("AthAlgSeq") 

## add our algorithm

'''

from InDetUpgradePerformanceAnalysis.MyPyAlg import MyPyAlg
job += MyPyAlg()

'''


from InDetUpgradePerformanceAnalysis.MyPyAlgV2 import MyPyAlgV2
job += MyPyAlgV2()

      


