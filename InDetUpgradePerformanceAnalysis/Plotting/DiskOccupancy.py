from ROOT import *
ROOT.gROOT.LoadMacro("AtlasStyle.C")
ROOT.gROOT.LoadMacro("AtlasLabels.C")
ROOT.gROOT.LoadMacro("AtlasUtils.C")

SetAtlasStyle()

#ROOT.gStyle.SetOptStat(True) #turn off stats boxes
ROOT.gStyle.SetMarkerSize(1.5)
ROOT.gStyle.SetMarkerStyle(20)
ROOT.gStyle.SetFillColor(0)
ROOT.gStyle.SetPalette(1)

from array import array
import numpy as np
import scipy
import sys
#import SetPalette


#p = SetPalette.SetPalette()
#p.set_palette(ncontours=20)


c1 = TCanvas()
inputFile = "/afs/cern.ch/user/a/altaylor/UpgradeITK_V2/InnerDetector/InDetPerformance/InDetUpgradePerformanceAnalysis/Disk.root"
f = TFile(inputFile,"READ")
myTree = f.Get("myTree")

entries = myTree.GetEntries()

outputhists=[]

# Plot Occ vs z/etaModule ID here.


for event in myTree:

    tester = event.DiskInfo
    tester2 = np.array(tester)

DiskBEC = { }
# tester2 is a vector of the form get diskID /  BEC / etaID / r / occupancy
#print ' testing , Disk_0 ' , tester2

#tgraphsArray = [ ] 

for i in xrange(6):
    for j in xrange(2):

        DiskBEC[(i,j)] = [ ] 
        DiskBEC[(i,j)].append([ ] )
        DiskBEC[(i,j)].append([ ] )
        
for x in tester2:

    # here we need to shift the x[1] variable from -2 and 2 to 0 and 1.
    if x[1] == 2:
        j = 0
    else:
        j = 1
    
    DiskBEC[( int(x[0]), j)  ][0].append(x[4])
    DiskBEC[( int(x[0]), j )][1].append(x[3])

print ' testing occupancies  ', DiskBEC[(0,0)][0]
print ' testing r ' , DiskBEC[(0,0)][1]

print 'testing sort... ', np.sort( DiskBEC[(0,0)][1] ) 
print ' testing post sort ' , DiskBEC[(0,0)][1]

print ' testing length of array ' , len( DiskBEC[(0,0)][0] ) 

# the job now is basically creating 12 TGraphs..

MultiG = TMultiGraph()
MultiG2 = TMultiGraph()

leg = TLegend(0.60,0.6,0.90,0.90)
leg2 = TLegend(0.60,0.6,0.90,0.90)

for i in xrange(6):
    for j in xrange(2):

        aString = 'Disk ' + str(i)
        print 'testing string ' ,  aString 

        if j == 0:

            aGraph = TGraph(len(DiskBEC[(i,j)][0]),np.sort( DiskBEC[(i,j)][1] ), np.array(  DiskBEC[(i,j)][0] ) )
            aGraph.SetMarkerColor(i+1)
            aGraph.SetLineColor(i+1)
            aGraph.SetMarkerStyle(20)
            aGraph.SetLineStyle(3)
            bString = '(L)' 
            MultiG.Add(aGraph)
            leg.AddEntry(aGraph, aString+bString, "p")

        else:

            aGraph2 = TGraph(len(DiskBEC[(i,j)][0]),np.sort( DiskBEC[(i,j)][1] ), np.array(  DiskBEC[(i,j)][0] ) )
            aGraph2.SetMarkerColor(i+1)
            aGraph2.SetLineColor(i+1)
            aGraph2.SetMarkerStyle(21)
            aGraph2.SetLineStyle(9)
            bString = '(R)'
            MultiG2.Add(aGraph2)
            leg2.AddEntry(aGraph2, aString+bString, "p")
            

MultiG.Draw("apl")
leg.SetFillColor(0)
leg.SetTextSize(0.04)
leg.SetBorderSize(0)
leg.Draw()

#mg5->GetYaxis()->SetRangeUser(35.,80.);
MultiG.GetYaxis().SetRangeUser(0.0,0.4)

MultiG.GetXaxis().SetTitle(" r [mm]  ")
MultiG.GetYaxis().SetTitle("Channel Occupancy in % ")

c1.Update()
c1.SaveAs("occD1.pdf")


MultiG2.Draw("apl")
leg2.SetFillColor(0)
leg2.SetTextSize(0.04)
leg2.SetBorderSize(0)
leg2.Draw()

#mg5->GetYaxis()->SetRangeUser(35.,80.);                                                                                                                                               
MultiG2.GetYaxis().SetRangeUser(0.0,0.4)

MultiG2.GetXaxis().SetTitle(" r [mm]  ")
MultiG2.GetYaxis().SetTitle("Channel Occupancy in % ")


c1.Update()
c1.SaveAs("occD2.pdf")


# 2D plot !!!!


plot = TH2D()
f.GetObject("sct_disk_map2d", plot);
plot.SetDirectory(0)

c1.SetRightMargin(0.18)
c1.SetLeftMargin(0.15)

plot.GetYaxis().SetRangeUser(350, 1100)
plot.GetXaxis().SetRangeUser(-3100, 3100)
plot.Draw("COLZ")


y_label = 0.88
#AtlasUtil.DrawText(0.18,y_label, "ITk LoI Layout, #mu=200");
#AtlasUtil.AtlasLabel(0.52,y_label, simulation=True)

t = TLatex()
t.SetTextAngle(90)
t.SetNDC()
t.DrawLatex(0.97, 0.4, "Channel Occupancy %")

plot.SetXTitle("z [mm] ")
plot.SetYTitle("r [mm] ")


c1.Update()
c1.SaveAs("occ2DDisk.pdf")


# adding 2D plots togther..

inputFile2 = "/afs/cern.ch/user/a/altaylor/UpgradeITK_V2/InnerDetector/InDetPerformance/InDetUpgradePerformanceAnalysis/Barrel.root"
f2 = TFile(inputFile2,"READ")

plot2 = TH2D()
f2.GetObject("sct_occ_map2d", plot2);
plot2.SetDirectory(0)

plot2.GetYaxis().SetRangeUser(350, 1100)
plot2.GetXaxis().SetRangeUser(-1500, 1500)
plot2.Draw("COLZ")


y_label = 0.88
#AtlasUtil.DrawText(0.18,y_label, "ITk LoI Layout, #mu=200");
#AtlasUtil.AtlasLabel(0.52,y_label, simulation=True)
t.DrawLatex(0.97, 0.4, "Channel Occupancy %")

plot2.SetXTitle("z [mm] ")
plot2.SetYTitle("r [mm] ")


c1.Update()
c1.SaveAs("occBarrel2D.pdf")

'''
plot3 = TH2D()
plot3 = plot.Add(plot2,1.0)
plot3.Draw("COLZ")

'''

plot.Add(plot2)
plot.Draw("COLZ")


c1.Update()
c1.SaveAs("occTotal.pdf")


