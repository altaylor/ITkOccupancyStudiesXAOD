from ROOT import *
ROOT.gROOT.LoadMacro("AtlasStyle.C")
ROOT.gROOT.LoadMacro("AtlasLabels.C")
ROOT.gROOT.LoadMacro("AtlasUtils.C")

SetAtlasStyle()

#ROOT.gStyle.SetOptStat(True) #turn off stats boxes
ROOT.gStyle.SetMarkerSize(1.5)
ROOT.gStyle.SetMarkerStyle(20)
ROOT.gStyle.SetFillColor(0)
ROOT.gStyle.SetPalette(1)

from array import array
import numpy as np
import scipy
import sys
#import SetPalette


#p = SetPalette.SetPalette()
#p.set_palette(ncontours=20)


c1 = TCanvas()
inputFile = "/afs/cern.ch/user/a/altaylor/UpgradeITK_V2/InnerDetector/InDetPerformance/InDetUpgradePerformanceAnalysis/Barrel.root"
f = TFile(inputFile,"READ")
myTree = f.Get("myTree")

entries = myTree.GetEntries()

outputhists=[]
#ROOT.gStyle.SetOptStat(True) #turn off stats boxes

# SCT 2D Occupancy MAP here.


'''
plot = TH2D()
f.GetObject("sct_occ_map2d", plot);
plot.SetDirectory(0)

c1.SetRightMargin(0.18)
c1.SetLeftMargin(0.15)

plot.GetYaxis().SetRangeUser(350, 1100)
plot.GetXaxis().SetRangeUser(-1500, 1500)
plot.Draw("COLZ")


y_label = 0.88
#AtlasUtil.DrawText(0.18,y_label, "ITk LoI Layout, #mu=200");
#AtlasUtil.AtlasLabel(0.52,y_label, simulation=True)

t = TLatex()
t.SetTextAngle(90)
t.SetNDC()
t.DrawLatex(0.97, 0.4, "Channel Occupancy %")

plot.SetXTitle("z [mm] ")
plot.SetYTitle("r [mm] ")


c1.Update()
c1.SaveAs("occ2D.pdf")

'''





# Plot Occ vs z/etaModule ID here. 


for event in myTree:
    #print event.pixClus_x[2]
    Occupancy_0Array = event.Occupancy_0
    z_0Array = event.z_0
    etaModuleID0Array = event.etaModuleID0
    
    Occupancy_1Array = event.Occupancy_1
    z_1Array = event.z_1
    etaModuleID1Array = event.etaModuleID1
    print ' etaModuleID2 array size ', etaModuleID1Array.size()

    Occupancy_2Array = event.Occupancy_2
    z_2Array = event.z_2
    etaModuleID2Array = event.etaModuleID2
    print ' etaModuleID2 array size ', etaModuleID2Array.size()

    Occupancy_3Array = event.Occupancy_3
    z_3Array = event.z_3
    etaModuleID3Array = event.etaModuleID3

occz_0 = TGraph(Occupancy_0Array.size(),np.array(z_0Array),np.array(Occupancy_0Array))
occz_1 = TGraph(Occupancy_1Array.size(),np.array(z_1Array),np.array(Occupancy_1Array))
occz_2 = TGraph(Occupancy_2Array.size(),np.array(z_2Array),np.array(Occupancy_2Array))
occz_3 = TGraph(Occupancy_3Array.size(),np.array(z_3Array),np.array(Occupancy_3Array))


occz_0.GetYaxis().SetTitle(" Channel Occupancy in %")
occz_0.GetXaxis().SetTitle(" z (mm)  ")

occz_0.SetMarkerColor(1)
occz_1.SetMarkerColor(2)
occz_2.SetMarkerColor(3)
occz_3.SetMarkerColor(4)

MultiG = TMultiGraph()
MultiG.Add(occz_0)
MultiG.Add(occz_1)
MultiG.Add(occz_2)
MultiG.Add(occz_3)

#mg->GetXaxis()->SetTitle("E_{#gamma} (GeV)"); 
MultiG.Draw("AP")

MultiG.GetXaxis().SetTitle(" z [mm] ")
MultiG.GetYaxis().SetTitle("Channel Occupancy in % ")

#mg->GetXaxis()->SetTitle("aaa");

#set tlegend here..

leg = TLegend(0.70,0.7,0.90,0.85)
leg.AddEntry(occz_0, "Layer 0", "p")
leg.AddEntry(occz_1, "Layer 1", "p")
leg.AddEntry(occz_2, "Layer 2", "p")
leg.AddEntry(occz_3, "Layer 3", "p")
leg.SetFillColor(0)
leg.SetTextSize(0.04)
leg.SetBorderSize(0)
leg.Draw()

c1.Update()
c1.SaveAs("occz.pdf")

occID_0 = TGraph(Occupancy_0Array.size(),np.array(etaModuleID0Array),np.array(Occupancy_0Array))
occID_1 = TGraph(Occupancy_1Array.size(),np.array(etaModuleID1Array),np.array(Occupancy_1Array))
occID_2 = TGraph(Occupancy_2Array.size(),np.array(etaModuleID2Array),np.array(Occupancy_2Array))
occID_3 = TGraph(Occupancy_3Array.size(),np.array(etaModuleID3Array),np.array(Occupancy_3Array))

occID_0.SetMarkerColor(1)
occID_1.SetMarkerColor(2)
occID_2.SetMarkerColor(3)
occID_3.SetMarkerColor(4)


MultiG2 = TMultiGraph()
MultiG2.Add(occID_0)
MultiG2.Add(occID_1)
MultiG2.Add(occID_2)
MultiG2.Add(occID_3)


MultiG2.Draw("AP")
MultiG2.GetXaxis().SetTitle("etaModuleID ")
MultiG2.GetYaxis().SetTitle("Channel Occupancy in % ")


leg = TLegend(0.70,0.7,0.90,0.85)
leg.AddEntry(occID_0, "Layer 0", "p")
leg.AddEntry(occID_1, "Layer 1", "p")
leg.AddEntry(occID_2, "Layer 2", "p")
leg.AddEntry(occID_3, "Layer 3", "p")
leg.SetFillColor(0)
leg.SetTextSize(0.04)
leg.SetBorderSize(0)
leg.Draw()



c1.SaveAs("occID.pdf")
