#ifndef SCALARBINNINGVARIABLE_H
#define SCALARBINNINGVARIABLE_H

#include "InDetUpgradePerformanceAnalysis/ScalarBranch.h"
#include "InDetUpgradePerformanceAnalysis/BinningVariable.h"

template <class T> 
class ScalarBinningVariable : public BinningVariable {

    public:
        ScalarBinningVariable(std::string branch_name);
        float Value(int );

    private:
        ScalarBranch<T> m_var;

};

#endif
