#ifndef DerivedBranch_H
#define DerivedBranch_H


#include "InDetUpgradePerformanceAnalysis/Observable.h"
#include "InDetUpgradePerformanceAnalysis/VectorBranch.h"

#include <TTreeFormula.h>
#include <TFile.h>

template < class observableT >
class DerivedBranch : public Observable {

  public:
    DerivedBranch(const std::string & new_branch_name, const std::string & formula);
    DerivedBranch(const DerivedBranch<observableT>& );
    int GetNTrks();
    void Initialize();
    //void AnalyzeEvent(UpgPerfAna::AnalysisStep); 
    void StorePlots(); 
    void Analyze(UpgPerfAna::AnalysisStep, int);
    void InitializeEvent(UpgPerfAna::AnalysisStep step=UpgPerfAna::INITIAL);

  private:
    void Notify();
    std::vector<observableT> m_outVec;
    //std::string m_formula_str;
    TTreeFormula m_formula;
    VectorBranch<observableT> m_out_branch;
    int m_tree_num;
};

#endif
