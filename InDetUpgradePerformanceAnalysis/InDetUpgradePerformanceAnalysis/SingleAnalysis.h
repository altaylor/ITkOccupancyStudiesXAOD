#ifndef SingleAnalysis_H
#define SingleAnalysis_H

#include "ObservableGroup.h"
#include "TString.h"

#include <set>

class TreeHouse;
class TChain;

class SingleAnalysis : public ObservableGroup {

 public:
  SingleAnalysis(const std::string & name, TreeHouse *);

  void Initialize();

   /// Sould perform loop over analysis steps and sub-analyses
  UpgPerfAna::AnalysisStep Execute(); 

 /// Should perform loop over events (i.e. tree)
  UpgPerfAna::AnalysisStep ExecuteStep(UpgPerfAna::AnalysisStep);

  void SetTreeHouse(TreeHouse *th) { m_treeHouse=th;}
  TreeHouse * GetTreeHouse() { return m_treeHouse; }
  void SetChain(TChain *c);
  void SetChain(const TString &);
  TChain * GetChain();
  void SetMaxEvents(int n);
  void AddFile(const TString &);


  void AddStep(UpgPerfAna::AnalysisStep step) { m_steps.insert(step); }
protected:
  TreeHouse * m_treeHouse;
  typedef std::set<UpgPerfAna::AnalysisStep> StepList;
  StepList m_steps;
};



#endif // SingleAnalysis_H
