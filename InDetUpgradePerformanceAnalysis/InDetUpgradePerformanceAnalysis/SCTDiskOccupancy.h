#ifndef SCTDiskOccupancy_h
#define SCTDiskOccupancy_h

#include "DiskOccupancy.h"

#include "TBranch.h"
#include "TTree.h"
#include "TChain.h"

class SCTDiskOccupancy : public DiskOccupancy {
public:

  SCTDiskOccupancy();
  void Initialize();
  void Analyze(UpgPerfAna::AnalysisStep, int);
  //  void Finalize();
  int GetNTrks();  ///< loop SCT clusters

 private:
 
  // int ndisk;
  // Branch<int> sctClus_n; // int

  VectorBranch<float> sctClus_x, sctClus_y, sctClus_z; // vector<float>
  //VectorBranch<float> sctClus_size;  // Utopia 1.0
  VectorBranch<int> sctClus_size;  // Cartigny
  VectorBranch<int> sctClus_layer, sctClus_bec, sctClus_eta_module; 
  VectorBranch<ULong64_t> sctClus_detElementId;
};

// Local Variables:
// mode: c++
// End:
#endif
