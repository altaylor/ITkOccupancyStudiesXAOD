#ifndef SelectorWithIndex_H
#define SelectorWithIndex_H

#include "Cuts.h"
#include "VectorBranch.h"

class SelectorWithIndex : public Cuts {
public:
  SelectorWithIndex(const std::string & varname, const std::string & indexname, int value);
  bool AcceptTrack(int);
private:
  VectorBranch<int> var;
  VectorBranch<int> index;
  int sel;
};

class AbsSelectorWithIndex : public Cuts {
public:
  AbsSelectorWithIndex(const std::string & varname, const std::string & indexname, int value);
  bool AcceptTrack(int);
private:
  VectorBranch<int> var;
  VectorBranch<int> index;
  int sel;
};

#endif // SelectorWithIndex_H
/*
 Local Variables:
 mode:c++
 End:
*/
