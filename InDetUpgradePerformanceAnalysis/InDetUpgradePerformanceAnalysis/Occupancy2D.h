#ifndef Occupancy2D_H
#define Occupancy2D_H

#include "InDetUpgradePerformanceAnalysis/OccupancyObservable.h"
#include "InDetUpgradePerformanceAnalysis/VectorBranch.h"
#include "TROOT.h"

class Occupancy2D : public OccupancyObservable {

 public:
  Occupancy2D( double pileup=200 );
  void Initialize();
  void Analyze(UpgPerfAna::AnalysisStep, int);
  void Finalize();
  int GetNTrks();
private:
  //int trk_n;
  double m_pileup;
  VectorBranch<int> trk_SCT_hit_n;
  VectorBranch<std::vector<float> > trk_SCT_hit_x,trk_SCT_hit_y,trk_SCT_hit_z;
  VectorBranch<std::vector<ULong64_t> > trk_SCT_hit_detElementId;

  VectorBranch<int> trk_Pixel_hit_n;
  VectorBranch<std::vector<float> > trk_Pixel_hit_x,trk_Pixel_hit_y,trk_Pixel_hit_z;
  VectorBranch<std::vector<ULong64_t> > trk_Pixel_hit_detElementId;

  typedef std::map<ULong64_t,Counters> OccupanciesMap;
  OccupanciesMap omap;
};

// Local Variables:
// mode: c++
// End:
#endif
