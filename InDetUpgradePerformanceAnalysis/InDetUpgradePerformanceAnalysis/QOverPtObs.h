#ifndef QOverPtObs_H
#define QOverPtObs_H

#include "ResolutionObservable.h"
#include "VectorBranch.h"

template < class observableT >
class QOverPtObs: public ResolutionObservable< observableT > {
public:
  QOverPtObs(const std::string & varname, const std::string & mctruth, const std::string & theta, Binning binning=Binning(), std::string indexstring = "trk_mc_index", double pt=0.);
  float EvaluateVariable(int i, const std::string & varname);

private:

  VectorBranch<float> eta_var;
};


#endif 
