#ifndef Cuts_H
#define Cuts_H

#include <string>
#include <iostream>

class InDetUpgradeAnalysis;


class Cuts{

 public:
  Cuts(const std::string &);
  virtual bool AcceptEvent();
  virtual bool AcceptTrack(int)=0;
  //virtual int  AcceptAndMatchTrack(int)=0;
  virtual ~Cuts();
  const std::string & Name() const { return m_name; }

  inline bool Count(bool);
 private:
  std::string m_name;
  long m_ntotal;
  long m_naccept;

  friend std::ostream & operator<<(std::ostream & s, const Cuts*);
};

bool Cuts::Count(bool sel) 
{
  ++m_ntotal;
  if (sel) ++m_naccept;
  return sel;
}

#endif
/*
 Local Variables:
 mode:c++
 End:
*/
