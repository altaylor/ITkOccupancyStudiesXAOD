#ifndef ResolutionObservable_H
#define ResolutionObservable_H

#include <vector>
#include <string>
#include <TH1.h>
#include <TGraphErrors.h>
#include <TF1.h>
#include <TFitResult.h>

#include "Observable.h"
#include "VectorBranch.h"
#include "Binning.h"

struct ResolutionResult {
    double res;
    double reserr;
    double chi2;
    double bias;
    double biaserr;
};

float wrap_pi(float value);


template < class observableT >
class ResolutionObservable : public Observable {

 public:

     /** Full constructor
     * @param name1 -- name of reco variable
     * @param name2 -- e.g. name of MC variable for resolutions
     * @param indexstring -- name of variable linking reco to mc
     * @param etastring -- name for binning variable (default eta)
     * @param name3 -- additional branch, used in QOverPtObs for truth phi etc
     * @param indexstring_2 -- name of second variable linking reco to mc e.g. track->mcpart->truthtrack
     * @param pt -- divide resolution by this amount (e.g for sigma(p)/p plots). 0 by default
     */
  ResolutionObservable(const std::string & name1, double pt=0, const std::string & name2="", std::string indexstring="trk_mc_index", Binning binning = Binning(), const std::string & name3="", std::string indexstring_2="");

  void StorePlots();
  void Initialize();
  void Analyze(UpgPerfAna::AnalysisStep, int);
  int GetNTrks();
  void Finalize();
  void DoFit(bool doFit=true) { m_doFit = doFit; };
  void DoubleGaussianFit() { m_doFit = true; m_doDoubleGaussian=true; };

  /// *AS* old interface:
  void DefinePlots();
  void DefineFinalPlots();
  void FillPlots(UpgPerfAna::AnalysisStep, int); 

  Binning* GetBinning() { return &m_binning; };
  void SetMaxRangeFinal(float range) { m_maxRangeFinal = range; };

 protected:
  //inline int EvaluateEtaBin(int);
  virtual float EvaluateVariable(int, const std::string &)=0;
  ResolutionResult GetResolution(TH1* hist);
  //double GetEta(int);

  Binning m_binning;

  float minh, maxh;
  int nbinshisto;
  //int nEtaBins;
  //float etaMax, etaMin;
  std::string varname;
  double m_pt;
  bool m_doFit;
  double m_maxRangeFinal;
  bool m_doDoubleGaussian;

  std::vector<TH1*> initialPlots;
  std::vector<TH1*> controlPlots;
  std::vector<TH1*> outlierPlots;
  std::vector<double> outlier_width;
  std::vector<double> outlier_mean;
  std::vector<double> sumw;
  std::vector<double> sumwx;
  std::vector<double> sumwx2;

  TGraphErrors *resPlot;
  TGraphErrors *biasPlot;
  TGraphErrors *biasPlot_init;
  TGraphErrors *fracPlot;
  TGraphErrors *tailPlot;
  TGraphErrors *sumPlot;

  // used branches
  //VectorBranch<float> m_binning_var;
  VectorBranch<int> mc_index;
  VectorBranch<int> mc_index_2;
  VectorBranch<observableT> rec_var;
  VectorBranch<observableT> MC_var;
  VectorBranch<observableT> add_var;
};

/*
int ResolutionObservable::EvaluateEtaBin(int iter)
{
  float Eta = eta[iter];
  if (etaMin==0. && Eta<0.) Eta= -Eta;
  if (Eta<=etaMin || Eta>=etaMax) 
     return -1;
  int theBin = int(((Eta-etaMin)*nEtaBins)/(etaMax-etaMin));

  return theBin;
}
*/

#endif
