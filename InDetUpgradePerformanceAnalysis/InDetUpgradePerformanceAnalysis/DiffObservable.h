#ifndef DiffObservable_H
#define DiffObservable_H

#include "ResolutionObservable.h"

template < class observableT >
class DiffObservable : public ResolutionObservable< observableT> {

 public:
  DiffObservable(const std::string & varname, const std::string & mctruth, Binning binning=Binning(), bool fractional=false, std::string indexstring="trk_mc_index") ;
  //DiffObservable(const std::string & varname, const std::string & mctruth, Binning binning=Binning(), std::string indexstring="trk_mc_index", double pt=0) ;
  float EvaluateVariable(int i, const std::string & varname);

 private:
  bool m_doubleIndexed;
  bool m_fractional;
};


#endif 
