#ifndef ObservableGroup_H
#define ObservableGroup_H

#include "Observable.h"
#include <vector>
#include <set>

class Cuts;

class ObservableGroup : public Observable {

 public:
  ObservableGroup(const std::string & name);

  /// might create plots via "DefinePlots"
  void Initialize();
  void AddSensor(SensorInfoBase*);
  void SetAnalysis(SingleAnalysis * ana);
  Observable * GetObservable(const std::string & name);


  /// Should perform loop over e.g. tracks (and check cuts)
  void AnalyzeEvent(UpgPerfAna::AnalysisStep step=UpgPerfAna::INITIAL);

  /// Should perform loop over e.g. tracks (and check cuts)
  void Analyze(UpgPerfAna::AnalysisStep , int);

  /// Might create plots via "FillProfiles"
  void Finalize();

  /// Should store results in a root file
  void StorePlots(); 

  virtual void Add(Observable*);

  virtual void SetCuts(Cuts*);

  void SetRunLoop(bool run_loop) { m_run_loop = run_loop; };

  void AddStepVeto( UpgPerfAna::AnalysisStep step ) { m_vetoed_steps.insert(step); };

  ~ObservableGroup();

protected:
  int GetNTrks();

  std::vector<Observable*> m_obs;
  Cuts * m_cuts;
  bool m_run_loop;
  std::set<UpgPerfAna::AnalysisStep> m_vetoed_steps;

  friend std::ostream & operator<<(std::ostream & s, const Observable*);

};

#endif // ObservableGroup_H
/*
 Local Variables:
 mode:c++
 End:
*/
