#ifndef InDetUpgradePerformanceAnalysis_InDetUpgradePerformanceAnalysisDict_H
#define InDetUpgradePerformanceAnalysis_InDetUpgradePerformanceAnalysisDict_H 1
	
#include "InDetUpgradePerformanceAnalysis/InDetUpgradeAnalysis.h"
#include "InDetUpgradePerformanceAnalysis/DumpSensorInfo.h"
#include "InDetUpgradePerformanceAnalysis/SensorInfoBase.h"

#include "InDetUpgradePerformanceAnalysis/SingleObservable.h"
#include "InDetUpgradePerformanceAnalysis/DiffObservable.h"
#include "InDetUpgradePerformanceAnalysis/PullObservable.h"
#include "InDetUpgradePerformanceAnalysis/StandardCuts.h"
#include "InDetUpgradePerformanceAnalysis/InDetUpgradeAnalysis.h"
#include "InDetUpgradePerformanceAnalysis/Z0SinThetaObs.h"
#include "InDetUpgradePerformanceAnalysis/QOverPtObs.h"

#include "InDetUpgradePerformanceAnalysis/TreeHouse.h"
#include "InDetUpgradePerformanceAnalysis/AllCuts.h"
#include "InDetUpgradePerformanceAnalysis/MinMaxCut.h"
#include "InDetUpgradePerformanceAnalysis/MinMaxCutWithIndex.h"
#include "InDetUpgradePerformanceAnalysis/MinimumHitsCut.h"

#include "InDetUpgradePerformanceAnalysis/Selector.h"
#include "InDetUpgradePerformanceAnalysis/SelectorWithIndex.h"
#include "InDetUpgradePerformanceAnalysis/Efficiency.h"
#include "InDetUpgradePerformanceAnalysis/FakeRate.h"
#include "InDetUpgradePerformanceAnalysis/TrackRatio.h"

#include "InDetUpgradePerformanceAnalysis/SCTDiskOccupancy.h"
#include "InDetUpgradePerformanceAnalysis/SCTBarrelOccupancy.h"
#include "InDetUpgradePerformanceAnalysis/SCTBarrelPerEventOccupancy.h"
#include "InDetUpgradePerformanceAnalysis/PixelBarrelPerEventOccupancy.h"
#include "InDetUpgradePerformanceAnalysis/PixelDiskPerEventOccupancy.h"
#include "InDetUpgradePerformanceAnalysis/PixelDiskOccupancy.h"
#include "InDetUpgradePerformanceAnalysis/PixelBarrelOccupancy.h"
#include "InDetUpgradePerformanceAnalysis/Occupancy2D.h"
#include "InDetUpgradePerformanceAnalysis/PixelOccupancy2D.h"
#include "InDetUpgradePerformanceAnalysis/Distribution.h"
#include "InDetUpgradePerformanceAnalysis/Binning.h"

#include "InDetUpgradePerformanceAnalysis/Calculator.h"
#include "InDetUpgradePerformanceAnalysis/Addition.h"
#include "InDetUpgradePerformanceAnalysis/DerivedBranch.h"


#ifdef __GCCXML__
// GCCXML explicit template instantiation block
template class RangeDependentMinCut<float,float>;
template class RangeDependentMinCut<int,float>;
template class Selector<int>;
template class Selector<float>;
template class AbsSelector<int>;
template class AbsSelector<float>;
template class MinCut<int>;
template class MinCut<float>;
template class MaxCut<float>;
template class MaxCut<int>;
template class RangeCut<int> ;
template class RangeCut<float> ;
template class RangeProductCut<float> ;
template class Distribution<float> ;
template class Distribution<int> ;
template class ResolutionObservable<float>;
template class ResolutionObservable<int>;
template class DiffObservable<float>;
template class PullObservable<float>;
template class Z0SinThetaObs<float>;
template class QOverPtObs<float>;
template class SingleObservable<float>;
template class SingleObservable<int>;

template class Calculator<int>;
template class Calculator<float>;
template class Addition<int>;
template class Addition<float>;
//template class DerivedBranch<double>;
template class DerivedBranch<float>;

#endif

#endif //> not InDetUpgradePerformanceAnalysis_InDetUpgradePerformanceAnalysisDict_H
