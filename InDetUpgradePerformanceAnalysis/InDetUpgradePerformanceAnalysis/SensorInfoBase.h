#ifndef SensorInfoBase_H
#define SensorInfoBase_H

#include <string>
#include <vector>
#include <map>


class SensorInfoBase {
  typedef std::vector<SensorInfoBase*> SensorArray;
  typedef std::map<std::string, SensorArray > SensorStore;
  typedef std::map<std::string, int > SensorParameters;
  typedef std::map<std::string, float > SensorFloats;
  SensorStore sensors;
  SensorParameters param;
  SensorFloats dim;
public:
  SensorInfoBase();
  void add(const std::string &,int);
  void addFloat(const std::string &,float);
  void addSensor(const std::string &,SensorInfoBase*,int id=0);

  int getValue(const std::string & key) const;
  float getFloat(const std::string & key) const;

  const SensorInfoBase* get(const std::string & key, int i=0) const {
    SensorStore::const_iterator it=sensors.find(key);
    if (it!=sensors.end()) {
      return (*it).second[i];
    }
    return 0;
  }

  friend class DumpSensorInfo;
};

// Local Variables:
// mode: c++
// End:
#endif
