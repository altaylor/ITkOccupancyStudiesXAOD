#ifndef Binning_H
#define Binning_H

#include "InDetUpgradePerformanceAnalysis/BinningVariable.h"
#include <string>
#include <vector>

/** Tool to manage bins for a binned measurement (e.g Efficiency, Resolution) */
class Binning{

    public:

        Binning(std::string varname, int nBins, float binMin, float binMax, std::string name="", std::string label = "", bool isAbs = false);
        Binning(std::string varname, std::vector<float> bins, std::string name="", std::string label = "", bool isAbs = false);
        Binning();
        Binning(const Binning &other);
        //Binning(Binning other);
        virtual ~Binning();

        /** Set the binning */
        void SetBinning(int nBins, float binMin, float binMax, bool isAbs=true, const std::vector<float> *bins=0);
        /** Set absolute binning - i.e. to only bin "single sided" in eta */
        void SetAbsBinning(bool isAbs=true) { m_isAbs = isAbs; };

        /** Return binning name */
        std::string name() { return m_name; };
        /** Return binning label */
        std::string label() { return m_label; };
        /** Return variable name */
        std::string varname() { return m_varname; };

        /** Get the bin index for object at d3pd index 'index'  */
        virtual int GetBinIndex(int d3pd_index);

        /** Get the bin index by value */
        virtual int GetBinIndexValue(float value);

        /** Get the central value of the bin with index 'index'  */
        float GetBinCenter(int index);

        /** Returns number of bins */
        int GetNBins() { return m_nBins; };

        /** Returns bin width  */
        float GetBinWidth(int index);

        /** Returns mininimum */
        float GetMinimum() { return m_binMin; };

        /** Returns maximum  */
        float GetMaximum() { return m_binMax; };

    private:

        void InitialiseBinningVariable();

        std::string m_varname;
        std::string m_name;
        std::string m_label;
        BinningVariable *m_binning_variable;
        int m_nBins;
        float m_binWidth;
        float m_binMin;
        float m_binMax;
        std::vector<float> m_bins;
        bool m_isAbs;

};
#endif
