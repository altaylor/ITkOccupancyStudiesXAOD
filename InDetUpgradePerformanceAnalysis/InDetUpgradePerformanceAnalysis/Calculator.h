#ifndef Calculator_H
#define Calculator_H


#include "InDetUpgradePerformanceAnalysis/Observable.h"
#include "InDetUpgradePerformanceAnalysis/VectorBranch.h"

template < class observableT >
class Calculator : public Observable {

 public:
  Calculator(const std::string & op,const std::string & in,
	     const std::string & out,const std::string & in2 = "");


  int GetNTrks();
  void Initialize();
  //void AnalyzeEvent(UpgPerfAna::AnalysisStep); 
  void StorePlots(); 
  void Analyze(UpgPerfAna::AnalysisStep, int);

  //  int GetNTrks();
  //  void Finalize();

private:
  virtual void Calc(UpgPerfAna::AnalysisStep,int)=0;
  // virtual void Calc(UpgPerfAna::AnalysisStep);
protected:
  VectorBranch<observableT> in_var;
  std::vector<observableT> outVec;
  VectorBranch<observableT> out_var;
  VectorBranch<observableT> in2_var;
};

#endif
