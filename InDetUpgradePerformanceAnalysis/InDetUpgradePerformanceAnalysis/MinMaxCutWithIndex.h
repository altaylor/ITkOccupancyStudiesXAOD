#ifndef MinMaxCutWithIndex_H
#define MinMaxCutWithIndex_H

#include "Cuts.h"
#include "VectorBranch.h"

class MinCutWithIndex : public Cuts {
public:
  MinCutWithIndex(const std::string & varname, const std::string & indexname, float value);
  bool AcceptTrack(int);
private:
  VectorBranch<float> var;
  VectorBranch<int> index;
  float minvar;
};


class MaxCutWithIndex : public Cuts {
public:
  MaxCutWithIndex(const std::string & varname, const std::string & indexname, float value);
  bool AcceptTrack(int);
private:
  VectorBranch<float> var;
  VectorBranch<int> index;
  float maxvar;
};

class RangeCutWithIndex: public Cuts {
public:
  RangeCutWithIndex(const std::string & varname, const std::string & indexname, float vmin, float vmax);
  bool AcceptTrack(int);
private:
  VectorBranch<float> var;
  VectorBranch<int> index;
  float minvar;
  float maxvar;
};

#endif // MinMaxCutWithIndex_H
