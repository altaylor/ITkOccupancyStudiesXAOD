#ifndef StandardCuts_H
#define StandardCuts_H

#include "Cuts.h"
#include "VectorBranch.h"

class InDetUpgradeAnalysis;



class StandardCuts : public Cuts{

 public:
  StandardCuts();
  bool AcceptEvent();
  bool AcceptTrack(int);

 private:
  float ptcut;
  //float probcut;
  float d0cut;
  float z0cut;
  float etaTRTcut;
  int barcodeMax;
  int barcodeMin;
  int pixSCThits;
  int TRThitscut;
  float genPtMin;
  float genPtMax;
  int motherID;

  // references to branch variables
  VectorBranch<float> eta;
  VectorBranch<float> pt;
  VectorBranch<float> probability;
  VectorBranch<float> rec_d0;
  VectorBranch<float> rec_z0;
  VectorBranch<int> barcode;
  VectorBranch<int> nPixHits;
  VectorBranch<int> nSCTHits;
  VectorBranch<int> nTRTHits;
  VectorBranch<float> genPt;
  VectorBranch<int> motherType;
};

#endif
