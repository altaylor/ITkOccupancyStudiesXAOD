#ifndef TrackRatio_H
#define TrackRatio_H

#include <iostream>
#include <vector>
#include <cmath>
#include "TH1.h"
#include "TBranch.h"
#include "TTree.h"
#include "TChain.h"
#include "TFile.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TCanvas.h"
#include "VectorBranch.h"
#include "ScalarBranch.h"
#include "ObservableGroup.h"
#include "TrackJetMatchCut.h"
#include "InDetUpgradeAnalysis.h"
#include "Binning.h"

class Cuts;

/* Class to plot ratio of reconstructed tracks to generated particles 
 * User can specify what cuts to apply to generated particles, but charged particles
 * from the hard scatter would be a good definition (pileup particles generally not
 * available in the truth record).
 *
 * Binning most sensibly by event level quantities e.g mu, number of truth vertices
 */

class TrackRatio : public Observable {

 public:

  TrackRatio( Cuts* truth_cuts, Cuts* reco_cuts, Binning binning = Binning("mcVx_n",150,0,300,"numMcVx"));

  void Initialize();
  void AnalyzeEvent(UpgPerfAna::AnalysisStep);
  void Finalize();
  void StorePlots();
  Binning* GetBinning() { return &m_binning; };

  // Not used - implemented because I have to. Throw errors
  void Analyze(UpgPerfAna::AnalysisStep,int);
  virtual void InitializeEvent(UpgPerfAna::AnalysisStep);
  int GetNTrks();  ///< Note: returns mc size -- needs renaming in Observable.h

 private:

  void SelectTracks( std::vector<int> & selected_tracks );
  void FillCounters(  );
  void CountTruth(  const std::vector<int> & selected_tracks );
  TGraphErrors* MakeGraph( Binning &binning, std::vector<int> &num, std::string name, std::string ylabel );
  TGraphErrors* MakeRatioGraph( Binning &binning, std::vector<int> &num, std::vector<int> &denom, std::string name, std::string ylabel, RatioErrorType error =BINOMIAL);

  Binning m_binning;
  Binning m_track_binning;
  Binning m_truth_binning;

  TGraphErrors *effPlot;

  ScalarBranch<int> mc_n;
  ScalarBranch<int> trk_n;
  VectorBranch<int> trk_mc_index;
  VectorBranch<float> trk_mc_probability;

  Cuts *m_reco_cuts;
  Cuts *m_truth_cuts;

  // Per-event counters
  int ev_nTruth;
  int ev_nTruthMatched;
  int ev_nTrack;

  // Binned total counters
  std::vector<int> nTruth_puBins;
  std::vector<int> nTruthMatched_puBins;
  std::vector<int> nTrack_puBins;
  std::vector<int> nEv_puBins;

  std::vector<int> nTruth_trackBins;
  std::vector<int> nTruthMatched_trackBins;
  std::vector<int> nTrack_trackBins;
  std::vector<int> nEv_trackBins;

  std::vector<int> nTruth_truthBins;
  std::vector<int> nTruthMatched_truthBins;
  std::vector<int> nTrack_truthBins;
  std::vector<int> nEv_truthBins;
  
  // Output container
  std::vector<TGraphErrors*> m_graphs;

  };

#endif
