#ifndef SCTBarrelOccupancy_h
#define SCTBarrelOccupancy_h

#include "OccupancyObservable.h"

#include "TBranch.h"
#include "TTree.h"
#include "TChain.h"

class SCTBarrelOccupancy : public OccupancyObservable {
public:

  SCTBarrelOccupancy();
  void Initialize();
  void Analyze(UpgPerfAna::AnalysisStep, int);
  void Finalize();
  int GetNTrks();  ///< loop SCT clusters

  void setDoubleStripLength(bool val) { doubleStripLength=val; }
  
 private:
 
  int nlayer;
  // Branch<int> sctClus_n; // int

  VectorBranch<float> sctClus_x, sctClus_y, sctClus_z; // vector<float>
  //  VectorBranch<float> sctClus_size; // Utopia 1.0
  VectorBranch<int> sctClus_size;
  VectorBranch<int> sctClus_layer, sctClus_bec, sctClus_eta_module; 
  VectorBranch<ULong64_t> sctClus_detElementId;
  VectorBranch<float> mc_gen_type, mc_gen_eta, mc_gen_phi;
  VectorBranch<int> mc_gen_barcode, mc_charge;

  ClusterTruthType clusterTruthType(float x, float y, float z);

  bool doubleStripLength;
};

// Local Variables:
// mode: c++
// End:
#endif
