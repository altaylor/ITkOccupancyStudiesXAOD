#ifndef Z0SinThetaObs_H
#define Z0SinThetaObs_H

#include "ResolutionObservable.h"
#include "VectorBranch.h"
#include "Binning.h"

template < class observableT >
class Z0SinThetaObs: public ResolutionObservable< observableT > {
public:
  Z0SinThetaObs(const std::string & varname, const std::string & mctruth, const std::string & theta, Binning binning = Binning(), std::string indexstring = "trk_mc_index", double pt=0.);
  float EvaluateVariable(int i, const std::string & varname);

private:

  VectorBranch<float> eta_var;
};


#endif 
