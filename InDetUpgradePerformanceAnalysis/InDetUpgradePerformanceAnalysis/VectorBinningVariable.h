#ifndef VECTORBINNINGVARIABLE_H
#define VECTORBINNINGVARIABLE_H

#include "InDetUpgradePerformanceAnalysis/VectorBranch.h"
#include "InDetUpgradePerformanceAnalysis/BinningVariable.h"

template <class T>
class VectorBinningVariable : public BinningVariable {

    public:
        VectorBinningVariable(std::string branch_name);
        float Value(int index);

    private:
        VectorBranch<T> m_var;

};

#endif
