#ifndef SingleObservable_H
#define SingleObservable_H

#include "ResolutionObservable.h"

template < class observableT >
class SingleObservable: public ResolutionObservable < observableT >{
public:
  SingleObservable(const std::string & varname, Binning binning = Binning(), double pt=0.);
  float EvaluateVariable(int i, const std::string & );
};


#endif 
