
#ifndef Scalar_H
#define Scalar_H

#include <vector>
#include <string>

class TreeHouse;

template <class T>
class ScalarBranch {
 public:

  ScalarBranch(const std::string & name);
  ScalarBranch(const std::string & name, T & var);
  const T & Get();
  // mimic vector
  T operator()();
  void SetAnalysis(TreeHouse *); // needed ?
  inline const std::string & Name() { return name; }
  void Fill();  // only for calc records
 private:
  std::string name;
  size_t id;
  TreeHouse * m_treehouse;
};


#endif
