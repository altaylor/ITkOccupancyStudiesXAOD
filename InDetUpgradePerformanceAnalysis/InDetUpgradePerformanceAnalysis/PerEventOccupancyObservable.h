
#ifndef PerEventOccupancyObservable_h
#define PerEventOccupancyObservable_h

#include "TBranch.h"
#include "TTree.h"
#include "TChain.h"

#include "InDetUpgradePerformanceAnalysis/OccupancyObservable.h"

#include <vector>

class TString;

class PerEventOccupancyObservable : public Observable {

  protected:

    class PerEventCounter {
      float f_area;
      long sum_clussize;
      long tot_pix;
      public:
      std::vector<float> clus_locXs;
      // How many strips hit in this cluster
      std::vector<int> clus_sizes;
      // Total size of cluster
      std::vector<int> clus_full_sizes;
      // TODO tidy up
      double elem_start, elem_end;
      PerEventCounter(double elem_start, double elem_end);
      PerEventCounter();
      OccupancyObservable::ElementList detElementIds;
      void setTotalPixel(int total) { tot_pix=total; }
      int totalPixel() { return tot_pix; }
      void add(float locX, int clussize, ULong64_t detElementId, int clus_fullsize=-1) {
        sum_clussize+=clussize; 
        detElementIds.insert(detElementId);
        clus_sizes.push_back(clussize);
        if (clus_fullsize>0)
          clus_full_sizes.push_back(clus_fullsize);
        else
          clus_full_sizes.push_back(clussize);
        clus_locXs.push_back(locX);
      }
      float area() const { return f_area; }
      void setArea(float a) { f_area=a; }
      float occupancy() const;
      float occupancyError() const;
      float hits() const;
      float hitsError() const;
      float cluster() const;
      float clusterError() const;
      friend class Occupancy2D;
      friend class PixelOccupancy2D;
    };

  public:

    typedef unsigned long long ElementId;
    typedef std::map< ElementId, PerEventCounter > PerElementCounters;

    // Return ID uniue to every sensor OR chip (if last argument is used)
    // There will be degenerate IDs between barrel and disk, but this doesn't matter
    // as they are only used within the individual classes
    static ElementId getElementId(int layer, int etaModule, int phiModule, int side, int chip=0) {
      // etaModule goes from -53 to 53  (0 to 3 for disk)
      // phiModule goes from -27 to 27 (same for disk)
      // side is 0 or 1
      // layer goes from 0 to 5 (same for disk)
      // chip goes from 0 to 9 (not used for disk)
      // ToDo - make this a bit nicer
      return (side*1e5 +layer*1e6 + (etaModule+53) + (phiModule+27)*1e3 + chip*1e7);
    };

    PerEventOccupancyObservable(const std::string & name1, const std::string & mType );
    void Initialize();
    virtual void Analyze(UpgPerfAna::AnalysisStep, int)=0;
    void Finalize();
    void StorePlots();

    std::vector< PerElementCounters > sensor_counters;
    std::vector< PerElementCounters > chip_counters;

    enum PlotVariable {
      MAX_CLUS=0,
      MAX_HITS,    
      MAX_OCC,     
      MAX_FLU,     
      MAX_FLU_CLUS,
      MAX_CLUS_HITS,
      MAX_HITS_CLUS,

      MEAN_CLUS,
      MEAN_HITS,    
      MEAN_OCC,     
      MEAN_FLU,     
      MEAN_FLU_CLUS,
      MEAN_CLUS_HITS,
      MEAN_HITS_CLUS,

      NPC_CLUS,
      NPC_HITS,    
      NPC_OCC,     
      NPC_FLU,     
      NPC_FLU_CLUS,
      NPC_CLUS_HITS,
      NPC_HITS_CLUS,

      ALL_CLUS,
      ALL_HITS,    
      ALL_OCC,     
      ALL_FLU,     
      ALL_FLU_CLUS,
      ALL_CLUS_HITS,
      ALL_HITS_CLUS
    };

    enum ElemType {
      SENSOR=0,
      CHIP,
      CLUSTER,
      LAYER
    };

  protected:

    std::string moduleType;
    std::string layerstr;
    int n_regions;
    int nEvents;

    bool EventInit(UpgPerfAna::AnalysisStep step, int k);
    virtual void fillHists( std::vector< PerElementCounters > &counters,  ElemType elem)=0;
    void fillHist(long id, double var, double weight=1.0);
    virtual void defineHists()=0;
    void defineHist(long id, TString name, int nbins, float min, float max);
    void defineHistLayers(ElemType elem, PlotVariable var, TString name, int nbins, float min, float max);
    long getHistId(int layer, ElemType elem, PlotVariable var, int ring=0);
    double find90pc(TH1D* hist);
    PerElementCounters& getCounters( int region, ElemType elem = SENSOR );

    std::vector<int> nClus_layer;

    std::vector< std::vector< TH1* > > m_histos;
    std::vector< std::vector< TH1* > > m_histos_chip;


    std::vector<TH1*> extra_hists;
    std::map<long, TH1*> m_hmap;
};



// Local Variables:
// mode: c++
// End:
#endif
