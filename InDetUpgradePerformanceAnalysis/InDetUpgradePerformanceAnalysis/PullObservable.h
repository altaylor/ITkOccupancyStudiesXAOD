#ifndef PullObservable_H
#define PullObservable_H

#include "ResolutionObservable.h"
#include "VectorBranch.h"

template < class observableT >
class PullObservable : public ResolutionObservable< observableT> {

 public:
  PullObservable(const std::string & var_reco, const std::string & var_err_reco, const std::string & var_truth, Binning binning=Binning(), std::string indexstring="trk_mc_index") ;
  //PullObservable(const std::string & varname, const std::string & mctruth, Binning binning=Binning(), std::string indexstring="trk_mc_index", double pt=0) ;
  float EvaluateVariable(int i, const std::string & );

 private:
  bool m_doubleIndexed;
  bool m_fractional;
  VectorBranch<observableT> m_var_err_reco;
};


#endif 
