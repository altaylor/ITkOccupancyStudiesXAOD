#-- start of make_header -----------------

#====================================
#  Library InDetUpgradePerformanceAnalysisDict
#
#   Generated Tue May 10 13:57:19 2016  by altaylor
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_InDetUpgradePerformanceAnalysisDict_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_InDetUpgradePerformanceAnalysisDict_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_InDetUpgradePerformanceAnalysisDict

InDetUpgradePerformanceAnalysis_tag = $(tag)

#cmt_local_tagfile_InDetUpgradePerformanceAnalysisDict = $(InDetUpgradePerformanceAnalysis_tag)_InDetUpgradePerformanceAnalysisDict.make
cmt_local_tagfile_InDetUpgradePerformanceAnalysisDict = $(bin)$(InDetUpgradePerformanceAnalysis_tag)_InDetUpgradePerformanceAnalysisDict.make

else

tags      = $(tag),$(CMTEXTRATAGS)

InDetUpgradePerformanceAnalysis_tag = $(tag)

#cmt_local_tagfile_InDetUpgradePerformanceAnalysisDict = $(InDetUpgradePerformanceAnalysis_tag).make
cmt_local_tagfile_InDetUpgradePerformanceAnalysisDict = $(bin)$(InDetUpgradePerformanceAnalysis_tag).make

endif

include $(cmt_local_tagfile_InDetUpgradePerformanceAnalysisDict)
#-include $(cmt_local_tagfile_InDetUpgradePerformanceAnalysisDict)

ifdef cmt_InDetUpgradePerformanceAnalysisDict_has_target_tag

cmt_final_setup_InDetUpgradePerformanceAnalysisDict = $(bin)setup_InDetUpgradePerformanceAnalysisDict.make
cmt_dependencies_in_InDetUpgradePerformanceAnalysisDict = $(bin)dependencies_InDetUpgradePerformanceAnalysisDict.in
#cmt_final_setup_InDetUpgradePerformanceAnalysisDict = $(bin)InDetUpgradePerformanceAnalysis_InDetUpgradePerformanceAnalysisDictsetup.make
cmt_local_InDetUpgradePerformanceAnalysisDict_makefile = $(bin)InDetUpgradePerformanceAnalysisDict.make

else

cmt_final_setup_InDetUpgradePerformanceAnalysisDict = $(bin)setup.make
cmt_dependencies_in_InDetUpgradePerformanceAnalysisDict = $(bin)dependencies.in
#cmt_final_setup_InDetUpgradePerformanceAnalysisDict = $(bin)InDetUpgradePerformanceAnalysissetup.make
cmt_local_InDetUpgradePerformanceAnalysisDict_makefile = $(bin)InDetUpgradePerformanceAnalysisDict.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)InDetUpgradePerformanceAnalysissetup.make

#InDetUpgradePerformanceAnalysisDict :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'InDetUpgradePerformanceAnalysisDict'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = InDetUpgradePerformanceAnalysisDict/
#InDetUpgradePerformanceAnalysisDict::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#-- start of libary_header ---------------

InDetUpgradePerformanceAnalysisDictlibname   = $(bin)$(library_prefix)InDetUpgradePerformanceAnalysisDict$(library_suffix)
InDetUpgradePerformanceAnalysisDictlib       = $(InDetUpgradePerformanceAnalysisDictlibname).a
InDetUpgradePerformanceAnalysisDictstamp     = $(bin)InDetUpgradePerformanceAnalysisDict.stamp
InDetUpgradePerformanceAnalysisDictshstamp   = $(bin)InDetUpgradePerformanceAnalysisDict.shstamp

InDetUpgradePerformanceAnalysisDict :: dirs  InDetUpgradePerformanceAnalysisDictLIB
	$(echo) "InDetUpgradePerformanceAnalysisDict ok"

#-- end of libary_header ----------------
#-- start of library_no_static ------

#InDetUpgradePerformanceAnalysisDictLIB :: $(InDetUpgradePerformanceAnalysisDictlib) $(InDetUpgradePerformanceAnalysisDictshstamp)
InDetUpgradePerformanceAnalysisDictLIB :: $(InDetUpgradePerformanceAnalysisDictshstamp)
	$(echo) "InDetUpgradePerformanceAnalysisDict : library ok"

$(InDetUpgradePerformanceAnalysisDictlib) :: $(bin)InDetUpgradePerformanceAnalysisDict_gen_InDetUpgradePerformanceAnalysis.o
	$(lib_echo) "static library $@"
	$(lib_silent) cd $(bin); \
	  $(ar) $(InDetUpgradePerformanceAnalysisDictlib) $?
	$(lib_silent) $(ranlib) $(InDetUpgradePerformanceAnalysisDictlib)
	$(lib_silent) cat /dev/null >$(InDetUpgradePerformanceAnalysisDictstamp)

#------------------------------------------------------------------
#  Future improvement? to empty the object files after
#  storing in the library
#
##	  for f in $?; do \
##	    rm $${f}; touch $${f}; \
##	  done
#------------------------------------------------------------------

#
# We add one level of dependency upon the true shared library 
# (rather than simply upon the stamp file)
# this is for cases where the shared library has not been built
# while the stamp was created (error??) 
#

$(InDetUpgradePerformanceAnalysisDictlibname).$(shlibsuffix) :: $(bin)InDetUpgradePerformanceAnalysisDict_gen_InDetUpgradePerformanceAnalysis.o $(use_requirements) $(InDetUpgradePerformanceAnalysisDictstamps)
	$(lib_echo) "shared library $@"
	$(lib_silent) $(shlibbuilder) $(shlibflags) -o $@ $(bin)InDetUpgradePerformanceAnalysisDict_gen_InDetUpgradePerformanceAnalysis.o $(InDetUpgradePerformanceAnalysisDict_shlibflags)
	$(lib_silent) cat /dev/null >$(InDetUpgradePerformanceAnalysisDictstamp) && \
	  cat /dev/null >$(InDetUpgradePerformanceAnalysisDictshstamp)

$(InDetUpgradePerformanceAnalysisDictshstamp) :: $(InDetUpgradePerformanceAnalysisDictlibname).$(shlibsuffix)
	$(lib_silent) if test -f $(InDetUpgradePerformanceAnalysisDictlibname).$(shlibsuffix) ; then \
	  cat /dev/null >$(InDetUpgradePerformanceAnalysisDictstamp) && \
	  cat /dev/null >$(InDetUpgradePerformanceAnalysisDictshstamp) ; fi

InDetUpgradePerformanceAnalysisDictclean ::
	$(cleanup_echo) objects InDetUpgradePerformanceAnalysisDict
	$(cleanup_silent) /bin/rm -f $(bin)InDetUpgradePerformanceAnalysisDict_gen_InDetUpgradePerformanceAnalysis.o
	$(cleanup_silent) /bin/rm -f $(patsubst %.o,%.d,$(bin)InDetUpgradePerformanceAnalysisDict_gen_InDetUpgradePerformanceAnalysis.o) $(patsubst %.o,%.dep,$(bin)InDetUpgradePerformanceAnalysisDict_gen_InDetUpgradePerformanceAnalysis.o) $(patsubst %.o,%.d.stamp,$(bin)InDetUpgradePerformanceAnalysisDict_gen_InDetUpgradePerformanceAnalysis.o)
	$(cleanup_silent) cd $(bin); /bin/rm -rf InDetUpgradePerformanceAnalysisDict_deps InDetUpgradePerformanceAnalysisDict_dependencies.make

#-----------------------------------------------------------------
#
#  New section for automatic installation
#
#-----------------------------------------------------------------

install_dir = ${CMTINSTALLAREA}/$(tag)/lib
InDetUpgradePerformanceAnalysisDictinstallname = $(library_prefix)InDetUpgradePerformanceAnalysisDict$(library_suffix).$(shlibsuffix)

InDetUpgradePerformanceAnalysisDict :: InDetUpgradePerformanceAnalysisDictinstall ;

install :: InDetUpgradePerformanceAnalysisDictinstall ;

InDetUpgradePerformanceAnalysisDictinstall :: $(install_dir)/$(InDetUpgradePerformanceAnalysisDictinstallname)
ifdef CMTINSTALLAREA
	$(echo) "installation done"
endif

$(install_dir)/$(InDetUpgradePerformanceAnalysisDictinstallname) :: $(bin)$(InDetUpgradePerformanceAnalysisDictinstallname)
ifdef CMTINSTALLAREA
	$(install_silent) $(cmt_install_action) \
	    -source "`(cd $(bin); pwd)`" \
	    -name "$(InDetUpgradePerformanceAnalysisDictinstallname)" \
	    -out "$(install_dir)" \
	    -cmd "$(cmt_installarea_command)" \
	    -cmtpath "$($(package)_cmtpath)"
endif

##InDetUpgradePerformanceAnalysisDictclean :: InDetUpgradePerformanceAnalysisDictuninstall

uninstall :: InDetUpgradePerformanceAnalysisDictuninstall ;

InDetUpgradePerformanceAnalysisDictuninstall ::
ifdef CMTINSTALLAREA
	$(cleanup_silent) $(cmt_uninstall_action) \
	    -source "`(cd $(bin); pwd)`" \
	    -name "$(InDetUpgradePerformanceAnalysisDictinstallname)" \
	    -out "$(install_dir)" \
	    -cmtpath "$($(package)_cmtpath)"
endif

#-- end of library_no_static ------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),InDetUpgradePerformanceAnalysisDictclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)InDetUpgradePerformanceAnalysisDict_gen_InDetUpgradePerformanceAnalysis.d

$(bin)$(binobj)InDetUpgradePerformanceAnalysisDict_gen_InDetUpgradePerformanceAnalysis.d :

$(bin)$(binobj)InDetUpgradePerformanceAnalysisDict_gen_InDetUpgradePerformanceAnalysis.o : $(cmt_final_setup_InDetUpgradePerformanceAnalysisDict)

$(bin)$(binobj)InDetUpgradePerformanceAnalysisDict_gen_InDetUpgradePerformanceAnalysis.o : ../x86_64-slc6-gcc49-opt/dict/InDetUpgradePerformanceAnalysis/InDetUpgradePerformanceAnalysisDict_gen.cpp
	$(cpp_echo) ../x86_64-slc6-gcc49-opt/dict/InDetUpgradePerformanceAnalysis/InDetUpgradePerformanceAnalysisDict_gen.cpp
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(InDetUpgradePerformanceAnalysisDict_pp_cppflags) $(lib_InDetUpgradePerformanceAnalysisDict_pp_cppflags) $(InDetUpgradePerformanceAnalysisDict_gen_pp_cppflags) $(use_cppflags) $(InDetUpgradePerformanceAnalysisDict_cppflags) $(lib_InDetUpgradePerformanceAnalysisDict_cppflags) $(InDetUpgradePerformanceAnalysisDict_gen_cppflags) $(InDetUpgradePerformanceAnalysisDict_gen_cpp_cppflags) -I../x86_64-slc6-gcc49-opt/dict/InDetUpgradePerformanceAnalysis ../x86_64-slc6-gcc49-opt/dict/InDetUpgradePerformanceAnalysis/InDetUpgradePerformanceAnalysisDict_gen.cpp
endif
endif

else
$(bin)InDetUpgradePerformanceAnalysisDict_dependencies.make : $(InDetUpgradePerformanceAnalysisDict_gen_cpp_dependencies)

$(bin)InDetUpgradePerformanceAnalysisDict_dependencies.make : ../x86_64-slc6-gcc49-opt/dict/InDetUpgradePerformanceAnalysis/InDetUpgradePerformanceAnalysisDict_gen.cpp

$(bin)$(binobj)InDetUpgradePerformanceAnalysisDict_gen_InDetUpgradePerformanceAnalysis.o : $(InDetUpgradePerformanceAnalysisDict_gen_cpp_dependencies)
	$(cpp_echo) ../x86_64-slc6-gcc49-opt/dict/InDetUpgradePerformanceAnalysis/InDetUpgradePerformanceAnalysisDict_gen.cpp
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(InDetUpgradePerformanceAnalysisDict_pp_cppflags) $(lib_InDetUpgradePerformanceAnalysisDict_pp_cppflags) $(InDetUpgradePerformanceAnalysisDict_gen_pp_cppflags) $(use_cppflags) $(InDetUpgradePerformanceAnalysisDict_cppflags) $(lib_InDetUpgradePerformanceAnalysisDict_cppflags) $(InDetUpgradePerformanceAnalysisDict_gen_cppflags) $(InDetUpgradePerformanceAnalysisDict_gen_cpp_cppflags) -I../x86_64-slc6-gcc49-opt/dict/InDetUpgradePerformanceAnalysis ../x86_64-slc6-gcc49-opt/dict/InDetUpgradePerformanceAnalysis/InDetUpgradePerformanceAnalysisDict_gen.cpp

endif

#-- end of cpp_library ------------------
#-- start of cleanup_header --------------

clean :: InDetUpgradePerformanceAnalysisDictclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(InDetUpgradePerformanceAnalysisDict.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

InDetUpgradePerformanceAnalysisDictclean ::
#-- end of cleanup_header ---------------
#-- start of cleanup_library -------------
	$(cleanup_echo) library InDetUpgradePerformanceAnalysisDict
	-$(cleanup_silent) cd $(bin) && \rm -f $(library_prefix)InDetUpgradePerformanceAnalysisDict$(library_suffix).a $(library_prefix)InDetUpgradePerformanceAnalysisDict$(library_suffix).$(shlibsuffix) InDetUpgradePerformanceAnalysisDict.stamp InDetUpgradePerformanceAnalysisDict.shstamp
#-- end of cleanup_library ---------------
