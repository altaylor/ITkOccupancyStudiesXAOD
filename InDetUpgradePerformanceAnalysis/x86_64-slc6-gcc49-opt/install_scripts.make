#-- start of make_header -----------------

#====================================
#  Document install_scripts
#
#   Generated Tue May 10 13:56:53 2016  by altaylor
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_install_scripts_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_install_scripts_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_install_scripts

InDetUpgradePerformanceAnalysis_tag = $(tag)

#cmt_local_tagfile_install_scripts = $(InDetUpgradePerformanceAnalysis_tag)_install_scripts.make
cmt_local_tagfile_install_scripts = $(bin)$(InDetUpgradePerformanceAnalysis_tag)_install_scripts.make

else

tags      = $(tag),$(CMTEXTRATAGS)

InDetUpgradePerformanceAnalysis_tag = $(tag)

#cmt_local_tagfile_install_scripts = $(InDetUpgradePerformanceAnalysis_tag).make
cmt_local_tagfile_install_scripts = $(bin)$(InDetUpgradePerformanceAnalysis_tag).make

endif

include $(cmt_local_tagfile_install_scripts)
#-include $(cmt_local_tagfile_install_scripts)

ifdef cmt_install_scripts_has_target_tag

cmt_final_setup_install_scripts = $(bin)setup_install_scripts.make
cmt_dependencies_in_install_scripts = $(bin)dependencies_install_scripts.in
#cmt_final_setup_install_scripts = $(bin)InDetUpgradePerformanceAnalysis_install_scriptssetup.make
cmt_local_install_scripts_makefile = $(bin)install_scripts.make

else

cmt_final_setup_install_scripts = $(bin)setup.make
cmt_dependencies_in_install_scripts = $(bin)dependencies.in
#cmt_final_setup_install_scripts = $(bin)InDetUpgradePerformanceAnalysissetup.make
cmt_local_install_scripts_makefile = $(bin)install_scripts.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)InDetUpgradePerformanceAnalysissetup.make

#install_scripts :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'install_scripts'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = install_scripts/
#install_scripts::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------


ifeq ($(INSTALLAREA),)
installarea = $(CMTINSTALLAREA)
else
ifeq ($(findstring `,$(INSTALLAREA)),`)
installarea = $(shell $(subst `,, $(INSTALLAREA)))
else
installarea = $(INSTALLAREA)
endif
endif

install_dir = ${installarea}/share/bin

install_scripts :: install_scriptsinstall ;

install :: install_scriptsinstall ;

install_scriptsclean :: install_scriptsuninstall

uninstall :: install_scriptsuninstall


# This is to avoid error in case there are no files to install
# Ideally, the fragment should not be used without files to install,
# and this line should be dropped then
install_scriptsinstall :: ;

upg_perf_analyze_py_dependencies = ../scripts/upg_perf_analyze.py
upg_scoping_varCuts_res_py_dependencies = ../scripts/upg_scoping_varCuts_res.py
upg_perf_anal_Zmumu_py_dependencies = ../scripts/upg_perf_anal_Zmumu.py
upg_idr_make_plots_py_dependencies = ../scripts/upg_idr_make_plots.py
upg_occupancies_py_dependencies = ../scripts/upg_occupancies.py
upg_cluster_sizes_py_dependencies = ../scripts/upg_cluster_sizes.py
upg_make_plots_py_dependencies = ../scripts/upg_make_plots.py
upg_plot_max_occupancies_py_dependencies = ../scripts/upg_plot_max_occupancies.py
upg_merge_py_dependencies = ../scripts/upg_merge.py
upg_scoping_matchProb_py_dependencies = ../scripts/upg_scoping_matchProb.py
upg_max_occupancies_py_dependencies = ../scripts/upg_max_occupancies.py
upg_occupancies_fastGeo_py_dependencies = ../scripts/upg_occupancies_fastGeo.py
upg_scoping_plot_nHits_py_dependencies = ../scripts/upg_scoping_plot_nHits.py
upg_plot_max_occupancy_stats_py_dependencies = ../scripts/upg_plot_max_occupancy_stats.py
upg_scoping_nHits_py_dependencies = ../scripts/upg_scoping_nHits.py
upg_occupancies_doubleStripLength_py_dependencies = ../scripts/upg_occupancies_doubleStripLength.py
upg_max_occupancies_doubleStripLength_py_dependencies = ../scripts/upg_max_occupancies_doubleStripLength.py
upg_scoping_plot_varHits_eff_py_dependencies = ../scripts/upg_scoping_plot_varHits_eff.py
upg_make_occ_plots_py_dependencies = ../scripts/upg_make_occ_plots.py
upg_idr_make_plots_compare_layouts_py_dependencies = ../scripts/upg_idr_make_plots_compare_layouts.py
upg_idr_make_plots_compareFitRMS_py_dependencies = ../scripts/upg_idr_make_plots_compareFitRMS.py
upg_perf_analyze_btagd3pd_py_dependencies = ../scripts/upg_perf_analyze_btagd3pd.py
upg_scoping_varCuts_eff_py_dependencies = ../scripts/upg_scoping_varCuts_eff.py
upg_idr_analyze_ttbar_py_dependencies = ../scripts/upg_idr_analyze_ttbar.py
upg_scoping_plot_flatCuts_py_dependencies = ../scripts/upg_scoping_plot_flatCuts.py
upg_scoping_flatCuts_py_dependencies = ../scripts/upg_scoping_flatCuts.py
upg_make_occ_plots_2d_py_dependencies = ../scripts/upg_make_occ_plots_2d.py
upg_scoping_plot_varHits_res_py_dependencies = ../scripts/upg_scoping_plot_varHits_res.py
upg_runJob_sh_dependencies = ../scripts/upg_runJob.sh
upg_submitJobs_sh_dependencies = ../scripts/upg_submitJobs.sh
upg_batch_sub_sh_dependencies = ../scripts/upg_batch_sub.sh
upg_batch_sub_copyLocal_sh_dependencies = ../scripts/upg_batch_sub_copyLocal.sh


install_scriptsinstall :: ${install_dir}/upg_perf_analyze.py ;

${install_dir}/upg_perf_analyze.py :: ../scripts/upg_perf_analyze.py
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_perf_analyze.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "upg_perf_analyze.py" "$(install_dir)" "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasCore/20.20.0/External/ExternalPolicy/src/symlink.sh" "$($(package)_cmtpath)"; \
	fi

../scripts/upg_perf_analyze.py : ;

install_scriptsuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_perf_analyze.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "upg_perf_analyze.py" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_scriptsinstall :: ${install_dir}/upg_scoping_varCuts_res.py ;

${install_dir}/upg_scoping_varCuts_res.py :: ../scripts/upg_scoping_varCuts_res.py
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_scoping_varCuts_res.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "upg_scoping_varCuts_res.py" "$(install_dir)" "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasCore/20.20.0/External/ExternalPolicy/src/symlink.sh" "$($(package)_cmtpath)"; \
	fi

../scripts/upg_scoping_varCuts_res.py : ;

install_scriptsuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_scoping_varCuts_res.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "upg_scoping_varCuts_res.py" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_scriptsinstall :: ${install_dir}/upg_perf_anal_Zmumu.py ;

${install_dir}/upg_perf_anal_Zmumu.py :: ../scripts/upg_perf_anal_Zmumu.py
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_perf_anal_Zmumu.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "upg_perf_anal_Zmumu.py" "$(install_dir)" "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasCore/20.20.0/External/ExternalPolicy/src/symlink.sh" "$($(package)_cmtpath)"; \
	fi

../scripts/upg_perf_anal_Zmumu.py : ;

install_scriptsuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_perf_anal_Zmumu.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "upg_perf_anal_Zmumu.py" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_scriptsinstall :: ${install_dir}/upg_idr_make_plots.py ;

${install_dir}/upg_idr_make_plots.py :: ../scripts/upg_idr_make_plots.py
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_idr_make_plots.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "upg_idr_make_plots.py" "$(install_dir)" "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasCore/20.20.0/External/ExternalPolicy/src/symlink.sh" "$($(package)_cmtpath)"; \
	fi

../scripts/upg_idr_make_plots.py : ;

install_scriptsuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_idr_make_plots.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "upg_idr_make_plots.py" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_scriptsinstall :: ${install_dir}/upg_occupancies.py ;

${install_dir}/upg_occupancies.py :: ../scripts/upg_occupancies.py
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_occupancies.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "upg_occupancies.py" "$(install_dir)" "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasCore/20.20.0/External/ExternalPolicy/src/symlink.sh" "$($(package)_cmtpath)"; \
	fi

../scripts/upg_occupancies.py : ;

install_scriptsuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_occupancies.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "upg_occupancies.py" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_scriptsinstall :: ${install_dir}/upg_cluster_sizes.py ;

${install_dir}/upg_cluster_sizes.py :: ../scripts/upg_cluster_sizes.py
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_cluster_sizes.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "upg_cluster_sizes.py" "$(install_dir)" "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasCore/20.20.0/External/ExternalPolicy/src/symlink.sh" "$($(package)_cmtpath)"; \
	fi

../scripts/upg_cluster_sizes.py : ;

install_scriptsuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_cluster_sizes.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "upg_cluster_sizes.py" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_scriptsinstall :: ${install_dir}/upg_make_plots.py ;

${install_dir}/upg_make_plots.py :: ../scripts/upg_make_plots.py
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_make_plots.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "upg_make_plots.py" "$(install_dir)" "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasCore/20.20.0/External/ExternalPolicy/src/symlink.sh" "$($(package)_cmtpath)"; \
	fi

../scripts/upg_make_plots.py : ;

install_scriptsuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_make_plots.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "upg_make_plots.py" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_scriptsinstall :: ${install_dir}/upg_plot_max_occupancies.py ;

${install_dir}/upg_plot_max_occupancies.py :: ../scripts/upg_plot_max_occupancies.py
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_plot_max_occupancies.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "upg_plot_max_occupancies.py" "$(install_dir)" "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasCore/20.20.0/External/ExternalPolicy/src/symlink.sh" "$($(package)_cmtpath)"; \
	fi

../scripts/upg_plot_max_occupancies.py : ;

install_scriptsuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_plot_max_occupancies.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "upg_plot_max_occupancies.py" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_scriptsinstall :: ${install_dir}/upg_merge.py ;

${install_dir}/upg_merge.py :: ../scripts/upg_merge.py
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_merge.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "upg_merge.py" "$(install_dir)" "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasCore/20.20.0/External/ExternalPolicy/src/symlink.sh" "$($(package)_cmtpath)"; \
	fi

../scripts/upg_merge.py : ;

install_scriptsuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_merge.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "upg_merge.py" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_scriptsinstall :: ${install_dir}/upg_scoping_matchProb.py ;

${install_dir}/upg_scoping_matchProb.py :: ../scripts/upg_scoping_matchProb.py
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_scoping_matchProb.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "upg_scoping_matchProb.py" "$(install_dir)" "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasCore/20.20.0/External/ExternalPolicy/src/symlink.sh" "$($(package)_cmtpath)"; \
	fi

../scripts/upg_scoping_matchProb.py : ;

install_scriptsuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_scoping_matchProb.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "upg_scoping_matchProb.py" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_scriptsinstall :: ${install_dir}/upg_max_occupancies.py ;

${install_dir}/upg_max_occupancies.py :: ../scripts/upg_max_occupancies.py
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_max_occupancies.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "upg_max_occupancies.py" "$(install_dir)" "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasCore/20.20.0/External/ExternalPolicy/src/symlink.sh" "$($(package)_cmtpath)"; \
	fi

../scripts/upg_max_occupancies.py : ;

install_scriptsuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_max_occupancies.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "upg_max_occupancies.py" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_scriptsinstall :: ${install_dir}/upg_occupancies_fastGeo.py ;

${install_dir}/upg_occupancies_fastGeo.py :: ../scripts/upg_occupancies_fastGeo.py
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_occupancies_fastGeo.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "upg_occupancies_fastGeo.py" "$(install_dir)" "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasCore/20.20.0/External/ExternalPolicy/src/symlink.sh" "$($(package)_cmtpath)"; \
	fi

../scripts/upg_occupancies_fastGeo.py : ;

install_scriptsuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_occupancies_fastGeo.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "upg_occupancies_fastGeo.py" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_scriptsinstall :: ${install_dir}/upg_scoping_plot_nHits.py ;

${install_dir}/upg_scoping_plot_nHits.py :: ../scripts/upg_scoping_plot_nHits.py
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_scoping_plot_nHits.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "upg_scoping_plot_nHits.py" "$(install_dir)" "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasCore/20.20.0/External/ExternalPolicy/src/symlink.sh" "$($(package)_cmtpath)"; \
	fi

../scripts/upg_scoping_plot_nHits.py : ;

install_scriptsuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_scoping_plot_nHits.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "upg_scoping_plot_nHits.py" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_scriptsinstall :: ${install_dir}/upg_plot_max_occupancy_stats.py ;

${install_dir}/upg_plot_max_occupancy_stats.py :: ../scripts/upg_plot_max_occupancy_stats.py
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_plot_max_occupancy_stats.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "upg_plot_max_occupancy_stats.py" "$(install_dir)" "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasCore/20.20.0/External/ExternalPolicy/src/symlink.sh" "$($(package)_cmtpath)"; \
	fi

../scripts/upg_plot_max_occupancy_stats.py : ;

install_scriptsuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_plot_max_occupancy_stats.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "upg_plot_max_occupancy_stats.py" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_scriptsinstall :: ${install_dir}/upg_scoping_nHits.py ;

${install_dir}/upg_scoping_nHits.py :: ../scripts/upg_scoping_nHits.py
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_scoping_nHits.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "upg_scoping_nHits.py" "$(install_dir)" "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasCore/20.20.0/External/ExternalPolicy/src/symlink.sh" "$($(package)_cmtpath)"; \
	fi

../scripts/upg_scoping_nHits.py : ;

install_scriptsuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_scoping_nHits.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "upg_scoping_nHits.py" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_scriptsinstall :: ${install_dir}/upg_occupancies_doubleStripLength.py ;

${install_dir}/upg_occupancies_doubleStripLength.py :: ../scripts/upg_occupancies_doubleStripLength.py
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_occupancies_doubleStripLength.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "upg_occupancies_doubleStripLength.py" "$(install_dir)" "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasCore/20.20.0/External/ExternalPolicy/src/symlink.sh" "$($(package)_cmtpath)"; \
	fi

../scripts/upg_occupancies_doubleStripLength.py : ;

install_scriptsuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_occupancies_doubleStripLength.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "upg_occupancies_doubleStripLength.py" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_scriptsinstall :: ${install_dir}/upg_max_occupancies_doubleStripLength.py ;

${install_dir}/upg_max_occupancies_doubleStripLength.py :: ../scripts/upg_max_occupancies_doubleStripLength.py
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_max_occupancies_doubleStripLength.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "upg_max_occupancies_doubleStripLength.py" "$(install_dir)" "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasCore/20.20.0/External/ExternalPolicy/src/symlink.sh" "$($(package)_cmtpath)"; \
	fi

../scripts/upg_max_occupancies_doubleStripLength.py : ;

install_scriptsuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_max_occupancies_doubleStripLength.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "upg_max_occupancies_doubleStripLength.py" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_scriptsinstall :: ${install_dir}/upg_scoping_plot_varHits_eff.py ;

${install_dir}/upg_scoping_plot_varHits_eff.py :: ../scripts/upg_scoping_plot_varHits_eff.py
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_scoping_plot_varHits_eff.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "upg_scoping_plot_varHits_eff.py" "$(install_dir)" "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasCore/20.20.0/External/ExternalPolicy/src/symlink.sh" "$($(package)_cmtpath)"; \
	fi

../scripts/upg_scoping_plot_varHits_eff.py : ;

install_scriptsuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_scoping_plot_varHits_eff.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "upg_scoping_plot_varHits_eff.py" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_scriptsinstall :: ${install_dir}/upg_make_occ_plots.py ;

${install_dir}/upg_make_occ_plots.py :: ../scripts/upg_make_occ_plots.py
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_make_occ_plots.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "upg_make_occ_plots.py" "$(install_dir)" "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasCore/20.20.0/External/ExternalPolicy/src/symlink.sh" "$($(package)_cmtpath)"; \
	fi

../scripts/upg_make_occ_plots.py : ;

install_scriptsuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_make_occ_plots.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "upg_make_occ_plots.py" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_scriptsinstall :: ${install_dir}/upg_idr_make_plots_compare_layouts.py ;

${install_dir}/upg_idr_make_plots_compare_layouts.py :: ../scripts/upg_idr_make_plots_compare_layouts.py
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_idr_make_plots_compare_layouts.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "upg_idr_make_plots_compare_layouts.py" "$(install_dir)" "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasCore/20.20.0/External/ExternalPolicy/src/symlink.sh" "$($(package)_cmtpath)"; \
	fi

../scripts/upg_idr_make_plots_compare_layouts.py : ;

install_scriptsuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_idr_make_plots_compare_layouts.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "upg_idr_make_plots_compare_layouts.py" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_scriptsinstall :: ${install_dir}/upg_idr_make_plots_compareFitRMS.py ;

${install_dir}/upg_idr_make_plots_compareFitRMS.py :: ../scripts/upg_idr_make_plots_compareFitRMS.py
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_idr_make_plots_compareFitRMS.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "upg_idr_make_plots_compareFitRMS.py" "$(install_dir)" "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasCore/20.20.0/External/ExternalPolicy/src/symlink.sh" "$($(package)_cmtpath)"; \
	fi

../scripts/upg_idr_make_plots_compareFitRMS.py : ;

install_scriptsuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_idr_make_plots_compareFitRMS.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "upg_idr_make_plots_compareFitRMS.py" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_scriptsinstall :: ${install_dir}/upg_perf_analyze_btagd3pd.py ;

${install_dir}/upg_perf_analyze_btagd3pd.py :: ../scripts/upg_perf_analyze_btagd3pd.py
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_perf_analyze_btagd3pd.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "upg_perf_analyze_btagd3pd.py" "$(install_dir)" "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasCore/20.20.0/External/ExternalPolicy/src/symlink.sh" "$($(package)_cmtpath)"; \
	fi

../scripts/upg_perf_analyze_btagd3pd.py : ;

install_scriptsuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_perf_analyze_btagd3pd.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "upg_perf_analyze_btagd3pd.py" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_scriptsinstall :: ${install_dir}/upg_scoping_varCuts_eff.py ;

${install_dir}/upg_scoping_varCuts_eff.py :: ../scripts/upg_scoping_varCuts_eff.py
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_scoping_varCuts_eff.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "upg_scoping_varCuts_eff.py" "$(install_dir)" "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasCore/20.20.0/External/ExternalPolicy/src/symlink.sh" "$($(package)_cmtpath)"; \
	fi

../scripts/upg_scoping_varCuts_eff.py : ;

install_scriptsuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_scoping_varCuts_eff.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "upg_scoping_varCuts_eff.py" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_scriptsinstall :: ${install_dir}/upg_idr_analyze_ttbar.py ;

${install_dir}/upg_idr_analyze_ttbar.py :: ../scripts/upg_idr_analyze_ttbar.py
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_idr_analyze_ttbar.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "upg_idr_analyze_ttbar.py" "$(install_dir)" "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasCore/20.20.0/External/ExternalPolicy/src/symlink.sh" "$($(package)_cmtpath)"; \
	fi

../scripts/upg_idr_analyze_ttbar.py : ;

install_scriptsuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_idr_analyze_ttbar.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "upg_idr_analyze_ttbar.py" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_scriptsinstall :: ${install_dir}/upg_scoping_plot_flatCuts.py ;

${install_dir}/upg_scoping_plot_flatCuts.py :: ../scripts/upg_scoping_plot_flatCuts.py
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_scoping_plot_flatCuts.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "upg_scoping_plot_flatCuts.py" "$(install_dir)" "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasCore/20.20.0/External/ExternalPolicy/src/symlink.sh" "$($(package)_cmtpath)"; \
	fi

../scripts/upg_scoping_plot_flatCuts.py : ;

install_scriptsuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_scoping_plot_flatCuts.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "upg_scoping_plot_flatCuts.py" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_scriptsinstall :: ${install_dir}/upg_scoping_flatCuts.py ;

${install_dir}/upg_scoping_flatCuts.py :: ../scripts/upg_scoping_flatCuts.py
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_scoping_flatCuts.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "upg_scoping_flatCuts.py" "$(install_dir)" "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasCore/20.20.0/External/ExternalPolicy/src/symlink.sh" "$($(package)_cmtpath)"; \
	fi

../scripts/upg_scoping_flatCuts.py : ;

install_scriptsuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_scoping_flatCuts.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "upg_scoping_flatCuts.py" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_scriptsinstall :: ${install_dir}/upg_make_occ_plots_2d.py ;

${install_dir}/upg_make_occ_plots_2d.py :: ../scripts/upg_make_occ_plots_2d.py
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_make_occ_plots_2d.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "upg_make_occ_plots_2d.py" "$(install_dir)" "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasCore/20.20.0/External/ExternalPolicy/src/symlink.sh" "$($(package)_cmtpath)"; \
	fi

../scripts/upg_make_occ_plots_2d.py : ;

install_scriptsuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_make_occ_plots_2d.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "upg_make_occ_plots_2d.py" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_scriptsinstall :: ${install_dir}/upg_scoping_plot_varHits_res.py ;

${install_dir}/upg_scoping_plot_varHits_res.py :: ../scripts/upg_scoping_plot_varHits_res.py
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_scoping_plot_varHits_res.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "upg_scoping_plot_varHits_res.py" "$(install_dir)" "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasCore/20.20.0/External/ExternalPolicy/src/symlink.sh" "$($(package)_cmtpath)"; \
	fi

../scripts/upg_scoping_plot_varHits_res.py : ;

install_scriptsuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_scoping_plot_varHits_res.py`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "upg_scoping_plot_varHits_res.py" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_scriptsinstall :: ${install_dir}/upg_runJob.sh ;

${install_dir}/upg_runJob.sh :: ../scripts/upg_runJob.sh
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_runJob.sh`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "upg_runJob.sh" "$(install_dir)" "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasCore/20.20.0/External/ExternalPolicy/src/symlink.sh" "$($(package)_cmtpath)"; \
	fi

../scripts/upg_runJob.sh : ;

install_scriptsuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_runJob.sh`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "upg_runJob.sh" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_scriptsinstall :: ${install_dir}/upg_submitJobs.sh ;

${install_dir}/upg_submitJobs.sh :: ../scripts/upg_submitJobs.sh
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_submitJobs.sh`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "upg_submitJobs.sh" "$(install_dir)" "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasCore/20.20.0/External/ExternalPolicy/src/symlink.sh" "$($(package)_cmtpath)"; \
	fi

../scripts/upg_submitJobs.sh : ;

install_scriptsuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_submitJobs.sh`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "upg_submitJobs.sh" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_scriptsinstall :: ${install_dir}/upg_batch_sub.sh ;

${install_dir}/upg_batch_sub.sh :: ../scripts/upg_batch_sub.sh
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_batch_sub.sh`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "upg_batch_sub.sh" "$(install_dir)" "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasCore/20.20.0/External/ExternalPolicy/src/symlink.sh" "$($(package)_cmtpath)"; \
	fi

../scripts/upg_batch_sub.sh : ;

install_scriptsuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_batch_sub.sh`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "upg_batch_sub.sh" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi


install_scriptsinstall :: ${install_dir}/upg_batch_sub_copyLocal.sh ;

${install_dir}/upg_batch_sub_copyLocal.sh :: ../scripts/upg_batch_sub_copyLocal.sh
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_batch_sub_copyLocal.sh`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "upg_batch_sub_copyLocal.sh" "$(install_dir)" "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasCore/20.20.0/External/ExternalPolicy/src/symlink.sh" "$($(package)_cmtpath)"; \
	fi

../scripts/upg_batch_sub_copyLocal.sh : ;

install_scriptsuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../scripts/upg_batch_sub_copyLocal.sh`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "upg_batch_sub_copyLocal.sh" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi
#-- start of cleanup_header --------------

clean :: install_scriptsclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(install_scripts.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

install_scriptsclean ::
#-- end of cleanup_header ---------------
