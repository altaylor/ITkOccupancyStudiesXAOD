#-- start of make_header -----------------

#====================================
#  Document InstallPCM_InDetUpgradePerformanceAnalysis
#
#   Generated Tue May 10 13:57:03 2016  by altaylor
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_InstallPCM_InDetUpgradePerformanceAnalysis_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_InstallPCM_InDetUpgradePerformanceAnalysis_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_InstallPCM_InDetUpgradePerformanceAnalysis

InDetUpgradePerformanceAnalysis_tag = $(tag)

#cmt_local_tagfile_InstallPCM_InDetUpgradePerformanceAnalysis = $(InDetUpgradePerformanceAnalysis_tag)_InstallPCM_InDetUpgradePerformanceAnalysis.make
cmt_local_tagfile_InstallPCM_InDetUpgradePerformanceAnalysis = $(bin)$(InDetUpgradePerformanceAnalysis_tag)_InstallPCM_InDetUpgradePerformanceAnalysis.make

else

tags      = $(tag),$(CMTEXTRATAGS)

InDetUpgradePerformanceAnalysis_tag = $(tag)

#cmt_local_tagfile_InstallPCM_InDetUpgradePerformanceAnalysis = $(InDetUpgradePerformanceAnalysis_tag).make
cmt_local_tagfile_InstallPCM_InDetUpgradePerformanceAnalysis = $(bin)$(InDetUpgradePerformanceAnalysis_tag).make

endif

include $(cmt_local_tagfile_InstallPCM_InDetUpgradePerformanceAnalysis)
#-include $(cmt_local_tagfile_InstallPCM_InDetUpgradePerformanceAnalysis)

ifdef cmt_InstallPCM_InDetUpgradePerformanceAnalysis_has_target_tag

cmt_final_setup_InstallPCM_InDetUpgradePerformanceAnalysis = $(bin)setup_InstallPCM_InDetUpgradePerformanceAnalysis.make
cmt_dependencies_in_InstallPCM_InDetUpgradePerformanceAnalysis = $(bin)dependencies_InstallPCM_InDetUpgradePerformanceAnalysis.in
#cmt_final_setup_InstallPCM_InDetUpgradePerformanceAnalysis = $(bin)InDetUpgradePerformanceAnalysis_InstallPCM_InDetUpgradePerformanceAnalysissetup.make
cmt_local_InstallPCM_InDetUpgradePerformanceAnalysis_makefile = $(bin)InstallPCM_InDetUpgradePerformanceAnalysis.make

else

cmt_final_setup_InstallPCM_InDetUpgradePerformanceAnalysis = $(bin)setup.make
cmt_dependencies_in_InstallPCM_InDetUpgradePerformanceAnalysis = $(bin)dependencies.in
#cmt_final_setup_InstallPCM_InDetUpgradePerformanceAnalysis = $(bin)InDetUpgradePerformanceAnalysissetup.make
cmt_local_InstallPCM_InDetUpgradePerformanceAnalysis_makefile = $(bin)InstallPCM_InDetUpgradePerformanceAnalysis.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)InDetUpgradePerformanceAnalysissetup.make

#InstallPCM_InDetUpgradePerformanceAnalysis :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'InstallPCM_InDetUpgradePerformanceAnalysis'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = InstallPCM_InDetUpgradePerformanceAnalysis/
#InstallPCM_InDetUpgradePerformanceAnalysis::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------

installDir := "${CMTINSTALLAREA}/$(tag)/lib/"

${installDir} :: 
	mkdir -p ${installDir}

# Basic dependency 
InstallPCM_InDetUpgradePerformanceAnalysis :: ${installDir} $(InstallPCM_InDetUpgradePerformanceAnalysis_output)InDetUpgradePerformanceAnalysisDict.install_lcgdict_pcms
	@echo "------> InstallPCM_InDetUpgradePerformanceAnalysis Done"
InDetUpgradePerformanceAnalysisDict_h_dependencies = ../InDetUpgradePerformanceAnalysis/InDetUpgradePerformanceAnalysisDict.h


pcmFN := InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisDict_gen_rdict.pcm


InDetUpgradePerformanceAnalysisDict.install_lcgdict_pcms :  ${installDir}${pcmFN}
	@echo ${pcmFN} installed


${installDir}${pcmFN} : ${bin}dict/InDetUpgradePerformanceAnalysis/${pcmFN}
	@echo "Installing PCM  ${pcmFN}"
#	$(symlink_command) $< $@   # MN: need to get the correct dst path good for symlink ($bin is relative)
	$(atlas_install_command)  $<  $@ 



clean :: InstallPCM_InDetUpgradePerformanceAnalysisclean
	@echo "InstallPCM_InDetUpgradePerformanceAnalysisclean  done"

InstallPCM_InDetUpgradePerformanceAnalysisclean::
	-rm -f ${installDir}${pcmFN}
#-- start of cleanup_header --------------

clean :: InstallPCM_InDetUpgradePerformanceAnalysisclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(InstallPCM_InDetUpgradePerformanceAnalysis.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

InstallPCM_InDetUpgradePerformanceAnalysisclean ::
#-- end of cleanup_header ---------------
