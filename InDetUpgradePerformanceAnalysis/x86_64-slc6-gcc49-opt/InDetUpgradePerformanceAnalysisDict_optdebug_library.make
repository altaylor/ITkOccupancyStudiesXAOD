#-- start of make_header -----------------

#====================================
#  Document InDetUpgradePerformanceAnalysisDict_optdebug_library
#
#   Generated Tue May 10 13:57:24 2016  by altaylor
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_InDetUpgradePerformanceAnalysisDict_optdebug_library_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_InDetUpgradePerformanceAnalysisDict_optdebug_library_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_InDetUpgradePerformanceAnalysisDict_optdebug_library

InDetUpgradePerformanceAnalysis_tag = $(tag)

#cmt_local_tagfile_InDetUpgradePerformanceAnalysisDict_optdebug_library = $(InDetUpgradePerformanceAnalysis_tag)_InDetUpgradePerformanceAnalysisDict_optdebug_library.make
cmt_local_tagfile_InDetUpgradePerformanceAnalysisDict_optdebug_library = $(bin)$(InDetUpgradePerformanceAnalysis_tag)_InDetUpgradePerformanceAnalysisDict_optdebug_library.make

else

tags      = $(tag),$(CMTEXTRATAGS)

InDetUpgradePerformanceAnalysis_tag = $(tag)

#cmt_local_tagfile_InDetUpgradePerformanceAnalysisDict_optdebug_library = $(InDetUpgradePerformanceAnalysis_tag).make
cmt_local_tagfile_InDetUpgradePerformanceAnalysisDict_optdebug_library = $(bin)$(InDetUpgradePerformanceAnalysis_tag).make

endif

include $(cmt_local_tagfile_InDetUpgradePerformanceAnalysisDict_optdebug_library)
#-include $(cmt_local_tagfile_InDetUpgradePerformanceAnalysisDict_optdebug_library)

ifdef cmt_InDetUpgradePerformanceAnalysisDict_optdebug_library_has_target_tag

cmt_final_setup_InDetUpgradePerformanceAnalysisDict_optdebug_library = $(bin)setup_InDetUpgradePerformanceAnalysisDict_optdebug_library.make
cmt_dependencies_in_InDetUpgradePerformanceAnalysisDict_optdebug_library = $(bin)dependencies_InDetUpgradePerformanceAnalysisDict_optdebug_library.in
#cmt_final_setup_InDetUpgradePerformanceAnalysisDict_optdebug_library = $(bin)InDetUpgradePerformanceAnalysis_InDetUpgradePerformanceAnalysisDict_optdebug_librarysetup.make
cmt_local_InDetUpgradePerformanceAnalysisDict_optdebug_library_makefile = $(bin)InDetUpgradePerformanceAnalysisDict_optdebug_library.make

else

cmt_final_setup_InDetUpgradePerformanceAnalysisDict_optdebug_library = $(bin)setup.make
cmt_dependencies_in_InDetUpgradePerformanceAnalysisDict_optdebug_library = $(bin)dependencies.in
#cmt_final_setup_InDetUpgradePerformanceAnalysisDict_optdebug_library = $(bin)InDetUpgradePerformanceAnalysissetup.make
cmt_local_InDetUpgradePerformanceAnalysisDict_optdebug_library_makefile = $(bin)InDetUpgradePerformanceAnalysisDict_optdebug_library.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)InDetUpgradePerformanceAnalysissetup.make

#InDetUpgradePerformanceAnalysisDict_optdebug_library :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'InDetUpgradePerformanceAnalysisDict_optdebug_library'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = InDetUpgradePerformanceAnalysisDict_optdebug_library/
#InDetUpgradePerformanceAnalysisDict_optdebug_library::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#-- start of optdebug_library_header ------
# create  a  two  part  executable.   One  a
# stripped  binary  which will occupy less space in RAM and in a dis-
# tribution and the second a debugging information file which is only
# needed  if  debugging abilities are required
# See GNU binutils OBJCOPY(1)
# http://sourceware.org/binutils/docs-2.17/binutils/objcopy.html#objcopy

depend=$(bin)$(library_prefix)InDetUpgradePerformanceAnalysisDict.$(shlibsuffix)
target=$(depend)$(debuginfosuffix)

InDetUpgradePerformanceAnalysisDict_optdebug_library :: $(target) ;

$(target) : $(depend)
	$(echo) stripping dbg symbols into separate file $@
	$(link_silent) objcopy --only-keep-debug $< $@
	$(link_silent) objcopy --strip-debug $<
	$(link_silent) cd $(@D) && objcopy --add-gnu-debuglink=$(@F) $(<F)
	$(link_silent) touch -c $@

InDetUpgradePerformanceAnalysisDict_optdebug_libraryclean ::
	$(cleanup_silent) /bin/rm -f $(target)

#-----------------------------------------------------------------
#
#  New section for automatic installation
#
#-----------------------------------------------------------------

install_dir = ${CMTINSTALLAREA}/$(tag)/lib
InDetUpgradePerformanceAnalysisDict_optdebug_libraryinstallname = $(library_prefix)InDetUpgradePerformanceAnalysisDict$(library_suffix).$(shlibsuffix)$(debuginfosuffix)

InDetUpgradePerformanceAnalysisDict_optdebug_library :: InDetUpgradePerformanceAnalysisDict_optdebug_libraryinstall ;

install :: InDetUpgradePerformanceAnalysisDict_optdebug_libraryinstall ;

InDetUpgradePerformanceAnalysisDict_optdebug_libraryinstall :: $(install_dir)/$(InDetUpgradePerformanceAnalysisDict_optdebug_libraryinstallname)
ifdef CMTINSTALLAREA
	$(echo) "$(InDetUpgradePerformanceAnalysisDict_optdebug_libraryinstallname) installation done"
endif

$(install_dir)/$(InDetUpgradePerformanceAnalysisDict_optdebug_libraryinstallname) :: $(bin)$(InDetUpgradePerformanceAnalysisDict_optdebug_libraryinstallname)
ifdef CMTINSTALLAREA
	$(install_silent) $(cmt_install_action) \
	    -source "`(cd $(bin); pwd)`" \
	    -name "$(InDetUpgradePerformanceAnalysisDict_optdebug_libraryinstallname)" \
	    -out "$(install_dir)" \
	    -cmd "$(cmt_installarea_command)" \
	    -cmtpath "$($(package)_cmtpath)"
endif

##InDetUpgradePerformanceAnalysisDict_optdebug_libraryclean :: InDetUpgradePerformanceAnalysisDict_optdebug_libraryuninstall

uninstall :: InDetUpgradePerformanceAnalysisDict_optdebug_libraryuninstall ;

InDetUpgradePerformanceAnalysisDict_optdebug_libraryuninstall ::
ifdef CMTINSTALLAREA
	$(cleanup_silent) $(cmt_uninstall_action) \
	    -source "`(cd $(bin); pwd)`" \
	    -name "$(InDetUpgradePerformanceAnalysisDict_optdebug_libraryinstallname)" \
	    -out "$(install_dir)" \
	    -cmtpath "$($(package)_cmtpath)"
endif

#-- start of optdebug_library_header ------
#-- start of cleanup_header --------------

clean :: InDetUpgradePerformanceAnalysisDict_optdebug_libraryclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(InDetUpgradePerformanceAnalysisDict_optdebug_library.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

InDetUpgradePerformanceAnalysisDict_optdebug_libraryclean ::
#-- end of cleanup_header ---------------
