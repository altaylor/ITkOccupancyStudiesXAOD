#-- start of make_header -----------------

#====================================
#  Document InDetUpgradePerformanceAnalysisDictMerge
#
#   Generated Tue May 10 13:57:24 2016  by altaylor
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_InDetUpgradePerformanceAnalysisDictMerge_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_InDetUpgradePerformanceAnalysisDictMerge_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_InDetUpgradePerformanceAnalysisDictMerge

InDetUpgradePerformanceAnalysis_tag = $(tag)

#cmt_local_tagfile_InDetUpgradePerformanceAnalysisDictMerge = $(InDetUpgradePerformanceAnalysis_tag)_InDetUpgradePerformanceAnalysisDictMerge.make
cmt_local_tagfile_InDetUpgradePerformanceAnalysisDictMerge = $(bin)$(InDetUpgradePerformanceAnalysis_tag)_InDetUpgradePerformanceAnalysisDictMerge.make

else

tags      = $(tag),$(CMTEXTRATAGS)

InDetUpgradePerformanceAnalysis_tag = $(tag)

#cmt_local_tagfile_InDetUpgradePerformanceAnalysisDictMerge = $(InDetUpgradePerformanceAnalysis_tag).make
cmt_local_tagfile_InDetUpgradePerformanceAnalysisDictMerge = $(bin)$(InDetUpgradePerformanceAnalysis_tag).make

endif

include $(cmt_local_tagfile_InDetUpgradePerformanceAnalysisDictMerge)
#-include $(cmt_local_tagfile_InDetUpgradePerformanceAnalysisDictMerge)

ifdef cmt_InDetUpgradePerformanceAnalysisDictMerge_has_target_tag

cmt_final_setup_InDetUpgradePerformanceAnalysisDictMerge = $(bin)setup_InDetUpgradePerformanceAnalysisDictMerge.make
cmt_dependencies_in_InDetUpgradePerformanceAnalysisDictMerge = $(bin)dependencies_InDetUpgradePerformanceAnalysisDictMerge.in
#cmt_final_setup_InDetUpgradePerformanceAnalysisDictMerge = $(bin)InDetUpgradePerformanceAnalysis_InDetUpgradePerformanceAnalysisDictMergesetup.make
cmt_local_InDetUpgradePerformanceAnalysisDictMerge_makefile = $(bin)InDetUpgradePerformanceAnalysisDictMerge.make

else

cmt_final_setup_InDetUpgradePerformanceAnalysisDictMerge = $(bin)setup.make
cmt_dependencies_in_InDetUpgradePerformanceAnalysisDictMerge = $(bin)dependencies.in
#cmt_final_setup_InDetUpgradePerformanceAnalysisDictMerge = $(bin)InDetUpgradePerformanceAnalysissetup.make
cmt_local_InDetUpgradePerformanceAnalysisDictMerge_makefile = $(bin)InDetUpgradePerformanceAnalysisDictMerge.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)InDetUpgradePerformanceAnalysissetup.make

#InDetUpgradePerformanceAnalysisDictMerge :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'InDetUpgradePerformanceAnalysisDictMerge'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = InDetUpgradePerformanceAnalysisDictMerge/
#InDetUpgradePerformanceAnalysisDictMerge::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#
#  Makefile fragment to merge a <package>Dict.dsomap file into 
#  a single <project>Dict.rootmap file in the install area
#

dsoMapFile    := ${dict_dir}/InDetUpgradePerformanceAnalysis/InDetUpgradePerformanceAnalysisDict.dsomap
dsoMapStamp   := $(dsoMapFile).stamp
mergedDictMap := ${CMTINSTALLAREA}/$(tag)/lib/$(project).rootmap

# drive the running of this fragment off of a <package>Dict.dsomap.stamp file
InDetUpgradePerformanceAnalysisDictMerge ::  $(dsoMapStamp) $(mergedDictMap)

$(mergedDictMap) : ${CMTINSTALLAREA}/$(tag)/lib
	touch $(mergedDictMap)

$(dsoMapFile) :
	touch $(dsoMapFile)

$(dsoMapStamp)   : $(mergedDictMap) | $(dsoMapFile)
	@echo "Running merge_dict_rootmap  InDetUpgradePerformanceAnalysisDictMerge. Map: ${mergedDictMap}" ; 
	$(merge_dict_rootmap_cmd) $(dsoMapFile) $(mergedDictMap)

InDetUpgradePerformanceAnalysisDictMergeclean ::
	$(cleanup_silent) $(uninstall_command) $(dsoMapStamp)
#-- start of dependencies ------------------
ifneq ($(MAKECMDGOALS),InDetUpgradePerformanceAnalysisDictMergeclean)
ifneq ($(MAKECMDGOALS),uninstall)

$(bin)InDetUpgradePerformanceAnalysisDictMerge_dependencies.make : $(use_requirements) $(cmt_final_setup_InDetUpgradePerformanceAnalysisDictMerge)
	$(echo) "(InDetUpgradePerformanceAnalysisDictMerge.make) Rebuilding $@"; \
	  $(cmtexe) -tag=$(tags) build dependencies -out=$@ -start_all ../InDetUpgradePerformanceAnalysis/InDetUpgradePerformanceAnalysisDict.h -end_all $(includes) $(app_InDetUpgradePerformanceAnalysisDictMerge_cppflags) $(lib_InDetUpgradePerformanceAnalysisDictMerge_cppflags) -name=InDetUpgradePerformanceAnalysisDictMerge $? -f=$(cmt_dependencies_in_InDetUpgradePerformanceAnalysisDictMerge) -without_cmt

-include $(bin)InDetUpgradePerformanceAnalysisDictMerge_dependencies.make

endif
endif

InDetUpgradePerformanceAnalysisDictMergeclean ::
	$(cleanup_silent) \rm -rf $(bin)InDetUpgradePerformanceAnalysisDictMerge_deps $(bin)InDetUpgradePerformanceAnalysisDictMerge_dependencies.make
#-- end of dependencies -------------------
#-- start of dependency ------------------
ifneq ($(MAKECMDGOALS),InDetUpgradePerformanceAnalysisDictMergeclean)
ifneq ($(MAKECMDGOALS),uninstall)

$(bin)InDetUpgradePerformanceAnalysisDictMerge_dependencies.make : $(InDetUpgradePerformanceAnalysisDict_h_dependencies)

$(bin)InDetUpgradePerformanceAnalysisDictMerge_dependencies.make : ../InDetUpgradePerformanceAnalysis/InDetUpgradePerformanceAnalysisDict.h

endif
endif
#-- end of dependency -------------------
 #-- start of cleanup_header --------------

clean :: InDetUpgradePerformanceAnalysisDictMergeclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(InDetUpgradePerformanceAnalysisDictMerge.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

InDetUpgradePerformanceAnalysisDictMergeclean ::
#-- end of cleanup_header ---------------
