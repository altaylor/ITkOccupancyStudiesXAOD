// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dOdOdIx86_64mIslc6mIgcc49mIoptdIdictdIInDetUpgradePerformanceAnalysisdIInDetUpgradePerformanceAnalysisDict_gen

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "../x86_64-slc6-gcc49-opt/dict/InDetUpgradePerformanceAnalysis/InDetUpgradePerformanceAnalysisDict_gen.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *InDetUpgradeAnalysis_Dictionary();
   static void InDetUpgradeAnalysis_TClassManip(TClass*);
   static void *new_InDetUpgradeAnalysis(void *p = 0);
   static void *newArray_InDetUpgradeAnalysis(Long_t size, void *p);
   static void delete_InDetUpgradeAnalysis(void *p);
   static void deleteArray_InDetUpgradeAnalysis(void *p);
   static void destruct_InDetUpgradeAnalysis(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::InDetUpgradeAnalysis*)
   {
      ::InDetUpgradeAnalysis *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::InDetUpgradeAnalysis));
      static ::ROOT::TGenericClassInfo 
         instance("InDetUpgradeAnalysis", "InDetUpgradePerformanceAnalysis/InDetUpgradeAnalysis.h", 19,
                  typeid(::InDetUpgradeAnalysis), DefineBehavior(ptr, ptr),
                  &InDetUpgradeAnalysis_Dictionary, isa_proxy, 0,
                  sizeof(::InDetUpgradeAnalysis) );
      instance.SetNew(&new_InDetUpgradeAnalysis);
      instance.SetNewArray(&newArray_InDetUpgradeAnalysis);
      instance.SetDelete(&delete_InDetUpgradeAnalysis);
      instance.SetDeleteArray(&deleteArray_InDetUpgradeAnalysis);
      instance.SetDestructor(&destruct_InDetUpgradeAnalysis);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::InDetUpgradeAnalysis*)
   {
      return GenerateInitInstanceLocal((::InDetUpgradeAnalysis*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::InDetUpgradeAnalysis*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *InDetUpgradeAnalysis_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::InDetUpgradeAnalysis*)0x0)->GetClass();
      InDetUpgradeAnalysis_TClassManip(theClass);
   return theClass;
   }

   static void InDetUpgradeAnalysis_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *SingleAnalysis_Dictionary();
   static void SingleAnalysis_TClassManip(TClass*);
   static void delete_SingleAnalysis(void *p);
   static void deleteArray_SingleAnalysis(void *p);
   static void destruct_SingleAnalysis(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::SingleAnalysis*)
   {
      ::SingleAnalysis *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::SingleAnalysis));
      static ::ROOT::TGenericClassInfo 
         instance("SingleAnalysis", "InDetUpgradePerformanceAnalysis/SingleAnalysis.h", 12,
                  typeid(::SingleAnalysis), DefineBehavior(ptr, ptr),
                  &SingleAnalysis_Dictionary, isa_proxy, 0,
                  sizeof(::SingleAnalysis) );
      instance.SetDelete(&delete_SingleAnalysis);
      instance.SetDeleteArray(&deleteArray_SingleAnalysis);
      instance.SetDestructor(&destruct_SingleAnalysis);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::SingleAnalysis*)
   {
      return GenerateInitInstanceLocal((::SingleAnalysis*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::SingleAnalysis*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *SingleAnalysis_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::SingleAnalysis*)0x0)->GetClass();
      SingleAnalysis_TClassManip(theClass);
   return theClass;
   }

   static void SingleAnalysis_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ObservableGroup_Dictionary();
   static void ObservableGroup_TClassManip(TClass*);
   static void delete_ObservableGroup(void *p);
   static void deleteArray_ObservableGroup(void *p);
   static void destruct_ObservableGroup(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ObservableGroup*)
   {
      ::ObservableGroup *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ObservableGroup));
      static ::ROOT::TGenericClassInfo 
         instance("ObservableGroup", "InDetUpgradePerformanceAnalysis/ObservableGroup.h", 10,
                  typeid(::ObservableGroup), DefineBehavior(ptr, ptr),
                  &ObservableGroup_Dictionary, isa_proxy, 0,
                  sizeof(::ObservableGroup) );
      instance.SetDelete(&delete_ObservableGroup);
      instance.SetDeleteArray(&deleteArray_ObservableGroup);
      instance.SetDestructor(&destruct_ObservableGroup);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ObservableGroup*)
   {
      return GenerateInitInstanceLocal((::ObservableGroup*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ObservableGroup*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ObservableGroup_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ObservableGroup*)0x0)->GetClass();
      ObservableGroup_TClassManip(theClass);
   return theClass;
   }

   static void ObservableGroup_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *Observable_Dictionary();
   static void Observable_TClassManip(TClass*);
   static void delete_Observable(void *p);
   static void deleteArray_Observable(void *p);
   static void destruct_Observable(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Observable*)
   {
      ::Observable *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Observable));
      static ::ROOT::TGenericClassInfo 
         instance("Observable", "InDetUpgradePerformanceAnalysis/Observable.h", 32,
                  typeid(::Observable), DefineBehavior(ptr, ptr),
                  &Observable_Dictionary, isa_proxy, 0,
                  sizeof(::Observable) );
      instance.SetDelete(&delete_Observable);
      instance.SetDeleteArray(&deleteArray_Observable);
      instance.SetDestructor(&destruct_Observable);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Observable*)
   {
      return GenerateInitInstanceLocal((::Observable*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::Observable*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *Observable_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Observable*)0x0)->GetClass();
      Observable_TClassManip(theClass);
   return theClass;
   }

   static void Observable_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *SensorInfoBase_Dictionary();
   static void SensorInfoBase_TClassManip(TClass*);
   static void *new_SensorInfoBase(void *p = 0);
   static void *newArray_SensorInfoBase(Long_t size, void *p);
   static void delete_SensorInfoBase(void *p);
   static void deleteArray_SensorInfoBase(void *p);
   static void destruct_SensorInfoBase(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::SensorInfoBase*)
   {
      ::SensorInfoBase *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::SensorInfoBase));
      static ::ROOT::TGenericClassInfo 
         instance("SensorInfoBase", "InDetUpgradePerformanceAnalysis/SensorInfoBase.h", 9,
                  typeid(::SensorInfoBase), DefineBehavior(ptr, ptr),
                  &SensorInfoBase_Dictionary, isa_proxy, 0,
                  sizeof(::SensorInfoBase) );
      instance.SetNew(&new_SensorInfoBase);
      instance.SetNewArray(&newArray_SensorInfoBase);
      instance.SetDelete(&delete_SensorInfoBase);
      instance.SetDeleteArray(&deleteArray_SensorInfoBase);
      instance.SetDestructor(&destruct_SensorInfoBase);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::SensorInfoBase*)
   {
      return GenerateInitInstanceLocal((::SensorInfoBase*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::SensorInfoBase*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *SensorInfoBase_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::SensorInfoBase*)0x0)->GetClass();
      SensorInfoBase_TClassManip(theClass);
   return theClass;
   }

   static void SensorInfoBase_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DumpSensorInfo_Dictionary();
   static void DumpSensorInfo_TClassManip(TClass*);
   static void *new_DumpSensorInfo(void *p = 0);
   static void *newArray_DumpSensorInfo(Long_t size, void *p);
   static void delete_DumpSensorInfo(void *p);
   static void deleteArray_DumpSensorInfo(void *p);
   static void destruct_DumpSensorInfo(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DumpSensorInfo*)
   {
      ::DumpSensorInfo *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DumpSensorInfo));
      static ::ROOT::TGenericClassInfo 
         instance("DumpSensorInfo", "InDetUpgradePerformanceAnalysis/DumpSensorInfo.h", 7,
                  typeid(::DumpSensorInfo), DefineBehavior(ptr, ptr),
                  &DumpSensorInfo_Dictionary, isa_proxy, 0,
                  sizeof(::DumpSensorInfo) );
      instance.SetNew(&new_DumpSensorInfo);
      instance.SetNewArray(&newArray_DumpSensorInfo);
      instance.SetDelete(&delete_DumpSensorInfo);
      instance.SetDeleteArray(&deleteArray_DumpSensorInfo);
      instance.SetDestructor(&destruct_DumpSensorInfo);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DumpSensorInfo*)
   {
      return GenerateInitInstanceLocal((::DumpSensorInfo*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DumpSensorInfo*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DumpSensorInfo_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DumpSensorInfo*)0x0)->GetClass();
      DumpSensorInfo_TClassManip(theClass);
   return theClass;
   }

   static void DumpSensorInfo_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *TreeHouse_Dictionary();
   static void TreeHouse_TClassManip(TClass*);
   static void *new_TreeHouse(void *p = 0);
   static void *newArray_TreeHouse(Long_t size, void *p);
   static void delete_TreeHouse(void *p);
   static void deleteArray_TreeHouse(void *p);
   static void destruct_TreeHouse(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::TreeHouse*)
   {
      ::TreeHouse *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::TreeHouse));
      static ::ROOT::TGenericClassInfo 
         instance("TreeHouse", "InDetUpgradePerformanceAnalysis/TreeHouse.h", 15,
                  typeid(::TreeHouse), DefineBehavior(ptr, ptr),
                  &TreeHouse_Dictionary, isa_proxy, 0,
                  sizeof(::TreeHouse) );
      instance.SetNew(&new_TreeHouse);
      instance.SetNewArray(&newArray_TreeHouse);
      instance.SetDelete(&delete_TreeHouse);
      instance.SetDeleteArray(&deleteArray_TreeHouse);
      instance.SetDestructor(&destruct_TreeHouse);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::TreeHouse*)
   {
      return GenerateInitInstanceLocal((::TreeHouse*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::TreeHouse*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *TreeHouse_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::TreeHouse*)0x0)->GetClass();
      TreeHouse_TClassManip(theClass);
   return theClass;
   }

   static void TreeHouse_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *Cuts_Dictionary();
   static void Cuts_TClassManip(TClass*);
   static void delete_Cuts(void *p);
   static void deleteArray_Cuts(void *p);
   static void destruct_Cuts(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Cuts*)
   {
      ::Cuts *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Cuts));
      static ::ROOT::TGenericClassInfo 
         instance("Cuts", "InDetUpgradePerformanceAnalysis/Cuts.h", 10,
                  typeid(::Cuts), DefineBehavior(ptr, ptr),
                  &Cuts_Dictionary, isa_proxy, 0,
                  sizeof(::Cuts) );
      instance.SetDelete(&delete_Cuts);
      instance.SetDeleteArray(&deleteArray_Cuts);
      instance.SetDestructor(&destruct_Cuts);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Cuts*)
   {
      return GenerateInitInstanceLocal((::Cuts*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::Cuts*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *Cuts_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Cuts*)0x0)->GetClass();
      Cuts_TClassManip(theClass);
   return theClass;
   }

   static void Cuts_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *AllCuts_Dictionary();
   static void AllCuts_TClassManip(TClass*);
   static void *new_AllCuts(void *p = 0);
   static void *newArray_AllCuts(Long_t size, void *p);
   static void delete_AllCuts(void *p);
   static void deleteArray_AllCuts(void *p);
   static void destruct_AllCuts(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::AllCuts*)
   {
      ::AllCuts *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::AllCuts));
      static ::ROOT::TGenericClassInfo 
         instance("AllCuts", "InDetUpgradePerformanceAnalysis/AllCuts.h", 8,
                  typeid(::AllCuts), DefineBehavior(ptr, ptr),
                  &AllCuts_Dictionary, isa_proxy, 0,
                  sizeof(::AllCuts) );
      instance.SetNew(&new_AllCuts);
      instance.SetNewArray(&newArray_AllCuts);
      instance.SetDelete(&delete_AllCuts);
      instance.SetDeleteArray(&deleteArray_AllCuts);
      instance.SetDestructor(&destruct_AllCuts);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::AllCuts*)
   {
      return GenerateInitInstanceLocal((::AllCuts*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::AllCuts*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *AllCuts_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::AllCuts*)0x0)->GetClass();
      AllCuts_TClassManip(theClass);
   return theClass;
   }

   static void AllCuts_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *StandardCuts_Dictionary();
   static void StandardCuts_TClassManip(TClass*);
   static void *new_StandardCuts(void *p = 0);
   static void *newArray_StandardCuts(Long_t size, void *p);
   static void delete_StandardCuts(void *p);
   static void deleteArray_StandardCuts(void *p);
   static void destruct_StandardCuts(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::StandardCuts*)
   {
      ::StandardCuts *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::StandardCuts));
      static ::ROOT::TGenericClassInfo 
         instance("StandardCuts", "InDetUpgradePerformanceAnalysis/StandardCuts.h", 11,
                  typeid(::StandardCuts), DefineBehavior(ptr, ptr),
                  &StandardCuts_Dictionary, isa_proxy, 0,
                  sizeof(::StandardCuts) );
      instance.SetNew(&new_StandardCuts);
      instance.SetNewArray(&newArray_StandardCuts);
      instance.SetDelete(&delete_StandardCuts);
      instance.SetDeleteArray(&deleteArray_StandardCuts);
      instance.SetDestructor(&destruct_StandardCuts);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::StandardCuts*)
   {
      return GenerateInitInstanceLocal((::StandardCuts*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::StandardCuts*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *StandardCuts_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::StandardCuts*)0x0)->GetClass();
      StandardCuts_TClassManip(theClass);
   return theClass;
   }

   static void StandardCuts_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RangeDependentMinCutlEfloatcOfloatgR_Dictionary();
   static void RangeDependentMinCutlEfloatcOfloatgR_TClassManip(TClass*);
   static void delete_RangeDependentMinCutlEfloatcOfloatgR(void *p);
   static void deleteArray_RangeDependentMinCutlEfloatcOfloatgR(void *p);
   static void destruct_RangeDependentMinCutlEfloatcOfloatgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RangeDependentMinCut<float,float>*)
   {
      ::RangeDependentMinCut<float,float> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RangeDependentMinCut<float,float>));
      static ::ROOT::TGenericClassInfo 
         instance("RangeDependentMinCut<float,float>", "InDetUpgradePerformanceAnalysis/MinMaxCut.h", 30,
                  typeid(::RangeDependentMinCut<float,float>), DefineBehavior(ptr, ptr),
                  &RangeDependentMinCutlEfloatcOfloatgR_Dictionary, isa_proxy, 0,
                  sizeof(::RangeDependentMinCut<float,float>) );
      instance.SetDelete(&delete_RangeDependentMinCutlEfloatcOfloatgR);
      instance.SetDeleteArray(&deleteArray_RangeDependentMinCutlEfloatcOfloatgR);
      instance.SetDestructor(&destruct_RangeDependentMinCutlEfloatcOfloatgR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RangeDependentMinCut<float,float>*)
   {
      return GenerateInitInstanceLocal((::RangeDependentMinCut<float,float>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RangeDependentMinCut<float,float>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RangeDependentMinCutlEfloatcOfloatgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RangeDependentMinCut<float,float>*)0x0)->GetClass();
      RangeDependentMinCutlEfloatcOfloatgR_TClassManip(theClass);
   return theClass;
   }

   static void RangeDependentMinCutlEfloatcOfloatgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RangeDependentMinCutlEintcOfloatgR_Dictionary();
   static void RangeDependentMinCutlEintcOfloatgR_TClassManip(TClass*);
   static void delete_RangeDependentMinCutlEintcOfloatgR(void *p);
   static void deleteArray_RangeDependentMinCutlEintcOfloatgR(void *p);
   static void destruct_RangeDependentMinCutlEintcOfloatgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RangeDependentMinCut<int,float>*)
   {
      ::RangeDependentMinCut<int,float> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RangeDependentMinCut<int,float>));
      static ::ROOT::TGenericClassInfo 
         instance("RangeDependentMinCut<int,float>", "InDetUpgradePerformanceAnalysis/MinMaxCut.h", 30,
                  typeid(::RangeDependentMinCut<int,float>), DefineBehavior(ptr, ptr),
                  &RangeDependentMinCutlEintcOfloatgR_Dictionary, isa_proxy, 0,
                  sizeof(::RangeDependentMinCut<int,float>) );
      instance.SetDelete(&delete_RangeDependentMinCutlEintcOfloatgR);
      instance.SetDeleteArray(&deleteArray_RangeDependentMinCutlEintcOfloatgR);
      instance.SetDestructor(&destruct_RangeDependentMinCutlEintcOfloatgR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RangeDependentMinCut<int,float>*)
   {
      return GenerateInitInstanceLocal((::RangeDependentMinCut<int,float>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RangeDependentMinCut<int,float>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RangeDependentMinCutlEintcOfloatgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RangeDependentMinCut<int,float>*)0x0)->GetClass();
      RangeDependentMinCutlEintcOfloatgR_TClassManip(theClass);
   return theClass;
   }

   static void RangeDependentMinCutlEintcOfloatgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *MinCutlEintgR_Dictionary();
   static void MinCutlEintgR_TClassManip(TClass*);
   static void delete_MinCutlEintgR(void *p);
   static void deleteArray_MinCutlEintgR(void *p);
   static void destruct_MinCutlEintgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::MinCut<int>*)
   {
      ::MinCut<int> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::MinCut<int>));
      static ::ROOT::TGenericClassInfo 
         instance("MinCut<int>", "InDetUpgradePerformanceAnalysis/MinMaxCut.h", 10,
                  typeid(::MinCut<int>), DefineBehavior(ptr, ptr),
                  &MinCutlEintgR_Dictionary, isa_proxy, 0,
                  sizeof(::MinCut<int>) );
      instance.SetDelete(&delete_MinCutlEintgR);
      instance.SetDeleteArray(&deleteArray_MinCutlEintgR);
      instance.SetDestructor(&destruct_MinCutlEintgR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::MinCut<int>*)
   {
      return GenerateInitInstanceLocal((::MinCut<int>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::MinCut<int>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *MinCutlEintgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::MinCut<int>*)0x0)->GetClass();
      MinCutlEintgR_TClassManip(theClass);
   return theClass;
   }

   static void MinCutlEintgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *MinCutlEfloatgR_Dictionary();
   static void MinCutlEfloatgR_TClassManip(TClass*);
   static void delete_MinCutlEfloatgR(void *p);
   static void deleteArray_MinCutlEfloatgR(void *p);
   static void destruct_MinCutlEfloatgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::MinCut<float>*)
   {
      ::MinCut<float> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::MinCut<float>));
      static ::ROOT::TGenericClassInfo 
         instance("MinCut<float>", "InDetUpgradePerformanceAnalysis/MinMaxCut.h", 10,
                  typeid(::MinCut<float>), DefineBehavior(ptr, ptr),
                  &MinCutlEfloatgR_Dictionary, isa_proxy, 0,
                  sizeof(::MinCut<float>) );
      instance.SetDelete(&delete_MinCutlEfloatgR);
      instance.SetDeleteArray(&deleteArray_MinCutlEfloatgR);
      instance.SetDestructor(&destruct_MinCutlEfloatgR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::MinCut<float>*)
   {
      return GenerateInitInstanceLocal((::MinCut<float>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::MinCut<float>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *MinCutlEfloatgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::MinCut<float>*)0x0)->GetClass();
      MinCutlEfloatgR_TClassManip(theClass);
   return theClass;
   }

   static void MinCutlEfloatgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *MaxCutlEfloatgR_Dictionary();
   static void MaxCutlEfloatgR_TClassManip(TClass*);
   static void delete_MaxCutlEfloatgR(void *p);
   static void deleteArray_MaxCutlEfloatgR(void *p);
   static void destruct_MaxCutlEfloatgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::MaxCut<float>*)
   {
      ::MaxCut<float> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::MaxCut<float>));
      static ::ROOT::TGenericClassInfo 
         instance("MaxCut<float>", "InDetUpgradePerformanceAnalysis/MinMaxCut.h", 44,
                  typeid(::MaxCut<float>), DefineBehavior(ptr, ptr),
                  &MaxCutlEfloatgR_Dictionary, isa_proxy, 0,
                  sizeof(::MaxCut<float>) );
      instance.SetDelete(&delete_MaxCutlEfloatgR);
      instance.SetDeleteArray(&deleteArray_MaxCutlEfloatgR);
      instance.SetDestructor(&destruct_MaxCutlEfloatgR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::MaxCut<float>*)
   {
      return GenerateInitInstanceLocal((::MaxCut<float>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::MaxCut<float>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *MaxCutlEfloatgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::MaxCut<float>*)0x0)->GetClass();
      MaxCutlEfloatgR_TClassManip(theClass);
   return theClass;
   }

   static void MaxCutlEfloatgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *MaxCutlEintgR_Dictionary();
   static void MaxCutlEintgR_TClassManip(TClass*);
   static void delete_MaxCutlEintgR(void *p);
   static void deleteArray_MaxCutlEintgR(void *p);
   static void destruct_MaxCutlEintgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::MaxCut<int>*)
   {
      ::MaxCut<int> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::MaxCut<int>));
      static ::ROOT::TGenericClassInfo 
         instance("MaxCut<int>", "InDetUpgradePerformanceAnalysis/MinMaxCut.h", 44,
                  typeid(::MaxCut<int>), DefineBehavior(ptr, ptr),
                  &MaxCutlEintgR_Dictionary, isa_proxy, 0,
                  sizeof(::MaxCut<int>) );
      instance.SetDelete(&delete_MaxCutlEintgR);
      instance.SetDeleteArray(&deleteArray_MaxCutlEintgR);
      instance.SetDestructor(&destruct_MaxCutlEintgR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::MaxCut<int>*)
   {
      return GenerateInitInstanceLocal((::MaxCut<int>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::MaxCut<int>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *MaxCutlEintgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::MaxCut<int>*)0x0)->GetClass();
      MaxCutlEintgR_TClassManip(theClass);
   return theClass;
   }

   static void MaxCutlEintgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RangeCutlEintgR_Dictionary();
   static void RangeCutlEintgR_TClassManip(TClass*);
   static void delete_RangeCutlEintgR(void *p);
   static void deleteArray_RangeCutlEintgR(void *p);
   static void destruct_RangeCutlEintgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RangeCut<int>*)
   {
      ::RangeCut<int> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RangeCut<int>));
      static ::ROOT::TGenericClassInfo 
         instance("RangeCut<int>", "InDetUpgradePerformanceAnalysis/MinMaxCut.h", 54,
                  typeid(::RangeCut<int>), DefineBehavior(ptr, ptr),
                  &RangeCutlEintgR_Dictionary, isa_proxy, 0,
                  sizeof(::RangeCut<int>) );
      instance.SetDelete(&delete_RangeCutlEintgR);
      instance.SetDeleteArray(&deleteArray_RangeCutlEintgR);
      instance.SetDestructor(&destruct_RangeCutlEintgR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RangeCut<int>*)
   {
      return GenerateInitInstanceLocal((::RangeCut<int>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RangeCut<int>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RangeCutlEintgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RangeCut<int>*)0x0)->GetClass();
      RangeCutlEintgR_TClassManip(theClass);
   return theClass;
   }

   static void RangeCutlEintgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RangeCutlEfloatgR_Dictionary();
   static void RangeCutlEfloatgR_TClassManip(TClass*);
   static void delete_RangeCutlEfloatgR(void *p);
   static void deleteArray_RangeCutlEfloatgR(void *p);
   static void destruct_RangeCutlEfloatgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RangeCut<float>*)
   {
      ::RangeCut<float> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RangeCut<float>));
      static ::ROOT::TGenericClassInfo 
         instance("RangeCut<float>", "InDetUpgradePerformanceAnalysis/MinMaxCut.h", 54,
                  typeid(::RangeCut<float>), DefineBehavior(ptr, ptr),
                  &RangeCutlEfloatgR_Dictionary, isa_proxy, 0,
                  sizeof(::RangeCut<float>) );
      instance.SetDelete(&delete_RangeCutlEfloatgR);
      instance.SetDeleteArray(&deleteArray_RangeCutlEfloatgR);
      instance.SetDestructor(&destruct_RangeCutlEfloatgR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RangeCut<float>*)
   {
      return GenerateInitInstanceLocal((::RangeCut<float>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RangeCut<float>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RangeCutlEfloatgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RangeCut<float>*)0x0)->GetClass();
      RangeCutlEfloatgR_TClassManip(theClass);
   return theClass;
   }

   static void RangeCutlEfloatgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RangeProductCutlEfloatgR_Dictionary();
   static void RangeProductCutlEfloatgR_TClassManip(TClass*);
   static void delete_RangeProductCutlEfloatgR(void *p);
   static void deleteArray_RangeProductCutlEfloatgR(void *p);
   static void destruct_RangeProductCutlEfloatgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RangeProductCut<float>*)
   {
      ::RangeProductCut<float> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RangeProductCut<float>));
      static ::ROOT::TGenericClassInfo 
         instance("RangeProductCut<float>", "InDetUpgradePerformanceAnalysis/MinMaxCut.h", 65,
                  typeid(::RangeProductCut<float>), DefineBehavior(ptr, ptr),
                  &RangeProductCutlEfloatgR_Dictionary, isa_proxy, 0,
                  sizeof(::RangeProductCut<float>) );
      instance.SetDelete(&delete_RangeProductCutlEfloatgR);
      instance.SetDeleteArray(&deleteArray_RangeProductCutlEfloatgR);
      instance.SetDestructor(&destruct_RangeProductCutlEfloatgR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RangeProductCut<float>*)
   {
      return GenerateInitInstanceLocal((::RangeProductCut<float>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RangeProductCut<float>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RangeProductCutlEfloatgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RangeProductCut<float>*)0x0)->GetClass();
      RangeProductCutlEfloatgR_TClassManip(theClass);
   return theClass;
   }

   static void RangeProductCutlEfloatgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DistributionlEintgR_Dictionary();
   static void DistributionlEintgR_TClassManip(TClass*);
   static void delete_DistributionlEintgR(void *p);
   static void deleteArray_DistributionlEintgR(void *p);
   static void destruct_DistributionlEintgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Distribution<int>*)
   {
      ::Distribution<int> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Distribution<int>));
      static ::ROOT::TGenericClassInfo 
         instance("Distribution<int>", "InDetUpgradePerformanceAnalysis/Distribution.h", 18,
                  typeid(::Distribution<int>), DefineBehavior(ptr, ptr),
                  &DistributionlEintgR_Dictionary, isa_proxy, 0,
                  sizeof(::Distribution<int>) );
      instance.SetDelete(&delete_DistributionlEintgR);
      instance.SetDeleteArray(&deleteArray_DistributionlEintgR);
      instance.SetDestructor(&destruct_DistributionlEintgR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Distribution<int>*)
   {
      return GenerateInitInstanceLocal((::Distribution<int>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::Distribution<int>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DistributionlEintgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Distribution<int>*)0x0)->GetClass();
      DistributionlEintgR_TClassManip(theClass);
   return theClass;
   }

   static void DistributionlEintgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DistributionlEfloatgR_Dictionary();
   static void DistributionlEfloatgR_TClassManip(TClass*);
   static void delete_DistributionlEfloatgR(void *p);
   static void deleteArray_DistributionlEfloatgR(void *p);
   static void destruct_DistributionlEfloatgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Distribution<float>*)
   {
      ::Distribution<float> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Distribution<float>));
      static ::ROOT::TGenericClassInfo 
         instance("Distribution<float>", "InDetUpgradePerformanceAnalysis/Distribution.h", 18,
                  typeid(::Distribution<float>), DefineBehavior(ptr, ptr),
                  &DistributionlEfloatgR_Dictionary, isa_proxy, 0,
                  sizeof(::Distribution<float>) );
      instance.SetDelete(&delete_DistributionlEfloatgR);
      instance.SetDeleteArray(&deleteArray_DistributionlEfloatgR);
      instance.SetDestructor(&destruct_DistributionlEfloatgR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Distribution<float>*)
   {
      return GenerateInitInstanceLocal((::Distribution<float>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::Distribution<float>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DistributionlEfloatgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Distribution<float>*)0x0)->GetClass();
      DistributionlEfloatgR_TClassManip(theClass);
   return theClass;
   }

   static void DistributionlEfloatgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *SelectorlEintgR_Dictionary();
   static void SelectorlEintgR_TClassManip(TClass*);
   static void delete_SelectorlEintgR(void *p);
   static void deleteArray_SelectorlEintgR(void *p);
   static void destruct_SelectorlEintgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Selector<int>*)
   {
      ::Selector<int> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Selector<int>));
      static ::ROOT::TGenericClassInfo 
         instance("Selector<int>", "InDetUpgradePerformanceAnalysis/Selector.h", 8,
                  typeid(::Selector<int>), DefineBehavior(ptr, ptr),
                  &SelectorlEintgR_Dictionary, isa_proxy, 0,
                  sizeof(::Selector<int>) );
      instance.SetDelete(&delete_SelectorlEintgR);
      instance.SetDeleteArray(&deleteArray_SelectorlEintgR);
      instance.SetDestructor(&destruct_SelectorlEintgR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Selector<int>*)
   {
      return GenerateInitInstanceLocal((::Selector<int>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::Selector<int>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *SelectorlEintgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Selector<int>*)0x0)->GetClass();
      SelectorlEintgR_TClassManip(theClass);
   return theClass;
   }

   static void SelectorlEintgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *SelectorlEfloatgR_Dictionary();
   static void SelectorlEfloatgR_TClassManip(TClass*);
   static void delete_SelectorlEfloatgR(void *p);
   static void deleteArray_SelectorlEfloatgR(void *p);
   static void destruct_SelectorlEfloatgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Selector<float>*)
   {
      ::Selector<float> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Selector<float>));
      static ::ROOT::TGenericClassInfo 
         instance("Selector<float>", "InDetUpgradePerformanceAnalysis/Selector.h", 8,
                  typeid(::Selector<float>), DefineBehavior(ptr, ptr),
                  &SelectorlEfloatgR_Dictionary, isa_proxy, 0,
                  sizeof(::Selector<float>) );
      instance.SetDelete(&delete_SelectorlEfloatgR);
      instance.SetDeleteArray(&deleteArray_SelectorlEfloatgR);
      instance.SetDestructor(&destruct_SelectorlEfloatgR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Selector<float>*)
   {
      return GenerateInitInstanceLocal((::Selector<float>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::Selector<float>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *SelectorlEfloatgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Selector<float>*)0x0)->GetClass();
      SelectorlEfloatgR_TClassManip(theClass);
   return theClass;
   }

   static void SelectorlEfloatgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *AbsSelectorlEintgR_Dictionary();
   static void AbsSelectorlEintgR_TClassManip(TClass*);
   static void delete_AbsSelectorlEintgR(void *p);
   static void deleteArray_AbsSelectorlEintgR(void *p);
   static void destruct_AbsSelectorlEintgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::AbsSelector<int>*)
   {
      ::AbsSelector<int> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::AbsSelector<int>));
      static ::ROOT::TGenericClassInfo 
         instance("AbsSelector<int>", "InDetUpgradePerformanceAnalysis/Selector.h", 18,
                  typeid(::AbsSelector<int>), DefineBehavior(ptr, ptr),
                  &AbsSelectorlEintgR_Dictionary, isa_proxy, 0,
                  sizeof(::AbsSelector<int>) );
      instance.SetDelete(&delete_AbsSelectorlEintgR);
      instance.SetDeleteArray(&deleteArray_AbsSelectorlEintgR);
      instance.SetDestructor(&destruct_AbsSelectorlEintgR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::AbsSelector<int>*)
   {
      return GenerateInitInstanceLocal((::AbsSelector<int>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::AbsSelector<int>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *AbsSelectorlEintgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::AbsSelector<int>*)0x0)->GetClass();
      AbsSelectorlEintgR_TClassManip(theClass);
   return theClass;
   }

   static void AbsSelectorlEintgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *AbsSelectorlEfloatgR_Dictionary();
   static void AbsSelectorlEfloatgR_TClassManip(TClass*);
   static void delete_AbsSelectorlEfloatgR(void *p);
   static void deleteArray_AbsSelectorlEfloatgR(void *p);
   static void destruct_AbsSelectorlEfloatgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::AbsSelector<float>*)
   {
      ::AbsSelector<float> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::AbsSelector<float>));
      static ::ROOT::TGenericClassInfo 
         instance("AbsSelector<float>", "InDetUpgradePerformanceAnalysis/Selector.h", 18,
                  typeid(::AbsSelector<float>), DefineBehavior(ptr, ptr),
                  &AbsSelectorlEfloatgR_Dictionary, isa_proxy, 0,
                  sizeof(::AbsSelector<float>) );
      instance.SetDelete(&delete_AbsSelectorlEfloatgR);
      instance.SetDeleteArray(&deleteArray_AbsSelectorlEfloatgR);
      instance.SetDestructor(&destruct_AbsSelectorlEfloatgR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::AbsSelector<float>*)
   {
      return GenerateInitInstanceLocal((::AbsSelector<float>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::AbsSelector<float>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *AbsSelectorlEfloatgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::AbsSelector<float>*)0x0)->GetClass();
      AbsSelectorlEfloatgR_TClassManip(theClass);
   return theClass;
   }

   static void AbsSelectorlEfloatgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *MinimumHitsCut_Dictionary();
   static void MinimumHitsCut_TClassManip(TClass*);
   static void delete_MinimumHitsCut(void *p);
   static void deleteArray_MinimumHitsCut(void *p);
   static void destruct_MinimumHitsCut(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::MinimumHitsCut*)
   {
      ::MinimumHitsCut *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::MinimumHitsCut));
      static ::ROOT::TGenericClassInfo 
         instance("MinimumHitsCut", "InDetUpgradePerformanceAnalysis/MinimumHitsCut.h", 8,
                  typeid(::MinimumHitsCut), DefineBehavior(ptr, ptr),
                  &MinimumHitsCut_Dictionary, isa_proxy, 0,
                  sizeof(::MinimumHitsCut) );
      instance.SetDelete(&delete_MinimumHitsCut);
      instance.SetDeleteArray(&deleteArray_MinimumHitsCut);
      instance.SetDestructor(&destruct_MinimumHitsCut);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::MinimumHitsCut*)
   {
      return GenerateInitInstanceLocal((::MinimumHitsCut*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::MinimumHitsCut*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *MinimumHitsCut_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::MinimumHitsCut*)0x0)->GetClass();
      MinimumHitsCut_TClassManip(theClass);
   return theClass;
   }

   static void MinimumHitsCut_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *AbsSelectorWithIndex_Dictionary();
   static void AbsSelectorWithIndex_TClassManip(TClass*);
   static void delete_AbsSelectorWithIndex(void *p);
   static void deleteArray_AbsSelectorWithIndex(void *p);
   static void destruct_AbsSelectorWithIndex(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::AbsSelectorWithIndex*)
   {
      ::AbsSelectorWithIndex *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::AbsSelectorWithIndex));
      static ::ROOT::TGenericClassInfo 
         instance("AbsSelectorWithIndex", "InDetUpgradePerformanceAnalysis/SelectorWithIndex.h", 17,
                  typeid(::AbsSelectorWithIndex), DefineBehavior(ptr, ptr),
                  &AbsSelectorWithIndex_Dictionary, isa_proxy, 0,
                  sizeof(::AbsSelectorWithIndex) );
      instance.SetDelete(&delete_AbsSelectorWithIndex);
      instance.SetDeleteArray(&deleteArray_AbsSelectorWithIndex);
      instance.SetDestructor(&destruct_AbsSelectorWithIndex);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::AbsSelectorWithIndex*)
   {
      return GenerateInitInstanceLocal((::AbsSelectorWithIndex*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::AbsSelectorWithIndex*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *AbsSelectorWithIndex_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::AbsSelectorWithIndex*)0x0)->GetClass();
      AbsSelectorWithIndex_TClassManip(theClass);
   return theClass;
   }

   static void AbsSelectorWithIndex_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *RangeCutWithIndex_Dictionary();
   static void RangeCutWithIndex_TClassManip(TClass*);
   static void delete_RangeCutWithIndex(void *p);
   static void deleteArray_RangeCutWithIndex(void *p);
   static void destruct_RangeCutWithIndex(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RangeCutWithIndex*)
   {
      ::RangeCutWithIndex *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::RangeCutWithIndex));
      static ::ROOT::TGenericClassInfo 
         instance("RangeCutWithIndex", "InDetUpgradePerformanceAnalysis/MinMaxCutWithIndex.h", 28,
                  typeid(::RangeCutWithIndex), DefineBehavior(ptr, ptr),
                  &RangeCutWithIndex_Dictionary, isa_proxy, 0,
                  sizeof(::RangeCutWithIndex) );
      instance.SetDelete(&delete_RangeCutWithIndex);
      instance.SetDeleteArray(&deleteArray_RangeCutWithIndex);
      instance.SetDestructor(&destruct_RangeCutWithIndex);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RangeCutWithIndex*)
   {
      return GenerateInitInstanceLocal((::RangeCutWithIndex*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::RangeCutWithIndex*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *RangeCutWithIndex_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::RangeCutWithIndex*)0x0)->GetClass();
      RangeCutWithIndex_TClassManip(theClass);
   return theClass;
   }

   static void RangeCutWithIndex_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *Efficiency_Dictionary();
   static void Efficiency_TClassManip(TClass*);
   static void *new_Efficiency(void *p = 0);
   static void *newArray_Efficiency(Long_t size, void *p);
   static void delete_Efficiency(void *p);
   static void deleteArray_Efficiency(void *p);
   static void destruct_Efficiency(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Efficiency*)
   {
      ::Efficiency *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Efficiency));
      static ::ROOT::TGenericClassInfo 
         instance("Efficiency", "InDetUpgradePerformanceAnalysis/Efficiency.h", 22,
                  typeid(::Efficiency), DefineBehavior(ptr, ptr),
                  &Efficiency_Dictionary, isa_proxy, 0,
                  sizeof(::Efficiency) );
      instance.SetNew(&new_Efficiency);
      instance.SetNewArray(&newArray_Efficiency);
      instance.SetDelete(&delete_Efficiency);
      instance.SetDeleteArray(&deleteArray_Efficiency);
      instance.SetDestructor(&destruct_Efficiency);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Efficiency*)
   {
      return GenerateInitInstanceLocal((::Efficiency*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::Efficiency*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *Efficiency_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Efficiency*)0x0)->GetClass();
      Efficiency_TClassManip(theClass);
   return theClass;
   }

   static void Efficiency_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *FakeRate_Dictionary();
   static void FakeRate_TClassManip(TClass*);
   static void *new_FakeRate(void *p = 0);
   static void *newArray_FakeRate(Long_t size, void *p);
   static void delete_FakeRate(void *p);
   static void deleteArray_FakeRate(void *p);
   static void destruct_FakeRate(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::FakeRate*)
   {
      ::FakeRate *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::FakeRate));
      static ::ROOT::TGenericClassInfo 
         instance("FakeRate", "InDetUpgradePerformanceAnalysis/FakeRate.h", 20,
                  typeid(::FakeRate), DefineBehavior(ptr, ptr),
                  &FakeRate_Dictionary, isa_proxy, 0,
                  sizeof(::FakeRate) );
      instance.SetNew(&new_FakeRate);
      instance.SetNewArray(&newArray_FakeRate);
      instance.SetDelete(&delete_FakeRate);
      instance.SetDeleteArray(&deleteArray_FakeRate);
      instance.SetDestructor(&destruct_FakeRate);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::FakeRate*)
   {
      return GenerateInitInstanceLocal((::FakeRate*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::FakeRate*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *FakeRate_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::FakeRate*)0x0)->GetClass();
      FakeRate_TClassManip(theClass);
   return theClass;
   }

   static void FakeRate_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *TrackRatio_Dictionary();
   static void TrackRatio_TClassManip(TClass*);
   static void delete_TrackRatio(void *p);
   static void deleteArray_TrackRatio(void *p);
   static void destruct_TrackRatio(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::TrackRatio*)
   {
      ::TrackRatio *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::TrackRatio));
      static ::ROOT::TGenericClassInfo 
         instance("TrackRatio", "InDetUpgradePerformanceAnalysis/TrackRatio.h", 32,
                  typeid(::TrackRatio), DefineBehavior(ptr, ptr),
                  &TrackRatio_Dictionary, isa_proxy, 0,
                  sizeof(::TrackRatio) );
      instance.SetDelete(&delete_TrackRatio);
      instance.SetDeleteArray(&deleteArray_TrackRatio);
      instance.SetDestructor(&destruct_TrackRatio);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::TrackRatio*)
   {
      return GenerateInitInstanceLocal((::TrackRatio*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::TrackRatio*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *TrackRatio_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::TrackRatio*)0x0)->GetClass();
      TrackRatio_TClassManip(theClass);
   return theClass;
   }

   static void TrackRatio_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *Binning_Dictionary();
   static void Binning_TClassManip(TClass*);
   static void *new_Binning(void *p = 0);
   static void *newArray_Binning(Long_t size, void *p);
   static void delete_Binning(void *p);
   static void deleteArray_Binning(void *p);
   static void destruct_Binning(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Binning*)
   {
      ::Binning *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Binning));
      static ::ROOT::TGenericClassInfo 
         instance("Binning", "InDetUpgradePerformanceAnalysis/Binning.h", 9,
                  typeid(::Binning), DefineBehavior(ptr, ptr),
                  &Binning_Dictionary, isa_proxy, 0,
                  sizeof(::Binning) );
      instance.SetNew(&new_Binning);
      instance.SetNewArray(&newArray_Binning);
      instance.SetDelete(&delete_Binning);
      instance.SetDeleteArray(&deleteArray_Binning);
      instance.SetDestructor(&destruct_Binning);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Binning*)
   {
      return GenerateInitInstanceLocal((::Binning*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::Binning*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *Binning_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Binning*)0x0)->GetClass();
      Binning_TClassManip(theClass);
   return theClass;
   }

   static void Binning_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *CalculatorlEfloatgR_Dictionary();
   static void CalculatorlEfloatgR_TClassManip(TClass*);
   static void delete_CalculatorlEfloatgR(void *p);
   static void deleteArray_CalculatorlEfloatgR(void *p);
   static void destruct_CalculatorlEfloatgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Calculator<float>*)
   {
      ::Calculator<float> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Calculator<float>));
      static ::ROOT::TGenericClassInfo 
         instance("Calculator<float>", "InDetUpgradePerformanceAnalysis/Calculator.h", 9,
                  typeid(::Calculator<float>), DefineBehavior(ptr, ptr),
                  &CalculatorlEfloatgR_Dictionary, isa_proxy, 0,
                  sizeof(::Calculator<float>) );
      instance.SetDelete(&delete_CalculatorlEfloatgR);
      instance.SetDeleteArray(&deleteArray_CalculatorlEfloatgR);
      instance.SetDestructor(&destruct_CalculatorlEfloatgR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Calculator<float>*)
   {
      return GenerateInitInstanceLocal((::Calculator<float>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::Calculator<float>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *CalculatorlEfloatgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Calculator<float>*)0x0)->GetClass();
      CalculatorlEfloatgR_TClassManip(theClass);
   return theClass;
   }

   static void CalculatorlEfloatgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *CalculatorlEintgR_Dictionary();
   static void CalculatorlEintgR_TClassManip(TClass*);
   static void delete_CalculatorlEintgR(void *p);
   static void deleteArray_CalculatorlEintgR(void *p);
   static void destruct_CalculatorlEintgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Calculator<int>*)
   {
      ::Calculator<int> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Calculator<int>));
      static ::ROOT::TGenericClassInfo 
         instance("Calculator<int>", "InDetUpgradePerformanceAnalysis/Calculator.h", 9,
                  typeid(::Calculator<int>), DefineBehavior(ptr, ptr),
                  &CalculatorlEintgR_Dictionary, isa_proxy, 0,
                  sizeof(::Calculator<int>) );
      instance.SetDelete(&delete_CalculatorlEintgR);
      instance.SetDeleteArray(&deleteArray_CalculatorlEintgR);
      instance.SetDestructor(&destruct_CalculatorlEintgR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Calculator<int>*)
   {
      return GenerateInitInstanceLocal((::Calculator<int>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::Calculator<int>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *CalculatorlEintgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Calculator<int>*)0x0)->GetClass();
      CalculatorlEintgR_TClassManip(theClass);
   return theClass;
   }

   static void CalculatorlEintgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *AdditionlEintgR_Dictionary();
   static void AdditionlEintgR_TClassManip(TClass*);
   static void delete_AdditionlEintgR(void *p);
   static void deleteArray_AdditionlEintgR(void *p);
   static void destruct_AdditionlEintgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Addition<int>*)
   {
      ::Addition<int> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Addition<int>));
      static ::ROOT::TGenericClassInfo 
         instance("Addition<int>", "InDetUpgradePerformanceAnalysis/Addition.h", 7,
                  typeid(::Addition<int>), DefineBehavior(ptr, ptr),
                  &AdditionlEintgR_Dictionary, isa_proxy, 0,
                  sizeof(::Addition<int>) );
      instance.SetDelete(&delete_AdditionlEintgR);
      instance.SetDeleteArray(&deleteArray_AdditionlEintgR);
      instance.SetDestructor(&destruct_AdditionlEintgR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Addition<int>*)
   {
      return GenerateInitInstanceLocal((::Addition<int>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::Addition<int>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *AdditionlEintgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Addition<int>*)0x0)->GetClass();
      AdditionlEintgR_TClassManip(theClass);
   return theClass;
   }

   static void AdditionlEintgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DerivedBranchlEfloatgR_Dictionary();
   static void DerivedBranchlEfloatgR_TClassManip(TClass*);
   static void delete_DerivedBranchlEfloatgR(void *p);
   static void deleteArray_DerivedBranchlEfloatgR(void *p);
   static void destruct_DerivedBranchlEfloatgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DerivedBranch<float>*)
   {
      ::DerivedBranch<float> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DerivedBranch<float>));
      static ::ROOT::TGenericClassInfo 
         instance("DerivedBranch<float>", "InDetUpgradePerformanceAnalysis/DerivedBranch.h", 12,
                  typeid(::DerivedBranch<float>), DefineBehavior(ptr, ptr),
                  &DerivedBranchlEfloatgR_Dictionary, isa_proxy, 0,
                  sizeof(::DerivedBranch<float>) );
      instance.SetDelete(&delete_DerivedBranchlEfloatgR);
      instance.SetDeleteArray(&deleteArray_DerivedBranchlEfloatgR);
      instance.SetDestructor(&destruct_DerivedBranchlEfloatgR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DerivedBranch<float>*)
   {
      return GenerateInitInstanceLocal((::DerivedBranch<float>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DerivedBranch<float>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DerivedBranchlEfloatgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DerivedBranch<float>*)0x0)->GetClass();
      DerivedBranchlEfloatgR_TClassManip(theClass);
   return theClass;
   }

   static void DerivedBranchlEfloatgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *AdditionlEfloatgR_Dictionary();
   static void AdditionlEfloatgR_TClassManip(TClass*);
   static void delete_AdditionlEfloatgR(void *p);
   static void deleteArray_AdditionlEfloatgR(void *p);
   static void destruct_AdditionlEfloatgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Addition<float>*)
   {
      ::Addition<float> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Addition<float>));
      static ::ROOT::TGenericClassInfo 
         instance("Addition<float>", "InDetUpgradePerformanceAnalysis/Addition.h", 7,
                  typeid(::Addition<float>), DefineBehavior(ptr, ptr),
                  &AdditionlEfloatgR_Dictionary, isa_proxy, 0,
                  sizeof(::Addition<float>) );
      instance.SetDelete(&delete_AdditionlEfloatgR);
      instance.SetDeleteArray(&deleteArray_AdditionlEfloatgR);
      instance.SetDestructor(&destruct_AdditionlEfloatgR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Addition<float>*)
   {
      return GenerateInitInstanceLocal((::Addition<float>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::Addition<float>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *AdditionlEfloatgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Addition<float>*)0x0)->GetClass();
      AdditionlEfloatgR_TClassManip(theClass);
   return theClass;
   }

   static void AdditionlEfloatgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ResolutionObservablelEfloatgR_Dictionary();
   static void ResolutionObservablelEfloatgR_TClassManip(TClass*);
   static void delete_ResolutionObservablelEfloatgR(void *p);
   static void deleteArray_ResolutionObservablelEfloatgR(void *p);
   static void destruct_ResolutionObservablelEfloatgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ResolutionObservable<float>*)
   {
      ::ResolutionObservable<float> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ResolutionObservable<float>));
      static ::ROOT::TGenericClassInfo 
         instance("ResolutionObservable<float>", "InDetUpgradePerformanceAnalysis/ResolutionObservable.h", 27,
                  typeid(::ResolutionObservable<float>), DefineBehavior(ptr, ptr),
                  &ResolutionObservablelEfloatgR_Dictionary, isa_proxy, 0,
                  sizeof(::ResolutionObservable<float>) );
      instance.SetDelete(&delete_ResolutionObservablelEfloatgR);
      instance.SetDeleteArray(&deleteArray_ResolutionObservablelEfloatgR);
      instance.SetDestructor(&destruct_ResolutionObservablelEfloatgR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ResolutionObservable<float>*)
   {
      return GenerateInitInstanceLocal((::ResolutionObservable<float>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ResolutionObservable<float>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ResolutionObservablelEfloatgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ResolutionObservable<float>*)0x0)->GetClass();
      ResolutionObservablelEfloatgR_TClassManip(theClass);
   return theClass;
   }

   static void ResolutionObservablelEfloatgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *ResolutionObservablelEintgR_Dictionary();
   static void ResolutionObservablelEintgR_TClassManip(TClass*);
   static void delete_ResolutionObservablelEintgR(void *p);
   static void deleteArray_ResolutionObservablelEintgR(void *p);
   static void destruct_ResolutionObservablelEintgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::ResolutionObservable<int>*)
   {
      ::ResolutionObservable<int> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::ResolutionObservable<int>));
      static ::ROOT::TGenericClassInfo 
         instance("ResolutionObservable<int>", "InDetUpgradePerformanceAnalysis/ResolutionObservable.h", 27,
                  typeid(::ResolutionObservable<int>), DefineBehavior(ptr, ptr),
                  &ResolutionObservablelEintgR_Dictionary, isa_proxy, 0,
                  sizeof(::ResolutionObservable<int>) );
      instance.SetDelete(&delete_ResolutionObservablelEintgR);
      instance.SetDeleteArray(&deleteArray_ResolutionObservablelEintgR);
      instance.SetDestructor(&destruct_ResolutionObservablelEintgR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::ResolutionObservable<int>*)
   {
      return GenerateInitInstanceLocal((::ResolutionObservable<int>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::ResolutionObservable<int>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *ResolutionObservablelEintgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::ResolutionObservable<int>*)0x0)->GetClass();
      ResolutionObservablelEintgR_TClassManip(theClass);
   return theClass;
   }

   static void ResolutionObservablelEintgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DiffObservablelEfloatgR_Dictionary();
   static void DiffObservablelEfloatgR_TClassManip(TClass*);
   static void delete_DiffObservablelEfloatgR(void *p);
   static void deleteArray_DiffObservablelEfloatgR(void *p);
   static void destruct_DiffObservablelEfloatgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DiffObservable<float>*)
   {
      ::DiffObservable<float> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DiffObservable<float>));
      static ::ROOT::TGenericClassInfo 
         instance("DiffObservable<float>", "InDetUpgradePerformanceAnalysis/DiffObservable.h", 7,
                  typeid(::DiffObservable<float>), DefineBehavior(ptr, ptr),
                  &DiffObservablelEfloatgR_Dictionary, isa_proxy, 0,
                  sizeof(::DiffObservable<float>) );
      instance.SetDelete(&delete_DiffObservablelEfloatgR);
      instance.SetDeleteArray(&deleteArray_DiffObservablelEfloatgR);
      instance.SetDestructor(&destruct_DiffObservablelEfloatgR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DiffObservable<float>*)
   {
      return GenerateInitInstanceLocal((::DiffObservable<float>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DiffObservable<float>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DiffObservablelEfloatgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DiffObservable<float>*)0x0)->GetClass();
      DiffObservablelEfloatgR_TClassManip(theClass);
   return theClass;
   }

   static void DiffObservablelEfloatgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *PullObservablelEfloatgR_Dictionary();
   static void PullObservablelEfloatgR_TClassManip(TClass*);
   static void delete_PullObservablelEfloatgR(void *p);
   static void deleteArray_PullObservablelEfloatgR(void *p);
   static void destruct_PullObservablelEfloatgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::PullObservable<float>*)
   {
      ::PullObservable<float> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::PullObservable<float>));
      static ::ROOT::TGenericClassInfo 
         instance("PullObservable<float>", "InDetUpgradePerformanceAnalysis/PullObservable.h", 8,
                  typeid(::PullObservable<float>), DefineBehavior(ptr, ptr),
                  &PullObservablelEfloatgR_Dictionary, isa_proxy, 0,
                  sizeof(::PullObservable<float>) );
      instance.SetDelete(&delete_PullObservablelEfloatgR);
      instance.SetDeleteArray(&deleteArray_PullObservablelEfloatgR);
      instance.SetDestructor(&destruct_PullObservablelEfloatgR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::PullObservable<float>*)
   {
      return GenerateInitInstanceLocal((::PullObservable<float>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::PullObservable<float>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *PullObservablelEfloatgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::PullObservable<float>*)0x0)->GetClass();
      PullObservablelEfloatgR_TClassManip(theClass);
   return theClass;
   }

   static void PullObservablelEfloatgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *Z0SinThetaObslEfloatgR_Dictionary();
   static void Z0SinThetaObslEfloatgR_TClassManip(TClass*);
   static void delete_Z0SinThetaObslEfloatgR(void *p);
   static void deleteArray_Z0SinThetaObslEfloatgR(void *p);
   static void destruct_Z0SinThetaObslEfloatgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Z0SinThetaObs<float>*)
   {
      ::Z0SinThetaObs<float> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Z0SinThetaObs<float>));
      static ::ROOT::TGenericClassInfo 
         instance("Z0SinThetaObs<float>", "InDetUpgradePerformanceAnalysis/Z0SinThetaObs.h", 9,
                  typeid(::Z0SinThetaObs<float>), DefineBehavior(ptr, ptr),
                  &Z0SinThetaObslEfloatgR_Dictionary, isa_proxy, 0,
                  sizeof(::Z0SinThetaObs<float>) );
      instance.SetDelete(&delete_Z0SinThetaObslEfloatgR);
      instance.SetDeleteArray(&deleteArray_Z0SinThetaObslEfloatgR);
      instance.SetDestructor(&destruct_Z0SinThetaObslEfloatgR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Z0SinThetaObs<float>*)
   {
      return GenerateInitInstanceLocal((::Z0SinThetaObs<float>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::Z0SinThetaObs<float>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *Z0SinThetaObslEfloatgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Z0SinThetaObs<float>*)0x0)->GetClass();
      Z0SinThetaObslEfloatgR_TClassManip(theClass);
   return theClass;
   }

   static void Z0SinThetaObslEfloatgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *QOverPtObslEfloatgR_Dictionary();
   static void QOverPtObslEfloatgR_TClassManip(TClass*);
   static void delete_QOverPtObslEfloatgR(void *p);
   static void deleteArray_QOverPtObslEfloatgR(void *p);
   static void destruct_QOverPtObslEfloatgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::QOverPtObs<float>*)
   {
      ::QOverPtObs<float> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::QOverPtObs<float>));
      static ::ROOT::TGenericClassInfo 
         instance("QOverPtObs<float>", "InDetUpgradePerformanceAnalysis/QOverPtObs.h", 8,
                  typeid(::QOverPtObs<float>), DefineBehavior(ptr, ptr),
                  &QOverPtObslEfloatgR_Dictionary, isa_proxy, 0,
                  sizeof(::QOverPtObs<float>) );
      instance.SetDelete(&delete_QOverPtObslEfloatgR);
      instance.SetDeleteArray(&deleteArray_QOverPtObslEfloatgR);
      instance.SetDestructor(&destruct_QOverPtObslEfloatgR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::QOverPtObs<float>*)
   {
      return GenerateInitInstanceLocal((::QOverPtObs<float>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::QOverPtObs<float>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *QOverPtObslEfloatgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::QOverPtObs<float>*)0x0)->GetClass();
      QOverPtObslEfloatgR_TClassManip(theClass);
   return theClass;
   }

   static void QOverPtObslEfloatgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *SingleObservablelEfloatgR_Dictionary();
   static void SingleObservablelEfloatgR_TClassManip(TClass*);
   static void delete_SingleObservablelEfloatgR(void *p);
   static void deleteArray_SingleObservablelEfloatgR(void *p);
   static void destruct_SingleObservablelEfloatgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::SingleObservable<float>*)
   {
      ::SingleObservable<float> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::SingleObservable<float>));
      static ::ROOT::TGenericClassInfo 
         instance("SingleObservable<float>", "InDetUpgradePerformanceAnalysis/SingleObservable.h", 7,
                  typeid(::SingleObservable<float>), DefineBehavior(ptr, ptr),
                  &SingleObservablelEfloatgR_Dictionary, isa_proxy, 0,
                  sizeof(::SingleObservable<float>) );
      instance.SetDelete(&delete_SingleObservablelEfloatgR);
      instance.SetDeleteArray(&deleteArray_SingleObservablelEfloatgR);
      instance.SetDestructor(&destruct_SingleObservablelEfloatgR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::SingleObservable<float>*)
   {
      return GenerateInitInstanceLocal((::SingleObservable<float>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::SingleObservable<float>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *SingleObservablelEfloatgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::SingleObservable<float>*)0x0)->GetClass();
      SingleObservablelEfloatgR_TClassManip(theClass);
   return theClass;
   }

   static void SingleObservablelEfloatgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *SingleObservablelEintgR_Dictionary();
   static void SingleObservablelEintgR_TClassManip(TClass*);
   static void delete_SingleObservablelEintgR(void *p);
   static void deleteArray_SingleObservablelEintgR(void *p);
   static void destruct_SingleObservablelEintgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::SingleObservable<int>*)
   {
      ::SingleObservable<int> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::SingleObservable<int>));
      static ::ROOT::TGenericClassInfo 
         instance("SingleObservable<int>", "InDetUpgradePerformanceAnalysis/SingleObservable.h", 7,
                  typeid(::SingleObservable<int>), DefineBehavior(ptr, ptr),
                  &SingleObservablelEintgR_Dictionary, isa_proxy, 0,
                  sizeof(::SingleObservable<int>) );
      instance.SetDelete(&delete_SingleObservablelEintgR);
      instance.SetDeleteArray(&deleteArray_SingleObservablelEintgR);
      instance.SetDestructor(&destruct_SingleObservablelEintgR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::SingleObservable<int>*)
   {
      return GenerateInitInstanceLocal((::SingleObservable<int>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::SingleObservable<int>*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *SingleObservablelEintgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::SingleObservable<int>*)0x0)->GetClass();
      SingleObservablelEintgR_TClassManip(theClass);
   return theClass;
   }

   static void SingleObservablelEintgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *OccupancyObservable_Dictionary();
   static void OccupancyObservable_TClassManip(TClass*);
   static void delete_OccupancyObservable(void *p);
   static void deleteArray_OccupancyObservable(void *p);
   static void destruct_OccupancyObservable(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::OccupancyObservable*)
   {
      ::OccupancyObservable *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::OccupancyObservable));
      static ::ROOT::TGenericClassInfo 
         instance("OccupancyObservable", "InDetUpgradePerformanceAnalysis/OccupancyObservable.h", 21,
                  typeid(::OccupancyObservable), DefineBehavior(ptr, ptr),
                  &OccupancyObservable_Dictionary, isa_proxy, 0,
                  sizeof(::OccupancyObservable) );
      instance.SetDelete(&delete_OccupancyObservable);
      instance.SetDeleteArray(&deleteArray_OccupancyObservable);
      instance.SetDestructor(&destruct_OccupancyObservable);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::OccupancyObservable*)
   {
      return GenerateInitInstanceLocal((::OccupancyObservable*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::OccupancyObservable*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *OccupancyObservable_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::OccupancyObservable*)0x0)->GetClass();
      OccupancyObservable_TClassManip(theClass);
   return theClass;
   }

   static void OccupancyObservable_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *DiskOccupancy_Dictionary();
   static void DiskOccupancy_TClassManip(TClass*);
   static void delete_DiskOccupancy(void *p);
   static void deleteArray_DiskOccupancy(void *p);
   static void destruct_DiskOccupancy(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::DiskOccupancy*)
   {
      ::DiskOccupancy *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::DiskOccupancy));
      static ::ROOT::TGenericClassInfo 
         instance("DiskOccupancy", "InDetUpgradePerformanceAnalysis/DiskOccupancy.h", 6,
                  typeid(::DiskOccupancy), DefineBehavior(ptr, ptr),
                  &DiskOccupancy_Dictionary, isa_proxy, 0,
                  sizeof(::DiskOccupancy) );
      instance.SetDelete(&delete_DiskOccupancy);
      instance.SetDeleteArray(&deleteArray_DiskOccupancy);
      instance.SetDestructor(&destruct_DiskOccupancy);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::DiskOccupancy*)
   {
      return GenerateInitInstanceLocal((::DiskOccupancy*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::DiskOccupancy*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *DiskOccupancy_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::DiskOccupancy*)0x0)->GetClass();
      DiskOccupancy_TClassManip(theClass);
   return theClass;
   }

   static void DiskOccupancy_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *SCTDiskOccupancy_Dictionary();
   static void SCTDiskOccupancy_TClassManip(TClass*);
   static void *new_SCTDiskOccupancy(void *p = 0);
   static void *newArray_SCTDiskOccupancy(Long_t size, void *p);
   static void delete_SCTDiskOccupancy(void *p);
   static void deleteArray_SCTDiskOccupancy(void *p);
   static void destruct_SCTDiskOccupancy(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::SCTDiskOccupancy*)
   {
      ::SCTDiskOccupancy *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::SCTDiskOccupancy));
      static ::ROOT::TGenericClassInfo 
         instance("SCTDiskOccupancy", "InDetUpgradePerformanceAnalysis/SCTDiskOccupancy.h", 10,
                  typeid(::SCTDiskOccupancy), DefineBehavior(ptr, ptr),
                  &SCTDiskOccupancy_Dictionary, isa_proxy, 0,
                  sizeof(::SCTDiskOccupancy) );
      instance.SetNew(&new_SCTDiskOccupancy);
      instance.SetNewArray(&newArray_SCTDiskOccupancy);
      instance.SetDelete(&delete_SCTDiskOccupancy);
      instance.SetDeleteArray(&deleteArray_SCTDiskOccupancy);
      instance.SetDestructor(&destruct_SCTDiskOccupancy);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::SCTDiskOccupancy*)
   {
      return GenerateInitInstanceLocal((::SCTDiskOccupancy*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::SCTDiskOccupancy*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *SCTDiskOccupancy_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::SCTDiskOccupancy*)0x0)->GetClass();
      SCTDiskOccupancy_TClassManip(theClass);
   return theClass;
   }

   static void SCTDiskOccupancy_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *SCTBarrelOccupancy_Dictionary();
   static void SCTBarrelOccupancy_TClassManip(TClass*);
   static void *new_SCTBarrelOccupancy(void *p = 0);
   static void *newArray_SCTBarrelOccupancy(Long_t size, void *p);
   static void delete_SCTBarrelOccupancy(void *p);
   static void deleteArray_SCTBarrelOccupancy(void *p);
   static void destruct_SCTBarrelOccupancy(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::SCTBarrelOccupancy*)
   {
      ::SCTBarrelOccupancy *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::SCTBarrelOccupancy));
      static ::ROOT::TGenericClassInfo 
         instance("SCTBarrelOccupancy", "InDetUpgradePerformanceAnalysis/SCTBarrelOccupancy.h", 10,
                  typeid(::SCTBarrelOccupancy), DefineBehavior(ptr, ptr),
                  &SCTBarrelOccupancy_Dictionary, isa_proxy, 0,
                  sizeof(::SCTBarrelOccupancy) );
      instance.SetNew(&new_SCTBarrelOccupancy);
      instance.SetNewArray(&newArray_SCTBarrelOccupancy);
      instance.SetDelete(&delete_SCTBarrelOccupancy);
      instance.SetDeleteArray(&deleteArray_SCTBarrelOccupancy);
      instance.SetDestructor(&destruct_SCTBarrelOccupancy);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::SCTBarrelOccupancy*)
   {
      return GenerateInitInstanceLocal((::SCTBarrelOccupancy*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::SCTBarrelOccupancy*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *SCTBarrelOccupancy_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::SCTBarrelOccupancy*)0x0)->GetClass();
      SCTBarrelOccupancy_TClassManip(theClass);
   return theClass;
   }

   static void SCTBarrelOccupancy_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *SCTBarrelPerEventOccupancy_Dictionary();
   static void SCTBarrelPerEventOccupancy_TClassManip(TClass*);
   static void *new_SCTBarrelPerEventOccupancy(void *p = 0);
   static void *newArray_SCTBarrelPerEventOccupancy(Long_t size, void *p);
   static void delete_SCTBarrelPerEventOccupancy(void *p);
   static void deleteArray_SCTBarrelPerEventOccupancy(void *p);
   static void destruct_SCTBarrelPerEventOccupancy(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::SCTBarrelPerEventOccupancy*)
   {
      ::SCTBarrelPerEventOccupancy *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::SCTBarrelPerEventOccupancy));
      static ::ROOT::TGenericClassInfo 
         instance("SCTBarrelPerEventOccupancy", "InDetUpgradePerformanceAnalysis/SCTBarrelPerEventOccupancy.h", 14,
                  typeid(::SCTBarrelPerEventOccupancy), DefineBehavior(ptr, ptr),
                  &SCTBarrelPerEventOccupancy_Dictionary, isa_proxy, 0,
                  sizeof(::SCTBarrelPerEventOccupancy) );
      instance.SetNew(&new_SCTBarrelPerEventOccupancy);
      instance.SetNewArray(&newArray_SCTBarrelPerEventOccupancy);
      instance.SetDelete(&delete_SCTBarrelPerEventOccupancy);
      instance.SetDeleteArray(&deleteArray_SCTBarrelPerEventOccupancy);
      instance.SetDestructor(&destruct_SCTBarrelPerEventOccupancy);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::SCTBarrelPerEventOccupancy*)
   {
      return GenerateInitInstanceLocal((::SCTBarrelPerEventOccupancy*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::SCTBarrelPerEventOccupancy*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *SCTBarrelPerEventOccupancy_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::SCTBarrelPerEventOccupancy*)0x0)->GetClass();
      SCTBarrelPerEventOccupancy_TClassManip(theClass);
   return theClass;
   }

   static void SCTBarrelPerEventOccupancy_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *PerEventOccupancyObservable_Dictionary();
   static void PerEventOccupancyObservable_TClassManip(TClass*);
   static void delete_PerEventOccupancyObservable(void *p);
   static void deleteArray_PerEventOccupancyObservable(void *p);
   static void destruct_PerEventOccupancyObservable(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::PerEventOccupancyObservable*)
   {
      ::PerEventOccupancyObservable *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::PerEventOccupancyObservable));
      static ::ROOT::TGenericClassInfo 
         instance("PerEventOccupancyObservable", "InDetUpgradePerformanceAnalysis/PerEventOccupancyObservable.h", 15,
                  typeid(::PerEventOccupancyObservable), DefineBehavior(ptr, ptr),
                  &PerEventOccupancyObservable_Dictionary, isa_proxy, 0,
                  sizeof(::PerEventOccupancyObservable) );
      instance.SetDelete(&delete_PerEventOccupancyObservable);
      instance.SetDeleteArray(&deleteArray_PerEventOccupancyObservable);
      instance.SetDestructor(&destruct_PerEventOccupancyObservable);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::PerEventOccupancyObservable*)
   {
      return GenerateInitInstanceLocal((::PerEventOccupancyObservable*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::PerEventOccupancyObservable*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *PerEventOccupancyObservable_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::PerEventOccupancyObservable*)0x0)->GetClass();
      PerEventOccupancyObservable_TClassManip(theClass);
   return theClass;
   }

   static void PerEventOccupancyObservable_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *PixelBarrelPerEventOccupancy_Dictionary();
   static void PixelBarrelPerEventOccupancy_TClassManip(TClass*);
   static void *new_PixelBarrelPerEventOccupancy(void *p = 0);
   static void *newArray_PixelBarrelPerEventOccupancy(Long_t size, void *p);
   static void delete_PixelBarrelPerEventOccupancy(void *p);
   static void deleteArray_PixelBarrelPerEventOccupancy(void *p);
   static void destruct_PixelBarrelPerEventOccupancy(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::PixelBarrelPerEventOccupancy*)
   {
      ::PixelBarrelPerEventOccupancy *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::PixelBarrelPerEventOccupancy));
      static ::ROOT::TGenericClassInfo 
         instance("PixelBarrelPerEventOccupancy", "InDetUpgradePerformanceAnalysis/PixelBarrelPerEventOccupancy.h", 14,
                  typeid(::PixelBarrelPerEventOccupancy), DefineBehavior(ptr, ptr),
                  &PixelBarrelPerEventOccupancy_Dictionary, isa_proxy, 0,
                  sizeof(::PixelBarrelPerEventOccupancy) );
      instance.SetNew(&new_PixelBarrelPerEventOccupancy);
      instance.SetNewArray(&newArray_PixelBarrelPerEventOccupancy);
      instance.SetDelete(&delete_PixelBarrelPerEventOccupancy);
      instance.SetDeleteArray(&deleteArray_PixelBarrelPerEventOccupancy);
      instance.SetDestructor(&destruct_PixelBarrelPerEventOccupancy);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::PixelBarrelPerEventOccupancy*)
   {
      return GenerateInitInstanceLocal((::PixelBarrelPerEventOccupancy*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::PixelBarrelPerEventOccupancy*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *PixelBarrelPerEventOccupancy_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::PixelBarrelPerEventOccupancy*)0x0)->GetClass();
      PixelBarrelPerEventOccupancy_TClassManip(theClass);
   return theClass;
   }

   static void PixelBarrelPerEventOccupancy_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *PixelDiskPerEventOccupancy_Dictionary();
   static void PixelDiskPerEventOccupancy_TClassManip(TClass*);
   static void *new_PixelDiskPerEventOccupancy(void *p = 0);
   static void *newArray_PixelDiskPerEventOccupancy(Long_t size, void *p);
   static void delete_PixelDiskPerEventOccupancy(void *p);
   static void deleteArray_PixelDiskPerEventOccupancy(void *p);
   static void destruct_PixelDiskPerEventOccupancy(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::PixelDiskPerEventOccupancy*)
   {
      ::PixelDiskPerEventOccupancy *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::PixelDiskPerEventOccupancy));
      static ::ROOT::TGenericClassInfo 
         instance("PixelDiskPerEventOccupancy", "InDetUpgradePerformanceAnalysis/PixelDiskPerEventOccupancy.h", 14,
                  typeid(::PixelDiskPerEventOccupancy), DefineBehavior(ptr, ptr),
                  &PixelDiskPerEventOccupancy_Dictionary, isa_proxy, 0,
                  sizeof(::PixelDiskPerEventOccupancy) );
      instance.SetNew(&new_PixelDiskPerEventOccupancy);
      instance.SetNewArray(&newArray_PixelDiskPerEventOccupancy);
      instance.SetDelete(&delete_PixelDiskPerEventOccupancy);
      instance.SetDeleteArray(&deleteArray_PixelDiskPerEventOccupancy);
      instance.SetDestructor(&destruct_PixelDiskPerEventOccupancy);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::PixelDiskPerEventOccupancy*)
   {
      return GenerateInitInstanceLocal((::PixelDiskPerEventOccupancy*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::PixelDiskPerEventOccupancy*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *PixelDiskPerEventOccupancy_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::PixelDiskPerEventOccupancy*)0x0)->GetClass();
      PixelDiskPerEventOccupancy_TClassManip(theClass);
   return theClass;
   }

   static void PixelDiskPerEventOccupancy_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *PixelDiskOccupancy_Dictionary();
   static void PixelDiskOccupancy_TClassManip(TClass*);
   static void *new_PixelDiskOccupancy(void *p = 0);
   static void *newArray_PixelDiskOccupancy(Long_t size, void *p);
   static void delete_PixelDiskOccupancy(void *p);
   static void deleteArray_PixelDiskOccupancy(void *p);
   static void destruct_PixelDiskOccupancy(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::PixelDiskOccupancy*)
   {
      ::PixelDiskOccupancy *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::PixelDiskOccupancy));
      static ::ROOT::TGenericClassInfo 
         instance("PixelDiskOccupancy", "InDetUpgradePerformanceAnalysis/PixelDiskOccupancy.h", 10,
                  typeid(::PixelDiskOccupancy), DefineBehavior(ptr, ptr),
                  &PixelDiskOccupancy_Dictionary, isa_proxy, 0,
                  sizeof(::PixelDiskOccupancy) );
      instance.SetNew(&new_PixelDiskOccupancy);
      instance.SetNewArray(&newArray_PixelDiskOccupancy);
      instance.SetDelete(&delete_PixelDiskOccupancy);
      instance.SetDeleteArray(&deleteArray_PixelDiskOccupancy);
      instance.SetDestructor(&destruct_PixelDiskOccupancy);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::PixelDiskOccupancy*)
   {
      return GenerateInitInstanceLocal((::PixelDiskOccupancy*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::PixelDiskOccupancy*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *PixelDiskOccupancy_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::PixelDiskOccupancy*)0x0)->GetClass();
      PixelDiskOccupancy_TClassManip(theClass);
   return theClass;
   }

   static void PixelDiskOccupancy_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *PixelBarrelOccupancy_Dictionary();
   static void PixelBarrelOccupancy_TClassManip(TClass*);
   static void *new_PixelBarrelOccupancy(void *p = 0);
   static void *newArray_PixelBarrelOccupancy(Long_t size, void *p);
   static void delete_PixelBarrelOccupancy(void *p);
   static void deleteArray_PixelBarrelOccupancy(void *p);
   static void destruct_PixelBarrelOccupancy(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::PixelBarrelOccupancy*)
   {
      ::PixelBarrelOccupancy *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::PixelBarrelOccupancy));
      static ::ROOT::TGenericClassInfo 
         instance("PixelBarrelOccupancy", "InDetUpgradePerformanceAnalysis/PixelBarrelOccupancy.h", 10,
                  typeid(::PixelBarrelOccupancy), DefineBehavior(ptr, ptr),
                  &PixelBarrelOccupancy_Dictionary, isa_proxy, 0,
                  sizeof(::PixelBarrelOccupancy) );
      instance.SetNew(&new_PixelBarrelOccupancy);
      instance.SetNewArray(&newArray_PixelBarrelOccupancy);
      instance.SetDelete(&delete_PixelBarrelOccupancy);
      instance.SetDeleteArray(&deleteArray_PixelBarrelOccupancy);
      instance.SetDestructor(&destruct_PixelBarrelOccupancy);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::PixelBarrelOccupancy*)
   {
      return GenerateInitInstanceLocal((::PixelBarrelOccupancy*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::PixelBarrelOccupancy*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *PixelBarrelOccupancy_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::PixelBarrelOccupancy*)0x0)->GetClass();
      PixelBarrelOccupancy_TClassManip(theClass);
   return theClass;
   }

   static void PixelBarrelOccupancy_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *Occupancy2D_Dictionary();
   static void Occupancy2D_TClassManip(TClass*);
   static void *new_Occupancy2D(void *p = 0);
   static void *newArray_Occupancy2D(Long_t size, void *p);
   static void delete_Occupancy2D(void *p);
   static void deleteArray_Occupancy2D(void *p);
   static void destruct_Occupancy2D(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Occupancy2D*)
   {
      ::Occupancy2D *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Occupancy2D));
      static ::ROOT::TGenericClassInfo 
         instance("Occupancy2D", "InDetUpgradePerformanceAnalysis/Occupancy2D.h", 8,
                  typeid(::Occupancy2D), DefineBehavior(ptr, ptr),
                  &Occupancy2D_Dictionary, isa_proxy, 0,
                  sizeof(::Occupancy2D) );
      instance.SetNew(&new_Occupancy2D);
      instance.SetNewArray(&newArray_Occupancy2D);
      instance.SetDelete(&delete_Occupancy2D);
      instance.SetDeleteArray(&deleteArray_Occupancy2D);
      instance.SetDestructor(&destruct_Occupancy2D);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Occupancy2D*)
   {
      return GenerateInitInstanceLocal((::Occupancy2D*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::Occupancy2D*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *Occupancy2D_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Occupancy2D*)0x0)->GetClass();
      Occupancy2D_TClassManip(theClass);
   return theClass;
   }

   static void Occupancy2D_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *PixelOccupancy2D_Dictionary();
   static void PixelOccupancy2D_TClassManip(TClass*);
   static void *new_PixelOccupancy2D(void *p = 0);
   static void *newArray_PixelOccupancy2D(Long_t size, void *p);
   static void delete_PixelOccupancy2D(void *p);
   static void deleteArray_PixelOccupancy2D(void *p);
   static void destruct_PixelOccupancy2D(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::PixelOccupancy2D*)
   {
      ::PixelOccupancy2D *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::PixelOccupancy2D));
      static ::ROOT::TGenericClassInfo 
         instance("PixelOccupancy2D", "InDetUpgradePerformanceAnalysis/PixelOccupancy2D.h", 10,
                  typeid(::PixelOccupancy2D), DefineBehavior(ptr, ptr),
                  &PixelOccupancy2D_Dictionary, isa_proxy, 0,
                  sizeof(::PixelOccupancy2D) );
      instance.SetNew(&new_PixelOccupancy2D);
      instance.SetNewArray(&newArray_PixelOccupancy2D);
      instance.SetDelete(&delete_PixelOccupancy2D);
      instance.SetDeleteArray(&deleteArray_PixelOccupancy2D);
      instance.SetDestructor(&destruct_PixelOccupancy2D);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::PixelOccupancy2D*)
   {
      return GenerateInitInstanceLocal((::PixelOccupancy2D*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::PixelOccupancy2D*)0x0); R__UseDummy(_R__UNIQUE_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *PixelOccupancy2D_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::PixelOccupancy2D*)0x0)->GetClass();
      PixelOccupancy2D_TClassManip(theClass);
   return theClass;
   }

   static void PixelOccupancy2D_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_InDetUpgradeAnalysis(void *p) {
      return  p ? new(p) ::InDetUpgradeAnalysis : new ::InDetUpgradeAnalysis;
   }
   static void *newArray_InDetUpgradeAnalysis(Long_t nElements, void *p) {
      return p ? new(p) ::InDetUpgradeAnalysis[nElements] : new ::InDetUpgradeAnalysis[nElements];
   }
   // Wrapper around operator delete
   static void delete_InDetUpgradeAnalysis(void *p) {
      delete ((::InDetUpgradeAnalysis*)p);
   }
   static void deleteArray_InDetUpgradeAnalysis(void *p) {
      delete [] ((::InDetUpgradeAnalysis*)p);
   }
   static void destruct_InDetUpgradeAnalysis(void *p) {
      typedef ::InDetUpgradeAnalysis current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::InDetUpgradeAnalysis

namespace ROOT {
   // Wrapper around operator delete
   static void delete_SingleAnalysis(void *p) {
      delete ((::SingleAnalysis*)p);
   }
   static void deleteArray_SingleAnalysis(void *p) {
      delete [] ((::SingleAnalysis*)p);
   }
   static void destruct_SingleAnalysis(void *p) {
      typedef ::SingleAnalysis current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::SingleAnalysis

namespace ROOT {
   // Wrapper around operator delete
   static void delete_ObservableGroup(void *p) {
      delete ((::ObservableGroup*)p);
   }
   static void deleteArray_ObservableGroup(void *p) {
      delete [] ((::ObservableGroup*)p);
   }
   static void destruct_ObservableGroup(void *p) {
      typedef ::ObservableGroup current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ObservableGroup

namespace ROOT {
   // Wrapper around operator delete
   static void delete_Observable(void *p) {
      delete ((::Observable*)p);
   }
   static void deleteArray_Observable(void *p) {
      delete [] ((::Observable*)p);
   }
   static void destruct_Observable(void *p) {
      typedef ::Observable current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Observable

namespace ROOT {
   // Wrappers around operator new
   static void *new_SensorInfoBase(void *p) {
      return  p ? new(p) ::SensorInfoBase : new ::SensorInfoBase;
   }
   static void *newArray_SensorInfoBase(Long_t nElements, void *p) {
      return p ? new(p) ::SensorInfoBase[nElements] : new ::SensorInfoBase[nElements];
   }
   // Wrapper around operator delete
   static void delete_SensorInfoBase(void *p) {
      delete ((::SensorInfoBase*)p);
   }
   static void deleteArray_SensorInfoBase(void *p) {
      delete [] ((::SensorInfoBase*)p);
   }
   static void destruct_SensorInfoBase(void *p) {
      typedef ::SensorInfoBase current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::SensorInfoBase

namespace ROOT {
   // Wrappers around operator new
   static void *new_DumpSensorInfo(void *p) {
      return  p ? new(p) ::DumpSensorInfo : new ::DumpSensorInfo;
   }
   static void *newArray_DumpSensorInfo(Long_t nElements, void *p) {
      return p ? new(p) ::DumpSensorInfo[nElements] : new ::DumpSensorInfo[nElements];
   }
   // Wrapper around operator delete
   static void delete_DumpSensorInfo(void *p) {
      delete ((::DumpSensorInfo*)p);
   }
   static void deleteArray_DumpSensorInfo(void *p) {
      delete [] ((::DumpSensorInfo*)p);
   }
   static void destruct_DumpSensorInfo(void *p) {
      typedef ::DumpSensorInfo current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DumpSensorInfo

namespace ROOT {
   // Wrappers around operator new
   static void *new_TreeHouse(void *p) {
      return  p ? new(p) ::TreeHouse : new ::TreeHouse;
   }
   static void *newArray_TreeHouse(Long_t nElements, void *p) {
      return p ? new(p) ::TreeHouse[nElements] : new ::TreeHouse[nElements];
   }
   // Wrapper around operator delete
   static void delete_TreeHouse(void *p) {
      delete ((::TreeHouse*)p);
   }
   static void deleteArray_TreeHouse(void *p) {
      delete [] ((::TreeHouse*)p);
   }
   static void destruct_TreeHouse(void *p) {
      typedef ::TreeHouse current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::TreeHouse

namespace ROOT {
   // Wrapper around operator delete
   static void delete_Cuts(void *p) {
      delete ((::Cuts*)p);
   }
   static void deleteArray_Cuts(void *p) {
      delete [] ((::Cuts*)p);
   }
   static void destruct_Cuts(void *p) {
      typedef ::Cuts current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Cuts

namespace ROOT {
   // Wrappers around operator new
   static void *new_AllCuts(void *p) {
      return  p ? new(p) ::AllCuts : new ::AllCuts;
   }
   static void *newArray_AllCuts(Long_t nElements, void *p) {
      return p ? new(p) ::AllCuts[nElements] : new ::AllCuts[nElements];
   }
   // Wrapper around operator delete
   static void delete_AllCuts(void *p) {
      delete ((::AllCuts*)p);
   }
   static void deleteArray_AllCuts(void *p) {
      delete [] ((::AllCuts*)p);
   }
   static void destruct_AllCuts(void *p) {
      typedef ::AllCuts current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::AllCuts

namespace ROOT {
   // Wrappers around operator new
   static void *new_StandardCuts(void *p) {
      return  p ? new(p) ::StandardCuts : new ::StandardCuts;
   }
   static void *newArray_StandardCuts(Long_t nElements, void *p) {
      return p ? new(p) ::StandardCuts[nElements] : new ::StandardCuts[nElements];
   }
   // Wrapper around operator delete
   static void delete_StandardCuts(void *p) {
      delete ((::StandardCuts*)p);
   }
   static void deleteArray_StandardCuts(void *p) {
      delete [] ((::StandardCuts*)p);
   }
   static void destruct_StandardCuts(void *p) {
      typedef ::StandardCuts current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::StandardCuts

namespace ROOT {
   // Wrapper around operator delete
   static void delete_RangeDependentMinCutlEfloatcOfloatgR(void *p) {
      delete ((::RangeDependentMinCut<float,float>*)p);
   }
   static void deleteArray_RangeDependentMinCutlEfloatcOfloatgR(void *p) {
      delete [] ((::RangeDependentMinCut<float,float>*)p);
   }
   static void destruct_RangeDependentMinCutlEfloatcOfloatgR(void *p) {
      typedef ::RangeDependentMinCut<float,float> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RangeDependentMinCut<float,float>

namespace ROOT {
   // Wrapper around operator delete
   static void delete_RangeDependentMinCutlEintcOfloatgR(void *p) {
      delete ((::RangeDependentMinCut<int,float>*)p);
   }
   static void deleteArray_RangeDependentMinCutlEintcOfloatgR(void *p) {
      delete [] ((::RangeDependentMinCut<int,float>*)p);
   }
   static void destruct_RangeDependentMinCutlEintcOfloatgR(void *p) {
      typedef ::RangeDependentMinCut<int,float> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RangeDependentMinCut<int,float>

namespace ROOT {
   // Wrapper around operator delete
   static void delete_MinCutlEintgR(void *p) {
      delete ((::MinCut<int>*)p);
   }
   static void deleteArray_MinCutlEintgR(void *p) {
      delete [] ((::MinCut<int>*)p);
   }
   static void destruct_MinCutlEintgR(void *p) {
      typedef ::MinCut<int> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::MinCut<int>

namespace ROOT {
   // Wrapper around operator delete
   static void delete_MinCutlEfloatgR(void *p) {
      delete ((::MinCut<float>*)p);
   }
   static void deleteArray_MinCutlEfloatgR(void *p) {
      delete [] ((::MinCut<float>*)p);
   }
   static void destruct_MinCutlEfloatgR(void *p) {
      typedef ::MinCut<float> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::MinCut<float>

namespace ROOT {
   // Wrapper around operator delete
   static void delete_MaxCutlEfloatgR(void *p) {
      delete ((::MaxCut<float>*)p);
   }
   static void deleteArray_MaxCutlEfloatgR(void *p) {
      delete [] ((::MaxCut<float>*)p);
   }
   static void destruct_MaxCutlEfloatgR(void *p) {
      typedef ::MaxCut<float> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::MaxCut<float>

namespace ROOT {
   // Wrapper around operator delete
   static void delete_MaxCutlEintgR(void *p) {
      delete ((::MaxCut<int>*)p);
   }
   static void deleteArray_MaxCutlEintgR(void *p) {
      delete [] ((::MaxCut<int>*)p);
   }
   static void destruct_MaxCutlEintgR(void *p) {
      typedef ::MaxCut<int> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::MaxCut<int>

namespace ROOT {
   // Wrapper around operator delete
   static void delete_RangeCutlEintgR(void *p) {
      delete ((::RangeCut<int>*)p);
   }
   static void deleteArray_RangeCutlEintgR(void *p) {
      delete [] ((::RangeCut<int>*)p);
   }
   static void destruct_RangeCutlEintgR(void *p) {
      typedef ::RangeCut<int> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RangeCut<int>

namespace ROOT {
   // Wrapper around operator delete
   static void delete_RangeCutlEfloatgR(void *p) {
      delete ((::RangeCut<float>*)p);
   }
   static void deleteArray_RangeCutlEfloatgR(void *p) {
      delete [] ((::RangeCut<float>*)p);
   }
   static void destruct_RangeCutlEfloatgR(void *p) {
      typedef ::RangeCut<float> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RangeCut<float>

namespace ROOT {
   // Wrapper around operator delete
   static void delete_RangeProductCutlEfloatgR(void *p) {
      delete ((::RangeProductCut<float>*)p);
   }
   static void deleteArray_RangeProductCutlEfloatgR(void *p) {
      delete [] ((::RangeProductCut<float>*)p);
   }
   static void destruct_RangeProductCutlEfloatgR(void *p) {
      typedef ::RangeProductCut<float> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RangeProductCut<float>

namespace ROOT {
   // Wrapper around operator delete
   static void delete_DistributionlEintgR(void *p) {
      delete ((::Distribution<int>*)p);
   }
   static void deleteArray_DistributionlEintgR(void *p) {
      delete [] ((::Distribution<int>*)p);
   }
   static void destruct_DistributionlEintgR(void *p) {
      typedef ::Distribution<int> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Distribution<int>

namespace ROOT {
   // Wrapper around operator delete
   static void delete_DistributionlEfloatgR(void *p) {
      delete ((::Distribution<float>*)p);
   }
   static void deleteArray_DistributionlEfloatgR(void *p) {
      delete [] ((::Distribution<float>*)p);
   }
   static void destruct_DistributionlEfloatgR(void *p) {
      typedef ::Distribution<float> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Distribution<float>

namespace ROOT {
   // Wrapper around operator delete
   static void delete_SelectorlEintgR(void *p) {
      delete ((::Selector<int>*)p);
   }
   static void deleteArray_SelectorlEintgR(void *p) {
      delete [] ((::Selector<int>*)p);
   }
   static void destruct_SelectorlEintgR(void *p) {
      typedef ::Selector<int> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Selector<int>

namespace ROOT {
   // Wrapper around operator delete
   static void delete_SelectorlEfloatgR(void *p) {
      delete ((::Selector<float>*)p);
   }
   static void deleteArray_SelectorlEfloatgR(void *p) {
      delete [] ((::Selector<float>*)p);
   }
   static void destruct_SelectorlEfloatgR(void *p) {
      typedef ::Selector<float> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Selector<float>

namespace ROOT {
   // Wrapper around operator delete
   static void delete_AbsSelectorlEintgR(void *p) {
      delete ((::AbsSelector<int>*)p);
   }
   static void deleteArray_AbsSelectorlEintgR(void *p) {
      delete [] ((::AbsSelector<int>*)p);
   }
   static void destruct_AbsSelectorlEintgR(void *p) {
      typedef ::AbsSelector<int> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::AbsSelector<int>

namespace ROOT {
   // Wrapper around operator delete
   static void delete_AbsSelectorlEfloatgR(void *p) {
      delete ((::AbsSelector<float>*)p);
   }
   static void deleteArray_AbsSelectorlEfloatgR(void *p) {
      delete [] ((::AbsSelector<float>*)p);
   }
   static void destruct_AbsSelectorlEfloatgR(void *p) {
      typedef ::AbsSelector<float> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::AbsSelector<float>

namespace ROOT {
   // Wrapper around operator delete
   static void delete_MinimumHitsCut(void *p) {
      delete ((::MinimumHitsCut*)p);
   }
   static void deleteArray_MinimumHitsCut(void *p) {
      delete [] ((::MinimumHitsCut*)p);
   }
   static void destruct_MinimumHitsCut(void *p) {
      typedef ::MinimumHitsCut current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::MinimumHitsCut

namespace ROOT {
   // Wrapper around operator delete
   static void delete_AbsSelectorWithIndex(void *p) {
      delete ((::AbsSelectorWithIndex*)p);
   }
   static void deleteArray_AbsSelectorWithIndex(void *p) {
      delete [] ((::AbsSelectorWithIndex*)p);
   }
   static void destruct_AbsSelectorWithIndex(void *p) {
      typedef ::AbsSelectorWithIndex current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::AbsSelectorWithIndex

namespace ROOT {
   // Wrapper around operator delete
   static void delete_RangeCutWithIndex(void *p) {
      delete ((::RangeCutWithIndex*)p);
   }
   static void deleteArray_RangeCutWithIndex(void *p) {
      delete [] ((::RangeCutWithIndex*)p);
   }
   static void destruct_RangeCutWithIndex(void *p) {
      typedef ::RangeCutWithIndex current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RangeCutWithIndex

namespace ROOT {
   // Wrappers around operator new
   static void *new_Efficiency(void *p) {
      return  p ? new(p) ::Efficiency : new ::Efficiency;
   }
   static void *newArray_Efficiency(Long_t nElements, void *p) {
      return p ? new(p) ::Efficiency[nElements] : new ::Efficiency[nElements];
   }
   // Wrapper around operator delete
   static void delete_Efficiency(void *p) {
      delete ((::Efficiency*)p);
   }
   static void deleteArray_Efficiency(void *p) {
      delete [] ((::Efficiency*)p);
   }
   static void destruct_Efficiency(void *p) {
      typedef ::Efficiency current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Efficiency

namespace ROOT {
   // Wrappers around operator new
   static void *new_FakeRate(void *p) {
      return  p ? new(p) ::FakeRate : new ::FakeRate;
   }
   static void *newArray_FakeRate(Long_t nElements, void *p) {
      return p ? new(p) ::FakeRate[nElements] : new ::FakeRate[nElements];
   }
   // Wrapper around operator delete
   static void delete_FakeRate(void *p) {
      delete ((::FakeRate*)p);
   }
   static void deleteArray_FakeRate(void *p) {
      delete [] ((::FakeRate*)p);
   }
   static void destruct_FakeRate(void *p) {
      typedef ::FakeRate current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::FakeRate

namespace ROOT {
   // Wrapper around operator delete
   static void delete_TrackRatio(void *p) {
      delete ((::TrackRatio*)p);
   }
   static void deleteArray_TrackRatio(void *p) {
      delete [] ((::TrackRatio*)p);
   }
   static void destruct_TrackRatio(void *p) {
      typedef ::TrackRatio current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::TrackRatio

namespace ROOT {
   // Wrappers around operator new
   static void *new_Binning(void *p) {
      return  p ? new(p) ::Binning : new ::Binning;
   }
   static void *newArray_Binning(Long_t nElements, void *p) {
      return p ? new(p) ::Binning[nElements] : new ::Binning[nElements];
   }
   // Wrapper around operator delete
   static void delete_Binning(void *p) {
      delete ((::Binning*)p);
   }
   static void deleteArray_Binning(void *p) {
      delete [] ((::Binning*)p);
   }
   static void destruct_Binning(void *p) {
      typedef ::Binning current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Binning

namespace ROOT {
   // Wrapper around operator delete
   static void delete_CalculatorlEfloatgR(void *p) {
      delete ((::Calculator<float>*)p);
   }
   static void deleteArray_CalculatorlEfloatgR(void *p) {
      delete [] ((::Calculator<float>*)p);
   }
   static void destruct_CalculatorlEfloatgR(void *p) {
      typedef ::Calculator<float> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Calculator<float>

namespace ROOT {
   // Wrapper around operator delete
   static void delete_CalculatorlEintgR(void *p) {
      delete ((::Calculator<int>*)p);
   }
   static void deleteArray_CalculatorlEintgR(void *p) {
      delete [] ((::Calculator<int>*)p);
   }
   static void destruct_CalculatorlEintgR(void *p) {
      typedef ::Calculator<int> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Calculator<int>

namespace ROOT {
   // Wrapper around operator delete
   static void delete_AdditionlEintgR(void *p) {
      delete ((::Addition<int>*)p);
   }
   static void deleteArray_AdditionlEintgR(void *p) {
      delete [] ((::Addition<int>*)p);
   }
   static void destruct_AdditionlEintgR(void *p) {
      typedef ::Addition<int> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Addition<int>

namespace ROOT {
   // Wrapper around operator delete
   static void delete_DerivedBranchlEfloatgR(void *p) {
      delete ((::DerivedBranch<float>*)p);
   }
   static void deleteArray_DerivedBranchlEfloatgR(void *p) {
      delete [] ((::DerivedBranch<float>*)p);
   }
   static void destruct_DerivedBranchlEfloatgR(void *p) {
      typedef ::DerivedBranch<float> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DerivedBranch<float>

namespace ROOT {
   // Wrapper around operator delete
   static void delete_AdditionlEfloatgR(void *p) {
      delete ((::Addition<float>*)p);
   }
   static void deleteArray_AdditionlEfloatgR(void *p) {
      delete [] ((::Addition<float>*)p);
   }
   static void destruct_AdditionlEfloatgR(void *p) {
      typedef ::Addition<float> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Addition<float>

namespace ROOT {
   // Wrapper around operator delete
   static void delete_ResolutionObservablelEfloatgR(void *p) {
      delete ((::ResolutionObservable<float>*)p);
   }
   static void deleteArray_ResolutionObservablelEfloatgR(void *p) {
      delete [] ((::ResolutionObservable<float>*)p);
   }
   static void destruct_ResolutionObservablelEfloatgR(void *p) {
      typedef ::ResolutionObservable<float> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ResolutionObservable<float>

namespace ROOT {
   // Wrapper around operator delete
   static void delete_ResolutionObservablelEintgR(void *p) {
      delete ((::ResolutionObservable<int>*)p);
   }
   static void deleteArray_ResolutionObservablelEintgR(void *p) {
      delete [] ((::ResolutionObservable<int>*)p);
   }
   static void destruct_ResolutionObservablelEintgR(void *p) {
      typedef ::ResolutionObservable<int> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::ResolutionObservable<int>

namespace ROOT {
   // Wrapper around operator delete
   static void delete_DiffObservablelEfloatgR(void *p) {
      delete ((::DiffObservable<float>*)p);
   }
   static void deleteArray_DiffObservablelEfloatgR(void *p) {
      delete [] ((::DiffObservable<float>*)p);
   }
   static void destruct_DiffObservablelEfloatgR(void *p) {
      typedef ::DiffObservable<float> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DiffObservable<float>

namespace ROOT {
   // Wrapper around operator delete
   static void delete_PullObservablelEfloatgR(void *p) {
      delete ((::PullObservable<float>*)p);
   }
   static void deleteArray_PullObservablelEfloatgR(void *p) {
      delete [] ((::PullObservable<float>*)p);
   }
   static void destruct_PullObservablelEfloatgR(void *p) {
      typedef ::PullObservable<float> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::PullObservable<float>

namespace ROOT {
   // Wrapper around operator delete
   static void delete_Z0SinThetaObslEfloatgR(void *p) {
      delete ((::Z0SinThetaObs<float>*)p);
   }
   static void deleteArray_Z0SinThetaObslEfloatgR(void *p) {
      delete [] ((::Z0SinThetaObs<float>*)p);
   }
   static void destruct_Z0SinThetaObslEfloatgR(void *p) {
      typedef ::Z0SinThetaObs<float> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Z0SinThetaObs<float>

namespace ROOT {
   // Wrapper around operator delete
   static void delete_QOverPtObslEfloatgR(void *p) {
      delete ((::QOverPtObs<float>*)p);
   }
   static void deleteArray_QOverPtObslEfloatgR(void *p) {
      delete [] ((::QOverPtObs<float>*)p);
   }
   static void destruct_QOverPtObslEfloatgR(void *p) {
      typedef ::QOverPtObs<float> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::QOverPtObs<float>

namespace ROOT {
   // Wrapper around operator delete
   static void delete_SingleObservablelEfloatgR(void *p) {
      delete ((::SingleObservable<float>*)p);
   }
   static void deleteArray_SingleObservablelEfloatgR(void *p) {
      delete [] ((::SingleObservable<float>*)p);
   }
   static void destruct_SingleObservablelEfloatgR(void *p) {
      typedef ::SingleObservable<float> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::SingleObservable<float>

namespace ROOT {
   // Wrapper around operator delete
   static void delete_SingleObservablelEintgR(void *p) {
      delete ((::SingleObservable<int>*)p);
   }
   static void deleteArray_SingleObservablelEintgR(void *p) {
      delete [] ((::SingleObservable<int>*)p);
   }
   static void destruct_SingleObservablelEintgR(void *p) {
      typedef ::SingleObservable<int> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::SingleObservable<int>

namespace ROOT {
   // Wrapper around operator delete
   static void delete_OccupancyObservable(void *p) {
      delete ((::OccupancyObservable*)p);
   }
   static void deleteArray_OccupancyObservable(void *p) {
      delete [] ((::OccupancyObservable*)p);
   }
   static void destruct_OccupancyObservable(void *p) {
      typedef ::OccupancyObservable current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::OccupancyObservable

namespace ROOT {
   // Wrapper around operator delete
   static void delete_DiskOccupancy(void *p) {
      delete ((::DiskOccupancy*)p);
   }
   static void deleteArray_DiskOccupancy(void *p) {
      delete [] ((::DiskOccupancy*)p);
   }
   static void destruct_DiskOccupancy(void *p) {
      typedef ::DiskOccupancy current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::DiskOccupancy

namespace ROOT {
   // Wrappers around operator new
   static void *new_SCTDiskOccupancy(void *p) {
      return  p ? new(p) ::SCTDiskOccupancy : new ::SCTDiskOccupancy;
   }
   static void *newArray_SCTDiskOccupancy(Long_t nElements, void *p) {
      return p ? new(p) ::SCTDiskOccupancy[nElements] : new ::SCTDiskOccupancy[nElements];
   }
   // Wrapper around operator delete
   static void delete_SCTDiskOccupancy(void *p) {
      delete ((::SCTDiskOccupancy*)p);
   }
   static void deleteArray_SCTDiskOccupancy(void *p) {
      delete [] ((::SCTDiskOccupancy*)p);
   }
   static void destruct_SCTDiskOccupancy(void *p) {
      typedef ::SCTDiskOccupancy current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::SCTDiskOccupancy

namespace ROOT {
   // Wrappers around operator new
   static void *new_SCTBarrelOccupancy(void *p) {
      return  p ? new(p) ::SCTBarrelOccupancy : new ::SCTBarrelOccupancy;
   }
   static void *newArray_SCTBarrelOccupancy(Long_t nElements, void *p) {
      return p ? new(p) ::SCTBarrelOccupancy[nElements] : new ::SCTBarrelOccupancy[nElements];
   }
   // Wrapper around operator delete
   static void delete_SCTBarrelOccupancy(void *p) {
      delete ((::SCTBarrelOccupancy*)p);
   }
   static void deleteArray_SCTBarrelOccupancy(void *p) {
      delete [] ((::SCTBarrelOccupancy*)p);
   }
   static void destruct_SCTBarrelOccupancy(void *p) {
      typedef ::SCTBarrelOccupancy current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::SCTBarrelOccupancy

namespace ROOT {
   // Wrappers around operator new
   static void *new_SCTBarrelPerEventOccupancy(void *p) {
      return  p ? new(p) ::SCTBarrelPerEventOccupancy : new ::SCTBarrelPerEventOccupancy;
   }
   static void *newArray_SCTBarrelPerEventOccupancy(Long_t nElements, void *p) {
      return p ? new(p) ::SCTBarrelPerEventOccupancy[nElements] : new ::SCTBarrelPerEventOccupancy[nElements];
   }
   // Wrapper around operator delete
   static void delete_SCTBarrelPerEventOccupancy(void *p) {
      delete ((::SCTBarrelPerEventOccupancy*)p);
   }
   static void deleteArray_SCTBarrelPerEventOccupancy(void *p) {
      delete [] ((::SCTBarrelPerEventOccupancy*)p);
   }
   static void destruct_SCTBarrelPerEventOccupancy(void *p) {
      typedef ::SCTBarrelPerEventOccupancy current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::SCTBarrelPerEventOccupancy

namespace ROOT {
   // Wrapper around operator delete
   static void delete_PerEventOccupancyObservable(void *p) {
      delete ((::PerEventOccupancyObservable*)p);
   }
   static void deleteArray_PerEventOccupancyObservable(void *p) {
      delete [] ((::PerEventOccupancyObservable*)p);
   }
   static void destruct_PerEventOccupancyObservable(void *p) {
      typedef ::PerEventOccupancyObservable current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::PerEventOccupancyObservable

namespace ROOT {
   // Wrappers around operator new
   static void *new_PixelBarrelPerEventOccupancy(void *p) {
      return  p ? new(p) ::PixelBarrelPerEventOccupancy : new ::PixelBarrelPerEventOccupancy;
   }
   static void *newArray_PixelBarrelPerEventOccupancy(Long_t nElements, void *p) {
      return p ? new(p) ::PixelBarrelPerEventOccupancy[nElements] : new ::PixelBarrelPerEventOccupancy[nElements];
   }
   // Wrapper around operator delete
   static void delete_PixelBarrelPerEventOccupancy(void *p) {
      delete ((::PixelBarrelPerEventOccupancy*)p);
   }
   static void deleteArray_PixelBarrelPerEventOccupancy(void *p) {
      delete [] ((::PixelBarrelPerEventOccupancy*)p);
   }
   static void destruct_PixelBarrelPerEventOccupancy(void *p) {
      typedef ::PixelBarrelPerEventOccupancy current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::PixelBarrelPerEventOccupancy

namespace ROOT {
   // Wrappers around operator new
   static void *new_PixelDiskPerEventOccupancy(void *p) {
      return  p ? new(p) ::PixelDiskPerEventOccupancy : new ::PixelDiskPerEventOccupancy;
   }
   static void *newArray_PixelDiskPerEventOccupancy(Long_t nElements, void *p) {
      return p ? new(p) ::PixelDiskPerEventOccupancy[nElements] : new ::PixelDiskPerEventOccupancy[nElements];
   }
   // Wrapper around operator delete
   static void delete_PixelDiskPerEventOccupancy(void *p) {
      delete ((::PixelDiskPerEventOccupancy*)p);
   }
   static void deleteArray_PixelDiskPerEventOccupancy(void *p) {
      delete [] ((::PixelDiskPerEventOccupancy*)p);
   }
   static void destruct_PixelDiskPerEventOccupancy(void *p) {
      typedef ::PixelDiskPerEventOccupancy current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::PixelDiskPerEventOccupancy

namespace ROOT {
   // Wrappers around operator new
   static void *new_PixelDiskOccupancy(void *p) {
      return  p ? new(p) ::PixelDiskOccupancy : new ::PixelDiskOccupancy;
   }
   static void *newArray_PixelDiskOccupancy(Long_t nElements, void *p) {
      return p ? new(p) ::PixelDiskOccupancy[nElements] : new ::PixelDiskOccupancy[nElements];
   }
   // Wrapper around operator delete
   static void delete_PixelDiskOccupancy(void *p) {
      delete ((::PixelDiskOccupancy*)p);
   }
   static void deleteArray_PixelDiskOccupancy(void *p) {
      delete [] ((::PixelDiskOccupancy*)p);
   }
   static void destruct_PixelDiskOccupancy(void *p) {
      typedef ::PixelDiskOccupancy current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::PixelDiskOccupancy

namespace ROOT {
   // Wrappers around operator new
   static void *new_PixelBarrelOccupancy(void *p) {
      return  p ? new(p) ::PixelBarrelOccupancy : new ::PixelBarrelOccupancy;
   }
   static void *newArray_PixelBarrelOccupancy(Long_t nElements, void *p) {
      return p ? new(p) ::PixelBarrelOccupancy[nElements] : new ::PixelBarrelOccupancy[nElements];
   }
   // Wrapper around operator delete
   static void delete_PixelBarrelOccupancy(void *p) {
      delete ((::PixelBarrelOccupancy*)p);
   }
   static void deleteArray_PixelBarrelOccupancy(void *p) {
      delete [] ((::PixelBarrelOccupancy*)p);
   }
   static void destruct_PixelBarrelOccupancy(void *p) {
      typedef ::PixelBarrelOccupancy current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::PixelBarrelOccupancy

namespace ROOT {
   // Wrappers around operator new
   static void *new_Occupancy2D(void *p) {
      return  p ? new(p) ::Occupancy2D : new ::Occupancy2D;
   }
   static void *newArray_Occupancy2D(Long_t nElements, void *p) {
      return p ? new(p) ::Occupancy2D[nElements] : new ::Occupancy2D[nElements];
   }
   // Wrapper around operator delete
   static void delete_Occupancy2D(void *p) {
      delete ((::Occupancy2D*)p);
   }
   static void deleteArray_Occupancy2D(void *p) {
      delete [] ((::Occupancy2D*)p);
   }
   static void destruct_Occupancy2D(void *p) {
      typedef ::Occupancy2D current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Occupancy2D

namespace ROOT {
   // Wrappers around operator new
   static void *new_PixelOccupancy2D(void *p) {
      return  p ? new(p) ::PixelOccupancy2D : new ::PixelOccupancy2D;
   }
   static void *newArray_PixelOccupancy2D(Long_t nElements, void *p) {
      return p ? new(p) ::PixelOccupancy2D[nElements] : new ::PixelOccupancy2D[nElements];
   }
   // Wrapper around operator delete
   static void delete_PixelOccupancy2D(void *p) {
      delete ((::PixelOccupancy2D*)p);
   }
   static void deleteArray_PixelOccupancy2D(void *p) {
      delete [] ((::PixelOccupancy2D*)p);
   }
   static void destruct_PixelOccupancy2D(void *p) {
      typedef ::PixelOccupancy2D current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::PixelOccupancy2D

namespace {
  void TriggerDictionaryInitialization_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisDict_gen_Impl() {
    static const char* headers[] = {
0    };
    static const char* includePaths[] = {
"/afs/cern.ch/user/a/altaylor/UpgradeITK_V2/InnerDetector/InDetPerformance/InDetUpgradePerformanceAnalysis",
"/cvmfs/atlas.cern.ch/repo/tools/slc6/cmt/InstallArea/include",
"/afs/cern.ch/user/a/altaylor/UpgradeITK_V2/InnerDetector/InDetPerformance/InstallArea/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasProduction/20.20.0.2/InstallArea/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasOffline/20.20.0/InstallArea/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasSimulation/20.20.0/InstallArea/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasAnalysis/20.20.0/InstallArea/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasTrigger/20.20.0/InstallArea/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasReconstruction/20.20.0/InstallArea/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasEvent/20.20.0/InstallArea/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasConditions/20.20.0/InstallArea/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasCore/20.20.0/InstallArea/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/DetCommon/20.20.0/InstallArea/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/GAUDI/v26r2p1-lcg81/InstallArea/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/LCGCMT/LCGCMT_81b/InstallArea/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasCore/20.20.0/InstallArea/include/AthAnalysisBaseComps",
"/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasCore/20.20.0/InstallArea/include/AthenaBaseComps",
"/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasCore/20.20.0/InstallArea/include/StoreGate",
"/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasCore/20.20.0/InstallArea/include/IOVDbDataModel",
"/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasCore/20.20.0/InstallArea/include/AthenaPoolUtilities",
"/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasCore/20.20.0/InstallArea/include/DBDataModel",
"/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasCore/20.20.0/InstallArea/include/DataModel",
"/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasCore/20.20.0/InstallArea/include/AthContainers",
"/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasCore/20.20.0/InstallArea/include/AthContainersInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasCore/20.20.0/InstallArea/include/CLIDSvc",
"/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasCore/20.20.0/InstallArea/include/AthLinks",
"/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasCore/20.20.0/InstallArea/include/SGTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasCore/20.20.0/InstallArea/include/AthenaKernel",
"/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasCore/20.20.0/InstallArea/include/CxxUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasCore/20.20.0/InstallArea/include/AthAllocators",
"/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasCore/20.20.0/InstallArea/include/AtlasBoost",
"/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasCore/20.20.0/InstallArea/include/AtlasBoost/AtlasBoost",
"/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasCore/20.20.0/InstallArea/include/DataModelRoot",
"/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasCore/20.20.0/AtlasCxxPolicy",
"/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/sw/lcg/releases/LCG_81b/ROOT/6.04.12/x86_64-slc6-gcc49-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/sw/lcg/releases/LCG_81b/ROOT/6.04.12/x86_64-slc6-gcc49-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/sw/lcg/releases/LCG_81b/CORAL/3_1_0/x86_64-slc6-gcc49-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/sw/lcg/releases/LCG_81b/Boost/1.59.0_python2.7/x86_64-slc6-gcc49-opt/include/boost-1_59",
"/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/sw/lcg/releases/LCG_81b/xrootd/4.2.3/x86_64-slc6-gcc49-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/DetCommon/20.20.0/AtlasCommonPolicy/src",
"/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/DetCommon/20.20.0/External/AtlasCompilers/src",
"/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/LCGCMT/LCGCMT_81b/LCG_Platforms/src",
"/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasCore/20.20.0/InstallArea/include/AtlasBoost/AtlasBoost",
"/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/sw/lcg/releases/LCG_81b/ROOT/6.04.12/x86_64-slc6-gcc49-opt/include",
"/afs/cern.ch/user/a/altaylor/UpgradeITK_V2/InnerDetector/InDetPerformance/InDetUpgradePerformanceAnalysis/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
class __attribute__((annotate("$clingAutoload$InDetUpgradePerformanceAnalysis/InDetUpgradeAnalysis.h")))  InDetUpgradeAnalysis;
class __attribute__((annotate("$clingAutoload$InDetUpgradePerformanceAnalysis/InDetUpgradeAnalysis.h")))  SingleAnalysis;
class __attribute__((annotate("$clingAutoload$InDetUpgradePerformanceAnalysis/InDetUpgradeAnalysis.h")))  ObservableGroup;
class __attribute__((annotate("$clingAutoload$InDetUpgradePerformanceAnalysis/InDetUpgradeAnalysis.h")))  Observable;
class __attribute__((annotate("$clingAutoload$InDetUpgradePerformanceAnalysis/SensorInfoBase.h")))  SensorInfoBase;
class __attribute__((annotate("$clingAutoload$InDetUpgradePerformanceAnalysis/DumpSensorInfo.h")))  DumpSensorInfo;
class __attribute__((annotate("$clingAutoload$InDetUpgradePerformanceAnalysis/TreeHouse.h")))  TreeHouse;
class __attribute__((annotate("$clingAutoload$InDetUpgradePerformanceAnalysis/StandardCuts.h")))  Cuts;
class __attribute__((annotate("$clingAutoload$InDetUpgradePerformanceAnalysis/AllCuts.h")))  AllCuts;
class __attribute__((annotate("$clingAutoload$InDetUpgradePerformanceAnalysis/StandardCuts.h")))  StandardCuts;
template <class T, class U> class __attribute__((annotate("$clingAutoload$InDetUpgradePerformanceAnalysis/MinMaxCut.h")))  RangeDependentMinCut;

template <class T> class __attribute__((annotate("$clingAutoload$InDetUpgradePerformanceAnalysis/MinMaxCut.h")))  MinCut;

template <class T> class __attribute__((annotate("$clingAutoload$InDetUpgradePerformanceAnalysis/MinMaxCut.h")))  MaxCut;

template <class T> class __attribute__((annotate("$clingAutoload$InDetUpgradePerformanceAnalysis/MinMaxCut.h")))  RangeCut;

template <class T> class __attribute__((annotate("$clingAutoload$InDetUpgradePerformanceAnalysis/MinMaxCut.h")))  RangeProductCut;

template <class T> class __attribute__((annotate("$clingAutoload$InDetUpgradePerformanceAnalysis/Distribution.h")))  Distribution;

template <class T> class __attribute__((annotate("$clingAutoload$InDetUpgradePerformanceAnalysis/Selector.h")))  Selector;

template <class T> class __attribute__((annotate("$clingAutoload$InDetUpgradePerformanceAnalysis/Selector.h")))  AbsSelector;

class __attribute__((annotate("$clingAutoload$InDetUpgradePerformanceAnalysis/MinimumHitsCut.h")))  MinimumHitsCut;
class __attribute__((annotate("$clingAutoload$InDetUpgradePerformanceAnalysis/SelectorWithIndex.h")))  AbsSelectorWithIndex;
class __attribute__((annotate("$clingAutoload$InDetUpgradePerformanceAnalysis/MinMaxCutWithIndex.h")))  RangeCutWithIndex;
class __attribute__((annotate("$clingAutoload$InDetUpgradePerformanceAnalysis/Efficiency.h")))  Efficiency;
class __attribute__((annotate("$clingAutoload$InDetUpgradePerformanceAnalysis/FakeRate.h")))  FakeRate;
class __attribute__((annotate("$clingAutoload$InDetUpgradePerformanceAnalysis/TrackRatio.h")))  TrackRatio;
class __attribute__((annotate("$clingAutoload$InDetUpgradePerformanceAnalysis/SingleObservable.h")))  Binning;
template <class observableT> class __attribute__((annotate("$clingAutoload$InDetUpgradePerformanceAnalysis/Calculator.h")))  Calculator;

template <class observableT> class __attribute__((annotate("$clingAutoload$InDetUpgradePerformanceAnalysis/Addition.h")))  Addition;

template <class observableT> class __attribute__((annotate("$clingAutoload$InDetUpgradePerformanceAnalysis/DerivedBranch.h")))  DerivedBranch;

template <class observableT> class __attribute__((annotate("$clingAutoload$InDetUpgradePerformanceAnalysis/SingleObservable.h")))  ResolutionObservable;

template <class observableT> class __attribute__((annotate("$clingAutoload$InDetUpgradePerformanceAnalysis/DiffObservable.h")))  DiffObservable;

template <class observableT> class __attribute__((annotate("$clingAutoload$InDetUpgradePerformanceAnalysis/PullObservable.h")))  PullObservable;

template <class observableT> class __attribute__((annotate("$clingAutoload$InDetUpgradePerformanceAnalysis/Z0SinThetaObs.h")))  Z0SinThetaObs;

template <class observableT> class __attribute__((annotate("$clingAutoload$InDetUpgradePerformanceAnalysis/QOverPtObs.h")))  QOverPtObs;

template <class observableT> class __attribute__((annotate("$clingAutoload$InDetUpgradePerformanceAnalysis/SingleObservable.h")))  SingleObservable;

class __attribute__((annotate("$clingAutoload$InDetUpgradePerformanceAnalysis/SCTDiskOccupancy.h")))  OccupancyObservable;
class __attribute__((annotate("$clingAutoload$InDetUpgradePerformanceAnalysis/SCTDiskOccupancy.h")))  DiskOccupancy;
class __attribute__((annotate("$clingAutoload$InDetUpgradePerformanceAnalysis/SCTDiskOccupancy.h")))  SCTDiskOccupancy;
class __attribute__((annotate("$clingAutoload$InDetUpgradePerformanceAnalysis/SCTBarrelOccupancy.h")))  SCTBarrelOccupancy;
class __attribute__((annotate("$clingAutoload$InDetUpgradePerformanceAnalysis/SCTBarrelPerEventOccupancy.h")))  SCTBarrelPerEventOccupancy;
class __attribute__((annotate("$clingAutoload$InDetUpgradePerformanceAnalysis/SCTBarrelPerEventOccupancy.h")))  PerEventOccupancyObservable;
class __attribute__((annotate("$clingAutoload$InDetUpgradePerformanceAnalysis/PixelBarrelPerEventOccupancy.h")))  PixelBarrelPerEventOccupancy;
class __attribute__((annotate("$clingAutoload$InDetUpgradePerformanceAnalysis/PixelDiskPerEventOccupancy.h")))  PixelDiskPerEventOccupancy;
class __attribute__((annotate("$clingAutoload$InDetUpgradePerformanceAnalysis/PixelDiskOccupancy.h")))  PixelDiskOccupancy;
class __attribute__((annotate("$clingAutoload$InDetUpgradePerformanceAnalysis/PixelBarrelOccupancy.h")))  PixelBarrelOccupancy;
class __attribute__((annotate("$clingAutoload$InDetUpgradePerformanceAnalysis/Occupancy2D.h")))  Occupancy2D;
class __attribute__((annotate("$clingAutoload$InDetUpgradePerformanceAnalysis/PixelOccupancy2D.h")))  PixelOccupancy2D;
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef NDEBUG
  #define NDEBUG 1
#endif
#ifndef _GLIBCXX_PERMIT_BACKWARD_HASH
  #define _GLIBCXX_PERMIT_BACKWARD_HASH 1
#endif
#ifndef HAVE_PRETTY_FUNCTION
  #define HAVE_PRETTY_FUNCTION 1
#endif
#ifndef HAVE_LONG_LONG
  #define HAVE_LONG_LONG 1
#endif
#ifndef HAVE_BOOL
  #define HAVE_BOOL 1
#endif
#ifndef HAVE_EXPLICIT
  #define HAVE_EXPLICIT 1
#endif
#ifndef HAVE_MUTABLE
  #define HAVE_MUTABLE 1
#endif
#ifndef HAVE_SIGNED
  #define HAVE_SIGNED 1
#endif
#ifndef HAVE_TYPENAME
  #define HAVE_TYPENAME 1
#endif
#ifndef HAVE_NEW_STYLE_CASTS
  #define HAVE_NEW_STYLE_CASTS 1
#endif
#ifndef HAVE_DYNAMIC_CAST
  #define HAVE_DYNAMIC_CAST 1
#endif
#ifndef HAVE_TYPEID
  #define HAVE_TYPEID 1
#endif
#ifndef HAVE_ANSI_TEMPLATE_INSTANTIATION
  #define HAVE_ANSI_TEMPLATE_INSTANTIATION 1
#endif
#ifndef HAVE_TEMPLATE_DEFAULT_ARGS
  #define HAVE_TEMPLATE_DEFAULT_ARGS 1
#endif
#ifndef HAVE_BROKEN_TEMPLATE_RESCOPE
  #define HAVE_BROKEN_TEMPLATE_RESCOPE 1
#endif
#ifndef HAVE_TEMPLATE_NULL_ARGS
  #define HAVE_TEMPLATE_NULL_ARGS 1
#endif
#ifndef HAVE_TEMPLATE_NULL_SPEC
  #define HAVE_TEMPLATE_NULL_SPEC 1
#endif
#ifndef HAVE_TEMPLATE_PARTIAL_SPEC
  #define HAVE_TEMPLATE_PARTIAL_SPEC 1
#endif
#ifndef HAVE_MEMBER_TEMPLATES
  #define HAVE_MEMBER_TEMPLATES 1
#endif
#ifndef HAVE_ANSI_OPERATOR_ARROW
  #define HAVE_ANSI_OPERATOR_ARROW 1
#endif
#ifndef HAVE_NAMESPACES
  #define HAVE_NAMESPACES 1
#endif
#ifndef HAVE_NAMESPACE_STD
  #define HAVE_NAMESPACE_STD 1
#endif
#ifndef HAVE_NEW_IOSTREAMS
  #define HAVE_NEW_IOSTREAMS 1
#endif
#ifndef HAVE_OSTREAM_CHAR_OVERLOAD
  #define HAVE_OSTREAM_CHAR_OVERLOAD 1
#endif
#ifndef HAVE_ITERATOR_TRAITS
  #define HAVE_ITERATOR_TRAITS 1
#endif
#ifndef HAVE_ITERATOR
  #define HAVE_ITERATOR 1
#endif
#ifndef HAVE_REVERSE_ITERATOR_STYLE
  #define HAVE_REVERSE_ITERATOR_STYLE 1
#endif
#ifndef HAVE_CXX_STDC_HEADERS
  #define HAVE_CXX_STDC_HEADERS 1
#endif
#ifndef HAVE_64_BITS
  #define HAVE_64_BITS 1
#endif
#ifndef __IDENTIFIER_64BIT__
  #define __IDENTIFIER_64BIT__ 1
#endif
#ifndef _GLIBCXX_PERMIT_BACKWARD_HASH
  #define _GLIBCXX_PERMIT_BACKWARD_HASH 1
#endif
#ifndef PACKAGE_VERSION
  #define PACKAGE_VERSION "InDetUpgradePerformanceAnalysis-r734676"
#endif
#ifndef PACKAGE_VERSION_UQ
  #define PACKAGE_VERSION_UQ InDetUpgradePerformanceAnalysis-r734676
#endif
#ifndef NDEBUG
  #define NDEBUG 1
#endif
#ifndef _GNU_SOURCE
  #define _GNU_SOURCE 1
#endif
#ifndef GAUDI_V20_COMPAT
  #define GAUDI_V20_COMPAT 1
#endif
#ifndef BOOST_FILESYSTEM_VERSION
  #define BOOST_FILESYSTEM_VERSION 3
#endif
#ifndef BOOST_SPIRIT_USE_PHOENIX_V3
  #define BOOST_SPIRIT_USE_PHOENIX_V3 1
#endif
#ifndef ATLAS_GAUDI_V21
  #define ATLAS_GAUDI_V21 1
#endif
#ifndef NDEBUG
  #define NDEBUG 1
#endif
#ifndef HAVE_GAUDI_PLUGINSVC
  #define HAVE_GAUDI_PLUGINSVC 1
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#ifndef InDetUpgradePerformanceAnalysis_InDetUpgradePerformanceAnalysisDict_H
#define InDetUpgradePerformanceAnalysis_InDetUpgradePerformanceAnalysisDict_H 1
	
#include "InDetUpgradePerformanceAnalysis/InDetUpgradeAnalysis.h"
#include "InDetUpgradePerformanceAnalysis/DumpSensorInfo.h"
#include "InDetUpgradePerformanceAnalysis/SensorInfoBase.h"

#include "InDetUpgradePerformanceAnalysis/SingleObservable.h"
#include "InDetUpgradePerformanceAnalysis/DiffObservable.h"
#include "InDetUpgradePerformanceAnalysis/PullObservable.h"
#include "InDetUpgradePerformanceAnalysis/StandardCuts.h"
#include "InDetUpgradePerformanceAnalysis/InDetUpgradeAnalysis.h"
#include "InDetUpgradePerformanceAnalysis/Z0SinThetaObs.h"
#include "InDetUpgradePerformanceAnalysis/QOverPtObs.h"

#include "InDetUpgradePerformanceAnalysis/TreeHouse.h"
#include "InDetUpgradePerformanceAnalysis/AllCuts.h"
#include "InDetUpgradePerformanceAnalysis/MinMaxCut.h"
#include "InDetUpgradePerformanceAnalysis/MinMaxCutWithIndex.h"
#include "InDetUpgradePerformanceAnalysis/MinimumHitsCut.h"

#include "InDetUpgradePerformanceAnalysis/Selector.h"
#include "InDetUpgradePerformanceAnalysis/SelectorWithIndex.h"
#include "InDetUpgradePerformanceAnalysis/Efficiency.h"
#include "InDetUpgradePerformanceAnalysis/FakeRate.h"
#include "InDetUpgradePerformanceAnalysis/TrackRatio.h"

#include "InDetUpgradePerformanceAnalysis/SCTDiskOccupancy.h"
#include "InDetUpgradePerformanceAnalysis/SCTBarrelOccupancy.h"
#include "InDetUpgradePerformanceAnalysis/SCTBarrelPerEventOccupancy.h"
#include "InDetUpgradePerformanceAnalysis/PixelBarrelPerEventOccupancy.h"
#include "InDetUpgradePerformanceAnalysis/PixelDiskPerEventOccupancy.h"
#include "InDetUpgradePerformanceAnalysis/PixelDiskOccupancy.h"
#include "InDetUpgradePerformanceAnalysis/PixelBarrelOccupancy.h"
#include "InDetUpgradePerformanceAnalysis/Occupancy2D.h"
#include "InDetUpgradePerformanceAnalysis/PixelOccupancy2D.h"
#include "InDetUpgradePerformanceAnalysis/Distribution.h"
#include "InDetUpgradePerformanceAnalysis/Binning.h"

#include "InDetUpgradePerformanceAnalysis/Calculator.h"
#include "InDetUpgradePerformanceAnalysis/Addition.h"
#include "InDetUpgradePerformanceAnalysis/DerivedBranch.h"


#ifdef __GCCXML__
// GCCXML explicit template instantiation block
template class RangeDependentMinCut<float,float>;
template class RangeDependentMinCut<int,float>;
template class Selector<int>;
template class Selector<float>;
template class AbsSelector<int>;
template class AbsSelector<float>;
template class MinCut<int>;
template class MinCut<float>;
template class MaxCut<float>;
template class MaxCut<int>;
template class RangeCut<int> ;
template class RangeCut<float> ;
template class RangeProductCut<float> ;
template class Distribution<float> ;
template class Distribution<int> ;
template class ResolutionObservable<float>;
template class ResolutionObservable<int>;
template class DiffObservable<float>;
template class PullObservable<float>;
template class Z0SinThetaObs<float>;
template class QOverPtObs<float>;
template class SingleObservable<float>;
template class SingleObservable<int>;

template class Calculator<int>;
template class Calculator<float>;
template class Addition<int>;
template class Addition<float>;
//template class DerivedBranch<double>;
template class DerivedBranch<float>;

#endif

#endif //> not InDetUpgradePerformanceAnalysis_InDetUpgradePerformanceAnalysisDict_H

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"AbsSelector<float>", payloadCode, "@",
"AbsSelector<int>", payloadCode, "@",
"AbsSelectorWithIndex", payloadCode, "@",
"Addition<float>", payloadCode, "@",
"Addition<int>", payloadCode, "@",
"AllCuts", payloadCode, "@",
"Binning", payloadCode, "@",
"Calculator<float>", payloadCode, "@",
"Calculator<int>", payloadCode, "@",
"Cuts", payloadCode, "@",
"DerivedBranch<float>", payloadCode, "@",
"DiffObservable<float>", payloadCode, "@",
"DiskOccupancy", payloadCode, "@",
"Distribution<float>", payloadCode, "@",
"Distribution<int>", payloadCode, "@",
"DumpSensorInfo", payloadCode, "@",
"Efficiency", payloadCode, "@",
"FakeRate", payloadCode, "@",
"InDetUpgradeAnalysis", payloadCode, "@",
"MaxCut<float>", payloadCode, "@",
"MaxCut<int>", payloadCode, "@",
"MinCut<float>", payloadCode, "@",
"MinCut<int>", payloadCode, "@",
"MinimumHitsCut", payloadCode, "@",
"Observable", payloadCode, "@",
"ObservableGroup", payloadCode, "@",
"Occupancy2D", payloadCode, "@",
"OccupancyObservable", payloadCode, "@",
"PerEventOccupancyObservable", payloadCode, "@",
"PixelBarrelOccupancy", payloadCode, "@",
"PixelBarrelPerEventOccupancy", payloadCode, "@",
"PixelDiskOccupancy", payloadCode, "@",
"PixelDiskPerEventOccupancy", payloadCode, "@",
"PixelOccupancy2D", payloadCode, "@",
"PullObservable<float>", payloadCode, "@",
"QOverPtObs<float>", payloadCode, "@",
"RangeCut<float>", payloadCode, "@",
"RangeCut<int>", payloadCode, "@",
"RangeCutWithIndex", payloadCode, "@",
"RangeDependentMinCut<float,float>", payloadCode, "@",
"RangeDependentMinCut<int,float>", payloadCode, "@",
"RangeProductCut<float>", payloadCode, "@",
"ResolutionObservable<float>", payloadCode, "@",
"ResolutionObservable<int>", payloadCode, "@",
"SCTBarrelOccupancy", payloadCode, "@",
"SCTBarrelPerEventOccupancy", payloadCode, "@",
"SCTDiskOccupancy", payloadCode, "@",
"Selector<float>", payloadCode, "@",
"Selector<int>", payloadCode, "@",
"SensorInfoBase", payloadCode, "@",
"SingleAnalysis", payloadCode, "@",
"SingleObservable<float>", payloadCode, "@",
"SingleObservable<int>", payloadCode, "@",
"StandardCuts", payloadCode, "@",
"TrackRatio", payloadCode, "@",
"TreeHouse", payloadCode, "@",
"UpgPerfAna::AnalysisStep", payloadCode, "@",
"Z0SinThetaObs<float>", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisDict_gen",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisDict_gen_Impl, {}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisDict_gen_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisDict_gen() {
  TriggerDictionaryInitialization_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisDict_gen_Impl();
}
