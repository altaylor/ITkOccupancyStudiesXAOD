#-- start of make_header -----------------

#====================================
#  Document InDetUpgradePerformanceAnalysisGen
#
#   Generated Tue May 10 13:56:52 2016  by altaylor
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_InDetUpgradePerformanceAnalysisGen_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_InDetUpgradePerformanceAnalysisGen_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_InDetUpgradePerformanceAnalysisGen

InDetUpgradePerformanceAnalysis_tag = $(tag)

#cmt_local_tagfile_InDetUpgradePerformanceAnalysisGen = $(InDetUpgradePerformanceAnalysis_tag)_InDetUpgradePerformanceAnalysisGen.make
cmt_local_tagfile_InDetUpgradePerformanceAnalysisGen = $(bin)$(InDetUpgradePerformanceAnalysis_tag)_InDetUpgradePerformanceAnalysisGen.make

else

tags      = $(tag),$(CMTEXTRATAGS)

InDetUpgradePerformanceAnalysis_tag = $(tag)

#cmt_local_tagfile_InDetUpgradePerformanceAnalysisGen = $(InDetUpgradePerformanceAnalysis_tag).make
cmt_local_tagfile_InDetUpgradePerformanceAnalysisGen = $(bin)$(InDetUpgradePerformanceAnalysis_tag).make

endif

include $(cmt_local_tagfile_InDetUpgradePerformanceAnalysisGen)
#-include $(cmt_local_tagfile_InDetUpgradePerformanceAnalysisGen)

ifdef cmt_InDetUpgradePerformanceAnalysisGen_has_target_tag

cmt_final_setup_InDetUpgradePerformanceAnalysisGen = $(bin)setup_InDetUpgradePerformanceAnalysisGen.make
cmt_dependencies_in_InDetUpgradePerformanceAnalysisGen = $(bin)dependencies_InDetUpgradePerformanceAnalysisGen.in
#cmt_final_setup_InDetUpgradePerformanceAnalysisGen = $(bin)InDetUpgradePerformanceAnalysis_InDetUpgradePerformanceAnalysisGensetup.make
cmt_local_InDetUpgradePerformanceAnalysisGen_makefile = $(bin)InDetUpgradePerformanceAnalysisGen.make

else

cmt_final_setup_InDetUpgradePerformanceAnalysisGen = $(bin)setup.make
cmt_dependencies_in_InDetUpgradePerformanceAnalysisGen = $(bin)dependencies.in
#cmt_final_setup_InDetUpgradePerformanceAnalysisGen = $(bin)InDetUpgradePerformanceAnalysissetup.make
cmt_local_InDetUpgradePerformanceAnalysisGen_makefile = $(bin)InDetUpgradePerformanceAnalysisGen.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)InDetUpgradePerformanceAnalysissetup.make

#InDetUpgradePerformanceAnalysisGen :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'InDetUpgradePerformanceAnalysisGen'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = InDetUpgradePerformanceAnalysisGen/
#InDetUpgradePerformanceAnalysisGen::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------

InDetUpgradePerformanceAnalysisGen :: ${dict_dir}/InDetUpgradePerformanceAnalysisGenDictEnd.stamp

#Begin: create dir and first part of required files
${dict_dir}/InDetUpgradePerformanceAnalysisGenDictBegin.stamp ::   $(reflex_dictInDetUpgradePerformanceAnalysis_selection_file)  $(cmt_final_setup_InDetUpgradePerformanceAnalysisGen)
	@echo "----- BEGIN Dictionary selection.xml file generation for InDetUpgradePerformanceAnalysis -----"
	@echo "  Cleaning up InDetUpgradePerformanceAnalysisDict-related generated files " 
	@$(cleanup_silent) cd $(bin); /bin/rm -rf *InDetUpgradePerformanceAnalysisDict*.a
	@if test -d ${dict_dir}/InDetUpgradePerformanceAnalysis ; then echo "Remove ${dict_dir}/InDetUpgradePerformanceAnalysis" ; /bin/rm -rf ${dict_dir}/InDetUpgradePerformanceAnalysis*; fi
	@echo "  Create ${dict_dir}" ; mkdir -p ${dict_dir}/InDetUpgradePerformanceAnalysis; 
	@echo "  Copy $(reflex_dictInDetUpgradePerformanceAnalysis_selection_file) to ${dict_dir}/InDetUpgradePerformanceAnalysis_selection.xml "
	@grep -v "</lcgdict>" $(reflex_dictInDetUpgradePerformanceAnalysis_selection_file) >| ${dict_dir}/InDetUpgradePerformanceAnalysis_selection.xml 
	@if test ! -z "${reflex_dictInDetUpgradePerformanceAnalysis_navigables}" ; then \
	    class_list=`echo '$(reflex_dictInDetUpgradePerformanceAnalysis_navigables)' | sed -e "s/[ ][ ]*/' '/g" -e "s/^/'/"  -e "s/$$/'/" -e "s/''//g"`; \
	    for class_name in $${class_list} ; do  \
	        echo "  Add navigable: $${class_name} to selection file" ; \
	        $(cmtexe) expand model "<Navigable_selection.xml class=$${class_name}/>" >> ${dict_dir}/InDetUpgradePerformanceAnalysis_selection.xml ; \
	        $(cmtexe) expand model "<ElementLink_selection.xml class=$${class_name}/>" >> ${dict_dir}/InDetUpgradePerformanceAnalysis_selection.xml ; \
	        $(cmtexe) expand model "<ElementLinkVector_selection.xml class=$${class_name}/>" >> ${dict_dir}/InDetUpgradePerformanceAnalysis_selection.xml ; \
	        $(cmtexe) expand model "<DataLink_selection.xml class=$${class_name}/>" >> ${dict_dir}/InDetUpgradePerformanceAnalysis_selection.xml ; \
	    done ; \
	fi 
	@if test ! -z "${reflex_dictInDetUpgradePerformanceAnalysis_data_links}" ; then \
	    class_list=`echo '$(reflex_dictInDetUpgradePerformanceAnalysis_data_links)' | sed -e "s/[ ][ ]*/' '/g" -e "s/^/'/"  -e "s/$$/'/" -e "s/''//g"`; \
	    for class_name in $${class_list} ; do  \
	        echo "  Add data link: $${class_name} to selection file" ; \
	        $(cmtexe) expand model "<DataLink_selection.xml class=$${class_name}/>" >> ${dict_dir}/InDetUpgradePerformanceAnalysis_selection.xml ; \
	    done ; \
	fi 
	@if test ! -z "${reflex_dictInDetUpgradePerformanceAnalysis_element_links}" ; then \
	    class_list=`echo '$(reflex_dictInDetUpgradePerformanceAnalysis_element_links)' | sed -e "s/[ ][ ]*/' '/g" -e "s/^/'/"  -e "s/$$/'/" -e "s/''//g"`; \
	    for class_name in $${class_list} ; do  \
	        echo "  Add element link: $${class_name} to selection file" ; \
	        $(cmtexe) expand model "<ElementLink_selection.xml class=$${class_name}/>" >> ${dict_dir}/InDetUpgradePerformanceAnalysis_selection.xml ; \
	        $(cmtexe) expand model "<ElementLinkVector_selection.xml class=$${class_name}/>" >> ${dict_dir}/InDetUpgradePerformanceAnalysis_selection.xml ; \
	        $(cmtexe) expand model "<DataLink_selection.xml class=$${class_name}/>" >> ${dict_dir}/InDetUpgradePerformanceAnalysis_selection.xml ; \
	    done ; \
	fi 
	@if test ! -z ${reflex_dictInDetUpgradePerformanceAnalysis_element_link_vectors} ; then \
	    class_list=`echo '$(reflex_dictInDetUpgradePerformanceAnalysis_element_link_vectors)' | sed -e "s/[ ][ ]*/' '/g" -e "s/^/'/"  -e "s/$$/'/" -e "s/''//g"`; \
	    for class_name in $${class_list} ; do  \
	        echo "  This is deprecated. Include the class in navigables if possible, or just use elementLinks" ; \
	        echo "  Add element link vector: $${class_name} to selection file" ; \
	        $(cmtexe) expand model "<ElementLinkVector_selection.xml class=$${class_name}/>" >> ${dict_dir}/InDetUpgradePerformanceAnalysis_selection.xml ; \
	        $(cmtexe) expand model "<DataLink_selection.xml class=$${class_name}/>" >> ${dict_dir}/InDetUpgradePerformanceAnalysis_selection.xml ; \
	    done ; \
	fi 
	@if test ! -z ${reflex_dictInDetUpgradePerformanceAnalysis_extra_selection} ; then \
            package=$(reflex_dictInDetUpgradePerformanceAnalysis_package) ; \
            sel_list="$(reflex_dictInDetUpgradePerformanceAnalysis_extra_selection)" ; \
	    for sel_name in $${sel_list} ; do  \
                cat ../$${package}/$${sel_name} >> ${dict_dir}/InDetUpgradePerformanceAnalysis_selection.xml ; \
	    done ; \
	fi 
	@echo "</lcgdict>" >> ${dict_dir}/InDetUpgradePerformanceAnalysis_selection.xml 
	@touch ${dict_dir}/InDetUpgradePerformanceAnalysisGenDictBegin.stamp

#End: create final part of required files
${dict_dir}/InDetUpgradePerformanceAnalysisGenDictEnd.stamp :: ${dict_dir}/InDetUpgradePerformanceAnalysisGenDict.stamp
	@echo "----- END Dictionary selection.xml file generation for InDetUpgradePerformanceAnalysis -----"
	@touch ${dict_dir}/InDetUpgradePerformanceAnalysisGenDictEnd.stamp
#-- start of dependencies ------------------
ifneq ($(MAKECMDGOALS),InDetUpgradePerformanceAnalysisGenclean)
ifneq ($(MAKECMDGOALS),uninstall)

$(bin)InDetUpgradePerformanceAnalysisGen_dependencies.make : $(use_requirements) $(cmt_final_setup_InDetUpgradePerformanceAnalysisGen)
	$(echo) "(InDetUpgradePerformanceAnalysisGen.make) Rebuilding $@"; \
	  $(cmtexe) -tag=$(tags) build dependencies -out=$@ -start_all ../InDetUpgradePerformanceAnalysis/InDetUpgradePerformanceAnalysisDict.h -end_all $(includes) $(app_InDetUpgradePerformanceAnalysisGen_cppflags) $(lib_InDetUpgradePerformanceAnalysisGen_cppflags) -name=InDetUpgradePerformanceAnalysisGen $? -f=$(cmt_dependencies_in_InDetUpgradePerformanceAnalysisGen) -without_cmt

-include $(bin)InDetUpgradePerformanceAnalysisGen_dependencies.make

endif
endif

InDetUpgradePerformanceAnalysisGenclean ::
	$(cleanup_silent) \rm -rf $(bin)InDetUpgradePerformanceAnalysisGen_deps $(bin)InDetUpgradePerformanceAnalysisGen_dependencies.make
#-- end of dependencies -------------------
#-- start of dependency ------------------
ifneq ($(MAKECMDGOALS),InDetUpgradePerformanceAnalysisGenclean)
ifneq ($(MAKECMDGOALS),uninstall)

$(bin)InDetUpgradePerformanceAnalysisGen_dependencies.make : $(InDetUpgradePerformanceAnalysisDict_h_dependencies)

$(bin)InDetUpgradePerformanceAnalysisGen_dependencies.make : ../InDetUpgradePerformanceAnalysis/InDetUpgradePerformanceAnalysisDict.h

endif
endif
#-- end of dependency -------------------

${dict_dir}/InDetUpgradePerformanceAnalysisGenDict.stamp :: ${dict_dir}/InDetUpgradePerformanceAnalysisGenDictBegin.stamp ${dict_dir}/InDetUpgradePerformanceAnalysis/InDetUpgradePerformanceAnalysisDict.xml.stamp
	@touch ${dict_dir}/InDetUpgradePerformanceAnalysisGenDict.stamp


InDetUpgradePerformanceAnalysisGen ::  ${dict_dir}/InDetUpgradePerformanceAnalysis/InDetUpgradePerformanceAnalysisDict.xml.stamp

${dict_dir}/InDetUpgradePerformanceAnalysis/InDetUpgradePerformanceAnalysisDict.xml.stamp :: $(InDetUpgradePerformanceAnalysisDict_h_dependencies) $(reflex_dictInDetUpgradePerformanceAnalysis_selection_file) ${dict_dir}/InDetUpgradePerformanceAnalysisGenDictBegin.stamp
	@$(silent) echo $@ InDetUpgradePerformanceAnalysisDict_gen.h
	@cat ../InDetUpgradePerformanceAnalysis/InDetUpgradePerformanceAnalysisDict.h >| ${dict_dir}/InDetUpgradePerformanceAnalysis/InDetUpgradePerformanceAnalysisDict_gen.h
	@if test ! -z "${reflex_dictInDetUpgradePerformanceAnalysis_navigables}" ; then \
	    class_list=`echo '$(reflex_dictInDetUpgradePerformanceAnalysis_navigables)' | sed -e "s/[ ][ ]*/' '/g" -e "s/^/'/"  -e "s/$$/'/" -e "s/''//g"`; \
            echo "class_list = $${class_list}";\
	    for class_name in $${class_list} ; do  \
	        echo "  Add navigable: $${class_name} to dict header file" ; \
		TYPE=`echo $${class_name} | sed 's/[[:punct:]]/_/g'` ;	\
	        $(cmtexe) expand model "<NavigableDict.h namespace=$${TYPE} cont=$${class_name}/>" >> ${dict_dir}/InDetUpgradePerformanceAnalysis/InDetUpgradePerformanceAnalysisDict_gen.h ; \
	        $(cmtexe) expand model "<ElementLinkDict.h namespace=$${TYPE} cont=$${class_name}/>" >> ${dict_dir}/InDetUpgradePerformanceAnalysis/InDetUpgradePerformanceAnalysisDict_gen.h ; \
	        $(cmtexe) expand model "<DataLinkDict.h namespace=$${TYPE} cont=$${class_name}/>" >> ${dict_dir}/InDetUpgradePerformanceAnalysis/InDetUpgradePerformanceAnalysisDict_gen.h ; \
	    done ; \
	fi 
	@if test ! -z "${reflex_dictInDetUpgradePerformanceAnalysis_element_links}" ; then \
	    class_list=`echo '$(reflex_dictInDetUpgradePerformanceAnalysis_element_links)' | sed -e "s/[ ][ ]*/' '/g" -e "s/^/'/"  -e "s/$$/'/" -e "s/''//g"`; \
	    for class_name in $${class_list} ; do  \
	        echo "  Add ElementLink & ElementLinkVector: $${class_name} to dict header file" ; \
		TYPE=`echo $${class_name} | sed 's/[[:punct:]]/_/g'` ;	\
	        $(cmtexe) expand model "<ElementLinkDict.h namespace=$${TYPE} cont=$${class_name}/>" >> ${dict_dir}/InDetUpgradePerformanceAnalysis/InDetUpgradePerformanceAnalysisDict_gen.h ; \
	        $(cmtexe) expand model "<DataLinkDict.h namespace=$${TYPE} cont=$${class_name}/>" >> ${dict_dir}/InDetUpgradePerformanceAnalysis/InDetUpgradePerformanceAnalysisDict_gen.h ; \
	    done ; \
	fi 
	@if test ! -z "${reflex_dictInDetUpgradePerformanceAnalysis_data_links}" ; then \
	    class_list=`echo '$(reflex_dictInDetUpgradePerformanceAnalysis_data_links)' | sed -e "s/[ ][ ]*/' '/g" -e "s/^/'/"  -e "s/$$/'/" -e "s/''//g"`; \
	    for class_name in $${class_list} ; do  \
	        echo "  Add DataLink: $${class_name} to dict header file" ; \
		TYPE=`echo $${class_name} | sed 's/[[:punct:]]/_/g'` ;	\
	        $(cmtexe) expand model "<DataLinkDict.h namespace=$${TYPE} cont=$${class_name}/>" >> ${dict_dir}/InDetUpgradePerformanceAnalysis/InDetUpgradePerformanceAnalysisDict_gen.h ; \
	    done ; \
	fi
	@if test ! -z "$(gcc_user_flags)" ; then \
	    export  GCCXML_USER_FLAGS="$(gcc_user_flags)" ; \
	fi
        # using explicit dictionary source file name to work around root5.99 genreflex bug
	$(silent) $(genreflex_cmd) ${dict_dir}/InDetUpgradePerformanceAnalysis/InDetUpgradePerformanceAnalysisDict_gen.h -o ${dict_dir}/InDetUpgradePerformanceAnalysis/InDetUpgradePerformanceAnalysisDict_gen.cpp $(reflex_dictInDetUpgradePerformanceAnalysis_options) ${munged_reflex_dict_options} ${genreflex_options} $(InDetUpgradePerformanceAnalysis_reflex_dict_options)  $(includes) $(use_pp_cppflags) 
	@touch ${dict_dir}/InDetUpgradePerformanceAnalysis/InDetUpgradePerformanceAnalysisDict.xml.stamp


InDetUpgradePerformanceAnalysisGenclean ::
	@if test -d ${dict_dir} ; then \
	    file_list=`ls ${dict_dir}` ; \
	    for file_name in $${file_list} ; do  \
	        if test -f ${dict_dir}/$${file_name} ; then \
	            echo "Remove ${dict_dir}/$${file_name}" ; \
		    /bin/rm -f ${dict_dir}/$${file_name} ; \
		fi  \
	    done ; \
	fi 

#-- start of cleanup_header --------------

clean :: InDetUpgradePerformanceAnalysisGenclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(InDetUpgradePerformanceAnalysisGen.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

InDetUpgradePerformanceAnalysisGenclean ::
#-- end of cleanup_header ---------------
