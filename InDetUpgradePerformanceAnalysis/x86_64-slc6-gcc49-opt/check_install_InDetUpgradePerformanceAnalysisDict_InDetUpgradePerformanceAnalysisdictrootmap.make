#-- start of make_header -----------------

#====================================
#  Document check_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap
#
#   Generated Tue May 10 13:57:03 2016  by altaylor
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_check_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_check_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_check_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap

InDetUpgradePerformanceAnalysis_tag = $(tag)

#cmt_local_tagfile_check_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap = $(InDetUpgradePerformanceAnalysis_tag)_check_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap.make
cmt_local_tagfile_check_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap = $(bin)$(InDetUpgradePerformanceAnalysis_tag)_check_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap.make

else

tags      = $(tag),$(CMTEXTRATAGS)

InDetUpgradePerformanceAnalysis_tag = $(tag)

#cmt_local_tagfile_check_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap = $(InDetUpgradePerformanceAnalysis_tag).make
cmt_local_tagfile_check_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap = $(bin)$(InDetUpgradePerformanceAnalysis_tag).make

endif

include $(cmt_local_tagfile_check_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap)
#-include $(cmt_local_tagfile_check_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap)

ifdef cmt_check_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap_has_target_tag

cmt_final_setup_check_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap = $(bin)setup_check_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap.make
cmt_dependencies_in_check_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap = $(bin)dependencies_check_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap.in
#cmt_final_setup_check_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap = $(bin)InDetUpgradePerformanceAnalysis_check_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmapsetup.make
cmt_local_check_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap_makefile = $(bin)check_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap.make

else

cmt_final_setup_check_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap = $(bin)setup.make
cmt_dependencies_in_check_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap = $(bin)dependencies.in
#cmt_final_setup_check_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap = $(bin)InDetUpgradePerformanceAnalysissetup.make
cmt_local_check_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap_makefile = $(bin)check_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)InDetUpgradePerformanceAnalysissetup.make

#check_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'check_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = check_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap/
#check_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#-- start of cmt_action_runner_header ---------------

ifdef ONCE
check_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap_once = 1
endif

ifdef check_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap_once

check_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmapactionstamp = $(bin)check_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap.actionstamp
#check_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmapactionstamp = check_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap.actionstamp

check_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap :: $(check_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmapactionstamp)
	$(echo) "check_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap ok"
#	@echo check_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap ok

#$(check_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmapactionstamp) :: $(check_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap_dependencies)
$(check_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmapactionstamp) ::
	$(silent) /cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasCore/20.20.0/External/ExternalPolicy/cmt/atlas_check_installations.sh -files=' ../x86_64-slc6-gcc49-opt/dict/InDetUpgradePerformanceAnalysis/InDetUpgradePerformanceAnalysisDict.dsomap ' -installdir=/afs/cern.ch/user/a/altaylor/UpgradeITK_V2/InnerDetector/InDetPerformance/InstallArea/x86_64-slc6-gcc49-opt/lib -level=
	$(silent) cat /dev/null > $(check_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmapactionstamp)
#	@echo ok > $(check_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmapactionstamp)

check_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmapclean ::
	$(cleanup_silent) /bin/rm -f $(check_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmapactionstamp)

else

#check_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap :: $(check_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap_dependencies)
check_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap ::
	$(silent) /cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasCore/20.20.0/External/ExternalPolicy/cmt/atlas_check_installations.sh -files=' ../x86_64-slc6-gcc49-opt/dict/InDetUpgradePerformanceAnalysis/InDetUpgradePerformanceAnalysisDict.dsomap ' -installdir=/afs/cern.ch/user/a/altaylor/UpgradeITK_V2/InnerDetector/InDetPerformance/InstallArea/x86_64-slc6-gcc49-opt/lib -level=

endif

install ::
uninstall ::

#-- end of cmt_action_runner_header -----------------
#-- start of cleanup_header --------------

clean :: check_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmapclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(check_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

check_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmapclean ::
#-- end of cleanup_header ---------------
