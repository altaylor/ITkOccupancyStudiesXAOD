#-- start of make_header -----------------

#====================================
#  Document install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap
#
#   Generated Tue May 10 13:57:04 2016  by altaylor
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap

InDetUpgradePerformanceAnalysis_tag = $(tag)

#cmt_local_tagfile_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap = $(InDetUpgradePerformanceAnalysis_tag)_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap.make
cmt_local_tagfile_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap = $(bin)$(InDetUpgradePerformanceAnalysis_tag)_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap.make

else

tags      = $(tag),$(CMTEXTRATAGS)

InDetUpgradePerformanceAnalysis_tag = $(tag)

#cmt_local_tagfile_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap = $(InDetUpgradePerformanceAnalysis_tag).make
cmt_local_tagfile_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap = $(bin)$(InDetUpgradePerformanceAnalysis_tag).make

endif

include $(cmt_local_tagfile_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap)
#-include $(cmt_local_tagfile_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap)

ifdef cmt_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap_has_target_tag

cmt_final_setup_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap = $(bin)setup_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap.make
cmt_dependencies_in_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap = $(bin)dependencies_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap.in
#cmt_final_setup_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap = $(bin)InDetUpgradePerformanceAnalysis_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmapsetup.make
cmt_local_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap_makefile = $(bin)install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap.make

else

cmt_final_setup_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap = $(bin)setup.make
cmt_dependencies_in_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap = $(bin)dependencies.in
#cmt_final_setup_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap = $(bin)InDetUpgradePerformanceAnalysissetup.make
cmt_local_install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap_makefile = $(bin)install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)InDetUpgradePerformanceAnalysissetup.make

#install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap/
#install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------


ifeq ($(INSTALLAREA),)
installarea = $(CMTINSTALLAREA)
else
ifeq ($(findstring `,$(INSTALLAREA)),`)
installarea = $(shell $(subst `,, $(INSTALLAREA)))
else
installarea = $(INSTALLAREA)
endif
endif

install_dir = ${installarea}/x86_64-slc6-gcc49-opt/lib

install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap :: install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmapinstall ;

install :: install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmapinstall ;

install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmapclean :: install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmapuninstall

uninstall :: install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmapuninstall


# This is to avoid error in case there are no files to install
# Ideally, the fragment should not be used without files to install,
# and this line should be dropped then
install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmapinstall :: ;

InDetUpgradePerformanceAnalysisDict_dsomap_dependencies = ../x86_64-slc6-gcc49-opt/dict/InDetUpgradePerformanceAnalysis/InDetUpgradePerformanceAnalysisDict.dsomap


install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmapinstall :: ${install_dir}/InDetUpgradePerformanceAnalysisDict.dsomap ;

${install_dir}/InDetUpgradePerformanceAnalysisDict.dsomap :: ../x86_64-slc6-gcc49-opt/dict/InDetUpgradePerformanceAnalysis/InDetUpgradePerformanceAnalysisDict.dsomap
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../x86_64-slc6-gcc49-opt/dict/InDetUpgradePerformanceAnalysis/InDetUpgradePerformanceAnalysisDict.dsomap`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_install_action) "$${d}" "InDetUpgradePerformanceAnalysisDict.dsomap" "$(install_dir)" "/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/AtlasCore/20.20.0/External/ExternalPolicy/src/symlink.sh" "$($(package)_cmtpath)"; \
	fi

../x86_64-slc6-gcc49-opt/dict/InDetUpgradePerformanceAnalysis/InDetUpgradePerformanceAnalysisDict.dsomap : ;

install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmapuninstall ::
	@if test ! "${installarea}" = ""; then \
	  d=`dirname ../x86_64-slc6-gcc49-opt/dict/InDetUpgradePerformanceAnalysis/InDetUpgradePerformanceAnalysisDict.dsomap`; \
	  d=`(cd $${d}; pwd)`; \
	  CMTINSTALLAREA=${CMTINSTALLAREA}; export CMTINSTALLAREA; \
	  $(cmt_uninstall_action) "$${d}" "InDetUpgradePerformanceAnalysisDict.dsomap" "$(install_dir)" "$($(package)_cmtpath)"; \
	fi
#-- start of cleanup_header --------------

clean :: install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmapclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmap.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

install_InDetUpgradePerformanceAnalysisDict_InDetUpgradePerformanceAnalysisdictrootmapclean ::
#-- end of cleanup_header ---------------
