#-- start of make_header -----------------

#====================================
#  Application UpgradePerformanceAnalysis
#
#   Generated Tue May 10 13:57:18 2016  by altaylor
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_UpgradePerformanceAnalysis_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_UpgradePerformanceAnalysis_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_UpgradePerformanceAnalysis

InDetUpgradePerformanceAnalysis_tag = $(tag)

#cmt_local_tagfile_UpgradePerformanceAnalysis = $(InDetUpgradePerformanceAnalysis_tag)_UpgradePerformanceAnalysis.make
cmt_local_tagfile_UpgradePerformanceAnalysis = $(bin)$(InDetUpgradePerformanceAnalysis_tag)_UpgradePerformanceAnalysis.make

else

tags      = $(tag),$(CMTEXTRATAGS)

InDetUpgradePerformanceAnalysis_tag = $(tag)

#cmt_local_tagfile_UpgradePerformanceAnalysis = $(InDetUpgradePerformanceAnalysis_tag).make
cmt_local_tagfile_UpgradePerformanceAnalysis = $(bin)$(InDetUpgradePerformanceAnalysis_tag).make

endif

include $(cmt_local_tagfile_UpgradePerformanceAnalysis)
#-include $(cmt_local_tagfile_UpgradePerformanceAnalysis)

ifdef cmt_UpgradePerformanceAnalysis_has_target_tag

cmt_final_setup_UpgradePerformanceAnalysis = $(bin)setup_UpgradePerformanceAnalysis.make
cmt_dependencies_in_UpgradePerformanceAnalysis = $(bin)dependencies_UpgradePerformanceAnalysis.in
#cmt_final_setup_UpgradePerformanceAnalysis = $(bin)InDetUpgradePerformanceAnalysis_UpgradePerformanceAnalysissetup.make
cmt_local_UpgradePerformanceAnalysis_makefile = $(bin)UpgradePerformanceAnalysis.make

else

cmt_final_setup_UpgradePerformanceAnalysis = $(bin)setup.make
cmt_dependencies_in_UpgradePerformanceAnalysis = $(bin)dependencies.in
#cmt_final_setup_UpgradePerformanceAnalysis = $(bin)InDetUpgradePerformanceAnalysissetup.make
cmt_local_UpgradePerformanceAnalysis_makefile = $(bin)UpgradePerformanceAnalysis.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)InDetUpgradePerformanceAnalysissetup.make

#UpgradePerformanceAnalysis :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'UpgradePerformanceAnalysis'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = UpgradePerformanceAnalysis/
#UpgradePerformanceAnalysis::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#-- start of application_header

UpgradePerformanceAnalysis :: dirs  $(bin)UpgradePerformanceAnalysis${application_suffix}
	$(echo) "UpgradePerformanceAnalysis ok"

#-- end of application_header
#-- start of application

$(bin)UpgradePerformanceAnalysis${application_suffix} :: $(bin)res_analysis.o $(use_stamps) $(UpgradePerformanceAnalysis_stamps) $(UpgradePerformanceAnalysisstamps) $(use_requirements)
	$(link_echo) "application $@"
	$(link_silent) $(cpplink) -o $(@).new $(bin)res_analysis.o $(cmt_installarea_linkopts) $(UpgradePerformanceAnalysis_use_linkopts) $(UpgradePerformanceAnalysislinkopts) && mv -f $(@).new $(@)
ifneq (${SEPARATEDEBUG},)
	$(echo) stripping dbg symbols into separate file $@$(debuginfosuffix)
	$(link_silent) objcopy --only-keep-debug $@ $@$(debuginfosuffix)
	$(link_silent) objcopy --strip-debug $@
	$(link_silent) cd $(@D) && objcopy --add-gnu-debuglink=$(@F)$(debuginfosuffix) $(@F)
endif

ifneq ($(strip $(use_stamps)),)
# Work around Make errors if stamps files do not exist
$(use_stamps) :
endif

#-----------------------------------------------------------------
#
#  New section for automatic installation
#
#-----------------------------------------------------------------

install_dir = ${CMTINSTALLAREA}/$(tag)/bin
UpgradePerformanceAnalysisinstallname = UpgradePerformanceAnalysis${application_suffix}

UpgradePerformanceAnalysis :: UpgradePerformanceAnalysisinstall ;

install :: UpgradePerformanceAnalysisinstall ;

UpgradePerformanceAnalysisinstall :: $(install_dir)/$(UpgradePerformanceAnalysisinstallname)
ifdef CMTINSTALLAREA
	$(echo) "installation done"
endif

$(install_dir)/$(UpgradePerformanceAnalysisinstallname) :: $(bin)$(UpgradePerformanceAnalysisinstallname)
ifdef CMTINSTALLAREA
	$(install_silent) $(cmt_install_action) \
	    -source "`(cd $(bin); pwd)`" \
	    -name "$(UpgradePerformanceAnalysisinstallname)" \
	    -out "$(install_dir)" \
	    -cmd "$(cmt_installarea_command)" \
	    -cmtpath "$($(package)_cmtpath)"
endif

##UpgradePerformanceAnalysisclean :: UpgradePerformanceAnalysisuninstall

uninstall :: UpgradePerformanceAnalysisuninstall ;

UpgradePerformanceAnalysisuninstall ::
ifdef CMTINSTALLAREA
	$(cleanup_silent) $(cmt_uninstall_action) \
	    -source "`(cd $(bin); pwd)`" \
	    -name "$(UpgradePerformanceAnalysisinstallname)" \
	    -out "$(install_dir)" \
	    -cmtpath "$($(package)_cmtpath)"
endif

#	@echo "------> (UpgradePerformanceAnalysis.make) Removing installed files"
#-- end of application
#-- start of cpp ------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),UpgradePerformanceAnalysisclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)res_analysis.d

$(bin)$(binobj)res_analysis.d :

$(bin)$(binobj)res_analysis.o : $(cmt_final_setup_UpgradePerformanceAnalysis)

$(bin)$(binobj)res_analysis.o : $(src)res_analysis.cpp
	$(cpp_echo) $(src)res_analysis.cpp
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(UpgradePerformanceAnalysis_pp_cppflags) $(app_UpgradePerformanceAnalysis_pp_cppflags) $(res_analysis_pp_cppflags) $(use_cppflags) $(UpgradePerformanceAnalysis_cppflags) $(app_UpgradePerformanceAnalysis_cppflags) $(res_analysis_cppflags) $(res_analysis_cpp_cppflags)  $(src)res_analysis.cpp
endif
endif

else
$(bin)UpgradePerformanceAnalysis_dependencies.make : $(res_analysis_cpp_dependencies)

$(bin)UpgradePerformanceAnalysis_dependencies.make : $(src)res_analysis.cpp

$(bin)$(binobj)res_analysis.o : $(res_analysis_cpp_dependencies)
	$(cpp_echo) $(src)res_analysis.cpp
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(UpgradePerformanceAnalysis_pp_cppflags) $(app_UpgradePerformanceAnalysis_pp_cppflags) $(res_analysis_pp_cppflags) $(use_cppflags) $(UpgradePerformanceAnalysis_cppflags) $(app_UpgradePerformanceAnalysis_cppflags) $(res_analysis_cppflags) $(res_analysis_cpp_cppflags)  $(src)res_analysis.cpp

endif

#-- end of cpp ------
#-- start of cleanup_header --------------

clean :: UpgradePerformanceAnalysisclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(UpgradePerformanceAnalysis.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

UpgradePerformanceAnalysisclean ::
#-- end of cleanup_header ---------------
#-- start of cleanup_application ------
	$(cleanup_echo) application UpgradePerformanceAnalysis
	-$(cleanup_silent) cd $(bin); /bin/rm -f UpgradePerformanceAnalysis${application_suffix}
#-- end of cleanup_application ------
#-- start of cleanup_objects ------
	$(cleanup_echo) objects UpgradePerformanceAnalysis
	-$(cleanup_silent) /bin/rm -f $(bin)res_analysis.o
	-$(cleanup_silent) /bin/rm -f $(patsubst %.o,%.d,$(bin)res_analysis.o) $(patsubst %.o,%.dep,$(bin)res_analysis.o) $(patsubst %.o,%.d.stamp,$(bin)res_analysis.o)
	-$(cleanup_silent) cd $(bin); /bin/rm -rf UpgradePerformanceAnalysis_deps UpgradePerformanceAnalysis_dependencies.make
#-- end of cleanup_objects ------
#-- start of check_application ------

check :: UpgradePerformanceAnalysischeck ;

#UpgradePerformanceAnalysischeck :: UpgradePerformanceAnalysis
UpgradePerformanceAnalysischeck ::
	$(silent) $(UpgradePerformanceAnalysis_pre_check)
	$(silent) $(bin)UpgradePerformanceAnalysis${application_suffix} $(UpgradePerformanceAnalysis_check_args)
	$(silent) $(UpgradePerformanceAnalysis_post_check)

#-- end of check_application ------
