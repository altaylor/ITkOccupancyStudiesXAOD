# import some useful constants for message level
import AthenaCommon.Constants as Lvl

# for job configuration: the application manager object
# as the name implies, there is only one application manager
from AthenaCommon.AppMgr import theApp

# for job configuration: the object to hold all services
# note: the real object is 'ServiceMgr' but we give it a shorter alias 'svcMgr'
from AthenaCommon.AppMgr import ServiceMgr as svcMgr

import AthenaPoolCnvSvc.ReadAthenaPool                   #sets up reading of POOL files (e.g. xAODs)
svcMgr.EventSelector.InputCollections=["/afs/cern.ch/work/a/altaylor/ITK_DATA/InclinedBarrel/tester.root"]   #insert your list of input files here

# for job configuration: the object to hold the sequence of algorithms to
# be run in turn
'''
from AthenaCommon.AlgSequence import AlgSequence
job = AlgSequence()

'''

algseq = CfgMgr.AthSequencer("AthAlgSeq")  

## schedule for 10 events
theApp.EvtMax = 10

# test if a THistSvc instance was already configured
if not hasattr (svcMgr, 'THistSvc'):
    # all automatically-generated configurables can be queried under
    # the 'CfgMgr' namespace
    svcMgr += CfgMgr.THistSvc()
    pass

# configure the job for writing out root files
# Note we used the '+=' operator and *NOT* the '=' operator
_thist_data = {
    'streamname' : "ostream",
    'filename'   : "ntuple.root",
    'mode'       : "RECREATE",
    }

svcMgr.THistSvc.Output += [
    "%(streamname)s DATAFILE='%(filename)s' TYP='ROOT' OPT='%(mode)s'" %
    _thist_data
    ]


## add our algorithm

'''

import MyPyAlg
job += MyPyAlg (                                                                                                       
   'tuple-writer',                                                                                                               
   ntuple_name = '/'.join(['',                                                                                                   
                           _thist_data['streamname'],                                                                            
                           'ntuples',                                                                                            
                           'ntup']),                                                                                             
   OutputLevel=Lvl.INFO                                                                                                          
   )


'''

from InDetUpgradePerformanceAnalysis.MyPyAlg import MyPyAlg
job += InDetUpgradePerformanceAnalysis.MyPyAlg (                                                                                                       
   'tuple-writer',                                                                                                               
   ntuple_name = '/'.join(['',                                                                                                   
                           _thist_data['streamname'],                                                                            
                           'ntuples',                                                                                            
                           'ntup']),                                                                                             
   OutputLevel=Lvl.INFO                                                                                                          
   )

      


