//===============================================//
// Chiara: re-write compiled version of the code //
//===============================================//

#include "TChain.h"
#include<vector>
#include "TMath.h"
#include "TString.h"
#include "TFile.h"
#include "TCut.h"
#include "TH1F.h"
#include "TH2D.h"
#include "TCanvas.h"
#include <iostream>
#include "TGraph.h"
#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>
#include <cmath>
#include <stdio.h>
#include <stdlib.h>
#include "TApplication.h"
#include "TStyle.h"
#include "TLegend.h"
	 
using namespace std;


void occInBoostedJets();


int main(int argc, char **argv){

  TApplication app("app", &argc, argv);

  std::string commandLine = argv[0];

  if(argc>1) cout << "===== FATAL:: something went wrong!! =====" << endl;
  else cout << "===== INFO:: executing command " << commandLine << endl;

  occInBoostedJets();
  app.Run();

  return 0;
}//int main(int argc, char **argv)


void occInBoostedJets(){

  TChain *Mychain = new TChain("btagd3pd","");

  //define constant quantities to be used

  //layer radii
  float const pixL0R = 37.;
  float const pixL1R = 75.;
  float const pixL2R = 156.139;
  float const pixL3R = 195.21;
  float const sctL0R = 380.;

  //layer z
  float const pixL0Z = 319.5;
  float const pixL1Z = 553.8;
  float const pixL2Z = 574.;
  float const pixL3Z = 574.;
  float const sctL0Z = 2357.;

  //number of staves
  int const pixL0NStaves = 16;
  int const pixL1NStaves = 32;
  int const pixL2NStaves = 32;
  int const pixL3NStaves = 40;
  int const sctL0NStaves = 28;

  //number of modules + 1 - eta modules counted starting always from 1 and not 0!!
  int const pixL0NMod = 31;
  int const pixL1NMod = 53;
  int const pixL2NMod = 33;
  int const pixL3NMod = 33;
  int const sctL0NMod = 25;

  //resolution and max x to be used in histograms
  int const nBins = 60; //running from R=0 to 0.3 (looking at jet core)
  float const xMax = 0.3;

  //pt cut to be applied on jets - in MeV
  float const ptCutJets = 800e3;

  //define number of sensors per layer
  float const nSensPixL0 = 336*80;
  float const nSensPixL1 = nSensPixL0;
  float const nSensPixL2 = 336*80*2*2;
  float const nSensPixL3 = nSensPixL2;
  float const nSensSctL0 = 1280*4;

  //compute maximal number of modules/staves and cast it to float

  //pixels - modules
  float pixMaxNMod = (float)pixL0NMod;
  if(pixMaxNMod<pixL1NMod) pixMaxNMod = (float)pixL1NMod;
  if(pixMaxNMod<pixL2NMod) pixMaxNMod = (float)pixL2NMod;
  if(pixMaxNMod<pixL3NMod) pixMaxNMod = (float)pixL3NMod;

  //pixel - staves
  float pixMaxNSt = (float)pixL0NStaves;
  if(pixMaxNSt<pixL1NStaves) pixMaxNSt = (float)pixL1NStaves;
  if(pixMaxNSt<pixL2NStaves) pixMaxNSt = (float)pixL2NStaves;
  if(pixMaxNSt<pixL3NStaves) pixMaxNSt = (float)pixL3NStaves;

  //sct - modules - for now looking only at 1st SCT layer
  float sctMaxNMod = (float)sctL0NMod;

  //sct - staves
  float sctMaxNSt = (float)sctL0NStaves;

  //define eta coordinate for each layer pix, sct
  float pixL0Eta = (-1)*log(tan((atan(pixL0R/pixL0Z))/2.));
  float pixL1Eta = (-1)*log(tan((atan(pixL1R/pixL1Z))/2.));
  float pixL2Eta = (-1)*log(tan((atan(pixL2R/pixL2Z))/2.));
  float pixL3Eta = (-1)*log(tan((atan(pixL3R/pixL3Z))/2.));
  
  float sctL0Eta = (-1)*log(tan((atan(sctL0R/sctL0Z))/2.));

  //add files to the chain
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00001.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00002.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00003.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00004.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00005.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00006.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00007.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00008.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00009.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00010.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00011.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00012.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00013.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00014.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00015.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00016.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00017.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00018.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00019.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00020.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00021.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00022.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00023.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00024.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00025.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00026.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00027.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00028.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00029.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00030.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00031.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00032.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00033.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00034.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00035.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00036.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00037.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00038.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00039.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00040.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00041.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00042.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00043.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00044.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00045.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00046.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00047.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00048.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00049.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00050.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00051.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00052.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00053.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00054.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00055.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00056.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00057.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00058.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00059.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00060.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00061.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00062.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00063.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00064.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00065.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00066.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00067.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00068.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00069.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00070.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00071.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00072.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00073.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00074.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00075.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00076.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00077.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00078.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00079.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00080.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00081.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00082.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00083.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00084.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00085.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00086.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00087.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00088.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00089.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00090.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00091.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00092.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00093.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00094.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00095.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00096.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00097.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00098.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00099.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00100.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00101.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00102.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00103.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00104.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00105.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00106.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00107.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00108.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00109.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00110.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00111.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00112.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00113.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00114.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00115.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00116.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00117.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00118.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00119.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00120.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00121.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00122.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00123.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00124.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00125.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00126.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00127.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00128.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00129.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00130.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00131.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00132.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00133.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00134.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00135.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00136.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00137.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00138.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00139.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00140.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00141.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00142.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00143.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00144.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00145.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00146.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00147.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00148.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00149.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00150.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00151.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00152.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00153.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00154.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00155.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00156.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00157.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00158.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00159.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00160.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00161.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00162.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00163.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00164.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00165.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00166.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00167.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00168.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00169.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00170.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00171.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00172.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00173.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00174.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00175.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00176.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00177.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00178.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00179.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00180.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00181.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00182.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00183.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00184.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00185.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00186.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00187.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00188.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00189.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00190.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00191.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00192.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00193.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00194.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00195.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00196.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00197.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00198.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00199.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00200.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00201.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00202.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00203.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00204.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00205.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00206.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00207.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00208.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00209.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00210.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00211.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00212.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00213.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00214.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00215.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00216.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00217.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00218.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00219.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00220.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00221.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00222.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00223.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00224.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00225.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00226.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00227.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00228.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00229.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00230.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00231.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00232.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00233.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00234.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00235.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00236.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00237.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00238.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00239.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00240.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00241.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00242.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00243.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00244.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00245.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00246.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00247.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00248.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00249.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00250.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00251.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00252.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00253.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00254.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00255.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00256.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00257.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00258.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00259.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00260.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00261.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00262.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00263.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00264.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00265.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00266.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00267.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00268.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00269.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00270.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00271.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00272.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00273.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00274.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00275.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00276.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00277.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00278.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00279.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00280.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00281.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00282.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00283.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00284.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00285.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00286.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00287.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00288.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00289.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00290.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00291.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00292.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00293.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00294.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00295.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00296.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00297.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00298.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00299.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00300.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00301.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00302.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00303.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00304.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00305.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00306.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00307.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00308.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00309.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00310.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00311.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00312.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00313.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00314.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00315.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00316.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00317.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00318.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00319.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00320.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00321.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00322.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00323.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00324.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00325.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00326.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00327.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00328.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00329.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00330.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00331.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00332.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00333.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00334.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00335.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00336.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00337.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00338.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00339.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00340.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00341.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00342.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00343.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00344.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00345.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00346.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00347.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00348.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00349.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00350.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00351.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00352.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00353.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00354.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00355.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00356.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00357.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00358.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00359.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00360.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00361.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00362.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00363.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00364.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00365.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00366.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00367.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00368.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00369.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00370.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00371.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00372.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00373.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00374.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00375.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00376.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00377.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00378.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00379.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00380.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00381.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00382.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00383.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00384.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00385.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00386.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00387.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00388.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00389.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00390.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00391.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00392.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00393.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00394.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00395.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00396.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00397.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00398.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00399.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00400.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00401.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00402.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00403.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00404.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00405.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00406.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00407.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00408.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00409.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00410.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00411.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00412.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00413.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00414.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00415.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00416.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00417.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00418.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00419.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00420.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00421.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00422.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00423.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00424.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00425.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00426.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00427.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00428.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00429.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00430.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00431.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00432.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00433.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00434.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00435.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00436.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00437.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00438.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00439.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00440.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00441.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00442.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00443.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00444.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00445.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00446.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00447.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00448.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00449.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00450.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00451.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00452.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00453.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00454.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00455.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00456.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00457.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00458.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00459.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00460.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00461.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00462.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00463.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00464.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00465.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00466.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00467.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00468.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00469.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00470.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00471.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00472.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00473.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00474.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00475.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00476.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00477.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00478.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00479.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00480.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00481.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00482.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00483.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00484.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00485.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00486.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00487.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00488.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00489.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00490.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00491.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00492.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00493.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00494.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00495.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00496.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00497.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00498.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00499.JetTagD3PD.root");
  Mychain->Add("/Disk/speyside7/Grid/grid-files/chiara/group.det-slhc.MC11.105249.pythia_bb500.rec.JetTagD3PD.large.120213154241/group.det-slhc.62673_000868.EXT0._00500.JetTagD3PD.root");

  //define output plots
  TH1F *pl_activeSens_pixL0 = new TH1F("pl_activeSens_pixL0", "", nBins, 0., xMax);
  TH1F *pl_activeSens_pixL1 = new TH1F("pl_activeSens_pixL1", "", nBins, 0., xMax);
  TH1F *pl_activeSens_pixL2 = new TH1F("pl_activeSens_pixL2", "", nBins, 0., xMax);
  TH1F *pl_activeSens_pixL3 = new TH1F("pl_activeSens_pixL3", "", nBins, 0., xMax);
  TH1F *pl_activeSens_sctL0 = new TH1F("pl_activeSens_sctL0", "", nBins, 0., xMax);

  TH1F *pl_activeSensRIndep_pixL0 = new TH1F("pl_activeSensRIndep_pixL0", "", nBins, 0., xMax);
  TH1F *pl_activeSensRIndep_pixL1 = new TH1F("pl_activeSensRIndep_pixL1", "", nBins, 0., xMax);
  TH1F *pl_activeSensRIndep_pixL2 = new TH1F("pl_activeSensRIndep_pixL2", "", nBins, 0., xMax);
  TH1F *pl_activeSensRIndep_pixL3 = new TH1F("pl_activeSensRIndep_pixL3", "", nBins, 0., xMax);
  TH1F *pl_activeSensRIndep_sctL0 = new TH1F("pl_activeSensRIndep_sctL0", "", nBins, 0., xMax);

  TH1F *pl_numSens_pixL0 = new TH1F("pl_numSens_pixL0", "", nBins, 0., xMax);
  TH1F *pl_numSens_pixL1 = new TH1F("pl_numSens_pixL1", "", nBins, 0., xMax);
  TH1F *pl_numSens_pixL2 = new TH1F("pl_numSens_pixL2", "", nBins, 0., xMax);
  TH1F *pl_numSens_pixL3 = new TH1F("pl_numSens_pixL3", "", nBins, 0., xMax);
  TH1F *pl_numSens_sctL0 = new TH1F("pl_numSens_sctL0", "", nBins, 0., xMax);

  TH1F *pl_occupancy_pixL0 = new TH1F("pl_occupancy_pixL0", "", nBins, 0., xMax);
  TH1F *pl_occupancy_pixL1 = new TH1F("pl_occupancy_pixL1", "", nBins, 0., xMax);
  TH1F *pl_occupancy_pixL2 = new TH1F("pl_occupancy_pixL2", "", nBins, 0., xMax);
  TH1F *pl_occupancy_pixL3 = new TH1F("pl_occupancy_pixL3", "", nBins, 0., xMax);
  TH1F *pl_occupancy_sctL0 = new TH1F("pl_occupancy_sctL0", "", nBins, 0., xMax);

  //define variables to be used
  //Pixel
  vector<int> *pixClus_size = 0;
  vector<float> *pixClus_x = 0;
  vector<float> *pixClus_y = 0;
  vector<float> *pixClus_z = 0;
  vector<char> *pixClus_phi_module = 0;
  vector<char> *pixClus_eta_module = 0;
  vector<char> *pixClus_layer = 0;
  vector<char> *pixClus_bec = 0;
  //SCT
  vector<float> *sctClus_size = 0;
  vector<float> *sctClus_x = 0;
  vector<float> *sctClus_y = 0;
  vector<float> *sctClus_z = 0;
  vector<int> *sctClus_phi_module = 0;
  vector<int> *sctClus_eta_module = 0;
  vector<int> *sctClus_layer = 0;
  vector<int> *sctClus_bec = 0;
  //jets
  vector<float> *jet_antikt4truth_eta = 0;
  vector<float> *jet_antikt4truth_phi = 0;
  vector<float> *jet_antikt4truth_pt = 0;

  //set branches
  //pixel
  Mychain->SetBranchAddress("pixClus_size", &pixClus_size);
  Mychain->SetBranchAddress("pixClus_x", &pixClus_x);
  Mychain->SetBranchAddress("pixClus_y", &pixClus_y);
  Mychain->SetBranchAddress("pixClus_z", &pixClus_z);
  Mychain->SetBranchAddress("pixClus_phi_module", &pixClus_phi_module);
  Mychain->SetBranchAddress("pixClus_eta_module", &pixClus_eta_module);
  Mychain->SetBranchAddress("pixClus_layer", &pixClus_layer);
  Mychain->SetBranchAddress("pixClus_bec", &pixClus_bec);
  //SCT
  Mychain->SetBranchAddress("sctClus_size", &sctClus_size);
  Mychain->SetBranchAddress("sctClus_x", &sctClus_x);
  Mychain->SetBranchAddress("sctClus_y", &sctClus_y);
  Mychain->SetBranchAddress("sctClus_z", &sctClus_z);
  Mychain->SetBranchAddress("sctClus_phi_module", &sctClus_phi_module);
  Mychain->SetBranchAddress("sctClus_eta_module", &sctClus_eta_module);
  Mychain->SetBranchAddress("sctClus_layer", &sctClus_layer);
  Mychain->SetBranchAddress("sctClus_bec", &sctClus_bec);
  //jets
  Mychain->SetBranchAddress("jet_antikt4truth_eta",&jet_antikt4truth_eta);
  Mychain->SetBranchAddress("jet_antikt4truth_pt",&jet_antikt4truth_pt);
  Mychain->SetBranchAddress("jet_antikt4truth_phi",&jet_antikt4truth_phi);

  //define number of events as constant
  int const nEvents = Mychain->GetEntries();

  cout << "===== INFO:: total number of events: " << nEvents << " =====" << endl;

  //define counters for number of jets used - needed to renormalise plots properly
  int nJetsPixL0 = 0;
  int nJetsPixL1 = 0;
  int nJetsPixL2 = 0;
  int nJetsPixL3 = 0;
  int nJetsSctL0 = 0;

  //define histograms matrices for clusters positions
  //x coordinate
  vector<vector<TH1F*> > h_x_pixL0(pixL0NMod);
  vector<vector<TH1F*> > h_x_pixL1(pixL1NMod);
  vector<vector<TH1F*> > h_x_pixL2(pixL2NMod);
  vector<vector<TH1F*> > h_x_pixL3(pixL3NMod);
  vector<vector<TH1F*> > h_x_sctL0(sctL0NMod);
  //y coordinate
  vector<vector<TH1F*> > h_y_pixL0(pixL0NMod);
  vector<vector<TH1F*> > h_y_pixL1(pixL1NMod);
  vector<vector<TH1F*> > h_y_pixL2(pixL2NMod);
  vector<vector<TH1F*> > h_y_pixL3(pixL3NMod);
  vector<vector<TH1F*> > h_y_sctL0(sctL0NMod);
  //z coordinate
  vector<vector<TH1F*> > h_z_pixL0(pixL0NMod);
  vector<vector<TH1F*> > h_z_pixL1(pixL1NMod);
  vector<vector<TH1F*> > h_z_pixL2(pixL2NMod);
  vector<vector<TH1F*> > h_z_pixL3(pixL3NMod);
  vector<vector<TH1F*> > h_z_sctL0(sctL0NMod);

  //initialise the histograms
  //pixel first
  for(int iMod=0; iMod<pixMaxNMod; iMod++){

    for(int iStave=0; iStave<pixMaxNSt; iStave++){

      if((iStave<pixL0NStaves)&&(iMod<pixL0NMod)){

	TH1F *h_temp_x_pixL0 = new TH1F(Form("h_x_pixL0_%d_%d", iStave, iMod), "", 1, 0., 1.);
	TH1F *h_temp_y_pixL0 = new TH1F(Form("h_y_pixL0_%d_%d", iStave, iMod), "", 1, 0., 1.);
	TH1F *h_temp_z_pixL0 = new TH1F(Form("h_z_pixL0_%d_%d", iStave, iMod), "", 1, 0., 1.);

	h_temp_x_pixL0->StatOverflows(1);
	h_temp_y_pixL0->StatOverflows(1);
	h_temp_z_pixL0->StatOverflows(1);

	h_x_pixL0[iMod].push_back(h_temp_x_pixL0);
	h_y_pixL0[iMod].push_back(h_temp_y_pixL0);
	h_z_pixL0[iMod].push_back(h_temp_z_pixL0);
      }//if((iStave<pixL0NStaves)&&(iMod<pixL0NMod))

      if((iStave<pixL1NStaves)&&(iMod<pixL1NMod)){

	TH1F *h_temp_x_pixL1 = new TH1F(Form("h_x_pixL1_%d_%d", iStave, iMod), "", 1, 0., 1.);
	TH1F *h_temp_y_pixL1 = new TH1F(Form("h_y_pixL1_%d_%d", iStave, iMod), "", 1, 0., 1.);
	TH1F *h_temp_z_pixL1 = new TH1F(Form("h_z_pixL1_%d_%d", iStave, iMod), "", 1, 0., 1.);

	h_temp_x_pixL1->StatOverflows(1);
	h_temp_y_pixL1->StatOverflows(1);
	h_temp_z_pixL1->StatOverflows(1);

	h_x_pixL1[iMod].push_back(h_temp_x_pixL1);
	h_y_pixL1[iMod].push_back(h_temp_y_pixL1);
	h_z_pixL1[iMod].push_back(h_temp_z_pixL1);
      }//if((iStave<pixL0NStaves)&&(iMod<pixL0NMod))

      if((iStave<pixL2NStaves)&&(iMod<pixL2NMod)){

	TH1F *h_temp_x_pixL2 = new TH1F(Form("h_x_pixL2_%d_%d", iStave, iMod), "", 1, 0., 1.);
	TH1F *h_temp_y_pixL2 = new TH1F(Form("h_y_pixL2_%d_%d", iStave, iMod), "", 1, 0., 1.);
	TH1F *h_temp_z_pixL2 = new TH1F(Form("h_z_pixL2_%d_%d", iStave, iMod), "", 1, 0., 1.);

	h_temp_x_pixL2->StatOverflows(1);
	h_temp_y_pixL2->StatOverflows(1);
	h_temp_z_pixL2->StatOverflows(1);

	h_x_pixL2[iMod].push_back(h_temp_x_pixL2);
	h_y_pixL2[iMod].push_back(h_temp_y_pixL2);
	h_z_pixL2[iMod].push_back(h_temp_z_pixL2);
      }//if((iStave<pixL0NStaves)&&(iMod<pixL0NMod))

      if((iStave<pixL3NStaves)&&(iMod<pixL3NMod)){

	TH1F *h_temp_x_pixL3 = new TH1F(Form("h_x_pixL3_%d_%d", iStave, iMod), "", 1, 0., 1.);
	TH1F *h_temp_y_pixL3 = new TH1F(Form("h_y_pixL3_%d_%d", iStave, iMod), "", 1, 0., 1.);
	TH1F *h_temp_z_pixL3 = new TH1F(Form("h_z_pixL3_%d_%d", iStave, iMod), "", 1, 0., 1.);

	h_temp_x_pixL3->StatOverflows(1);
	h_temp_y_pixL3->StatOverflows(1);
	h_temp_z_pixL3->StatOverflows(1);

	h_x_pixL3[iMod].push_back(h_temp_x_pixL3);
	h_y_pixL3[iMod].push_back(h_temp_y_pixL3);
	h_z_pixL3[iMod].push_back(h_temp_z_pixL3);
      }//if((iStave<pixL0NStaves)&&(iMod<pixL0NMod))
    }//for(int iStave; iStave<pixMaxNSt; iStave++)
  }//for(int iMod; iMod<pixMaxNMod; iMod++)

  //SCT init
  for(int iMod=0; iMod<sctMaxNMod; iMod++){

    for(int iStave=0; iStave<sctMaxNSt; iStave++){

      if((iStave<sctL0NStaves)&&(iMod<sctL0NMod)){

	TH1F *h_temp_x_sctL0 = new TH1F(Form("h_x_sctL0_%d_%d", iStave, iMod), "", 1, 0., 1.);
	TH1F *h_temp_y_sctL0 = new TH1F(Form("h_y_sctL0_%d_%d", iStave, iMod), "", 1, 0., 1.);
	TH1F *h_temp_z_sctL0 = new TH1F(Form("h_z_sctL0_%d_%d", iStave, iMod), "", 1, 0., 1.);

	h_temp_x_sctL0->StatOverflows(1);
	h_temp_y_sctL0->StatOverflows(1);
	h_temp_z_sctL0->StatOverflows(1);

	h_x_sctL0[iMod].push_back(h_temp_x_sctL0);
	h_y_sctL0[iMod].push_back(h_temp_y_sctL0);
	h_z_sctL0[iMod].push_back(h_temp_z_sctL0);
      }//if((iStave<sctL0NStaves)&&(iMod<sctL0NMod))
    }//for(int iStave; iStave<sctMaxNSt; iStave++)
  }//for(int iMod; iMod<sctMaxNMod; iMod++)

  //start event loop to fill histograms
  for(int iEvent=0; iEvent<nEvents; iEvent++){

    Mychain->GetEvent(iEvent);

    //loop over pixClus_x
    for(int iClus=0; iClus<pixClus_x->size(); iClus++){

      //necessary cast from char to int
      int pixLayer = (int)((*pixClus_layer)[iClus]);
      int pixBec = (int)((*pixClus_bec)[iClus]);

      if((pixLayer==0)&&(pixBec==0)){

	for(int iMod=0; iMod<pixL0NMod; iMod++){

	  //necessary cast to int of char variables
	  int etaMod = (int)((*pixClus_eta_module)[iClus]);
	  int phiMod = (int)((*pixClus_phi_module)[iClus]);

	  int etaMidOffset = (int)(((float)(pixL0NMod-1))/2.);

	  if(etaMod==(iMod-etaMidOffset)){

	    for(int iStave=0; iStave<pixL0NStaves; iStave++){

	      if(phiMod==iStave){

		h_x_pixL0[iMod][iStave]->Fill((*pixClus_x)[iClus]);
		h_y_pixL0[iMod][iStave]->Fill((*pixClus_y)[iClus]);
		h_z_pixL0[iMod][iStave]->Fill((*pixClus_z)[iClus]);
	      }//if(phiMod==iStave)
	    }//for(int iStave; iStave<pixL0NStaves; iStave++)
	  }//if(etaMod==(iMod-etaMidOffset))
	}//for(int iMod; iMod<pixL0NMod; iMod++)
      }//if((pixLayer==0)&&(pixBec==0))

      if((pixLayer==1)&&(pixBec==0)){

	for(int iMod=0; iMod<pixL0NMod; iMod++){

	  //necessary cast to int of char variables
	  int etaMod = (int)((*pixClus_eta_module)[iClus]);
	  int phiMod = (int)((*pixClus_phi_module)[iClus]);

	  int etaMidOffset = (int)(((float)(pixL1NMod-1))/2.);

	  if(etaMod==(iMod-etaMidOffset)){

	    for(int iStave=0; iStave<pixL1NStaves; iStave++){

	      if(phiMod==iStave){

		h_x_pixL1[iMod][iStave]->Fill((*pixClus_x)[iClus]);
		h_y_pixL1[iMod][iStave]->Fill((*pixClus_y)[iClus]);
		h_z_pixL1[iMod][iStave]->Fill((*pixClus_z)[iClus]);
	      }//if(phiMod==iStave)
	    }//for(int iStave; iStave<pixL1NStaves; iStave++)
	  }//if(etaMod==(iMod-etaMidOffset))
	}//for(int iMod; iMod<pixL1NMod; iMod++)
      }//if((pixLayer==1)&&(pixBec==0))

      if((pixLayer==2)&&(pixBec==0)){

	for(int iMod=0; iMod<pixL0NMod; iMod++){

	  //necessary cast to int of char variables
	  int etaMod = (int)((*pixClus_eta_module)[iClus]);
	  int phiMod = (int)((*pixClus_phi_module)[iClus]);

	  int etaMidOffset = (int)(((float)(pixL2NMod-1))/2.);

	  if(etaMod==(iMod-etaMidOffset)){

	    for(int iStave=0; iStave<pixL2NStaves; iStave++){

	      if(phiMod==iStave){

		h_x_pixL2[iMod][iStave]->Fill((*pixClus_x)[iClus]);
		h_y_pixL2[iMod][iStave]->Fill((*pixClus_y)[iClus]);
		h_z_pixL2[iMod][iStave]->Fill((*pixClus_z)[iClus]);
	      }//if(phiMod==iStave)
	    }//for(int iStave; iStave<pixL2NStaves; iStave++)
	  }//if(etaMod==(iMod-etaMidOffset))
	}//for(int iMod; iMod<pixL2NMod; iMod++)
      }//if((pixLayer==2)&&(pixBec==0))

      if((pixLayer==3)&&(pixBec==0)){

	for(int iMod=0; iMod<pixL0NMod; iMod++){

	  //necessary cast to int of char variables
	  int etaMod = (int)((*pixClus_eta_module)[iClus]);
	  int phiMod = (int)((*pixClus_phi_module)[iClus]);

	  int etaMidOffset = (int)(((float)(pixL3NMod-1))/2.);

	  if(etaMod==(iMod-etaMidOffset)){

	    for(int iStave=0; iStave<pixL3NStaves; iStave++){

	      if(phiMod==iStave){

		h_x_pixL3[iMod][iStave]->Fill((*pixClus_x)[iClus]);
		h_y_pixL3[iMod][iStave]->Fill((*pixClus_y)[iClus]);
		h_z_pixL3[iMod][iStave]->Fill((*pixClus_z)[iClus]);
	      }//if(phiMod==iStave)
	    }//for(int iStave; iStave<pixL3NStaves; iStave++)
	  }//if(etaMod==(iMod-etaMidOffset))
	}//for(int iMod; iMod<pixL3NMod; iMod++)
      }//if((pixLayer==3)&&(pixBec==0))
    }//for(int iClus=0; iClus<pixClus_x->size(); iClus++)

    //loop over sct cluster
    for(int iClus=0; iClus<sctClus_x->size(); iClus++){

      if(((*sctClus_layer)[iClus]==0)&&((*sctClus_bec)[iClus]==0)){

	for(int iMod=0; iMod<sctL0NMod; iMod++){

	  int etaMidOffsetSct = (int)((sctL0NMod-1)/2.);
	  int etaSctMod = (int)((*sctClus_eta_module)[iClus]);

	  if(etaSctMod==(iMod-etaMidOffsetSct)){

	    for(int iSt=0; iSt<sctL0NStaves; iSt++){

	      int phiSctMod = (int)((*sctClus_phi_module)[iClus]);
	      if(phiSctMod==iSt){

		h_x_sctL0[iMod][iSt]->Fill((*sctClus_x)[iClus]);
		h_y_sctL0[iMod][iSt]->Fill((*sctClus_x)[iClus]);
		h_z_sctL0[iMod][iSt]->Fill((*sctClus_x)[iClus]);
	      }//if(phiSctMod==iPhi)
	    }//for(int iSt=0; iSt<sctL0NStaves; iSt++)
	  }//if(etaSctMod==(iMod-etaMidOffsetSct))
	}//for(int iMod=0; iMod<sctL0NMod; iMod++)
      }//if(((*sctClus_layer)[iClus]==0)&&((*sctClus_bec)[iClus]==0))
    }//for(int iClus=0; iClus<sctClus_x->size(); iClus++)
  }//for(int iEvent=0; iEvent<nEvents; iEvent++)

  //second loop on events - is this necessary? Reflect on this, for now leave it this way
  for(int iEv=0; iEv<nEvents; iEv++){

    Mychain->GetEvent(iEv);
    if(iEv%1000==0) cout << "===== DEBUG:: event number " << iEv << " =====" << endl;

    //loop over jets
    for(int iJet=0; iJet<jet_antikt4truth_eta->size(); iJet++){

      //apply pt cut on jets
      if((*jet_antikt4truth_pt)[iJet]>ptCutJets){

	//count number of jets hitting each layer
	if(fabs((*jet_antikt4truth_eta)[iJet])<pixL0Eta) nJetsPixL0++;
	if(fabs((*jet_antikt4truth_eta)[iJet])<pixL1Eta) nJetsPixL1++;
	if(fabs((*jet_antikt4truth_eta)[iJet])<pixL2Eta) nJetsPixL2++;
	if(fabs((*jet_antikt4truth_eta)[iJet])<pixL3Eta) nJetsPixL3++;
	if(fabs((*jet_antikt4truth_eta)[iJet])<sctL0Eta) nJetsSctL0++;

	//start loop to fill pixel histos
	for(int iPcl=0; iPcl<pixClus_x->size(); iPcl++){

	  int pixLayer = (int)((*pixClus_layer)[iPcl]);
	  int pixBec = (int)((*pixClus_bec)[iPcl]);

	  if(pixBec==0){

	    float pixClus_R = sqrt(((*pixClus_x)[iPcl])*((*pixClus_x)[iPcl])+((*pixClus_y)[iPcl])*((*pixClus_y)[iPcl])); 
	    float pixClus_phi = atan(((*pixClus_y)[iPcl])/((*pixClus_x)[iPcl]));
	    float pixClus_theta = atan(pixClus_R/((*pixClus_z)[iPcl]));
	    float pixClus_eta = (-1)*(log(fabs(tan(pixClus_theta/2.))));
	    float pixClus_dPhi = fabs(pixClus_phi-((*jet_antikt4truth_phi)[iJet]));
	    if(pixClus_dPhi>TMath::Pi()) pixClus_dPhi = 2*TMath::Pi()-pixClus_dPhi;

	    //fill histograms with active pixel sensors for each layer
	    if((pixLayer==0)&&(fabs((*jet_antikt4truth_eta)[iJet]))<pixL0Eta) pl_activeSens_pixL0->Fill(fabs(sqrt((pixClus_eta-(*jet_antikt4truth_eta)[iJet])*(pixClus_eta-(*jet_antikt4truth_eta)[iJet])+pixClus_dPhi*pixClus_dPhi)), (*pixClus_size)[iPcl]);
	    if((pixLayer==1)&&(fabs((*jet_antikt4truth_eta)[iJet]))<pixL1Eta) pl_activeSens_pixL1->Fill(fabs(sqrt((pixClus_eta-(*jet_antikt4truth_eta)[iJet])*(pixClus_eta-(*jet_antikt4truth_eta)[iJet])+pixClus_dPhi*pixClus_dPhi)), (*pixClus_size)[iPcl]);
	    if((pixLayer==2)&&(fabs((*jet_antikt4truth_eta)[iJet]))<pixL2Eta) pl_activeSens_pixL2->Fill(fabs(sqrt((pixClus_eta-(*jet_antikt4truth_eta)[iJet])*(pixClus_eta-(*jet_antikt4truth_eta)[iJet])+pixClus_dPhi*pixClus_dPhi)), (*pixClus_size)[iPcl]);
	    if((pixLayer==3)&&(fabs((*jet_antikt4truth_eta)[iJet]))<pixL3Eta) pl_activeSens_pixL3->Fill(fabs(sqrt((pixClus_eta-(*jet_antikt4truth_eta)[iJet])*(pixClus_eta-(*jet_antikt4truth_eta)[iJet])+pixClus_dPhi*pixClus_dPhi)), (*pixClus_size)[iPcl]);
	  }//if(pixBec==0)
	}//for(int iPcl; iPcl<pixClus_x->size(); iPcl++)

	//same for SCT
	for(int iScl=0; iScl<sctClus_x->size(); iScl++){

	  int sctLayer = (int)((*sctClus_layer)[iScl]);
	  int sctBec = (int)((*sctClus_bec)[iScl]);

	  if(sctBec==0){

	    float sctClus_R = sqrt(((*sctClus_x)[iScl])*((*sctClus_x)[iScl])+((*sctClus_y)[iScl])*((*sctClus_y)[iScl])); 
	    float sctClus_phi = atan(((*sctClus_y)[iScl])/((*sctClus_x)[iScl]));
	    float sctClus_theta = atan(sctClus_R/((*sctClus_z)[iScl]));
	    float sctClus_eta = (-1)*(log(fabs(tan(sctClus_theta/2.))));
	    float sctClus_dPhi = fabs(sctClus_phi-((*jet_antikt4truth_phi)[iJet]));
	    if(sctClus_dPhi>TMath::Pi()) sctClus_dPhi = 2*TMath::Pi()-sctClus_dPhi;

	    //fill histograms with active strip sensors for first layer
	    if((sctLayer==0)&&(fabs((*jet_antikt4truth_eta)[iJet]))<sctL0Eta) pl_activeSens_sctL0->Fill(fabs(sqrt((sctClus_eta-(*jet_antikt4truth_eta)[iJet])*(sctClus_eta-(*jet_antikt4truth_eta)[iJet])+sctClus_dPhi*sctClus_dPhi)), (*sctClus_size)[iScl]);
	  }//if(sctBec==0)
	}//for(int iScl; iScl<sctClus_x->size(); iScl++)

	//fill number of sensors plot
	for(int iMod=0; iMod<pixL0NMod; iMod++){

	  for(int iSt=0; iSt<pixL0NStaves; iSt++){

	    int halfMod = (int)(((pixL0NMod-1)/2)+1);

	    if((iMod!=halfMod)&&(fabs((*jet_antikt4truth_eta)[iJet])<pixL0Eta)){

	      float histX = h_x_pixL0[iMod][iSt]->GetMean();
	      float histY = h_y_pixL0[iMod][iSt]->GetMean();
	      float histZ = h_z_pixL0[iMod][iSt]->GetMean();
	      float histR = sqrt(histX*histX+histY*histY);
	      float histPhi = atan(histY/histX);
	      float histTheta = atan(histR/histZ);
	      float histEta = (-1)*(log(fabs(tan(histTheta/2))));
	      float histDphi = fabs(histPhi-(*jet_antikt4truth_phi)[iJet]);
	      if(histDphi>TMath::Pi()) histDphi = 2*TMath::Pi()-histDphi;

	      pl_numSens_pixL0->Fill(fabs(sqrt((histEta-(*jet_antikt4truth_eta)[iJet])*(histEta-(*jet_antikt4truth_eta)[iJet])+histDphi*histDphi)), nSensPixL0);
	    }//if((iMod!=halfMod)&&(fabs((*jet_antikt4truth_eta)[iJet])<sctL0Eta))
	  }//for(int iSt=0; iSt<pixL0NStaves; iSt++)
	}//for(int iMod=0; iMod<pixL0NMod; iMod++)

	for(int iMod=0; iMod<pixL1NMod; iMod++){

	  for(int iSt=0; iSt<pixL1NStaves; iSt++){

	    int halfMod = (int)(((pixL1NMod-1)/2)+1);

	    if((iMod!=halfMod)&&(fabs((*jet_antikt4truth_eta)[iJet])<pixL1Eta)){

	      float histX = h_x_pixL1[iMod][iSt]->GetMean();
	      float histY = h_y_pixL1[iMod][iSt]->GetMean();
	      float histZ = h_z_pixL1[iMod][iSt]->GetMean();
	      float histR = sqrt(histX*histX+histY*histY);
	      float histPhi = atan(histY/histX);
	      float histTheta = atan(histR/histZ);
	      float histEta = (-1)*(log(fabs(tan(histTheta/2))));
	      float histDphi = fabs(histPhi-(*jet_antikt4truth_phi)[iJet]);
	      if(histDphi>TMath::Pi()) histDphi = 2*TMath::Pi()-histDphi;

	      pl_numSens_pixL1->Fill(fabs(sqrt((histEta-(*jet_antikt4truth_eta)[iJet])*(histEta-(*jet_antikt4truth_eta)[iJet])+histDphi*histDphi)), nSensPixL1);
	    }//if((iMod!=halfMod)&&(fabs((*jet_antikt4truth_eta)[iJet])<sctL1Eta))
	  }//for(int iSt=0; iSt<pixL1NStaves; iSt++)
	}//for(int iMod=0; iMod<pixL1NMod; iMod++)

	for(int iMod=0; iMod<pixL2NMod; iMod++){

	  for(int iSt=0; iSt<pixL2NStaves; iSt++){

	    int halfMod = (int)(((pixL2NMod-1)/2)+1);

	    if((iMod!=halfMod)&&(fabs((*jet_antikt4truth_eta)[iJet])<pixL2Eta)){

	      float histX = h_x_pixL2[iMod][iSt]->GetMean();
	      float histY = h_y_pixL2[iMod][iSt]->GetMean();
	      float histZ = h_z_pixL2[iMod][iSt]->GetMean();
	      float histR = sqrt(histX*histX+histY*histY);
	      float histPhi = atan(histY/histX);
	      float histTheta = atan(histR/histZ);
	      float histEta = (-1)*(log(fabs(tan(histTheta/2))));
	      float histDphi = fabs(histPhi-(*jet_antikt4truth_phi)[iJet]);
	      if(histDphi>TMath::Pi()) histDphi = 2*TMath::Pi()-histDphi;

	      pl_numSens_pixL2->Fill(fabs(sqrt((histEta-(*jet_antikt4truth_eta)[iJet])*(histEta-(*jet_antikt4truth_eta)[iJet])+histDphi*histDphi)), nSensPixL2);
	    }//if((iMod!=halfMod)&&(fabs((*jet_antikt4truth_eta)[iJet])<sctL2Eta))
	  }//for(int iSt=0; iSt<pixL2NStaves; iSt++)
	}//for(int iMod=0; iMod<pixL2NMod; iMod++)

	for(int iMod=0; iMod<pixL3NMod; iMod++){

	  for(int iSt=0; iSt<pixL3NStaves; iSt++){

	    int halfMod = (int)(((pixL3NMod-1)/2)+1);

	    if((iMod!=halfMod)&&(fabs((*jet_antikt4truth_eta)[iJet])<pixL3Eta)){

	      float histX = h_x_pixL3[iMod][iSt]->GetMean();
	      float histY = h_y_pixL3[iMod][iSt]->GetMean();
	      float histZ = h_z_pixL3[iMod][iSt]->GetMean();
	      float histR = sqrt(histX*histX+histY*histY);
	      float histPhi = atan(histY/histX);
	      float histTheta = atan(histR/histZ);
	      float histEta = (-1)*(log(fabs(tan(histTheta/2))));
	      float histDphi = fabs(histPhi-(*jet_antikt4truth_phi)[iJet]);
	      if(histDphi>TMath::Pi()) histDphi = 2*TMath::Pi()-histDphi;

	      pl_numSens_pixL3->Fill(fabs(sqrt((histEta-(*jet_antikt4truth_eta)[iJet])*(histEta-(*jet_antikt4truth_eta)[iJet])+histDphi*histDphi)), nSensPixL3);
	    }//if((iMod!=halfMod)&&(fabs((*jet_antikt4truth_eta)[iJet])<sctL3Eta))
	  }//for(int iSt=0; iSt<pixL3NStaves; iSt++)
	}//for(int iMod=0; iMod<pixL3NMod; iMod++)

	//same for SCT
	for(int iMod=0; iMod<sctL0NMod; iMod++){

	  for(int iSt=0; iSt<sctL0NStaves; iSt++){

	    int halfMod = (int)(((sctL0NMod-1)/2)+1);

	    if((iMod!=halfMod)&&(fabs((*jet_antikt4truth_eta)[iJet])<sctL0Eta)){

	      float histX = h_x_sctL0[iMod][iSt]->GetMean();
	      float histY = h_y_sctL0[iMod][iSt]->GetMean();
	      float histZ = h_z_sctL0[iMod][iSt]->GetMean();
	      float histR = sqrt(histX*histX+histY*histY);
	      float histPhi = atan(histY/histX);
	      float histTheta = atan(histR/histZ);
	      float histEta = (-1)*(log(fabs(tan(histTheta/2))));
	      float histDphi = fabs(histPhi-(*jet_antikt4truth_phi)[iJet]);
	      if(histDphi>TMath::Pi()) histDphi = 2*TMath::Pi()-histDphi;

	      pl_numSens_sctL0->Fill(fabs(sqrt((histEta-(*jet_antikt4truth_eta)[iJet])*(histEta-(*jet_antikt4truth_eta)[iJet])+histDphi*histDphi)), nSensSctL0);
	    }//if((iMod!=halfMod)&&(fabs((*jet_antikt4truth_eta)[iJet])<sctL0Eta))
	  }//for(int iSt=0; iSt<sctL0NStaves; iSt++)
	}//for(int iMod=0; iMod<sctL0NMod; iMod++)
      }//if((*jet_antikt4truth_pt)[iJet]>ptCutJets)
    }//for(int iJet=0; iJet<jet_antikt4truth_eta->size(); iJet++)
  }//for(int iEv=0; iEv<nEvents; iEv++)

  //loop over bins to fill correctly various histograms and reweight them accordingly
  for(int bin=1; bin<=nBins; bin++){

    //getting bin content from active sensors histogram
    float contActSensPixL0 = pl_activeSens_pixL0->GetBinContent(bin);
    float contActSensPixL1 = pl_activeSens_pixL1->GetBinContent(bin);
    float contActSensPixL2 = pl_activeSens_pixL2->GetBinContent(bin);
    float contActSensPixL3 = pl_activeSens_pixL3->GetBinContent(bin);
    float contActSensSctL0 = pl_activeSens_sctL0->GetBinContent(bin);

    //calculating R value to use as a denominator in plot of active sens/R
    float const denR = (bin+0.5)/(xMax/nBins);

    //setting content for active sens/R plot
    pl_activeSensRIndep_pixL0->SetBinContent(bin, contActSensPixL0/denR);
    pl_activeSensRIndep_pixL1->SetBinContent(bin, contActSensPixL1/denR);
    pl_activeSensRIndep_pixL2->SetBinContent(bin, contActSensPixL2/denR);
    pl_activeSensRIndep_pixL3->SetBinContent(bin, contActSensPixL3/denR);
    pl_activeSensRIndep_sctL0->SetBinContent(bin, contActSensSctL0/denR);

    //getting bin content for denominator in occupancy calculation
    float occDenPixL0 = pl_numSens_pixL0->GetBinContent(bin);
    float occDenPixL1 = pl_numSens_pixL1->GetBinContent(bin);
    float occDenPixL2 = pl_numSens_pixL2->GetBinContent(bin);
    float occDenPixL3 = pl_numSens_pixL3->GetBinContent(bin);
    float occDenSctL0 = pl_numSens_sctL0->GetBinContent(bin);

    //set occupancy bin content to 0 and then to occpuancy value if den!=0
    pl_occupancy_pixL0->SetBinContent(bin, 0);
    pl_occupancy_pixL1->SetBinContent(bin, 0);
    pl_occupancy_pixL2->SetBinContent(bin, 0);
    pl_occupancy_pixL3->SetBinContent(bin, 0);
    pl_occupancy_sctL0->SetBinContent(bin, 0);

    if(occDenPixL0!=0) pl_occupancy_pixL0->SetBinContent(bin, contActSensPixL0/occDenPixL0);
    if(occDenPixL1!=0) pl_occupancy_pixL1->SetBinContent(bin, contActSensPixL1/occDenPixL1);
    if(occDenPixL2!=0) pl_occupancy_pixL2->SetBinContent(bin, contActSensPixL2/occDenPixL2);
    if(occDenPixL3!=0) pl_occupancy_pixL3->SetBinContent(bin, contActSensPixL3/occDenPixL3);
    if(occDenSctL0!=0) pl_occupancy_pixL0->SetBinContent(bin, contActSensSctL0/occDenSctL0);
  }//for(int bin=1; bin<=nBins; bin++)

  //print out number of used jets per layer, pt cut and number of events used
  cout << "===== INFO:: Pixel layer 0, hit by " << nJetsPixL0 << " jets =====" << endl;
  cout << "===== INFO:: Pixel layer 1, hit by " << nJetsPixL1 << " jets =====" << endl;
  cout << "===== INFO:: Pixel layer 2, hit by " << nJetsPixL2 << " jets =====" << endl;
  cout << "===== INFO:: Pixel layer 3, hit by " << nJetsPixL3 << " jets =====" << endl;
  cout << "===== INFO:: SCT layer 0, hit by " << nJetsSctL0 << " jets =====" << endl;
  cout << "===== INFO:: pt cut used: " << ptCutJets << "MeV =====" << endl;
  cout << "===== INFO:: total number of events: " << nEvents << " =====" << endl;

  //define canvas
  TCanvas *c2 = new TCanvas("c2","square",200,10,1200,1200);
  c2->RangeAxis(0.,0.00005,35.,0.1);
  c2->SetFillColor(10);
  c2->Divide(2,2);
  gStyle->SetOptStat(kFALSE);

  c2->cd(1);

  pl_activeSens_pixL0->SetLineColor(kOrange-4);
  pl_activeSens_pixL1->SetLineColor(kAzure+8);
  pl_activeSens_pixL2->SetLineColor(kRed-7);
  pl_activeSens_pixL3->SetLineColor(kGreen-3);
  pl_activeSens_sctL0->SetLineColor(kMagenta+2);

  //scaling to the number of jets actually used in the plots - hitting Pix/SCT
  pl_activeSens_pixL0->Scale(1./nJetsPixL0);
  pl_activeSens_pixL1->Scale(1./nJetsPixL1);
  pl_activeSens_pixL2->Scale(1./nJetsPixL2);
  pl_activeSens_pixL3->Scale(1./nJetsPixL3);
  pl_activeSens_sctL0->Scale(1./nJetsSctL0);

  pl_activeSens_pixL2->SetTitle("All clusters");
  pl_activeSens_pixL2->GetYaxis()->SetTitle("R (distance from jet axis)");
  pl_activeSens_pixL2->GetYaxis()->SetTitle("Number of pixels/strips hit per jet");

  pl_activeSens_pixL2->Draw();
  pl_activeSens_pixL1->Draw("same");
  pl_activeSens_pixL0->Draw("same");
  pl_activeSens_pixL3->Draw("same");
  pl_activeSens_sctL0->Draw("same");

  TLegend *legSCT = new TLegend(0.2, 0.55, 0.5, 0.85); 
  legSCT->SetHeader("Pixel layers+1st SCT layer");
  legSCT->AddEntry(pl_activeSens_pixL0, "Pixel layer 0");
  legSCT->AddEntry(pl_activeSens_pixL1, "Pixel layer 1");
  legSCT->AddEntry(pl_activeSens_pixL2, "Pixel layer 2");
  legSCT->AddEntry(pl_activeSens_pixL3, "Pixel layer 3");
  legSCT->AddEntry(pl_activeSens_sctL0, "SCT layer 0");
  legSCT->SetFillColor(10);
  legSCT->Draw("same");
  c2->Update();

  c2->cd(2);
  pl_activeSensRIndep_pixL0->SetLineColor(kOrange-4);
  pl_activeSensRIndep_pixL1->SetLineColor(kAzure+8);
  pl_activeSensRIndep_pixL2->SetLineColor(kRed-7);
  pl_activeSensRIndep_pixL3->SetLineColor(kGreen-3);
  pl_activeSensRIndep_sctL0->SetLineColor(kMagenta+2);

  //scaling to the number of jets actually used in the plots - hitting Pix/SCT
  pl_activeSensRIndep_pixL0->Scale(1./nJetsPixL0);
  pl_activeSensRIndep_pixL1->Scale(1./nJetsPixL1);
  pl_activeSensRIndep_pixL2->Scale(1./nJetsPixL2);
  pl_activeSensRIndep_pixL3->Scale(1./nJetsPixL3);
  pl_activeSensRIndep_sctL0->Scale(1./nJetsSctL0);

  pl_activeSensRIndep_pixL3->SetTitle("All clusters");
  pl_activeSensRIndep_pixL3->GetYaxis()->SetTitle("R (distance from jet axis)");
  pl_activeSensRIndep_pixL3->GetYaxis()->SetTitle("Number of pixels/strips hit per jet");

  pl_activeSensRIndep_pixL3->Draw();
  pl_activeSensRIndep_pixL1->Draw("same");
  pl_activeSensRIndep_pixL2->Draw("same");
  pl_activeSensRIndep_pixL0->Draw("same");
  pl_activeSensRIndep_sctL0->Draw("same");

  TLegend *legSCT_b = new TLegend(0.6, 0.6, 0.9, 0.9); 
  legSCT_b->SetHeader("Pixel layers+1st SCT layer");
  legSCT_b->AddEntry(pl_activeSensRIndep_pixL0, "Pixel layer 0");
  legSCT_b->AddEntry(pl_activeSensRIndep_pixL1, "Pixel layer 1");
  legSCT_b->AddEntry(pl_activeSensRIndep_pixL2, "Pixel layer 2");
  legSCT_b->AddEntry(pl_activeSensRIndep_pixL3, "Pixel layer 3");
  legSCT_b->AddEntry(pl_activeSensRIndep_sctL0, "SCT layer 0");
  legSCT_b->SetFillColor(10);
  legSCT_b->Draw("same");
  c2->Update();

  c2->cd(3);
  pl_numSens_pixL0->SetLineColor(kOrange-4);
  pl_numSens_pixL1->SetLineColor(kAzure+8);
  pl_numSens_pixL2->SetLineColor(kRed-7);
  pl_numSens_pixL3->SetLineColor(kGreen-3);
  pl_numSens_sctL0->SetLineColor(kMagenta+2);

  //scaling to the number of jets actually used in the plots - hitting Pix/SCT
  pl_numSens_pixL0->Scale(1./nJetsPixL0);
  pl_numSens_pixL1->Scale(1./nJetsPixL1);
  pl_numSens_pixL2->Scale(1./nJetsPixL2);
  pl_numSens_pixL3->Scale(1./nJetsPixL3);
  pl_numSens_sctL0->Scale(1./nJetsSctL0);

  pl_numSens_pixL3->SetTitle("All clusters");
  pl_numSens_pixL3->GetYaxis()->SetTitle("R (distance from jet axis)");
  pl_numSens_pixL3->GetYaxis()->SetTitle("Number of pixels/strips hit per jet");

  pl_numSens_pixL3->Draw();
  pl_numSens_pixL1->Draw("same");
  pl_numSens_pixL2->Draw("same");
  pl_numSens_pixL0->Draw("same");
  pl_numSens_sctL0->Draw("same");

  TLegend *legSCT_c = new TLegend(0.1, 0.6, 0.4, 0.9); 
  legSCT_c->SetHeader("Pixel layers+1st SCT layer");
  legSCT_c->AddEntry(pl_numSens_pixL0, "Pixel layer 0");
  legSCT_c->AddEntry(pl_numSens_pixL1, "Pixel layer 1");
  legSCT_c->AddEntry(pl_numSens_pixL2, "Pixel layer 2");
  legSCT_c->AddEntry(pl_numSens_pixL3, "Pixel layer 3");
  legSCT_c->AddEntry(pl_numSens_sctL0, "SCT layer 0");
  legSCT_c->SetFillColor(10);
  legSCT_c->Draw("same");
  c2->Update();

  c2->cd(4);
  // c2->SetLogy();
  pl_occupancy_pixL0->SetLineColor(kOrange-4);
  pl_occupancy_pixL1->SetLineColor(kAzure+8);
  pl_occupancy_pixL2->SetLineColor(kRed-7);
  pl_occupancy_pixL3->SetLineColor(kGreen-3);
  pl_occupancy_sctL0->SetLineColor(kMagenta+2);

  pl_occupancy_sctL0->SetTitle("All clusters");
  pl_occupancy_sctL0->GetYaxis()->SetTitle("R (distance from jet axis)");
  pl_occupancy_sctL0->GetYaxis()->SetTitle("Number of pixels/strips hit per jet");

  pl_occupancy_sctL0->Draw();
  pl_occupancy_pixL1->Draw("same");
  pl_occupancy_pixL2->Draw("same");
  pl_occupancy_pixL0->Draw("same");
  pl_occupancy_pixL3->Draw("same");

  legSCT_b->Draw("same");
  c2->Update();

  return;
}//void occInBoostedJets
