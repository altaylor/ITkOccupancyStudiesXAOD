#ifndef __CINT__
#include "TFile.h"
#include "TTree.h"
#include "TH2.h"
#include "TCanvas.h"
#include "Riostream.h"
#include <iostream>
#include <sstream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <string>
#include <stdlib.h>
#include <math.h>


using std::cout;
using std::endl;
#endif

void TrackingEfficiency()
{
  
  gStyle->SetOptStat(kFALSE);

  TCanvas *c1 = new TCanvas("c1","c1",800,600);

  //// --> Tracking Cuts 
  Double_t z0Cut = 150;
  Double_t d0Cut = 1.0;
  Double_t matchCut = 0.5;
  Double_t hitsCut = 9;
  Double_t chi2cut = 10000000;
  Double_t qptCut = 3;


  //switch efficiency/fakes calculations on & off
  Bool_t doFakes = true;
  Bool_t doEff = true;

 
  TH1F *recoTracksAll   = new TH1F("recoTracksAll","RecEta distribution",20,0,3);
  TH1F *recoPrimaryTracks   = new TH1F("recoPrimaryTracks","RecEta barcode cut",20,0,3);
  TH1F *recoSelectedTracks   = new TH1F("recoSelectedTracks","RecEta Cuts",20,0,3);
  TH1F *fakeDenominator   = new TH1F("fakeDenominator","RecEta Cuts",20,0,3);
  TH1F *fakeNumerator   = new TH1F("fakeNumerator","RecEta inverted matching cut",20,0,3);
  TH1F *truthTracksAll   = new TH1F("truthTracksAll","TruthEta distribution",20,0,3);
  TH1F *truthPrimaryTracks   = new TH1F("truthPrimaryTracks","TruthEta barcode cut",20,0,3);
  TH1F *truthSelectedTracks   = new TH1F("truthSelectedTracks","TruthEta Cuts",20,0,3); 
  TH1F *efficiencyDenominator   = new TH1F("efficiencyDenominator","RecEta barcode cut",10,0,2.5);
  TH1F *efficiencyNumerator   = new TH1F("efficiencyNumerator","RecEta Cuts",10,0,2.5);

  TH1F *z0tracks   = new TH1F("z0tracks","z0tracks",20,0,2.5);
  TH1F *d0tracks   = new TH1F("d0tracks","d0tracks",20,0,2.5); 
  TH1F *probtracks   = new TH1F("probtracks","probtracks",10,0,2.5);
  TH1F *hitstracks   = new TH1F("hitstracks","hitstracks",10,0,2.5);
  
  TH1F *zProdTruth_hist   = new TH1F("truth_prod_z","truth_prod_z",100,0.,2000.);
  

  Int_t n_files = 100;

  for(int filenum=0;filenum<n_files;filenum++){

    std::stringstream filename_str;
    
    //define path + filename(s) to be looked at
    filename_str << "root://castoratlas//castor/cern.ch/user/n/nstyles/20GeVPions_pu10/TrkValidation_20GeV_pions_pu10_" << filenum << ".root" ;

    cout<<"file: "<<(filename_str.str()).c_str() <<endl;
    
    //TFile *f = TFile::Open((filename_str.str()).c_str());
    TString in_file = (filename_str.str()).c_str();
    
    EventLoop(in_file,z0Cut,d0Cut,matchCut,hitsCut,chi2cut,qptCut,recoTracksAll,
	      recoPrimaryTracks,recoSelectedTracks,fakeDenominator,fakeNumerator ,
	      truthTracksAll,truthPrimaryTracks,truthSelectedTracks,
	      efficiencyDenominator,efficiencyNumerator,zProdTruth_hist,
	      z0tracks, d0tracks,probtracks,hitstracks,doFakes,doEff);
  
  }
  
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  if(doEff==true){
    cout<<"###################################################################"<<endl;
    cout<<"Number of Good MC tracks:                      "<<truthSelectedTracks->GetEntries()<<endl;
    cout<<"Number of Reco tracks before cuts:             "<<efficiencyDenominator->GetEntries()<<endl;
    cout<<"-->Number of Reco tracks after z0 cut:         "<<z0tracks->GetEntries()<<endl;
    cout<<"-->Number of Reco tracks after d0 cut:         "<<d0tracks->GetEntries()<<endl;
    cout<<"-->Number of Reco tracks after matching cut:   "<<probtracks->GetEntries()<<endl;
    cout<<"-->Number of Reco tracks after nhits cut:      "<<hitstracks->GetEntries()<<endl;
    
    cout<<"Number of Reco tracks after cuts:              "<<efficiencyNumerator->GetEntries()<<endl;
    Double_t eff = ((Double_t)efficiencyNumerator->GetEntries())/((Double_t)efficiencyDenominator->GetEntries());
    cout<<"############## Average Efficiency = "<<eff<<" ######################"<<endl;
  }
  
  if(doFakes==true){
    cout<<"###################################################################"<<endl;
    cout<<"Number of Good Reco tracks: "<<fakeDenominator->GetEntries()<<endl;
    cout<<"Number of unmatched tracks: "<<fakeNumerator->GetEntries()<<endl;
    Double_t fake = ((Double_t)fakeNumerator->GetEntries())/((Double_t)fakeDenominator->GetEntries());
    cout<<"############# Average Fake rate = "<<fake<<" ######################"<<endl;
  }
 

  if(doEff==true){
  
    //TCanvas *c1 = new TCanvas("c1","c1",800,600);
    //Setting plot colours & widths
    recoTracksAll->SetLineColor(2);  
    recoTracksAll->SetLineWidth(2);
    recoPrimaryTracks->SetLineColor(4);
    recoPrimaryTracks->SetLineWidth(2);
    fakeNumerator->SetLineColor(8);
    fakeNumerator->SetLineWidth(2);
    recoSelectedTracks->SetLineColor(8);
    recoSelectedTracks->SetLineWidth(2);
    truthTracksAll->SetLineColor(2);
    truthTracksAll->SetLineWidth(2);  
    truthPrimaryTracks->SetLineColor(4);
    truthPrimaryTracks->SetLineWidth(2); 
    truthSelectedTracks->SetLineColor(8);
    truthSelectedTracks->SetLineWidth(2);
    
    
    //Fake rate histo
    TH1D *fakeHist = (TH1D*)fakeNumerator->Clone("fakeNumerator");
    fakeHist->SetDirectory(gROOT);
    fakeNumerator->Sumw2();
    recoSelectedTracks->Sumw2();
    fakeHist->Divide(fakeNumerator,fakeDenominator,1.0,1.0,"B");
    fakeHist->SetLineColor(30);
    fakeHist->SetLineWidth(2);
    
    
    //efficiency histo
    TH1D *effHisto = (TH1D*)efficiencyNumerator->Clone("efficiencyNumerator");
    effHisto->SetDirectory(gROOT);
    efficiencyNumerator->Sumw2();
    truthSelectedTracks->Sumw2();
    effHisto->Divide(efficiencyNumerator,efficiencyDenominator,1.0,1.0,"B");
    effHisto->SetLineColor(40);
    effHisto->SetLineWidth(2);
    
    
    c1->cd();
    
    ///Efficiency 
    effHisto->SetMinimum(0.);
    effHisto->SetMaximum(1.1);
    // effHisto->SetTitle(0);  
    effHisto->SetTitle("Efficiency");  
    effHisto->SetXTitle("#\eta");  
    effHisto->SetYTitle("Efficiency");  
    effHisto->SetMarkerStyle(20);//20 for filled dot, 4 for empty dot 
    effHisto->SetMarkerSize(1.8);
    effHisto->SetMarkerColor(2);
    effHisto->Draw("pe");
  }
  
}


Int_t EventLoop(TString in_file, Double_t z0Cut, Double_t d0Cut, Double_t matchCut,
		Double_t hitsCut,  Double_t chi2cut, Double_t qptCut,TH1F *recoTracksAll,
		TH1F *recoPrimaryTracks,TH1F *recoSelectedTracks,TH1F *fakeDenominator,
		TH1F *fakeNumerator ,TH1F *truthTracksAll,TH1F *truthPrimaryTracks,
		TH1F *truthSelectedTracks,TH1F *efficiencyDenominator,TH1F *efficiencyNumerator,TH1F *zProdTruth_hist,
		TH1F *z0tracks, TH1F *d0tracks,TH1F *probtracks,TH1F *hitstracks, Bool_t doFakes, Bool_t doEff)
{

  // Int_t m_particle_ID =13;//<-- Muon
  Int_t m_particle_ID =11;//<-- Electron

  TFile *f = TFile::Open(in_file);

  if (!f) return 0;

  TTree *t1 = (TTree*)f->Get("Validation/CombinedInDetTracks");
  TTree *t2 = (TTree*)f->Get("Validation/Truth");

  if((!t1)||!(t2)) return 0;

  //Reconstruction variables
  Float_t etaRec;
  Float_t matchProb;
  Float_t z0Rec;
  Float_t d0Rec; 
  Float_t chi2;
  Int_t nhits;
  Float_t z0Truthcomp;
  Float_t d0Truthcomp;
  Int_t pixHits;
  Int_t sctHits;
  Float_t thetaRec;
  Float_t qpRec;
  Int_t barcode_rec;
  Int_t event_rec;
 
  //Truth variables  
  Int_t barcode;
  Float_t z0Truth;
  Float_t d0Truth;
  Float_t qptTruth;
  Float_t etaTruth; 
  Float_t zProdTruth;
  Int_t truthParticleID; 
  Int_t particleID;
  Int_t event_truth; 

  //Vectors linking Truth tracks to reconstructed tracks
  std::vector<float> * probVect;
  std::vector<unsigned int> * linkVect;

  // Reconstruction variables from NTuple
  t1->SetBranchAddress("RecEta",&etaRec);
  t1->SetBranchAddress("trk_Mc_barcode",&barcode_rec);
  t1->SetBranchAddress("trk_Mc_prob",&matchProb);
  t1->SetBranchAddress("RecZ0",&z0Rec);
  t1->SetBranchAddress("RecD0",&d0Rec);
  t1->SetBranchAddress("Chi2overNdof",&chi2);
  t1->SetBranchAddress("nHits",&nhits);
  t1->SetBranchAddress("trk_Mc_d0",&d0Truthcomp);
  t1->SetBranchAddress("trk_Mc_z0",&z0Truthcomp);
  t1->SetBranchAddress("nPixelHits",&pixHits);
  t1->SetBranchAddress("nSCTHits",&sctHits);
  t1->SetBranchAddress("RecTheta",&thetaRec);
  t1->SetBranchAddress("RecQoverP",&qpRec);
  t1->SetBranchAddress("trk_Mc_particleID",&particleID);
  t1->SetBranchAddress("EventNumber",&event_rec);
 

  
  //Truth variables from NTuple
  t2->SetBranchAddress("truth_eta",&etaTruth);
  t2->SetBranchAddress("truth_barcode",&barcode);
  t2->SetBranchAddress("truth_z0",&z0Truth);
  t2->SetBranchAddress("truth_d0",&d0Truth);
  t2->SetBranchAddress("truth_qOverPt",&qptTruth);
  t2->SetBranchAddress("truth_prob_CombinedInDetTracks",&probVect);
  t2->SetBranchAddress("TrackLinkIndex_CombinedInDetTracks",&linkVect);
  t2->SetBranchAddress("truth_prod_z",&zProdTruth);
  t2->SetBranchAddress("truth_particleID",  &truthParticleID);
  t2->SetBranchAddress("EventNumber", &event_truth);
  
  //switching off reading of all branches (runs quicker) 
   t1->SetBranchStatus("*",0);
   t2->SetBranchStatus("*",0);


   //switching reading of required branches back on
   t1->SetBranchStatus("RecEta",1);
   t1->SetBranchStatus("trk_Mc_barcode",1);
   t1->SetBranchStatus("trk_Mc_prob",1);
   t1->SetBranchStatus("RecZ0",1);
   t1->SetBranchStatus("RecD0",1);
   t1->SetBranchStatus("Chi2overNdof",1);
   t1->SetBranchStatus("nHits",1);
   t1->SetBranchStatus("trk_Mc_d0",1);
   t1->SetBranchStatus("trk_Mc_z0",1);
   t1->SetBranchStatus("nPixelHits",1);
   t1->SetBranchStatus("nSCTHits",1);
   t1->SetBranchStatus("RecTheta",1);
   t1->SetBranchStatus("RecQoverP",1);
   t1->SetBranchStatus("trk_Mc_particleID",1);
   t1->SetBranchStatus("EventNumber",1);

   t2->SetBranchStatus("truth_eta",1);
   t2->SetBranchStatus("truth_barcode",1);
   t2->SetBranchStatus("truth_z0",1);
   t2->SetBranchStatus("truth_d0",1);
   t2->SetBranchStatus("truth_qOverPt",1);
   t2->SetBranchStatus("truth_prob_CombinedInDetTracks",1);
   t2->SetBranchStatus("TrackLinkIndex_CombinedInDetTracks",1);
   t2->SetBranchStatus("truth_prod_z",1);
   t2->SetBranchStatus("truth_particleID",1);
   t2->SetBranchStatus("EventNumber",1);
  

  if (doFakes==true){

    Int_t nentries = (Int_t)t1->GetEntries();
    cout<<"Reading Rec NTuple, #entries: "<< nentries <<endl;


    //FAKE RATES SECTION
    //// --> Running over all Reco tracks
    for (Int_t i=0;i<nentries;i++) { 
      t1->GetEntry(i);
      if (fabs(etaRec<2.5) ) {
	recoTracksAll->Fill(fabs(etaRec));
      }
      
      //Checking barcode to see if it is a pythia generated or G4 generated particle
      if(
	 (barcode_rec>10000) &&
	 (barcode_rec!=0) &&
	 (barcode_rec<100000) &&
	 (fabs(etaRec<2.5))
	 ){
	recoPrimaryTracks->Fill(fabs(etaRec));
      }
      
      //Applying cuts 
      
      if((fabs(z0Rec)<z0Cut) &&
	 (fabs(d0Rec)<d0Cut) &&
	 (matchProb>matchCut) &&
	 (barcode_rec>10000) &&
	 (barcode_rec!=0) &&
	 (barcode_rec<100000) &&
	 (chi2<chi2cut) &&
	 (fabs(etaRec<2.5)) &&
	 ((pixHits+sctHits)>=hitsCut)
	 ) {
	recoSelectedTracks->Fill(fabs(etaRec));
      }
      
      //Cuts w/o matching proabability cut for fake rate denominator
      if((fabs(z0Rec)<z0Cut) &&
	 (fabs(d0Rec)<d0Cut) &&
	 (chi2<chi2cut) &&
	 (fabs(etaRec<2.5)) &&
	 (fabs((sin(thetaRec)/qpRec/1000.0))>qptCut) &&
	 ((pixHits+sctHits)>=hitsCut)
	 ) {
	fakeDenominator->Fill(fabs(etaRec));
      }
      
      //Cuts with inverted matching probability cut for fake rate numerator
      if((fabs(z0Rec)<z0Cut) &&
	 (fabs(d0Rec)<d0Cut) &&
	 (chi2<chi2cut) &&
	 (fabs(etaRec<2.5)) &&
	 (fabs((sin(thetaRec)/qpRec/1000.0))>qptCut) &&
	 ((matchProb<matchCut) || (barcode_rec==0)) &&
	 ((pixHits+sctHits)>=hitsCut)
	 ) {
	fakeNumerator->Fill(fabs(etaRec));
      }
      

    }

  }
  
  if(doEff==true){

  //EFFICIENCIES SECTION  
  Int_t nentriesT = (Int_t)t2->GetEntries();
  cout<<"Reading Truth NTuple, #entries: "<<nentriesT<<endl;
  // --> Running over all truth tracks
  for (Int_t i=0;i<nentriesT;i++)
    {
    t2->GetEntry(i);
    
     
    if(fabs(etaTruth<2.5) ){
      truthTracksAll->Fill(fabs(etaTruth));

      zProdTruth_hist->Fill(zProdTruth);

    }
    
    //Checking barcode to see if it is a pythia generated or G4 generated particle
    if(
       (barcode > 10000)&&
       (barcode < 100000) &&
       (fabs(etaTruth<2.5)) 
      ) {
      truthPrimaryTracks->Fill(fabs(etaTruth));
    }
     
    //Applying cuts to truth tracks
    if(
       (barcode > 10000) &&
      (barcode < 100000) &&
      (fabs(etaTruth<2.5)) && 
      (fabs(z0Truth)<z0Cut) &&
      (fabs(d0Truth)<d0Cut) &&
      (fabs(1/qptTruth/1000.0)>qptCut)
      )
    {
      truthSelectedTracks->Fill(fabs(etaTruth));
      
      //finding the reco track with the best matching probability associated to this truth track
      float best_prob   = -999;
      int track_index   = -999;
      for(int unsigned ii=0; ii<probVect->size(); ii++){
	if((probVect->at(ii) > best_prob)){
          best_prob = probVect->at(ii);
          track_index = ii;
        }
      }
      
     
      if(track_index != -999) t1->GetEntry(linkVect->at(track_index));
     


      efficiencyDenominator->Fill(fabs(etaTruth));

      if ( fabs(z0Rec) < z0Cut ) z0tracks->Fill(fabs(etaTruth));
      if ( fabs(d0Rec) < d0Cut ) d0tracks->Fill(fabs(etaTruth));
      if ( best_prob > matchCut ) probtracks->Fill(fabs(etaTruth));
      if ((pixHits+sctHits) >= hitsCut ) hitstracks->Fill(fabs(etaTruth));
       
      //applying cuts to best matched reco track for efficiency numerator
      if(
	 ( fabs(z0Rec) < z0Cut ) &&
	 ( fabs(d0Rec) < d0Cut ) &&
	 ( best_prob > matchCut ) &&
	 ( chi2 < chi2cut ) &&
	 ( event_rec==event_truth ) && 
        ( (pixHits+sctHits) >= hitsCut )
        )
      {
        efficiencyNumerator->Fill(fabs(etaTruth));
      }

    }

    }
  }
  
  return 1;

}


