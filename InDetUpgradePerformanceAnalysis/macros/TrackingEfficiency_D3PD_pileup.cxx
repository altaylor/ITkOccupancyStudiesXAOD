#ifndef __CINT__
#include "TFile.h"
#include "TTree.h"
#include "TH2.h"
#include "TCanvas.h"
#include "Riostream.h"
#include <iostream>
#include <sstream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <string>
#include <stdlib.h>
#include <math.h>


using std::cout;
using std::endl;
#endif

void TrackingEfficiency_D3PD_pileup()
{
  
  gStyle->SetOptStat(kFALSE);

  TCanvas *c1 = new TCanvas("c1","c1",800,600);

  //// --> Tracking Cuts 
  Double_t z0Cut = 150;
  Double_t d0Cut = 1.0;
  Double_t matchCut = 0.5;
  Double_t hitsCut = 9;
  Double_t holesCut = 1;
  Double_t chi2cut = 10000000;
  Double_t ptCut = 1000;
  Int_t barcode_cut = 10000; //set to 10000 for single particles, otherwise 0


  //switch efficiency/fakes calculations on & off
  Bool_t doFakes = true;
  Bool_t doEff = true;

 
  TH1F *recoTracksAll   = new TH1F("recoTracksAll","RecEta distribution",20,0,3);
  TH1F *recoPrimaryTracks   = new TH1F("recoPrimaryTracks","RecEta barcode cut",20,0,3);
  TH1F *recoSelectedTracks   = new TH1F("recoSelectedTracks","RecEta Cuts",20,0,3);
  TH1F *fakeDenominator   = new TH1F("fakeDenominator","RecEta Cuts",10,0,2.5);
  TH1F *fakeNumerator   = new TH1F("fakeNumerator","RecEta inverted matching cut",10,0,2.5);
  TH1F *truthTracksAll   = new TH1F("truthTracksAll","TruthEta distribution",20,0,3);
  TH1F *truthInclusiveTracks   = new TH1F("truthInclusiveTracks","TruthEta no barcode cut",10,0,2.5);
  TH1F *truthSelectedTracks   = new TH1F("truthSelectedTracks","TruthEta Cuts",10,0,2.5); 
  TH1F *efficiencyDenominator   = new TH1F("efficiencyDenominator","RecEta barcode cut",10,0,2.5);
  TH1F *efficiencyNumerator   = new TH1F("efficiencyNumerator","RecEta Cuts",10,0,2.5);
  TH1F *z0tracks   = new TH1F("z0tracks","z0tracks",20,0,2.5);
  TH1F *d0tracks   = new TH1F("d0tracks","d0tracks",20,0,2.5); 
  TH1F *probtracks   = new TH1F("probtracks","probtracks",10,0,2.5);
  TH1F *hitstracks   = new TH1F("hitstracks","hitstracks",10,0,2.5);
  TH1F *zProdTruth_hist   = new TH1F("truth_prod_z","truth_prod_z",100,0.,2000.);
  
  /////////////////////deine pile-up datasets here
  const Int_t n_pileup_points = 5;

  Double_t pileup [n_pileup_points] = {5,50,100,150,200};
  Int_t n_pileup_files [n_pileup_points] = {2,2,2,2,2};
  TString pileup_path [n_pileup_points] = {
				       "root://castoratlas//castor/cern.ch/atlas/atlascerngroupdisk/det-slhc/jtseng/16.4.1/v1/mmu-SLHC/muontrk__5_1_",
				       "root://castoratlas//castor/cern.ch/atlas/atlascerngroupdisk/det-slhc/jtseng/16.4.1/v1/mmu-SLHC/muontrk__50_1_",
				       "root://castoratlas//castor/cern.ch/atlas/atlascerngroupdisk/det-slhc/jtseng/16.4.1/v1/mmu-SLHC/muontrk__100_1_",
				       "root://castoratlas//castor/cern.ch/atlas/atlascerngroupdisk/det-slhc/jtseng/16.4.1/v1/mmu-SLHC/muontrk__150_1_",
				       "root://castoratlas//castor/cern.ch/atlas/atlascerngroupdisk/det-slhc/jtseng/16.4.1/v1/mmu-SLHC/muontrk__200_1_"	       
					 };
   TString pileup_suffix [n_pileup_points] = {
				       "_D3PD.root",
				        "_D3PD.root",
				        "_D3PD.root",
				        "_D3PD.root",
				        "_D3PD.root"       
					 };

   TH1F *efficiency   = new TH1F("efficiency","Tracking Efficiency",10,0,210);
   TH1F *fakerate   = new TH1F("fakerate","Fake Rate",10,0,210);
   TH1F *ratio   = new TH1F("ratio","Inclusive Rec/Gen",10,0,210);


   for(int pileup_point=0;pileup_point<n_pileup_points;pileup_point++){

  std::stringstream pileup_str;
  pileup_str <<pileup[pileup_point];
  TString pileup_string = (pileup_str.str()).c_str();


  for(int filenum=0;filenum<n_pileup_files[pileup_point];filenum++){

    std::stringstream filename_str;
    
    filename_str << pileup_path[pileup_point] << filenum << pileup_suffix[pileup_point] ;

    recoTracksAll->Reset();
    recoPrimaryTracks->Reset();
    recoSelectedTracks->Reset();
    fakeDenominator->Reset();
    fakeNumerator->Reset();
    truthTracksAll->Reset();
    truthInclusiveTracks->Reset();
    truthSelectedTracks->Reset();
    efficiencyDenominator->Reset();
    efficiencyNumerator->Reset();
    z0tracks->Reset();
    d0tracks->Reset();
    probtracks->Reset();
    hitstracks->Reset();

    cout<<"file: "<<(filename_str.str()).c_str() <<endl;
    
   
    TString in_file = (filename_str.str()).c_str();

    
    
    EventLoop(in_file,z0Cut,d0Cut,matchCut,hitsCut,chi2cut,ptCut,holesCut,barcode_cut,recoTracksAll,
	      recoPrimaryTracks,recoSelectedTracks,fakeDenominator,fakeNumerator ,
	      truthTracksAll,truthInclusiveTracks,truthSelectedTracks,
	      efficiencyDenominator,efficiencyNumerator,zProdTruth_hist,
	      z0tracks, d0tracks,probtracks,hitstracks,doFakes,doEff);
  
  }
  
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  if(doEff==true){
    cout<<"################# pileup "<<pileup[pileup_point] <<" #####################################"<<endl;
     cout<<"Number of tracks:                             "<<truthTracksAll->GetEntries()<<endl;
    cout<<"Number of Good MC tracks:                      "<<truthSelectedTracks->GetEntries()<<endl;
    cout<<"Number of Reco tracks before cuts:             "<<efficiencyDenominator->GetEntries()<<endl;
    cout<<"-->Number of Reco tracks after z0 cut:         "<<z0tracks->GetEntries()<<endl;
    cout<<"-->Number of Reco tracks after d0 cut:         "<<d0tracks->GetEntries()<<endl;
    cout<<"-->Number of Reco tracks after matching cut:   "<<probtracks->GetEntries()<<endl;
    cout<<"-->Number of Reco tracks after nhits cut:      "<<hitstracks->GetEntries()<<endl;
    
    cout<<"Number of Reco tracks after cuts:              "<<efficiencyNumerator->GetEntries()<<endl;
    Double_t eff = ((Double_t)efficiencyNumerator->GetEntries())/((Double_t)truthSelectedTracks->GetEntries());
    Double_t eff_err = sqrt(eff * (1 - eff) * ((Double_t)truthSelectedTracks->GetEntries()))/ ((Double_t)truthSelectedTracks->GetEntries()); 
    cout<<"Average Efficiency = "<<eff<<" +/- "<< eff_err <<endl;
    cout<<"###################################################################"<<endl;
    
  }
  
  if(doFakes==true){
    cout<<"################ 'Exclusive' Fake Rate ############################"<<endl;
    cout<<"Number of Good Reco tracks: "<<fakeDenominator->GetEntries()<<endl;
    cout<<"Number of unmatched tracks: "<<fakeNumerator->GetEntries()<<endl;
    Double_t fake = ((Double_t)fakeNumerator->GetEntries())/((Double_t)fakeDenominator->GetEntries());
    Double_t fake_err = sqrt(fake * (1 - fake) * ((Double_t)fakeDenominator->GetEntries()))/ ((Double_t)fakeDenominator->GetEntries());
    cout<<"Average Fake rate = "<<fake<<" +/- "<<fake_err<<endl;
    cout<<"###################################################################"<<endl;
    cout<<"################ 'Inclusive' Fake Rate ############################"<<endl;
    cout<<"Number of generated tracks: "<<truthInclusiveTracks->GetEntries()<<endl;
    Double_t inclfake = ((Double_t)fakeDenominator->GetEntries())/((Double_t)truthInclusiveTracks->GetEntries());
    Double_t rec_n = (Double_t)fakeDenominator->GetEntries();
    Double_t gen_n = (Double_t)truthInclusiveTracks->GetEntries();
    Double_t inclfake_err = sqrt( (1/rec_n) + (1/gen_n)  );
    cout<<"Ratio of Reconstructed to Generated tracks = "<<inclfake<<" +/- "<<inclfake_err<<endl;
    cout<<"###################################################################"<<endl;
  }
 

  if(doEff==true){
 
 
    //Setting plot colours & widths
    recoTracksAll->SetLineColor(2);  
    recoTracksAll->SetLineWidth(2);
    recoPrimaryTracks->SetLineColor(4);
    recoPrimaryTracks->SetLineWidth(2);
   
    recoSelectedTracks->SetLineColor(8);
    recoSelectedTracks->SetLineWidth(2);
    truthTracksAll->SetLineColor(2);
    truthTracksAll->SetLineWidth(2);  
    truthInclusiveTracks->SetLineColor(4);
    truthInclusiveTracks->SetLineWidth(2); 
    truthSelectedTracks->SetLineColor(8);
    truthSelectedTracks->SetLineWidth(2);
    
    
   
    TH1F *effHisto = new TH1F("efficiencyHisto","Efficiency",10,0,2.5);
    effHisto->SetDirectory(gROOT);
    efficiencyNumerator->Sumw2();
    truthSelectedTracks->Sumw2();
    effHisto->Divide(efficiencyNumerator,truthSelectedTracks,1.0,1.0,"B");
    effHisto->SetLineWidth(2);
    
    c1->cd();
    
    ///Efficiency 
    effHisto->SetMinimum(0.);
    effHisto->SetMaximum(1.1);
    // effHisto->SetTitle(0);  
    effHisto->SetTitle("Efficiency");  
    effHisto->SetXTitle("#\eta");  
    effHisto->SetYTitle("Efficiency");  
    effHisto->SetMarkerStyle(20);//20 for filled dot, 4 for empty dot 
    effHisto->SetMarkerSize(1.8);
    effHisto->SetMarkerColor(pileup_point+2);
    effHisto->SetLineColor(pileup_point+2);
    effHisto->Draw("pe");
    c1->Update();
    c1->SaveAs("plots/efficiency_pileup"+pileup_string+".png");
    
    
  }
  
  if(doFakes==true){
   
  //Fake rate histos
    TH1D *fakeHist = (TH1D*)fakeNumerator->Clone("fakeNumerator");
    fakeHist->SetDirectory(gROOT);
    fakeNumerator->Sumw2();
    fakeDenominator->Sumw2();
    fakeHist->Divide(fakeNumerator,fakeDenominator,1.0,1.0,"B");
    fakeHist->SetLineWidth(2);
    fakeHist->SetMinimum(0.);
    fakeHist->SetTitle("FakeRate");  
    fakeHist->SetXTitle("#\eta");  
    fakeHist->SetYTitle("Fake Rate");  
    fakeHist->SetMarkerStyle(20);//20 for filled dot, 4 for empty dot 
    fakeHist->SetMarkerSize(1.8);
    fakeHist->SetMarkerColor(pileup_point+2);
    fakeHist->SetLineColor(pileup_point+2);
    fakeHist->Draw("pe");
    c1->Update();
    c1->SaveAs("plots/fakerate_pileup"+pileup_string+".png");

   
    
    TH1D *inclFakeHist = (TH1D*)fakeDenominator->Clone("fakeDenominator");
    inclFakeHist->SetDirectory(gROOT);
    fakeDenominator->Sumw2();
    truthSelectedTracks->Sumw2();
    inclFakeHist->Divide(fakeDenominator,truthInclusiveTracks,1.0,1.0,"B");
    inclFakeHist->SetLineWidth(2);
    inclFakeHist->SetMinimum(0.);
    inclFakeHist->SetTitle("Inclusive Rec/Gen");  
    inclFakeHist->SetXTitle("#\eta");  
    inclFakeHist->SetYTitle("Ratio Rec/Gen");  
    inclFakeHist->SetMarkerStyle(20);//20 for filled dot, 4 for empty dot 
    inclFakeHist->SetMarkerSize(1.8);
    inclFakeHist->SetMarkerColor(pileup_point+2);
    inclFakeHist->SetLineColor(pileup_point+2);
    inclFakeHist->Draw("pe");
    c1->Update();
    c1->SaveAs("plots/ratio_pileup"+pileup_string+".png");
  }

 
  efficiency->Fill(pileup[pileup_point],eff);
  efficiency->SetBinError((efficiency->FindBin(pileup[pileup_point])), eff_err);
 
  fakerate->Fill(pileup[pileup_point],fake);
  fakerate->SetBinError((fakerate->FindBin(pileup[pileup_point])), fake_err);
  
  ratio->Fill(pileup[pileup_point],inclfake);
  ratio->SetBinError((ratio->FindBin(pileup[pileup_point])), inclfake_err);
  
 }

 efficiency->SetMaximum(1.1);
 efficiency->SetLineWidth(2);
 efficiency->SetMinimum(0.);
 efficiency->SetTitle("Efficiency");  
 efficiency->SetXTitle("pile-up");  
 efficiency->SetYTitle("Efficiency");  
 efficiency->SetMarkerStyle(20);//20 for filled dot, 4 for empty dot 
 efficiency->SetMarkerSize(1.8);
 efficiency->SetMarkerColor(2);
 efficiency->SetLineColor(2); 
 efficiency->Draw("pe");
 c1->Update();
 c1->SaveAs("plots/eff_vs_pileup.png");
 

 fakerate->SetLineWidth(2);
 fakerate->SetMinimum(0.);
 fakerate->SetTitle("Fake Rate");  
 fakerate->SetXTitle("pile-up");  
 fakerate->SetYTitle("Fake Rate");  
 fakerate->SetMarkerStyle(20);//20 for filled dot, 4 for empty dot 
 fakerate->SetMarkerSize(1.8);
 fakerate->SetMarkerColor(2);
 fakerate->SetLineColor(2); 
 fakerate->Draw("pe");
 c1->Update();
 c1->SaveAs("plots/fakerate_vs_pileup.png");
  
 

 ratio->SetLineWidth(2);
 ratio->SetMinimum(0.);
 ratio->SetTitle("Inclusive Rec/Gen");  
 ratio->SetXTitle("pile-up");  
 ratio->SetYTitle("Rec/Gen");  
 ratio->SetMarkerStyle(20);//20 for filled dot, 4 for empty dot 
 ratio->SetMarkerSize(1.8);
 ratio->SetMarkerColor(2);
 ratio->SetLineColor(2); 
 ratio->Draw("pe");
 c1->Update();
 c1->SaveAs("plots/ratio_vs_pileup.png");
 
 
}


Int_t EventLoop(TString in_file, Double_t z0Cut, Double_t d0Cut, Double_t matchCut,
		Double_t hitsCut,  Double_t chi2cut, Double_t ptCut,Double_t holesCut,Double_t barcode_cut,TH1F *recoTracksAll,
		TH1F *recoPrimaryTracks,TH1F *recoSelectedTracks,TH1F *fakeDenominator,
		TH1F *fakeNumerator ,TH1F *truthTracksAll,TH1F *truthInclusiveTracks,
		TH1F *truthSelectedTracks,TH1F *efficiencyDenominator,TH1F *efficiencyNumerator,TH1F *zProdTruth_hist,
		TH1F *z0tracks, TH1F *d0tracks,TH1F *probtracks,TH1F *hitstracks, Bool_t doFakes, Bool_t doEff)
{

  // Int_t m_particle_ID =13;//<-- Muon
  Int_t m_particle_ID =11;//<-- Electron

  TFile *f = TFile::Open(in_file);

  if (!f) return 0;

  TTree *t1 = (TTree*)f->Get("InDetTrackTree");
  //TTree *t2 = (TTree*)f->Get("Validation/Truth");

  if(!t1) return 0;

  vector<float>   *trk_eta;
  vector<float>   *trk_mc_probability;
  vector<float>   *trk_z0_wrtPV;
  vector<float>   *trk_d0_wrtPV;
  vector<int>     *trk_nHits;
  vector<int>     *trk_nHoles;
  vector<int>     *trk_nBLHits;
  vector<int>     *trk_nPixHits;
  vector<int>     *trk_nSCTHits;
  vector<float>   *trk_pt;
  vector<int>     *trk_mc_barcode;
  vector<int>     *trk_mc_index;
  Int_t           trk_n; 

  vector<int>     *mc_gen_barcode;
  vector<float>     *mc_charge;
  vector<float>   *mc_perigee_z0;
  vector<float>   *mc_perigee_d0;
  vector<float>   *mc_gen_pt;
  vector<float>   *mc_gen_eta;
  Int_t           mc_n;
  vector<float>   *mc_begVtx_x;
  vector<float>   *mc_begVtx_y;
 
  t1->SetBranchAddress("trk_eta", &trk_eta);
  t1->SetBranchAddress("trk_mc_probability", &trk_mc_probability);
  t1->SetBranchAddress("trk_z0_wrtPV", &trk_z0_wrtPV);
  t1->SetBranchAddress("trk_d0_wrtPV", &trk_d0_wrtPV);
  t1->SetBranchAddress("trk_nHits", &trk_nHits);
  t1->SetBranchAddress("trk_nHoles", &trk_nHoles);
  t1->SetBranchAddress("trk_nBLHits", &trk_nBLHits);
  t1->SetBranchAddress("trk_nPixHits", &trk_nPixHits);
  t1->SetBranchAddress("trk_nSCTHits", &trk_nSCTHits);
  t1->SetBranchAddress("trk_pt", &trk_pt);
  t1->SetBranchAddress("trk_mc_barcode", &trk_mc_barcode);
  t1->SetBranchAddress("trk_n", &trk_n); 
  t1->SetBranchAddress("trk_mc_index", &trk_mc_index); 

  t1->SetBranchAddress("mc_charge", &mc_charge);
  t1->SetBranchAddress("mc_gen_barcode", &mc_gen_barcode);
  t1->SetBranchAddress("mc_perigee_d0", &mc_perigee_d0);
  t1->SetBranchAddress("mc_perigee_z0", &mc_perigee_z0);
  t1->SetBranchAddress("mc_gen_pt", &mc_gen_pt);
  t1->SetBranchAddress("mc_gen_eta", &mc_gen_eta);
  t1->SetBranchAddress("mc_n", &mc_n);
  t1->SetBranchAddress("mc_begVtx_x", &mc_begVtx_x);
  t1->SetBranchAddress("mc_begVtx_y", &mc_begVtx_y);
 

  if (doFakes==true){

    Int_t nentries = (Int_t)t1->GetEntries();
    //cout<<"Reading Rec NTuple, #entries: "<< nentries <<endl;


//     //FAKE RATES SECTION
//     //// --> Running over all Reco tracks
     for (Int_t i=0;i<nentries;i++) { 
       t1->GetEntry(i);
       for(Int_t j=0;j<trk_n;j++){
 	if (fabs((*trk_eta)[j]<2.5) ) {
 	  recoTracksAll->Fill(fabs((*trk_eta)[j]));
	}
//       //Checking barcode to see if it is a pythia generated or G4 generated particle
       if(
// 	 //((*trk_mc_barcode)[j]>10000) &&
 	 ((*trk_mc_barcode)[j]!=0) &&
 	 ((*trk_mc_barcode)[j]<200000) &&
 	 (fabs(*trk_eta)[j]<2.5)
 	 )
	recoPrimaryTracks->Fill(fabs(*trk_eta)[j]);
      
      //Applying cuts 
      
      if((fabs((*trk_z0_wrtPV)[j])<z0Cut) &&
	 (fabs((*trk_d0_wrtPV)[j])<d0Cut) &&
	 ((*trk_mc_probability)[j]>matchCut) &&
	 ((*trk_mc_barcode)[j]>barcode_cut) &&
	 ((*trk_mc_barcode)[j]!=0) &&
	 ((*trk_mc_barcode)[j]<200000) &&
	 (fabs((*trk_eta)[j]<2.5)) &&
	 (((*trk_nPixHits)[j]+(*trk_nSCTHits)[j])>=hitsCut)
	 ) {
	recoSelectedTracks->Fill(fabs((*trk_eta)[j]));
      }
      
      //Cuts w/o matching proabability cut for fake rate denominator
      if((fabs((*trk_z0_wrtPV)[j])<z0Cut) &&
	 (fabs((*trk_d0_wrtPV)[j])<d0Cut) &&
	 (fabs((*trk_eta)[j]<2.5)) &&
	 (fabs((*trk_pt)[j])>ptCut) &&
	 ((*trk_nHoles)[j] < holesCut) &&
	 (((*trk_nPixHits)[j]+(*trk_nSCTHits)[j])>=hitsCut)
	 ) {
	fakeDenominator->Fill(fabs((*trk_eta)[j]));
      }
      
      //Cuts with inverted matching probability cut for fake rate numerator
      if((fabs((*trk_z0_wrtPV)[j])<z0Cut) &&
	 (fabs((*trk_d0_wrtPV)[j])<d0Cut) &&
	 (fabs((*trk_eta)[j]<2.5)) &&
	 (fabs((*trk_pt)[j])>ptCut) &&
	 (((*trk_mc_probability)[j]<matchCut)) &&
	  ((*trk_nHoles)[j] < holesCut) &&
	 (((*trk_nPixHits)[j]+(*trk_nSCTHits)[j])>=hitsCut)
	 ) {
	fakeNumerator->Fill(fabs((*trk_eta)[j]));
      }
      
      }
    }
  }


  //cout<<"Starting"<<endl;
  
  if(doEff==true || doFakes==true){

    //cout<<"Efficiencies"<<endl;
    
  //EFFICIENCIES SECTION  
  Int_t nentriesT = (Int_t)t1->GetEntries();
  //cout<<"Reading Truth NTuple, #entries: "<<nentriesT<<endl;
  

  // --> Running over all truth tracks
  for (Int_t i=0;i<nentriesT;i++)
    {
    t1->GetEntry(i);

   std::vector<int> truth_index;  
   std::vector<int> matched_index;
   std::vector<double> truth_eta;

  for(Int_t j=0;j<mc_n;j++){    
     
    if(fabs((*mc_gen_eta)[j]<2.5) &&
       ((*mc_charge)[j]!=0)
       ){
      truthTracksAll->Fill(fabs((*mc_gen_eta)[j]));

      //zProdTruth_hist->Fill(zProdTruth);

    }
    
    //Checking (*mc_gen_barcode)[j] to see if it is a pythia generated or G4 generated particle
    if(
       ((*mc_gen_barcode)[j] != 0) &&
      ((*mc_gen_barcode)[j] < 200000) &&
       (fabs((*mc_gen_eta)[j])<2.5) && 
       (fabs((*mc_perigee_z0)[j])<z0Cut) &&
       (fabs((*mc_perigee_d0)[j])<d0Cut) &&
       (fabs((*mc_charge)[j])>0) &&
       (fabs((*mc_gen_pt)[j])>ptCut)
      ) {
      truthInclusiveTracks->Fill(fabs((*mc_gen_eta)[j]));
    }
     
    Double_t prodX = (Double_t)(*mc_begVtx_x)[j];
    Double_t prodY = (Double_t)(*mc_begVtx_y)[j];
    Double_t prodR = sqrt((prodX * prodX) + (prodY * prodY)); 
    //std::cout<<prodR<<std::endl;

    //Applying cuts to truth tracks
    if(
       ((*mc_gen_barcode)[j] > barcode_cut) &&
       ((*mc_gen_barcode)[j] != 0) &&
      ((*mc_gen_barcode)[j] < 200000) &&
       (fabs((*mc_gen_eta)[j])<2.5) && 
       (fabs((*mc_perigee_z0)[j])<z0Cut) &&
       (fabs((*mc_perigee_d0)[j])<d0Cut) &&
       (fabs((*mc_charge)[j])>0) &&
       (fabs((*mc_gen_pt)[j])>ptCut)
      )
    {
      //cout<<prodR<<endl;
      truthSelectedTracks->Fill(fabs((*mc_gen_eta)[j]));
      //efficiencyDenominator->Fill(fabs((*mc_gen_eta)[j])); 
      truth_index.push_back((int)j);
     
    }
    
 
  }      

 
  for(Int_t j=0;j<trk_n;j++){
    
    //cout<<(*trk_mc_index)[j]<<" trk mc index"<<endl;

    if((*trk_mc_index)[j] == -1) continue;
    Bool_t matched  = false;
    
    for(int unsigned ii=0; ii<truth_index.size(); ii++){
      if(truth_index.at(ii) == (*trk_mc_index)[j]) {
	for(int unsigned jj=0; jj<matched_index.size() ; jj++){
	  if(((*trk_mc_index)[matched_index.at(jj)] == (*trk_mc_index)[j]) &&
	     ((*trk_mc_probability)[j] > 
	     (*trk_mc_probability)[matched_index.at(jj)])
	     ){ 
	    matched_index.at(jj) = j;
	    matched = true;
	  }
	}
	if (matched == false) matched_index.push_back((int)j);
	truth_eta.push_back((*mc_gen_eta)[truth_index.at(ii)]);
      }
    }	
  }
	
  //std::cout<<"matched tracks: "<<matched_index.size()<<std::endl;

  //for(Int_t j=0;j<trk_n;j++){
  for(int unsigned ii=0; ii<matched_index.size(); ii++){

   
  Int_t j = matched_index.at(ii);

   
    
  efficiencyDenominator->Fill(fabs((*mc_gen_eta)[j])); 
    

    if ( fabs((*trk_z0_wrtPV)[j]) < z0Cut ) z0tracks->Fill(fabs((*mc_gen_eta)[j]));
    //cout<<"1"<<endl;
    if ( fabs((*trk_d0_wrtPV)[j]) < d0Cut ) d0tracks->Fill(fabs((*mc_gen_eta)[j]));
    //cout<<"2"<<endl;
    if ((*trk_mc_probability)[j] > matchCut ) probtracks->Fill(fabs((*mc_gen_eta)[j]));
    //cout<<"3"<<endl;
    if (((*trk_nPixHits)[j]+(*trk_nSCTHits)[j]) >= hitsCut && ((*trk_nHoles)[j]<holesCut)) hitstracks->Fill(fabs((*mc_gen_eta)[j]));
    //cout<<"4"<<endl;
    
    //applying cuts to best matched reco track for efficiency numerator
    if(
       ( fabs((*trk_z0_wrtPV)[j]) < z0Cut ) &&
       ( fabs((*trk_d0_wrtPV)[j]) < d0Cut ) &&
       ( (*trk_mc_probability)[j] > matchCut ) &&
       ((*trk_nHoles)[j]<holesCut) &&
       ( ((*trk_nPixHits)[j]+(*trk_nSCTHits)[j]) >= hitsCut )
       )
      {
	//cout<<(*trk_eta)[j]<<endl;
	//efficiencyNumerator->Fill(fabs((*trk_eta)[j]));
	efficiencyNumerator->Fill(fabs(truth_eta.at(ii)));
      }
    
  } 
  }
  
  }
  
  return 1;
  

}


