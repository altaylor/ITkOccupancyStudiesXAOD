#ifndef __CINT__
#include "TFile.h"
#include "TTree.h"
#include "TH2.h"
#include "TCanvas.h"
#include "Riostream.h"
#include <iostream>
#include <sstream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <string>
#include <stdlib.h>
#include <math.h>


using std::cout;
using std::endl;
#endif

void TrackingEfficiency_D3PD()
{
  
  gStyle->SetOptStat(kFALSE);

  TCanvas *c1 = new TCanvas("c1","c1",800,600);

  //// --> Tracking Cuts 
  Double_t z0Cut = 150;
  Double_t d0Cut = 1.0;
  Double_t matchCut = 0.5;
  Double_t hitsCut = 9;
  Double_t holesCut = 1;
  Double_t chi2cut = 10000000;
  Double_t ptCut = 1000;
  Double_t barcode_cut = 0; //10,000 for single particles, 0 otherwise


  //switch efficiency/fakes calculations on & off
  Bool_t doFakes = true;
  Bool_t doEff = true;

 
  TH1F *recoTracksAll   = new TH1F("recoTracksAll","RecEta distribution",20,0,3);
  TH1F *recoInclusiveTracks   = new TH1F("recoInclusiveTracks","RecEta barcode cut",20,0,3);
  TH1F *recoSelectedTracks   = new TH1F("recoSelectedTracks","RecEta Cuts",20,0,3);
  TH1F *fakeDenominator   = new TH1F("fakeDenominator","RecEta Cuts",10,0,2.5);
  TH1F *fakeNumerator   = new TH1F("fakeNumerator","RecEta inverted matching cut",10,0,2.5);
  TH1F *truthTracksAll   = new TH1F("truthTracksAll","TruthEta distribution",20,0,3);
  TH1F *truthInclusiveTracks   = new TH1F("truthInclusiveTracks","TruthEta barcode cut",10,0,2.5);
  TH1F *truthSelectedTracks   = new TH1F("truthSelectedTracks","TruthEta Cuts",10,0,2.5); 
  TH1F *efficiencyDenominator   = new TH1F("efficiencyDenominator","RecEta barcode cut",10,0,2.5);
  TH1F *efficiencyNumerator   = new TH1F("efficiencyNumerator","RecEta Cuts",10,0,2.5);

  TH1F *z0tracks   = new TH1F("z0tracks","z0tracks",20,0,2.5);
  TH1F *d0tracks   = new TH1F("d0tracks","d0tracks",20,0,2.5); 
  TH1F *probtracks   = new TH1F("probtracks","probtracks",10,0,2.5);
  TH1F *hitstracks   = new TH1F("hitstracks","hitstracks",10,0,2.5);
  
  TH1F *zProdTruth_hist   = new TH1F("truth_prod_z","truth_prod_z",100,0.,2000.);
  


  Int_t n_files = 2;

  for(int filenum=0;filenum<n_files;filenum++){

    std::stringstream filename_str;
    
    //define path + filename(s) to be looked at
    //filename_str << "root://castoratlas//castor/cern.ch/atlas/atlascerngroupdisk/det-slhc/user.pvankov.mc11_14TeV.105568.pythia_ttbar.ESD.ibl02.val0.110629160413/user.pvankov.000007.EXT2._0000" << filenum << ".TrkD3PD.root" ;
    //filename_str << "root://castoratlas//castor/cern.ch/atlas/atlascerngroupdisk/det-slhc/aschaeli/17.0.1.1/IBL-02-00-01/mc11_14TeV.105568.pythia_ttbar.pileup50/d3pd/group.det-slhc.24806_000521.EXT2._0000" << filenum << ".TRK_D3PD.root" ;
filename_str << "root://castoratlas//castor/cern.ch/atlas/atlascerngroupdisk/det-slhc/aschaeli/17.0.1.1/IBL-02-00-00/mc11_14TeV.105568.pythia_ttbar.pileup50/d3pd/group.det-slhc.24729_000450.EXT2._0000" << filenum << ".TRK_D3PD.root" ;

    cout<<"file: "<<(filename_str.str()).c_str() <<endl;
    
    //TFile *f = TFile::Open((filename_str.str()).c_str());
    TString in_file = (filename_str.str()).c_str();
    
    EventLoop(in_file,z0Cut,d0Cut,matchCut,hitsCut,chi2cut,ptCut,holesCut,barcode_cut,recoTracksAll,
	      recoInclusiveTracks,recoSelectedTracks,fakeDenominator,fakeNumerator ,
	      truthTracksAll,truthInclusiveTracks,truthSelectedTracks,
	      efficiencyDenominator,efficiencyNumerator,zProdTruth_hist,
	      z0tracks, d0tracks,probtracks,hitstracks,doFakes,doEff);
  
  }
  
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  if(doEff==true){
    cout<<"###################################################################"<<endl;
     cout<<"Number of tracks:                             "<<truthTracksAll->GetEntries()<<endl;
    cout<<"Number of Good MC tracks:                      "<<truthSelectedTracks->GetEntries()<<endl;
    cout<<"Number of Reco tracks before cuts:             "<<efficiencyDenominator->GetEntries()<<endl;
    cout<<"-->Number of Reco tracks after z0 cut:         "<<z0tracks->GetEntries()<<endl;
    cout<<"-->Number of Reco tracks after d0 cut:         "<<d0tracks->GetEntries()<<endl;
    cout<<"-->Number of Reco tracks after matching cut:   "<<probtracks->GetEntries()<<endl;
    cout<<"-->Number of Reco tracks after nhits cut:      "<<hitstracks->GetEntries()<<endl;
    
    cout<<"Number of Reco tracks after cuts:              "<<efficiencyNumerator->GetEntries()<<endl;
    Double_t eff = ((Double_t)efficiencyNumerator->GetEntries())/((Double_t)truthSelectedTracks->GetEntries());
   Double_t eff_err = sqrt(eff * (1 - eff) * ((Double_t)truthSelectedTracks->GetEntries()))/ ((Double_t)truthSelectedTracks->GetEntries()); 
   cout<<"Average Efficiency = "<<eff<<" +/- "<< eff_err <<endl;
    cout<<"###################################################################"<<endl;
    //cout<<"underflows: "<<efficiencyNumerator->GetBinContent(0)<<" "<<truthSelectedTracks->GetBinContent(0)<<endl;
    //cout<<"overflows: "<<efficiencyNumerator->GetBinContent(11)<<" "<<truthSelectedTracks->GetBinContent(11)<<endl;
  }
  
  if(doFakes==true){
    cout<<"################ 'Exclusive' Fake Rate ############################"<<endl;
    cout<<"Number of Good Reco tracks: "<<fakeDenominator->GetEntries()<<endl;
    cout<<"Number of unmatched tracks: "<<fakeNumerator->GetEntries()<<endl;
    Double_t fake = ((Double_t)fakeNumerator->GetEntries())/((Double_t)fakeDenominator->GetEntries());
    Double_t fake_err = sqrt(fake * (1 - fake) * ((Double_t)fakeDenominator->GetEntries()))/ ((Double_t)fakeDenominator->GetEntries());
    cout<<"Average Fake rate = "<<fake<<" +/- "<<fake_err<<endl;
    cout<<"###################################################################"<<endl;
    cout<<"################ 'Inclusive' Fake Rate ############################"<<endl;
    cout<<"Number of generated tracks: "<<truthInclusiveTracks->GetEntries()<<endl;
    Double_t inclfake = ((Double_t)fakeDenominator->GetEntries())/((Double_t)truthInclusiveTracks->GetEntries());
    Double_t rec_n = (Double_t)fakeDenominator->GetEntries();
    Double_t gen_n = (Double_t)truthInclusiveTracks->GetEntries();
    Double_t inclfake_err = sqrt( (1/rec_n) + (1/gen_n)  );
    cout<<"Ratio of Reconstructed to Generated tracks = "<<inclfake<<" +/- "<<inclfake_err<<endl;
    cout<<"###################################################################"<<endl;
  }
 

  if(doEff==true){
 
    //TCanvas *c1 = new TCanvas("c1","c1",800,600);
 
    //TCanvas *c1 = new TCanvas("c1","c1",800,600);
    //Setting plot colours & widths
    recoTracksAll->SetLineColor(2);  
    recoTracksAll->SetLineWidth(2);
    recoInclusiveTracks->SetLineColor(4);
    recoInclusiveTracks->SetLineWidth(2);
    //fakeNumerator->SetLineColor(8);
    //fakeNumerator->SetLineWidth(2);
    recoSelectedTracks->SetLineColor(8);
    recoSelectedTracks->SetLineWidth(2);
    truthTracksAll->SetLineColor(2);
    truthTracksAll->SetLineWidth(2);  
    truthInclusiveTracks->SetLineColor(4);
    truthInclusiveTracks->SetLineWidth(2); 
    truthSelectedTracks->SetLineColor(8);
    truthSelectedTracks->SetLineWidth(2);
    
    
    //efficiency histo
    //TH1D *effHisto = (TH1D*)truthSelectedTracks->Clone("truthSelectedTracks");
    TH1F *effHisto = new TH1F("efficiencyHisto","Efficiency",10,0,2.5);
    effHisto->SetDirectory(gROOT);
    efficiencyNumerator->Sumw2();
    truthSelectedTracks->Sumw2();
    effHisto->Divide(efficiencyNumerator,truthSelectedTracks,1.0,1.0,"B");
    effHisto->SetLineColor(40);
    effHisto->SetLineWidth(2);
    
    c1->cd();
    
    ///Efficiency 
    effHisto->SetMinimum(0.);
    effHisto->SetMaximum(1.1);
    // effHisto->SetTitle(0);  
    effHisto->SetTitle("Efficiency");  
    effHisto->SetXTitle("#\eta");  
    effHisto->SetYTitle("Efficiency");  
    effHisto->SetMarkerStyle(20);//20 for filled dot, 4 for empty dot 
    effHisto->SetMarkerSize(1.8);
    effHisto->SetMarkerColor(2);
    effHisto->Draw("pe");
    
  }
  
  if(doFakes==true){
    TCanvas *c2 = new TCanvas("c2","c2",800,600);

    c2->cd();
  //Fake rate histo
    TH1D *fakeHist = (TH1D*)fakeNumerator->Clone("fakeNumerator");
    fakeHist->SetDirectory(gROOT);
    fakeNumerator->Sumw2();
    fakeDenominator->Sumw2();
    fakeHist->Divide(fakeNumerator,fakeDenominator,1.0,1.0,"B");
    fakeHist->SetLineWidth(2);
    fakeHist->SetMinimum(0.);
    fakeHist->SetTitle("FakeRate");  
    fakeHist->SetXTitle("#\eta");  
    fakeHist->SetYTitle("Fake Rate");  
    fakeHist->SetMarkerStyle(20);//20 for filled dot, 4 for empty dot 
    fakeHist->SetMarkerSize(1.8);
    fakeHist->SetMarkerColor(2);
    fakeHist->Draw("pe");

    TCanvas *c3 = new TCanvas("c3","c3",800,600);

    
    TH1D *inclFakeHist = (TH1D*)fakeDenominator->Clone("fakeDenominator");
    inclFakeHist->SetDirectory(gROOT);
    fakeDenominator->Sumw2();
    truthSelectedTracks->Sumw2();
    inclFakeHist->Divide(fakeDenominator,truthInclusiveTracks,1.0,1.0,"B");
    inclFakeHist->SetLineWidth(2);
    inclFakeHist->SetMinimum(0.);
    inclFakeHist->SetTitle("Inclusive Rec/Gen");  
    inclFakeHist->SetXTitle("#\eta");  
    inclFakeHist->SetYTitle("Ratio Rec/Gen");  
    inclFakeHist->SetMarkerStyle(20);//20 for filled dot, 4 for empty dot 
    inclFakeHist->SetMarkerSize(1.8);
    inclFakeHist->SetMarkerColor(2);
    inclFakeHist->Draw("pe");
  }
}


Int_t EventLoop(TString in_file, Double_t z0Cut, Double_t d0Cut, Double_t matchCut,
		Double_t hitsCut,  Double_t chi2cut, Double_t ptCut,Double_t holesCut,Double_t barcode_cut, TH1F *recoTracksAll,
		TH1F *recoInclusiveTracks,TH1F *recoSelectedTracks,TH1F *fakeDenominator,
		TH1F *fakeNumerator ,TH1F *truthTracksAll,TH1F *truthInclusiveTracks,
		TH1F *truthSelectedTracks,TH1F *efficiencyDenominator,TH1F *efficiencyNumerator,TH1F *zProdTruth_hist,
		TH1F *z0tracks, TH1F *d0tracks,TH1F *probtracks,TH1F *hitstracks, Bool_t doFakes, Bool_t doEff)
{

  // Int_t m_particle_ID =13;//<-- Muon
  Int_t m_particle_ID =11;//<-- Electron

  TFile *f = TFile::Open(in_file);

  if (!f) return 0;

  TTree *t1 = (TTree*)f->Get("InDetTrackTree");
  //TTree *t2 = (TTree*)f->Get("Validation/Truth");

  if(!t1) return 0;

  vector<float>   *trk_eta;
  vector<float>   *trk_mc_probability;
  vector<float>   *trk_z0_wrtPV;
  vector<float>   *trk_d0_wrtPV;
  vector<int>     *trk_nHits;
  vector<int>     *trk_nHoles;
  vector<int>     *trk_nBLHits;
  vector<int>     *trk_nPixHits;
  vector<int>     *trk_nSCTHits;
  vector<float>   *trk_pt;
  vector<int>     *trk_mc_barcode;
  vector<int>     *trk_mc_index;
  Int_t           trk_n; 

  vector<int>     *mc_gen_barcode;
  vector<float>     *mc_charge;
  vector<float>   *mc_perigee_z0;
  vector<float>   *mc_perigee_d0;
  vector<float>   *mc_gen_pt;
  vector<float>   *mc_gen_eta;
  Int_t           mc_n;
  vector<float>   *mc_begVtx_x;
  vector<float>   *mc_begVtx_y;
 
  t1->SetBranchAddress("trk_eta", &trk_eta);
  t1->SetBranchAddress("trk_mc_probability", &trk_mc_probability);
  t1->SetBranchAddress("trk_z0_wrtPV", &trk_z0_wrtPV);
  t1->SetBranchAddress("trk_d0_wrtPV", &trk_d0_wrtPV);
  t1->SetBranchAddress("trk_nHits", &trk_nHits);
  t1->SetBranchAddress("trk_nHoles", &trk_nHoles);
  t1->SetBranchAddress("trk_nBLHits", &trk_nBLHits);
  t1->SetBranchAddress("trk_nPixHits", &trk_nPixHits);
  t1->SetBranchAddress("trk_nSCTHits", &trk_nSCTHits);
  t1->SetBranchAddress("trk_pt", &trk_pt);
  t1->SetBranchAddress("trk_mc_barcode", &trk_mc_barcode);
  t1->SetBranchAddress("trk_n", &trk_n); 
  t1->SetBranchAddress("trk_mc_index", &trk_mc_index); 

  t1->SetBranchAddress("mc_charge", &mc_charge);
  t1->SetBranchAddress("mc_gen_barcode", &mc_gen_barcode);
  t1->SetBranchAddress("mc_perigee_d0", &mc_perigee_d0);
  t1->SetBranchAddress("mc_perigee_z0", &mc_perigee_z0);
  t1->SetBranchAddress("mc_gen_pt", &mc_gen_pt);
  t1->SetBranchAddress("mc_gen_eta", &mc_gen_eta);
  t1->SetBranchAddress("mc_n", &mc_n);
  t1->SetBranchAddress("mc_begVtx_x", &mc_begVtx_x);
  t1->SetBranchAddress("mc_begVtx_y", &mc_begVtx_y);
 

  if (doFakes==true){

    Int_t nentries = (Int_t)t1->GetEntries();
    cout<<"Reading Rec NTuple, #entries: "<< nentries <<endl;


//     //FAKE RATES SECTION
//     //// --> Running over all Reco tracks
     for (Int_t i=0;i<nentries;i++) { 
       t1->GetEntry(i);
       for(Int_t j=0;j<trk_n;j++){
 	if (fabs((*trk_eta)[j]<2.5) ) {
 	  recoTracksAll->Fill(fabs((*trk_eta)[j]));
	}
//       //Checking barcode to see if it is a pythia generated or G4 generated particle
       if(
// 	 //((*trk_mc_barcode)[j]>10000) &&
 	 ((*trk_mc_barcode)[j]!=0) &&
 	 ((*trk_mc_barcode)[j]<200000) &&
 	 (fabs(*trk_eta)[j]<2.5)
 	 )
	recoInclusiveTracks->Fill(fabs(*trk_eta)[j]);
      
      //Applying cuts 
      
      if((fabs((*trk_z0_wrtPV)[j])<z0Cut) &&
	 (fabs((*trk_d0_wrtPV)[j])<d0Cut) &&
	 ((*trk_mc_probability)[j]>matchCut) &&
	 ((*trk_mc_barcode)[j]>barcode_cut) &&
	 ((*trk_mc_barcode)[j]!=0) &&
	 ((*trk_mc_barcode)[j]<200000) &&
	 (fabs((*trk_eta)[j]<2.5)) &&
	 (((*trk_nPixHits)[j]+(*trk_nSCTHits)[j])>=hitsCut)
	 ) {
	recoSelectedTracks->Fill(fabs((*trk_eta)[j]));
      }
    
      //Cuts w/o matching proabability cut for fake rate denominator
      if((fabs((*trk_z0_wrtPV)[j])<z0Cut) &&
	 (fabs((*trk_d0_wrtPV)[j])<d0Cut) &&
	 (fabs((*trk_eta)[j]<2.5)) &&
	 (fabs((*trk_pt)[j])>ptCut) &&
	 ((*trk_nHoles)[j] < holesCut) &&
	 (((*trk_nPixHits)[j]+(*trk_nSCTHits)[j])>=hitsCut)
	 ) {
	fakeDenominator->Fill(fabs((*trk_eta)[j]));
      }
      
      //Cuts with inverted matching probability cut for fake rate numerator
      if((fabs((*trk_z0_wrtPV)[j])<z0Cut) &&
	 (fabs((*trk_d0_wrtPV)[j])<d0Cut) &&
	 (fabs((*trk_eta)[j]<2.5)) &&
	 (fabs((*trk_pt)[j])>ptCut) &&
	 (((*trk_mc_probability)[j]<matchCut)) &&
	 ((*trk_nHoles)[j] < holesCut) &&
	 (((*trk_nPixHits)[j]+(*trk_nSCTHits)[j])>=hitsCut)
	 ) {
	fakeNumerator->Fill(fabs((*trk_eta)[j]));
      }
      
      }
    }
  }


  cout<<"Starting"<<endl;
  
  if(doEff==true || doFakes==true){

    cout<<"Efficiencies"<<endl;
    
  //EFFICIENCIES SECTION  
  Int_t nentriesT = (Int_t)t1->GetEntries();
  cout<<"Reading Truth NTuple, #entries: "<<nentriesT<<endl;
  

  // --> Running over all truth tracks
  for (Int_t i=0;i<nentriesT;i++)
    {
    t1->GetEntry(i);

   std::vector<int> truth_index;  
   std::vector<int> matched_index;
   std::vector<double> truth_eta;

  for(Int_t j=0;j<mc_n;j++){    
     
    if(fabs((*mc_gen_eta)[j]<2.5) &&
       ((*mc_charge)[j]!=0)
       ){
      truthTracksAll->Fill(fabs((*mc_gen_eta)[j]));

      //zProdTruth_hist->Fill(zProdTruth);

    }
    
    //Checking (*mc_gen_barcode)[j] to see if it is a pythia generated or G4 generated particle
    if(
      ((*mc_gen_barcode)[j] != 0 ) &&
      ((*mc_gen_barcode)[j] < 200000) &&
       (fabs((*mc_gen_eta)[j])<2.5) && 
       (fabs((*mc_perigee_z0)[j])<z0Cut) &&
       (fabs((*mc_perigee_d0)[j])<d0Cut) &&
       (fabs((*mc_charge)[j])>0) &&
       (fabs((*mc_gen_pt)[j])>ptCut)
      ) {
      truthInclusiveTracks->Fill(fabs((*mc_gen_eta)[j]));
    }
     
    Double_t prodX = (Double_t)(*mc_begVtx_x)[j];
    Double_t prodY = (Double_t)(*mc_begVtx_y)[j];
    Double_t prodR = sqrt((prodX * prodX) + (prodY * prodY)); 
    //std::cout<<prodR<<std::endl;

    //Applying cuts to truth tracks
    if(
       ((*mc_gen_barcode)[j] > barcode_cut) &&
       ((*mc_gen_barcode)[j] != 0 ) &&
      ((*mc_gen_barcode)[j] < 200000) &&
       (fabs((*mc_gen_eta)[j])<2.5) && 
       (fabs((*mc_perigee_z0)[j])<z0Cut) &&
       (fabs((*mc_perigee_d0)[j])<d0Cut) &&
       (fabs((*mc_charge)[j])>0) &&
       (fabs((*mc_gen_pt)[j])>ptCut)
      )
    {
      //cout<<prodR<<endl;
      truthSelectedTracks->Fill(fabs((*mc_gen_eta)[j]));
      //efficiencyDenominator->Fill(fabs((*mc_gen_eta)[j])); 
      truth_index.push_back((int)j);
     
    }
    
 
  }      

 
  for(Int_t j=0;j<trk_n;j++){
    
    //cout<<(*trk_mc_index)[j]<<" trk mc index"<<endl;

    if((*trk_mc_index)[j] == -1) continue;
    Bool_t matched  = false;
    
    for(int unsigned ii=0; ii<truth_index.size(); ii++){
      if(truth_index.at(ii) == (*trk_mc_index)[j]) {
	for(int unsigned jj=0; jj<matched_index.size() ; jj++){
	  if(((*trk_mc_index)[matched_index.at(jj)] == (*trk_mc_index)[j]) &&
	     ((*trk_mc_probability)[j] > 
	     (*trk_mc_probability)[matched_index.at(jj)])
	     ){ 
	    matched_index.at(jj) = j;
	    matched = true;
	  }
	}
	if (matched == false) matched_index.push_back((int)j);
	truth_eta.push_back((*mc_gen_eta)[truth_index.at(ii)]);
      }
    }	
  }
	
  //std::cout<<"matched tracks: "<<matched_index.size()<<std::endl;

  //for(Int_t j=0;j<trk_n;j++){
  for(int unsigned ii=0; ii<matched_index.size(); ii++){

   
  Int_t j = matched_index.at(ii);

   
    
  efficiencyDenominator->Fill(fabs((*mc_gen_eta)[j])); 
    

    if ( fabs((*trk_z0_wrtPV)[j]) < z0Cut ) z0tracks->Fill(fabs((*mc_gen_eta)[j]));
    //cout<<"1"<<endl;
    if ( fabs((*trk_d0_wrtPV)[j]) < d0Cut ) d0tracks->Fill(fabs((*mc_gen_eta)[j]));
    //cout<<"2"<<endl;
    if ((*trk_mc_probability)[j] > matchCut ) probtracks->Fill(fabs((*mc_gen_eta)[j]));
    //cout<<"3"<<endl;
    if (((*trk_nPixHits)[j]+(*trk_nSCTHits)[j]) >= hitsCut && ((*trk_nHoles)[j]<holesCut)) hitstracks->Fill(fabs((*mc_gen_eta)[j]));
    //cout<<"4"<<endl;
    
    //applying cuts to best matched reco track for efficiency numerator
    if(
       ( fabs((*trk_z0_wrtPV)[j]) < z0Cut ) &&
       ( fabs((*trk_d0_wrtPV)[j]) < d0Cut ) &&
       ( (*trk_mc_probability)[j] > matchCut ) &&
       ((*trk_nHoles)[j]<holesCut) &&
       ( ((*trk_nPixHits)[j]+(*trk_nSCTHits)[j]) >= hitsCut )
       )
      {
	//cout<<(*trk_eta)[j]<<endl;
	//efficiencyNumerator->Fill(fabs((*trk_eta)[j]));
	efficiencyNumerator->Fill(fabs(truth_eta.at(ii)));
      }
    
  } 
  }
  
  }
  
  return 1;
  

}


