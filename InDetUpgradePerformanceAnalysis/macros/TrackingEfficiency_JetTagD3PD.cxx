#ifndef __CINT__
#include "TFile.h"
#include "TTree.h"
#include "TH2.h"
#include "TCanvas.h"
#include "Riostream.h"
#include <iostream>
#include <sstream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <string>
#include <stdlib.h>
#include <math.h>


using std::cout;
using std::endl;
#endif

void TrackingEfficiency_JetTagD3PD()
{
  
  gInterpreter->GenerateDictionary("vector<vector<int> >","vector"); 
  gStyle->SetOptStat(kFALSE);

  TCanvas *c1 = new TCanvas("c1","Efficiency vs. #\eta",800,600);

  //// --> Tracking Cuts 
  Double_t z0Cut = 150;
  Double_t d0Cut = 1.0;
  Double_t matchCut = 0.5;
  Double_t hitsCut = 9;
  Double_t holesCut = 1;
  Double_t chi2cut = 10000000;
  Double_t ptCut = 1000;
  Double_t barcode_cut = 0; //10,000 for single particles, 0 otherwise


  //switch efficiency/fakes calculations on & off
  Bool_t doFakes = true;
  Bool_t doEff = true;

 
  TH1F *recoTracksAll   = new TH1F("recoTracksAll","RecEta distribution",20,0,3);
  TH1F *recoInclusiveTracks   = new TH1F("recoInclusiveTracks","RecEta barcode cut",20,0,3);
  TH1F *recoSelectedTracks   = new TH1F("recoSelectedTracks","RecEta Cuts",20,0,3);
  TH1F *fakeDenominator   = new TH1F("fakeDenominator","RecEta Cuts",10,0,2.5);
  TH1F *fakeNumerator   = new TH1F("fakeNumerator","RecEta inverted matching cut",10,0,2.5);
  TH1F *truthTracksAll   = new TH1F("truthTracksAll","TruthEta distribution",20,0,3);
  TH1F *truthInclusiveTracks   = new TH1F("truthInclusiveTracks","TruthEta barcode cut",10,0,2.5);
  TH1F *truthSelectedTracks   = new TH1F("truthSelectedTracks","TruthEta Cuts",10,0,2.5); 
  TH1F *truthSelectedTracksJetPt   = new TH1F("truthSelectedTracksJetPt","TruthEta Cuts",10,0,600000); 
  TH1F *truthSelectedTracksJetDR   = new TH1F("truthSelectedTracksJetDR","TruthEta Cuts",10,0,0.55); 
  TH1F *efficiencyDenominator   = new TH1F("efficiencyDenominator","RecEta barcode cut",10,0,2.5);
  TH1F *efficiencyNumerator   = new TH1F("efficiencyNumerator","RecEta Cuts",10,0,2.5);
  TH1F *efficiencyNumeratorJetPt   = new TH1F("efficiencyNumeratorJetPt","RecEta Cuts",10,0,600000);
  TH1F *efficiencyNumeratorJetDR   = new TH1F("efficiencyNumeratorJetDR","RecEta Cuts",10,0,0.55); 

  TH1F *z0tracks   = new TH1F("z0tracks","z0tracks",20,0,2.5);
  TH1F *d0tracks   = new TH1F("d0tracks","d0tracks",20,0,2.5); 
  TH1F *probtracks   = new TH1F("probtracks","probtracks",10,0,2.5);
  TH1F *hitstracks   = new TH1F("hitstracks","hitstracks",10,0,2.5);
  
  TH1F *zProdTruth_hist   = new TH1F("truth_prod_z","truth_prod_z",100,0.,2000.);
  


  Int_t n_files = 20;

  for(int filenum=0;filenum<n_files;filenum++){

    std::stringstream filename_str;
    
    //define path + filename(s) to be looked at
    //filename_str << "root://castoratlas//castor/cern.ch/atlas/atlascerngroupdisk/det-slhc/user.pvankov.mc11_14TeV.105568.pythia_ttbar.ESD.ibl02.val0.110629160413/user.pvankov.000007.EXT2._0000" << filenum << ".TrkD3PD.root" ;
    //filename_str << "root://castoratlas//castor/cern.ch/atlas/atlascerngroupdisk/det-slhc/aschaeli/17.0.1.1/IBL-02-00-01/mc11_14TeV.105568.pythia_ttbar.pileup50/d3pd/group.det-slhc.24806_000521.EXT2._0000" << filenum << ".TRK_D3PD.root" ;
    //filename_str << "root://castoratlas//castor/cern.ch/atlas/atlascerngroupdisk/det-slhc/aschaeli/17.0.1.1/IBL-02-00-00/mc11_14TeV.105568.pythia_ttbar.pileup50/d3pd/group.det-slhc.24729_000450.EXT2._0000" << filenum << ".TRK_D3PD.root" ;
    
    //filename_str << "/tmp/nstyles/group.det-slhc.mc11_14TeV.singleParticleMix.ESD.test1.110728175940/group.det-slhc.27877_000135.EXT2._00" << filenum << ".TrkD3PD.root" ;

    if (filenum<10) filename_str << "root://castoratlas//castor/cern.ch/atlas/atlascerngroupdisk/det-slhc/user.tcorneli.mc11_14TeV.105568.pythia_ttbar.JetTagD3PD.ibl02.test6.110706101606/user.tcorneli.000301.EXT0._0000" << filenum << ".JetTagD3PD.root" ;

if (filenum>=10 && filenum<100) filename_str << "root://castoratlas//castor/cern.ch/atlas/atlascerngroupdisk/det-slhc/user.tcorneli.mc11_14TeV.105568.pythia_ttbar.JetTagD3PD.ibl02.test6.110706101606/user.tcorneli.000301.EXT0._000" << filenum << ".JetTagD3PD.root" ;

    cout<<"file: "<<(filename_str.str()).c_str() <<endl;
    
    //TFile *f = TFile::Open((filename_str.str()).c_str());
    TString in_file = (filename_str.str()).c_str();
    
    EventLoop(in_file,z0Cut,d0Cut,matchCut,hitsCut,chi2cut,ptCut,holesCut,barcode_cut,recoTracksAll,
	      recoInclusiveTracks,recoSelectedTracks,fakeDenominator,fakeNumerator ,
	      truthTracksAll,truthInclusiveTracks,truthSelectedTracks,truthSelectedTracksJetPt,truthSelectedTracksJetDR,
	      efficiencyDenominator,efficiencyNumerator,efficiencyNumeratorJetPt,efficiencyNumeratorJetDR,zProdTruth_hist,
	      z0tracks, d0tracks,probtracks,hitstracks,doFakes,doEff);
  
  }
  
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  if(doEff==true){
    cout<<"###################################################################"<<endl;
     cout<<"Number of tracks:                             "<<truthTracksAll->GetEntries()<<endl;
    cout<<"Number of Good MC tracks:                      "<<truthSelectedTracks->GetEntries()<<endl;
    cout<<"Number of Reco tracks before cuts:             "<<efficiencyDenominator->GetEntries()<<endl;
    cout<<"-->Number of Reco tracks after z0 cut:         "<<z0tracks->GetEntries()<<endl;
    cout<<"-->Number of Reco tracks after d0 cut:         "<<d0tracks->GetEntries()<<endl;
    cout<<"-->Number of Reco tracks after matching cut:   "<<probtracks->GetEntries()<<endl;
    cout<<"-->Number of Reco tracks after nhits cut:      "<<hitstracks->GetEntries()<<endl;
    
    cout<<"Number of Reco tracks after cuts:              "<<efficiencyNumerator->GetEntries()<<endl;
    Double_t eff = ((Double_t)efficiencyNumerator->GetEntries())/((Double_t)truthSelectedTracks->GetEntries());
   Double_t eff_err = sqrt(eff * (1 - eff) * ((Double_t)truthSelectedTracks->GetEntries()))/ ((Double_t)truthSelectedTracks->GetEntries()); 
   cout<<"Average Efficiency = "<<eff<<" +/- "<< eff_err <<endl;
    cout<<"###################################################################"<<endl;
    //cout<<"underflows: "<<efficiencyNumerator->GetBinContent(0)<<" "<<truthSelectedTracks->GetBinContent(0)<<endl;
    //cout<<"overflows: "<<efficiencyNumerator->GetBinContent(11)<<" "<<truthSelectedTracks->GetBinContent(11)<<endl;
  }
  
  if(doFakes==true){
    cout<<"################ 'Exclusive' Fake Rate ############################"<<endl;
    cout<<"Number of Good Reco tracks: "<<fakeDenominator->GetEntries()<<endl;
    cout<<"Number of unmatched tracks: "<<fakeNumerator->GetEntries()<<endl;
    Double_t fake = ((Double_t)fakeNumerator->GetEntries())/((Double_t)fakeDenominator->GetEntries());
    Double_t fake_err = sqrt(fake * (1 - fake) * ((Double_t)fakeDenominator->GetEntries()))/ ((Double_t)fakeDenominator->GetEntries());
    cout<<"Average Fake rate = "<<fake<<" +/- "<<fake_err<<endl;
    cout<<"###################################################################"<<endl;
    cout<<"################ 'Inclusive' Fake Rate ############################"<<endl;
    cout<<"Number of generated tracks: "<<truthInclusiveTracks->GetEntries()<<endl;
    Double_t inclfake = ((Double_t)fakeDenominator->GetEntries())/((Double_t)truthInclusiveTracks->GetEntries());
    Double_t rec_n = (Double_t)fakeDenominator->GetEntries();
    Double_t gen_n = (Double_t)truthInclusiveTracks->GetEntries();
    Double_t inclfake_err = sqrt( (1/rec_n) + (1/gen_n)  );
    cout<<"Ratio of Reconstructed to Generated tracks = "<<inclfake<<" +/- "<<inclfake_err<<endl;
    cout<<"###################################################################"<<endl;
  }
 

  if(doEff==true){
 
    //TCanvas *c1 = new TCanvas("c1","c1",800,600);
 
    //TCanvas *c1 = new TCanvas("c1","c1",800,600);
    //Setting plot colours & widths
    recoTracksAll->SetLineColor(2);  
    recoTracksAll->SetLineWidth(2);
    recoInclusiveTracks->SetLineColor(4);
    recoInclusiveTracks->SetLineWidth(2);
    //fakeNumerator->SetLineColor(8);
    //fakeNumerator->SetLineWidth(2);
    recoSelectedTracks->SetLineColor(8);
    recoSelectedTracks->SetLineWidth(2);
    truthTracksAll->SetLineColor(2);
    truthTracksAll->SetLineWidth(2);  
    truthInclusiveTracks->SetLineColor(4);
    truthInclusiveTracks->SetLineWidth(2); 
    truthSelectedTracks->SetLineColor(8);
    truthSelectedTracks->SetLineWidth(2);
    
    
    //efficiency histo
    //TH1D *effHisto = (TH1D*)truthSelectedTracks->Clone("truthSelectedTracks");
    TH1F *effHisto = new TH1F("efficiencyHisto","Efficiency",10,0,2.5);
    effHisto->SetDirectory(gROOT);
    efficiencyNumerator->Sumw2();
    truthSelectedTracks->Sumw2();
    effHisto->Divide(efficiencyNumerator,truthSelectedTracks,1.0,1.0,"B");
    effHisto->SetLineColor(40);
    effHisto->SetLineWidth(2);
    
    c1->cd();
    
    ///Efficiency 
    effHisto->SetMinimum(0.);
    effHisto->SetMaximum(1.1);
    // effHisto->SetTitle(0);  
    effHisto->SetTitle("Efficiency");  
    effHisto->SetXTitle("#\eta");  
    effHisto->SetYTitle("Efficiency");  
    effHisto->SetMarkerStyle(20);//20 for filled dot, 4 for empty dot 
    effHisto->SetMarkerSize(1.8);
    effHisto->SetMarkerColor(2);
    effHisto->Draw("pe");


    TH1F *effHistoJetPt = new TH1F("efficiencyHistoJetPt","Efficiency",10,0,600000);
    effHistoJetPt->SetDirectory(gROOT);
    efficiencyNumeratorJetPt->Sumw2();
    truthSelectedTracksJetPt->Sumw2();
    effHistoJetPt->Divide(efficiencyNumeratorJetPt,truthSelectedTracksJetPt,1.0,1.0,"B");
    effHistoJetPt->SetLineColor(40);
    effHistoJetPt->SetLineWidth(2);
    TCanvas *c1a = new TCanvas("c1a","Efficiency vs . Jet p_{T}",800,600);
    c1a->cd();
    
    ///Efficiency 
    effHistoJetPt->SetMinimum(0.);
    effHistoJetPt->SetMaximum(1.1);
    // effHisto->SetTitle(0);  
    effHistoJetPt->SetTitle("Efficiency");  
    effHistoJetPt->SetXTitle("Jet p_{T}");  
    effHistoJetPt->SetYTitle("Efficiency");  
    effHistoJetPt->SetMarkerStyle(20);//20 for filled dot, 4 for empty dot 
    effHistoJetPt->SetMarkerSize(1.8);
    effHistoJetPt->SetMarkerColor(2);
    effHistoJetPt->Draw("pe");

    TH1F *effHistoJetDR = new TH1F("efficiencyHistoJetDR","Efficiency",10,0,0.55);
    effHistoJetDR->SetDirectory(gROOT);
    efficiencyNumeratorJetDR->Sumw2();
    truthSelectedTracksJetDR->Sumw2();
    effHistoJetDR->Divide(efficiencyNumeratorJetDR,truthSelectedTracksJetDR,1.0,1.0,"B");
    effHistoJetDR->SetLineColor(40);
    effHistoJetDR->SetLineWidth(2);
    TCanvas *c1b = new TCanvas("c1b","Efficiency vs. Track #\Delta R_{Jet}",800,600);
    c1b->cd();
    
    ///Efficiency 
    effHistoJetDR->SetMinimum(0.);
    effHistoJetDR->SetMaximum(1.1);
    // effHisto->SetTitle(0);  
    effHistoJetDR->SetTitle("Efficiency");  
    effHistoJetDR->SetXTitle("Track #\Delta R_{Jet}");  
    effHistoJetDR->SetYTitle("Efficiency");  
    effHistoJetDR->SetMarkerStyle(20);//20 for filled dot, 4 for empty dot 
    effHistoJetDR->SetMarkerSize(1.8);
    effHistoJetDR->SetMarkerColor(2);
    effHistoJetDR->Draw("pe");
    
  }
  
  if(doFakes==true){
    TCanvas *c2 = new TCanvas("c2","Fake Rate vs. #\eta",800,600);

    c2->cd();
  //Fake rate histo
    TH1D *fakeHist = (TH1D*)fakeNumerator->Clone("fakeNumerator");
    fakeHist->SetDirectory(gROOT);
    fakeNumerator->Sumw2();
    fakeDenominator->Sumw2();
    fakeHist->Divide(fakeNumerator,fakeDenominator,1.0,1.0,"B");
    fakeHist->SetLineWidth(2);
    fakeHist->SetMinimum(0.);
    fakeHist->SetTitle("FakeRate");  
    fakeHist->SetXTitle("#\eta");  
    fakeHist->SetYTitle("Fake Rate");  
    fakeHist->SetMarkerStyle(20);//20 for filled dot, 4 for empty dot 
    fakeHist->SetMarkerSize(1.8);
    fakeHist->SetMarkerColor(2);
    fakeHist->Draw("pe");

    TCanvas *c3 = new TCanvas("c3","Inclusive Rec/Gen vs. #\eta",800,600);

    
    TH1D *inclFakeHist = (TH1D*)fakeDenominator->Clone("fakeDenominator");
    inclFakeHist->SetDirectory(gROOT);
    fakeDenominator->Sumw2();
    truthSelectedTracks->Sumw2();
    inclFakeHist->Divide(fakeDenominator,truthInclusiveTracks,1.0,1.0,"B");
    inclFakeHist->SetLineWidth(2);
    inclFakeHist->SetMinimum(0.);
    inclFakeHist->SetTitle("Inclusive Rec/Gen");  
    inclFakeHist->SetXTitle("#\eta");  
    inclFakeHist->SetYTitle("Ratio Rec/Gen");  
    inclFakeHist->SetMarkerStyle(20);//20 for filled dot, 4 for empty dot 
    inclFakeHist->SetMarkerSize(1.8);
    inclFakeHist->SetMarkerColor(2);
    inclFakeHist->Draw("pe");
  }
}


Int_t EventLoop(TString in_file, Double_t z0Cut, Double_t d0Cut, Double_t matchCut,
		Double_t hitsCut,  Double_t chi2cut, Double_t ptCut,Double_t holesCut,Double_t barcode_cut, TH1F *recoTracksAll,
		TH1F *recoInclusiveTracks,TH1F *recoSelectedTracks,TH1F *fakeDenominator,
		TH1F *fakeNumerator ,TH1F *truthTracksAll,TH1F *truthInclusiveTracks,
		TH1F *truthSelectedTracks,TH1F *truthSelectedTracksJetPt,TH1F *truthSelectedTracksJetDR,TH1F *efficiencyDenominator,TH1F *efficiencyNumerator,
		TH1F *efficiencyNumeratorJetPt,TH1F *efficiencyNumeratorJetDR,TH1F *zProdTruth_hist,
		TH1F *z0tracks, TH1F *d0tracks,TH1F *probtracks,TH1F *hitstracks, Bool_t doFakes, Bool_t doEff)
{

  // Int_t m_particle_ID =13;//<-- Muon
  Int_t m_particle_ID =11;//<-- Electron

  TFile *f = TFile::Open(in_file);

  if (!f) return 0;

  //TTree *t1 = (TTree*)f->Get("InDetTrackTree");
  TTree *t1 = (TTree*)f->Get("btagd3pd");
  //TTree *t2 = (TTree*)f->Get("Validation/Truth");

  if(!t1) return 0;

  vector<float>   *trk_eta;
  vector<float>   *trk_mcpart_probability;
  vector<float>   *trk_z0_wrtPV;
  vector<float>   *trk_d0_wrtPV;
  //vector<int>     *trk_nHits;
  vector<int>     *trk_nPixHoles;
  vector<int>     *trk_nSCTHoles;
  vector<int>     *trk_nBLHits;
  vector<int>     *trk_nPixHits;
  vector<int>     *trk_nSCTHits;
  vector<float>   *trk_pt;
  vector<int>     *trk_mcpart_barcode;
  vector<int>     *trk_mcpart_index;
  Int_t           trk_n; 

  vector<int>     *mcpart_barcode;
  vector<int>     *mcpart_truthtracks_index;
  //vector<float>     *mcpart_charge;
  vector<float>   *truthtrack_z0;
  vector<float>   *truthtrack_d0;
  vector<float>   *mcpart_pt;
  vector<float>   *mcpart_eta;
   vector<float>   *mcpart_phi;
  Int_t           mcpart_n;
  vector<int>   *mcpart_type;

  Int_t           jet_n;
  vector<float>   *jet_pt;
  vector<float>   *jet_eta;
  vector<float>   *jet_phi;
  vector<float>   *jet_deltaPhi;
  vector<float>   *jet_deltaEta;

  vector<int>     *jet_assoctrk_n;
  vector<vector<int> > *jet_assoctrk_index;
 
  t1->SetBranchAddress("trk_eta", &trk_eta);
  t1->SetBranchAddress("trk_mcpart_probability", &trk_mcpart_probability);
  t1->SetBranchAddress("trk_z0_wrtPV", &trk_z0_wrtPV);
  t1->SetBranchAddress("trk_d0_wrtPV", &trk_d0_wrtPV);
  //t1->SetBranchAddress("trk_nHits", &trk_nHits);
  t1->SetBranchAddress("trk_nPixHoles", &trk_nPixHoles);
  t1->SetBranchAddress("trk_nSCTHoles", &trk_nSCTHoles);
  t1->SetBranchAddress("trk_nBLHits", &trk_nBLHits);
  t1->SetBranchAddress("trk_nPixHits", &trk_nPixHits);
  t1->SetBranchAddress("trk_nSCTHits", &trk_nSCTHits);
  t1->SetBranchAddress("trk_pt", &trk_pt);
  t1->SetBranchAddress("trk_mcpart_barcode", &trk_mcpart_barcode);
  t1->SetBranchAddress("trk_n", &trk_n); 
  t1->SetBranchAddress("trk_mcpart_index", &trk_mcpart_index); 

  //t1->SetBranchAddress("mcpart_charge", &mcpart_charge);
  t1->SetBranchAddress("mcpart_barcode", &mcpart_barcode);
  t1->SetBranchAddress("truthtrack_d0", &truthtrack_d0);
  t1->SetBranchAddress("truthtrack_z0", &truthtrack_z0);
  t1->SetBranchAddress("mcpart_pt", &mcpart_pt);
  t1->SetBranchAddress("mcpart_eta", &mcpart_eta);
  t1->SetBranchAddress("mcpart_phi", &mcpart_phi);
  t1->SetBranchAddress("mcpart_n", &mcpart_n);
  t1->SetBranchAddress("mcpart_type", &mcpart_type);
  t1->SetBranchAddress("mcpart_truthtracks_index", &mcpart_truthtracks_index);
  
  t1->SetBranchAddress("jet_antikt4truth_n", &jet_n);
  t1->SetBranchAddress("jet_antikt4truth_pt", &jet_pt);
  t1->SetBranchAddress("jet_antikt4truth_eta", &jet_eta);
  t1->SetBranchAddress("jet_antikt4truth_phi", &jet_phi);
  t1->SetBranchAddress("jet_antikt4truth_flavor_component_jfit_deltaPhi", &jet_deltaPhi);
  t1->SetBranchAddress("jet_antikt4truth_flavor_component_jfit_deltaEta", &jet_deltaEta);
  t1->SetBranchAddress("jet_antikt4truth_flavor_component_assoctrk_n", &jet_assoctrk_n);
  t1->SetBranchAddress("jet_antikt4truth_flavor_component_assoctrk_index", &jet_assoctrk_index);
 
  if (doFakes==true){

    Int_t nentries = (Int_t)t1->GetEntries();
    cout<<"Reading Rec NTuple, #entries: "<< nentries <<endl;



//     //FAKE RATES SECTION
//     //// --> Running over all Reco tracks
     for (Int_t i=0;i<nentries;i++) { 
       t1->GetEntry(i);



       for(Int_t j=0;j<trk_n;j++){
 	if (fabs((*trk_eta)[j]<2.5) ) {
 	  recoTracksAll->Fill(fabs((*trk_eta)[j]));
	}
//       //Checking barcode to see if it is a pythia generated or G4 generated particle
       if(
// 	 //((*trk_mcpart_barcode)[j]>10000) &&
 	 ((*trk_mcpart_barcode)[j]!=0) &&
 	 ((*trk_mcpart_barcode)[j]<200000) &&
 	 (fabs(*trk_eta)[j]<2.5)
 	 )
	recoInclusiveTracks->Fill(fabs(*trk_eta)[j]);
      
      //Applying cuts 
      
      if((fabs((*trk_z0_wrtPV)[j])<z0Cut) &&
	 (fabs((*trk_d0_wrtPV)[j])<d0Cut) &&
	 ((*trk_mcpart_probability)[j]>matchCut) &&
	 ((*trk_mcpart_barcode)[j]>barcode_cut) &&
	 ((*trk_mcpart_barcode)[j]!=0) &&
	 ((*trk_mcpart_barcode)[j]<200000) &&
	 (fabs((*trk_eta)[j]<2.5)) &&
	 (((*trk_nPixHits)[j]+(*trk_nSCTHits)[j])>=hitsCut)
	 ) {
	recoSelectedTracks->Fill(fabs((*trk_eta)[j]));
      }
    
      //Cuts w/o matching proabability cut for fake rate denominator
      if((fabs((*trk_z0_wrtPV)[j])<z0Cut) &&
	 (fabs((*trk_d0_wrtPV)[j])<d0Cut) &&
	 (fabs((*trk_eta)[j]<2.5)) &&
	 (fabs((*trk_pt)[j])>ptCut) &&
	 ((*trk_nPixHoles)[j]+(*trk_nSCTHoles)[j] < holesCut) &&
	 (((*trk_nPixHits)[j]+(*trk_nSCTHits)[j])>=hitsCut)
	 ) {
	fakeDenominator->Fill(fabs((*trk_eta)[j]));
      }
      
      //Cuts with inverted matching probability cut for fake rate numerator
      if((fabs((*trk_z0_wrtPV)[j])<z0Cut) &&
	 (fabs((*trk_d0_wrtPV)[j])<d0Cut) &&
	 (fabs((*trk_eta)[j]<2.5)) &&
	 (fabs((*trk_pt)[j])>ptCut) &&
	 (((*trk_mcpart_probability)[j]<matchCut)) &&
	 ((*trk_nPixHoles)[j]+(*trk_nSCTHoles)[j] < holesCut) &&
	 (((*trk_nPixHits)[j]+(*trk_nSCTHits)[j])>=hitsCut)
	 ) {
	fakeNumerator->Fill(fabs((*trk_eta)[j]));
      }
      
      }
    }
  }


  cout<<"Starting"<<endl;
  
  if(doEff==true || doFakes==true){

    cout<<"Efficiencies"<<endl;
    
  //EFFICIENCIES SECTION  
  Int_t nentriesT = (Int_t)t1->GetEntries();
  cout<<"Reading Truth NTuple, #entries: "<<nentriesT<<endl;
  

  Float_t highest_pt = 0;

  // --> Running over all truth tracks
  for (Int_t i=0;i<nentriesT;i++)
    {
    t1->GetEntry(i);

   bool doJetDeltaRMatching = false;
   Int_t n_jets = 1;

   if(doJetDeltaRMatching){
     n_jets = jet_n;
   }


     std::vector<int> truth_index;  
     std::vector<int> matched_index;
     std::vector<double> truth_eta;
     std::vector<double> truth_phi;
     std::vector<int> truth_jet_index;
     std::vector<double> truth_jet_pt;
     std::vector<double> truth_jet_eta;
     std::vector<double> truth_jet_phi;
     
  for(Int_t j=0;j<mcpart_n;j++){    

    Int_t truthtrack_index;
    truthtrack_index = (*mcpart_truthtracks_index)[j];
   
     
    if(fabs((*mcpart_eta)[j]<2.5) &&
       ( truthtrack_index != -1)  
       ){
      truthTracksAll->Fill(fabs((*mcpart_eta)[j]));

     

    }
    
   
    //Checking (*mcpart_barcode)[j] to see if it is a pythia generated or G4 generated particle
    if(
       ((*mcpart_barcode)[j] != 0 ) &&
       ((*mcpart_barcode)[j] < 200000) &&
       (fabs((*mcpart_eta)[j])<2.5) && 
       ( truthtrack_index != -1) && 
       (fabs((*truthtrack_z0)[truthtrack_index])<z0Cut) &&
       (fabs((*truthtrack_d0)[truthtrack_index])<d0Cut) &&
       (fabs((*mcpart_pt)[j])>ptCut)
      ) {
      truthInclusiveTracks->Fill(fabs((*mcpart_eta)[j]));
    }



    Int_t closest_jet_index = -1;
    Float_t closest_jet_dR = 9999;

    //finding jrt to which this track is closest

    for(Int_t ijet=0;ijet<n_jets;ijet++){
      
      Float_t diff_eta = fabs((*mcpart_eta)[j] - (*jet_eta)[ijet]);
      Float_t diff_phi = fabs((*mcpart_phi)[j] - (*jet_phi)[ijet]);
      Float_t diff_R = sqrt(diff_eta*diff_eta + diff_phi*diff_phi);
      
      if (diff_R < closest_jet_dR){
	closest_jet_dR = diff_R;
	closest_jet_index = ijet;
      }
      
    }
    

    //calculating pt-dependent jet cone size for dR matching
    Float_t param1 = 0.239;
    Float_t param2 = -1.22;
    Float_t param3 = -0.0000164;
    Float_t closest_jet_pt = (*jet_pt)[closest_jet_index];
    
    Float_t jet_cone = param1 + exp(param2 + param3*closest_jet_pt);

    //checking if truth track matches criteria to go into denominator
    if(
       (
	(closest_jet_dR < jet_cone) 
        || (doJetDeltaRMatching==false)
	) && 
       ((*mcpart_barcode)[j] > barcode_cut) &&
       ((*mcpart_barcode)[j] != 0 ) &&
       ((*mcpart_barcode)[j] < 200000) &&
       (fabs((*mcpart_eta)[j])<2.5) && 
       ( truthtrack_index != -1) && 
       (fabs((*truthtrack_z0)[ truthtrack_index])<z0Cut) &&
       (fabs((*truthtrack_d0)[ truthtrack_index])<d0Cut) &&
       (fabs((*mcpart_pt)[j])>ptCut)
      )
    {
     
      truthSelectedTracks->Fill(fabs((*mcpart_eta)[j]));
      truthSelectedTracksJetPt->Fill((*jet_pt)[closest_jet_index]);
      Float_t deltaEta = (*mcpart_eta)[j] - (*jet_eta)[closest_jet_index];
      Float_t deltaPhi = (*mcpart_phi)[j] - (*jet_phi)[closest_jet_index];
      Float_t deltaR = sqrt(deltaEta*deltaEta + deltaPhi*deltaPhi);
      truthSelectedTracksJetDR->Fill(deltaR);
      
      truth_index.push_back((int)j);
      truth_jet_index.push_back((int)closest_jet_index);
    }
    
   
  }

        
   
  //looping over reco tracks
  for(Int_t j=0;j<trk_n;j++){
    
   
    //finding best macthed reco track for each truth track
    if((*trk_mcpart_index)[j] == -1) continue;
    Bool_t matched  = false;
    
    for(int unsigned ii=0; ii<truth_index.size(); ii++){
      if(truth_index.at(ii) == (*trk_mcpart_index)[j]) {
	for(int unsigned jj=0; jj<matched_index.size() ; jj++){
	  if(((*trk_mcpart_index)[matched_index.at(jj)] == (*trk_mcpart_index)[j]) &&
	     ((*trk_mcpart_probability)[j] > 
	     (*trk_mcpart_probability)[matched_index.at(jj)])
	     ){ 
	    matched_index.at(jj) = j;
	    matched = true;
	  }
	}
	if (matched == false) matched_index.push_back((int)j);
	truth_eta.push_back((*mcpart_eta)[truth_index.at(ii)]);
	truth_phi.push_back((*mcpart_phi)[truth_index.at(ii)]);
	truth_jet_pt.push_back((*jet_pt)[truth_jet_index.at(ii)]);
	truth_jet_eta.push_back((*jet_eta)[truth_jet_index.at(ii)]);
	truth_jet_phi.push_back((*jet_phi)[truth_jet_index.at(ii)]);
      }
    }	
  }
	
 
  for(int unsigned ii=0; ii<matched_index.size(); ii++){

   
  Int_t j = matched_index.at(ii);

   
    
  efficiencyDenominator->Fill(fabs((*mcpart_eta)[j])); 
    

    if ( fabs((*trk_z0_wrtPV)[j]) < z0Cut ) z0tracks->Fill(fabs((*mcpart_eta)[j]));
    //cout<<"1"<<endl;
    if ( fabs((*trk_d0_wrtPV)[j]) < d0Cut ) d0tracks->Fill(fabs((*mcpart_eta)[j]));
    //cout<<"2"<<endl;
    if ((*trk_mcpart_probability)[j] > matchCut ) probtracks->Fill(fabs((*mcpart_eta)[j]));
    //cout<<"3"<<endl;
    if (((*trk_nPixHits)[j]+(*trk_nSCTHits)[j]) >= hitsCut && ((*trk_nPixHoles)[j]+(*trk_nSCTHoles)[j]<holesCut)) hitstracks->Fill(fabs((*mcpart_eta)[j]));
    //cout<<"4"<<endl;
    
    //applying cuts to best matched reco track for efficiency numerator
    if(
       ( fabs((*trk_z0_wrtPV)[j]) < z0Cut ) &&
       ( fabs((*trk_d0_wrtPV)[j]) < d0Cut ) &&
       ( (*trk_mcpart_probability)[j] > matchCut ) &&
       ((*trk_nPixHoles)[j]+(*trk_nSCTHoles)[j]<holesCut) &&
       ( ((*trk_nPixHits)[j]+(*trk_nSCTHits)[j]) >= hitsCut )
       )
      {

	efficiencyNumerator->Fill(fabs(truth_eta.at(ii)));
	efficiencyNumeratorJetPt->Fill(truth_jet_pt.at(ii));
	Float_t deltaEta = truth_eta.at(ii) - truth_jet_eta.at(ii);
	Float_t deltaPhi = truth_phi.at(ii) - truth_jet_phi.at(ii);
	Float_t deltaR = sqrt(deltaEta*deltaEta + deltaPhi*deltaPhi);
	efficiencyNumeratorJetDR->Fill(deltaR);
      }
   
  } 
   } 
    
  
  }
  
  return 1;
  
  
}


