void occ_pixdisk_mgr()
{
//=========Macro generated from canvas: c1_n13/c1_n13
//=========  (Mon Apr 25 16:02:06 2016) by ROOT version6.04/12
   TCanvas *c1_n13 = new TCanvas("c1_n13", "c1_n13",0,0,700,500);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   c1_n13->SetHighLightColor(2);
   c1_n13->Range(59.56247,-0.0002310467,334.9837,0.004130753);
   c1_n13->SetFillColor(0);
   c1_n13->SetBorderMode(0);
   c1_n13->SetBorderSize(2);
   c1_n13->SetTickx(1);
   c1_n13->SetTicky(1);
   c1_n13->SetFrameBorderMode(0);
   c1_n13->SetFrameBorderMode(0);
   
   TMultiGraph *multigraph = new TMultiGraph();
   multigraph->SetName("occ_pixdisk_mgr");
   multigraph->SetTitle("");
   
   Double_t occ_r_pixdisk_0_fx1069[4] = {
   97.11992,
   98.16271,
   98.49258,
   98.6056};
   Double_t occ_r_pixdisk_0_fy1069[4] = {
   0.002579365,
   0.002427455,
   0.002759177,
   0.002092634};
   Double_t occ_r_pixdisk_0_fex1069[4] = {
   0,
   0,
   0,
   0};
   Double_t occ_r_pixdisk_0_fey1069[4] = {
   5.577026e-07,
   2.705172e-07,
   6.555383e-07,
   1.803561e-07};
   TGraphErrors *gre = new TGraphErrors(4,occ_r_pixdisk_0_fx1069,occ_r_pixdisk_0_fy1069,occ_r_pixdisk_0_fex1069,occ_r_pixdisk_0_fey1069);
   gre->SetName("occ_r_pixdisk_0");
   gre->SetTitle("Disk 0");
   gre->SetFillColor(1);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_occ_r_pixdisk_01069 = new TH1F("Graph_occ_r_pixdisk_01069","Disk 0",100,96.97135,98.75417);
   Graph_occ_r_pixdisk_01069->SetMinimum(0.002025716);
   Graph_occ_r_pixdisk_01069->SetMaximum(0.00282657);
   Graph_occ_r_pixdisk_01069->SetDirectory(0);
   Graph_occ_r_pixdisk_01069->SetStats(0);
   gre->SetHistogram(Graph_occ_r_pixdisk_01069);
   
   multigraph->Add(gre,"lp");
   
   Double_t occ_r_pixdisk_1_fx1070[4] = {
   168.2737,
   168.5629,
   167.8105,
   169.1694};
   Double_t occ_r_pixdisk_1_fy1070[4] = {
   0.0009321264,
   0.001033399,
   0.001240079,
   0.0009796628};
   Double_t occ_r_pixdisk_1_fex1070[4] = {
   0,
   0,
   0,
   0};
   Double_t occ_r_pixdisk_1_fey1070[4] = {
   2.084277e-07,
   4.19096e-07,
   4.090313e-07,
   2.564985e-07};
   gre = new TGraphErrors(4,occ_r_pixdisk_1_fx1070,occ_r_pixdisk_1_fy1070,occ_r_pixdisk_1_fex1070,occ_r_pixdisk_1_fey1070);
   gre->SetName("occ_r_pixdisk_1");
   gre->SetTitle("Disk 1");
   gre->SetFillColor(1);
   gre->SetLineColor(2);
   gre->SetMarkerColor(2);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_occ_r_pixdisk_11070 = new TH1F("Graph_occ_r_pixdisk_11070","Disk 1",100,167.6746,169.3053);
   Graph_occ_r_pixdisk_11070->SetMinimum(0.0009010609);
   Graph_occ_r_pixdisk_11070->SetMaximum(0.001271345);
   Graph_occ_r_pixdisk_11070->SetDirectory(0);
   Graph_occ_r_pixdisk_11070->SetStats(0);
   gre->SetHistogram(Graph_occ_r_pixdisk_11070);
   
   multigraph->Add(gre,"lp");
   
   Double_t occ_r_pixdisk_2_fx1071[4] = {
   231.2599,
   231.0032,
   231.0164,
   232.6809};
   Double_t occ_r_pixdisk_2_fy1071[4] = {
   0.0006153894,
   0.0007424976,
   0.0007440476,
   0.0007378472};
   Double_t occ_r_pixdisk_2_fex1071[4] = {
   0,
   0,
   0,
   0};
   Double_t occ_r_pixdisk_2_fey1071[4] = {
   2.548508e-07,
   4.268766e-07,
   8.062506e-07,
   3.536895e-07};
   gre = new TGraphErrors(4,occ_r_pixdisk_2_fx1071,occ_r_pixdisk_2_fy1071,occ_r_pixdisk_2_fex1071,occ_r_pixdisk_2_fey1071);
   gre->SetName("occ_r_pixdisk_2");
   gre->SetTitle("Disk 2");
   gre->SetFillColor(1);
   gre->SetLineColor(3);
   gre->SetMarkerColor(3);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_occ_r_pixdisk_21071 = new TH1F("Graph_occ_r_pixdisk_21071","Disk 2",100,230.8355,232.8487);
   Graph_occ_r_pixdisk_21071->SetMinimum(0.0006021627);
   Graph_occ_r_pixdisk_21071->SetMaximum(0.0007578257);
   Graph_occ_r_pixdisk_21071->SetDirectory(0);
   Graph_occ_r_pixdisk_21071->SetStats(0);
   gre->SetHistogram(Graph_occ_r_pixdisk_21071);
   
   multigraph->Add(gre,"lp");
   
   Double_t occ_r_pixdisk_3_fx1072[4] = {
   295.8305,
   294.3915,
   294.292,
   293.0196};
   Double_t occ_r_pixdisk_3_fy1072[4] = {
   0.0003571429,
   0.0005009921,
   0.0003918651,
   0.0003943453};
   Double_t occ_r_pixdisk_3_fex1072[4] = {
   0,
   0,
   0,
   0};
   Double_t occ_r_pixdisk_3_fey1072[4] = {
   2.587889e-07,
   3.334275e-07,
   2.417392e-07,
   2.578611e-07};
   gre = new TGraphErrors(4,occ_r_pixdisk_3_fx1072,occ_r_pixdisk_3_fy1072,occ_r_pixdisk_3_fex1072,occ_r_pixdisk_3_fey1072);
   gre->SetName("occ_r_pixdisk_3");
   gre->SetTitle("Disk 3");
   gre->SetFillColor(1);
   gre->SetLineColor(4);
   gre->SetMarkerColor(4);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_occ_r_pixdisk_31072 = new TH1F("Graph_occ_r_pixdisk_31072","Disk 3",100,292.7385,296.1116);
   Graph_occ_r_pixdisk_31072->SetMinimum(0.0003424399);
   Graph_occ_r_pixdisk_31072->SetMaximum(0.0005157696);
   Graph_occ_r_pixdisk_31072->SetDirectory(0);
   Graph_occ_r_pixdisk_31072->SetStats(0);
   gre->SetHistogram(Graph_occ_r_pixdisk_31072);
   
   multigraph->Add(gre,"lp");
   
   Double_t occ_r_pixdisk_4_fx1073[4] = {
   97.21445,
   97.88796,
   99.10734,
   98.11107};
   Double_t occ_r_pixdisk_4_fy1073[4] = {
   0.002616567,
   0.002672371,
   0.002799479,
   0.002821181};
   Double_t occ_r_pixdisk_4_fex1073[4] = {
   0,
   0,
   0,
   0};
   Double_t occ_r_pixdisk_4_fey1073[4] = {
   2.086926e-07,
   1.967063e-07,
   2.118826e-07,
   2.874735e-07};
   gre = new TGraphErrors(4,occ_r_pixdisk_4_fx1073,occ_r_pixdisk_4_fy1073,occ_r_pixdisk_4_fex1073,occ_r_pixdisk_4_fey1073);
   gre->SetName("occ_r_pixdisk_4");
   gre->SetTitle("Disk 0");
   gre->SetFillColor(1);
   gre->SetLineStyle(2);
   gre->SetMarkerStyle(21);
   
   TH1F *Graph_occ_r_pixdisk_41073 = new TH1F("Graph_occ_r_pixdisk_41073","Disk 0",100,97.02516,99.29663);
   Graph_occ_r_pixdisk_41073->SetMinimum(0.002595848);
   Graph_occ_r_pixdisk_41073->SetMaximum(0.002841979);
   Graph_occ_r_pixdisk_41073->SetDirectory(0);
   Graph_occ_r_pixdisk_41073->SetStats(0);
   gre->SetHistogram(Graph_occ_r_pixdisk_41073);
   
   multigraph->Add(gre,"lp");
   
   Double_t occ_r_pixdisk_5_fx1074[4] = {
   168.1227,
   170.2475,
   168.4129,
   169.3295};
   Double_t occ_r_pixdisk_5_fy1074[4] = {
   0.0010458,
   0.001023066,
   0.0009796628,
   0.0009321264};
   Double_t occ_r_pixdisk_5_fex1074[4] = {
   0,
   0,
   0,
   0};
   Double_t occ_r_pixdisk_5_fey1074[4] = {
   2.335174e-07,
   4.244415e-07,
   4.089694e-07,
   2.543807e-07};
   gre = new TGraphErrors(4,occ_r_pixdisk_5_fx1074,occ_r_pixdisk_5_fy1074,occ_r_pixdisk_5_fex1074,occ_r_pixdisk_5_fey1074);
   gre->SetName("occ_r_pixdisk_5");
   gre->SetTitle("Disk 1");
   gre->SetFillColor(1);
   gre->SetLineColor(2);
   gre->SetLineStyle(2);
   gre->SetMarkerColor(2);
   gre->SetMarkerStyle(21);
   
   TH1F *Graph_occ_r_pixdisk_51074 = new TH1F("Graph_occ_r_pixdisk_51074","Disk 1",100,167.9103,170.46);
   Graph_occ_r_pixdisk_51074->SetMinimum(0.0009204558);
   Graph_occ_r_pixdisk_51074->SetMaximum(0.00105745);
   Graph_occ_r_pixdisk_51074->SetDirectory(0);
   Graph_occ_r_pixdisk_51074->SetStats(0);
   gre->SetHistogram(Graph_occ_r_pixdisk_51074);
   
   multigraph->Add(gre,"lp");
   
   Double_t occ_r_pixdisk_6_fx1075[4] = {
   230.8701,
   232.6299,
   231.7065,
   232.6676};
   Double_t occ_r_pixdisk_6_fy1075[4] = {
   0.0005347842,
   0.0004944816,
   0.000640191,
   0.0005285838};
   Double_t occ_r_pixdisk_6_fex1075[4] = {
   0,
   0,
   0,
   0};
   Double_t occ_r_pixdisk_6_fey1075[4] = {
   2.093017e-07,
   2.687846e-07,
   2.907623e-07,
   2.131867e-07};
   gre = new TGraphErrors(4,occ_r_pixdisk_6_fx1075,occ_r_pixdisk_6_fy1075,occ_r_pixdisk_6_fex1075,occ_r_pixdisk_6_fey1075);
   gre->SetName("occ_r_pixdisk_6");
   gre->SetTitle("Disk 2");
   gre->SetFillColor(1);
   gre->SetLineColor(3);
   gre->SetLineStyle(2);
   gre->SetMarkerColor(3);
   gre->SetMarkerStyle(21);
   
   TH1F *Graph_occ_r_pixdisk_61075 = new TH1F("Graph_occ_r_pixdisk_61075","Disk 2",100,230.6904,232.8474);
   Graph_occ_r_pixdisk_61075->SetMinimum(0.000479586);
   Graph_occ_r_pixdisk_61075->SetMaximum(0.0006551086);
   Graph_occ_r_pixdisk_61075->SetDirectory(0);
   Graph_occ_r_pixdisk_61075->SetStats(0);
   gre->SetHistogram(Graph_occ_r_pixdisk_61075);
   
   multigraph->Add(gre,"lp");
   
   Double_t occ_r_pixdisk_7_fx1076[4] = {
   292.1764,
   297.4263,
   293.1649,
   294.1825};
   Double_t occ_r_pixdisk_7_fy1076[4] = {
   0.0004042659,
   0.0004563492,
   0.0003298611,
   0.0004104663};
   Double_t occ_r_pixdisk_7_fex1076[4] = {
   0,
   0,
   0,
   0};
   Double_t occ_r_pixdisk_7_fey1076[4] = {
   1.875461e-07,
   4.57888e-07,
   1.404782e-07,
   3.733944e-07};
   gre = new TGraphErrors(4,occ_r_pixdisk_7_fx1076,occ_r_pixdisk_7_fy1076,occ_r_pixdisk_7_fex1076,occ_r_pixdisk_7_fey1076);
   gre->SetName("occ_r_pixdisk_7");
   gre->SetTitle("Disk 3");
   gre->SetFillColor(1);
   gre->SetLineColor(4);
   gre->SetLineStyle(2);
   gre->SetMarkerColor(4);
   gre->SetMarkerStyle(21);
   
   TH1F *Graph_occ_r_pixdisk_71076 = new TH1F("Graph_occ_r_pixdisk_71076","Disk 3",100,291.6514,297.9513);
   Graph_occ_r_pixdisk_71076->SetMinimum(0.000317012);
   Graph_occ_r_pixdisk_71076->SetMaximum(0.0004695157);
   Graph_occ_r_pixdisk_71076->SetDirectory(0);
   Graph_occ_r_pixdisk_71076->SetStats(0);
   gre->SetHistogram(Graph_occ_r_pixdisk_71076);
   
   multigraph->Add(gre,"lp");
   multigraph->Draw("Apl");
   multigraph->GetXaxis()->SetTitle("r [mm]");
   multigraph->GetYaxis()->SetTitle("Occupancy in percent");
   
   TLegend *leg = new TLegend(0.57,0.62,0.77,0.87,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextFont(62);
   leg->SetTextSize(0.04);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(10);
   leg->SetFillStyle(0);
   TLegendEntry *entry=leg->AddEntry("NULL","Pixel Disk (left)","h");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("occ_r_pixdisk_0","Disk 0","lp");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("occ_r_pixdisk_1","Disk 1","lp");
   entry->SetLineColor(2);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(2);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("occ_r_pixdisk_2","Disk 2","lp");
   entry->SetLineColor(3);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(3);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("occ_r_pixdisk_3","Disk 3","lp");
   entry->SetLineColor(4);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(4);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   leg->Draw();
   
   leg = new TLegend(0.7,0.62,0.9,0.87,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextFont(62);
   leg->SetTextSize(0.04);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(10);
   leg->SetFillStyle(0);
   entry=leg->AddEntry("NULL","  (right)","h");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("occ_r_pixdisk_4","Disk 0","lp");
   entry->SetLineColor(1);
   entry->SetLineStyle(2);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("occ_r_pixdisk_5","Disk 1","lp");
   entry->SetLineColor(2);
   entry->SetLineStyle(2);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(2);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("occ_r_pixdisk_6","Disk 2","lp");
   entry->SetLineColor(3);
   entry->SetLineStyle(2);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(3);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("occ_r_pixdisk_7","Disk 3","lp");
   entry->SetLineColor(4);
   entry->SetLineStyle(2);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(4);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   leg->Draw();
   c1_n13->Modified();
   c1_n13->cd();
   c1_n13->SetSelected(c1_n13);
}
