void clus_z_pixlayer_2_pos()
{
//=========Macro generated from canvas: c1_n49/c1_n49
//=========  (Mon Apr 25 16:02:15 2016) by ROOT version6.04/12
   TCanvas *c1_n49 = new TCanvas("c1_n49", "c1_n49",0,0,700,500);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   c1_n49->SetHighLightColor(2);
   c1_n49->Range(-40.02605,20.9307,406.5616,108.6077);
   c1_n49->SetFillColor(0);
   c1_n49->SetBorderMode(0);
   c1_n49->SetBorderSize(2);
   c1_n49->SetTickx(1);
   c1_n49->SetTicky(1);
   c1_n49->SetFrameBorderMode(0);
   c1_n49->SetFrameBorderMode(0);
   
   TMultiGraph *multigraph = new TMultiGraph();
   multigraph->SetName("");
   multigraph->SetTitle("");
   
   Double_t clus_z_pixlayer_2_pos_fx1308[9] = {
   20.87226,
   61.74091,
   102.6117,
   141.5903,
   182.9152,
   223.3563,
   263.8003,
   304.406,
   345.6632};
   Double_t clus_z_pixlayer_2_pos_fy1308[9] = {
   76.8,
   59.2,
   65.2,
   56.8,
   51.6,
   55.6,
   38.8,
   40.4,
   36};
   Double_t clus_z_pixlayer_2_pos_fex1308[9] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t clus_z_pixlayer_2_pos_fey1308[9] = {
   5.542562,
   4.86621,
   5.106858,
   4.76655,
   4.543127,
   4.71593,
   3.939543,
   4.01995,
   3.794733};
   TGraphErrors *gre = new TGraphErrors(9,clus_z_pixlayer_2_pos_fx1308,clus_z_pixlayer_2_pos_fy1308,clus_z_pixlayer_2_pos_fex1308,clus_z_pixlayer_2_pos_fey1308);
   gre->SetName("clus_z_pixlayer_2_pos");
   gre->SetTitle("Layer 2 (z>0)");
   
   TH1F *Graph_clus_z_pixlayer_2_pos1308 = new TH1F("Graph_clus_z_pixlayer_2_pos1308","Layer 2 (z>0)",100,0,378.1423);
   Graph_clus_z_pixlayer_2_pos1308->SetMinimum(27.19154);
   Graph_clus_z_pixlayer_2_pos1308->SetMaximum(87.3563);
   Graph_clus_z_pixlayer_2_pos1308->SetDirectory(0);
   Graph_clus_z_pixlayer_2_pos1308->SetStats(0);
   gre->SetHistogram(Graph_clus_z_pixlayer_2_pos1308);
   
   multigraph->Add(gre,"");
   multigraph->Draw("Apl");
   
   TLegend *leg = new TLegend(0.48,0.65,0.9,0.9,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextFont(62);
   leg->SetTextSize(0.04);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(10);
   leg->SetFillStyle(0);
   TLegendEntry *entry=leg->AddEntry("clus_z_pixlayer_2_pos","Pixel Occupancies","lp");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(1);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   leg->Draw();
   c1_n49->Modified();
   c1_n49->cd();
   c1_n49->SetSelected(c1_n49);
}
