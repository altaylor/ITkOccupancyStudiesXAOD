void clus_pixlayer_mgz()
{
//=========Macro generated from canvas: c1_n4/c1_n4
//=========  (Mon Apr 25 16:02:02 2016) by ROOT version6.04/12
   TCanvas *c1_n4 = new TCanvas("c1_n4", "c1_n4",0,0,700,500);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   c1_n4->SetHighLightColor(2);
   c1_n4->Range(-963.2321,-17.31638,963.1965,316.2304);
   c1_n4->SetFillColor(0);
   c1_n4->SetBorderMode(0);
   c1_n4->SetBorderSize(2);
   c1_n4->SetTickx(1);
   c1_n4->SetTicky(1);
   c1_n4->SetFrameBorderMode(0);
   c1_n4->SetFrameBorderMode(0);
   
   TMultiGraph *multigraph = new TMultiGraph();
   multigraph->SetName("clus_pixlayer_mgz");
   multigraph->SetTitle("");
   
   Double_t clus_z_pixlayer_0_fx1016[30] = {
   -700.5373,
   -642.4635,
   -588.7615,
   -518.1097,
   -458.2943,
   -407.2276,
   -364.0303,
   -327.1149,
   -290.2938,
   -238.5248,
   -201.8977,
   -160.985,
   -121.9201,
   -80.0913,
   -40.61866,
   40.04303,
   79.4361,
   119.9563,
   162.0787,
   202.4932,
   238.2208,
   289.8448,
   327.1444,
   364.3445,
   407.7397,
   457.7918,
   517.9092,
   588.5283,
   642.8512,
   700.5016};
   Double_t clus_z_pixlayer_0_fy1016[30] = {
   106.4,
   108.8,
   113.6,
   97.2,
   97.2,
   93.6,
   102.4,
   106.4,
   94.8,
   102.4,
   100,
   88.4,
   129.2,
   168,
   189.2,
   196,
   145.6,
   120.4,
   83.2,
   109.2,
   109.6,
   108.8,
   107.2,
   110.4,
   120.4,
   112.4,
   105.2,
   89.60001,
   90.4,
   84.4};
   Double_t clus_z_pixlayer_0_fex1016[30] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t clus_z_pixlayer_0_fey1016[30] = {
   6.523803,
   6.596969,
   6.74092,
   6.235383,
   6.235383,
   6.118824,
   6.4,
   6.523803,
   6.157922,
   6.4,
   6.324555,
   5.946427,
   7.18888,
   8.197561,
   8.699426,
   8.854378,
   7.631514,
   6.939741,
   5.768882,
   6.609085,
   6.621178,
   6.596969,
   6.548283,
   6.645299,
   6.939741,
   6.705222,
   6.48691,
   5.986652,
   6.013319,
   5.810336};
   TGraphErrors *gre = new TGraphErrors(30,clus_z_pixlayer_0_fx1016,clus_z_pixlayer_0_fy1016,clus_z_pixlayer_0_fex1016,clus_z_pixlayer_0_fey1016);
   gre->SetName("clus_z_pixlayer_0");
   gre->SetTitle("Layer 0");
   gre->SetFillColor(1);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_clus_z_pixlayer_01016 = new TH1F("Graph_clus_z_pixlayer_01016","Layer 0",100,-840.6412,840.6055);
   Graph_clus_z_pixlayer_01016->SetMinimum(64.6888);
   Graph_clus_z_pixlayer_01016->SetMaximum(217.5967);
   Graph_clus_z_pixlayer_01016->SetDirectory(0);
   Graph_clus_z_pixlayer_01016->SetStats(0);
   gre->SetHistogram(Graph_clus_z_pixlayer_01016);
   
   multigraph->Add(gre,"lp");
   
   Double_t clus_z_pixlayer_1_fx1017[30] = {
   -650.2698,
   -603.9572,
   -555.6611,
   -477.8751,
   -412.8007,
   -358.9518,
   -313.312,
   -276.4003,
   -244.6967,
   -218.29,
   -181.4305,
   -141.7165,
   -102.7788,
   -60.39693,
   -20.12264,
   20.94774,
   60.53968,
   100.2868,
   141.1761,
   182.888,
   218.1884,
   244.9504,
   275.4924,
   313.6545,
   359.3023,
   414.127,
   477.5489,
   555.8186,
   603.5179,
   650.3217};
   Double_t clus_z_pixlayer_1_fy1017[30] = {
   56.4,
   57.2,
   56.8,
   52.8,
   52.8,
   54.4,
   60.4,
   61.6,
   59.6,
   50,
   57.2,
   81.2,
   94.4,
   100.4,
   115.6,
   110.4,
   102,
   99.6,
   86,
   64.8,
   54.8,
   51.2,
   56.4,
   58,
   52.4,
   60,
   63.2,
   66.39999,
   64.8,
   59.6};
   Double_t clus_z_pixlayer_1_fex1017[30] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t clus_z_pixlayer_1_fey1017[30] = {
   4.749737,
   4.783304,
   4.76655,
   4.59565,
   4.59565,
   4.664762,
   4.915282,
   4.96387,
   4.882622,
   4.472136,
   4.783304,
   5.699123,
   6.144917,
   6.337192,
   6.8,
   6.645299,
   6.387488,
   6.311893,
   5.865151,
   5.091169,
   4.68188,
   4.525484,
   4.749737,
   4.816638,
   4.578209,
   4.89898,
   5.027922,
   5.15364,
   5.091169,
   4.882622};
   gre = new TGraphErrors(30,clus_z_pixlayer_1_fx1017,clus_z_pixlayer_1_fy1017,clus_z_pixlayer_1_fex1017,clus_z_pixlayer_1_fey1017);
   gre->SetName("clus_z_pixlayer_1");
   gre->SetTitle("Layer 1");
   gre->SetFillColor(1);
   gre->SetLineColor(2);
   gre->SetMarkerColor(2);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_clus_z_pixlayer_11017 = new TH1F("Graph_clus_z_pixlayer_11017","Layer 1",100,-780.3289,780.3808);
   Graph_clus_z_pixlayer_11017->SetMinimum(37.84065);
   Graph_clus_z_pixlayer_11017->SetMaximum(130.0872);
   Graph_clus_z_pixlayer_11017->SetDirectory(0);
   Graph_clus_z_pixlayer_11017->SetStats(0);
   gre->SetHistogram(Graph_clus_z_pixlayer_11017);
   
   multigraph->Add(gre,"lp");
   
   Double_t clus_z_pixlayer_2_fx1018[18] = {
   -346.1053,
   -302.6237,
   -261.7174,
   -221.6377,
   -185.4234,
   -143.1822,
   -99.71312,
   -59.66911,
   -21.136,
   20.87226,
   61.74091,
   102.6117,
   141.5903,
   182.9152,
   223.3563,
   263.8003,
   304.406,
   345.6632};
   Double_t clus_z_pixlayer_2_fy1018[18] = {
   39.6,
   34.8,
   42.4,
   42,
   49.6,
   58.4,
   59.6,
   59.6,
   72.39999,
   76.8,
   59.2,
   65.2,
   56.8,
   51.6,
   55.6,
   38.8,
   40.4,
   36};
   Double_t clus_z_pixlayer_2_fex1018[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t clus_z_pixlayer_2_fey1018[18] = {
   3.97995,
   3.730952,
   4.118252,
   4.098781,
   4.454212,
   4.833219,
   4.882622,
   4.882622,
   5.38145,
   5.542562,
   4.86621,
   5.106858,
   4.76655,
   4.543127,
   4.71593,
   3.939543,
   4.01995,
   3.794733};
   gre = new TGraphErrors(18,clus_z_pixlayer_2_fx1018,clus_z_pixlayer_2_fy1018,clus_z_pixlayer_2_fex1018,clus_z_pixlayer_2_fey1018);
   gre->SetName("clus_z_pixlayer_2");
   gre->SetTitle("Layer 2");
   gre->SetFillColor(1);
   gre->SetLineColor(3);
   gre->SetMarkerColor(3);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_clus_z_pixlayer_21018 = new TH1F("Graph_clus_z_pixlayer_21018","Layer 2",100,-415.2822,414.8401);
   Graph_clus_z_pixlayer_21018->SetMinimum(25.9417);
   Graph_clus_z_pixlayer_21018->SetMaximum(87.46992);
   Graph_clus_z_pixlayer_21018->SetDirectory(0);
   Graph_clus_z_pixlayer_21018->SetStats(0);
   gre->SetHistogram(Graph_clus_z_pixlayer_21018);
   
   multigraph->Add(gre,"lp");
   
   Double_t clus_z_pixlayer_3_fx1019[18] = {
   -345.6833,
   -303.4428,
   -262.8259,
   -222.8707,
   -179.3497,
   -143.3847,
   -100.7758,
   -59.92731,
   -22.4765,
   20.51613,
   60.57324,
   100.4896,
   144.6533,
   182.4282,
   225.3959,
   265.3056,
   304.2318,
   345.2086};
   Double_t clus_z_pixlayer_3_fy1019[18] = {
   30.8,
   38.4,
   41.2,
   38.4,
   43.6,
   42,
   51.6,
   55.6,
   55.2,
   57.2,
   44,
   50.8,
   41.6,
   44,
   40.4,
   40.8,
   45.2,
   42.4};
   Double_t clus_z_pixlayer_3_fex1019[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t clus_z_pixlayer_3_fey1019[18] = {
   3.509986,
   3.919183,
   4.059556,
   3.919183,
   4.176123,
   4.098781,
   4.543127,
   4.71593,
   4.698936,
   4.783304,
   4.195235,
   4.507771,
   4.079216,
   4.195235,
   4.01995,
   4.039802,
   4.252058,
   4.118252};
   gre = new TGraphErrors(18,clus_z_pixlayer_3_fx1019,clus_z_pixlayer_3_fy1019,clus_z_pixlayer_3_fex1019,clus_z_pixlayer_3_fey1019);
   gre->SetName("clus_z_pixlayer_3");
   gre->SetTitle("Layer 3");
   gre->SetFillColor(1);
   gre->SetLineColor(4);
   gre->SetMarkerColor(4);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_clus_z_pixlayer_31019 = new TH1F("Graph_clus_z_pixlayer_31019","Layer 3",100,-414.7724,414.2978);
   Graph_clus_z_pixlayer_31019->SetMinimum(23.82068);
   Graph_clus_z_pixlayer_31019->SetMaximum(65.45264);
   Graph_clus_z_pixlayer_31019->SetDirectory(0);
   Graph_clus_z_pixlayer_31019->SetStats(0);
   gre->SetHistogram(Graph_clus_z_pixlayer_31019);
   
   multigraph->Add(gre,"lp");
   
   Double_t clus_z_pixlayer_4_fx1020[18] = {
   -346.0602,
   -305.2684,
   -264.4916,
   -222.4552,
   -182.0478,
   -139.4596,
   -100.7201,
   -59.73218,
   -20.87935,
   19.89506,
   61.55643,
   102.2831,
   140.4745,
   183.8383,
   224.7865,
   262.6696,
   304.5421,
   343.557};
   Double_t clus_z_pixlayer_4_fy1020[18] = {
   32.4,
   30.8,
   28.4,
   35.6,
   34.8,
   36,
   36,
   47.6,
   44.4,
   42.4,
   39.6,
   45.2,
   38.4,
   36.8,
   42.8,
   37.6,
   35.2,
   37.6};
   Double_t clus_z_pixlayer_4_fex1020[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t clus_z_pixlayer_4_fey1020[18] = {
   3.6,
   3.509986,
   3.37046,
   3.773592,
   3.730952,
   3.794733,
   3.794733,
   4.363485,
   4.214262,
   4.118252,
   3.97995,
   4.252058,
   3.919183,
   3.836665,
   4.137632,
   3.878144,
   3.752332,
   3.878144};
   gre = new TGraphErrors(18,clus_z_pixlayer_4_fx1020,clus_z_pixlayer_4_fy1020,clus_z_pixlayer_4_fex1020,clus_z_pixlayer_4_fey1020);
   gre->SetName("clus_z_pixlayer_4");
   gre->SetTitle("Layer 4");
   gre->SetFillColor(1);
   gre->SetLineColor(5);
   gre->SetMarkerColor(5);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_clus_z_pixlayer_41020 = new TH1F("Graph_clus_z_pixlayer_41020","Layer 4",100,-415.0219,412.5187);
   Graph_clus_z_pixlayer_41020->SetMinimum(22.33615);
   Graph_clus_z_pixlayer_41020->SetMaximum(54.65688);
   Graph_clus_z_pixlayer_41020->SetDirectory(0);
   Graph_clus_z_pixlayer_41020->SetStats(0);
   gre->SetHistogram(Graph_clus_z_pixlayer_41020);
   
   multigraph->Add(gre,"lp");
   multigraph->Draw("Apl");
   multigraph->GetXaxis()->SetTitle("z [mm]");
   multigraph->GetYaxis()->SetTitle("Cluster per etaModuleID");
   
   TLegend *leg = new TLegend(0.67,0.45,0.9,0.7,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextFont(62);
   leg->SetTextSize(0.04);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(10);
   leg->SetFillStyle(0);
   TLegendEntry *entry=leg->AddEntry("NULL","Pixel Barrel","h");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("clus_z_pixlayer_0","Layer 0","lp");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("clus_z_pixlayer_1","Layer 1","lp");
   entry->SetLineColor(2);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(2);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("clus_z_pixlayer_2","Layer 2","lp");
   entry->SetLineColor(3);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(3);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("clus_z_pixlayer_3","Layer 3","lp");
   entry->SetLineColor(4);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(4);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("clus_z_pixlayer_4","Layer 4","lp");
   entry->SetLineColor(5);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(5);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   leg->Draw();
   c1_n4->Modified();
   c1_n4->cd();
   c1_n4->SetSelected(c1_n4);
}
