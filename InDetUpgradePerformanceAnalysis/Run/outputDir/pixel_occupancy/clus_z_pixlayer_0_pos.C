void clus_z_pixlayer_0_pos()
{
//=========Macro generated from canvas: c1_n51/c1_n51
//=========  (Mon Apr 25 16:02:15 2016) by ROOT version6.04/12
   TCanvas *c1_n51 = new TCanvas("c1_n51", "c1_n51",0,0,700,500);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   c1_n51->SetHighLightColor(2);
   c1_n51->Range(-83.79297,48.09245,824.3376,277.7675);
   c1_n51->SetFillColor(0);
   c1_n51->SetBorderMode(0);
   c1_n51->SetBorderSize(2);
   c1_n51->SetTickx(1);
   c1_n51->SetTicky(1);
   c1_n51->SetFrameBorderMode(0);
   c1_n51->SetFrameBorderMode(0);
   
   TMultiGraph *multigraph = new TMultiGraph();
   multigraph->SetName("");
   multigraph->SetTitle("");
   
   Double_t clus_z_pixlayer_0_pos_fx1310[15] = {
   40.04303,
   79.4361,
   119.9563,
   162.0787,
   202.4932,
   238.2208,
   289.8448,
   327.1444,
   364.3445,
   407.7397,
   457.7918,
   517.9092,
   588.5283,
   642.8512,
   700.5016};
   Double_t clus_z_pixlayer_0_pos_fy1310[15] = {
   196,
   145.6,
   120.4,
   83.2,
   109.2,
   109.6,
   108.8,
   107.2,
   110.4,
   120.4,
   112.4,
   105.2,
   89.60001,
   90.4,
   84.4};
   Double_t clus_z_pixlayer_0_pos_fex1310[15] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t clus_z_pixlayer_0_pos_fey1310[15] = {
   8.854378,
   7.631514,
   6.939741,
   5.768882,
   6.609085,
   6.621178,
   6.596969,
   6.548283,
   6.645299,
   6.939741,
   6.705222,
   6.48691,
   5.986652,
   6.013319,
   5.810336};
   TGraphErrors *gre = new TGraphErrors(15,clus_z_pixlayer_0_pos_fx1310,clus_z_pixlayer_0_pos_fy1310,clus_z_pixlayer_0_pos_fex1310,clus_z_pixlayer_0_pos_fey1310);
   gre->SetName("clus_z_pixlayer_0_pos");
   gre->SetTitle("Layer 0 (z>0)");
   
   TH1F *Graph_clus_z_pixlayer_0_pos1310 = new TH1F("Graph_clus_z_pixlayer_0_pos1310","Layer 0 (z>0)",100,0,766.5475);
   Graph_clus_z_pixlayer_0_pos1310->SetMinimum(64.6888);
   Graph_clus_z_pixlayer_0_pos1310->SetMaximum(217.5967);
   Graph_clus_z_pixlayer_0_pos1310->SetDirectory(0);
   Graph_clus_z_pixlayer_0_pos1310->SetStats(0);
   gre->SetHistogram(Graph_clus_z_pixlayer_0_pos1310);
   
   multigraph->Add(gre,"");
   multigraph->Draw("Apl");
   
   TLegend *leg = new TLegend(0.48,0.65,0.9,0.9,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextFont(62);
   leg->SetTextSize(0.04);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(10);
   leg->SetFillStyle(0);
   TLegendEntry *entry=leg->AddEntry("clus_z_pixlayer_0_pos","Pixel Occupancies","lp");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(1);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   leg->Draw();
   c1_n51->Modified();
   c1_n51->cd();
   c1_n51->SetSelected(c1_n51);
}
