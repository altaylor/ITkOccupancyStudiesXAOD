void hit_pixlayer_mg()
{
//=========Macro generated from canvas: c1_n6/c1_n6
//=========  (Mon Apr 25 16:02:03 2016) by ROOT version6.04/12
   TCanvas *c1_n6 = new TCanvas("c1_n6", "c1_n6",0,0,700,500);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   c1_n6->SetHighLightColor(2);
   c1_n6->Range(-20.625,-161.0986,20.625,1909.626);
   c1_n6->SetFillColor(0);
   c1_n6->SetBorderMode(0);
   c1_n6->SetBorderSize(2);
   c1_n6->SetTickx(1);
   c1_n6->SetTicky(1);
   c1_n6->SetFrameBorderMode(0);
   c1_n6->SetFrameBorderMode(0);
   
   TMultiGraph *multigraph = new TMultiGraph();
   multigraph->SetName("hit_pixlayer_mg");
   multigraph->SetTitle("");
   
   Double_t hit_pixlayer_0_fx1031[30] = {
   -15,
   -14,
   -13,
   -12,
   -11,
   -10,
   -9,
   -8,
   -7,
   -6,
   -5,
   -4,
   -3,
   -2,
   -1,
   1,
   2,
   3,
   4,
   5,
   6,
   7,
   8,
   9,
   10,
   11,
   12,
   13,
   14,
   15};
   Double_t hit_pixlayer_0_fy1031[30] = {
   358,
   340,
   366,
   297.2,
   342.4,
   279.6,
   304.8,
   318,
   269.6,
   314.4,
   279.2,
   1011.6,
   1179.2,
   1060,
   876.8,
   992,
   1032.8,
   1120,
   1136.8,
   286,
   306.8,
   317.2,
   321.2,
   330,
   364,
   351.2,
   338.4,
   281.2,
   300.4,
   272.8};
   Double_t hit_pixlayer_0_fex1031[30] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t hit_pixlayer_0_fey1031[30] = {
   9.22281,
   6.961189,
   10.22409,
   5.90716,
   20.40917,
   8.717719,
   8.054248,
   10.0825,
   7.618443,
   19.05613,
   15.69378,
   38.42143,
   35.21315,
   34.6864,
   25.5008,
   30.48414,
   34.7322,
   38.44201,
   39.38538,
   9.228919,
   8.40544,
   13.29733,
   7.571878,
   10.17694,
   9.722551,
   11.06609,
   12.46009,
   7.224026,
   11.7797,
   8.252399};
   TGraphErrors *gre = new TGraphErrors(30,hit_pixlayer_0_fx1031,hit_pixlayer_0_fy1031,hit_pixlayer_0_fex1031,hit_pixlayer_0_fey1031);
   gre->SetName("hit_pixlayer_0");
   gre->SetTitle("Layer 0");
   gre->SetFillColor(1);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_hit_pixlayer_01031 = new TH1F("Graph_hit_pixlayer_01031","Layer 0",100,-18,18);
   Graph_hit_pixlayer_01031->SetMinimum(166.7384);
   Graph_hit_pixlayer_01031->SetMaximum(1309.656);
   Graph_hit_pixlayer_01031->SetDirectory(0);
   Graph_hit_pixlayer_01031->SetStats(0);
   gre->SetHistogram(Graph_hit_pixlayer_01031);
   
   multigraph->Add(gre,"lp");
   
   Double_t hit_pixlayer_1_fx1032[30] = {
   -15,
   -14,
   -13,
   -12,
   -11,
   -10,
   -9,
   -8,
   -7,
   -6,
   -5,
   -4,
   -3,
   -2,
   -1,
   1,
   2,
   3,
   4,
   5,
   6,
   7,
   8,
   9,
   10,
   11,
   12,
   13,
   14,
   15};
   Double_t hit_pixlayer_1_fy1032[30] = {
   156.8,
   166.8,
   167.6,
   147.6,
   139.6,
   229.6,
   169.2,
   165.6,
   145.6,
   110,
   407.2,
   554.8,
   427.6,
   363.2,
   366.8,
   432.8,
   404.4,
   502.8,
   519.6,
   548.4,
   128.4,
   131.2,
   121.6,
   138.8,
   156.4,
   159.2,
   168.8,
   218,
   206,
   199.6};
   Double_t hit_pixlayer_1_fex1032[30] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t hit_pixlayer_1_fey1032[30] = {
   5.016772,
   5.633902,
   6.253011,
   5.699337,
   5.390974,
   56.48933,
   15.7598,
   13.54391,
   11.74771,
   6.450807,
   14.08345,
   63.93014,
   17.91218,
   11.65274,
   12.44959,
   62.0444,
   15.34904,
   16.83108,
   14.2916,
   42.6303,
   6.459466,
   10.81638,
   4.407785,
   5.797126,
   9.83532,
   5.618378,
   4.66039,
   25.46245,
   14.82858,
   11.07791};
   gre = new TGraphErrors(30,hit_pixlayer_1_fx1032,hit_pixlayer_1_fy1032,hit_pixlayer_1_fex1032,hit_pixlayer_1_fey1032);
   gre->SetName("hit_pixlayer_1");
   gre->SetTitle("Layer 1");
   gre->SetFillColor(1);
   gre->SetLineColor(2);
   gre->SetMarkerColor(2);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_hit_pixlayer_11032 = new TH1F("Graph_hit_pixlayer_11032","Layer 1",100,-18,18);
   Graph_hit_pixlayer_11032->SetMinimum(52.0311);
   Graph_hit_pixlayer_11032->SetMaximum(670.2482);
   Graph_hit_pixlayer_11032->SetDirectory(0);
   Graph_hit_pixlayer_11032->SetStats(0);
   gre->SetHistogram(Graph_hit_pixlayer_11032);
   
   multigraph->Add(gre,"lp");
   
   Double_t hit_pixlayer_2_fx1033[18] = {
   -9,
   -8,
   -7,
   -6,
   -5,
   -4,
   -3,
   -2,
   -1,
   1,
   2,
   3,
   4,
   5,
   6,
   7,
   8,
   9};
   Double_t hit_pixlayer_2_fy1033[18] = {
   282.8,
   266.8,
   298.8,
   224,
   258,
   231.2,
   213.2,
   170.8,
   186,
   204.8,
   187.6,
   250,
   241.2,
   272.4,
   312,
   274.8,
   303.2,
   281.6};
   Double_t hit_pixlayer_2_fex1033[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t hit_pixlayer_2_fey1033[18] = {
   13.24901,
   12.59029,
   9.997257,
   9.28191,
   10.23909,
   7.862824,
   8.667304,
   7.979839,
   7.246517,
   9.062806,
   8.445487,
   14.51402,
   9.348538,
   15.26958,
   10.11631,
   11.9772,
   13.26734,
   14.22794};
   gre = new TGraphErrors(18,hit_pixlayer_2_fx1033,hit_pixlayer_2_fy1033,hit_pixlayer_2_fex1033,hit_pixlayer_2_fey1033);
   gre->SetName("hit_pixlayer_2");
   gre->SetTitle("Layer 2");
   gre->SetFillColor(1);
   gre->SetLineColor(3);
   gre->SetMarkerColor(3);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_hit_pixlayer_21033 = new TH1F("Graph_hit_pixlayer_21033","Layer 2",100,-10.8,10.8);
   Graph_hit_pixlayer_21033->SetMinimum(146.8905);
   Graph_hit_pixlayer_21033->SetMaximum(338.0459);
   Graph_hit_pixlayer_21033->SetDirectory(0);
   Graph_hit_pixlayer_21033->SetStats(0);
   gre->SetHistogram(Graph_hit_pixlayer_21033);
   
   multigraph->Add(gre,"lp");
   
   Double_t hit_pixlayer_3_fx1034[18] = {
   -9,
   -8,
   -7,
   -6,
   -5,
   -4,
   -3,
   -2,
   -1,
   1,
   2,
   3,
   4,
   5,
   6,
   7,
   8,
   9};
   Double_t hit_pixlayer_3_fy1034[18] = {
   173.6,
   232.4,
   230.4,
   191.6,
   172.4,
   150.8,
   169.2,
   148.4,
   138,
   143.6,
   120,
   170.8,
   151.2,
   198.8,
   214.4,
   206,
   255.2,
   258.4};
   Double_t hit_pixlayer_3_fex1034[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t hit_pixlayer_3_fey1034[18] = {
   7.887899,
   14.00289,
   20.86641,
   10.66125,
   6.612446,
   9.01759,
   14.67728,
   8.838913,
   6.121399,
   6.926251,
   5.792751,
   10.34294,
   6.123645,
   10.25624,
   14.55802,
   6.711361,
   9.938238,
   12.51102};
   gre = new TGraphErrors(18,hit_pixlayer_3_fx1034,hit_pixlayer_3_fy1034,hit_pixlayer_3_fex1034,hit_pixlayer_3_fey1034);
   gre->SetName("hit_pixlayer_3");
   gre->SetTitle("Layer 3");
   gre->SetFillColor(1);
   gre->SetLineColor(4);
   gre->SetMarkerColor(4);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_hit_pixlayer_31034 = new TH1F("Graph_hit_pixlayer_31034","Layer 3",100,-10.8,10.8);
   Graph_hit_pixlayer_31034->SetMinimum(98.53688);
   Graph_hit_pixlayer_31034->SetMaximum(286.5814);
   Graph_hit_pixlayer_31034->SetDirectory(0);
   Graph_hit_pixlayer_31034->SetStats(0);
   gre->SetHistogram(Graph_hit_pixlayer_31034);
   
   multigraph->Add(gre,"lp");
   
   Double_t hit_pixlayer_4_fx1035[18] = {
   -9,
   -8,
   -7,
   -6,
   -5,
   -4,
   -3,
   -2,
   -1,
   1,
   2,
   3,
   4,
   5,
   6,
   7,
   8,
   9};
   Double_t hit_pixlayer_4_fy1035[18] = {
   166.4,
   150.4,
   120.4,
   132.4,
   122.8,
   118,
   117.2,
   157.2,
   140.8,
   119.6,
   110,
   152.4,
   134.8,
   172.8,
   184,
   192.4,
   193.6,
   232};
   Double_t hit_pixlayer_4_fex1035[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t hit_pixlayer_4_fey1035[18] = {
   7.171889,
   6.565941,
   5.507812,
   5.151169,
   6.228517,
   4.927337,
   6.863157,
   25.74935,
   23.42872,
   11.44953,
   8.386189,
   9.646021,
   8.299145,
   18.71588,
   8.604694,
   13.77728,
   12.59374,
   28.91887};
   gre = new TGraphErrors(18,hit_pixlayer_4_fx1035,hit_pixlayer_4_fy1035,hit_pixlayer_4_fex1035,hit_pixlayer_4_fey1035);
   gre->SetName("hit_pixlayer_4");
   gre->SetTitle("Layer 4");
   gre->SetFillColor(1);
   gre->SetLineColor(5);
   gre->SetMarkerColor(5);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_hit_pixlayer_41035 = new TH1F("Graph_hit_pixlayer_41035","Layer 4",100,-10.8,10.8);
   Graph_hit_pixlayer_41035->SetMinimum(85.68331);
   Graph_hit_pixlayer_41035->SetMaximum(276.8494);
   Graph_hit_pixlayer_41035->SetDirectory(0);
   Graph_hit_pixlayer_41035->SetStats(0);
   gre->SetHistogram(Graph_hit_pixlayer_41035);
   
   multigraph->Add(gre,"lp");
   multigraph->Draw("Apl");
   multigraph->GetXaxis()->SetTitle("moduleID");
   multigraph->GetYaxis()->SetTitle("Hits per etaModuleID");
   
   TLegend *leg = new TLegend(0.67,0.45,0.9,0.7,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextFont(62);
   leg->SetTextSize(0.04);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(10);
   leg->SetFillStyle(0);
   TLegendEntry *entry=leg->AddEntry("NULL","Pixel Barrel","h");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("hit_pixlayer_0","Layer 0","lp");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("hit_pixlayer_1","Layer 1","lp");
   entry->SetLineColor(2);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(2);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("hit_pixlayer_2","Layer 2","lp");
   entry->SetLineColor(3);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(3);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("hit_pixlayer_3","Layer 3","lp");
   entry->SetLineColor(4);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(4);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("hit_pixlayer_4","Layer 4","lp");
   entry->SetLineColor(5);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(5);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   leg->Draw();
   c1_n6->Modified();
   c1_n6->cd();
   c1_n6->SetSelected(c1_n6);
}
