void size_pixdisk_mgr()
{
//=========Macro generated from canvas: c1_n20/c1_n20
//=========  (Mon Apr 25 16:02:08 2016) by ROOT version6.04/12
   TCanvas *c1_n20 = new TCanvas("c1_n20", "c1_n20",0,0,700,500);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   c1_n20->SetHighLightColor(2);
   c1_n20->Range(59.56247,1.254145,334.9837,5.56236);
   c1_n20->SetFillColor(0);
   c1_n20->SetBorderMode(0);
   c1_n20->SetBorderSize(2);
   c1_n20->SetTickx(1);
   c1_n20->SetTicky(1);
   c1_n20->SetFrameBorderMode(0);
   c1_n20->SetFrameBorderMode(0);
   
   TMultiGraph *multigraph = new TMultiGraph();
   multigraph->SetName("size_pixdisk_mgr");
   multigraph->SetTitle("");
   
   Double_t size_r_pixdisk_0_fx1125[4] = {
   97.11992,
   98.16271,
   98.49258,
   98.6056};
   Double_t size_r_pixdisk_0_fy1125[4] = {
   2.10101,
   1.992367,
   2.181373,
   1.844262};
   Double_t size_r_pixdisk_0_fex1125[4] = {
   0,
   0,
   0,
   0};
   Double_t size_r_pixdisk_0_fey1125[4] = {
   0.1798926,
   0.08725805,
   0.2114504,
   0.05817566};
   TGraphErrors *gre = new TGraphErrors(4,size_r_pixdisk_0_fx1125,size_r_pixdisk_0_fy1125,size_r_pixdisk_0_fex1125,size_r_pixdisk_0_fey1125);
   gre->SetName("size_r_pixdisk_0");
   gre->SetTitle("Disk 0");
   gre->SetFillColor(1);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_size_r_pixdisk_01125 = new TH1F("Graph_size_r_pixdisk_01125","Disk 0",100,96.97135,98.75417);
   Graph_size_r_pixdisk_01125->SetMinimum(1.725413);
   Graph_size_r_pixdisk_01125->SetMaximum(2.453497);
   Graph_size_r_pixdisk_01125->SetDirectory(0);
   Graph_size_r_pixdisk_01125->SetStats(0);
   gre->SetHistogram(Graph_size_r_pixdisk_01125);
   
   multigraph->Add(gre,"lp");
   
   Double_t size_r_pixdisk_1_fx1126[4] = {
   168.2737,
   168.5629,
   167.8105,
   169.1694};
   Double_t size_r_pixdisk_1_fy1126[4] = {
   2.221675,
   2.427184,
   2.666667,
   2.125561};
   Double_t size_r_pixdisk_1_fex1126[4] = {
   0,
   0,
   0,
   0};
   Double_t size_r_pixdisk_1_fey1126[4] = {
   0.1008456,
   0.2027754,
   0.1979057,
   0.1241042};
   gre = new TGraphErrors(4,size_r_pixdisk_1_fx1126,size_r_pixdisk_1_fy1126,size_r_pixdisk_1_fex1126,size_r_pixdisk_1_fey1126);
   gre->SetName("size_r_pixdisk_1");
   gre->SetTitle("Disk 1");
   gre->SetFillColor(1);
   gre->SetLineColor(2);
   gre->SetMarkerColor(2);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_size_r_pixdisk_11126 = new TH1F("Graph_size_r_pixdisk_11126","Disk 1",100,167.6746,169.3053);
   Graph_size_r_pixdisk_11126->SetMinimum(1.915145);
   Graph_size_r_pixdisk_11126->SetMaximum(2.950884);
   Graph_size_r_pixdisk_11126->SetDirectory(0);
   Graph_size_r_pixdisk_11126->SetStats(0);
   gre->SetHistogram(Graph_size_r_pixdisk_11126);
   
   multigraph->Add(gre,"lp");
   
   Double_t size_r_pixdisk_2_fx1127[4] = {
   231.2599,
   231.0032,
   231.0164,
   232.6809};
   Double_t size_r_pixdisk_2_fy1127[4] = {
   2.682432,
   3.280822,
   2.981366,
   2.884849};
   Double_t size_r_pixdisk_2_fex1127[4] = {
   0,
   0,
   0,
   0};
   Double_t size_r_pixdisk_2_fey1127[4] = {
   0.1644094,
   0.2753867,
   0.5201284,
   0.2281722};
   gre = new TGraphErrors(4,size_r_pixdisk_2_fx1127,size_r_pixdisk_2_fy1127,size_r_pixdisk_2_fex1127,size_r_pixdisk_2_fey1127);
   gre->SetName("size_r_pixdisk_2");
   gre->SetTitle("Disk 2");
   gre->SetFillColor(1);
   gre->SetLineColor(3);
   gre->SetMarkerColor(3);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_size_r_pixdisk_21127 = new TH1F("Graph_size_r_pixdisk_21127","Disk 2",100,230.8355,232.8487);
   Graph_size_r_pixdisk_21127->SetMinimum(2.351741);
   Graph_size_r_pixdisk_21127->SetMaximum(3.665706);
   Graph_size_r_pixdisk_21127->SetDirectory(0);
   Graph_size_r_pixdisk_21127->SetStats(0);
   gre->SetHistogram(Graph_size_r_pixdisk_21127);
   
   multigraph->Add(gre,"lp");
   
   Double_t size_r_pixdisk_3_fx1128[4] = {
   295.8305,
   294.3915,
   294.292,
   293.0196};
   Double_t size_r_pixdisk_3_fy1128[4] = {
   3.031579,
   3.206349,
   2.821429,
   2.944444};
   Double_t size_r_pixdisk_3_fex1128[4] = {
   0,
   0,
   0,
   0};
   Double_t size_r_pixdisk_3_fey1128[4] = {
   0.2086874,
   0.268876,
   0.1949385,
   0.2079392};
   gre = new TGraphErrors(4,size_r_pixdisk_3_fx1128,size_r_pixdisk_3_fy1128,size_r_pixdisk_3_fex1128,size_r_pixdisk_3_fey1128);
   gre->SetName("size_r_pixdisk_3");
   gre->SetTitle("Disk 3");
   gre->SetFillColor(1);
   gre->SetLineColor(4);
   gre->SetMarkerColor(4);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_size_r_pixdisk_31128 = new TH1F("Graph_size_r_pixdisk_31128","Disk 3",100,292.7385,296.1116);
   Graph_size_r_pixdisk_31128->SetMinimum(2.541617);
   Graph_size_r_pixdisk_31128->SetMaximum(3.560099);
   Graph_size_r_pixdisk_31128->SetDirectory(0);
   Graph_size_r_pixdisk_31128->SetStats(0);
   gre->SetHistogram(Graph_size_r_pixdisk_31128);
   
   multigraph->Add(gre,"lp");
   
   Double_t size_r_pixdisk_4_fx1129[4] = {
   97.21445,
   97.88796,
   99.10734,
   98.11107};
   Double_t size_r_pixdisk_4_fy1129[4] = {
   1.879733,
   1.861771,
   1.984615,
   1.995614};
   Double_t size_r_pixdisk_4_fex1129[4] = {
   0,
   0,
   0,
   0};
   Double_t size_r_pixdisk_4_fey1129[4] = {
   0.06731587,
   0.06344958,
   0.06834485,
   0.09272744};
   gre = new TGraphErrors(4,size_r_pixdisk_4_fx1129,size_r_pixdisk_4_fy1129,size_r_pixdisk_4_fex1129,size_r_pixdisk_4_fey1129);
   gre->SetName("size_r_pixdisk_4");
   gre->SetTitle("Disk 0");
   gre->SetFillColor(1);
   gre->SetLineStyle(2);
   gre->SetMarkerStyle(21);
   
   TH1F *Graph_size_r_pixdisk_41129 = new TH1F("Graph_size_r_pixdisk_41129","Disk 0",100,97.02516,99.29663);
   Graph_size_r_pixdisk_41129->SetMinimum(1.769319);
   Graph_size_r_pixdisk_41129->SetMaximum(2.117343);
   Graph_size_r_pixdisk_41129->SetDirectory(0);
   Graph_size_r_pixdisk_41129->SetStats(0);
   gre->SetHistogram(Graph_size_r_pixdisk_41129);
   
   multigraph->Add(gre,"lp");
   
   Double_t size_r_pixdisk_5_fx1130[4] = {
   168.1227,
   170.2475,
   168.4129,
   169.3295};
   Double_t size_r_pixdisk_5_fy1130[4] = {
   2.421052,
   2.487437,
   2.393939,
   2.266332};
   Double_t size_r_pixdisk_5_fex1130[4] = {
   0,
   0,
   0,
   0};
   Double_t size_r_pixdisk_5_fey1130[4] = {
   0.1129851,
   0.2053618,
   0.1978758,
   0.1230796};
   gre = new TGraphErrors(4,size_r_pixdisk_5_fx1130,size_r_pixdisk_5_fy1130,size_r_pixdisk_5_fex1130,size_r_pixdisk_5_fey1130);
   gre->SetName("size_r_pixdisk_5");
   gre->SetTitle("Disk 1");
   gre->SetFillColor(1);
   gre->SetLineColor(2);
   gre->SetLineStyle(2);
   gre->SetMarkerColor(2);
   gre->SetMarkerStyle(21);
   
   TH1F *Graph_size_r_pixdisk_51130 = new TH1F("Graph_size_r_pixdisk_51130","Disk 1",100,167.9103,170.46);
   Graph_size_r_pixdisk_51130->SetMinimum(2.088298);
   Graph_size_r_pixdisk_51130->SetMaximum(2.747754);
   Graph_size_r_pixdisk_51130->SetDirectory(0);
   Graph_size_r_pixdisk_51130->SetStats(0);
   gre->SetHistogram(Graph_size_r_pixdisk_51130);
   
   multigraph->Add(gre,"lp");
   
   Double_t size_r_pixdisk_6_fx1131[4] = {
   230.8701,
   232.6299,
   231.7065,
   232.6676};
   Double_t size_r_pixdisk_6_fy1131[4] = {
   2.593985,
   2.453846,
   2.848276,
   2.368056};
   Double_t size_r_pixdisk_6_fex1131[4] = {
   0,
   0,
   0,
   0};
   Double_t size_r_pixdisk_6_fey1131[4] = {
   0.1350247,
   0.1733983,
   0.1875766,
   0.137531};
   gre = new TGraphErrors(4,size_r_pixdisk_6_fx1131,size_r_pixdisk_6_fy1131,size_r_pixdisk_6_fex1131,size_r_pixdisk_6_fey1131);
   gre->SetName("size_r_pixdisk_6");
   gre->SetTitle("Disk 2");
   gre->SetFillColor(1);
   gre->SetLineColor(3);
   gre->SetLineStyle(2);
   gre->SetMarkerColor(3);
   gre->SetMarkerStyle(21);
   
   TH1F *Graph_size_r_pixdisk_61131 = new TH1F("Graph_size_r_pixdisk_61131","Disk 2",100,230.6904,232.8474);
   Graph_size_r_pixdisk_61131->SetMinimum(2.149992);
   Graph_size_r_pixdisk_61131->SetMaximum(3.116385);
   Graph_size_r_pixdisk_61131->SetDirectory(0);
   Graph_size_r_pixdisk_61131->SetStats(0);
   gre->SetHistogram(Graph_size_r_pixdisk_61131);
   
   multigraph->Add(gre,"lp");
   
   Double_t size_r_pixdisk_7_fx1132[4] = {
   292.1764,
   297.4263,
   293.1649,
   294.1825};
   Double_t size_r_pixdisk_7_fy1132[4] = {
   2.834783,
   3.439252,
   2.533333,
   2.903509};
   Double_t size_r_pixdisk_7_fex1132[4] = {
   0,
   0,
   0,
   0};
   Double_t size_r_pixdisk_7_fey1132[4] = {
   0.1512372,
   0.3692409,
   0.1132816,
   0.3011053};
   gre = new TGraphErrors(4,size_r_pixdisk_7_fx1132,size_r_pixdisk_7_fy1132,size_r_pixdisk_7_fex1132,size_r_pixdisk_7_fey1132);
   gre->SetName("size_r_pixdisk_7");
   gre->SetTitle("Disk 3");
   gre->SetFillColor(1);
   gre->SetLineColor(4);
   gre->SetLineStyle(2);
   gre->SetMarkerColor(4);
   gre->SetMarkerStyle(21);
   
   TH1F *Graph_size_r_pixdisk_71132 = new TH1F("Graph_size_r_pixdisk_71132","Disk 3",100,291.6514,297.9513);
   Graph_size_r_pixdisk_71132->SetMinimum(2.281208);
   Graph_size_r_pixdisk_71132->SetMaximum(3.947337);
   Graph_size_r_pixdisk_71132->SetDirectory(0);
   Graph_size_r_pixdisk_71132->SetStats(0);
   gre->SetHistogram(Graph_size_r_pixdisk_71132);
   
   multigraph->Add(gre,"lp");
   multigraph->Draw("Apl");
   multigraph->GetXaxis()->SetTitle("r [mm]");
   multigraph->GetYaxis()->SetTitle("Cluster size");
   
   TLegend *leg = new TLegend(0.14,0.62,0.34,0.87,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextFont(62);
   leg->SetTextSize(0.04);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(10);
   leg->SetFillStyle(0);
   TLegendEntry *entry=leg->AddEntry("NULL","Pixel Disk (left)","h");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("size_r_pixdisk_0","Disk 0","lp");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("size_r_pixdisk_1","Disk 1","lp");
   entry->SetLineColor(2);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(2);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("size_r_pixdisk_2","Disk 2","lp");
   entry->SetLineColor(3);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(3);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("size_r_pixdisk_3","Disk 3","lp");
   entry->SetLineColor(4);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(4);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   leg->Draw();
   
   leg = new TLegend(0.27,0.62,0.47,0.87,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextFont(62);
   leg->SetTextSize(0.04);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(10);
   leg->SetFillStyle(0);
   entry=leg->AddEntry("NULL","  (right)","h");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("size_r_pixdisk_4","Disk 0","lp");
   entry->SetLineColor(1);
   entry->SetLineStyle(2);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("size_r_pixdisk_5","Disk 1","lp");
   entry->SetLineColor(2);
   entry->SetLineStyle(2);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(2);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("size_r_pixdisk_6","Disk 2","lp");
   entry->SetLineColor(3);
   entry->SetLineStyle(2);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(3);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("size_r_pixdisk_7","Disk 3","lp");
   entry->SetLineColor(4);
   entry->SetLineStyle(2);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(4);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   leg->Draw();
   c1_n20->Modified();
   c1_n20->cd();
   c1_n20->SetSelected(c1_n20);
}
