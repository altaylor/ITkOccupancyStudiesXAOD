void hit_sctdisk_mg()
{
//=========Macro generated from canvas: c1_n35/c1_n35
//=========  (Mon Apr 25 16:02:11 2016) by ROOT version6.04/12
   TCanvas *c1_n35 = new TCanvas("c1_n35", "c1_n35",0,0,700,500);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   c1_n35->SetHighLightColor(2);
   c1_n35->Range(-3.1875,-26.26488,20.1875,310.7357);
   c1_n35->SetFillColor(0);
   c1_n35->SetBorderMode(0);
   c1_n35->SetBorderSize(2);
   c1_n35->SetTickx(1);
   c1_n35->SetTicky(1);
   c1_n35->SetFrameBorderMode(0);
   c1_n35->SetFrameBorderMode(0);
   
   TMultiGraph *multigraph = new TMultiGraph();
   multigraph->SetName("hit_sctdisk_mg");
   multigraph->SetTitle("");
   
   Double_t hit_sctdisk_0_fx1221[18] = {
   0,
   1,
   2,
   3,
   4,
   5,
   6,
   7,
   8,
   9,
   10,
   11,
   12,
   13,
   14,
   15,
   16,
   17};
   Double_t hit_sctdisk_0_fy1221[18] = {
   44,
   52,
   62,
   71.6,
   30.4,
   52.4,
   34.8,
   28,
   58.8,
   48.4,
   54.8,
   52,
   26.8,
   51.6,
   64,
   76,
   41.6,
   51.2};
   Double_t hit_sctdisk_0_fex1221[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t hit_sctdisk_0_fey1221[18] = {
   3.336935,
   2.526135,
   2.556743,
   3.651602,
   3.426507,
   3.997831,
   2.630456,
   1.807016,
   3.646002,
   2.902291,
   3.632334,
   6.839523,
   1.58535,
   4.410232,
   3.315102,
   19.5047,
   10.37644,
   3.367068};
   TGraphErrors *gre = new TGraphErrors(18,hit_sctdisk_0_fx1221,hit_sctdisk_0_fy1221,hit_sctdisk_0_fex1221,hit_sctdisk_0_fey1221);
   gre->SetName("hit_sctdisk_0");
   gre->SetTitle("Disk 0");
   gre->SetFillColor(1);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_hit_sctdisk_01221 = new TH1F("Graph_hit_sctdisk_01221","Disk 0",100,0,18.7);
   Graph_hit_sctdisk_01221->SetMinimum(18.18565);
   Graph_hit_sctdisk_01221->SetMaximum(102.5337);
   Graph_hit_sctdisk_01221->SetDirectory(0);
   Graph_hit_sctdisk_01221->SetStats(0);
   gre->SetHistogram(Graph_hit_sctdisk_01221);
   
   multigraph->Add(gre,"lp");
   
   Double_t hit_sctdisk_1_fx1222[18] = {
   0,
   1,
   2,
   3,
   4,
   5,
   6,
   7,
   8,
   9,
   10,
   11,
   12,
   13,
   14,
   15,
   16,
   17};
   Double_t hit_sctdisk_1_fy1222[18] = {
   42.4,
   42.8,
   48.8,
   62.8,
   22.4,
   52.4,
   48,
   28.8,
   62.4,
   58,
   51.2,
   36.4,
   28.8,
   44.4,
   66.8,
   55.6,
   33.6,
   54.8};
   Double_t hit_sctdisk_1_fex1222[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t hit_sctdisk_1_fey1222[18] = {
   2.071607,
   2.357965,
   2.008823,
   4.764783,
   1.390763,
   2.467089,
   2.794996,
   2.433869,
   6.927416,
   3.688815,
   3.111418,
   1.783075,
   2.733369,
   2.423954,
   6.79063,
   2.511337,
   3.078961,
   5.009397};
   gre = new TGraphErrors(18,hit_sctdisk_1_fx1222,hit_sctdisk_1_fy1222,hit_sctdisk_1_fex1222,hit_sctdisk_1_fey1222);
   gre->SetName("hit_sctdisk_1");
   gre->SetTitle("Disk 1");
   gre->SetFillColor(1);
   gre->SetLineColor(2);
   gre->SetMarkerColor(2);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_hit_sctdisk_11222 = new TH1F("Graph_hit_sctdisk_11222","Disk 1",100,0,18.7);
   Graph_hit_sctdisk_11222->SetMinimum(15.7511);
   Graph_hit_sctdisk_11222->SetMaximum(78.84876);
   Graph_hit_sctdisk_11222->SetDirectory(0);
   Graph_hit_sctdisk_11222->SetStats(0);
   gre->SetHistogram(Graph_hit_sctdisk_11222);
   
   multigraph->Add(gre,"lp");
   
   Double_t hit_sctdisk_2_fx1223[18] = {
   0,
   1,
   2,
   3,
   4,
   5,
   6,
   7,
   8,
   9,
   10,
   11,
   12,
   13,
   14,
   15,
   16,
   17};
   Double_t hit_sctdisk_2_fy1223[18] = {
   56,
   63.6,
   69.6,
   65.2,
   35.6,
   54,
   45.2,
   25.6,
   63.2,
   46.8,
   38.4,
   28,
   36.4,
   57.2,
   61.6,
   76.8,
   58.8,
   44.8};
   Double_t hit_sctdisk_2_fex1223[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t hit_sctdisk_2_fey1223[18] = {
   3.211859,
   4.057643,
   3.228121,
   2.860667,
   3.879229,
   4.012663,
   2.69904,
   3.042464,
   3.958715,
   3.054847,
   2.604215,
   2.8,
   2.772243,
   3.235789,
   3.65044,
   5.278777,
   6.324049,
   2.085958};
   gre = new TGraphErrors(18,hit_sctdisk_2_fx1223,hit_sctdisk_2_fy1223,hit_sctdisk_2_fex1223,hit_sctdisk_2_fey1223);
   gre->SetName("hit_sctdisk_2");
   gre->SetTitle("Disk 2");
   gre->SetFillColor(1);
   gre->SetLineColor(3);
   gre->SetMarkerColor(3);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_hit_sctdisk_21223 = new TH1F("Graph_hit_sctdisk_21223","Disk 2",100,0,18.7);
   Graph_hit_sctdisk_21223->SetMinimum(16.60541);
   Graph_hit_sctdisk_21223->SetMaximum(88.0309);
   Graph_hit_sctdisk_21223->SetDirectory(0);
   Graph_hit_sctdisk_21223->SetStats(0);
   gre->SetHistogram(Graph_hit_sctdisk_21223);
   
   multigraph->Add(gre,"lp");
   
   Double_t hit_sctdisk_3_fx1224[18] = {
   0,
   1,
   2,
   3,
   4,
   5,
   6,
   7,
   8,
   9,
   10,
   11,
   12,
   13,
   14,
   15,
   16,
   17};
   Double_t hit_sctdisk_3_fy1224[18] = {
   49.2,
   53.2,
   82.4,
   76.8,
   42.8,
   54,
   49.2,
   25.2,
   52,
   66,
   48.4,
   32.8,
   37.6,
   30.8,
   54,
   60,
   63.2,
   69.2};
   Double_t hit_sctdisk_3_fex1224[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t hit_sctdisk_3_fey1224[18] = {
   13.22247,
   2.234765,
   2.885508,
   3.221206,
   3.327923,
   2.433725,
   2.16669,
   2.602024,
   4.119656,
   2.957782,
   3.827645,
   2.279459,
   1.870065,
   1.498444,
   2.702245,
   4.153385,
   13.16274,
   8.774422};
   gre = new TGraphErrors(18,hit_sctdisk_3_fx1224,hit_sctdisk_3_fy1224,hit_sctdisk_3_fex1224,hit_sctdisk_3_fey1224);
   gre->SetName("hit_sctdisk_3");
   gre->SetTitle("Disk 3");
   gre->SetFillColor(1);
   gre->SetLineColor(4);
   gre->SetMarkerColor(4);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_hit_sctdisk_31224 = new TH1F("Graph_hit_sctdisk_31224","Disk 3",100,0,18.7);
   Graph_hit_sctdisk_31224->SetMinimum(16.32922);
   Graph_hit_sctdisk_31224->SetMaximum(91.55426);
   Graph_hit_sctdisk_31224->SetDirectory(0);
   Graph_hit_sctdisk_31224->SetStats(0);
   gre->SetHistogram(Graph_hit_sctdisk_31224);
   
   multigraph->Add(gre,"lp");
   
   Double_t hit_sctdisk_4_fx1225[18] = {
   0,
   1,
   2,
   3,
   4,
   5,
   6,
   7,
   8,
   9,
   10,
   11,
   12,
   13,
   14,
   15,
   16,
   17};
   Double_t hit_sctdisk_4_fy1225[18] = {
   46,
   52.8,
   65.2,
   66,
   39.6,
   50,
   50.8,
   26,
   62.8,
   53.6,
   35.6,
   27.2,
   40.8,
   43.2,
   50.8,
   57.6,
   38,
   60.4};
   Double_t hit_sctdisk_4_fex1225[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t hit_sctdisk_4_fey1225[18] = {
   2.386982,
   2.33042,
   2.640018,
   2.838133,
   2.434906,
   1.685795,
   3.387904,
   2.328567,
   3.64493,
   3.313818,
   2.200922,
   1.802056,
   2.591451,
   1.648087,
   2.32,
   2.66567,
   1.6,
   4.472805};
   gre = new TGraphErrors(18,hit_sctdisk_4_fx1225,hit_sctdisk_4_fy1225,hit_sctdisk_4_fex1225,hit_sctdisk_4_fey1225);
   gre->SetName("hit_sctdisk_4");
   gre->SetTitle("Disk 4");
   gre->SetFillColor(1);
   gre->SetLineColor(5);
   gre->SetMarkerColor(5);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_hit_sctdisk_41225 = new TH1F("Graph_hit_sctdisk_41225","Disk 4",100,0,18.7);
   Graph_hit_sctdisk_41225->SetMinimum(19.15476);
   Graph_hit_sctdisk_41225->SetMaximum(73.3548);
   Graph_hit_sctdisk_41225->SetDirectory(0);
   Graph_hit_sctdisk_41225->SetStats(0);
   gre->SetHistogram(Graph_hit_sctdisk_41225);
   
   multigraph->Add(gre,"lp");
   
   Double_t hit_sctdisk_5_fx1226[18] = {
   0,
   1,
   2,
   3,
   4,
   5,
   6,
   7,
   8,
   9,
   10,
   11,
   12,
   13,
   14,
   15,
   16,
   17};
   Double_t hit_sctdisk_5_fy1226[18] = {
   44.4,
   48.8,
   63.6,
   60.8,
   46.4,
   47.2,
   40,
   27.2,
   51.6,
   77.60001,
   64.4,
   42,
   48.8,
   38,
   64.8,
   69.2,
   50.8,
   82.4};
   Double_t hit_sctdisk_5_fex1226[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t hit_sctdisk_5_fey1226[18] = {
   1.75662,
   4.074806,
   2.334133,
   2.030741,
   5.712354,
   2.50977,
   1.894035,
   1.251757,
   2.532151,
   3.747842,
   5.7315,
   1.938288,
   2.377438,
   1.857802,
   4.568167,
   2.591136,
   2.66151,
   3.918624};
   gre = new TGraphErrors(18,hit_sctdisk_5_fx1226,hit_sctdisk_5_fy1226,hit_sctdisk_5_fex1226,hit_sctdisk_5_fey1226);
   gre->SetName("hit_sctdisk_5");
   gre->SetTitle("Disk 5");
   gre->SetFillColor(1);
   gre->SetLineColor(6);
   gre->SetMarkerColor(6);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_hit_sctdisk_51226 = new TH1F("Graph_hit_sctdisk_51226","Disk 5",100,0,18.7);
   Graph_hit_sctdisk_51226->SetMinimum(19.91121);
   Graph_hit_sctdisk_51226->SetMaximum(92.35566);
   Graph_hit_sctdisk_51226->SetDirectory(0);
   Graph_hit_sctdisk_51226->SetStats(0);
   gre->SetHistogram(Graph_hit_sctdisk_51226);
   
   multigraph->Add(gre,"lp");
   
   Double_t hit_sctdisk_6_fx1227[18] = {
   0,
   1,
   2,
   3,
   4,
   5,
   6,
   7,
   8,
   9,
   10,
   11,
   12,
   13,
   14,
   15,
   16,
   17};
   Double_t hit_sctdisk_6_fy1227[18] = {
   40.8,
   50.8,
   62.8,
   79.6,
   32.4,
   52,
   41.6,
   34,
   39.6,
   61.2,
   38.4,
   25.6,
   33.6,
   42.8,
   55.2,
   80,
   33.6,
   51.6};
   Double_t hit_sctdisk_6_fex1227[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t hit_sctdisk_6_fey1227[18] = {
   1.797889,
   2.990779,
   2.97363,
   14.43219,
   2.773686,
   2.989506,
   3.792816,
   5.523504,
   4.35403,
   9.601851,
   2.06956,
   1.494523,
   1.754116,
   3.074598,
   3.141218,
   9.578855,
   2.519475,
   3.670301};
   gre = new TGraphErrors(18,hit_sctdisk_6_fx1227,hit_sctdisk_6_fy1227,hit_sctdisk_6_fex1227,hit_sctdisk_6_fey1227);
   gre->SetName("hit_sctdisk_6");
   gre->SetTitle("Disk 0");
   gre->SetFillColor(1);
   gre->SetLineStyle(2);
   gre->SetMarkerStyle(21);
   
   TH1F *Graph_hit_sctdisk_61227 = new TH1F("Graph_hit_sctdisk_61227","Disk 0",100,0,18.7);
   Graph_hit_sctdisk_61227->SetMinimum(17.11281);
   Graph_hit_sctdisk_61227->SetMaximum(101.0249);
   Graph_hit_sctdisk_61227->SetDirectory(0);
   Graph_hit_sctdisk_61227->SetStats(0);
   gre->SetHistogram(Graph_hit_sctdisk_61227);
   
   multigraph->Add(gre,"lp");
   
   Double_t hit_sctdisk_7_fx1228[18] = {
   0,
   1,
   2,
   3,
   4,
   5,
   6,
   7,
   8,
   9,
   10,
   11,
   12,
   13,
   14,
   15,
   16,
   17};
   Double_t hit_sctdisk_7_fy1228[18] = {
   46.4,
   44.8,
   50.4,
   60.8,
   38.4,
   52.8,
   58.8,
   22,
   54.8,
   49.2,
   38.8,
   48.8,
   48.8,
   141.6,
   62.4,
   56.8,
   46.4,
   56.4};
   Double_t hit_sctdisk_7_fex1228[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t hit_sctdisk_7_fey1228[18] = {
   2.221043,
   2.057068,
   2.6403,
   3.979762,
   2.775064,
   14.60876,
   4.011707,
   2.257685,
   3.714777,
   2.845347,
   3.069978,
   6.673213,
   11.351,
   50.03679,
   3.619061,
   6.191545,
   4.453139,
   3.683732};
   gre = new TGraphErrors(18,hit_sctdisk_7_fx1228,hit_sctdisk_7_fy1228,hit_sctdisk_7_fex1228,hit_sctdisk_7_fey1228);
   gre->SetName("hit_sctdisk_7");
   gre->SetTitle("Disk 1");
   gre->SetFillColor(1);
   gre->SetLineColor(2);
   gre->SetLineStyle(2);
   gre->SetMarkerColor(2);
   gre->SetMarkerStyle(21);
   
   TH1F *Graph_hit_sctdisk_71228 = new TH1F("Graph_hit_sctdisk_71228","Disk 1",100,0,18.7);
   Graph_hit_sctdisk_71228->SetMinimum(2.552867);
   Graph_hit_sctdisk_71228->SetMaximum(208.8262);
   Graph_hit_sctdisk_71228->SetDirectory(0);
   Graph_hit_sctdisk_71228->SetStats(0);
   gre->SetHistogram(Graph_hit_sctdisk_71228);
   
   multigraph->Add(gre,"lp");
   
   Double_t hit_sctdisk_8_fx1229[18] = {
   0,
   1,
   2,
   3,
   4,
   5,
   6,
   7,
   8,
   9,
   10,
   11,
   12,
   13,
   14,
   15,
   16,
   17};
   Double_t hit_sctdisk_8_fy1229[18] = {
   63.6,
   61.2,
   68.8,
   46.8,
   33.6,
   64.8,
   38.8,
   22.4,
   47.6,
   46.4,
   36.4,
   46.4,
   26,
   40,
   61.2,
   53.6,
   37.6,
   59.2};
   Double_t hit_sctdisk_8_fex1229[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t hit_sctdisk_8_fey1229[18] = {
   3.901479,
   3.178776,
   2.989847,
   2.523353,
   2.073145,
   20.46797,
   2.035363,
   1.488624,
   2.427619,
   3.412331,
   1.755663,
   3.455208,
   1.623823,
   2.688322,
   3.936388,
   3.471618,
   2.134577,
   3.782107};
   gre = new TGraphErrors(18,hit_sctdisk_8_fx1229,hit_sctdisk_8_fy1229,hit_sctdisk_8_fex1229,hit_sctdisk_8_fey1229);
   gre->SetName("hit_sctdisk_8");
   gre->SetTitle("Disk 2");
   gre->SetFillColor(1);
   gre->SetLineColor(3);
   gre->SetLineStyle(2);
   gre->SetMarkerColor(3);
   gre->SetMarkerStyle(21);
   
   TH1F *Graph_hit_sctdisk_81229 = new TH1F("Graph_hit_sctdisk_81229","Disk 2",100,0,18.7);
   Graph_hit_sctdisk_81229->SetMinimum(14.47572);
   Graph_hit_sctdisk_81229->SetMaximum(91.70363);
   Graph_hit_sctdisk_81229->SetDirectory(0);
   Graph_hit_sctdisk_81229->SetStats(0);
   gre->SetHistogram(Graph_hit_sctdisk_81229);
   
   multigraph->Add(gre,"lp");
   
   Double_t hit_sctdisk_9_fx1230[18] = {
   0,
   1,
   2,
   3,
   4,
   5,
   6,
   7,
   8,
   9,
   10,
   11,
   12,
   13,
   14,
   15,
   16,
   17};
   Double_t hit_sctdisk_9_fy1230[18] = {
   34.8,
   48.8,
   68.8,
   71.6,
   38.4,
   46.8,
   48,
   18,
   49.2,
   77.2,
   73.6,
   30.8,
   42.4,
   34,
   73.2,
   55.6,
   50.8,
   49.6};
   Double_t hit_sctdisk_9_fex1230[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t hit_sctdisk_9_fey1230[18] = {
   2.569793,
   2.620263,
   2.797001,
   3.419778,
   2.355783,
   2.162396,
   2.969614,
   1.595448,
   3.144519,
   5.746603,
   11.7567,
   3.991054,
   4.120102,
   2.83216,
   3.739519,
   2.00921,
   4.518912,
   2.380663};
   gre = new TGraphErrors(18,hit_sctdisk_9_fx1230,hit_sctdisk_9_fy1230,hit_sctdisk_9_fex1230,hit_sctdisk_9_fey1230);
   gre->SetName("hit_sctdisk_9");
   gre->SetTitle("Disk 3");
   gre->SetFillColor(1);
   gre->SetLineColor(4);
   gre->SetLineStyle(2);
   gre->SetMarkerColor(4);
   gre->SetMarkerStyle(21);
   
   TH1F *Graph_hit_sctdisk_91230 = new TH1F("Graph_hit_sctdisk_91230","Disk 3",100,0,18.7);
   Graph_hit_sctdisk_91230->SetMinimum(9.509337);
   Graph_hit_sctdisk_91230->SetMaximum(92.25191);
   Graph_hit_sctdisk_91230->SetDirectory(0);
   Graph_hit_sctdisk_91230->SetStats(0);
   gre->SetHistogram(Graph_hit_sctdisk_91230);
   
   multigraph->Add(gre,"lp");
   
   Double_t hit_sctdisk_10_fx1231[18] = {
   0,
   1,
   2,
   3,
   4,
   5,
   6,
   7,
   8,
   9,
   10,
   11,
   12,
   13,
   14,
   15,
   16,
   17};
   Double_t hit_sctdisk_10_fy1231[18] = {
   77.2,
   59.2,
   147.6,
   65.6,
   38,
   58.4,
   47.6,
   24.4,
   45.6,
   47.2,
   50,
   30,
   43.2,
   39.6,
   58.4,
   50,
   38,
   76.8};
   Double_t hit_sctdisk_10_fex1231[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t hit_sctdisk_10_fey1231[18] = {
   20.21763,
   4.66984,
   48.19202,
   3.145437,
   3.213703,
   2.385632,
   2.313078,
   1.731001,
   2.617009,
   3.779607,
   6.666377,
   1.159516,
   3.127381,
   2.075295,
   2.199504,
   2.371516,
   3.230822,
   3.656277};
   gre = new TGraphErrors(18,hit_sctdisk_10_fx1231,hit_sctdisk_10_fy1231,hit_sctdisk_10_fex1231,hit_sctdisk_10_fey1231);
   gre->SetName("hit_sctdisk_10");
   gre->SetTitle("Disk 4");
   gre->SetFillColor(1);
   gre->SetLineColor(5);
   gre->SetLineStyle(2);
   gre->SetMarkerColor(5);
   gre->SetMarkerStyle(21);
   
   TH1F *Graph_hit_sctdisk_101231 = new TH1F("Graph_hit_sctdisk_101231","Disk 4",100,0,18.7);
   Graph_hit_sctdisk_101231->SetMinimum(5.356698);
   Graph_hit_sctdisk_101231->SetMaximum(213.1043);
   Graph_hit_sctdisk_101231->SetDirectory(0);
   Graph_hit_sctdisk_101231->SetStats(0);
   gre->SetHistogram(Graph_hit_sctdisk_101231);
   
   multigraph->Add(gre,"lp");
   
   Double_t hit_sctdisk_11_fx1232[18] = {
   0,
   1,
   2,
   3,
   4,
   5,
   6,
   7,
   8,
   9,
   10,
   11,
   12,
   13,
   14,
   15,
   16,
   17};
   Double_t hit_sctdisk_11_fy1232[18] = {
   51.6,
   36.8,
   58.8,
   85.2,
   24.4,
   42.4,
   26.4,
   34,
   63.2,
   64.8,
   42.8,
   42,
   31.2,
   45.2,
   74.8,
   60.4,
   40.4,
   46};
   Double_t hit_sctdisk_11_fex1232[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t hit_sctdisk_11_fey1232[18] = {
   4.398468,
   1.551839,
   1.580348,
   11.9887,
   1.690021,
   2.316138,
   1.942507,
   1.884144,
   3.05512,
   3.89079,
   2.287023,
   2.696665,
   2.231364,
   3.446662,
   2.954006,
   3.629375,
   2.30524,
   1.472468};
   gre = new TGraphErrors(18,hit_sctdisk_11_fx1232,hit_sctdisk_11_fy1232,hit_sctdisk_11_fex1232,hit_sctdisk_11_fey1232);
   gre->SetName("hit_sctdisk_11");
   gre->SetTitle("Disk 5");
   gre->SetFillColor(1);
   gre->SetLineColor(6);
   gre->SetLineStyle(2);
   gre->SetMarkerColor(6);
   gre->SetMarkerStyle(21);
   
   TH1F *Graph_hit_sctdisk_111232 = new TH1F("Graph_hit_sctdisk_111232","Disk 5",100,0,18.7);
   Graph_hit_sctdisk_111232->SetMinimum(15.26211);
   Graph_hit_sctdisk_111232->SetMaximum(104.6366);
   Graph_hit_sctdisk_111232->SetDirectory(0);
   Graph_hit_sctdisk_111232->SetStats(0);
   gre->SetHistogram(Graph_hit_sctdisk_111232);
   
   multigraph->Add(gre,"lp");
   multigraph->Draw("Apl");
   multigraph->GetXaxis()->SetTitle("moduleID");
   multigraph->GetYaxis()->SetTitle("Hits per etaModuleID");
   
   TLegend *leg = new TLegend(0.14,0.62,0.34,0.87,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextFont(62);
   leg->SetTextSize(0.04);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(10);
   leg->SetFillStyle(0);
   TLegendEntry *entry=leg->AddEntry("NULL","Sct Disk (left)","h");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("hit_sctdisk_0","Disk 0","lp");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("hit_sctdisk_1","Disk 1","lp");
   entry->SetLineColor(2);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(2);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("hit_sctdisk_2","Disk 2","lp");
   entry->SetLineColor(3);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(3);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("hit_sctdisk_3","Disk 3","lp");
   entry->SetLineColor(4);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(4);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("hit_sctdisk_4","Disk 4","lp");
   entry->SetLineColor(5);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(5);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("hit_sctdisk_5","Disk 5","lp");
   entry->SetLineColor(6);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(6);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   leg->Draw();
   
   leg = new TLegend(0.27,0.62,0.47,0.87,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextFont(62);
   leg->SetTextSize(0.04);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(10);
   leg->SetFillStyle(0);
   entry=leg->AddEntry("NULL","  (right)","h");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("hit_sctdisk_6","Disk 0","lp");
   entry->SetLineColor(1);
   entry->SetLineStyle(2);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("hit_sctdisk_7","Disk 1","lp");
   entry->SetLineColor(2);
   entry->SetLineStyle(2);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(2);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("hit_sctdisk_8","Disk 2","lp");
   entry->SetLineColor(3);
   entry->SetLineStyle(2);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(3);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("hit_sctdisk_9","Disk 3","lp");
   entry->SetLineColor(4);
   entry->SetLineStyle(2);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(4);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("hit_sctdisk_10","Disk 4","lp");
   entry->SetLineColor(5);
   entry->SetLineStyle(2);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(5);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("hit_sctdisk_11","Disk 5","lp");
   entry->SetLineColor(6);
   entry->SetLineStyle(2);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(6);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   leg->Draw();
   c1_n35->Modified();
   c1_n35->cd();
   c1_n35->SetSelected(c1_n35);
}
