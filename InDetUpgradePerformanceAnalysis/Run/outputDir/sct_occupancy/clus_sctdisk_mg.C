void clus_sctdisk_mg()
{
//=========Macro generated from canvas: c1_n33/c1_n33
//=========  (Mon Apr 25 16:02:11 2016) by ROOT version6.04/12
   TCanvas *c1_n33 = new TCanvas("c1_n33", "c1_n33",0,0,700,500);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   c1_n33->SetHighLightColor(2);
   c1_n33->Range(-3.1875,-2.413035,20.1875,106.1213);
   c1_n33->SetFillColor(0);
   c1_n33->SetBorderMode(0);
   c1_n33->SetBorderSize(2);
   c1_n33->SetTickx(1);
   c1_n33->SetTicky(1);
   c1_n33->SetFrameBorderMode(0);
   c1_n33->SetFrameBorderMode(0);
   
   TMultiGraph *multigraph = new TMultiGraph();
   multigraph->SetName("clus_sctdisk_mg");
   multigraph->SetTitle("");
   
   Double_t clus_sctdisk_0_fx1197[18] = {
   0,
   1,
   2,
   3,
   4,
   5,
   6,
   7,
   8,
   9,
   10,
   11,
   12,
   13,
   14,
   15,
   16,
   17};
   Double_t clus_sctdisk_0_fy1197[18] = {
   30,
   41.6,
   44.8,
   50,
   17.2,
   33.6,
   23.2,
   20,
   38.8,
   32,
   31.6,
   26.4,
   19.6,
   28.8,
   40,
   33.2,
   20.8,
   31.2};
   Double_t clus_sctdisk_0_fex1197[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t clus_sctdisk_0_fey1197[18] = {
   3.464102,
   4.079216,
   4.233202,
   4.472136,
   2.622975,
   3.66606,
   3.046309,
   2.828427,
   3.939543,
   3.577709,
   3.555278,
   3.249615,
   2.8,
   3.394112,
   4,
   3.644174,
   2.884441,
   3.532704};
   TGraphErrors *gre = new TGraphErrors(18,clus_sctdisk_0_fx1197,clus_sctdisk_0_fy1197,clus_sctdisk_0_fex1197,clus_sctdisk_0_fey1197);
   gre->SetName("clus_sctdisk_0");
   gre->SetTitle("Disk 0");
   gre->SetFillColor(1);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_clus_sctdisk_01197 = new TH1F("Graph_clus_sctdisk_01197","Disk 0",100,0,18.7);
   Graph_clus_sctdisk_01197->SetMinimum(10.58751);
   Graph_clus_sctdisk_01197->SetMaximum(58.46165);
   Graph_clus_sctdisk_01197->SetDirectory(0);
   Graph_clus_sctdisk_01197->SetStats(0);
   gre->SetHistogram(Graph_clus_sctdisk_01197);
   
   multigraph->Add(gre,"lp");
   
   Double_t clus_sctdisk_1_fx1198[18] = {
   0,
   1,
   2,
   3,
   4,
   5,
   6,
   7,
   8,
   9,
   10,
   11,
   12,
   13,
   14,
   15,
   16,
   17};
   Double_t clus_sctdisk_1_fy1198[18] = {
   36.4,
   35.6,
   38.4,
   46,
   18.4,
   39.6,
   32.4,
   17.6,
   35.6,
   35.2,
   35.2,
   25.2,
   18.8,
   29.2,
   39.6,
   36.8,
   22.8,
   30};
   Double_t clus_sctdisk_1_fex1198[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t clus_sctdisk_1_fey1198[18] = {
   3.815757,
   3.773592,
   3.919183,
   4.289522,
   2.712932,
   3.97995,
   3.6,
   2.6533,
   3.773592,
   3.752332,
   3.752332,
   3.174902,
   2.742262,
   3.417601,
   3.97995,
   3.836665,
   3.019934,
   3.464102};
   gre = new TGraphErrors(18,clus_sctdisk_1_fx1198,clus_sctdisk_1_fy1198,clus_sctdisk_1_fex1198,clus_sctdisk_1_fey1198);
   gre->SetName("clus_sctdisk_1");
   gre->SetTitle("Disk 1");
   gre->SetFillColor(1);
   gre->SetLineColor(2);
   gre->SetMarkerColor(2);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_clus_sctdisk_11198 = new TH1F("Graph_clus_sctdisk_11198","Disk 1",100,0,18.7);
   Graph_clus_sctdisk_11198->SetMinimum(11.41242);
   Graph_clus_sctdisk_11198->SetMaximum(53.8238);
   Graph_clus_sctdisk_11198->SetDirectory(0);
   Graph_clus_sctdisk_11198->SetStats(0);
   gre->SetHistogram(Graph_clus_sctdisk_11198);
   
   multigraph->Add(gre,"lp");
   
   Double_t clus_sctdisk_2_fx1199[18] = {
   0,
   1,
   2,
   3,
   4,
   5,
   6,
   7,
   8,
   9,
   10,
   11,
   12,
   13,
   14,
   15,
   16,
   17};
   Double_t clus_sctdisk_2_fy1199[18] = {
   40.8,
   41.6,
   52.8,
   49.6,
   23.2,
   33.2,
   33.6,
   16.8,
   45.2,
   34.8,
   25.2,
   19.6,
   24.4,
   36.8,
   39.6,
   45.6,
   30.4,
   33.2};
   Double_t clus_sctdisk_2_fex1199[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t clus_sctdisk_2_fey1199[18] = {
   4.039802,
   4.079216,
   4.59565,
   4.454212,
   3.046309,
   3.644174,
   3.66606,
   2.592296,
   4.252058,
   3.730952,
   3.174902,
   2.8,
   3.1241,
   3.836665,
   3.97995,
   4.270831,
   3.487119,
   3.644174};
   gre = new TGraphErrors(18,clus_sctdisk_2_fx1199,clus_sctdisk_2_fy1199,clus_sctdisk_2_fex1199,clus_sctdisk_2_fey1199);
   gre->SetName("clus_sctdisk_2");
   gre->SetTitle("Disk 2");
   gre->SetFillColor(1);
   gre->SetLineColor(3);
   gre->SetMarkerColor(3);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_clus_sctdisk_21199 = new TH1F("Graph_clus_sctdisk_21199","Disk 2",100,0,18.7);
   Graph_clus_sctdisk_21199->SetMinimum(9.888908);
   Graph_clus_sctdisk_21199->SetMaximum(61.71444);
   Graph_clus_sctdisk_21199->SetDirectory(0);
   Graph_clus_sctdisk_21199->SetStats(0);
   gre->SetHistogram(Graph_clus_sctdisk_21199);
   
   multigraph->Add(gre,"lp");
   
   Double_t clus_sctdisk_3_fx1200[18] = {
   0,
   1,
   2,
   3,
   4,
   5,
   6,
   7,
   8,
   9,
   10,
   11,
   12,
   13,
   14,
   15,
   16,
   17};
   Double_t clus_sctdisk_3_fy1200[18] = {
   30.8,
   41.6,
   62.8,
   56.8,
   29.6,
   42.8,
   35.6,
   15.6,
   33.6,
   47.6,
   32.8,
   24,
   28.4,
   24.4,
   38,
   39.6,
   29.2,
   42.4};
   Double_t clus_sctdisk_3_fex1200[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t clus_sctdisk_3_fey1200[18] = {
   3.509986,
   4.079216,
   5.011985,
   4.76655,
   3.44093,
   4.137632,
   3.773592,
   2.497999,
   3.66606,
   4.363485,
   3.622154,
   3.098387,
   3.37046,
   3.1241,
   3.898718,
   3.97995,
   3.417601,
   4.118252};
   gre = new TGraphErrors(18,clus_sctdisk_3_fx1200,clus_sctdisk_3_fy1200,clus_sctdisk_3_fex1200,clus_sctdisk_3_fey1200);
   gre->SetName("clus_sctdisk_3");
   gre->SetTitle("Disk 3");
   gre->SetFillColor(1);
   gre->SetLineColor(4);
   gre->SetMarkerColor(4);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_clus_sctdisk_31200 = new TH1F("Graph_clus_sctdisk_31200","Disk 3",100,0,18.7);
   Graph_clus_sctdisk_31200->SetMinimum(7.631002);
   Graph_clus_sctdisk_31200->SetMaximum(73.28299);
   Graph_clus_sctdisk_31200->SetDirectory(0);
   Graph_clus_sctdisk_31200->SetStats(0);
   gre->SetHistogram(Graph_clus_sctdisk_31200);
   
   multigraph->Add(gre,"lp");
   
   Double_t clus_sctdisk_4_fx1201[18] = {
   0,
   1,
   2,
   3,
   4,
   5,
   6,
   7,
   8,
   9,
   10,
   11,
   12,
   13,
   14,
   15,
   16,
   17};
   Double_t clus_sctdisk_4_fy1201[18] = {
   38.4,
   42.4,
   53.2,
   51.6,
   29.6,
   42.4,
   38.4,
   18.4,
   46.8,
   40.8,
   28,
   22,
   29.6,
   34,
   40.4,
   43.2,
   30.8,
   43.2};
   Double_t clus_sctdisk_4_fex1201[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t clus_sctdisk_4_fey1201[18] = {
   3.919183,
   4.118252,
   4.613025,
   4.543127,
   3.44093,
   4.118252,
   3.919183,
   2.712932,
   4.326662,
   4.039802,
   3.34664,
   2.966479,
   3.44093,
   3.687818,
   4.01995,
   4.156922,
   3.509986,
   4.156922};
   gre = new TGraphErrors(18,clus_sctdisk_4_fx1201,clus_sctdisk_4_fy1201,clus_sctdisk_4_fex1201,clus_sctdisk_4_fey1201);
   gre->SetName("clus_sctdisk_4");
   gre->SetTitle("Disk 4");
   gre->SetFillColor(1);
   gre->SetLineColor(5);
   gre->SetMarkerColor(5);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_clus_sctdisk_41201 = new TH1F("Graph_clus_sctdisk_41201","Disk 4",100,0,18.7);
   Graph_clus_sctdisk_41201->SetMinimum(11.47447);
   Graph_clus_sctdisk_41201->SetMaximum(62.02562);
   Graph_clus_sctdisk_41201->SetDirectory(0);
   Graph_clus_sctdisk_41201->SetStats(0);
   gre->SetHistogram(Graph_clus_sctdisk_41201);
   
   multigraph->Add(gre,"lp");
   
   Double_t clus_sctdisk_5_fx1202[18] = {
   0,
   1,
   2,
   3,
   4,
   5,
   6,
   7,
   8,
   9,
   10,
   11,
   12,
   13,
   14,
   15,
   16,
   17};
   Double_t clus_sctdisk_5_fy1202[18] = {
   36.8,
   36,
   55.2,
   53.6,
   28.8,
   38.4,
   30.8,
   23.6,
   38.4,
   53.6,
   46.4,
   32,
   38.4,
   31.2,
   43.6,
   53.6,
   35.6,
   58.8};
   Double_t clus_sctdisk_5_fex1202[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t clus_sctdisk_5_fey1202[18] = {
   3.836665,
   3.794733,
   4.698936,
   4.630335,
   3.394112,
   3.919183,
   3.509986,
   3.072458,
   3.919183,
   4.630335,
   4.308132,
   3.577709,
   3.919183,
   3.532704,
   4.176123,
   4.630335,
   3.773592,
   4.849742};
   gre = new TGraphErrors(18,clus_sctdisk_5_fx1202,clus_sctdisk_5_fy1202,clus_sctdisk_5_fex1202,clus_sctdisk_5_fey1202);
   gre->SetName("clus_sctdisk_5");
   gre->SetTitle("Disk 5");
   gre->SetFillColor(1);
   gre->SetLineColor(6);
   gre->SetMarkerColor(6);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_clus_sctdisk_51202 = new TH1F("Graph_clus_sctdisk_51202","Disk 5",100,0,18.7);
   Graph_clus_sctdisk_51202->SetMinimum(16.21532);
   Graph_clus_sctdisk_51202->SetMaximum(67.96196);
   Graph_clus_sctdisk_51202->SetDirectory(0);
   Graph_clus_sctdisk_51202->SetStats(0);
   gre->SetHistogram(Graph_clus_sctdisk_51202);
   
   multigraph->Add(gre,"lp");
   
   Double_t clus_sctdisk_6_fx1203[18] = {
   0,
   1,
   2,
   3,
   4,
   5,
   6,
   7,
   8,
   9,
   10,
   11,
   12,
   13,
   14,
   15,
   16,
   17};
   Double_t clus_sctdisk_6_fy1203[18] = {
   32,
   34,
   45.6,
   40.4,
   19.6,
   39.6,
   26.8,
   18,
   26.8,
   29.2,
   26.4,
   20.4,
   21.2,
   29.6,
   36.8,
   40.8,
   20,
   29.2};
   Double_t clus_sctdisk_6_fex1203[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t clus_sctdisk_6_fey1203[18] = {
   3.577709,
   3.687818,
   4.270831,
   4.01995,
   2.8,
   3.97995,
   3.274141,
   2.683281,
   3.274141,
   3.417601,
   3.249615,
   2.856571,
   2.912044,
   3.44093,
   3.836665,
   4.039802,
   2.828427,
   3.417601};
   gre = new TGraphErrors(18,clus_sctdisk_6_fx1203,clus_sctdisk_6_fy1203,clus_sctdisk_6_fex1203,clus_sctdisk_6_fey1203);
   gre->SetName("clus_sctdisk_6");
   gre->SetTitle("Disk 0");
   gre->SetFillColor(1);
   gre->SetLineStyle(2);
   gre->SetMarkerStyle(21);
   
   TH1F *Graph_clus_sctdisk_61203 = new TH1F("Graph_clus_sctdisk_61203","Disk 0",100,0,18.7);
   Graph_clus_sctdisk_61203->SetMinimum(11.86131);
   Graph_clus_sctdisk_61203->SetMaximum(53.32624);
   Graph_clus_sctdisk_61203->SetDirectory(0);
   Graph_clus_sctdisk_61203->SetStats(0);
   gre->SetHistogram(Graph_clus_sctdisk_61203);
   
   multigraph->Add(gre,"lp");
   
   Double_t clus_sctdisk_7_fx1204[18] = {
   0,
   1,
   2,
   3,
   4,
   5,
   6,
   7,
   8,
   9,
   10,
   11,
   12,
   13,
   14,
   15,
   16,
   17};
   Double_t clus_sctdisk_7_fy1204[18] = {
   36,
   34.4,
   37.6,
   43.2,
   24.8,
   26.8,
   35.2,
   14.4,
   37.6,
   30.4,
   25.6,
   27.6,
   21.6,
   31.2,
   40.4,
   34,
   27.2,
   34.4};
   Double_t clus_sctdisk_7_fex1204[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t clus_sctdisk_7_fey1204[18] = {
   3.794733,
   3.709447,
   3.878144,
   4.156922,
   3.149603,
   3.274141,
   3.752332,
   2.4,
   3.878144,
   3.487119,
   3.2,
   3.322649,
   2.939388,
   3.532704,
   4.01995,
   3.687818,
   3.298485,
   3.709447};
   gre = new TGraphErrors(18,clus_sctdisk_7_fx1204,clus_sctdisk_7_fy1204,clus_sctdisk_7_fex1204,clus_sctdisk_7_fey1204);
   gre->SetName("clus_sctdisk_7");
   gre->SetTitle("Disk 1");
   gre->SetFillColor(1);
   gre->SetLineColor(2);
   gre->SetLineStyle(2);
   gre->SetMarkerColor(2);
   gre->SetMarkerStyle(21);
   
   TH1F *Graph_clus_sctdisk_71204 = new TH1F("Graph_clus_sctdisk_71204","Disk 1",100,0,18.7);
   Graph_clus_sctdisk_71204->SetMinimum(8.464307);
   Graph_clus_sctdisk_71204->SetMaximum(50.89261);
   Graph_clus_sctdisk_71204->SetDirectory(0);
   Graph_clus_sctdisk_71204->SetStats(0);
   gre->SetHistogram(Graph_clus_sctdisk_71204);
   
   multigraph->Add(gre,"lp");
   
   Double_t clus_sctdisk_8_fx1205[18] = {
   0,
   1,
   2,
   3,
   4,
   5,
   6,
   7,
   8,
   9,
   10,
   11,
   12,
   13,
   14,
   15,
   16,
   17};
   Double_t clus_sctdisk_8_fy1205[18] = {
   42,
   47.2,
   49.6,
   37.6,
   23.6,
   29.2,
   30,
   16.4,
   34,
   32.4,
   27.6,
   31.6,
   20.4,
   26.4,
   41.6,
   36,
   27.2,
   39.2};
   Double_t clus_sctdisk_8_fex1205[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t clus_sctdisk_8_fey1205[18] = {
   4.098781,
   4.345112,
   4.454212,
   3.878144,
   3.072458,
   3.417601,
   3.464102,
   2.56125,
   3.687818,
   3.6,
   3.322649,
   3.555278,
   2.856571,
   3.249615,
   4.079216,
   3.794733,
   3.298485,
   3.959798};
   gre = new TGraphErrors(18,clus_sctdisk_8_fx1205,clus_sctdisk_8_fy1205,clus_sctdisk_8_fex1205,clus_sctdisk_8_fey1205);
   gre->SetName("clus_sctdisk_8");
   gre->SetTitle("Disk 2");
   gre->SetFillColor(1);
   gre->SetLineColor(3);
   gre->SetLineStyle(2);
   gre->SetMarkerColor(3);
   gre->SetMarkerStyle(21);
   
   TH1F *Graph_clus_sctdisk_81205 = new TH1F("Graph_clus_sctdisk_81205","Disk 2",100,0,18.7);
   Graph_clus_sctdisk_81205->SetMinimum(9.817204);
   Graph_clus_sctdisk_81205->SetMaximum(58.07576);
   Graph_clus_sctdisk_81205->SetDirectory(0);
   Graph_clus_sctdisk_81205->SetStats(0);
   gre->SetHistogram(Graph_clus_sctdisk_81205);
   
   multigraph->Add(gre,"lp");
   
   Double_t clus_sctdisk_9_fx1206[18] = {
   0,
   1,
   2,
   3,
   4,
   5,
   6,
   7,
   8,
   9,
   10,
   11,
   12,
   13,
   14,
   15,
   16,
   17};
   Double_t clus_sctdisk_9_fy1206[18] = {
   29.6,
   36.4,
   57.6,
   52,
   28.4,
   36,
   34.8,
   13.6,
   34.4,
   46.8,
   38.8,
   19.2,
   25.6,
   21.6,
   54.4,
   42,
   28.4,
   36.4};
   Double_t clus_sctdisk_9_fex1206[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t clus_sctdisk_9_fey1206[18] = {
   3.44093,
   3.815757,
   4.8,
   4.560701,
   3.37046,
   3.794733,
   3.730952,
   2.332381,
   3.709447,
   4.326662,
   3.939543,
   2.771281,
   3.2,
   2.939388,
   4.664762,
   4.098781,
   3.37046,
   3.815757};
   gre = new TGraphErrors(18,clus_sctdisk_9_fx1206,clus_sctdisk_9_fy1206,clus_sctdisk_9_fex1206,clus_sctdisk_9_fey1206);
   gre->SetName("clus_sctdisk_9");
   gre->SetTitle("Disk 3");
   gre->SetFillColor(1);
   gre->SetLineColor(4);
   gre->SetLineStyle(2);
   gre->SetMarkerColor(4);
   gre->SetMarkerStyle(21);
   
   TH1F *Graph_clus_sctdisk_91206 = new TH1F("Graph_clus_sctdisk_91206","Disk 3",100,0,18.7);
   Graph_clus_sctdisk_91206->SetMinimum(6.154382);
   Graph_clus_sctdisk_91206->SetMaximum(67.51324);
   Graph_clus_sctdisk_91206->SetDirectory(0);
   Graph_clus_sctdisk_91206->SetStats(0);
   gre->SetHistogram(Graph_clus_sctdisk_91206);
   
   multigraph->Add(gre,"lp");
   
   Double_t clus_sctdisk_10_fx1207[18] = {
   0,
   1,
   2,
   3,
   4,
   5,
   6,
   7,
   8,
   9,
   10,
   11,
   12,
   13,
   14,
   15,
   16,
   17};
   Double_t clus_sctdisk_10_fy1207[18] = {
   43.6,
   43.6,
   52.8,
   49.2,
   28.8,
   48.8,
   36.8,
   18,
   35.2,
   32.8,
   28,
   27.2,
   31.6,
   29.6,
   44.4,
   37.6,
   27.2,
   54};
   Double_t clus_sctdisk_10_fex1207[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t clus_sctdisk_10_fey1207[18] = {
   4.176123,
   4.176123,
   4.59565,
   4.436214,
   3.394112,
   4.418144,
   3.836665,
   2.683281,
   3.752332,
   3.622154,
   3.34664,
   3.298485,
   3.555278,
   3.44093,
   4.214262,
   3.878144,
   3.298485,
   4.64758};
   gre = new TGraphErrors(18,clus_sctdisk_10_fx1207,clus_sctdisk_10_fy1207,clus_sctdisk_10_fex1207,clus_sctdisk_10_fey1207);
   gre->SetName("clus_sctdisk_10");
   gre->SetTitle("Disk 4");
   gre->SetFillColor(1);
   gre->SetLineColor(5);
   gre->SetLineStyle(2);
   gre->SetMarkerColor(5);
   gre->SetMarkerStyle(21);
   
   TH1F *Graph_clus_sctdisk_101207 = new TH1F("Graph_clus_sctdisk_101207","Disk 4",100,0,18.7);
   Graph_clus_sctdisk_101207->SetMinimum(10.98363);
   Graph_clus_sctdisk_101207->SetMaximum(62.98067);
   Graph_clus_sctdisk_101207->SetDirectory(0);
   Graph_clus_sctdisk_101207->SetStats(0);
   gre->SetHistogram(Graph_clus_sctdisk_101207);
   
   multigraph->Add(gre,"lp");
   
   Double_t clus_sctdisk_11_fx1208[18] = {
   0,
   1,
   2,
   3,
   4,
   5,
   6,
   7,
   8,
   9,
   10,
   11,
   12,
   13,
   14,
   15,
   16,
   17};
   Double_t clus_sctdisk_11_fy1208[18] = {
   38.4,
   31.6,
   51.6,
   58,
   19.2,
   36,
   19.6,
   26,
   50.4,
   46,
   34,
   32.4,
   24,
   31.2,
   57.6,
   44.4,
   30.4,
   39.6};
   Double_t clus_sctdisk_11_fex1208[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t clus_sctdisk_11_fey1208[18] = {
   3.919183,
   3.555278,
   4.543127,
   4.816638,
   2.771281,
   3.794733,
   2.8,
   3.224903,
   4.489989,
   4.289522,
   3.687818,
   3.6,
   3.098387,
   3.532704,
   4.8,
   4.214262,
   3.487119,
   3.97995};
   gre = new TGraphErrors(18,clus_sctdisk_11_fx1208,clus_sctdisk_11_fy1208,clus_sctdisk_11_fex1208,clus_sctdisk_11_fey1208);
   gre->SetName("clus_sctdisk_11");
   gre->SetTitle("Disk 5");
   gre->SetFillColor(1);
   gre->SetLineColor(6);
   gre->SetLineStyle(2);
   gre->SetMarkerColor(6);
   gre->SetMarkerStyle(21);
   
   TH1F *Graph_clus_sctdisk_111208 = new TH1F("Graph_clus_sctdisk_111208","Disk 5",100,0,18.7);
   Graph_clus_sctdisk_111208->SetMinimum(11.78993);
   Graph_clus_sctdisk_111208->SetMaximum(67.45543);
   Graph_clus_sctdisk_111208->SetDirectory(0);
   Graph_clus_sctdisk_111208->SetStats(0);
   gre->SetHistogram(Graph_clus_sctdisk_111208);
   
   multigraph->Add(gre,"lp");
   multigraph->Draw("Apl");
   multigraph->GetXaxis()->SetTitle("moduleID");
   multigraph->GetYaxis()->SetTitle("Cluster per etaModuleID");
   
   TLegend *leg = new TLegend(0.14,0.62,0.34,0.87,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextFont(62);
   leg->SetTextSize(0.04);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(10);
   leg->SetFillStyle(0);
   TLegendEntry *entry=leg->AddEntry("NULL","Sct Disk (left)","h");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("clus_sctdisk_0","Disk 0","lp");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("clus_sctdisk_1","Disk 1","lp");
   entry->SetLineColor(2);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(2);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("clus_sctdisk_2","Disk 2","lp");
   entry->SetLineColor(3);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(3);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("clus_sctdisk_3","Disk 3","lp");
   entry->SetLineColor(4);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(4);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("clus_sctdisk_4","Disk 4","lp");
   entry->SetLineColor(5);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(5);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("clus_sctdisk_5","Disk 5","lp");
   entry->SetLineColor(6);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(6);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   leg->Draw();
   
   leg = new TLegend(0.27,0.62,0.47,0.87,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextFont(62);
   leg->SetTextSize(0.04);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(10);
   leg->SetFillStyle(0);
   entry=leg->AddEntry("NULL","  (right)","h");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("clus_sctdisk_6","Disk 0","lp");
   entry->SetLineColor(1);
   entry->SetLineStyle(2);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("clus_sctdisk_7","Disk 1","lp");
   entry->SetLineColor(2);
   entry->SetLineStyle(2);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(2);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("clus_sctdisk_8","Disk 2","lp");
   entry->SetLineColor(3);
   entry->SetLineStyle(2);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(3);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("clus_sctdisk_9","Disk 3","lp");
   entry->SetLineColor(4);
   entry->SetLineStyle(2);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(4);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("clus_sctdisk_10","Disk 4","lp");
   entry->SetLineColor(5);
   entry->SetLineStyle(2);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(5);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("clus_sctdisk_11","Disk 5","lp");
   entry->SetLineColor(6);
   entry->SetLineStyle(2);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(6);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   leg->Draw();
   c1_n33->Modified();
   c1_n33->cd();
   c1_n33->SetSelected(c1_n33);
}
