void flu_sctdisk_mgr()
{
//=========Macro generated from canvas: c1_n38/c1_n38
//=========  (Mon Apr 25 16:02:12 2016) by ROOT version6.04/12
   TCanvas *c1_n38 = new TCanvas("c1_n38", "c1_n38",0,0,700,500);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   c1_n38->SetHighLightColor(2);
   c1_n38->Range(292.6036,-0.0001488782,1040.112,0.001557877);
   c1_n38->SetFillColor(0);
   c1_n38->SetBorderMode(0);
   c1_n38->SetBorderSize(2);
   c1_n38->SetTickx(1);
   c1_n38->SetTicky(1);
   c1_n38->SetFrameBorderMode(0);
   c1_n38->SetFrameBorderMode(0);
   
   TMultiGraph *multigraph = new TMultiGraph();
   multigraph->SetName("flu_sctdisk_mgr");
   multigraph->SetTitle("");
   
   Double_t flu_r_sctdisk_0_fx1257[18] = {
   394.5976,
   416.1201,
   442.8141,
   473.1741,
   499.6678,
   522.1546,
   547.7357,
   567.5679,
   591.9154,
   622.9136,
   655.0179,
   684.2723,
   710.5016,
   739.6959,
   784.5984,
   839.1926,
   887.8282,
   938.0497};
   Double_t flu_r_sctdisk_0_fy1257[18] = {
   0.0004906555,
   0.0004394519,
   0.0004108687,
   0.000403136,
   0.000277746,
   0.0003106697,
   0.0002194374,
   0.0002673834,
   0.0002707038,
   0.0002112444,
   0.000217271,
   0.0002399269,
   0.0001189159,
   0.0001801315,
   0.0001275126,
   0.0001409248,
   9.707482e-05,
   7.695672e-05};
   Double_t flu_r_sctdisk_0_fex1257[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t flu_r_sctdisk_0_fey1257[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   TGraphErrors *gre = new TGraphErrors(18,flu_r_sctdisk_0_fx1257,flu_r_sctdisk_0_fy1257,flu_r_sctdisk_0_fex1257,flu_r_sctdisk_0_fey1257);
   gre->SetName("flu_r_sctdisk_0");
   gre->SetTitle("Disk 0");
   gre->SetFillColor(1);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_flu_r_sctdisk_01257 = new TH1F("Graph_flu_r_sctdisk_01257","Disk 0",100,340.2524,992.3949);
   Graph_flu_r_sctdisk_01257->SetMinimum(3.558684e-05);
   Graph_flu_r_sctdisk_01257->SetMaximum(0.0005320254);
   Graph_flu_r_sctdisk_01257->SetDirectory(0);
   Graph_flu_r_sctdisk_01257->SetStats(0);
   gre->SetHistogram(Graph_flu_r_sctdisk_01257);
   
   multigraph->Add(gre,"lp");
   
   Double_t flu_r_sctdisk_1_fx1258[18] = {
   394.5908,
   416.1961,
   442.493,
   473.1425,
   499.8581,
   522.3767,
   547.853,
   567.7786,
   591.9629,
   622.6929,
   654.9957,
   684.2572,
   710.5485,
   739.65,
   784.5213,
   839.1216,
   887.9511,
   938.1284};
   Double_t flu_r_sctdisk_1_fy1258[18] = {
   0.0004728135,
   0.0003617028,
   0.0003233934,
   0.0003535885,
   0.000204655,
   0.0003106697,
   0.0003026723,
   0.0002750229,
   0.0002872776,
   0.0002531441,
   0.0002029978,
   0.0001679488,
   0.0001277902,
   0.0001549968,
   0.0001330912,
   0.0001030976,
   7.840658e-05,
   8.236773e-05};
   Double_t flu_r_sctdisk_1_fex1258[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t flu_r_sctdisk_1_fey1258[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   gre = new TGraphErrors(18,flu_r_sctdisk_1_fx1258,flu_r_sctdisk_1_fy1258,flu_r_sctdisk_1_fex1258,flu_r_sctdisk_1_fey1258);
   gre->SetName("flu_r_sctdisk_1");
   gre->SetTitle("Disk 1");
   gre->SetFillColor(1);
   gre->SetLineColor(2);
   gre->SetMarkerColor(2);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_flu_r_sctdisk_11258 = new TH1F("Graph_flu_r_sctdisk_11258","Disk 1",100,340.2371,992.4821);
   Graph_flu_r_sctdisk_11258->SetMinimum(3.896589e-05);
   Graph_flu_r_sctdisk_11258->SetMaximum(0.0005122542);
   Graph_flu_r_sctdisk_11258->SetDirectory(0);
   Graph_flu_r_sctdisk_11258->SetStats(0);
   gre->SetHistogram(Graph_flu_r_sctdisk_11258);
   
   multigraph->Add(gre,"lp");
   
   Double_t flu_r_sctdisk_2_fx1259[18] = {
   394.6503,
   416.2518,
   442.7244,
   473.0844,
   499.5251,
   522.2836,
   547.7934,
   567.9912,
   592.1614,
   622.5195,
   654.9697,
   684.2762,
   710.4731,
   739.7036,
   784.5469,
   839.1573,
   887.9089,
   938.0342};
   Double_t flu_r_sctdisk_2_fy1259[18] = {
   0.0006244706,
   0.0005374835,
   0.0004612332,
   0.0003671015,
   0.0003252552,
   0.0003201559,
   0.0002850164,
   0.0002444648,
   0.0002909606,
   0.0002042611,
   0.0001522483,
   0.0001291914,
   0.0001615126,
   0.0001996806,
   0.0001227308,
   0.0001424082,
   0.0001372115,
   6.733713e-05};
   Double_t flu_r_sctdisk_2_fex1259[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t flu_r_sctdisk_2_fey1259[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   gre = new TGraphErrors(18,flu_r_sctdisk_2_fx1259,flu_r_sctdisk_2_fy1259,flu_r_sctdisk_2_fex1259,flu_r_sctdisk_2_fey1259);
   gre->SetName("flu_r_sctdisk_2");
   gre->SetTitle("Disk 2");
   gre->SetFillColor(1);
   gre->SetLineColor(3);
   gre->SetMarkerColor(3);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_flu_r_sctdisk_21259 = new TH1F("Graph_flu_r_sctdisk_21259","Disk 2",100,340.3119,992.3726);
   Graph_flu_r_sctdisk_21259->SetMinimum(1.162378e-05);
   Graph_flu_r_sctdisk_21259->SetMaximum(0.000680184);
   Graph_flu_r_sctdisk_21259->SetDirectory(0);
   Graph_flu_r_sctdisk_21259->SetStats(0);
   gre->SetHistogram(Graph_flu_r_sctdisk_21259);
   
   multigraph->Add(gre,"lp");
   
   Double_t flu_r_sctdisk_3_fx1260[18] = {
   394.7426,
   416.2264,
   442.6763,
   473.2142,
   499.7383,
   522.1961,
   547.8118,
   567.5052,
   591.9087,
   622.8864,
   654.9446,
   684.1832,
   710.4445,
   739.7549,
   784.5415,
   839.1552,
   887.8798,
   938.096};
   Double_t flu_r_sctdisk_3_fy1260[18] = {
   0.0005486421,
   0.0004495931,
   0.0005460577,
   0.000432414,
   0.0003910371,
   0.0003201559,
   0.0003102391,
   0.000240645,
   0.000239398,
   0.0002880606,
   0.0001918963,
   0.0001513385,
   0.0001668372,
   0.0001075203,
   0.0001075887,
   0.0001112564,
   0.0001474791,
   0.0001040118};
   Double_t flu_r_sctdisk_3_fex1260[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t flu_r_sctdisk_3_fey1260[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   gre = new TGraphErrors(18,flu_r_sctdisk_3_fx1260,flu_r_sctdisk_3_fy1260,flu_r_sctdisk_3_fex1260,flu_r_sctdisk_3_fey1260);
   gre->SetName("flu_r_sctdisk_3");
   gre->SetTitle("Disk 3");
   gre->SetFillColor(1);
   gre->SetLineColor(4);
   gre->SetMarkerColor(4);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_flu_r_sctdisk_31260 = new TH1F("Graph_flu_r_sctdisk_31260","Disk 3",100,340.4072,992.4314);
   Graph_flu_r_sctdisk_31260->SetMinimum(5.954878e-05);
   Graph_flu_r_sctdisk_31260->SetMaximum(0.0005931051);
   Graph_flu_r_sctdisk_31260->SetDirectory(0);
   Graph_flu_r_sctdisk_31260->SetStats(0);
   gre->SetHistogram(Graph_flu_r_sctdisk_31260);
   
   multigraph->Add(gre,"lp");
   
   Double_t flu_r_sctdisk_4_fx1261[18] = {
   394.5669,
   416.1204,
   442.6604,
   473.0742,
   499.4576,
   522.3825,
   548.0878,
   567.7948,
   591.9396,
   622.9161,
   654.9081,
   684.2251,
   710.4995,
   739.6686,
   784.4875,
   839.1482,
   887.9378,
   938.0194};
   Double_t flu_r_sctdisk_4_fy1261[18] = {
   0.000512958,
   0.0004462127,
   0.0004320748,
   0.0003716058,
   0.0003618007,
   0.0002964406,
   0.0003203282,
   0.0002482845,
   0.0002891191,
   0.0002339401,
   0.0001411469,
   0.0001255002,
   0.0001810361,
   0.0001508077,
   0.0001012131,
   0.0001068061,
   8.867411e-05,
   9.078487e-05};
   Double_t flu_r_sctdisk_4_fex1261[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t flu_r_sctdisk_4_fey1261[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   gre = new TGraphErrors(18,flu_r_sctdisk_4_fx1261,flu_r_sctdisk_4_fy1261,flu_r_sctdisk_4_fex1261,flu_r_sctdisk_4_fey1261);
   gre->SetName("flu_r_sctdisk_4");
   gre->SetTitle("Disk 4");
   gre->SetFillColor(1);
   gre->SetLineColor(5);
   gre->SetMarkerColor(5);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_flu_r_sctdisk_41261 = new TH1F("Graph_flu_r_sctdisk_41261","Disk 4",100,340.2216,992.3647);
   Graph_flu_r_sctdisk_41261->SetMinimum(4.624572e-05);
   Graph_flu_r_sctdisk_41261->SetMaximum(0.0005553864);
   Graph_flu_r_sctdisk_41261->SetDirectory(0);
   Graph_flu_r_sctdisk_41261->SetStats(0);
   gre->SetHistogram(Graph_flu_r_sctdisk_41261);
   
   multigraph->Add(gre,"lp");
   
   Double_t flu_r_sctdisk_5_fx1262[18] = {
   394.5584,
   416.0294,
   442.7339,
   473.196,
   499.6534,
   522.3008,
   547.9767,
   567.2534,
   591.9658,
   622.6855,
   654.9569,
   684.1801,
   710.5383,
   739.6921,
   784.5513,
   839.2067,
   887.8656,
   938.0667};
   Double_t flu_r_sctdisk_5_fy1262[18] = {
   0.0004951161,
   0.0004124088,
   0.0004214717,
   0.0003423277,
   0.0004239281,
   0.0002798399,
   0.0002522269,
   0.0002597438,
   0.0002375564,
   0.0003386894,
   0.0002553331,
   0.0001937871,
   0.0002165334,
   0.0001326549,
   0.0001291065,
   0.0001283157,
   0.0001185433,
   0.0001238522};
   Double_t flu_r_sctdisk_5_fex1262[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t flu_r_sctdisk_5_fey1262[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   gre = new TGraphErrors(18,flu_r_sctdisk_5_fx1262,flu_r_sctdisk_5_fy1262,flu_r_sctdisk_5_fex1262,flu_r_sctdisk_5_fey1262);
   gre->SetName("flu_r_sctdisk_5");
   gre->SetTitle("Disk 5");
   gre->SetFillColor(1);
   gre->SetLineColor(6);
   gre->SetMarkerColor(6);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_flu_r_sctdisk_51262 = new TH1F("Graph_flu_r_sctdisk_51262","Disk 5",100,340.2076,992.4175);
   Graph_flu_r_sctdisk_51262->SetMinimum(8.088602e-05);
   Graph_flu_r_sctdisk_51262->SetMaximum(0.0005327733);
   Graph_flu_r_sctdisk_51262->SetDirectory(0);
   Graph_flu_r_sctdisk_51262->SetStats(0);
   gre->SetHistogram(Graph_flu_r_sctdisk_51262);
   
   multigraph->Add(gre,"lp");
   
   Double_t flu_r_sctdisk_6_fx1263[18] = {
   394.5622,
   416.165,
   442.6784,
   473.1169,
   499.8646,
   522.1742,
   547.9354,
   567.6384,
   592.1525,
   622.7692,
   654.9816,
   684.1514,
   710.468,
   739.6739,
   784.5026,
   839.1164,
   887.9125,
   938.0317};
   Double_t flu_r_sctdisk_6_fy1263[18] = {
   0.0004549715,
   0.0004293108,
   0.0004161702,
   0.0004481791,
   0.0002960188,
   0.0003082982,
   0.000262316,
   0.0003246798,
   0.0001823107,
   0.0002671107,
   0.0001522483,
   0.0001181179,
   0.0001490886,
   0.0001494114,
   0.0001099796,
   0.0001483419,
   7.840658e-05,
   7.755794e-05};
   Double_t flu_r_sctdisk_6_fex1263[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t flu_r_sctdisk_6_fey1263[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   gre = new TGraphErrors(18,flu_r_sctdisk_6_fx1263,flu_r_sctdisk_6_fy1263,flu_r_sctdisk_6_fex1263,flu_r_sctdisk_6_fey1263);
   gre->SetName("flu_r_sctdisk_6");
   gre->SetTitle("Disk 0");
   gre->SetFillColor(1);
   gre->SetLineStyle(2);
   gre->SetMarkerStyle(21);
   
   TH1F *Graph_flu_r_sctdisk_61263 = new TH1F("Graph_flu_r_sctdisk_61263","Disk 0",100,340.2152,992.3786);
   Graph_flu_r_sctdisk_61263->SetMinimum(3.981659e-05);
   Graph_flu_r_sctdisk_61263->SetMaximum(0.0004927128);
   Graph_flu_r_sctdisk_61263->SetDirectory(0);
   Graph_flu_r_sctdisk_61263->SetStats(0);
   gre->SetHistogram(Graph_flu_r_sctdisk_61263);
   
   multigraph->Add(gre,"lp");
   
   Double_t flu_r_sctdisk_7_fx1264[18] = {
   394.5466,
   416.3852,
   442.6211,
   473.0388,
   499.6594,
   522.2758,
   547.9081,
   568.0096,
   591.7261,
   623.2017,
   655.0447,
   684.2628,
   710.5269,
   739.8071,
   784.4604,
   839.1193,
   887.8909,
   938.1528};
   Double_t flu_r_sctdisk_7_fy1264[18] = {
   0.0005174185,
   0.0003786048,
   0.0003339965,
   0.0003423277,
   0.0003508371,
   0.0003130413,
   0.0003707736,
   0.0002100869,
   0.0002522886,
   0.0002147361,
   0.0001538342,
   0.0002251622,
   0.0002165334,
   0.0004943142,
   0.0001243247,
   0.0001053227,
   0.0001082758,
   8.477263e-05};
   Double_t flu_r_sctdisk_7_fex1264[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t flu_r_sctdisk_7_fey1264[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   gre = new TGraphErrors(18,flu_r_sctdisk_7_fx1264,flu_r_sctdisk_7_fy1264,flu_r_sctdisk_7_fex1264,flu_r_sctdisk_7_fey1264);
   gre->SetName("flu_r_sctdisk_7");
   gre->SetTitle("Disk 1");
   gre->SetFillColor(1);
   gre->SetLineColor(2);
   gre->SetLineStyle(2);
   gre->SetMarkerColor(2);
   gre->SetMarkerStyle(21);
   
   TH1F *Graph_flu_r_sctdisk_71264 = new TH1F("Graph_flu_r_sctdisk_71264","Disk 1",100,340.186,992.5134);
   Graph_flu_r_sctdisk_71264->SetMinimum(4.150805e-05);
   Graph_flu_r_sctdisk_71264->SetMaximum(0.0005606831);
   Graph_flu_r_sctdisk_71264->SetDirectory(0);
   Graph_flu_r_sctdisk_71264->SetStats(0);
   gre->SetHistogram(Graph_flu_r_sctdisk_71264);
   
   multigraph->Add(gre,"lp");
   
   Double_t flu_r_sctdisk_8_fx1265[18] = {
   394.6206,
   416.2966,
   442.6881,
   473.2151,
   499.6693,
   522.3622,
   547.6646,
   567.9028,
   591.8683,
   622.7546,
   654.9688,
   684.2357,
   710.6269,
   739.7043,
   784.5977,
   839.1431,
   887.8836,
   938.0175};
   Double_t flu_r_sctdisk_8_fy1265[18] = {
   0.0007092202,
   0.0005172011,
   0.0004559317,
   0.0002635023,
   0.0003069824,
   0.000384187,
   0.0002446602,
   0.0002139067,
   0.0002191412,
   0.0002025153,
   0.0001443187,
   0.0002140886,
   0.0001153662,
   0.0001396368,
   0.0001219339,
   9.938906e-05,
   8.77407e-05,
   8.89812e-05};
   Double_t flu_r_sctdisk_8_fex1265[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t flu_r_sctdisk_8_fey1265[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   gre = new TGraphErrors(18,flu_r_sctdisk_8_fx1265,flu_r_sctdisk_8_fy1265,flu_r_sctdisk_8_fex1265,flu_r_sctdisk_8_fey1265);
   gre->SetName("flu_r_sctdisk_8");
   gre->SetTitle("Disk 2");
   gre->SetFillColor(1);
   gre->SetLineColor(3);
   gre->SetLineStyle(2);
   gre->SetMarkerColor(3);
   gre->SetMarkerStyle(21);
   
   TH1F *Graph_flu_r_sctdisk_81265 = new TH1F("Graph_flu_r_sctdisk_81265","Disk 2",100,340.2809,992.3571);
   Graph_flu_r_sctdisk_81265->SetMinimum(2.559275e-05);
   Graph_flu_r_sctdisk_81265->SetMaximum(0.0007713681);
   Graph_flu_r_sctdisk_81265->SetDirectory(0);
   Graph_flu_r_sctdisk_81265->SetStats(0);
   gre->SetHistogram(Graph_flu_r_sctdisk_81265);
   
   multigraph->Add(gre,"lp");
   
   Double_t flu_r_sctdisk_9_fx1266[18] = {
   394.5367,
   416.2726,
   442.5883,
   473.2233,
   499.7676,
   522.1681,
   547.8637,
   567.3957,
   592.2314,
   622.7515,
   654.951,
   684.2245,
   710.4421,
   739.7258,
   784.513,
   839.1268,
   887.8638,
   938.1793};
   Double_t flu_r_sctdisk_9_fy1266[18] = {
   0.0003880639,
   0.0004124088,
   0.0004559317,
   0.000403136,
   0.0003508371,
   0.0002774684,
   0.0003026723,
   0.0001718893,
   0.0002265073,
   0.0003369436,
   0.0002918093,
   0.0001421105,
   0.0001881356,
   0.0001186913,
   0.0001458425,
   0.0001030976,
   0.0001185433,
   7.455182e-05};
   Double_t flu_r_sctdisk_9_fex1266[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t flu_r_sctdisk_9_fey1266[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   gre = new TGraphErrors(18,flu_r_sctdisk_9_fx1266,flu_r_sctdisk_9_fy1266,flu_r_sctdisk_9_fex1266,flu_r_sctdisk_9_fey1266);
   gre->SetName("flu_r_sctdisk_9");
   gre->SetTitle("Disk 3");
   gre->SetFillColor(1);
   gre->SetLineColor(4);
   gre->SetLineStyle(2);
   gre->SetMarkerColor(4);
   gre->SetMarkerStyle(21);
   
   TH1F *Graph_flu_r_sctdisk_91266 = new TH1F("Graph_flu_r_sctdisk_91266","Disk 3",100,340.1724,992.5436);
   Graph_flu_r_sctdisk_91266->SetMinimum(3.641383e-05);
   Graph_flu_r_sctdisk_91266->SetMaximum(0.0004940697);
   Graph_flu_r_sctdisk_91266->SetDirectory(0);
   Graph_flu_r_sctdisk_91266->SetStats(0);
   gre->SetHistogram(Graph_flu_r_sctdisk_91266);
   
   multigraph->Add(gre,"lp");
   
   Double_t flu_r_sctdisk_10_fx1267[18] = {
   394.6073,
   416.2075,
   442.6281,
   473.1593,
   499.8021,
   522.1978,
   547.9536,
   567.8353,
   591.959,
   622.7655,
   654.909,
   684.2789,
   710.5042,
   739.6782,
   784.6115,
   839.1562,
   887.8726,
   938.0632};
   Double_t flu_r_sctdisk_10_fy1267[18] = {
   0.0008608775,
   0.0005002992,
   0.0009781325,
   0.0003693536,
   0.0003471825,
   0.0003462426,
   0.0003001501,
   0.0002330055,
   0.0002099336,
   0.000206007,
   0.00019824,
   0.0001384194,
   0.0001916853,
   0.0001382404,
   0.0001163552,
   9.271366e-05,
   8.867411e-05,
   0.0001154351};
   Double_t flu_r_sctdisk_10_fex1267[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t flu_r_sctdisk_10_fey1267[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   gre = new TGraphErrors(18,flu_r_sctdisk_10_fx1267,flu_r_sctdisk_10_fy1267,flu_r_sctdisk_10_fex1267,flu_r_sctdisk_10_fey1267);
   gre->SetName("flu_r_sctdisk_10");
   gre->SetTitle("Disk 4");
   gre->SetFillColor(1);
   gre->SetLineColor(5);
   gre->SetLineStyle(2);
   gre->SetMarkerColor(5);
   gre->SetMarkerStyle(21);
   
   TH1F *Graph_flu_r_sctdisk_101267 = new TH1F("Graph_flu_r_sctdisk_101267","Disk 4",100,340.2617,992.4088);
   Graph_flu_r_sctdisk_101267->SetMinimum(0);
   Graph_flu_r_sctdisk_101267->SetMaximum(0.001067078);
   Graph_flu_r_sctdisk_101267->SetDirectory(0);
   Graph_flu_r_sctdisk_101267->SetStats(0);
   gre->SetHistogram(Graph_flu_r_sctdisk_101267);
   
   multigraph->Add(gre,"lp");
   
   Double_t flu_r_sctdisk_11_fx1268[18] = {
   394.5892,
   416.188,
   442.7265,
   473.1999,
   499.7158,
   522.3035,
   547.814,
   567.7383,
   591.7271,
   622.9506,
   655.0025,
   684.3044,
   710.5106,
   739.6681,
   784.5076,
   839.1005,
   887.9859,
   938.1379};
   Double_t flu_r_sctdisk_11_fy1268[18] = {
   0.0005754051,
   0.0003109968,
   0.0003896625,
   0.0004797092,
   0.0002229277,
   0.0002513816,
   0.0001664698,
   0.0003246798,
   0.0002909606,
   0.0002828231,
   0.0001696934,
   0.0001937871,
   0.0001384394,
   0.0001577896,
   0.0001490303,
   0.0001119981,
   9.427459e-05,
   6.91408e-05};
   Double_t flu_r_sctdisk_11_fex1268[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t flu_r_sctdisk_11_fey1268[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   gre = new TGraphErrors(18,flu_r_sctdisk_11_fx1268,flu_r_sctdisk_11_fy1268,flu_r_sctdisk_11_fex1268,flu_r_sctdisk_11_fey1268);
   gre->SetName("flu_r_sctdisk_11");
   gre->SetTitle("Disk 5");
   gre->SetFillColor(1);
   gre->SetLineColor(6);
   gre->SetLineStyle(2);
   gre->SetMarkerColor(6);
   gre->SetMarkerStyle(21);
   
   TH1F *Graph_flu_r_sctdisk_111268 = new TH1F("Graph_flu_r_sctdisk_111268","Disk 5",100,340.2343,992.4928);
   Graph_flu_r_sctdisk_111268->SetMinimum(1.851437e-05);
   Graph_flu_r_sctdisk_111268->SetMaximum(0.0006260315);
   Graph_flu_r_sctdisk_111268->SetDirectory(0);
   Graph_flu_r_sctdisk_111268->SetStats(0);
   gre->SetHistogram(Graph_flu_r_sctdisk_111268);
   
   multigraph->Add(gre,"lp");
   multigraph->Draw("Apl");
   multigraph->GetXaxis()->SetTitle("r [mm]");
   multigraph->GetYaxis()->SetTitle("Hits per mm^{2}");
   
   TLegend *leg = new TLegend(0.57,0.62,0.77,0.87,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextFont(62);
   leg->SetTextSize(0.04);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(10);
   leg->SetFillStyle(0);
   TLegendEntry *entry=leg->AddEntry("NULL","Sct Disk (left)","h");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("flu_r_sctdisk_0","Disk 0","lp");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("flu_r_sctdisk_1","Disk 1","lp");
   entry->SetLineColor(2);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(2);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("flu_r_sctdisk_2","Disk 2","lp");
   entry->SetLineColor(3);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(3);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("flu_r_sctdisk_3","Disk 3","lp");
   entry->SetLineColor(4);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(4);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("flu_r_sctdisk_4","Disk 4","lp");
   entry->SetLineColor(5);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(5);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("flu_r_sctdisk_5","Disk 5","lp");
   entry->SetLineColor(6);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(6);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   leg->Draw();
   
   leg = new TLegend(0.7,0.62,0.9,0.87,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextFont(62);
   leg->SetTextSize(0.04);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(10);
   leg->SetFillStyle(0);
   entry=leg->AddEntry("NULL","  (right)","h");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("flu_r_sctdisk_6","Disk 0","lp");
   entry->SetLineColor(1);
   entry->SetLineStyle(2);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("flu_r_sctdisk_7","Disk 1","lp");
   entry->SetLineColor(2);
   entry->SetLineStyle(2);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(2);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("flu_r_sctdisk_8","Disk 2","lp");
   entry->SetLineColor(3);
   entry->SetLineStyle(2);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(3);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("flu_r_sctdisk_9","Disk 3","lp");
   entry->SetLineColor(4);
   entry->SetLineStyle(2);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(4);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("flu_r_sctdisk_10","Disk 4","lp");
   entry->SetLineColor(5);
   entry->SetLineStyle(2);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(5);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("flu_r_sctdisk_11","Disk 5","lp");
   entry->SetLineColor(6);
   entry->SetLineStyle(2);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(6);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   leg->Draw();
   c1_n38->Modified();
   c1_n38->cd();
   c1_n38->SetSelected(c1_n38);
}
