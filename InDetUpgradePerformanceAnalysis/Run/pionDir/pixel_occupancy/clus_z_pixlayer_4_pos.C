void clus_z_pixlayer_4_pos()
{
//=========Macro generated from canvas: c1_n41/c1_n41
//=========  (Tue Apr 19 16:53:28 2016) by ROOT version6.04/12
   TCanvas *c1_n41 = new TCanvas("c1_n41", "c1_n41",0,0,700,500);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   c1_n41->SetHighLightColor(2);
   c1_n41->Range(-40.64496,0.1105933,412.9815,1.836601);
   c1_n41->SetFillColor(0);
   c1_n41->SetBorderMode(0);
   c1_n41->SetBorderSize(2);
   c1_n41->SetTickx(1);
   c1_n41->SetTicky(1);
   c1_n41->SetFrameBorderMode(0);
   c1_n41->SetFrameBorderMode(0);
   
   TMultiGraph *multigraph = new TMultiGraph();
   multigraph->SetName("");
   multigraph->SetTitle("");
   
   Double_t clus_z_pixlayer_4_pos_fx1282[9] = {
   21.2132,
   59.77019,
   101.5238,
   136.9499,
   181.4745,
   226.1201,
   264.993,
   304.296,
   351.1233};
   Double_t clus_z_pixlayer_4_pos_fy1282[9] = {
   1.2,
   0.56,
   1.28,
   0.64,
   0.48,
   0.64,
   0.56,
   0.8,
   0.96};
   Double_t clus_z_pixlayer_4_pos_fex1282[9] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t clus_z_pixlayer_4_pos_fey1282[9] = {
   0.219089,
   0.1496663,
   0.2262742,
   0.16,
   0.1385641,
   0.16,
   0.1496663,
   0.1788854,
   0.1959592};
   TGraphErrors *gre = new TGraphErrors(9,clus_z_pixlayer_4_pos_fx1282,clus_z_pixlayer_4_pos_fy1282,clus_z_pixlayer_4_pos_fex1282,clus_z_pixlayer_4_pos_fey1282);
   gre->SetName("clus_z_pixlayer_4_pos");
   gre->SetTitle("Layer 4 (z>0)");
   
   TH1F *Graph_clus_z_pixlayer_4_pos1282 = new TH1F("Graph_clus_z_pixlayer_4_pos1282","Layer 4 (z>0)",100,0,384.1143);
   Graph_clus_z_pixlayer_4_pos1282->SetMinimum(0.2249521);
   Graph_clus_z_pixlayer_4_pos1282->SetMaximum(1.622758);
   Graph_clus_z_pixlayer_4_pos1282->SetDirectory(0);
   Graph_clus_z_pixlayer_4_pos1282->SetStats(0);
   gre->SetHistogram(Graph_clus_z_pixlayer_4_pos1282);
   
   multigraph->Add(gre,"");
   multigraph->Draw("Apl");
   
   TLegend *leg = new TLegend(0.48,0.65,0.9,0.9,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextFont(62);
   leg->SetTextSize(0.04);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(10);
   leg->SetFillStyle(0);
   TLegendEntry *entry=leg->AddEntry("clus_z_pixlayer_4_pos","Pixel Occupancies","lp");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(1);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   leg->Draw();
   c1_n41->Modified();
   c1_n41->cd();
   c1_n41->SetSelected(c1_n41);
}
