void clus_pixlayer_mgz()
{
//=========Macro generated from canvas: c1_n4/c1_n4
//=========  (Tue Apr 19 16:53:22 2016) by ROOT version6.04/12
   TCanvas *c1_n4 = new TCanvas("c1_n4", "c1_n4",0,0,700,500);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   c1_n4->SetHighLightColor(2);
   c1_n4->Range(-806.506,-0.2290575,809.7271,3.749138);
   c1_n4->SetFillColor(0);
   c1_n4->SetBorderMode(0);
   c1_n4->SetBorderSize(2);
   c1_n4->SetTickx(1);
   c1_n4->SetTicky(1);
   c1_n4->SetFrameBorderMode(0);
   c1_n4->SetFrameBorderMode(0);
   
   TMultiGraph *multigraph = new TMultiGraph();
   multigraph->SetName("clus_pixlayer_mgz");
   multigraph->SetTitle("");
   
   Double_t clus_z_pixlayer_0_fx1016[30] = {
   -580.9996,
   -548.7394,
   -509.2677,
   -464.9046,
   -428.8747,
   -383.9047,
   -342.3423,
   -302.4016,
   -262.3781,
   -221.7947,
   -180.7514,
   -141.6643,
   -104.8999,
   -56.21987,
   -18.96358,
   22.91197,
   62.09036,
   94.89594,
   142.7016,
   182.8834,
   224.587,
   262.6204,
   298.9709,
   349.4744,
   387.2241,
   427.0578,
   466.9356,
   506.2732,
   550.659,
   589.0854};
   Double_t clus_z_pixlayer_0_fy1016[30] = {
   1.92,
   2,
   2,
   1.28,
   1.16,
   0.84,
   0.92,
   0.6,
   1.44,
   0.64,
   1.4,
   1.44,
   0.8,
   1.24,
   0.72,
   0.92,
   1,
   0.88,
   0.92,
   1.16,
   1.36,
   1.32,
   0.88,
   1.12,
   1.44,
   1.04,
   0.56,
   1.44,
   0.88,
   0.76};
   Double_t clus_z_pixlayer_0_fex1016[30] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t clus_z_pixlayer_0_fey1016[30] = {
   0.2771281,
   0.2828427,
   0.2828427,
   0.2262742,
   0.2154066,
   0.183303,
   0.1918333,
   0.1549193,
   0.24,
   0.16,
   0.2366432,
   0.24,
   0.1788854,
   0.2227106,
   0.1697056,
   0.1918333,
   0.2,
   0.1876166,
   0.1918333,
   0.2154066,
   0.2332381,
   0.2297825,
   0.1876166,
   0.2116601,
   0.24,
   0.2039608,
   0.1496663,
   0.24,
   0.1876166,
   0.174356};
   TGraphErrors *gre = new TGraphErrors(30,clus_z_pixlayer_0_fx1016,clus_z_pixlayer_0_fy1016,clus_z_pixlayer_0_fex1016,clus_z_pixlayer_0_fey1016);
   gre->SetName("clus_z_pixlayer_0");
   gre->SetTitle("Layer 0");
   gre->SetFillColor(1);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_clus_z_pixlayer_01016 = new TH1F("Graph_clus_z_pixlayer_01016","Layer 0",100,-698.0081,706.094);
   Graph_clus_z_pixlayer_01016->SetMinimum(0.2230828);
   Graph_clus_z_pixlayer_01016->SetMaximum(2.470094);
   Graph_clus_z_pixlayer_01016->SetDirectory(0);
   Graph_clus_z_pixlayer_01016->SetStats(0);
   gre->SetHistogram(Graph_clus_z_pixlayer_01016);
   
   multigraph->Add(gre,"lp");
   
   Double_t clus_z_pixlayer_1_fx1017[30] = {
   -586.1105,
   -550.5223,
   -508.3498,
   -467.2518,
   -422.5674,
   -381.4763,
   -343.2574,
   -311.1511,
   -261.1222,
   -227.0084,
   -181.0954,
   -141.1308,
   -101.6735,
   -65.6633,
   -21.85734,
   15.50172,
   58.21874,
   98.17815,
   141.0817,
   183.8513,
   224.0543,
   264.0359,
   306.1262,
   344.713,
   384.5615,
   423.1552,
   465.9757,
   508.4945,
   546.4675,
   589.3317};
   Double_t clus_z_pixlayer_1_fy1017[30] = {
   1.6,
   1.6,
   1.32,
   1.52,
   1.52,
   0.8,
   0.8,
   1.24,
   0.4,
   0.64,
   1.8,
   1.44,
   0.84,
   1.04,
   2.08,
   1.48,
   0.92,
   0.64,
   0.64,
   0.6,
   1.24,
   1.4,
   0.84,
   0.92,
   1,
   1.32,
   1.4,
   1.96,
   1.28,
   1.04};
   Double_t clus_z_pixlayer_1_fex1017[30] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t clus_z_pixlayer_1_fey1017[30] = {
   0.2529822,
   0.2529822,
   0.2297825,
   0.2465766,
   0.2465766,
   0.1788854,
   0.1788854,
   0.2227106,
   0.1264911,
   0.16,
   0.2683282,
   0.24,
   0.183303,
   0.2039608,
   0.2884441,
   0.2433105,
   0.1918333,
   0.16,
   0.16,
   0.1549193,
   0.2227106,
   0.2366432,
   0.183303,
   0.1918333,
   0.2,
   0.2297825,
   0.2366432,
   0.28,
   0.2262742,
   0.2039608};
   gre = new TGraphErrors(30,clus_z_pixlayer_1_fx1017,clus_z_pixlayer_1_fy1017,clus_z_pixlayer_1_fex1017,clus_z_pixlayer_1_fey1017);
   gre->SetName("clus_z_pixlayer_1");
   gre->SetTitle("Layer 1");
   gre->SetFillColor(1);
   gre->SetLineColor(2);
   gre->SetMarkerColor(2);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_clus_z_pixlayer_11017 = new TH1F("Graph_clus_z_pixlayer_11017","Layer 1",100,-703.6548,706.8759);
   Graph_clus_z_pixlayer_11017->SetMinimum(0.06401539);
   Graph_clus_z_pixlayer_11017->SetMaximum(2.577938);
   Graph_clus_z_pixlayer_11017->SetDirectory(0);
   Graph_clus_z_pixlayer_11017->SetStats(0);
   gre->SetHistogram(Graph_clus_z_pixlayer_11017);
   
   multigraph->Add(gre,"lp");
   
   Double_t clus_z_pixlayer_2_fx1018[18] = {
   -347.2089,
   -301.9579,
   -262.062,
   -221.7359,
   -181.9033,
   -143.3988,
   -99.77371,
   -56.6355,
   -19.54092,
   24.59268,
   60.64408,
   101.2142,
   145.3979,
   184.559,
   220.7629,
   270.3661,
   301.6473,
   342.4056};
   Double_t clus_z_pixlayer_2_fy1018[18] = {
   1.24,
   0.92,
   1.92,
   0.92,
   1.24,
   0.76,
   1.04,
   1.4,
   1.68,
   1.56,
   0.44,
   0.64,
   1.16,
   0.92,
   1.08,
   0.92,
   0.72,
   0.56};
   Double_t clus_z_pixlayer_2_fex1018[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t clus_z_pixlayer_2_fey1018[18] = {
   0.2227106,
   0.1918333,
   0.2771281,
   0.1918333,
   0.2227106,
   0.174356,
   0.2039608,
   0.2366432,
   0.2592296,
   0.2497999,
   0.132665,
   0.16,
   0.2154066,
   0.1918333,
   0.2078461,
   0.1918333,
   0.1697056,
   0.1496663};
   gre = new TGraphErrors(18,clus_z_pixlayer_2_fx1018,clus_z_pixlayer_2_fy1018,clus_z_pixlayer_2_fex1018,clus_z_pixlayer_2_fey1018);
   gre->SetName("clus_z_pixlayer_2");
   gre->SetTitle("Layer 2");
   gre->SetFillColor(1);
   gre->SetLineColor(3);
   gre->SetMarkerColor(3);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_clus_z_pixlayer_21018 = new TH1F("Graph_clus_z_pixlayer_21018","Layer 2",100,-416.1703,411.367);
   Graph_clus_z_pixlayer_21018->SetMinimum(0.1183557);
   Graph_clus_z_pixlayer_21018->SetMaximum(2.386108);
   Graph_clus_z_pixlayer_21018->SetDirectory(0);
   Graph_clus_z_pixlayer_21018->SetStats(0);
   gre->SetHistogram(Graph_clus_z_pixlayer_21018);
   
   multigraph->Add(gre,"lp");
   
   Double_t clus_z_pixlayer_3_fx1019[18] = {
   -343.933,
   -303.2109,
   -263.2148,
   -227.772,
   -180.9658,
   -142.8276,
   -100.2237,
   -66.10307,
   -21.77124,
   21.24817,
   61.7226,
   103.5666,
   140.7037,
   187.2653,
   227.6289,
   261.2863,
   305.2266,
   347.7696};
   Double_t clus_z_pixlayer_3_fy1019[18] = {
   0.92,
   0.88,
   1.04,
   0.64,
   0.88,
   1.04,
   0.48,
   0.64,
   1.08,
   1,
   0.88,
   0.92,
   0.6,
   1.08,
   1.16,
   0.8,
   0.8,
   1.48};
   Double_t clus_z_pixlayer_3_fex1019[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t clus_z_pixlayer_3_fey1019[18] = {
   0.1918333,
   0.1876166,
   0.2039608,
   0.16,
   0.1876166,
   0.2039608,
   0.1385641,
   0.16,
   0.2078461,
   0.2,
   0.1876166,
   0.1918333,
   0.1549193,
   0.2078461,
   0.2154066,
   0.1788854,
   0.1788854,
   0.2433105};
   gre = new TGraphErrors(18,clus_z_pixlayer_3_fx1019,clus_z_pixlayer_3_fy1019,clus_z_pixlayer_3_fex1019,clus_z_pixlayer_3_fey1019);
   gre->SetName("clus_z_pixlayer_3");
   gre->SetTitle("Layer 3");
   gre->SetFillColor(1);
   gre->SetLineColor(4);
   gre->SetMarkerColor(4);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_clus_z_pixlayer_31019 = new TH1F("Graph_clus_z_pixlayer_31019","Layer 3",100,-413.1033,416.9399);
   Graph_clus_z_pixlayer_31019->SetMinimum(0.2032485);
   Graph_clus_z_pixlayer_31019->SetMaximum(1.861498);
   Graph_clus_z_pixlayer_31019->SetDirectory(0);
   Graph_clus_z_pixlayer_31019->SetStats(0);
   gre->SetHistogram(Graph_clus_z_pixlayer_31019);
   
   multigraph->Add(gre,"lp");
   
   Double_t clus_z_pixlayer_4_fx1020[18] = {
   -346.1445,
   -304.4877,
   -258.5805,
   -221.5823,
   -183.6958,
   -141.8354,
   -101.0585,
   -60.60258,
   -22.4814,
   21.2132,
   59.77019,
   101.5238,
   136.9499,
   181.4745,
   226.1201,
   264.993,
   304.296,
   351.1233};
   Double_t clus_z_pixlayer_4_fy1020[18] = {
   0.84,
   0.96,
   1.2,
   0.96,
   0.84,
   0.72,
   1.52,
   1.92,
   1.76,
   1.2,
   0.56,
   1.28,
   0.64,
   0.48,
   0.64,
   0.56,
   0.8,
   0.96};
   Double_t clus_z_pixlayer_4_fex1020[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t clus_z_pixlayer_4_fey1020[18] = {
   0.183303,
   0.1959592,
   0.219089,
   0.1959592,
   0.183303,
   0.1697056,
   0.2465766,
   0.2771281,
   0.26533,
   0.219089,
   0.1496663,
   0.2262742,
   0.16,
   0.1385641,
   0.16,
   0.1496663,
   0.1788854,
   0.1959592};
   gre = new TGraphErrors(18,clus_z_pixlayer_4_fx1020,clus_z_pixlayer_4_fy1020,clus_z_pixlayer_4_fex1020,clus_z_pixlayer_4_fey1020);
   gre->SetName("clus_z_pixlayer_4");
   gre->SetTitle("Layer 4");
   gre->SetFillColor(1);
   gre->SetLineColor(5);
   gre->SetMarkerColor(5);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_clus_z_pixlayer_41020 = new TH1F("Graph_clus_z_pixlayer_41020","Layer 4",100,-415.8712,420.8501);
   Graph_clus_z_pixlayer_41020->SetMinimum(0.1558667);
   Graph_clus_z_pixlayer_41020->SetMaximum(2.382697);
   Graph_clus_z_pixlayer_41020->SetDirectory(0);
   Graph_clus_z_pixlayer_41020->SetStats(0);
   gre->SetHistogram(Graph_clus_z_pixlayer_41020);
   
   multigraph->Add(gre,"lp");
   multigraph->Draw("Apl");
   multigraph->GetXaxis()->SetTitle("z [mm]");
   multigraph->GetYaxis()->SetTitle("Cluster per etaModuleID");
   
   TLegend *leg = new TLegend(0.67,0.45,0.9,0.7,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextFont(62);
   leg->SetTextSize(0.04);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(10);
   leg->SetFillStyle(0);
   TLegendEntry *entry=leg->AddEntry("NULL","Pixel Barrel","h");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("clus_z_pixlayer_0","Layer 0","lp");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("clus_z_pixlayer_1","Layer 1","lp");
   entry->SetLineColor(2);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(2);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("clus_z_pixlayer_2","Layer 2","lp");
   entry->SetLineColor(3);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(3);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("clus_z_pixlayer_3","Layer 3","lp");
   entry->SetLineColor(4);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(4);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("clus_z_pixlayer_4","Layer 4","lp");
   entry->SetLineColor(5);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(5);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   leg->Draw();
   c1_n4->Modified();
   c1_n4->cd();
   c1_n4->SetSelected(c1_n4);
}
