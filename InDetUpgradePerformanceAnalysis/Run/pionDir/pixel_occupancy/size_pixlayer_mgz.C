void size_pixlayer_mgz()
{
//=========Macro generated from canvas: c1_n11/c1_n11
//=========  (Tue Apr 19 16:53:23 2016) by ROOT version6.04/12
   TCanvas *c1_n11 = new TCanvas("c1_n11", "c1_n11",0,0,700,500);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   c1_n11->SetHighLightColor(2);
   c1_n11->Range(-806.506,-6.379268,809.7271,60.31919);
   c1_n11->SetFillColor(0);
   c1_n11->SetBorderMode(0);
   c1_n11->SetBorderSize(2);
   c1_n11->SetTickx(1);
   c1_n11->SetTicky(1);
   c1_n11->SetFrameBorderMode(0);
   c1_n11->SetFrameBorderMode(0);
   
   TMultiGraph *multigraph = new TMultiGraph();
   multigraph->SetName("size_pixlayer_mgz");
   multigraph->SetTitle("");
   
   Double_t size_z_pixlayer_0_fx1056[30] = {
   -580.9996,
   -548.7394,
   -509.2677,
   -464.9046,
   -428.8747,
   -383.9047,
   -342.3423,
   -302.4016,
   -262.3781,
   -221.7947,
   -180.7514,
   -141.6643,
   -104.8999,
   -56.21987,
   -18.96358,
   22.91197,
   62.09036,
   94.89594,
   142.7016,
   182.8834,
   224.587,
   262.6204,
   298.9709,
   349.4744,
   387.2241,
   427.0578,
   466.9356,
   506.2732,
   550.659,
   589.0854};
   Double_t size_z_pixlayer_0_fy1056[30] = {
   24.0625,
   24.08,
   25.52,
   19.65625,
   17.13793,
   17.61905,
   16.39131,
   22.13333,
   15.97222,
   11.0625,
   13.57143,
   9.13889,
   10.15,
   8.193548,
   3.666667,
   4.086957,
   6.4,
   6,
   7.956522,
   14.06897,
   14.88235,
   24.93939,
   15.68182,
   15.42857,
   18.38889,
   21.46154,
   26.92857,
   23.16667,
   15.13636,
   31.73684};
   Double_t size_z_pixlayer_0_fex1056[30] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t size_z_pixlayer_0_fey1056[30] = {
   4.703195,
   3.671828,
   3.408541,
   3.817302,
   3.731019,
   4.518468,
   3.042998,
   3.881417,
   2.70962,
   2.56006,
   1.492438,
   1.240298,
   1.036885,
   0.8391231,
   0.4714046,
   0.8686754,
   1.066146,
   1.085122,
   1.161773,
   1.950073,
   1.823184,
   2.313064,
   2.26911,
   2.654949,
   2.631397,
   3.527139,
   5.350839,
   4.399044,
   3.674703,
   6.070657};
   TGraphErrors *gre = new TGraphErrors(30,size_z_pixlayer_0_fx1056,size_z_pixlayer_0_fy1056,size_z_pixlayer_0_fex1056,size_z_pixlayer_0_fey1056);
   gre->SetName("size_z_pixlayer_0");
   gre->SetTitle("Layer 0");
   gre->SetFillColor(1);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_size_z_pixlayer_01056 = new TH1F("Graph_size_z_pixlayer_01056","Layer 0",100,-698.0081,706.094);
   Graph_size_z_pixlayer_01056->SetMinimum(0);
   Graph_size_z_pixlayer_01056->SetMaximum(41.26872);
   Graph_size_z_pixlayer_01056->SetDirectory(0);
   Graph_size_z_pixlayer_01056->SetStats(0);
   gre->SetHistogram(Graph_size_z_pixlayer_01056);
   
   multigraph->Add(gre,"lp");
   
   Double_t size_z_pixlayer_1_fx1057[30] = {
   -586.1105,
   -550.5223,
   -508.3498,
   -467.2518,
   -422.5674,
   -381.4763,
   -343.2574,
   -311.1511,
   -261.1222,
   -227.0084,
   -181.0954,
   -141.1308,
   -101.6735,
   -65.6633,
   -21.85734,
   15.50172,
   58.21874,
   98.17815,
   141.0817,
   183.8513,
   224.0543,
   264.0359,
   306.1262,
   344.713,
   384.5615,
   423.1552,
   465.9757,
   508.4945,
   546.4675,
   589.3317};
   Double_t size_z_pixlayer_1_fy1057[30] = {
   18.575,
   19.65,
   16.72727,
   10.05263,
   11.05263,
   8.55,
   12.9,
   12.87097,
   16.5,
   10.5625,
   8.488889,
   7.444444,
   4.857143,
   7.076923,
   4.096154,
   4.108108,
   3,
   4.6875,
   7.5,
   8.933332,
   8.419355,
   9.342855,
   13.33333,
   15.13043,
   9.68,
   17.06061,
   18.97143,
   16.93877,
   19,
   17.34615};
   Double_t size_z_pixlayer_1_fex1057[30] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t size_z_pixlayer_1_fey1057[30] = {
   2.199618,
   2.15417,
   3.344,
   1.448474,
   1.506245,
   1.371659,
   1.299595,
   1.326116,
   3.493645,
   1.072259,
   0.6783984,
   0.6985751,
   0.6183146,
   0.6992812,
   0.3519603,
   0.586105,
   0.3665888,
   0.6690713,
   1.008299,
   0.9732941,
   0.8820482,
   0.9838552,
   1.335118,
   1.553148,
   1.528311,
   1.833991,
   2.083745,
   2.010899,
   2.203827,
   2.550983};
   gre = new TGraphErrors(30,size_z_pixlayer_1_fx1057,size_z_pixlayer_1_fy1057,size_z_pixlayer_1_fex1057,size_z_pixlayer_1_fey1057);
   gre->SetName("size_z_pixlayer_1");
   gre->SetTitle("Layer 1");
   gre->SetFillColor(1);
   gre->SetLineColor(2);
   gre->SetMarkerColor(2);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_size_z_pixlayer_11057 = new TH1F("Graph_size_z_pixlayer_11057","Layer 1",100,-703.6548,706.8759);
   Graph_size_z_pixlayer_11057->SetMinimum(0.7163354);
   Graph_size_z_pixlayer_11057->SetMaximum(23.72125);
   Graph_size_z_pixlayer_11057->SetDirectory(0);
   Graph_size_z_pixlayer_11057->SetStats(0);
   gre->SetHistogram(Graph_size_z_pixlayer_11057);
   
   multigraph->Add(gre,"lp");
   
   Double_t size_z_pixlayer_2_fx1058[18] = {
   -347.2089,
   -301.9579,
   -262.062,
   -221.7359,
   -181.9033,
   -143.3988,
   -99.77371,
   -56.6355,
   -19.54092,
   24.59268,
   60.64408,
   101.2142,
   145.3979,
   184.559,
   220.7629,
   270.3661,
   301.6473,
   342.4056};
   Double_t size_z_pixlayer_2_fy1058[18] = {
   7.83871,
   7.695652,
   6,
   5.782608,
   7.354838,
   5,
   4.576923,
   4.2,
   4.428572,
   4.153846,
   3.454545,
   3.875,
   5.206897,
   5.521739,
   7.925926,
   5.565217,
   6.722223,
   8.285714};
   Double_t size_z_pixlayer_2_fex1058[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t size_z_pixlayer_2_fey1058[18] = {
   0.6832824,
   0.8789025,
   0.5465884,
   0.6379206,
   1.098923,
   0.5514468,
   0.2996053,
   0.5647933,
   0.626684,
   0.7883587,
   0.4545455,
   0.4643544,
   0.3268724,
   0.6825596,
   1.717122,
   0.7108033,
   0.9281564,
   0.9342598};
   gre = new TGraphErrors(18,size_z_pixlayer_2_fx1058,size_z_pixlayer_2_fy1058,size_z_pixlayer_2_fex1058,size_z_pixlayer_2_fey1058);
   gre->SetName("size_z_pixlayer_2");
   gre->SetTitle("Layer 2");
   gre->SetFillColor(1);
   gre->SetLineColor(3);
   gre->SetMarkerColor(3);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_size_z_pixlayer_21058 = new TH1F("Graph_size_z_pixlayer_21058","Layer 2",100,-416.1703,411.367);
   Graph_size_z_pixlayer_21058->SetMinimum(2.335695);
   Graph_size_z_pixlayer_21058->SetMaximum(10.30735);
   Graph_size_z_pixlayer_21058->SetDirectory(0);
   Graph_size_z_pixlayer_21058->SetStats(0);
   gre->SetHistogram(Graph_size_z_pixlayer_21058);
   
   multigraph->Add(gre,"lp");
   
   Double_t size_z_pixlayer_3_fx1059[18] = {
   -343.933,
   -303.2109,
   -263.2148,
   -227.772,
   -180.9658,
   -142.8276,
   -100.2237,
   -66.10307,
   -21.77124,
   21.24817,
   61.7226,
   103.5666,
   140.7037,
   187.2653,
   227.6289,
   261.2863,
   305.2266,
   347.7696};
   Double_t size_z_pixlayer_3_fy1059[18] = {
   5.565217,
   6.636364,
   5.692308,
   4.6875,
   5.045455,
   4.346154,
   2.333333,
   3.0625,
   2.629629,
   2.56,
   2.727273,
   2.826087,
   4,
   3.740741,
   5.862069,
   4.9,
   5.05,
   6.297297};
   Double_t size_z_pixlayer_3_fex1059[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t size_z_pixlayer_3_fey1059[18] = {
   0.3815202,
   0.6498267,
   0.5512761,
   0.6501202,
   0.5242074,
   0.3880615,
   0.2562354,
   0.2953635,
   0.2144904,
   0.3919184,
   0.2099455,
   0.2322954,
   0.4140393,
   0.4015376,
   0.733982,
   0.4285839,
   0.5914968,
   0.7492387};
   gre = new TGraphErrors(18,size_z_pixlayer_3_fx1059,size_z_pixlayer_3_fy1059,size_z_pixlayer_3_fex1059,size_z_pixlayer_3_fey1059);
   gre->SetName("size_z_pixlayer_3");
   gre->SetTitle("Layer 3");
   gre->SetFillColor(1);
   gre->SetLineColor(4);
   gre->SetMarkerColor(4);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_size_z_pixlayer_31059 = new TH1F("Graph_size_z_pixlayer_31059","Layer 3",100,-413.1033,416.9399);
   Graph_size_z_pixlayer_31059->SetMinimum(1.556189);
   Graph_size_z_pixlayer_31059->SetMaximum(7.8071);
   Graph_size_z_pixlayer_31059->SetDirectory(0);
   Graph_size_z_pixlayer_31059->SetStats(0);
   gre->SetHistogram(Graph_size_z_pixlayer_31059);
   
   multigraph->Add(gre,"lp");
   
   Double_t size_z_pixlayer_4_fx1060[18] = {
   -346.1445,
   -304.4877,
   -258.5805,
   -221.5823,
   -183.6958,
   -141.8354,
   -101.0585,
   -60.60258,
   -22.4814,
   21.2132,
   59.77019,
   101.5238,
   136.9499,
   181.4745,
   226.1201,
   264.993,
   304.296,
   351.1233};
   Double_t size_z_pixlayer_4_fy1060[18] = {
   5.047619,
   4.916667,
   5,
   4.208333,
   3.857143,
   3.388889,
   3.578948,
   2.895833,
   3.613637,
   2.633333,
   2.642857,
   3.65625,
   3.25,
   5.333333,
   4.6875,
   4.928571,
   6.1,
   6.458333};
   Double_t size_z_pixlayer_4_fex1060[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t size_z_pixlayer_4_fey1060[18] = {
   1.09648,
   0.4252734,
   0.4889585,
   0.3660362,
   0.2869023,
   0.3888889,
   0.2936783,
   0.2131318,
   0.2762936,
   0.2690269,
   0.3075451,
   0.3032398,
   0.3095696,
   0.6666666,
   0.1983001,
   0.6501418,
   0.6194224,
   0.4422405};
   gre = new TGraphErrors(18,size_z_pixlayer_4_fx1060,size_z_pixlayer_4_fy1060,size_z_pixlayer_4_fex1060,size_z_pixlayer_4_fey1060);
   gre->SetName("size_z_pixlayer_4");
   gre->SetTitle("Layer 4");
   gre->SetFillColor(1);
   gre->SetLineColor(5);
   gre->SetMarkerColor(5);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_size_z_pixlayer_41060 = new TH1F("Graph_size_z_pixlayer_41060","Layer 4",100,-415.8712,420.8501);
   Graph_size_z_pixlayer_41060->SetMinimum(1.878786);
   Graph_size_z_pixlayer_41060->SetMaximum(7.3571);
   Graph_size_z_pixlayer_41060->SetDirectory(0);
   Graph_size_z_pixlayer_41060->SetStats(0);
   gre->SetHistogram(Graph_size_z_pixlayer_41060);
   
   multigraph->Add(gre,"lp");
   multigraph->Draw("Apl");
   multigraph->GetXaxis()->SetTitle("z [mm]");
   multigraph->GetYaxis()->SetTitle("Cluster size");
   
   TLegend *leg = new TLegend(0.61,0.7,0.81,0.85,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextFont(62);
   leg->SetTextSize(0.04);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(10);
   leg->SetFillStyle(0);
   TLegendEntry *entry=leg->AddEntry("NULL","Pixel Barrel","h");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("size_z_pixlayer_0","Layer 0","lp");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("size_z_pixlayer_1","Layer 1","lp");
   entry->SetLineColor(2);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(2);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("size_z_pixlayer_2","Layer 2","lp");
   entry->SetLineColor(3);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(3);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("size_z_pixlayer_3","Layer 3","lp");
   entry->SetLineColor(4);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(4);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("size_z_pixlayer_4","Layer 4","lp");
   entry->SetLineColor(5);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(5);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   leg->Draw();
   c1_n11->Modified();
   c1_n11->cd();
   c1_n11->SetSelected(c1_n11);
}
