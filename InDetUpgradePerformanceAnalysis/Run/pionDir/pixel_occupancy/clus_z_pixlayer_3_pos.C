void clus_z_pixlayer_3_pos()
{
//=========Macro generated from canvas: c1_n44/c1_n44
//=========  (Tue Apr 19 16:53:28 2016) by ROOT version6.04/12
   TCanvas *c1_n44 = new TCanvas("c1_n44", "c1_n44",0,0,700,500);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   c1_n44->SetHighLightColor(2);
   c1_n44->Range(-39.97461,0.1883153,408.9924,2.116854);
   c1_n44->SetFillColor(0);
   c1_n44->SetBorderMode(0);
   c1_n44->SetBorderSize(2);
   c1_n44->SetTickx(1);
   c1_n44->SetTicky(1);
   c1_n44->SetFrameBorderMode(0);
   c1_n44->SetFrameBorderMode(0);
   
   TMultiGraph *multigraph = new TMultiGraph();
   multigraph->SetName("");
   multigraph->SetTitle("");
   
   Double_t clus_z_pixlayer_3_pos_fx1292[9] = {
   21.24817,
   61.7226,
   103.5666,
   140.7037,
   187.2653,
   227.6289,
   261.2863,
   305.2266,
   347.7696};
   Double_t clus_z_pixlayer_3_pos_fy1292[9] = {
   1,
   0.88,
   0.92,
   0.6,
   1.08,
   1.16,
   0.8,
   0.8,
   1.48};
   Double_t clus_z_pixlayer_3_pos_fex1292[9] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t clus_z_pixlayer_3_pos_fey1292[9] = {
   0.2,
   0.1876166,
   0.1918333,
   0.1549193,
   0.2078461,
   0.2154066,
   0.1788854,
   0.1788854,
   0.2433105};
   TGraphErrors *gre = new TGraphErrors(9,clus_z_pixlayer_3_pos_fx1292,clus_z_pixlayer_3_pos_fy1292,clus_z_pixlayer_3_pos_fex1292,clus_z_pixlayer_3_pos_fey1292);
   gre->SetName("clus_z_pixlayer_3_pos");
   gre->SetTitle("Layer 3 (z>0)");
   
   TH1F *Graph_clus_z_pixlayer_3_pos1292 = new TH1F("Graph_clus_z_pixlayer_3_pos1292","Layer 3 (z>0)",100,0,380.4218);
   Graph_clus_z_pixlayer_3_pos1292->SetMinimum(0.3172577);
   Graph_clus_z_pixlayer_3_pos1292->SetMaximum(1.851133);
   Graph_clus_z_pixlayer_3_pos1292->SetDirectory(0);
   Graph_clus_z_pixlayer_3_pos1292->SetStats(0);
   gre->SetHistogram(Graph_clus_z_pixlayer_3_pos1292);
   
   multigraph->Add(gre,"");
   multigraph->Draw("Apl");
   
   TLegend *leg = new TLegend(0.48,0.65,0.9,0.9,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextFont(62);
   leg->SetTextSize(0.04);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(10);
   leg->SetFillStyle(0);
   TLegendEntry *entry=leg->AddEntry("clus_z_pixlayer_3_pos","Pixel Occupancies","lp");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(1);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   leg->Draw();
   c1_n44->Modified();
   c1_n44->cd();
   c1_n44->SetSelected(c1_n44);
}
