void occ_pixdisk_mg()
{
//=========Macro generated from canvas: c1_n12/c1_n12
//=========  (Tue Apr 19 16:53:23 2016) by ROOT version6.04/12
   TCanvas *c1_n12 = new TCanvas("c1_n12", "c1_n12",0,0,700,500);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   c1_n12->SetHighLightColor(2);
   c1_n12->Range(-0.5625,-3.185619e-05,3.5625,0.0003233454);
   c1_n12->SetFillColor(0);
   c1_n12->SetBorderMode(0);
   c1_n12->SetBorderSize(2);
   c1_n12->SetTickx(1);
   c1_n12->SetTicky(1);
   c1_n12->SetFrameBorderMode(0);
   c1_n12->SetFrameBorderMode(0);
   
   TMultiGraph *multigraph = new TMultiGraph();
   multigraph->SetName("occ_pixdisk_mg");
   multigraph->SetTitle("");
   
   Double_t occ_pixdisk_0_fx1061[4] = {
   0,
   1,
   2,
   3};
   Double_t occ_pixdisk_0_fy1061[4] = {
   0.0001463294,
   0.0001912822,
   0.0002145337,
   0.0001968626};
   Double_t occ_pixdisk_0_fex1061[4] = {
   0,
   0,
   0,
   0};
   Double_t occ_pixdisk_0_fey1061[4] = {
   2.267454e-08,
   3.725559e-08,
   4.329686e-08,
   2.962192e-08};
   TGraphErrors *gre = new TGraphErrors(4,occ_pixdisk_0_fx1061,occ_pixdisk_0_fy1061,occ_pixdisk_0_fex1061,occ_pixdisk_0_fey1061);
   gre->SetName("occ_pixdisk_0");
   gre->SetTitle("Disk 0");
   gre->SetFillColor(1);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_occ_pixdisk_01061 = new TH1F("Graph_occ_pixdisk_01061","Disk 0",100,0,3.3);
   Graph_occ_pixdisk_01061->SetMinimum(0.0001394797);
   Graph_occ_pixdisk_01061->SetMaximum(0.0002214041);
   Graph_occ_pixdisk_01061->SetDirectory(0);
   Graph_occ_pixdisk_01061->SetStats(0);
   gre->SetHistogram(Graph_occ_pixdisk_01061);
   
   multigraph->Add(gre,"lp");
   
   Double_t occ_pixdisk_1_fx1062[4] = {
   0,
   1,
   2,
   3};
   Double_t occ_pixdisk_1_fy1062[4] = {
   6.097057e-05,
   3.59623e-05,
   4.856977e-05,
   5.14633e-05};
   Double_t occ_pixdisk_1_fex1062[4] = {
   0,
   0,
   0,
   0};
   Double_t occ_pixdisk_1_fey1062[4] = {
   5.914583e-08,
   3.348671e-08,
   5.377384e-08,
   3.351878e-08};
   gre = new TGraphErrors(4,occ_pixdisk_1_fx1062,occ_pixdisk_1_fy1062,occ_pixdisk_1_fex1062,occ_pixdisk_1_fey1062);
   gre->SetName("occ_pixdisk_1");
   gre->SetTitle("Disk 1");
   gre->SetFillColor(1);
   gre->SetLineColor(2);
   gre->SetMarkerColor(2);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_occ_pixdisk_11062 = new TH1F("Graph_occ_pixdisk_11062","Disk 1",100,0,3.3);
   Graph_occ_pixdisk_11062->SetMinimum(3.341872e-05);
   Graph_occ_pixdisk_11062->SetMaximum(6.353981e-05);
   Graph_occ_pixdisk_11062->SetDirectory(0);
   Graph_occ_pixdisk_11062->SetStats(0);
   gre->SetHistogram(Graph_occ_pixdisk_11062);
   
   multigraph->Add(gre,"lp");
   
   Double_t occ_pixdisk_2_fx1063[4] = {
   0,
   1,
   2,
   3};
   Double_t occ_pixdisk_2_fy1063[4] = {
   3.720238e-05,
   3.813244e-05,
   2.97619e-05,
   3.472222e-05};
   Double_t occ_pixdisk_2_fex1063[4] = {
   0,
   0,
   0,
   0};
   Double_t occ_pixdisk_2_fey1063[4] = {
   6.116748e-08,
   3.709218e-08,
   2.874857e-08,
   5.272311e-08};
   gre = new TGraphErrors(4,occ_pixdisk_2_fx1063,occ_pixdisk_2_fy1063,occ_pixdisk_2_fex1063,occ_pixdisk_2_fey1063);
   gre->SetName("occ_pixdisk_2");
   gre->SetTitle("Disk 2");
   gre->SetFillColor(1);
   gre->SetLineColor(3);
   gre->SetMarkerColor(3);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_occ_pixdisk_21063 = new TH1F("Graph_occ_pixdisk_21063","Disk 2",100,0,3.3);
   Graph_occ_pixdisk_21063->SetMinimum(2.888952e-05);
   Graph_occ_pixdisk_21063->SetMaximum(3.901317e-05);
   Graph_occ_pixdisk_21063->SetDirectory(0);
   Graph_occ_pixdisk_21063->SetStats(0);
   gre->SetHistogram(Graph_occ_pixdisk_21063);
   
   multigraph->Add(gre,"lp");
   
   Double_t occ_pixdisk_3_fx1064[4] = {
   0,
   1,
   2,
   3};
   Double_t occ_pixdisk_3_fy1064[4] = {
   1.946925e-05,
   1.835317e-05,
   1.698909e-05,
   2.058532e-05};
   Double_t occ_pixdisk_3_fex1064[4] = {
   0,
   0,
   0,
   0};
   Double_t occ_pixdisk_3_fey1064[4] = {
   1.562254e-08,
   1.740364e-08,
   4.728747e-08,
   1.88442e-08};
   gre = new TGraphErrors(4,occ_pixdisk_3_fx1064,occ_pixdisk_3_fy1064,occ_pixdisk_3_fex1064,occ_pixdisk_3_fey1064);
   gre->SetName("occ_pixdisk_3");
   gre->SetTitle("Disk 3");
   gre->SetFillColor(1);
   gre->SetLineColor(4);
   gre->SetMarkerColor(4);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_occ_pixdisk_31064 = new TH1F("Graph_occ_pixdisk_31064","Disk 3",100,0,3.3);
   Graph_occ_pixdisk_31064->SetMinimum(1.657556e-05);
   Graph_occ_pixdisk_31064->SetMaximum(2.09704e-05);
   Graph_occ_pixdisk_31064->SetDirectory(0);
   Graph_occ_pixdisk_31064->SetStats(0);
   gre->SetHistogram(Graph_occ_pixdisk_31064);
   
   multigraph->Add(gre,"lp");
   
   Double_t occ_pixdisk_4_fx1065[4] = {
   0,
   1,
   2,
   3};
   Double_t occ_pixdisk_4_fy1065[4] = {
   0.0001807416,
   0.0002052331,
   0.0001996528,
   0.0001931424};
   Double_t occ_pixdisk_4_fex1065[4] = {
   0,
   0,
   0,
   0};
   Double_t occ_pixdisk_4_fey1065[4] = {
   3.561853e-08,
   3.956198e-08,
   2.014538e-08,
   2.430428e-08};
   gre = new TGraphErrors(4,occ_pixdisk_4_fx1065,occ_pixdisk_4_fy1065,occ_pixdisk_4_fex1065,occ_pixdisk_4_fey1065);
   gre->SetName("occ_pixdisk_4");
   gre->SetTitle("Disk 0");
   gre->SetFillColor(1);
   gre->SetLineStyle(2);
   gre->SetMarkerStyle(21);
   
   TH1F *Graph_occ_pixdisk_41065 = new TH1F("Graph_occ_pixdisk_41065","Disk 0",100,0,3.3);
   Graph_occ_pixdisk_41065->SetMinimum(0.0001782493);
   Graph_occ_pixdisk_41065->SetMaximum(0.0002077294);
   Graph_occ_pixdisk_41065->SetDirectory(0);
   Graph_occ_pixdisk_41065->SetStats(0);
   gre->SetHistogram(Graph_occ_pixdisk_41065);
   
   multigraph->Add(gre,"lp");
   
   Double_t occ_pixdisk_5_fx1066[4] = {
   0,
   1,
   2,
   3};
   Double_t occ_pixdisk_5_fy1066[4] = {
   3.926918e-05,
   4.092262e-05,
   4.257606e-05,
   3.658234e-05};
   Double_t occ_pixdisk_5_fex1066[4] = {
   0,
   0,
   0,
   0};
   Double_t occ_pixdisk_5_fey1066[4] = {
   3.926753e-08,
   4.006156e-08,
   4.220571e-08,
   3.031914e-08};
   gre = new TGraphErrors(4,occ_pixdisk_5_fx1066,occ_pixdisk_5_fy1066,occ_pixdisk_5_fex1066,occ_pixdisk_5_fey1066);
   gre->SetName("occ_pixdisk_5");
   gre->SetTitle("Disk 1");
   gre->SetFillColor(1);
   gre->SetLineColor(2);
   gre->SetLineStyle(2);
   gre->SetMarkerColor(2);
   gre->SetMarkerStyle(21);
   
   TH1F *Graph_occ_pixdisk_51066 = new TH1F("Graph_occ_pixdisk_51066","Disk 1",100,0,3.3);
   Graph_occ_pixdisk_51066->SetMinimum(3.59454e-05);
   Graph_occ_pixdisk_51066->SetMaximum(4.322489e-05);
   Graph_occ_pixdisk_51066->SetDirectory(0);
   Graph_occ_pixdisk_51066->SetStats(0);
   gre->SetHistogram(Graph_occ_pixdisk_51066);
   
   multigraph->Add(gre,"lp");
   
   Double_t occ_pixdisk_6_fx1067[4] = {
   0,
   1,
   2,
   3};
   Double_t occ_pixdisk_6_fy1067[4] = {
   3.1312e-05,
   3.224206e-05,
   3.472222e-05,
   3.224206e-05};
   Double_t occ_pixdisk_6_fex1067[4] = {
   0,
   0,
   0,
   0};
   Double_t occ_pixdisk_6_fey1067[4] = {
   3.710089e-08,
   2.440684e-08,
   3.363129e-08,
   2.337355e-08};
   gre = new TGraphErrors(4,occ_pixdisk_6_fx1067,occ_pixdisk_6_fy1067,occ_pixdisk_6_fex1067,occ_pixdisk_6_fey1067);
   gre->SetName("occ_pixdisk_6");
   gre->SetTitle("Disk 2");
   gre->SetFillColor(1);
   gre->SetLineColor(3);
   gre->SetLineStyle(2);
   gre->SetMarkerColor(3);
   gre->SetMarkerStyle(21);
   
   TH1F *Graph_occ_pixdisk_61067 = new TH1F("Graph_occ_pixdisk_61067","Disk 2",100,0,3.3);
   Graph_occ_pixdisk_61067->SetMinimum(3.092681e-05);
   Graph_occ_pixdisk_61067->SetMaximum(3.510395e-05);
   Graph_occ_pixdisk_61067->SetDirectory(0);
   Graph_occ_pixdisk_61067->SetStats(0);
   gre->SetHistogram(Graph_occ_pixdisk_61067);
   
   multigraph->Add(gre,"lp");
   
   Double_t occ_pixdisk_7_fx1068[4] = {
   0,
   1,
   2,
   3};
   Double_t occ_pixdisk_7_fy1068[4] = {
   1.376488e-05,
   1.860119e-05,
   1.512897e-05,
   1.624504e-05};
   Double_t occ_pixdisk_7_fex1068[4] = {
   0,
   0,
   0,
   0};
   Double_t occ_pixdisk_7_fey1068[4] = {
   5.742709e-08,
   5.030523e-08,
   2.804158e-08,
   1.825252e-08};
   gre = new TGraphErrors(4,occ_pixdisk_7_fx1068,occ_pixdisk_7_fy1068,occ_pixdisk_7_fex1068,occ_pixdisk_7_fey1068);
   gre->SetName("occ_pixdisk_7");
   gre->SetTitle("Disk 3");
   gre->SetFillColor(1);
   gre->SetLineColor(4);
   gre->SetLineStyle(2);
   gre->SetMarkerColor(4);
   gre->SetMarkerStyle(21);
   
   TH1F *Graph_occ_pixdisk_71068 = new TH1F("Graph_occ_pixdisk_71068","Disk 3",100,0,3.3);
   Graph_occ_pixdisk_71068->SetMinimum(1.321305e-05);
   Graph_occ_pixdisk_71068->SetMaximum(1.91459e-05);
   Graph_occ_pixdisk_71068->SetDirectory(0);
   Graph_occ_pixdisk_71068->SetStats(0);
   gre->SetHistogram(Graph_occ_pixdisk_71068);
   
   multigraph->Add(gre,"lp");
   multigraph->Draw("Apl");
   multigraph->GetXaxis()->SetTitle("moduleID");
   multigraph->GetYaxis()->SetTitle("Occupancy in percent");
   
   TLegend *leg = new TLegend(0.14,0.62,0.34,0.87,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextFont(62);
   leg->SetTextSize(0.04);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(10);
   leg->SetFillStyle(0);
   TLegendEntry *entry=leg->AddEntry("NULL","Pixel Disk (left)","h");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("occ_pixdisk_0","Disk 0","lp");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("occ_pixdisk_1","Disk 1","lp");
   entry->SetLineColor(2);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(2);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("occ_pixdisk_2","Disk 2","lp");
   entry->SetLineColor(3);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(3);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("occ_pixdisk_3","Disk 3","lp");
   entry->SetLineColor(4);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(4);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   leg->Draw();
   
   leg = new TLegend(0.27,0.62,0.47,0.87,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextFont(62);
   leg->SetTextSize(0.04);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(10);
   leg->SetFillStyle(0);
   entry=leg->AddEntry("NULL","  (right)","h");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("occ_pixdisk_4","Disk 0","lp");
   entry->SetLineColor(1);
   entry->SetLineStyle(2);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("occ_pixdisk_5","Disk 1","lp");
   entry->SetLineColor(2);
   entry->SetLineStyle(2);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(2);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("occ_pixdisk_6","Disk 2","lp");
   entry->SetLineColor(3);
   entry->SetLineStyle(2);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(3);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("occ_pixdisk_7","Disk 3","lp");
   entry->SetLineColor(4);
   entry->SetLineStyle(2);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(4);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   leg->Draw();
   c1_n12->Modified();
   c1_n12->cd();
   c1_n12->SetSelected(c1_n12);
}
