void occ_pixdisk_mgr()
{
//=========Macro generated from canvas: c1_n13/c1_n13
//=========  (Tue Apr 19 16:53:24 2016) by ROOT version6.04/12
   TCanvas *c1_n13 = new TCanvas("c1_n13", "c1_n13",0,0,700,500);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   c1_n13->SetHighLightColor(2);
   c1_n13->Range(56.17329,-3.185619e-05,336.699,0.0003233454);
   c1_n13->SetFillColor(0);
   c1_n13->SetBorderMode(0);
   c1_n13->SetBorderSize(2);
   c1_n13->SetTickx(1);
   c1_n13->SetTicky(1);
   c1_n13->SetFrameBorderMode(0);
   c1_n13->SetFrameBorderMode(0);
   
   TMultiGraph *multigraph = new TMultiGraph();
   multigraph->SetName("occ_pixdisk_mgr");
   multigraph->SetTitle("");
   
   Double_t occ_r_pixdisk_0_fx1069[4] = {
   94.4268,
   96.76626,
   95.74606,
   97.37815};
   Double_t occ_r_pixdisk_0_fy1069[4] = {
   0.0001463294,
   0.0001912822,
   0.0002145337,
   0.0001968626};
   Double_t occ_r_pixdisk_0_fex1069[4] = {
   0,
   0,
   0,
   0};
   Double_t occ_r_pixdisk_0_fey1069[4] = {
   2.267454e-08,
   3.725559e-08,
   4.329686e-08,
   2.962192e-08};
   TGraphErrors *gre = new TGraphErrors(4,occ_r_pixdisk_0_fx1069,occ_r_pixdisk_0_fy1069,occ_r_pixdisk_0_fex1069,occ_r_pixdisk_0_fey1069);
   gre->SetName("occ_r_pixdisk_0");
   gre->SetTitle("Disk 0");
   gre->SetFillColor(1);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_occ_r_pixdisk_01069 = new TH1F("Graph_occ_r_pixdisk_01069","Disk 0",100,94.13167,97.67329);
   Graph_occ_r_pixdisk_01069->SetMinimum(0.0001394797);
   Graph_occ_r_pixdisk_01069->SetMaximum(0.0002214041);
   Graph_occ_r_pixdisk_01069->SetDirectory(0);
   Graph_occ_r_pixdisk_01069->SetStats(0);
   gre->SetHistogram(Graph_occ_r_pixdisk_01069);
   
   multigraph->Add(gre,"lp");
   
   Double_t occ_r_pixdisk_1_fx1070[4] = {
   171.3051,
   165.9816,
   167.5691,
   167.4642};
   Double_t occ_r_pixdisk_1_fy1070[4] = {
   6.097057e-05,
   3.59623e-05,
   4.856977e-05,
   5.14633e-05};
   Double_t occ_r_pixdisk_1_fex1070[4] = {
   0,
   0,
   0,
   0};
   Double_t occ_r_pixdisk_1_fey1070[4] = {
   5.914583e-08,
   3.348671e-08,
   5.377384e-08,
   3.351878e-08};
   gre = new TGraphErrors(4,occ_r_pixdisk_1_fx1070,occ_r_pixdisk_1_fy1070,occ_r_pixdisk_1_fex1070,occ_r_pixdisk_1_fey1070);
   gre->SetName("occ_r_pixdisk_1");
   gre->SetTitle("Disk 1");
   gre->SetFillColor(1);
   gre->SetLineColor(2);
   gre->SetMarkerColor(2);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_occ_r_pixdisk_11070 = new TH1F("Graph_occ_r_pixdisk_11070","Disk 1",100,165.4492,171.8375);
   Graph_occ_r_pixdisk_11070->SetMinimum(3.341872e-05);
   Graph_occ_r_pixdisk_11070->SetMaximum(6.353981e-05);
   Graph_occ_r_pixdisk_11070->SetDirectory(0);
   Graph_occ_r_pixdisk_11070->SetStats(0);
   gre->SetHistogram(Graph_occ_r_pixdisk_11070);
   
   multigraph->Add(gre,"lp");
   
   Double_t occ_r_pixdisk_2_fx1071[4] = {
   230.0413,
   230.4702,
   232.5179,
   230.8548};
   Double_t occ_r_pixdisk_2_fy1071[4] = {
   3.720238e-05,
   3.813244e-05,
   2.97619e-05,
   3.472222e-05};
   Double_t occ_r_pixdisk_2_fex1071[4] = {
   0,
   0,
   0,
   0};
   Double_t occ_r_pixdisk_2_fey1071[4] = {
   6.116748e-08,
   3.709218e-08,
   2.874857e-08,
   5.272311e-08};
   gre = new TGraphErrors(4,occ_r_pixdisk_2_fx1071,occ_r_pixdisk_2_fy1071,occ_r_pixdisk_2_fex1071,occ_r_pixdisk_2_fey1071);
   gre->SetName("occ_r_pixdisk_2");
   gre->SetTitle("Disk 2");
   gre->SetFillColor(1);
   gre->SetLineColor(3);
   gre->SetMarkerColor(3);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_occ_r_pixdisk_21071 = new TH1F("Graph_occ_r_pixdisk_21071","Disk 2",100,229.7936,232.7656);
   Graph_occ_r_pixdisk_21071->SetMinimum(2.888952e-05);
   Graph_occ_r_pixdisk_21071->SetMaximum(3.901317e-05);
   Graph_occ_r_pixdisk_21071->SetDirectory(0);
   Graph_occ_r_pixdisk_21071->SetStats(0);
   gre->SetHistogram(Graph_occ_r_pixdisk_21071);
   
   multigraph->Add(gre,"lp");
   
   Double_t occ_r_pixdisk_3_fx1072[4] = {
   292.6031,
   298.4455,
   289.4542,
   292.08};
   Double_t occ_r_pixdisk_3_fy1072[4] = {
   1.946925e-05,
   1.835317e-05,
   1.698909e-05,
   2.058532e-05};
   Double_t occ_r_pixdisk_3_fex1072[4] = {
   0,
   0,
   0,
   0};
   Double_t occ_r_pixdisk_3_fey1072[4] = {
   1.562254e-08,
   1.740364e-08,
   4.728747e-08,
   1.88442e-08};
   gre = new TGraphErrors(4,occ_r_pixdisk_3_fx1072,occ_r_pixdisk_3_fy1072,occ_r_pixdisk_3_fex1072,occ_r_pixdisk_3_fey1072);
   gre->SetName("occ_r_pixdisk_3");
   gre->SetTitle("Disk 3");
   gre->SetFillColor(1);
   gre->SetLineColor(4);
   gre->SetMarkerColor(4);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_occ_r_pixdisk_31072 = new TH1F("Graph_occ_r_pixdisk_31072","Disk 3",100,288.5551,299.3447);
   Graph_occ_r_pixdisk_31072->SetMinimum(1.657556e-05);
   Graph_occ_r_pixdisk_31072->SetMaximum(2.09704e-05);
   Graph_occ_r_pixdisk_31072->SetDirectory(0);
   Graph_occ_r_pixdisk_31072->SetStats(0);
   gre->SetHistogram(Graph_occ_r_pixdisk_31072);
   
   multigraph->Add(gre,"lp");
   
   Double_t occ_r_pixdisk_4_fx1073[4] = {
   96.76033,
   98.19654,
   99.04703,
   98.58714};
   Double_t occ_r_pixdisk_4_fy1073[4] = {
   0.0001807416,
   0.0002052331,
   0.0001996528,
   0.0001931424};
   Double_t occ_r_pixdisk_4_fex1073[4] = {
   0,
   0,
   0,
   0};
   Double_t occ_r_pixdisk_4_fey1073[4] = {
   3.561853e-08,
   3.956198e-08,
   2.014538e-08,
   2.430428e-08};
   gre = new TGraphErrors(4,occ_r_pixdisk_4_fx1073,occ_r_pixdisk_4_fy1073,occ_r_pixdisk_4_fex1073,occ_r_pixdisk_4_fey1073);
   gre->SetName("occ_r_pixdisk_4");
   gre->SetTitle("Disk 0");
   gre->SetFillColor(1);
   gre->SetLineStyle(2);
   gre->SetMarkerStyle(21);
   
   TH1F *Graph_occ_r_pixdisk_41073 = new TH1F("Graph_occ_r_pixdisk_41073","Disk 0",100,96.53166,99.2757);
   Graph_occ_r_pixdisk_41073->SetMinimum(0.0001782493);
   Graph_occ_r_pixdisk_41073->SetMaximum(0.0002077294);
   Graph_occ_r_pixdisk_41073->SetDirectory(0);
   Graph_occ_r_pixdisk_41073->SetStats(0);
   gre->SetHistogram(Graph_occ_r_pixdisk_41073);
   
   multigraph->Add(gre,"lp");
   
   Double_t occ_r_pixdisk_5_fx1074[4] = {
   168.3024,
   167.5795,
   169.7196,
   163.9075};
   Double_t occ_r_pixdisk_5_fy1074[4] = {
   3.926918e-05,
   4.092262e-05,
   4.257606e-05,
   3.658234e-05};
   Double_t occ_r_pixdisk_5_fex1074[4] = {
   0,
   0,
   0,
   0};
   Double_t occ_r_pixdisk_5_fey1074[4] = {
   3.926753e-08,
   4.006156e-08,
   4.220571e-08,
   3.031914e-08};
   gre = new TGraphErrors(4,occ_r_pixdisk_5_fx1074,occ_r_pixdisk_5_fy1074,occ_r_pixdisk_5_fex1074,occ_r_pixdisk_5_fey1074);
   gre->SetName("occ_r_pixdisk_5");
   gre->SetTitle("Disk 1");
   gre->SetFillColor(1);
   gre->SetLineColor(2);
   gre->SetLineStyle(2);
   gre->SetMarkerColor(2);
   gre->SetMarkerStyle(21);
   
   TH1F *Graph_occ_r_pixdisk_51074 = new TH1F("Graph_occ_r_pixdisk_51074","Disk 1",100,163.3263,170.3008);
   Graph_occ_r_pixdisk_51074->SetMinimum(3.59454e-05);
   Graph_occ_r_pixdisk_51074->SetMaximum(4.322489e-05);
   Graph_occ_r_pixdisk_51074->SetDirectory(0);
   Graph_occ_r_pixdisk_51074->SetStats(0);
   gre->SetHistogram(Graph_occ_r_pixdisk_51074);
   
   multigraph->Add(gre,"lp");
   
   Double_t occ_r_pixdisk_6_fx1075[4] = {
   228.8628,
   232.4234,
   228.8805,
   232.0264};
   Double_t occ_r_pixdisk_6_fy1075[4] = {
   3.1312e-05,
   3.224206e-05,
   3.472222e-05,
   3.224206e-05};
   Double_t occ_r_pixdisk_6_fex1075[4] = {
   0,
   0,
   0,
   0};
   Double_t occ_r_pixdisk_6_fey1075[4] = {
   3.710089e-08,
   2.440684e-08,
   3.363129e-08,
   2.337355e-08};
   gre = new TGraphErrors(4,occ_r_pixdisk_6_fx1075,occ_r_pixdisk_6_fy1075,occ_r_pixdisk_6_fex1075,occ_r_pixdisk_6_fey1075);
   gre->SetName("occ_r_pixdisk_6");
   gre->SetTitle("Disk 2");
   gre->SetFillColor(1);
   gre->SetLineColor(3);
   gre->SetLineStyle(2);
   gre->SetMarkerColor(3);
   gre->SetMarkerStyle(21);
   
   TH1F *Graph_occ_r_pixdisk_61075 = new TH1F("Graph_occ_r_pixdisk_61075","Disk 2",100,228.5068,232.7794);
   Graph_occ_r_pixdisk_61075->SetMinimum(3.092681e-05);
   Graph_occ_r_pixdisk_61075->SetMaximum(3.510395e-05);
   Graph_occ_r_pixdisk_61075->SetDirectory(0);
   Graph_occ_r_pixdisk_61075->SetStats(0);
   gre->SetHistogram(Graph_occ_r_pixdisk_61075);
   
   multigraph->Add(gre,"lp");
   
   Double_t occ_r_pixdisk_7_fx1076[4] = {
   296.7885,
   292.8497,
   292.7878,
   295.6867};
   Double_t occ_r_pixdisk_7_fy1076[4] = {
   1.376488e-05,
   1.860119e-05,
   1.512897e-05,
   1.624504e-05};
   Double_t occ_r_pixdisk_7_fex1076[4] = {
   0,
   0,
   0,
   0};
   Double_t occ_r_pixdisk_7_fey1076[4] = {
   5.742709e-08,
   5.030523e-08,
   2.804158e-08,
   1.825252e-08};
   gre = new TGraphErrors(4,occ_r_pixdisk_7_fx1076,occ_r_pixdisk_7_fy1076,occ_r_pixdisk_7_fex1076,occ_r_pixdisk_7_fey1076);
   gre->SetName("occ_r_pixdisk_7");
   gre->SetTitle("Disk 3");
   gre->SetFillColor(1);
   gre->SetLineColor(4);
   gre->SetLineStyle(2);
   gre->SetMarkerColor(4);
   gre->SetMarkerStyle(21);
   
   TH1F *Graph_occ_r_pixdisk_71076 = new TH1F("Graph_occ_r_pixdisk_71076","Disk 3",100,292.3877,297.1885);
   Graph_occ_r_pixdisk_71076->SetMinimum(1.321305e-05);
   Graph_occ_r_pixdisk_71076->SetMaximum(1.91459e-05);
   Graph_occ_r_pixdisk_71076->SetDirectory(0);
   Graph_occ_r_pixdisk_71076->SetStats(0);
   gre->SetHistogram(Graph_occ_r_pixdisk_71076);
   
   multigraph->Add(gre,"lp");
   multigraph->Draw("Apl");
   multigraph->GetXaxis()->SetTitle("r [mm]");
   multigraph->GetYaxis()->SetTitle("Occupancy in percent");
   
   TLegend *leg = new TLegend(0.57,0.62,0.77,0.87,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextFont(62);
   leg->SetTextSize(0.04);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(10);
   leg->SetFillStyle(0);
   TLegendEntry *entry=leg->AddEntry("NULL","Pixel Disk (left)","h");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("occ_r_pixdisk_0","Disk 0","lp");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("occ_r_pixdisk_1","Disk 1","lp");
   entry->SetLineColor(2);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(2);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("occ_r_pixdisk_2","Disk 2","lp");
   entry->SetLineColor(3);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(3);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("occ_r_pixdisk_3","Disk 3","lp");
   entry->SetLineColor(4);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(4);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   leg->Draw();
   
   leg = new TLegend(0.7,0.62,0.9,0.87,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextFont(62);
   leg->SetTextSize(0.04);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(10);
   leg->SetFillStyle(0);
   entry=leg->AddEntry("NULL","  (right)","h");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("occ_r_pixdisk_4","Disk 0","lp");
   entry->SetLineColor(1);
   entry->SetLineStyle(2);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("occ_r_pixdisk_5","Disk 1","lp");
   entry->SetLineColor(2);
   entry->SetLineStyle(2);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(2);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("occ_r_pixdisk_6","Disk 2","lp");
   entry->SetLineColor(3);
   entry->SetLineStyle(2);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(3);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("occ_r_pixdisk_7","Disk 3","lp");
   entry->SetLineColor(4);
   entry->SetLineStyle(2);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(4);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   leg->Draw();
   c1_n13->Modified();
   c1_n13->cd();
   c1_n13->SetSelected(c1_n13);
}
