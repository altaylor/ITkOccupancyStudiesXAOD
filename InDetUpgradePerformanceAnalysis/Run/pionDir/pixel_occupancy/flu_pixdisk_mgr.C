void flu_pixdisk_mgr()
{
//=========Macro generated from canvas: c1_n19/c1_n19
//=========  (Tue Apr 19 16:53:24 2016) by ROOT version6.04/12
   TCanvas *c1_n19 = new TCanvas("c1_n19", "c1_n19",0,0,700,500);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   c1_n19->SetHighLightColor(2);
   c1_n19->Range(56.17329,-0.000508445,336.699,0.005172235);
   c1_n19->SetFillColor(0);
   c1_n19->SetBorderMode(0);
   c1_n19->SetBorderSize(2);
   c1_n19->SetTickx(1);
   c1_n19->SetTicky(1);
   c1_n19->SetFrameBorderMode(0);
   c1_n19->SetFrameBorderMode(0);
   
   TMultiGraph *multigraph = new TMultiGraph();
   multigraph->SetName("flu_pixdisk_mgr");
   multigraph->SetTitle("");
   
   Double_t flu_r_pixdisk_0_fx1117[4] = {
   94.4268,
   96.76626,
   95.74606,
   97.37815};
   Double_t flu_r_pixdisk_0_fy1117[4] = {
   0.00234127,
   0.003060516,
   0.00343254,
   0.003149802};
   Double_t flu_r_pixdisk_0_fex1117[4] = {
   0,
   0,
   0,
   0};
   Double_t flu_r_pixdisk_0_fey1117[4] = {
   0,
   0,
   0,
   0};
   TGraphErrors *gre = new TGraphErrors(4,flu_r_pixdisk_0_fx1117,flu_r_pixdisk_0_fy1117,flu_r_pixdisk_0_fex1117,flu_r_pixdisk_0_fey1117);
   gre->SetName("flu_r_pixdisk_0");
   gre->SetTitle("Disk 0");
   gre->SetFillColor(1);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_flu_r_pixdisk_01117 = new TH1F("Graph_flu_r_pixdisk_01117","Disk 0",100,94.13167,97.67329);
   Graph_flu_r_pixdisk_01117->SetMinimum(0.002232143);
   Graph_flu_r_pixdisk_01117->SetMaximum(0.003541667);
   Graph_flu_r_pixdisk_01117->SetDirectory(0);
   Graph_flu_r_pixdisk_01117->SetStats(0);
   gre->SetHistogram(Graph_flu_r_pixdisk_01117);
   
   multigraph->Add(gre,"lp");
   
   Double_t flu_r_pixdisk_1_fx1118[4] = {
   171.3051,
   165.9816,
   167.5691,
   167.4642};
   Double_t flu_r_pixdisk_1_fy1118[4] = {
   0.0009755291,
   0.0005753968,
   0.0007771164,
   0.0008234127};
   Double_t flu_r_pixdisk_1_fex1118[4] = {
   0,
   0,
   0,
   0};
   Double_t flu_r_pixdisk_1_fey1118[4] = {
   0,
   0,
   0,
   0};
   gre = new TGraphErrors(4,flu_r_pixdisk_1_fx1118,flu_r_pixdisk_1_fy1118,flu_r_pixdisk_1_fex1118,flu_r_pixdisk_1_fey1118);
   gre->SetName("flu_r_pixdisk_1");
   gre->SetTitle("Disk 1");
   gre->SetFillColor(1);
   gre->SetLineColor(2);
   gre->SetMarkerColor(2);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_flu_r_pixdisk_11118 = new TH1F("Graph_flu_r_pixdisk_11118","Disk 1",100,165.4492,171.8375);
   Graph_flu_r_pixdisk_11118->SetMinimum(0.0005353836);
   Graph_flu_r_pixdisk_11118->SetMaximum(0.001015542);
   Graph_flu_r_pixdisk_11118->SetDirectory(0);
   Graph_flu_r_pixdisk_11118->SetStats(0);
   gre->SetHistogram(Graph_flu_r_pixdisk_11118);
   
   multigraph->Add(gre,"lp");
   
   Double_t flu_r_pixdisk_2_fx1119[4] = {
   230.0413,
   230.4702,
   232.5179,
   230.8548};
   Double_t flu_r_pixdisk_2_fy1119[4] = {
   0.0005952381,
   0.0006101191,
   0.0004761905,
   0.0005555556};
   Double_t flu_r_pixdisk_2_fex1119[4] = {
   0,
   0,
   0,
   0};
   Double_t flu_r_pixdisk_2_fey1119[4] = {
   0,
   0,
   0,
   0};
   gre = new TGraphErrors(4,flu_r_pixdisk_2_fx1119,flu_r_pixdisk_2_fy1119,flu_r_pixdisk_2_fex1119,flu_r_pixdisk_2_fey1119);
   gre->SetName("flu_r_pixdisk_2");
   gre->SetTitle("Disk 2");
   gre->SetFillColor(1);
   gre->SetLineColor(3);
   gre->SetMarkerColor(3);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_flu_r_pixdisk_21119 = new TH1F("Graph_flu_r_pixdisk_21119","Disk 2",100,229.7936,232.7656);
   Graph_flu_r_pixdisk_21119->SetMinimum(0.0004627976);
   Graph_flu_r_pixdisk_21119->SetMaximum(0.0006235119);
   Graph_flu_r_pixdisk_21119->SetDirectory(0);
   Graph_flu_r_pixdisk_21119->SetStats(0);
   gre->SetHistogram(Graph_flu_r_pixdisk_21119);
   
   multigraph->Add(gre,"lp");
   
   Double_t flu_r_pixdisk_3_fx1120[4] = {
   292.6031,
   298.4455,
   289.4542,
   292.08};
   Double_t flu_r_pixdisk_3_fy1120[4] = {
   0.0003115079,
   0.0002936508,
   0.0002718254,
   0.0003293651};
   Double_t flu_r_pixdisk_3_fex1120[4] = {
   0,
   0,
   0,
   0};
   Double_t flu_r_pixdisk_3_fey1120[4] = {
   0,
   0,
   0,
   0};
   gre = new TGraphErrors(4,flu_r_pixdisk_3_fx1120,flu_r_pixdisk_3_fy1120,flu_r_pixdisk_3_fex1120,flu_r_pixdisk_3_fey1120);
   gre->SetName("flu_r_pixdisk_3");
   gre->SetTitle("Disk 3");
   gre->SetFillColor(1);
   gre->SetLineColor(4);
   gre->SetMarkerColor(4);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_flu_r_pixdisk_31120 = new TH1F("Graph_flu_r_pixdisk_31120","Disk 3",100,288.5551,299.3447);
   Graph_flu_r_pixdisk_31120->SetMinimum(0.0002660714);
   Graph_flu_r_pixdisk_31120->SetMaximum(0.000335119);
   Graph_flu_r_pixdisk_31120->SetDirectory(0);
   Graph_flu_r_pixdisk_31120->SetStats(0);
   gre->SetHistogram(Graph_flu_r_pixdisk_31120);
   
   multigraph->Add(gre,"lp");
   
   Double_t flu_r_pixdisk_4_fx1121[4] = {
   96.76033,
   98.19654,
   99.04703,
   98.58714};
   Double_t flu_r_pixdisk_4_fy1121[4] = {
   0.002891865,
   0.00328373,
   0.003194445,
   0.003090278};
   Double_t flu_r_pixdisk_4_fex1121[4] = {
   0,
   0,
   0,
   0};
   Double_t flu_r_pixdisk_4_fey1121[4] = {
   0,
   0,
   0,
   0};
   gre = new TGraphErrors(4,flu_r_pixdisk_4_fx1121,flu_r_pixdisk_4_fy1121,flu_r_pixdisk_4_fex1121,flu_r_pixdisk_4_fey1121);
   gre->SetName("flu_r_pixdisk_4");
   gre->SetTitle("Disk 0");
   gre->SetFillColor(1);
   gre->SetLineStyle(2);
   gre->SetMarkerStyle(21);
   
   TH1F *Graph_flu_r_pixdisk_41121 = new TH1F("Graph_flu_r_pixdisk_41121","Disk 0",100,96.53166,99.2757);
   Graph_flu_r_pixdisk_41121->SetMinimum(0.002852679);
   Graph_flu_r_pixdisk_41121->SetMaximum(0.003322917);
   Graph_flu_r_pixdisk_41121->SetDirectory(0);
   Graph_flu_r_pixdisk_41121->SetStats(0);
   gre->SetHistogram(Graph_flu_r_pixdisk_41121);
   
   multigraph->Add(gre,"lp");
   
   Double_t flu_r_pixdisk_5_fx1122[4] = {
   168.3024,
   167.5795,
   169.7196,
   163.9075};
   Double_t flu_r_pixdisk_5_fy1122[4] = {
   0.0006283069,
   0.0006547619,
   0.0006812169,
   0.0005853174};
   Double_t flu_r_pixdisk_5_fex1122[4] = {
   0,
   0,
   0,
   0};
   Double_t flu_r_pixdisk_5_fey1122[4] = {
   0,
   0,
   0,
   0};
   gre = new TGraphErrors(4,flu_r_pixdisk_5_fx1122,flu_r_pixdisk_5_fy1122,flu_r_pixdisk_5_fex1122,flu_r_pixdisk_5_fey1122);
   gre->SetName("flu_r_pixdisk_5");
   gre->SetTitle("Disk 1");
   gre->SetFillColor(1);
   gre->SetLineColor(2);
   gre->SetLineStyle(2);
   gre->SetMarkerColor(2);
   gre->SetMarkerStyle(21);
   
   TH1F *Graph_flu_r_pixdisk_51122 = new TH1F("Graph_flu_r_pixdisk_51122","Disk 1",100,163.3263,170.3008);
   Graph_flu_r_pixdisk_51122->SetMinimum(0.0005757275);
   Graph_flu_r_pixdisk_51122->SetMaximum(0.0006908068);
   Graph_flu_r_pixdisk_51122->SetDirectory(0);
   Graph_flu_r_pixdisk_51122->SetStats(0);
   gre->SetHistogram(Graph_flu_r_pixdisk_51122);
   
   multigraph->Add(gre,"lp");
   
   Double_t flu_r_pixdisk_6_fx1123[4] = {
   228.8628,
   232.4234,
   228.8805,
   232.0264};
   Double_t flu_r_pixdisk_6_fy1123[4] = {
   0.0005009921,
   0.000515873,
   0.0005555556,
   0.000515873};
   Double_t flu_r_pixdisk_6_fex1123[4] = {
   0,
   0,
   0,
   0};
   Double_t flu_r_pixdisk_6_fey1123[4] = {
   0,
   0,
   0,
   0};
   gre = new TGraphErrors(4,flu_r_pixdisk_6_fx1123,flu_r_pixdisk_6_fy1123,flu_r_pixdisk_6_fex1123,flu_r_pixdisk_6_fey1123);
   gre->SetName("flu_r_pixdisk_6");
   gre->SetTitle("Disk 2");
   gre->SetFillColor(1);
   gre->SetLineColor(3);
   gre->SetLineStyle(2);
   gre->SetMarkerColor(3);
   gre->SetMarkerStyle(21);
   
   TH1F *Graph_flu_r_pixdisk_61123 = new TH1F("Graph_flu_r_pixdisk_61123","Disk 2",100,228.5068,232.7794);
   Graph_flu_r_pixdisk_61123->SetMinimum(0.0004955357);
   Graph_flu_r_pixdisk_61123->SetMaximum(0.0005610119);
   Graph_flu_r_pixdisk_61123->SetDirectory(0);
   Graph_flu_r_pixdisk_61123->SetStats(0);
   gre->SetHistogram(Graph_flu_r_pixdisk_61123);
   
   multigraph->Add(gre,"lp");
   
   Double_t flu_r_pixdisk_7_fx1124[4] = {
   296.7885,
   292.8497,
   292.7878,
   295.6867};
   Double_t flu_r_pixdisk_7_fy1124[4] = {
   0.0002202381,
   0.0002976191,
   0.0002420635,
   0.0002599206};
   Double_t flu_r_pixdisk_7_fex1124[4] = {
   0,
   0,
   0,
   0};
   Double_t flu_r_pixdisk_7_fey1124[4] = {
   0,
   0,
   0,
   0};
   gre = new TGraphErrors(4,flu_r_pixdisk_7_fx1124,flu_r_pixdisk_7_fy1124,flu_r_pixdisk_7_fex1124,flu_r_pixdisk_7_fey1124);
   gre->SetName("flu_r_pixdisk_7");
   gre->SetTitle("Disk 3");
   gre->SetFillColor(1);
   gre->SetLineColor(4);
   gre->SetLineStyle(2);
   gre->SetMarkerColor(4);
   gre->SetMarkerStyle(21);
   
   TH1F *Graph_flu_r_pixdisk_71124 = new TH1F("Graph_flu_r_pixdisk_71124","Disk 3",100,292.3877,297.1885);
   Graph_flu_r_pixdisk_71124->SetMinimum(0.0002125);
   Graph_flu_r_pixdisk_71124->SetMaximum(0.0003053572);
   Graph_flu_r_pixdisk_71124->SetDirectory(0);
   Graph_flu_r_pixdisk_71124->SetStats(0);
   gre->SetHistogram(Graph_flu_r_pixdisk_71124);
   
   multigraph->Add(gre,"lp");
   multigraph->Draw("Apl");
   multigraph->GetXaxis()->SetTitle("r [mm]");
   multigraph->GetYaxis()->SetTitle("Hits per mm^{2}");
   
   TLegend *leg = new TLegend(0.57,0.62,0.77,0.87,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextFont(62);
   leg->SetTextSize(0.04);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(10);
   leg->SetFillStyle(0);
   TLegendEntry *entry=leg->AddEntry("NULL","Pixel Disk (left)","h");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("flu_r_pixdisk_0","Disk 0","lp");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("flu_r_pixdisk_1","Disk 1","lp");
   entry->SetLineColor(2);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(2);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("flu_r_pixdisk_2","Disk 2","lp");
   entry->SetLineColor(3);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(3);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("flu_r_pixdisk_3","Disk 3","lp");
   entry->SetLineColor(4);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(4);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   leg->Draw();
   
   leg = new TLegend(0.7,0.62,0.9,0.87,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextFont(62);
   leg->SetTextSize(0.04);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(10);
   leg->SetFillStyle(0);
   entry=leg->AddEntry("NULL","  (right)","h");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("flu_r_pixdisk_4","Disk 0","lp");
   entry->SetLineColor(1);
   entry->SetLineStyle(2);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("flu_r_pixdisk_5","Disk 1","lp");
   entry->SetLineColor(2);
   entry->SetLineStyle(2);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(2);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("flu_r_pixdisk_6","Disk 2","lp");
   entry->SetLineColor(3);
   entry->SetLineStyle(2);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(3);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("flu_r_pixdisk_7","Disk 3","lp");
   entry->SetLineColor(4);
   entry->SetLineStyle(2);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(4);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   leg->Draw();
   c1_n19->Modified();
   c1_n19->cd();
   c1_n19->SetSelected(c1_n19);
}
