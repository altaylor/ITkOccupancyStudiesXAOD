void occ_sctdisk_mgr()
{
//=========Macro generated from canvas: c1_n32/c1_n32
//=========  (Tue Apr 19 16:53:27 2016) by ROOT version6.04/12
   TCanvas *c1_n32 = new TCanvas("c1_n32", "c1_n32",0,0,700,500);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   c1_n32->SetHighLightColor(2);
   c1_n32->Range(292.4667,-0.001784347,1040.139,0.01827704);
   c1_n32->SetFillColor(0);
   c1_n32->SetBorderMode(0);
   c1_n32->SetBorderSize(2);
   c1_n32->SetTickx(1);
   c1_n32->SetTicky(1);
   c1_n32->SetFrameBorderMode(0);
   c1_n32->SetFrameBorderMode(0);
   
   TMultiGraph *multigraph = new TMultiGraph();
   multigraph->SetName("occ_sctdisk_mgr");
   multigraph->SetTitle("");
   
   Double_t occ_r_sctdisk_0_fx1185[18] = {
   394.4221,
   416.0944,
   442.4763,
   473.5385,
   499.8765,
   522.1027,
   547.8742,
   567.5033,
   592.0626,
   622.8918,
   654.8169,
   684.2667,
   710.2542,
   739.7765,
   784.5938,
   839.207,
   887.6057,
   938.0043};
   Double_t occ_r_sctdisk_0_fy1185[18] = {
   0.005859375,
   0.005664062,
   0.005761719,
   0.00546875,
   0.004589844,
   0.00625,
   0.00390625,
   0.003027344,
   0.006445313,
   0.005175781,
   0.002441406,
   0.002197266,
   0.001269531,
   0.001367188,
   0.004101562,
   0.003222656,
   0.00078125,
   0.001416016};
   Double_t occ_r_sctdisk_0_fex1185[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t occ_r_sctdisk_0_fey1185[18] = {
   1.029387e-05,
   1.339363e-05,
   9.495121e-06,
   1.251001e-05,
   1.2624e-05,
   6.611013e-06,
   6.480654e-06,
   1.802782e-05,
   1.536327e-05,
   1.198323e-05,
   6.13701e-06,
   7.371596e-06,
   1.719127e-05,
   1.020556e-05,
   5.967241e-06,
   4.052969e-06,
   2.063365e-05,
   6.993786e-06};
   TGraphErrors *gre = new TGraphErrors(18,occ_r_sctdisk_0_fx1185,occ_r_sctdisk_0_fy1185,occ_r_sctdisk_0_fex1185,occ_r_sctdisk_0_fey1185);
   gre->SetName("occ_r_sctdisk_0");
   gre->SetTitle("Disk 0");
   gre->SetFillColor(1);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_occ_r_sctdisk_01185 = new TH1F("Graph_occ_r_sctdisk_01185","Disk 0",100,340.0638,992.3626);
   Graph_occ_r_sctdisk_01185->SetMinimum(0.0001906103);
   Graph_occ_r_sctdisk_01185->SetMaximum(0.007030682);
   Graph_occ_r_sctdisk_01185->SetDirectory(0);
   Graph_occ_r_sctdisk_01185->SetStats(0);
   gre->SetHistogram(Graph_occ_r_sctdisk_01185);
   
   multigraph->Add(gre,"lp");
   
   Double_t occ_r_sctdisk_1_fx1186[18] = {
   394.7541,
   416.1317,
   442.6298,
   473.2135,
   499.5509,
   522.8507,
   548.2441,
   567.8464,
   591.7681,
   622.7177,
   654.8351,
   684.2275,
   710.4683,
   739.6525,
   784.6198,
   839.1673,
   887.9419,
   938.0532};
   Double_t occ_r_sctdisk_1_fy1186[18] = {
   0.005371094,
   0.007324219,
   0.009179687,
   0.0078125,
   0.006640625,
   0.005175781,
   0.006152344,
   0.004003906,
   0.006933594,
   0.004882812,
   0.002099609,
   0.002148438,
   0.001660156,
   0.003076172,
   0.004833984,
   0.003466797,
   0.001416016,
   0.003125};
   Double_t occ_r_sctdisk_1_fex1186[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t occ_r_sctdisk_1_fey1186[18] = {
   1.251149e-05,
   8.867002e-06,
   5.884292e-06,
   7.928423e-06,
   1.14553e-05,
   2.48846e-05,
   9.390078e-06,
   7.885957e-06,
   6.764098e-06,
   1.615641e-05,
   6.796366e-06,
   1.199886e-05,
   4.507212e-06,
   1.06983e-05,
   5.800944e-06,
   5.328267e-06,
   7.884963e-06,
   6.370781e-06};
   gre = new TGraphErrors(18,occ_r_sctdisk_1_fx1186,occ_r_sctdisk_1_fy1186,occ_r_sctdisk_1_fex1186,occ_r_sctdisk_1_fey1186);
   gre->SetName("occ_r_sctdisk_1");
   gre->SetTitle("Disk 1");
   gre->SetFillColor(1);
   gre->SetLineColor(2);
   gre->SetMarkerColor(2);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_occ_r_sctdisk_11186 = new TH1F("Graph_occ_r_sctdisk_11186","Disk 1",100,340.4242,992.3831);
   Graph_occ_r_sctdisk_11186->SetMinimum(0.0006303865);
   Graph_occ_r_sctdisk_11186->SetMaximum(0.009963316);
   Graph_occ_r_sctdisk_11186->SetDirectory(0);
   Graph_occ_r_sctdisk_11186->SetStats(0);
   gre->SetHistogram(Graph_occ_r_sctdisk_11186);
   
   multigraph->Add(gre,"lp");
   
   Double_t occ_r_sctdisk_2_fx1187[18] = {
   394.4399,
   415.9905,
   442.5901,
   473.2523,
   499.6278,
   522.1873,
   548.286,
   567.6979,
   591.9041,
   622.7636,
   654.9409,
   684.1786,
   710.5516,
   739.7081,
   784.4871,
   839.1527,
   887.9425,
   938.0197};
   Double_t occ_r_sctdisk_2_fy1187[18] = {
   0.005859375,
   0.008496094,
   0.008691407,
   0.01054687,
   0.004101562,
   0.005078125,
   0.00546875,
   0.00234375,
   0.006445313,
   0.005371094,
   0.005078125,
   0.0015625,
   0.001611328,
   0.00234375,
   0.003759766,
   0.005566406,
   0.001904297,
   0.003125};
   Double_t occ_r_sctdisk_2_fex1187[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t occ_r_sctdisk_2_fey1187[18] = {
   1.631924e-05,
   9.063992e-06,
   5.287484e-06,
   9.765582e-06,
   7.278867e-06,
   1.759209e-05,
   5.665475e-06,
   1.142378e-05,
   1.158534e-05,
   9.107184e-06,
   3.553389e-06,
   4.114502e-06,
   3.981117e-06,
   9.284668e-06,
   4.58213e-06,
   4.05327e-06,
   5.583709e-06,
   5.941908e-06};
   gre = new TGraphErrors(18,occ_r_sctdisk_2_fx1187,occ_r_sctdisk_2_fy1187,occ_r_sctdisk_2_fex1187,occ_r_sctdisk_2_fey1187);
   gre->SetName("occ_r_sctdisk_2");
   gre->SetTitle("Disk 2");
   gre->SetFillColor(1);
   gre->SetLineColor(3);
   gre->SetMarkerColor(3);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_occ_r_sctdisk_21187 = new TH1F("Graph_occ_r_sctdisk_21187","Disk 2",100,340.082,992.3777);
   Graph_occ_r_sctdisk_21187->SetMinimum(0.00065856);
   Graph_occ_r_sctdisk_21187->SetMaximum(0.01145647);
   Graph_occ_r_sctdisk_21187->SetDirectory(0);
   Graph_occ_r_sctdisk_21187->SetStats(0);
   gre->SetHistogram(Graph_occ_r_sctdisk_21187);
   
   multigraph->Add(gre,"lp");
   
   Double_t occ_r_sctdisk_3_fx1188[18] = {
   394.6196,
   416.3269,
   442.4458,
   473.1245,
   499.596,
   522.17,
   548.2299,
   567.1807,
   592.2401,
   622.8047,
   654.9147,
   684.183,
   710.4252,
   739.6489,
   784.5177,
   839.1287,
   887.8914,
   938.1483};
   Double_t occ_r_sctdisk_3_fy1188[18] = {
   0.003222656,
   0.005761719,
   0.006054688,
   0.008984375,
   0.004296875,
   0.005957031,
   0.006933594,
   0.004101562,
   0.005664062,
   0.006835937,
   0.002880859,
   0.003759766,
   0.002294922,
   0.002539062,
   0.00444336,
   0.002734375,
   0.003369141,
   0.004150391};
   Double_t occ_r_sctdisk_3_fex1188[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t occ_r_sctdisk_3_fey1188[18] = {
   1.277043e-05,
   1.153908e-05,
   1.139855e-05,
   7.16965e-06,
   8.343136e-06,
   5.483234e-06,
   9.598387e-06,
   1.950413e-05,
   6.156248e-06,
   6.872805e-06,
   6.990577e-06,
   5.020144e-06,
   4.450416e-06,
   3.822124e-06,
   5.291424e-06,
   4.30795e-06,
   4.484215e-06,
   2.937862e-06};
   gre = new TGraphErrors(18,occ_r_sctdisk_3_fx1188,occ_r_sctdisk_3_fy1188,occ_r_sctdisk_3_fex1188,occ_r_sctdisk_3_fey1188);
   gre->SetName("occ_r_sctdisk_3");
   gre->SetTitle("Disk 3");
   gre->SetFillColor(1);
   gre->SetLineColor(4);
   gre->SetMarkerColor(4);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_occ_r_sctdisk_31188 = new TH1F("Graph_occ_r_sctdisk_31188","Disk 3",100,340.2668,992.5011);
   Graph_occ_r_sctdisk_31188->SetMinimum(0.001620364);
   Graph_occ_r_sctdisk_31188->SetMaximum(0.009661652);
   Graph_occ_r_sctdisk_31188->SetDirectory(0);
   Graph_occ_r_sctdisk_31188->SetStats(0);
   gre->SetHistogram(Graph_occ_r_sctdisk_31188);
   
   multigraph->Add(gre,"lp");
   
   Double_t occ_r_sctdisk_4_fx1189[18] = {
   394.5581,
   416.0321,
   442.7567,
   473.3475,
   499.6526,
   522.1202,
   547.8279,
   567.6064,
   592.2683,
   622.6852,
   655.0346,
   684.2567,
   710.4611,
   739.6423,
   784.541,
   839.1758,
   887.7589,
   938.0259};
   Double_t occ_r_sctdisk_4_fy1189[18] = {
   0.006347656,
   0.006542969,
   0.0078125,
   0.006445313,
   0.002832031,
   0.005371094,
   0.004589844,
   0.001855469,
   0.00703125,
   0.008398437,
   0.002294922,
   0.003320313,
   0.002685547,
   0.003564453,
   0.004541016,
   0.003710938,
   0.003710938,
   0.002880859};
   Double_t occ_r_sctdisk_4_fex1189[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t occ_r_sctdisk_4_fey1189[18] = {
   8.843882e-06,
   8.78333e-06,
   3.64228e-06,
   1.073282e-05,
   6.239952e-06,
   5.775081e-06,
   1.016025e-05,
   0,
   6.052961e-06,
   4.976996e-06,
   3.877935e-06,
   2.97434e-06,
   3.525328e-06,
   2.388333e-06,
   2.450422e-06,
   2.744073e-06,
   4.68641e-06,
   2.993485e-06};
   gre = new TGraphErrors(18,occ_r_sctdisk_4_fx1189,occ_r_sctdisk_4_fy1189,occ_r_sctdisk_4_fex1189,occ_r_sctdisk_4_fey1189);
   gre->SetName("occ_r_sctdisk_4");
   gre->SetTitle("Disk 4");
   gre->SetFillColor(1);
   gre->SetLineColor(5);
   gre->SetMarkerColor(5);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_occ_r_sctdisk_41189 = new TH1F("Graph_occ_r_sctdisk_41189","Disk 4",100,340.2113,992.3727);
   Graph_occ_r_sctdisk_41189->SetMinimum(0.001200674);
   Graph_occ_r_sctdisk_41189->SetMaximum(0.009058208);
   Graph_occ_r_sctdisk_41189->SetDirectory(0);
   Graph_occ_r_sctdisk_41189->SetStats(0);
   gre->SetHistogram(Graph_occ_r_sctdisk_41189);
   
   multigraph->Add(gre,"lp");
   
   Double_t occ_r_sctdisk_5_fx1190[18] = {
   394.6233,
   416.1436,
   442.4849,
   472.9776,
   499.7473,
   522.2602,
   548.0542,
   567.3267,
   592.0724,
   623.03,
   654.9313,
   684.1475,
   710.4598,
   739.6385,
   784.519,
   839.1204,
   887.9235,
   938.1686};
   Double_t occ_r_sctdisk_5_fy1190[18] = {
   0.008691407,
   0.01123047,
   0.007910157,
   0.007910157,
   0.004296875,
   0.005664062,
   0.002539062,
   0.003320313,
   0.006738281,
   0.006347656,
   0.002587891,
   0.002636719,
   0.002832031,
   0.003662109,
   0.005615234,
   0.002783203,
   0.005175781,
   0.004785156};
   Double_t occ_r_sctdisk_5_fex1190[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t occ_r_sctdisk_5_fey1190[18] = {
   1.04962e-05,
   7.402311e-06,
   6.599312e-06,
   1.175229e-05,
   4.021042e-06,
   8.851996e-06,
   5.627963e-06,
   1.898162e-05,
   7.489456e-06,
   1.096647e-05,
   7.698102e-06,
   7.944368e-06,
   5.988848e-06,
   3.398697e-06,
   2.733656e-06,
   2.779927e-06,
   2.572068e-06,
   2.223077e-06};
   gre = new TGraphErrors(18,occ_r_sctdisk_5_fx1190,occ_r_sctdisk_5_fy1190,occ_r_sctdisk_5_fex1190,occ_r_sctdisk_5_fey1190);
   gre->SetName("occ_r_sctdisk_5");
   gre->SetTitle("Disk 5");
   gre->SetFillColor(1);
   gre->SetLineColor(6);
   gre->SetMarkerColor(6);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_occ_r_sctdisk_51190 = new TH1F("Graph_occ_r_sctdisk_51190","Disk 5",100,340.2687,992.5231);
   Graph_occ_r_sctdisk_51190->SetMinimum(0.001662991);
   Graph_occ_r_sctdisk_51190->SetMaximum(0.01210831);
   Graph_occ_r_sctdisk_51190->SetDirectory(0);
   Graph_occ_r_sctdisk_51190->SetStats(0);
   gre->SetHistogram(Graph_occ_r_sctdisk_51190);
   
   multigraph->Add(gre,"lp");
   
   Double_t occ_r_sctdisk_6_fx1191[18] = {
   394.651,
   416.2668,
   442.5946,
   473.1169,
   499.5471,
   521.7943,
   548.1607,
   567.6057,
   591.9247,
   622.7909,
   654.9354,
   684.2776,
   710.4514,
   739.7426,
   784.4957,
   839.1406,
   887.9501,
   938.0457};
   Double_t occ_r_sctdisk_6_fy1191[18] = {
   0.008300781,
   0.00859375,
   0.005566406,
   0.007128906,
   0.002832031,
   0.004101562,
   0.004980469,
   0.001855469,
   0.008691407,
   0.002246094,
   0.002294922,
   0.001708984,
   0.002734375,
   0.003515625,
   0.005322266,
   0.003662109,
   0.002050781,
   0.003515625};
   Double_t occ_r_sctdisk_6_fex1191[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t occ_r_sctdisk_6_fey1191[18] = {
   1.096549e-05,
   7.991378e-06,
   7.499872e-06,
   6.15215e-06,
   1.219726e-05,
   1.335366e-05,
   2.096483e-05,
   2.235432e-05,
   1.462471e-05,
   1.199287e-05,
   5.460009e-06,
   7.458619e-06,
   7.888772e-06,
   1.102173e-05,
   4.389155e-05,
   8.457751e-06,
   4.636747e-06,
   4.597434e-06};
   gre = new TGraphErrors(18,occ_r_sctdisk_6_fx1191,occ_r_sctdisk_6_fy1191,occ_r_sctdisk_6_fex1191,occ_r_sctdisk_6_fey1191);
   gre->SetName("occ_r_sctdisk_6");
   gre->SetTitle("Disk 0");
   gre->SetFillColor(1);
   gre->SetLineStyle(2);
   gre->SetMarkerStyle(21);
   
   TH1F *Graph_occ_r_sctdisk_61191 = new TH1F("Graph_occ_r_sctdisk_61191","Disk 0",100,340.3115,992.3852);
   Graph_occ_r_sctdisk_61191->SetMinimum(0.001001075);
   Graph_occ_r_sctdisk_61191->SetMaximum(0.009406482);
   Graph_occ_r_sctdisk_61191->SetDirectory(0);
   Graph_occ_r_sctdisk_61191->SetStats(0);
   gre->SetHistogram(Graph_occ_r_sctdisk_61191);
   
   multigraph->Add(gre,"lp");
   
   Double_t occ_r_sctdisk_7_fx1192[18] = {
   394.472,
   416.1751,
   442.8187,
   473.1606,
   499.5253,
   521.9348,
   547.8596,
   567.5008,
   591.6261,
   623.0983,
   654.912,
   684.2283,
   710.3389,
   739.6007,
   784.4897,
   839.0643,
   887.7781,
   937.9369};
   Double_t occ_r_sctdisk_7_fy1192[18] = {
   0.004980469,
   0.006738281,
   0.008105469,
   0.008203125,
   0.00625,
   0.005273437,
   0.00546875,
   0.003027344,
   0.003613281,
   0.005273437,
   0.002001953,
   0.002783203,
   0.00234375,
   0.001953125,
   0.005029297,
   0.003564453,
   0.001171875,
   0.002636719};
   Double_t occ_r_sctdisk_7_fex1192[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t occ_r_sctdisk_7_fey1192[18] = {
   9.882304e-06,
   8.954094e-06,
   1.622766e-05,
   7.637733e-06,
   1.267719e-05,
   9.245099e-06,
   8.593159e-06,
   1.657736e-05,
   1.011284e-05,
   1.16885e-05,
   9.529896e-06,
   4.650039e-06,
   9.516689e-06,
   5.891713e-06,
   7.216214e-06,
   5.628728e-06,
   1.147597e-05,
   3.853585e-06};
   gre = new TGraphErrors(18,occ_r_sctdisk_7_fx1192,occ_r_sctdisk_7_fy1192,occ_r_sctdisk_7_fex1192,occ_r_sctdisk_7_fey1192);
   gre->SetName("occ_r_sctdisk_7");
   gre->SetTitle("Disk 1");
   gre->SetFillColor(1);
   gre->SetLineColor(2);
   gre->SetLineStyle(2);
   gre->SetMarkerColor(2);
   gre->SetMarkerStyle(21);
   
   TH1F *Graph_occ_r_sctdisk_71192 = new TH1F("Graph_occ_r_sctdisk_71192","Disk 1",100,340.1256,992.2834);
   Graph_occ_r_sctdisk_71192->SetMinimum(0.0004553626);
   Graph_occ_r_sctdisk_71192->SetMaximum(0.008915799);
   Graph_occ_r_sctdisk_71192->SetDirectory(0);
   Graph_occ_r_sctdisk_71192->SetStats(0);
   gre->SetHistogram(Graph_occ_r_sctdisk_71192);
   
   multigraph->Add(gre,"lp");
   
   Double_t occ_r_sctdisk_8_fx1193[18] = {
   394.5248,
   416.1408,
   442.6912,
   472.9475,
   499.7349,
   522.359,
   547.7468,
   567.748,
   592.2082,
   622.8531,
   654.9347,
   684.1058,
   710.455,
   739.7698,
   784.429,
   839.1171,
   887.8689,
   938.1837};
   Double_t occ_r_sctdisk_8_fy1193[18] = {
   0.005761719,
   0.006738281,
   0.006738281,
   0.007128906,
   0.002441406,
   0.007421875,
   0.004980469,
   0.003808594,
   0.006640625,
   0.00546875,
   0.001806641,
   0.001074219,
   0.001464844,
   0.002929688,
   0.004589844,
   0.003613281,
   0.001953125,
   0.002783203};
   Double_t occ_r_sctdisk_8_fex1193[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t occ_r_sctdisk_8_fey1193[18] = {
   8.366306e-06,
   9.151666e-06,
   6.319293e-06,
   1.293536e-05,
   1.069935e-05,
   6.716245e-06,
   9.595348e-06,
   1.360759e-05,
   8.668922e-06,
   9.215136e-06,
   3.522033e-06,
   6.510417e-06,
   3.523449e-06,
   5.62852e-06,
   6.245601e-06,
   5.048279e-06,
   2.593812e-06,
   1.363112e-05};
   gre = new TGraphErrors(18,occ_r_sctdisk_8_fx1193,occ_r_sctdisk_8_fy1193,occ_r_sctdisk_8_fex1193,occ_r_sctdisk_8_fey1193);
   gre->SetName("occ_r_sctdisk_8");
   gre->SetTitle("Disk 2");
   gre->SetFillColor(1);
   gre->SetLineColor(3);
   gre->SetLineStyle(2);
   gre->SetMarkerColor(3);
   gre->SetMarkerStyle(21);
   
   TH1F *Graph_occ_r_sctdisk_81193 = new TH1F("Graph_occ_r_sctdisk_81193","Disk 2",100,340.1589,992.5496);
   Graph_occ_r_sctdisk_81193->SetMinimum(0.00043162);
   Graph_occ_r_sctdisk_81193->SetMaximum(0.00806468);
   Graph_occ_r_sctdisk_81193->SetDirectory(0);
   Graph_occ_r_sctdisk_81193->SetStats(0);
   gre->SetHistogram(Graph_occ_r_sctdisk_81193);
   
   multigraph->Add(gre,"lp");
   
   Double_t occ_r_sctdisk_9_fx1194[18] = {
   394.8511,
   416.0346,
   442.8318,
   473.0781,
   499.4621,
   522.2874,
   547.8464,
   567.2081,
   592.0955,
   623.1335,
   655.0034,
   684.2133,
   710.4826,
   739.649,
   784.5347,
   839.101,
   887.7556,
   937.9823};
   Double_t occ_r_sctdisk_9_fy1194[18] = {
   0.005566406,
   0.00546875,
   0.008300781,
   0.009863282,
   0.002929688,
   0.006738281,
   0.005175781,
   0.002929688,
   0.007226563,
   0.007519531,
   0.003417969,
   0.003808594,
   0.002246094,
   0.002294922,
   0.003027344,
   0.002880859,
   0.002197266,
   0.003466797};
   Double_t occ_r_sctdisk_9_fex1194[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t occ_r_sctdisk_9_fey1194[18] = {
   5.209791e-06,
   6.728297e-06,
   9.048264e-06,
   5.13695e-06,
   1.55143e-05,
   9.404128e-06,
   1.379293e-05,
   1.643857e-05,
   1.396297e-05,
   5.997229e-06,
   5.431656e-06,
   1.847359e-05,
   4.18243e-06,
   3.198449e-06,
   5.722817e-06,
   4.474266e-06,
   1.58666e-06,
   4.341216e-06};
   gre = new TGraphErrors(18,occ_r_sctdisk_9_fx1194,occ_r_sctdisk_9_fy1194,occ_r_sctdisk_9_fex1194,occ_r_sctdisk_9_fey1194);
   gre->SetName("occ_r_sctdisk_9");
   gre->SetTitle("Disk 3");
   gre->SetFillColor(1);
   gre->SetLineColor(4);
   gre->SetLineStyle(2);
   gre->SetMarkerColor(4);
   gre->SetMarkerStyle(21);
   
   TH1F *Graph_occ_r_sctdisk_91194 = new TH1F("Graph_occ_r_sctdisk_91194","Disk 3",100,340.538,992.2954);
   Graph_occ_r_sctdisk_91194->SetMinimum(0.001428405);
   Graph_occ_r_sctdisk_91194->SetMaximum(0.01063569);
   Graph_occ_r_sctdisk_91194->SetDirectory(0);
   Graph_occ_r_sctdisk_91194->SetStats(0);
   gre->SetHistogram(Graph_occ_r_sctdisk_91194);
   
   multigraph->Add(gre,"lp");
   
   Double_t occ_r_sctdisk_10_fx1195[18] = {
   394.7114,
   416.1909,
   442.7821,
   473.0434,
   499.6741,
   522.2058,
   547.5615,
   567.5627,
   592.0118,
   622.602,
   655.053,
   684.2001,
   710.547,
   739.6685,
   784.5911,
   839.1618,
   888.1649,
   938.07};
   Double_t occ_r_sctdisk_10_fy1195[18] = {
   0.01035156,
   0.006738281,
   0.007324219,
   0.007714844,
   0.004785156,
   0.01152344,
   0.006640625,
   0.001757813,
   0.003222656,
   0.004589844,
   0.002832031,
   0.002148438,
   0.002294922,
   0.003564453,
   0.004785156,
   0.003564453,
   0.002441406,
   0.003417969};
   Double_t occ_r_sctdisk_10_fex1195[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t occ_r_sctdisk_10_fey1195[18] = {
   9.41046e-06,
   6.319293e-06,
   6.194143e-06,
   7.644768e-06,
   7.792334e-06,
   1.36692e-05,
   1.212859e-05,
   1.043989e-05,
   8.663283e-06,
   1.385542e-05,
   2.729007e-06,
   4.847614e-06,
   2.728763e-06,
   5.459375e-06,
   2.532546e-06,
   5.406963e-06,
   4.717327e-06,
   4.215783e-06};
   gre = new TGraphErrors(18,occ_r_sctdisk_10_fx1195,occ_r_sctdisk_10_fy1195,occ_r_sctdisk_10_fex1195,occ_r_sctdisk_10_fey1195);
   gre->SetName("occ_r_sctdisk_10");
   gre->SetTitle("Disk 4");
   gre->SetFillColor(1);
   gre->SetLineColor(5);
   gre->SetLineStyle(2);
   gre->SetMarkerColor(5);
   gre->SetMarkerStyle(21);
   
   TH1F *Graph_occ_r_sctdisk_101195 = new TH1F("Graph_occ_r_sctdisk_101195","Disk 4",100,340.3755,992.4059);
   Graph_occ_r_sctdisk_101195->SetMinimum(0.0007683993);
   Graph_occ_r_sctdisk_101195->SetMaximum(0.01251608);
   Graph_occ_r_sctdisk_101195->SetDirectory(0);
   Graph_occ_r_sctdisk_101195->SetStats(0);
   gre->SetHistogram(Graph_occ_r_sctdisk_101195);
   
   multigraph->Add(gre,"lp");
   
   Double_t occ_r_sctdisk_11_fx1196[18] = {
   394.6143,
   415.998,
   442.6399,
   473.0866,
   500.0164,
   522.356,
   547.9612,
   567.2525,
   591.8386,
   622.6384,
   654.8702,
   684.2174,
   710.4064,
   739.6919,
   784.4448,
   839.0448,
   887.8703,
   938.0878};
   Double_t occ_r_sctdisk_11_fy1196[18] = {
   0.005566406,
   0.008105469,
   0.009374999,
   0.007910157,
   0.00546875,
   0.0078125,
   0.007617188,
   0.002441406,
   0.005371094,
   0.008300781,
   0.002587891,
   0.003027344,
   0.001806641,
   0.002685547,
   0.004296875,
   0.004785156,
   0.002929688,
   0.004052734};
   Double_t occ_r_sctdisk_11_fex1196[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t occ_r_sctdisk_11_fey1196[18] = {
   7.501711e-06,
   8.397814e-06,
   3.582479e-06,
   3.832921e-06,
   1.054802e-05,
   5.327586e-06,
   8.65537e-06,
   1.678653e-05,
   5.411287e-06,
   7.381699e-06,
   4.568661e-06,
   4.743304e-06,
   3.834977e-06,
   2.903884e-06,
   3.476628e-06,
   2.457925e-06,
   3.977997e-06,
   4.108258e-06};
   gre = new TGraphErrors(18,occ_r_sctdisk_11_fx1196,occ_r_sctdisk_11_fy1196,occ_r_sctdisk_11_fex1196,occ_r_sctdisk_11_fey1196);
   gre->SetName("occ_r_sctdisk_11");
   gre->SetTitle("Disk 5");
   gre->SetFillColor(1);
   gre->SetLineColor(6);
   gre->SetLineStyle(2);
   gre->SetMarkerColor(6);
   gre->SetMarkerStyle(21);
   
   TH1F *Graph_occ_r_sctdisk_111196 = new TH1F("Graph_occ_r_sctdisk_111196","Disk 5",100,340.2669,992.4351);
   Graph_occ_r_sctdisk_111196->SetMinimum(0.001045228);
   Graph_occ_r_sctdisk_111196->SetMaximum(0.01013616);
   Graph_occ_r_sctdisk_111196->SetDirectory(0);
   Graph_occ_r_sctdisk_111196->SetStats(0);
   gre->SetHistogram(Graph_occ_r_sctdisk_111196);
   
   multigraph->Add(gre,"lp");
   multigraph->Draw("Apl");
   multigraph->GetXaxis()->SetTitle("r [mm]");
   multigraph->GetYaxis()->SetTitle("Occupancy in percent");
   
   TLegend *leg = new TLegend(0.14,0.62,0.34,0.87,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextFont(62);
   leg->SetTextSize(0.04);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(10);
   leg->SetFillStyle(0);
   TLegendEntry *entry=leg->AddEntry("NULL","Sct Disk (left)","h");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("occ_r_sctdisk_0","Disk 0","lp");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("occ_r_sctdisk_1","Disk 1","lp");
   entry->SetLineColor(2);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(2);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("occ_r_sctdisk_2","Disk 2","lp");
   entry->SetLineColor(3);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(3);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("occ_r_sctdisk_3","Disk 3","lp");
   entry->SetLineColor(4);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(4);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("occ_r_sctdisk_4","Disk 4","lp");
   entry->SetLineColor(5);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(5);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("occ_r_sctdisk_5","Disk 5","lp");
   entry->SetLineColor(6);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(6);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   leg->Draw();
   
   leg = new TLegend(0.27,0.62,0.47,0.87,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextFont(62);
   leg->SetTextSize(0.04);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(10);
   leg->SetFillStyle(0);
   entry=leg->AddEntry("NULL","  (right)","h");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("occ_r_sctdisk_6","Disk 0","lp");
   entry->SetLineColor(1);
   entry->SetLineStyle(2);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("occ_r_sctdisk_7","Disk 1","lp");
   entry->SetLineColor(2);
   entry->SetLineStyle(2);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(2);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("occ_r_sctdisk_8","Disk 2","lp");
   entry->SetLineColor(3);
   entry->SetLineStyle(2);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(3);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("occ_r_sctdisk_9","Disk 3","lp");
   entry->SetLineColor(4);
   entry->SetLineStyle(2);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(4);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("occ_r_sctdisk_10","Disk 4","lp");
   entry->SetLineColor(5);
   entry->SetLineStyle(2);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(5);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("occ_r_sctdisk_11","Disk 5","lp");
   entry->SetLineColor(6);
   entry->SetLineStyle(2);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(6);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   leg->Draw();
   c1_n32->Modified();
   c1_n32->cd();
   c1_n32->SetSelected(c1_n32);
}
