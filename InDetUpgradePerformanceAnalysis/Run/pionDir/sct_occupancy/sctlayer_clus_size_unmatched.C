void sctlayer_clus_size_unmatched()
{
//=========Macro generated from canvas: c1_n55/c1_n55
//=========  (Tue Apr 19 16:53:29 2016) by ROOT version6.04/12
   TCanvas *c1_n55 = new TCanvas("c1_n55", "c1_n55",0,0,700,500);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   c1_n55->SetHighLightColor(2);
   c1_n55->Range(-6.25,-125.2706,56.25,1127.435);
   c1_n55->SetFillColor(0);
   c1_n55->SetBorderMode(0);
   c1_n55->SetBorderSize(2);
   c1_n55->SetTickx(1);
   c1_n55->SetTicky(1);
   c1_n55->SetTopMargin(0.05);
   c1_n55->SetFrameBorderMode(0);
   c1_n55->SetFrameBorderMode(0);
   
   TH1D *sctlayer_clus_size_unmatched__1 = new TH1D("sctlayer_clus_size_unmatched__1","Cluster size - unmatched clusters",50,0,50);
   sctlayer_clus_size_unmatched__1->SetBinContent(2,968);
   sctlayer_clus_size_unmatched__1->SetBinContent(3,647);
   sctlayer_clus_size_unmatched__1->SetBinContent(4,284);
   sctlayer_clus_size_unmatched__1->SetBinContent(5,136);
   sctlayer_clus_size_unmatched__1->SetBinContent(6,90);
   sctlayer_clus_size_unmatched__1->SetBinContent(7,49);
   sctlayer_clus_size_unmatched__1->SetBinContent(8,31);
   sctlayer_clus_size_unmatched__1->SetBinContent(9,17);
   sctlayer_clus_size_unmatched__1->SetBinContent(10,12);
   sctlayer_clus_size_unmatched__1->SetBinContent(11,9);
   sctlayer_clus_size_unmatched__1->SetBinContent(12,9);
   sctlayer_clus_size_unmatched__1->SetBinContent(13,5);
   sctlayer_clus_size_unmatched__1->SetBinContent(14,2);
   sctlayer_clus_size_unmatched__1->SetBinContent(15,4);
   sctlayer_clus_size_unmatched__1->SetBinContent(17,4);
   sctlayer_clus_size_unmatched__1->SetBinContent(18,2);
   sctlayer_clus_size_unmatched__1->SetBinContent(20,1);
   sctlayer_clus_size_unmatched__1->SetBinContent(22,1);
   sctlayer_clus_size_unmatched__1->SetBinContent(25,1);
   sctlayer_clus_size_unmatched__1->SetBinContent(26,1);
   sctlayer_clus_size_unmatched__1->SetBinContent(27,1);
   sctlayer_clus_size_unmatched__1->SetBinContent(29,1);
   sctlayer_clus_size_unmatched__1->SetBinContent(51,1);
   sctlayer_clus_size_unmatched__1->SetMaximum(1064.8);
   sctlayer_clus_size_unmatched__1->SetEntries(2276);
   sctlayer_clus_size_unmatched__1->SetStats(0);
   sctlayer_clus_size_unmatched__1->GetXaxis()->SetTitle("Events");
   sctlayer_clus_size_unmatched__1->GetYaxis()->SetTitle("Cluster Size (No. Hits)");
   sctlayer_clus_size_unmatched__1->Draw("");
   
   TLegend *leg = new TLegend(0.48,0.65,0.9,0.9,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextFont(62);
   leg->SetTextSize(0.04);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(10);
   leg->SetFillStyle(0);
   TLegendEntry *entry=leg->AddEntry("sctlayer_clus_size_unmatched","Pixel Occupancies","l");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   leg->Draw();
   c1_n55->Modified();
   c1_n55->cd();
   c1_n55->SetSelected(c1_n55);
}
