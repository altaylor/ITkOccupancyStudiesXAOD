void clus_z_pixlayer_1_pos()
{
//=========Macro generated from canvas: c1_n43/c1_n43
//=========  (Tue Apr 26 00:12:46 2016) by ROOT version6.04/12
   TCanvas *c1_n43 = new TCanvas("c1_n43", "c1_n43",0,0,700,500);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   c1_n43->SetHighLightColor(2);
   c1_n43->Range(-nan,-245.375,-nan,2208.375);
   c1_n43->SetFillColor(0);
   c1_n43->SetBorderMode(0);
   c1_n43->SetBorderSize(2);
   c1_n43->SetTickx(1);
   c1_n43->SetTicky(1);
   c1_n43->SetFrameBorderMode(0);
   c1_n43->SetFrameBorderMode(0);
   
   TMultiGraph *multigraph = new TMultiGraph();
   multigraph->SetName("");
   multigraph->SetTitle("");
   
   Double_t clus_z_pixlayer_1_pos_fx1271[14] = {
   -nan,
   20.59842,
   61.8738,
   103.415,
   144.6811,
   186.4653,
   227.6544,
   269.5872,
   310.6381,
   351.8254,
   393.6407,
   436.1101,
   477.5812,
   519.0551};
   Double_t clus_z_pixlayer_1_pos_fy1271[14] = {
   0,
   1510,
   1473.6,
   1268,
   1135.6,
   1014.8,
   841.2,
   822.8,
   835.9999,
   753.2,
   724,
   702,
   706.4,
   696.4};
   Double_t clus_z_pixlayer_1_pos_fex1271[14] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t clus_z_pixlayer_1_pos_fey1271[14] = {
   0,
   24.57641,
   24.27839,
   22.5211,
   21.31291,
   20.14746,
   18.34339,
   18.14166,
   18.28661,
   17.35742,
   17.01764,
   16.75709,
   16.80952,
   16.69012};
   TGraphErrors *gre = new TGraphErrors(14,clus_z_pixlayer_1_pos_fx1271,clus_z_pixlayer_1_pos_fy1271,clus_z_pixlayer_1_pos_fex1271,clus_z_pixlayer_1_pos_fey1271);
   gre->SetName("clus_z_pixlayer_1_pos");
   gre->SetTitle("Layer 1 (z>0)");
   
   TH1F *Graph_clus_z_pixlayer_1_pos1271 = new TH1F("Graph_clus_z_pixlayer_1_pos1271","Layer 1 (z>0)",100,-nan,-nan);
   Graph_clus_z_pixlayer_1_pos1271->SetMinimum(0);
   Graph_clus_z_pixlayer_1_pos1271->SetMaximum(1688.034);
   Graph_clus_z_pixlayer_1_pos1271->SetDirectory(0);
   Graph_clus_z_pixlayer_1_pos1271->SetStats(0);
   gre->SetHistogram(Graph_clus_z_pixlayer_1_pos1271);
   
   multigraph->Add(gre,"");
   multigraph->Draw("Apl");
   
   TLegend *leg = new TLegend(0.48,0.65,0.9,0.9,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextFont(62);
   leg->SetTextSize(0.04);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(10);
   leg->SetFillStyle(0);
   TLegendEntry *entry=leg->AddEntry("clus_z_pixlayer_1_pos","Pixel Occupancies","lp");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(1);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   leg->Draw();
   c1_n43->Modified();
   c1_n43->cd();
   c1_n43->SetSelected(c1_n43);
}
