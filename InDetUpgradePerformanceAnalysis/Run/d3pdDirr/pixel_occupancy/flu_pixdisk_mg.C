void flu_pixdisk_mg()
{
//=========Macro generated from canvas: c1_n18/c1_n18
//=========  (Tue Apr 26 00:12:43 2016) by ROOT version6.04/12
   TCanvas *c1_n18 = new TCanvas("c1_n18", "c1_n18",0,0,700,500);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   c1_n18->SetHighLightColor(2);
   c1_n18->Range(-0.375,0.02444572,2.375,0.7329712);
   c1_n18->SetFillColor(0);
   c1_n18->SetBorderMode(0);
   c1_n18->SetBorderSize(2);
   c1_n18->SetTickx(1);
   c1_n18->SetTicky(1);
   c1_n18->SetFrameBorderMode(0);
   c1_n18->SetFrameBorderMode(0);
   
   TMultiGraph *multigraph = new TMultiGraph();
   multigraph->SetName("flu_pixdisk_mg");
   multigraph->SetTitle("");
   
   Double_t flu_pixdisk_0_fx1097[3] = {
   0,
   1,
   2};
   Double_t flu_pixdisk_0_fy1097[3] = {
   0.1887235,
   0.2583002,
   0.1906746};
   Double_t flu_pixdisk_0_fex1097[3] = {
   0,
   0,
   0};
   Double_t flu_pixdisk_0_fey1097[3] = {
   0,
   0,
   0};
   TGraphErrors *gre = new TGraphErrors(3,flu_pixdisk_0_fx1097,flu_pixdisk_0_fy1097,flu_pixdisk_0_fex1097,flu_pixdisk_0_fey1097);
   gre->SetName("flu_pixdisk_0");
   gre->SetTitle("Disk 0");
   gre->SetFillColor(1);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_flu_pixdisk_01097 = new TH1F("Graph_flu_pixdisk_01097","Disk 0",100,0,2.2);
   Graph_flu_pixdisk_01097->SetMinimum(0.1817659);
   Graph_flu_pixdisk_01097->SetMaximum(0.2652579);
   Graph_flu_pixdisk_01097->SetDirectory(0);
   Graph_flu_pixdisk_01097->SetStats(0);
   gre->SetHistogram(Graph_flu_pixdisk_01097);
   
   multigraph->Add(gre,"lp");
   
   Double_t flu_pixdisk_1_fx1098[3] = {
   0,
   1,
   2};
   Double_t flu_pixdisk_1_fy1098[3] = {
   0.1607143,
   0.2546131,
   0.1424851};
   Double_t flu_pixdisk_1_fex1098[3] = {
   0,
   0,
   0};
   Double_t flu_pixdisk_1_fey1098[3] = {
   0,
   0,
   0};
   gre = new TGraphErrors(3,flu_pixdisk_1_fx1098,flu_pixdisk_1_fy1098,flu_pixdisk_1_fex1098,flu_pixdisk_1_fey1098);
   gre->SetName("flu_pixdisk_1");
   gre->SetTitle("Disk 1");
   gre->SetFillColor(1);
   gre->SetLineColor(2);
   gre->SetMarkerColor(2);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_flu_pixdisk_11098 = new TH1F("Graph_flu_pixdisk_11098","Disk 1",100,0,2.2);
   Graph_flu_pixdisk_11098->SetMinimum(0.1312723);
   Graph_flu_pixdisk_11098->SetMaximum(0.2658259);
   Graph_flu_pixdisk_11098->SetDirectory(0);
   Graph_flu_pixdisk_11098->SetStats(0);
   gre->SetHistogram(Graph_flu_pixdisk_11098);
   
   multigraph->Add(gre,"lp");
   
   Double_t flu_pixdisk_2_fx1099[3] = {
   0,
   1,
   2};
   Double_t flu_pixdisk_2_fy1099[3] = {
   0.1244444,
   0.1559524,
   0.1165476};
   Double_t flu_pixdisk_2_fex1099[3] = {
   0,
   0,
   0};
   Double_t flu_pixdisk_2_fey1099[3] = {
   0,
   0,
   0};
   gre = new TGraphErrors(3,flu_pixdisk_2_fx1099,flu_pixdisk_2_fy1099,flu_pixdisk_2_fex1099,flu_pixdisk_2_fey1099);
   gre->SetName("flu_pixdisk_2");
   gre->SetTitle("Disk 2");
   gre->SetFillColor(1);
   gre->SetLineColor(3);
   gre->SetMarkerColor(3);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_flu_pixdisk_21099 = new TH1F("Graph_flu_pixdisk_21099","Disk 2",100,0,2.2);
   Graph_flu_pixdisk_21099->SetMinimum(0.1126071);
   Graph_flu_pixdisk_21099->SetMaximum(0.1598929);
   Graph_flu_pixdisk_21099->SetDirectory(0);
   Graph_flu_pixdisk_21099->SetStats(0);
   gre->SetHistogram(Graph_flu_pixdisk_21099);
   
   multigraph->Add(gre,"lp");
   
   Double_t flu_pixdisk_3_fx1100[3] = {
   0,
   1,
   2};
   Double_t flu_pixdisk_3_fy1100[3] = {
   0.4303572,
   0.501951,
   0.4282407};
   Double_t flu_pixdisk_3_fex1100[3] = {
   0,
   0,
   0};
   Double_t flu_pixdisk_3_fey1100[3] = {
   0,
   0,
   0};
   gre = new TGraphErrors(3,flu_pixdisk_3_fx1100,flu_pixdisk_3_fy1100,flu_pixdisk_3_fex1100,flu_pixdisk_3_fey1100);
   gre->SetName("flu_pixdisk_3");
   gre->SetTitle("Disk 0");
   gre->SetFillColor(1);
   gre->SetLineStyle(2);
   gre->SetMarkerStyle(21);
   
   TH1F *Graph_flu_pixdisk_31100 = new TH1F("Graph_flu_pixdisk_31100","Disk 0",100,0,2.2);
   Graph_flu_pixdisk_31100->SetMinimum(0.4208697);
   Graph_flu_pixdisk_31100->SetMaximum(0.5093221);
   Graph_flu_pixdisk_31100->SetDirectory(0);
   Graph_flu_pixdisk_31100->SetStats(0);
   gre->SetHistogram(Graph_flu_pixdisk_31100);
   
   multigraph->Add(gre,"lp");
   
   Double_t flu_pixdisk_4_fx1101[3] = {
   0,
   1,
   2};
   Double_t flu_pixdisk_4_fy1101[3] = {
   0.3461061,
   0.3073165,
   0.2896081};
   Double_t flu_pixdisk_4_fex1101[3] = {
   0,
   0,
   0};
   Double_t flu_pixdisk_4_fey1101[3] = {
   0,
   0,
   0};
   gre = new TGraphErrors(3,flu_pixdisk_4_fx1101,flu_pixdisk_4_fy1101,flu_pixdisk_4_fex1101,flu_pixdisk_4_fey1101);
   gre->SetName("flu_pixdisk_4");
   gre->SetTitle("Disk 1");
   gre->SetFillColor(1);
   gre->SetLineColor(2);
   gre->SetLineStyle(2);
   gre->SetMarkerColor(2);
   gre->SetMarkerStyle(21);
   
   TH1F *Graph_flu_pixdisk_41101 = new TH1F("Graph_flu_pixdisk_41101","Disk 1",100,0,2.2);
   Graph_flu_pixdisk_41101->SetMinimum(0.2839583);
   Graph_flu_pixdisk_41101->SetMaximum(0.3517559);
   Graph_flu_pixdisk_41101->SetDirectory(0);
   Graph_flu_pixdisk_41101->SetStats(0);
   gre->SetHistogram(Graph_flu_pixdisk_41101);
   
   multigraph->Add(gre,"lp");
   
   Double_t flu_pixdisk_5_fx1102[3] = {
   0,
   1,
   2};
   Double_t flu_pixdisk_5_fy1102[3] = {
   0.2723016,
   0.2510714,
   0.1146627};
   Double_t flu_pixdisk_5_fex1102[3] = {
   0,
   0,
   0};
   Double_t flu_pixdisk_5_fey1102[3] = {
   0,
   0,
   0};
   gre = new TGraphErrors(3,flu_pixdisk_5_fx1102,flu_pixdisk_5_fy1102,flu_pixdisk_5_fex1102,flu_pixdisk_5_fey1102);
   gre->SetName("flu_pixdisk_5");
   gre->SetTitle("Disk 2");
   gre->SetFillColor(1);
   gre->SetLineColor(3);
   gre->SetLineStyle(2);
   gre->SetMarkerColor(3);
   gre->SetMarkerStyle(21);
   
   TH1F *Graph_flu_pixdisk_51102 = new TH1F("Graph_flu_pixdisk_51102","Disk 2",100,0,2.2);
   Graph_flu_pixdisk_51102->SetMinimum(0.0988988);
   Graph_flu_pixdisk_51102->SetMaximum(0.2880655);
   Graph_flu_pixdisk_51102->SetDirectory(0);
   Graph_flu_pixdisk_51102->SetStats(0);
   gre->SetHistogram(Graph_flu_pixdisk_51102);
   
   multigraph->Add(gre,"lp");
   multigraph->Draw("Apl");
   multigraph->GetXaxis()->SetTitle("moduleID");
   multigraph->GetYaxis()->SetTitle("Hits per mm^{2}");
   
   TLegend *leg = new TLegend(0.14,0.62,0.34,0.87,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextFont(62);
   leg->SetTextSize(0.04);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(10);
   leg->SetFillStyle(0);
   TLegendEntry *entry=leg->AddEntry("NULL","Pixel Disk (left)","h");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("flu_pixdisk_0","Disk 0","lp");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("flu_pixdisk_1","Disk 1","lp");
   entry->SetLineColor(2);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(2);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("flu_pixdisk_2","Disk 2","lp");
   entry->SetLineColor(3);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(3);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   leg->Draw();
   
   leg = new TLegend(0.27,0.62,0.47,0.87,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextFont(62);
   leg->SetTextSize(0.04);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(10);
   leg->SetFillStyle(0);
   entry=leg->AddEntry("NULL","  (right)","h");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("flu_pixdisk_3","Disk 0","lp");
   entry->SetLineColor(1);
   entry->SetLineStyle(2);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("flu_pixdisk_4","Disk 1","lp");
   entry->SetLineColor(2);
   entry->SetLineStyle(2);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(2);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("flu_pixdisk_5","Disk 2","lp");
   entry->SetLineColor(3);
   entry->SetLineStyle(2);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(3);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   leg->Draw();
   c1_n18->Modified();
   c1_n18->cd();
   c1_n18->SetSelected(c1_n18);
}
