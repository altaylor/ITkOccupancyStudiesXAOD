void clus_z_pixlayer_2_pos()
{
//=========Macro generated from canvas: c1_n49/c1_n49
//=========  (Tue Apr 26 00:12:47 2016) by ROOT version6.04/12
   TCanvas *c1_n49 = new TCanvas("c1_n49", "c1_n49",0,0,700,500);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   c1_n49->SetHighLightColor(2);
   c1_n49->Range(-20.73658,490.6381,433.2676,1196.374);
   c1_n49->SetFillColor(0);
   c1_n49->SetBorderMode(0);
   c1_n49->SetBorderSize(2);
   c1_n49->SetTickx(1);
   c1_n49->SetTicky(1);
   c1_n49->SetFrameBorderMode(0);
   c1_n49->SetFrameBorderMode(0);
   
   TMultiGraph *multigraph = new TMultiGraph();
   multigraph->SetName("");
   multigraph->SetTitle("");
   
   Double_t clus_z_pixlayer_2_pos_fx1288[9] = {
   41.17309,
   82.65405,
   123.4875,
   165.4787,
   206.8579,
   248.1609,
   288.7308,
   330.394,
   371.358};
   Double_t clus_z_pixlayer_2_pos_fy1288[9] = {
   866,
   824,
   758.4,
   728.8,
   704,
   626.8,
   668.8,
   592,
   621.2};
   Double_t clus_z_pixlayer_2_pos_fex1288[9] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t clus_z_pixlayer_2_pos_fey1288[9] = {
   18.61182,
   18.15489,
   17.41723,
   17.07396,
   16.78094,
   15.83414,
   16.35604,
   15.38831,
   15.76325};
   TGraphErrors *gre = new TGraphErrors(9,clus_z_pixlayer_2_pos_fx1288,clus_z_pixlayer_2_pos_fy1288,clus_z_pixlayer_2_pos_fex1288,clus_z_pixlayer_2_pos_fey1288);
   gre->SetName("clus_z_pixlayer_2_pos");
   gre->SetTitle("Layer 2 (z>0)");
   
   TH1F *Graph_clus_z_pixlayer_2_pos1288 = new TH1F("Graph_clus_z_pixlayer_2_pos1288","Layer 2 (z>0)",100,8.154604,404.3765);
   Graph_clus_z_pixlayer_2_pos1288->SetMinimum(545.8117);
   Graph_clus_z_pixlayer_2_pos1288->SetMaximum(915.4118);
   Graph_clus_z_pixlayer_2_pos1288->SetDirectory(0);
   Graph_clus_z_pixlayer_2_pos1288->SetStats(0);
   gre->SetHistogram(Graph_clus_z_pixlayer_2_pos1288);
   
   multigraph->Add(gre,"");
   multigraph->Draw("Apl");
   
   TLegend *leg = new TLegend(0.48,0.65,0.9,0.9,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextFont(62);
   leg->SetTextSize(0.04);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(10);
   leg->SetFillStyle(0);
   TLegendEntry *entry=leg->AddEntry("clus_z_pixlayer_2_pos","Pixel Occupancies","lp");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(1);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   leg->Draw();
   c1_n49->Modified();
   c1_n49->cd();
   c1_n49->SetSelected(c1_n49);
}
