void clus_z_pixlayer_3_pos()
{
//=========Macro generated from canvas: c1_n44/c1_n44
//=========  (Tue Apr 26 00:12:46 2016) by ROOT version6.04/12
   TCanvas *c1_n44 = new TCanvas("c1_n44", "c1_n44",0,0,700,500);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   c1_n44->SetHighLightColor(2);
   c1_n44->Range(-20.74629,476.9056,433.3254,870.8771);
   c1_n44->SetFillColor(0);
   c1_n44->SetBorderMode(0);
   c1_n44->SetBorderSize(2);
   c1_n44->SetTickx(1);
   c1_n44->SetTicky(1);
   c1_n44->SetFrameBorderMode(0);
   c1_n44->SetFrameBorderMode(0);
   
   TMultiGraph *multigraph = new TMultiGraph();
   multigraph->SetName("");
   multigraph->SetTitle("");
   
   Double_t clus_z_pixlayer_3_pos_fx1272[9] = {
   41.17258,
   82.56261,
   123.4788,
   164.9611,
   206.3639,
   247.109,
   289.1987,
   329.658,
   371.4065};
   Double_t clus_z_pixlayer_3_pos_fy1272[9] = {
   617.2,
   639.6,
   636.8,
   565.2,
   620,
   566.4,
   559.6,
   582,
   537.6};
   Double_t clus_z_pixlayer_3_pos_fex1272[9] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t clus_z_pixlayer_3_pos_fey1272[9] = {
   15.71242,
   15.995,
   15.95995,
   15.03596,
   15.74802,
   15.05191,
   14.96128,
   15.25778,
   14.66424};
   TGraphErrors *gre = new TGraphErrors(9,clus_z_pixlayer_3_pos_fx1272,clus_z_pixlayer_3_pos_fy1272,clus_z_pixlayer_3_pos_fex1272,clus_z_pixlayer_3_pos_fey1272);
   gre->SetName("clus_z_pixlayer_3_pos");
   gre->SetTitle("Layer 3 (z>0)");
   
   TH1F *Graph_clus_z_pixlayer_3_pos1272 = new TH1F("Graph_clus_z_pixlayer_3_pos1272","Layer 3 (z>0)",100,8.149185,404.4299);
   Graph_clus_z_pixlayer_3_pos1272->SetMinimum(509.6698);
   Graph_clus_z_pixlayer_3_pos1272->SetMaximum(668.8609);
   Graph_clus_z_pixlayer_3_pos1272->SetDirectory(0);
   Graph_clus_z_pixlayer_3_pos1272->SetStats(0);
   gre->SetHistogram(Graph_clus_z_pixlayer_3_pos1272);
   
   multigraph->Add(gre,"");
   multigraph->Draw("Apl");
   
   TLegend *leg = new TLegend(0.48,0.65,0.9,0.9,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextFont(62);
   leg->SetTextSize(0.04);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(10);
   leg->SetFillStyle(0);
   TLegendEntry *entry=leg->AddEntry("clus_z_pixlayer_3_pos","Pixel Occupancies","lp");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(1);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   leg->Draw();
   c1_n44->Modified();
   c1_n44->cd();
   c1_n44->SetSelected(c1_n44);
}
