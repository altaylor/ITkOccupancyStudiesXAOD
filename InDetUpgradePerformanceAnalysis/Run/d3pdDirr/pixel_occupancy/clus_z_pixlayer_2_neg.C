void clus_z_pixlayer_2_neg()
{
//=========Macro generated from canvas: c1_n46/c1_n46
//=========  (Tue Apr 26 00:12:47 2016) by ROOT version6.04/12
   TCanvas *c1_n46 = new TCanvas("c1_n46", "c1_n46",0,0,700,500);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   c1_n46->SetHighLightColor(2);
   c1_n46->Range(-20.24816,481.8703,433.1996,1187.526);
   c1_n46->SetFillColor(0);
   c1_n46->SetBorderMode(0);
   c1_n46->SetBorderSize(2);
   c1_n46->SetTickx(1);
   c1_n46->SetTicky(1);
   c1_n46->SetFrameBorderMode(0);
   c1_n46->SetFrameBorderMode(0);
   
   TMultiGraph *multigraph = new TMultiGraph();
   multigraph->SetName("");
   multigraph->SetTitle("");
   
   Double_t clus_z_pixlayer_2_neg_fx1274[9] = {
   371.3658,
   330.5065,
   289.9196,
   247.6979,
   205.975,
   164.5711,
   123.9324,
   81.97437,
   41.58564};
   Double_t clus_z_pixlayer_2_neg_fy1274[9] = {
   631.6,
   597.2,
   583.2,
   643.2,
   731.2,
   775.2,
   785.2,
   816,
   859.2};
   Double_t clus_z_pixlayer_2_neg_fex1274[9] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t clus_z_pixlayer_2_neg_fey1274[9] = {
   15.89465,
   15.45574,
   15.27351,
   16.03995,
   17.10205,
   17.60909,
   17.7223,
   18.06654,
   18.53861};
   TGraphErrors *gre = new TGraphErrors(9,clus_z_pixlayer_2_neg_fx1274,clus_z_pixlayer_2_neg_fy1274,clus_z_pixlayer_2_neg_fex1274,clus_z_pixlayer_2_neg_fey1274);
   gre->SetName("clus_z_pixlayer_2_neg");
   gre->SetTitle("Layer 2 (z<0)");
   
   TH1F *Graph_clus_z_pixlayer_2_neg1274 = new TH1F("Graph_clus_z_pixlayer_2_neg1274","Layer 2 (z<0)",100,8.607615,404.3439);
   Graph_clus_z_pixlayer_2_neg1274->SetMinimum(536.9452);
   Graph_clus_z_pixlayer_2_neg1274->SetMaximum(908.7198);
   Graph_clus_z_pixlayer_2_neg1274->SetDirectory(0);
   Graph_clus_z_pixlayer_2_neg1274->SetStats(0);
   gre->SetHistogram(Graph_clus_z_pixlayer_2_neg1274);
   
   multigraph->Add(gre,"");
   multigraph->Draw("Apl");
   
   TLegend *leg = new TLegend(0.48,0.65,0.9,0.9,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextFont(62);
   leg->SetTextSize(0.04);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(10);
   leg->SetFillStyle(0);
   TLegendEntry *entry=leg->AddEntry("clus_z_pixlayer_2_neg","Pixel Occupancies","lp");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(1);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   leg->Draw();
   c1_n46->Modified();
   c1_n46->cd();
   c1_n46->SetSelected(c1_n46);
}
