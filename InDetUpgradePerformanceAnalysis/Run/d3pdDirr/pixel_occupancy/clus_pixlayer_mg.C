void clus_pixlayer_mg()
{
//=========Macro generated from canvas: c1_n3/c1_n3
//=========  (Tue Apr 26 00:12:41 2016) by ROOT version6.04/12
   TCanvas *c1_n3 = new TCanvas("c1_n3", "c1_n3",0,0,700,500);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   c1_n3->SetHighLightColor(2);
   c1_n3->Range(-17.875,-356.8652,17.875,3211.786);
   c1_n3->SetFillColor(0);
   c1_n3->SetBorderMode(0);
   c1_n3->SetBorderSize(2);
   c1_n3->SetTickx(1);
   c1_n3->SetTicky(1);
   c1_n3->SetFrameBorderMode(0);
   c1_n3->SetFrameBorderMode(0);
   
   TMultiGraph *multigraph = new TMultiGraph();
   multigraph->SetName("clus_pixlayer_mg");
   multigraph->SetTitle("");
   
   Double_t clus_pixlayer_0_fx1011[16] = {
   -8,
   -7,
   -6,
   -5,
   -4,
   -3,
   -2,
   -1,
   1,
   2,
   3,
   4,
   5,
   6,
   7,
   8};
   Double_t clus_pixlayer_0_fy1011[16] = {
   739.6,
   750.8,
   896,
   1028.8,
   1284,
   1612,
   1900.4,
   2033.6,
   1986.8,
   1847.2,
   1536.8,
   1296.8,
   1072,
   900,
   828,
   818.4};
   Double_t clus_pixlayer_0_fex1011[16] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t clus_pixlayer_0_fey1011[16] = {
   17.2,
   17.32974,
   18.93146,
   20.28596,
   22.66274,
   25.39291,
   27.571,
   28.52087,
   28.19078,
   27.18235,
   24.79355,
   22.77542,
   20.70749,
   18.97367,
   18.1989,
   18.09309};
   TGraphErrors *gre = new TGraphErrors(16,clus_pixlayer_0_fx1011,clus_pixlayer_0_fy1011,clus_pixlayer_0_fex1011,clus_pixlayer_0_fey1011);
   gre->SetName("clus_pixlayer_0");
   gre->SetTitle("Layer 0");
   gre->SetFillColor(1);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_clus_pixlayer_01011 = new TH1F("Graph_clus_pixlayer_01011","Layer 0",100,-9.6,9.6);
   Graph_clus_pixlayer_01011->SetMinimum(588.4279);
   Graph_clus_pixlayer_01011->SetMaximum(2196.093);
   Graph_clus_pixlayer_01011->SetDirectory(0);
   Graph_clus_pixlayer_01011->SetStats(0);
   gre->SetHistogram(Graph_clus_pixlayer_01011);
   
   multigraph->Add(gre,"lp");
   
   Double_t clus_pixlayer_1_fx1012[27] = {
   -13,
   -12,
   -11,
   -10,
   -9,
   -8,
   -7,
   -6,
   -5,
   -4,
   -3,
   -2,
   -1,
   0,
   1,
   2,
   3,
   4,
   5,
   6,
   7,
   8,
   9,
   10,
   11,
   12,
   13};
   Double_t clus_pixlayer_1_fy1012[27] = {
   774.8,
   773.2,
   699.2,
   704.4,
   703.6,
   736.8,
   838.4,
   982.4,
   1040.4,
   1215.6,
   1332.4,
   1392,
   1485.6,
   0,
   1510,
   1473.6,
   1268,
   1135.6,
   1014.8,
   841.2,
   822.8,
   835.9999,
   753.2,
   724,
   702,
   706.4,
   696.4};
   Double_t clus_pixlayer_1_fex1012[27] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t clus_pixlayer_1_fey1012[27] = {
   17.60455,
   17.58636,
   16.72364,
   16.78571,
   16.77617,
   17.16741,
   18.31284,
   19.82322,
   20.4,
   22.05085,
   23.08593,
   23.59661,
   24.37704,
   0,
   24.57641,
   24.27839,
   22.5211,
   21.31291,
   20.14746,
   18.34339,
   18.14166,
   18.28661,
   17.35742,
   17.01764,
   16.75709,
   16.80952,
   16.69012};
   gre = new TGraphErrors(27,clus_pixlayer_1_fx1012,clus_pixlayer_1_fy1012,clus_pixlayer_1_fex1012,clus_pixlayer_1_fey1012);
   gre->SetName("clus_pixlayer_1");
   gre->SetTitle("Layer 1");
   gre->SetFillColor(1);
   gre->SetLineColor(2);
   gre->SetMarkerColor(2);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_clus_pixlayer_11012 = new TH1F("Graph_clus_pixlayer_11012","Layer 1",100,-15.6,15.6);
   Graph_clus_pixlayer_11012->SetMinimum(0);
   Graph_clus_pixlayer_11012->SetMaximum(1688.034);
   Graph_clus_pixlayer_11012->SetDirectory(0);
   Graph_clus_pixlayer_11012->SetStats(0);
   gre->SetHistogram(Graph_clus_pixlayer_11012);
   
   multigraph->Add(gre,"lp");
   
   Double_t clus_pixlayer_2_fx1013[18] = {
   -9,
   -8,
   -7,
   -6,
   -5,
   -4,
   -3,
   -2,
   -1,
   1,
   2,
   3,
   4,
   5,
   6,
   7,
   8,
   9};
   Double_t clus_pixlayer_2_fy1013[18] = {
   631.6,
   597.2,
   583.2,
   643.2,
   731.2,
   775.2,
   785.2,
   816,
   859.2,
   866,
   824,
   758.4,
   728.8,
   704,
   626.8,
   668.8,
   592,
   621.2};
   Double_t clus_pixlayer_2_fex1013[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t clus_pixlayer_2_fey1013[18] = {
   15.89465,
   15.45574,
   15.27351,
   16.03995,
   17.10205,
   17.60909,
   17.7223,
   18.06654,
   18.53861,
   18.61182,
   18.15489,
   17.41723,
   17.07396,
   16.78094,
   15.83414,
   16.35604,
   15.38831,
   15.76325};
   gre = new TGraphErrors(18,clus_pixlayer_2_fx1013,clus_pixlayer_2_fy1013,clus_pixlayer_2_fex1013,clus_pixlayer_2_fey1013);
   gre->SetName("clus_pixlayer_2");
   gre->SetTitle("Layer 2");
   gre->SetFillColor(1);
   gre->SetLineColor(3);
   gre->SetMarkerColor(3);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_clus_pixlayer_21013 = new TH1F("Graph_clus_pixlayer_21013","Layer 2",100,-10.8,10.8);
   Graph_clus_pixlayer_21013->SetMinimum(536.2579);
   Graph_clus_pixlayer_21013->SetMaximum(916.2804);
   Graph_clus_pixlayer_21013->SetDirectory(0);
   Graph_clus_pixlayer_21013->SetStats(0);
   gre->SetHistogram(Graph_clus_pixlayer_21013);
   
   multigraph->Add(gre,"lp");
   
   Double_t clus_pixlayer_3_fx1014[18] = {
   -9,
   -8,
   -7,
   -6,
   -5,
   -4,
   -3,
   -2,
   -1,
   1,
   2,
   3,
   4,
   5,
   6,
   7,
   8,
   9};
   Double_t clus_pixlayer_3_fy1014[18] = {
   494.8,
   544,
   530,
   598.4,
   620.4,
   584.8,
   631.6,
   615.2,
   668.8,
   617.2,
   639.6,
   636.8,
   565.2,
   620,
   566.4,
   559.6,
   582,
   537.6};
   Double_t clus_pixlayer_3_fex1014[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t clus_pixlayer_3_fey1014[18] = {
   14.0684,
   14.75127,
   14.56022,
   15.47126,
   15.75309,
   15.29444,
   15.89465,
   15.68694,
   16.35604,
   15.71242,
   15.995,
   15.95995,
   15.03596,
   15.74802,
   15.05191,
   14.96128,
   15.25778,
   14.66424};
   gre = new TGraphErrors(18,clus_pixlayer_3_fx1014,clus_pixlayer_3_fy1014,clus_pixlayer_3_fex1014,clus_pixlayer_3_fey1014);
   gre->SetName("clus_pixlayer_3");
   gre->SetTitle("Layer 3");
   gre->SetFillColor(1);
   gre->SetLineColor(4);
   gre->SetMarkerColor(4);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_clus_pixlayer_31014 = new TH1F("Graph_clus_pixlayer_31014","Layer 3",100,-10.8,10.8);
   Graph_clus_pixlayer_31014->SetMinimum(460.2891);
   Graph_clus_pixlayer_31014->SetMaximum(705.5985);
   Graph_clus_pixlayer_31014->SetDirectory(0);
   Graph_clus_pixlayer_31014->SetStats(0);
   gre->SetHistogram(Graph_clus_pixlayer_31014);
   
   multigraph->Add(gre,"lp");
   
   Double_t clus_pixlayer_4_fx1015[18] = {
   -9,
   -8,
   -7,
   -6,
   -5,
   -4,
   -3,
   -2,
   -1,
   1,
   2,
   3,
   4,
   5,
   6,
   7,
   8,
   9};
   Double_t clus_pixlayer_4_fy1015[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t clus_pixlayer_4_fex1015[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t clus_pixlayer_4_fey1015[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   gre = new TGraphErrors(18,clus_pixlayer_4_fx1015,clus_pixlayer_4_fy1015,clus_pixlayer_4_fex1015,clus_pixlayer_4_fey1015);
   gre->SetName("clus_pixlayer_4");
   gre->SetTitle("Layer 4");
   gre->SetFillColor(1);
   gre->SetLineColor(5);
   gre->SetMarkerColor(5);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_clus_pixlayer_41015 = new TH1F("Graph_clus_pixlayer_41015","Layer 4",100,-10.8,10.8);
   Graph_clus_pixlayer_41015->SetMinimum(0);
   Graph_clus_pixlayer_41015->SetMaximum(1.1);
   Graph_clus_pixlayer_41015->SetDirectory(0);
   Graph_clus_pixlayer_41015->SetStats(0);
   gre->SetHistogram(Graph_clus_pixlayer_41015);
   
   multigraph->Add(gre,"lp");
   multigraph->Draw("Apl");
   multigraph->GetXaxis()->SetTitle("moduleID");
   multigraph->GetYaxis()->SetTitle("Cluster per etaModuleID");
   
   TLegend *leg = new TLegend(0.67,0.45,0.9,0.7,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextFont(62);
   leg->SetTextSize(0.04);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(10);
   leg->SetFillStyle(0);
   TLegendEntry *entry=leg->AddEntry("NULL","Pixel Barrel","h");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("clus_pixlayer_0","Layer 0","lp");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("clus_pixlayer_1","Layer 1","lp");
   entry->SetLineColor(2);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(2);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("clus_pixlayer_2","Layer 2","lp");
   entry->SetLineColor(3);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(3);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("clus_pixlayer_3","Layer 3","lp");
   entry->SetLineColor(4);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(4);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("clus_pixlayer_4","Layer 4","lp");
   entry->SetLineColor(5);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(5);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   leg->Draw();
   c1_n3->Modified();
   c1_n3->cd();
   c1_n3->SetSelected(c1_n3);
}
