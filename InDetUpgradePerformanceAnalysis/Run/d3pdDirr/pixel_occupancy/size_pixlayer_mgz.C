void size_pixlayer_mgz()
{
//=========Macro generated from canvas: c1_n11/c1_n11
//=========  (Tue Apr 26 00:12:42 2016) by ROOT version6.04/12
   TCanvas *c1_n11 = new TCanvas("c1_n11", "c1_n11",0,0,700,500);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   c1_n11->SetHighLightColor(2);
   c1_n11->Range(-713.4657,-0.0039863,713.6637,15.59564);
   c1_n11->SetFillColor(0);
   c1_n11->SetBorderMode(0);
   c1_n11->SetBorderSize(2);
   c1_n11->SetTickx(1);
   c1_n11->SetTicky(1);
   c1_n11->SetFrameBorderMode(0);
   c1_n11->SetFrameBorderMode(0);
   
   TMultiGraph *multigraph = new TMultiGraph();
   multigraph->SetName("size_pixlayer_mgz");
   multigraph->SetTitle("");
   
   Double_t size_z_pixlayer_0_fx1056[16] = {
   -311.4796,
   -269.302,
   -227.5325,
   -186.0868,
   -144.8367,
   -102.6844,
   -61.99612,
   -20.55679,
   20.61349,
   61.13606,
   103.2006,
   144.1883,
   185.6079,
   228.459,
   269.96,
   310.5051};
   Double_t size_z_pixlayer_0_fy1056[16] = {
   9.743646,
   8.871604,
   7.791965,
   6.165241,
   5.008411,
   4.06129,
   3.544517,
   3.109166,
   3.149185,
   3.363794,
   4.497137,
   4.838063,
   6.187314,
   7.012001,
   7.870531,
   8.005376};
   Double_t size_z_pixlayer_0_fex1056[16] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t size_z_pixlayer_0_fey1056[16] = {
   0.3496026,
   0.2734265,
   0.1781514,
   0.1225088,
   0.06269456,
   0.05852801,
   0.05679725,
   0.0501582,
   0.032917,
   0.03703292,
   0.1299695,
   0.06572986,
   0.0993746,
   0.1296518,
   0.1118847,
   0.1294899};
   TGraphErrors *gre = new TGraphErrors(16,size_z_pixlayer_0_fx1056,size_z_pixlayer_0_fy1056,size_z_pixlayer_0_fex1056,size_z_pixlayer_0_fey1056);
   gre->SetName("size_z_pixlayer_0");
   gre->SetTitle("Layer 0");
   gre->SetFillColor(1);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_size_z_pixlayer_01056 = new TH1F("Graph_size_z_pixlayer_01056","Layer 0",100,-373.6781,372.7036);
   Graph_size_z_pixlayer_01056->SetMinimum(2.355584);
   Graph_size_z_pixlayer_01056->SetMaximum(10.79667);
   Graph_size_z_pixlayer_01056->SetDirectory(0);
   Graph_size_z_pixlayer_01056->SetStats(0);
   gre->SetHistogram(Graph_size_z_pixlayer_01056);
   
   multigraph->Add(gre,"lp");
   
   Double_t size_z_pixlayer_1_fx1057[27] = {
   -518.8571,
   -477.3647,
   -435.4999,
   -392.9316,
   -352.4182,
   -311.2982,
   -270.062,
   -226.7134,
   -186.0436,
   -145.0319,
   -103.2227,
   -61.66889,
   -20.85781,
   -nan,
   20.59842,
   61.8738,
   103.415,
   144.6811,
   186.4653,
   227.6544,
   269.5872,
   310.6381,
   351.8254,
   393.6407,
   436.1101,
   477.5812,
   519.0551};
   Double_t size_z_pixlayer_1_fy1057[27] = {
   7.066082,
   7.210036,
   6.715103,
   6.64452,
   5.952246,
   5.734528,
   5.09876,
   4.535016,
   4.258362,
   3.689371,
   3.090964,
   2.912644,
   2.637588,
   -nan,
   2.697483,
   2.985885,
   3.117035,
   3.705882,
   4.30745,
   4.80837,
   5.193,
   5.583733,
   5.823154,
   6.392817,
   6.562963,
   6.850509,
   6.667432};
   Double_t size_z_pixlayer_1_fex1057[27] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t size_z_pixlayer_1_fey1057[27] = {
   0.2113715,
   0.2570828,
   0.128362,
   0.1547827,
   0.08823314,
   0.1065834,
   0.07477269,
   0.08169682,
   0.08221491,
   0.05646584,
   0.03986666,
   0.04696243,
   0.03698193,
   -nan,
   0.0372011,
   0.06498738,
   0.04034001,
   0.06088086,
   0.1020736,
   0.1137804,
   0.08399606,
   0.07518765,
   0.09397754,
   0.1442713,
   0.1351519,
   0.1689793,
   0.1202802};
   gre = new TGraphErrors(27,size_z_pixlayer_1_fx1057,size_z_pixlayer_1_fy1057,size_z_pixlayer_1_fex1057,size_z_pixlayer_1_fey1057);
   gre->SetName("size_z_pixlayer_1");
   gre->SetTitle("Layer 1");
   gre->SetFillColor(1);
   gre->SetLineColor(2);
   gre->SetMarkerColor(2);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_size_z_pixlayer_11057 = new TH1F("Graph_size_z_pixlayer_11057","Layer 1",100,-622.6483,622.8463);
   Graph_size_z_pixlayer_11057->SetMinimum(2.113954);
   Graph_size_z_pixlayer_11057->SetMaximum(7.95377);
   Graph_size_z_pixlayer_11057->SetDirectory(0);
   Graph_size_z_pixlayer_11057->SetStats(0);
   gre->SetHistogram(Graph_size_z_pixlayer_11057);
   
   multigraph->Add(gre,"lp");
   
   Double_t size_z_pixlayer_2_fx1058[18] = {
   -371.3658,
   -330.5065,
   -289.9196,
   -247.6979,
   -205.975,
   -164.5711,
   -123.9324,
   -81.97437,
   -41.58564,
   41.17309,
   82.65405,
   123.4875,
   165.4787,
   206.8579,
   248.1609,
   288.7308,
   330.394,
   371.358};
   Double_t size_z_pixlayer_2_fy1058[18] = {
   2.9886,
   2.982585,
   2.593278,
   2.543532,
   2.615974,
   2.353973,
   2.181865,
   1.993627,
   2.120577,
   2.37321,
   2.295631,
   2.33597,
   2.433041,
   2.74375,
   2.648373,
   2.674043,
   3.025,
   3.078557};
   Double_t size_z_pixlayer_2_fex1058[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t size_z_pixlayer_2_fey1058[18] = {
   0.06009962,
   0.07121602,
   0.04353297,
   0.05062614,
   0.07971124,
   0.05114934,
   0.03539136,
   0.03111449,
   0.06590623,
   0.0976871,
   0.06179905,
   0.09431915,
   0.06601536,
   0.08472974,
   0.06990398,
   0.04676946,
   0.08118188,
   0.07273284};
   gre = new TGraphErrors(18,size_z_pixlayer_2_fx1058,size_z_pixlayer_2_fy1058,size_z_pixlayer_2_fex1058,size_z_pixlayer_2_fey1058);
   gre->SetName("size_z_pixlayer_2");
   gre->SetTitle("Layer 2");
   gre->SetFillColor(1);
   gre->SetLineColor(3);
   gre->SetMarkerColor(3);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_size_z_pixlayer_21058 = new TH1F("Graph_size_z_pixlayer_21058","Layer 2",100,-445.6382,445.6304);
   Graph_size_z_pixlayer_21058->SetMinimum(1.843635);
   Graph_size_z_pixlayer_21058->SetMaximum(3.270168);
   Graph_size_z_pixlayer_21058->SetDirectory(0);
   Graph_size_z_pixlayer_21058->SetStats(0);
   gre->SetHistogram(Graph_size_z_pixlayer_21058);
   
   multigraph->Add(gre,"lp");
   
   Double_t size_z_pixlayer_3_fx1059[18] = {
   -371.689,
   -330.248,
   -289.1628,
   -248.3445,
   -205.7286,
   -165.2772,
   -123.7082,
   -82.30088,
   -41.32036,
   41.17258,
   82.56261,
   123.4788,
   164.9611,
   206.3639,
   247.109,
   289.1987,
   329.658,
   371.4065};
   Double_t size_z_pixlayer_3_fy1059[18] = {
   2.764754,
   2.972059,
   2.743396,
   2.654412,
   2.504191,
   2.461013,
   2.207093,
   2.26788,
   2.275119,
   2.14582,
   2.183865,
   2.14887,
   2.547771,
   2.397419,
   2.60452,
   2.661901,
   3.145017,
   2.674851};
   Double_t size_z_pixlayer_3_fex1059[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t size_z_pixlayer_3_fey1059[18] = {
   0.05826561,
   0.07681963,
   0.1168834,
   0.09262101,
   0.07059727,
   0.07728839,
   0.053152,
   0.06403659,
   0.06390928,
   0.04945878,
   0.04678929,
   0.04192347,
   0.09903923,
   0.05745308,
   0.07671736,
   0.06619959,
   0.1227999,
   0.06044589};
   gre = new TGraphErrors(18,size_z_pixlayer_3_fx1059,size_z_pixlayer_3_fy1059,size_z_pixlayer_3_fex1059,size_z_pixlayer_3_fey1059);
   gre->SetName("size_z_pixlayer_3");
   gre->SetTitle("Layer 3");
   gre->SetFillColor(1);
   gre->SetLineColor(4);
   gre->SetMarkerColor(4);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_size_z_pixlayer_31059 = new TH1F("Graph_size_z_pixlayer_31059","Layer 3",100,-445.9985,445.716);
   Graph_size_z_pixlayer_31059->SetMinimum(1.979216);
   Graph_size_z_pixlayer_31059->SetMaximum(3.384963);
   Graph_size_z_pixlayer_31059->SetDirectory(0);
   Graph_size_z_pixlayer_31059->SetStats(0);
   gre->SetHistogram(Graph_size_z_pixlayer_31059);
   
   multigraph->Add(gre,"lp");
   
   Double_t size_z_pixlayer_4_fx1060[18] = {
   -nan,
   -nan,
   -nan,
   -nan,
   -nan,
   -nan,
   -nan,
   -nan,
   -nan,
   -nan,
   -nan,
   -nan,
   -nan,
   -nan,
   -nan,
   -nan,
   -nan,
   -nan};
   Double_t size_z_pixlayer_4_fy1060[18] = {
   -nan,
   -nan,
   -nan,
   -nan,
   -nan,
   -nan,
   -nan,
   -nan,
   -nan,
   -nan,
   -nan,
   -nan,
   -nan,
   -nan,
   -nan,
   -nan,
   -nan,
   -nan};
   Double_t size_z_pixlayer_4_fex1060[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t size_z_pixlayer_4_fey1060[18] = {
   -nan,
   -nan,
   -nan,
   -nan,
   -nan,
   -nan,
   -nan,
   -nan,
   -nan,
   -nan,
   -nan,
   -nan,
   -nan,
   -nan,
   -nan,
   -nan,
   -nan,
   -nan};
   gre = new TGraphErrors(18,size_z_pixlayer_4_fx1060,size_z_pixlayer_4_fy1060,size_z_pixlayer_4_fex1060,size_z_pixlayer_4_fey1060);
   gre->SetName("size_z_pixlayer_4");
   gre->SetTitle("Layer 4");
   gre->SetFillColor(1);
   gre->SetLineColor(5);
   gre->SetMarkerColor(5);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_size_z_pixlayer_41060 = new TH1F("Graph_size_z_pixlayer_41060","Layer 4",100,-nan,-nan);
   Graph_size_z_pixlayer_41060->SetMinimum(-nan);
   Graph_size_z_pixlayer_41060->SetMaximum(-nan);
   Graph_size_z_pixlayer_41060->SetDirectory(0);
   Graph_size_z_pixlayer_41060->SetStats(0);
   gre->SetHistogram(Graph_size_z_pixlayer_41060);
   
   multigraph->Add(gre,"lp");
   multigraph->Draw("Apl");
   multigraph->GetXaxis()->SetTitle("z [mm]");
   multigraph->GetYaxis()->SetTitle("Cluster size");
   
   TLegend *leg = new TLegend(0.61,0.7,0.81,0.85,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextFont(62);
   leg->SetTextSize(0.04);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(10);
   leg->SetFillStyle(0);
   TLegendEntry *entry=leg->AddEntry("NULL","Pixel Barrel","h");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("size_z_pixlayer_0","Layer 0","lp");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("size_z_pixlayer_1","Layer 1","lp");
   entry->SetLineColor(2);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(2);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("size_z_pixlayer_2","Layer 2","lp");
   entry->SetLineColor(3);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(3);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("size_z_pixlayer_3","Layer 3","lp");
   entry->SetLineColor(4);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(4);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("size_z_pixlayer_4","Layer 4","lp");
   entry->SetLineColor(5);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(5);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   leg->Draw();
   c1_n11->Modified();
   c1_n11->cd();
   c1_n11->SetSelected(c1_n11);
}
