void hit_sctdisk_mgr()
{
//=========Macro generated from canvas: c1_n36/c1_n36
//=========  (Tue Apr 26 00:12:45 2016) by ROOT version6.04/12
   TCanvas *c1_n36 = new TCanvas("c1_n36", "c1_n36",0,0,700,500);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   c1_n36->SetHighLightColor(2);
   c1_n36->Range(304.8057,-299.5319,926.7768,3619.652);
   c1_n36->SetFillColor(0);
   c1_n36->SetBorderMode(0);
   c1_n36->SetBorderSize(2);
   c1_n36->SetTickx(1);
   c1_n36->SetTicky(1);
   c1_n36->SetFrameBorderMode(0);
   c1_n36->SetFrameBorderMode(0);
   
   TMultiGraph *multigraph = new TMultiGraph();
   multigraph->SetName("hit_sctdisk_mgr");
   multigraph->SetTitle("");
   
   Double_t hit_r_sctdisk_0_fx1215[18] = {
   389.741,
   400.7666,
   414.7675,
   427.9818,
   440.109,
   452.5323,
   465.6726,
   481.3315,
   504.0509,
   525.375,
   545.1746,
   566.5513,
   594.4127,
   625.8277,
   671.3082,
   729.6426,
   787.4825,
   841.9625};
   Double_t hit_r_sctdisk_0_fy1215[18] = {
   234,
   412,
   422.4,
   315.6,
   275.2,
   315.6,
   290.8,
   390.4,
   606,
   395.6,
   442.4,
   472.4,
   577.2,
   623.6,
   1007.2,
   930,
   789.6,
   676};
   Double_t hit_r_sctdisk_0_fex1215[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t hit_r_sctdisk_0_fey1215[18] = {
   10.56581,
   12.41068,
   19.46298,
   11.06577,
   7.21752,
   13.53855,
   9.604051,
   13.04912,
   40.04208,
   9.723877,
   11.40911,
   20.02446,
   11.90446,
   20.61349,
   21.08006,
   26.11501,
   19.74026,
   23.41147};
   TGraphErrors *gre = new TGraphErrors(18,hit_r_sctdisk_0_fx1215,hit_r_sctdisk_0_fy1215,hit_r_sctdisk_0_fex1215,hit_r_sctdisk_0_fey1215);
   gre->SetName("hit_r_sctdisk_0");
   gre->SetTitle("Disk 0");
   gre->SetFillColor(1);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_hit_r_sctdisk_01215 = new TH1F("Graph_hit_r_sctdisk_01215","Disk 0",100,344.5189,887.1847);
   Graph_hit_r_sctdisk_01215->SetMinimum(142.9496);
   Graph_hit_r_sctdisk_01215->SetMaximum(1108.765);
   Graph_hit_r_sctdisk_01215->SetDirectory(0);
   Graph_hit_r_sctdisk_01215->SetStats(0);
   gre->SetHistogram(Graph_hit_r_sctdisk_01215);
   
   multigraph->Add(gre,"lp");
   
   Double_t hit_r_sctdisk_1_fx1216[18] = {
   389.7341,
   400.7509,
   414.8286,
   427.9088,
   440.0252,
   452.6654,
   465.7025,
   481.3385,
   504.0708,
   525.3853,
   545.2266,
   566.5574,
   594.4515,
   625.7282,
   671.3051,
   729.6364,
   787.5168,
   841.9386};
   Double_t hit_r_sctdisk_1_fy1216[18] = {
   263.6,
   397.2,
   434,
   346,
   282.4,
   394.4,
   354.4,
   426,
   534,
   445.2,
   478,
   425.6,
   629.6,
   656.8,
   1098.8,
   850,
   864.0001,
   770.4};
   Double_t hit_r_sctdisk_1_fex1216[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t hit_r_sctdisk_1_fey1216[18] = {
   7.36327,
   10.88595,
   18.513,
   11.32778,
   8.373626,
   14.73306,
   14.17945,
   12.3883,
   12.20147,
   14.34535,
   15.22763,
   11.48837,
   25.59529,
   15.13874,
   35.58559,
   17.47181,
   21.85183,
   17.82393};
   gre = new TGraphErrors(18,hit_r_sctdisk_1_fx1216,hit_r_sctdisk_1_fy1216,hit_r_sctdisk_1_fex1216,hit_r_sctdisk_1_fey1216);
   gre->SetName("hit_r_sctdisk_1");
   gre->SetTitle("Disk 1");
   gre->SetFillColor(1);
   gre->SetLineColor(2);
   gre->SetMarkerColor(2);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_hit_r_sctdisk_11216 = new TH1F("Graph_hit_r_sctdisk_11216","Disk 1",100,344.5136,887.1591);
   Graph_hit_r_sctdisk_11216->SetMinimum(168.4218);
   Graph_hit_r_sctdisk_11216->SetMaximum(1222.201);
   Graph_hit_r_sctdisk_11216->SetDirectory(0);
   Graph_hit_r_sctdisk_11216->SetStats(0);
   gre->SetHistogram(Graph_hit_r_sctdisk_11216);
   
   multigraph->Add(gre,"lp");
   
   Double_t hit_r_sctdisk_2_fx1217[18] = {
   389.6734,
   400.7595,
   414.8188,
   427.9513,
   440.0908,
   452.5648,
   465.7538,
   481.331,
   504.0398,
   525.4056,
   545.24,
   566.6274,
   594.3582,
   625.7314,
   671.3165,
   729.6663,
   787.5295,
   841.945};
   Double_t hit_r_sctdisk_2_fy1217[18] = {
   234.4,
   405.6,
   382.4,
   376,
   394.8,
   348.4,
   323.2,
   468,
   578.4,
   464,
   461.2,
   408.8,
   562.4,
   598,
   1141.2,
   985.6,
   846.4,
   883.2};
   Double_t hit_r_sctdisk_2_fex1217[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t hit_r_sctdisk_2_fey1217[18] = {
   11.87033,
   15.71934,
   12.01237,
   15.96075,
   20.15785,
   10.48413,
   10.87334,
   15.38385,
   12.40948,
   13.06845,
   10.57797,
   10.01371,
   13.01424,
   11.9919,
   17.39933,
   36.32228,
   16.00689,
   28.85616};
   gre = new TGraphErrors(18,hit_r_sctdisk_2_fx1217,hit_r_sctdisk_2_fy1217,hit_r_sctdisk_2_fex1217,hit_r_sctdisk_2_fey1217);
   gre->SetName("hit_r_sctdisk_2");
   gre->SetTitle("Disk 2");
   gre->SetFillColor(1);
   gre->SetLineColor(3);
   gre->SetMarkerColor(3);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_hit_r_sctdisk_21217 = new TH1F("Graph_hit_r_sctdisk_21217","Disk 2",100,344.4462,887.1722);
   Graph_hit_r_sctdisk_21217->SetMinimum(128.9227);
   Graph_hit_r_sctdisk_21217->SetMaximum(1252.206);
   Graph_hit_r_sctdisk_21217->SetDirectory(0);
   Graph_hit_r_sctdisk_21217->SetStats(0);
   gre->SetHistogram(Graph_hit_r_sctdisk_21217);
   
   multigraph->Add(gre,"lp");
   
   Double_t hit_r_sctdisk_3_fx1218[18] = {
   389.62,
   400.7244,
   414.8489,
   427.9322,
   440.1068,
   452.6191,
   465.7914,
   481.3147,
   504.0581,
   525.441,
   545.2514,
   566.6274,
   594.443,
   625.7984,
   671.3168,
   729.6447,
   787.5072,
   841.9218};
   Double_t hit_r_sctdisk_3_fy1218[18] = {
   280.8,
   419.6,
   426.4,
   325.6,
   346.8,
   354,
   370.8,
   452.4,
   622,
   404,
   517.2,
   458.4,
   678.8,
   577.6,
   1110,
   972.4,
   837.2,
   782.4};
   Double_t hit_r_sctdisk_3_fex1218[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t hit_r_sctdisk_3_fey1218[18] = {
   10.59808,
   9.170254,
   10.84511,
   7.639797,
   11.78385,
   11.48051,
   13.8796,
   16.22585,
   33.31982,
   8.34731,
   10.66943,
   8.243931,
   13.15139,
   11.568,
   19.19854,
   16.34023,
   15.91188,
   15.58346};
   gre = new TGraphErrors(18,hit_r_sctdisk_3_fx1218,hit_r_sctdisk_3_fy1218,hit_r_sctdisk_3_fex1218,hit_r_sctdisk_3_fey1218);
   gre->SetName("hit_r_sctdisk_3");
   gre->SetTitle("Disk 3");
   gre->SetFillColor(1);
   gre->SetLineColor(4);
   gre->SetMarkerColor(4);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_hit_r_sctdisk_31218 = new TH1F("Graph_hit_r_sctdisk_31218","Disk 3",100,344.3898,887.152);
   Graph_hit_r_sctdisk_31218->SetMinimum(184.3023);
   Graph_hit_r_sctdisk_31218->SetMaximum(1215.098);
   Graph_hit_r_sctdisk_31218->SetDirectory(0);
   Graph_hit_r_sctdisk_31218->SetStats(0);
   gre->SetHistogram(Graph_hit_r_sctdisk_31218);
   
   multigraph->Add(gre,"lp");
   
   Double_t hit_r_sctdisk_4_fx1219[18] = {
   389.7077,
   400.7091,
   414.7454,
   427.8857,
   440.049,
   452.6122,
   465.7831,
   481.2607,
   504.0315,
   525.3616,
   545.17,
   566.5881,
   594.3796,
   625.7939,
   671.324,
   729.6425,
   787.5304,
   841.9453};
   Double_t hit_r_sctdisk_4_fy1219[18] = {
   290,
   458,
   477.6,
   357.6,
   348,
   389.2,
   352.4,
   512.4,
   641.6,
   397.6,
   436.4,
   414.8,
   706.4,
   664.4,
   1099.2,
   1115.2,
   871.6,
   836.8};
   Double_t hit_r_sctdisk_4_fex1219[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t hit_r_sctdisk_4_fey1219[18] = {
   11.12875,
   12.95992,
   12.87525,
   15.98561,
   7.733825,
   16.09057,
   7.181852,
   10.70898,
   12.8319,
   8.578451,
   10.77268,
   8.676964,
   36.66339,
   12.29361,
   24.80745,
   32.98969,
   18.72255,
   14.68138};
   gre = new TGraphErrors(18,hit_r_sctdisk_4_fx1219,hit_r_sctdisk_4_fy1219,hit_r_sctdisk_4_fex1219,hit_r_sctdisk_4_fey1219);
   gre->SetName("hit_r_sctdisk_4");
   gre->SetTitle("Disk 4");
   gre->SetFillColor(1);
   gre->SetLineColor(5);
   gre->SetMarkerColor(5);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_hit_r_sctdisk_41219 = new TH1F("Graph_hit_r_sctdisk_41219","Disk 4",100,344.4839,887.169);
   Graph_hit_r_sctdisk_41219->SetMinimum(191.9394);
   Graph_hit_r_sctdisk_41219->SetMaximum(1235.122);
   Graph_hit_r_sctdisk_41219->SetDirectory(0);
   Graph_hit_r_sctdisk_41219->SetStats(0);
   gre->SetHistogram(Graph_hit_r_sctdisk_41219);
   
   multigraph->Add(gre,"lp");
   
   Double_t hit_r_sctdisk_5_fx1220[18] = {
   389.7091,
   400.7306,
   414.8481,
   427.9638,
   440.0479,
   452.5998,
   465.676,
   481.2945,
   504.0497,
   525.4467,
   545.2393,
   566.6334,
   594.4165,
   625.7171,
   671.313,
   729.6239,
   787.5134,
   841.9309};
   Double_t hit_r_sctdisk_5_fy1220[18] = {
   282.4,
   520.8,
   490,
   396.4,
   371.6,
   406.4,
   392.4,
   504,
   665.2,
   473.6,
   545.6,
   488.8,
   644,
   644.4,
   1156.4,
   1122,
   870.8,
   820.4};
   Double_t hit_r_sctdisk_5_fex1220[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t hit_r_sctdisk_5_fey1220[18] = {
   7.830614,
   11.44176,
   10.57284,
   8.728833,
   17.85462,
   12.06355,
   9.024771,
   13.55648,
   16.34521,
   14.07249,
   21.98045,
   10.23819,
   14.16384,
   15.61059,
   22.01812,
   21.86374,
   18.87242,
   19.28168};
   gre = new TGraphErrors(18,hit_r_sctdisk_5_fx1220,hit_r_sctdisk_5_fy1220,hit_r_sctdisk_5_fex1220,hit_r_sctdisk_5_fey1220);
   gre->SetName("hit_r_sctdisk_5");
   gre->SetTitle("Disk 5");
   gre->SetFillColor(1);
   gre->SetLineColor(6);
   gre->SetMarkerColor(6);
   gre->SetMarkerStyle(20);
   
   TH1F *Graph_hit_r_sctdisk_51220 = new TH1F("Graph_hit_r_sctdisk_51220","Disk 5",100,344.4869,887.1531);
   Graph_hit_r_sctdisk_51220->SetMinimum(184.1845);
   Graph_hit_r_sctdisk_51220->SetMaximum(1268.803);
   Graph_hit_r_sctdisk_51220->SetDirectory(0);
   Graph_hit_r_sctdisk_51220->SetStats(0);
   gre->SetHistogram(Graph_hit_r_sctdisk_51220);
   
   multigraph->Add(gre,"lp");
   
   Double_t hit_r_sctdisk_6_fx1221[18] = {
   389.6436,
   400.7643,
   414.7897,
   427.9144,
   440.0257,
   452.6147,
   465.6681,
   481.3489,
   504.0893,
   525.3794,
   545.2571,
   566.5358,
   594.3668,
   625.7487,
   671.3149,
   729.6439,
   787.5208,
   841.915};
   Double_t hit_r_sctdisk_6_fy1221[18] = {
   591.2,
   913.6,
   939.6,
   668,
   834.4,
   705.2,
   709.6,
   1017.2,
   1276.8,
   891.6,
   975.2,
   897.6,
   1228,
   1313.2,
   2272.4,
   1968,
   1816.8,
   1554.8};
   Double_t hit_r_sctdisk_6_fex1221[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t hit_r_sctdisk_6_fey1221[18] = {
   27.74634,
   23.09789,
   22.8245,
   12.32499,
   47.25999,
   15.15733,
   16.22838,
   28.26176,
   19.53099,
   20.52055,
   20.12694,
   16.31865,
   23.82267,
   25.26809,
   35.97942,
   34.03085,
   36.30152,
   30.26678};
   gre = new TGraphErrors(18,hit_r_sctdisk_6_fx1221,hit_r_sctdisk_6_fy1221,hit_r_sctdisk_6_fex1221,hit_r_sctdisk_6_fey1221);
   gre->SetName("hit_r_sctdisk_6");
   gre->SetTitle("Disk 0");
   gre->SetFillColor(1);
   gre->SetLineStyle(2);
   gre->SetMarkerStyle(21);
   
   TH1F *Graph_hit_r_sctdisk_61221 = new TH1F("Graph_hit_r_sctdisk_61221","Disk 0",100,344.4165,887.1421);
   Graph_hit_r_sctdisk_61221->SetMinimum(388.9611);
   Graph_hit_r_sctdisk_61221->SetMaximum(2482.872);
   Graph_hit_r_sctdisk_61221->SetDirectory(0);
   Graph_hit_r_sctdisk_61221->SetStats(0);
   gre->SetHistogram(Graph_hit_r_sctdisk_61221);
   
   multigraph->Add(gre,"lp");
   
   Double_t hit_r_sctdisk_7_fx1222[18] = {
   389.6562,
   400.734,
   414.8259,
   427.9379,
   440.0842,
   452.6036,
   465.7164,
   481.3549,
   504.0641,
   525.3306,
   545.1436,
   566.5118,
   594.4177,
   625.7552,
   671.3208,
   729.6627,
   787.5062,
   841.9374};
   Double_t hit_r_sctdisk_7_fy1222[18] = {
   204.4,
   478,
   422.8,
   340.4,
   387.6,
   320.8,
   384,
   454.8,
   599.2,
   445.2,
   412,
   456.8,
   541.6,
   558.8,
   1028.4,
   981.6,
   823.6,
   767.2};
   Double_t hit_r_sctdisk_7_fex1222[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t hit_r_sctdisk_7_fey1222[18] = {
   6.489974,
   33.65751,
   10.00337,
   12.76184,
   50.10304,
   9.327637,
   33.42351,
   13.08562,
   57.0825,
   16.5742,
   10.36882,
   20.64783,
   12.143,
   12.00963,
   18.17523,
   32.1126,
   24.8286,
   28.65502};
   gre = new TGraphErrors(18,hit_r_sctdisk_7_fx1222,hit_r_sctdisk_7_fy1222,hit_r_sctdisk_7_fex1222,hit_r_sctdisk_7_fey1222);
   gre->SetName("hit_r_sctdisk_7");
   gre->SetTitle("Disk 1");
   gre->SetFillColor(1);
   gre->SetLineColor(2);
   gre->SetLineStyle(2);
   gre->SetMarkerColor(2);
   gre->SetMarkerStyle(21);
   
   TH1F *Graph_hit_r_sctdisk_71222 = new TH1F("Graph_hit_r_sctdisk_71222","Disk 1",100,344.4281,887.1655);
   Graph_hit_r_sctdisk_71222->SetMinimum(113.0435);
   Graph_hit_r_sctdisk_71222->SetMaximum(1131.442);
   Graph_hit_r_sctdisk_71222->SetDirectory(0);
   Graph_hit_r_sctdisk_71222->SetStats(0);
   gre->SetHistogram(Graph_hit_r_sctdisk_71222);
   
   multigraph->Add(gre,"lp");
   
   Double_t hit_r_sctdisk_8_fx1223[18] = {
   389.6548,
   400.799,
   414.8031,
   427.9718,
   440.0384,
   452.6519,
   465.6404,
   481.3394,
   504.0625,
   525.4635,
   545.1807,
   566.5186,
   594.3565,
   625.8138,
   671.3182,
   729.6373,
   787.5262,
   841.9208};
   Double_t hit_r_sctdisk_8_fy1223[18] = {
   212.8,
   366.8,
   400.4,
   322.4,
   331.2,
   335.2,
   342.8,
   445.2,
   534,
   372.4,
   462,
   412.4,
   636,
   545.2,
   1024.8,
   903.6,
   795.2,
   790};
   Double_t hit_r_sctdisk_8_fex1223[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t hit_r_sctdisk_8_fey1223[18] = {
   6.729447,
   10.8938,
   8.540934,
   7.878183,
   9.465357,
   10.26783,
   10.06633,
   12.97696,
   13.92291,
   8.033727,
   17.41631,
   15.3576,
   12.13424,
   14.27168,
   20.09438,
   17.60789,
   20.62651,
   24.46591};
   gre = new TGraphErrors(18,hit_r_sctdisk_8_fx1223,hit_r_sctdisk_8_fy1223,hit_r_sctdisk_8_fex1223,hit_r_sctdisk_8_fey1223);
   gre->SetName("hit_r_sctdisk_8");
   gre->SetTitle("Disk 2");
   gre->SetFillColor(1);
   gre->SetLineColor(3);
   gre->SetLineStyle(2);
   gre->SetMarkerColor(3);
   gre->SetMarkerStyle(21);
   
   TH1F *Graph_hit_r_sctdisk_81223 = new TH1F("Graph_hit_r_sctdisk_81223","Disk 2",100,344.4282,887.1474);
   Graph_hit_r_sctdisk_81223->SetMinimum(122.1882);
   Graph_hit_r_sctdisk_81223->SetMaximum(1128.777);
   Graph_hit_r_sctdisk_81223->SetDirectory(0);
   Graph_hit_r_sctdisk_81223->SetStats(0);
   gre->SetHistogram(Graph_hit_r_sctdisk_81223);
   
   multigraph->Add(gre,"lp");
   
   Double_t hit_r_sctdisk_9_fx1224[18] = {
   389.7069,
   400.7034,
   414.8758,
   428.049,
   440.1303,
   452.6259,
   465.7327,
   481.3084,
   504.0244,
   525.3147,
   545.1908,
   566.5726,
   594.4221,
   625.7643,
   671.3097,
   729.6312,
   787.4977,
   841.9293};
   Double_t hit_r_sctdisk_9_fy1224[18] = {
   247.6,
   413.6,
   392,
   358,
   330,
   341.6,
   354.4,
   416.4,
   550.8,
   404.4,
   473.6,
   492,
   681.6,
   621.2,
   1051.6,
   972.8,
   876.4,
   738};
   Double_t hit_r_sctdisk_9_fex1224[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t hit_r_sctdisk_9_fey1224[18] = {
   7.590745,
   10.23875,
   9.746778,
   10.38825,
   9.984435,
   11.08268,
   13.83882,
   8.273705,
   12.0724,
   12.06524,
   18.46776,
   21.80764,
   20.68974,
   21.74703,
   18.0409,
   24.26266,
   25.91386,
   15.43346};
   gre = new TGraphErrors(18,hit_r_sctdisk_9_fx1224,hit_r_sctdisk_9_fy1224,hit_r_sctdisk_9_fex1224,hit_r_sctdisk_9_fey1224);
   gre->SetName("hit_r_sctdisk_9");
   gre->SetTitle("Disk 3");
   gre->SetFillColor(1);
   gre->SetLineColor(4);
   gre->SetLineStyle(2);
   gre->SetMarkerColor(4);
   gre->SetMarkerStyle(21);
   
   TH1F *Graph_hit_r_sctdisk_91224 = new TH1F("Graph_hit_r_sctdisk_91224","Disk 3",100,344.4847,887.1515);
   Graph_hit_r_sctdisk_91224->SetMinimum(157.0461);
   Graph_hit_r_sctdisk_91224->SetMaximum(1152.604);
   Graph_hit_r_sctdisk_91224->SetDirectory(0);
   Graph_hit_r_sctdisk_91224->SetStats(0);
   gre->SetHistogram(Graph_hit_r_sctdisk_91224);
   
   multigraph->Add(gre,"lp");
   
   Double_t hit_r_sctdisk_10_fx1225[18] = {
   389.7,
   400.7366,
   414.8197,
   427.9176,
   440.004,
   452.6247,
   465.6966,
   481.351,
   504.0894,
   525.3832,
   545.1796,
   566.5347,
   594.4395,
   625.7532,
   671.3155,
   729.6365,
   787.5087,
   841.9364};
   Double_t hit_r_sctdisk_10_fy1225[18] = {
   264.8,
   464.8,
   482.4,
   364,
   393.6,
   370,
   373.6,
   481.6,
   671.6,
   450,
   467.6,
   498,
   657.2,
   616.4,
   1098.4,
   993.9999,
   928.8,
   792.8};
   Double_t hit_r_sctdisk_10_fex1225[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t hit_r_sctdisk_10_fey1225[18] = {
   7.82247,
   14.53355,
   16.78669,
   11.79767,
   10.46905,
   10.8798,
   11.58481,
   18.32954,
   13.56416,
   44.11712,
   8.963228,
   22.32169,
   13.59773,
   13.32589,
   37.59328,
   24.30194,
   22.51186,
   16.62088};
   gre = new TGraphErrors(18,hit_r_sctdisk_10_fx1225,hit_r_sctdisk_10_fy1225,hit_r_sctdisk_10_fex1225,hit_r_sctdisk_10_fey1225);
   gre->SetName("hit_r_sctdisk_10");
   gre->SetTitle("Disk 4");
   gre->SetFillColor(1);
   gre->SetLineColor(5);
   gre->SetLineStyle(2);
   gre->SetMarkerColor(5);
   gre->SetMarkerStyle(21);
   
   TH1F *Graph_hit_r_sctdisk_101225 = new TH1F("Graph_hit_r_sctdisk_101225","Disk 4",100,344.4763,887.16);
   Graph_hit_r_sctdisk_101225->SetMinimum(169.0759);
   Graph_hit_r_sctdisk_101225->SetMaximum(1223.895);
   Graph_hit_r_sctdisk_101225->SetDirectory(0);
   Graph_hit_r_sctdisk_101225->SetStats(0);
   gre->SetHistogram(Graph_hit_r_sctdisk_101225);
   
   multigraph->Add(gre,"lp");
   
   Double_t hit_r_sctdisk_11_fx1226[18] = {
   389.6855,
   400.7267,
   414.8235,
   427.9095,
   440.0385,
   452.6555,
   465.6844,
   481.2881,
   504.08,
   525.374,
   545.2011,
   566.5844,
   594.3644,
   625.7396,
   671.3143,
   729.64,
   787.5166,
   841.9351};
   Double_t hit_r_sctdisk_11_fy1226[18] = {
   275.6,
   454,
   427.2,
   389.2,
   401.6,
   393.2,
   402,
   489.6,
   624.8,
   500.8,
   494.4,
   503.2,
   655.6,
   625.6,
   1002,
   928.8,
   943.6,
   850.8};
   Double_t hit_r_sctdisk_11_fex1226[18] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t hit_r_sctdisk_11_fey1226[18] = {
   7.844326,
   10.15403,
   9.693449,
   12.45858,
   11.40722,
   7.991274,
   10.22403,
   11.90733,
   19.80041,
   11.59112,
   9.73736,
   14.90646,
   19.34873,
   11.38087,
   14.74287,
   14.01804,
   22.09375,
   21.91148};
   gre = new TGraphErrors(18,hit_r_sctdisk_11_fx1226,hit_r_sctdisk_11_fy1226,hit_r_sctdisk_11_fex1226,hit_r_sctdisk_11_fey1226);
   gre->SetName("hit_r_sctdisk_11");
   gre->SetTitle("Disk 5");
   gre->SetFillColor(1);
   gre->SetLineColor(6);
   gre->SetLineStyle(2);
   gre->SetMarkerColor(6);
   gre->SetMarkerStyle(21);
   
   TH1F *Graph_hit_r_sctdisk_111226 = new TH1F("Graph_hit_r_sctdisk_111226","Disk 5",100,344.4606,887.1601);
   Graph_hit_r_sctdisk_111226->SetMinimum(192.857);
   Graph_hit_r_sctdisk_111226->SetMaximum(1091.642);
   Graph_hit_r_sctdisk_111226->SetDirectory(0);
   Graph_hit_r_sctdisk_111226->SetStats(0);
   gre->SetHistogram(Graph_hit_r_sctdisk_111226);
   
   multigraph->Add(gre,"lp");
   multigraph->Draw("Apl");
   multigraph->GetXaxis()->SetTitle("r [mm]");
   multigraph->GetYaxis()->SetTitle("Hits per etaModuleID");
   
   TLegend *leg = new TLegend(0.14,0.62,0.34,0.87,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextFont(62);
   leg->SetTextSize(0.04);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(10);
   leg->SetFillStyle(0);
   TLegendEntry *entry=leg->AddEntry("NULL","Sct Disk (left)","h");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("hit_r_sctdisk_0","Disk 0","lp");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("hit_r_sctdisk_1","Disk 1","lp");
   entry->SetLineColor(2);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(2);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("hit_r_sctdisk_2","Disk 2","lp");
   entry->SetLineColor(3);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(3);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("hit_r_sctdisk_3","Disk 3","lp");
   entry->SetLineColor(4);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(4);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("hit_r_sctdisk_4","Disk 4","lp");
   entry->SetLineColor(5);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(5);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("hit_r_sctdisk_5","Disk 5","lp");
   entry->SetLineColor(6);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(6);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   leg->Draw();
   
   leg = new TLegend(0.27,0.62,0.47,0.87,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextFont(62);
   leg->SetTextSize(0.04);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(10);
   leg->SetFillStyle(0);
   entry=leg->AddEntry("NULL","  (right)","h");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("hit_r_sctdisk_6","Disk 0","lp");
   entry->SetLineColor(1);
   entry->SetLineStyle(2);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("hit_r_sctdisk_7","Disk 1","lp");
   entry->SetLineColor(2);
   entry->SetLineStyle(2);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(2);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("hit_r_sctdisk_8","Disk 2","lp");
   entry->SetLineColor(3);
   entry->SetLineStyle(2);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(3);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("hit_r_sctdisk_9","Disk 3","lp");
   entry->SetLineColor(4);
   entry->SetLineStyle(2);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(4);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("hit_r_sctdisk_10","Disk 4","lp");
   entry->SetLineColor(5);
   entry->SetLineStyle(2);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(5);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   entry=leg->AddEntry("hit_r_sctdisk_11","Disk 5","lp");
   entry->SetLineColor(6);
   entry->SetLineStyle(2);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(6);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   leg->Draw();
   c1_n36->Modified();
   c1_n36->cd();
   c1_n36->SetSelected(c1_n36);
}
