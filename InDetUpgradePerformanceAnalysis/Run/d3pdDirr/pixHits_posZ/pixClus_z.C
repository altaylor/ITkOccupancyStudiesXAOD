void pixClus_z()
{
//=========Macro generated from canvas: c1_n72/c1_n72
//=========  (Tue Apr 26 00:12:48 2016) by ROOT version6.04/12
   TCanvas *c1_n72 = new TCanvas("c1_n72", "c1_n72",0,0,700,500);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   c1_n72->SetHighLightColor(2);
   c1_n72->Range(-2250,-1025.2,2250,9226.8);
   c1_n72->SetFillColor(0);
   c1_n72->SetBorderMode(0);
   c1_n72->SetBorderSize(2);
   c1_n72->SetTickx(1);
   c1_n72->SetTicky(1);
   c1_n72->SetTopMargin(0.05);
   c1_n72->SetFrameBorderMode(0);
   c1_n72->SetFrameBorderMode(0);
   
   TH1D *pixClus_z__9 = new TH1D("pixClus_z__9","pixClus_z",720,-1800,1800);
   pixClus_z__9->SetBinContent(535,6515);
   pixClus_z__9->SetBinContent(537,4201);
   pixClus_z__9->SetBinContent(571,2231);
   pixClus_z__9->SetBinContent(572,2358);
   pixClus_z__9->SetBinContent(573,6809);
   pixClus_z__9->SetBinContent(601,3607);
   pixClus_z__9->SetBinContent(602,3671);
   pixClus_z__9->SetBinContent(603,4513);
   pixClus_z__9->SetBinContent(631,2281);
   pixClus_z__9->SetBinContent(632,2285);
   pixClus_z__9->SetBinContent(633,7922);
   pixClus_z__9->SetBinContent(661,3849);
   pixClus_z__9->SetBinContent(662,3794);
   pixClus_z__9->SetBinContent(663,2763);
   pixClus_z__9->SetBinContent(695,2873);
   pixClus_z__9->SetBinContent(696,3960);
   pixClus_z__9->SetMaximum(8714.2);
   pixClus_z__9->SetEntries(63632);
   pixClus_z__9->SetStats(0);
   pixClus_z__9->Draw("");
   
   TLegend *leg = new TLegend(0.48,0.65,0.9,0.9,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextFont(62);
   leg->SetTextSize(0.04);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(10);
   leg->SetFillStyle(0);
   TLegendEntry *entry=leg->AddEntry("pixClus_z","Pixel Occupancies","l");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(62);
   leg->Draw();
   c1_n72->Modified();
   c1_n72->cd();
   c1_n72->SetSelected(c1_n72);
}
