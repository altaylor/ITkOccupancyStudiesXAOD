#include "InDetUpgradePerformanceAnalysis/QOverPtObs.h"
#include "TMath.h"

template < class observableT >
QOverPtObs< observableT >::QOverPtObs(const std::string & varname, const std::string & mctruth, const std::string & theta, Binning binning, std::string indexstring, double pt) :
    ResolutionObservable<observableT>(varname,  pt, mctruth, indexstring, binning, theta), eta_var("trk_eta")
{
    this->nameobs=varname+"_"+mctruth+"_"+theta;
}

template < class observableT >
float QOverPtObs< observableT >::EvaluateVariable(int i/*referred to the iteration we are in*/, const std::string & /*varname*/){ 

    // MC_Var, rec_var are q/p
    // add_var is mc_theta
    float qoverptMC = (this->MC_var[this->mc_index[i]]*(1./TMath::Sin(this->add_var[this->mc_index[i]])));
    float variable = ((this->rec_var[i]*TMath::CosH(eta_var[i])) - qoverptMC)/(qoverptMC);
    return variable;
}
template class QOverPtObs<float>;
