#include "InDetUpgradePerformanceAnalysis/MinMaxCut.h"

#include <sstream>

template <class T>
std::string strf(T value) {
  std::string s;
  std::stringstream sstr;
  sstr<<value;
  sstr>>s;
  return s;
}

template <class T> 
std::string classname(T value) { return ""; }

template <> std::string classname(int ) { return "int_"; }
template <> std::string classname(float ) { return "float_"; }

// Minimum cut: constructor
template <class T>
MinCut<T>::MinCut(const std::string & varname, T value) 
  : Cuts(strf(value)+"<="+classname(value)+varname),
    var(varname),
    minvar(value)
{}

// Minimum cut: implementation
template <class T>
bool MinCut<T>::AcceptTrack(int i) 
{
  return minvar <= var[i];
}

// Minimum cut: constructor
template <class T>
AbsMinCut<T>::AbsMinCut(const std::string & varname, T value) 
  : Cuts(strf(value)+"<="+classname(value)+varname),
    var(varname),
    minvar(value)
{}

// Minimum cut: implementation
template <class T>
bool AbsMinCut<T>::AcceptTrack(int i) 
{
  return minvar <= fabs(var[i]);
}

// Range Dependent Minimum cut: constructor
// If ranges is the same length as values, the last bin is taken to be inclusive
// If ranges is len(values)+1 overflow events will cause an erro
template <class T, class U>
RangeDependentMinCut<T,U>::RangeDependentMinCut(const std::string & varname, const std::string & range_varname, std::vector<U> & ranges, std::vector<T> & values, bool absrange) 
  : Cuts(range_varname+"_dependent_"+varname),
    var(varname),
    range_var(range_varname),
    minvars(values),
    ranges(ranges),
    absrange(absrange)
{
  std::cout << "RangeDependentMinCut: " << std::endl;
  for (size_t iBin=0; iBin<ranges.size()-1; iBin++) {
    printf("%.2f~%.2f: %.2f\n", ranges[iBin], ranges[iBin+1], (float) values[iBin]);
  }
  printf("\n");


}

template <class T, class U>
bool RangeDependentMinCut<T, U>::AcceptTrack(int i) 
{
  // Find bin
  U range_val = range_var[i];
  if (absrange) range_val = fabs((float)range_val);
  int trackBin=-1;
  for (size_t iBin=0; iBin<ranges.size(); iBin++) {
    if (range_val > ranges[iBin]) trackBin = iBin;
  }
  if (trackBin<0 or trackBin>=int(ranges.size())) {
    std::cerr << "ERROR bin index out of range for " << this->Name() << " bin="<<trackBin<< " val="<<range_val << std::endl;
  }

  return minvars[trackBin] <= var[i];
}



// Minimum cut: constructor
template <class T>
MaxCut<T>::MaxCut(const std::string & varname, T value) 
  : Cuts(classname(value)+varname+"<"+strf(value)),
    var(varname),
    maxvar(value)
{}

// Minimum cut: implementation
template <class T>
bool MaxCut<T>::AcceptTrack(int i) 
{
  return var[i] < maxvar;
}

// Range cut: constructor
template <class T>
RangeCut<T>::RangeCut(const std::string & varname, T vmin, T vmax) 
  : Cuts(strf(vmin)+"<"+varname+"<"+strf(vmax)),
    var(varname),
    minvar(vmin),
    maxvar(vmax)
{}

// Range cut: implementation
template <class T>
bool RangeCut<T>::AcceptTrack(int i) 
{
  return  ( minvar <= var[i]) && (var[i] < maxvar);
}

// RangeProduct cut ( var*sin(var2) ): constructor
template <class T>
RangeProductCut<T>::RangeProductCut(const std::string & varname,const std::string & varname2, T vmin, T vmax) 
  : Cuts(strf(vmin)+"<"+varname+"*sin("+varname2+")<"+strf(vmax)),
    var(varname),
    var2(varname2),
    minvar(vmin),
    maxvar(vmax)
{}

// RangeProduct cut ( var*sin(var2) ): implementation
template <class T>
bool RangeProductCut<T>::AcceptTrack(int i) 
{
  return  ( minvar <= var[i]*sin(var2[i])) && (var[i]*sin(var2[i]) < maxvar);
}

// explicit instantiation

template class RangeCut<float>;
template class RangeCut<int>;
template class MaxCut<float>;
template class MaxCut<int>;
template class MinCut<float>;
template class MinCut<int>;
template class AbsMinCut<float>;
template class AbsMinCut<int>;
template class RangeDependentMinCut<float, float>;
template class RangeDependentMinCut<int, float>;
template class RangeProductCut<float>;
