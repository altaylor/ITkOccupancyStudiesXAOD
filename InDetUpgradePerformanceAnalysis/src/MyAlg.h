#ifndef INDETUPGRADEPERFORMANCEANALYSIS_MYALG_H
#define INDETUPGRADEPERFORMANCEANALYSIS_MYALG_H 1

#include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"



class MyAlg: public ::AthAnalysisAlgorithm { 
 public: 
  MyAlg( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~MyAlg(); 

  virtual StatusCode  initialize();
  virtual StatusCode  execute();
  virtual StatusCode  finalize();
  
  virtual StatusCode beginInputFile();

 private: 

}; 

#endif //> !INDETUPGRADEPERFORMANCEANALYSIS_MYALG_H
