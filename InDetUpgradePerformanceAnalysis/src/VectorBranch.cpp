#include "InDetUpgradePerformanceAnalysis/VectorBranch.h"
#include "InDetUpgradePerformanceAnalysis/TreeHouse.h"

#include <iostream>

// ======================================================================

template <class T>
VectorBranch<T>::VectorBranch(const std::string & _name) 
  : name(_name), id(99999) {
  m_treehouse=TreeHouse::Current();
  if (name!="") {
    id=m_treehouse->GetBranchId(name);
    //std::cout<<" register "<<name<<std::endl;
  }
}


template <class T>
VectorBranch<T>::VectorBranch(const std::string & _name,VecT & var) 
  : name(_name), id(99999) {
  m_treehouse=TreeHouse::Current();
  if (name!="") {
    id=m_treehouse->AddBranch(name,var);
    //std::cout<<" register "<<name<<std::endl;
  }
}

template <class T>
const std::vector<T> & VectorBranch<T>::Get() {
  return  *(VecT*)(m_treehouse->branches[id]);
}

template <class T>
void VectorBranch<T>::Fill() {
    //std::cout << "Filling friend tree branch, #entries " << m_treehouse->GetChain()->GetEntries() << std::endl;
  m_treehouse->metabranches[id]->Fill();
}

template <class T>
T VectorBranch<T>::operator[](size_t i) {
    if ( ! (&Get())) {
        std::cerr << " Branch " << Name() << " does not exist! " << std::cerr << std::endl;
    }
    if ( ( size()) <= i) {
        std::cerr << " Branch " << Name() << " size is " << size() << " but requesting element " << i << std::endl;
    }
  return  Get()[i];
}

template <class T>
size_t VectorBranch<T>::size() {
  return (size_t) Get().size();
}



// explicit instantiation

template class VectorBranch<float>;
//template class VectorBranch<double>;
template class VectorBranch<int>;
template class VectorBranch<char>;
template class VectorBranch<ULong64_t>;
template class VectorBranch<std::vector<float> >;
template class VectorBranch<std::vector<ULong64_t> >;
