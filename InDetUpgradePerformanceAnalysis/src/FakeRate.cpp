#include "InDetUpgradePerformanceAnalysis/FakeRate.h"
#include <iostream>
#include "InDetUpgradePerformanceAnalysis/InDetUpgradeAnalysis.h"
#include "InDetUpgradePerformanceAnalysis/StandardCuts.h"
#include <vector>
#include <cmath>
#include "TH1.h"
#include "TBranch.h"
#include "TTree.h"
#include "TChain.h"
#include "TFile.h"
#include "TGraphErrors.h"
#include "TString.h"

using namespace std;

FakeRate::FakeRate(Binning binning) : 
    Observable("FakeRate"),  
    m_binning(binning),
    probability("trk_mc_probability"), eta("trk_eta") 
{
    probCut = 0.5;
}

FakeRate::FakeRate(std::string etastring, std::string probstring, Binning binning) : 
    Observable("FakeRate"),  
    m_binning(binning),
    probability(probstring), eta(etastring) 
{
        probCut = 0.5;
}

void FakeRate::Initialize(){

    nNum.resize(GetBinning()->GetNBins());
    nDen.resize(GetBinning()->GetNBins());

    for(int l=0; l < GetBinning()->GetNBins(); l++){
        nDen[l] = 0;
        nNum[l] = 0;
    }
}


void FakeRate::AnalyzeEvent(UpgPerfAna::AnalysisStep step){

    if (step!=UpgPerfAna::INITIAL) return;

    Ntrks = eta.size();///GetNTrks();

    for(int i=0; i<Ntrks; i++){
        Analyze(step,i);
    }
}


void FakeRate::Analyze(UpgPerfAna::AnalysisStep, int i){

    int bin = GetBinning()->GetBinIndex( i ) ;
 
    if (bin>=0) {
        nDen[bin]++;
        if(probability[i]<probCut) nNum[bin]++;
    }
}


void FakeRate::Finalize(){

    std::string name = "frPlot";

    int nbins = GetBinning()->GetNBins();
    double* x = new double[nbins];
    double* xerr = new double[nbins];
    double* y_frPlot = new double[nbins];
    double* yerr_frPlot = new double[nbins];
    double* yerr_num = new double[nbins];
    double* yerr_den = new double[nbins];
    double* y_num = new double[nbins];
    double* y_den = new double[nbins];

    cout << "filling fake rate plot" << endl;
    for(int i=0; i< GetBinning()->GetNBins(); i++){
        x[i] = GetBinning()->GetBinCenter(i);
        y_frPlot[i] = (double)(nNum[i])/(double)(nDen[i]);
        yerr_frPlot[i] = sqrt((y_frPlot[i] * (1-y_frPlot[i])) / ((double)nDen[i])) ;
        xerr[i] = GetBinning()->GetBinWidth(i)/2;
        y_num[i] = (double) nNum[i];
        y_den[i] = (double) nDen[i];
        yerr_num[i] = sqrt(nNum[i]);
        yerr_den[i] = sqrt(nDen[i]);
        cout << i << " : " << x[i] << ", " << y_frPlot[i] << "=" << nNum[i] << "/" << nDen[i] << endl;
    }

    frPlot = MakeGraphErrors(name, nbins, x, y_frPlot, xerr, yerr_frPlot, m_binning);
    frPlot_num = MakeGraphErrors("frPlot_num", nbins, x, y_num, xerr, yerr_num, m_binning);
    frPlot_den  = MakeGraphErrors("frPlot_den", nbins, x, y_den, xerr, yerr_den, m_binning);
    delete[] x; delete[] xerr;
    delete[] y_frPlot; delete[] yerr_frPlot;
}


void FakeRate::StorePlots(){

    frPlot_num->Write();
    frPlot_den->Write();
    frPlot->Write();
}


int FakeRate::GetNTrks(){

    return eta.size();
}
