#include "InDetUpgradePerformanceAnalysis/ObservableGroup.h"
#include "InDetUpgradePerformanceAnalysis/Cuts.h"

#include <iostream>

#include "TDirectory.h"

using namespace std;

ObservableGroup::ObservableGroup(const std::string & name)
    : Observable(name),m_cuts(0),m_run_loop(true)
{

}

// might create plots via "DefinePlots"
void ObservableGroup::Initialize()
{
    cd(path);
    for (size_t i=0; i<m_obs.size(); i++) {
        m_obs[i]->Initialize(); 
    }
    cd("..");
} 


// should perform loop over e.g. tracks (and check cuts)
void ObservableGroup::AnalyzeEvent(UpgPerfAna::AnalysisStep step) {

  if (m_vetoed_steps.find(step) != m_vetoed_steps.end()) return;

    if (m_run_loop) {
        for (size_t i=0; i<m_obs.size(); i++) {
            m_obs[i]->InitializeEvent(step);
        }    

        int n_trks = GetNTrks();

        for(int j=0; j<n_trks; j++){
            Analyze(step,j);
        }

    } else {
        for (size_t i=0; i<m_obs.size(); i++) {
            m_obs[i]->AnalyzeEvent(step);
        }    
    }
}

void ObservableGroup::Analyze(UpgPerfAna::AnalysisStep step, int j) {
    bool passTrack = true;
    if (m_cuts && step!=UpgPerfAna::EVENT) 
        passTrack = m_cuts->Count(m_cuts->AcceptTrack(j));
    if (passTrack) {
        for (size_t i=0; i<m_obs.size(); i++) {
            m_obs[i]->Analyze(step, j);
        }    
    }
}



// might create plots via "FillProfiles"
void ObservableGroup::Finalize()
{
    for (size_t i=0; i<m_obs.size(); i++) {
        m_obs[i]->Finalize(); 
    }
} 

// should store results in a root file
void ObservableGroup::StorePlots() 
{
    cd(path);
    for (size_t i=0; i<m_obs.size(); i++) {
        m_obs[i]->StorePlots(); 
    }
    cd("..");
} 

void ObservableGroup::Add(Observable* obs) 
{
    m_obs.push_back(obs);
    if (path!="") obs->SetPath(path); // *AS* needs possibility to append
    if (sensor) obs->AddSensor(sensor);
    obs->SetAnalysis(analysis);
}

void ObservableGroup::AddSensor(SensorInfoBase* s) {
    Observable::AddSensor(s);
    for (size_t i=0; i<m_obs.size(); i++) {
        m_obs[i]->AddSensor(s);
    }
}

void ObservableGroup::SetAnalysis(SingleAnalysis* a) {
    Observable::SetAnalysis(a);
    for (size_t i=0; i<m_obs.size(); i++) {
        m_obs[i]->SetAnalysis(a);
    }
}

Observable * ObservableGroup::GetObservable(const std::string & name) {
    if (name==nameobs) {
        return this;
    }
    for (size_t i=0; i<m_obs.size(); i++) {
        Observable * o=m_obs[i]->GetObservable(name);
        if (o!=0) return o;
    }
    return 0;
}


void ObservableGroup::SetCuts(Cuts* cut) 
{
    if (m_cuts) delete m_cuts;
    m_cuts=cut;
} 

ObservableGroup::~ObservableGroup()
{
    for (size_t i=0; i<m_obs.size(); i++) {
        delete m_obs[i];
    }
    m_obs.clear();
    if (m_cuts) delete m_cuts;
}

int ObservableGroup::GetNTrks() {
    if (m_obs.size()>0) {
        return m_obs[0]->GetNTrks();
    }
    std::cout<<" Warning ObservableGroup::GetNTrks() called without daugthers"<<std::endl;
    return 0;
}

