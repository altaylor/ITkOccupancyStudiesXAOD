#include "InDetUpgradePerformanceAnalysis/SensorInfoBase.h"
//#include <Python.h>
#include <iostream>

SensorInfoBase::SensorInfoBase()
{
  /*
    // Initialize a global variable for
    // display of expression results
    PyRun_SimpleString("x = 0");

    // Get a reference to the main module
    // and global dictionary
    PyObject * main_module = PyImport_AddModule("__main__");
    PyObject * global_dict = PyModule_GetDict(main_module);

    // Extract a reference to the function "func_name"
    // from the global dictionary
    std::string func_name="f_run";

    PyObject * expression =
      PyDict_GetItemString(global_dict, func_name.c_str());

    PyObject_CallObject(expression, NULL);

    PyRun_SimpleString("print x");
  */ 
}

int SensorInfoBase::getValue(const std::string & key) const {
  SensorParameters::const_iterator it=param.find(key);
  if (it!=param.end()) return (*it).second;
  std::cout<<" key "<<key<<" not found "<<std::endl;
  it = param.begin();
  for (; it!=param.end(); it++) std::cout << (*it).first << std::endl;
  return 0;
}

float SensorInfoBase::getFloat(const std::string & key) const {
  SensorFloats::const_iterator it=dim.find(key);
  if (it!=dim.end()) return (*it).second;
  std::cout<<" key "<<key<<" float not found "<<std::endl;
  return 0.;
}

void SensorInfoBase::add(const std::string & key, int value)
{
  std::cout<<"key "<<key<<" add value="<<value<<std::endl;
  param[key]=value;
}

void SensorInfoBase::addFloat(const std::string & key, float value)
{
  std::cout<<"key "<<key<<" add float value="<<value<<std::endl;
  dim[key]=value;
}

void SensorInfoBase::addSensor(const std::string & key, SensorInfoBase* sensor, int id)
{
  //  std::cout<<"key "<<key<<" add sensor"<<" id="<<id<<std::endl;
  SensorStore::iterator it = sensors.find(key);

  if (it != sensors.end()) {
    //    std::cout<<" found "<<(*it).second.size()<<" elements "<<std::endl;
  }
  else {
    it=sensors.insert(std::pair<std::string,SensorArray>(key,SensorArray())).first;
  }
  if ( (*it).second.size()==(size_t)id ) {
    //    std::cout<<" added "<<std::endl;
    (*it).second.push_back(sensor);
  }
  else {
    std::cout<<" already "<< (*it).second.size()<<" elements, insert failed "<<std::endl;
  }
}
