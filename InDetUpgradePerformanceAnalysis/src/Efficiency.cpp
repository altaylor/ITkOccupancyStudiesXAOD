#include "InDetUpgradePerformanceAnalysis/Efficiency.h"
#include <iostream>
#include "InDetUpgradePerformanceAnalysis/InDetUpgradeAnalysis.h"
#include "InDetUpgradePerformanceAnalysis/StandardCuts.h"
#include "InDetUpgradePerformanceAnalysis/TrackJetMatchCut.h"
#include <vector>
#include <cmath>
#include "TH1.h"
#include "TBranch.h"
#include "TTree.h"
#include "TChain.h"
#include "TFile.h"
#include "TGraphErrors.h"
#include "TCanvas.h"

using namespace std;

// Using this constructor, no track-jet matching required...
//
Efficiency::Efficiency(Binning binning, std::string mc_eta_name, std::string mc_pt_name, std::string mc_index_name, std::string mc_prob_name, std::string eta_name) : 
    ObservableGroup("Efficiency"), 
    m_binning(binning),
    //mcEta("mc_gen_eta"), mcPt("mc_gen_pt"), mc_index("trk_mc_index"), 
    //probability("trk_mc_probability"), eta("trk_eta")
    mcEta(mc_eta_name), mcPt(mc_pt_name), mc_index(mc_index_name), probability(mc_prob_name), 
    eta(eta_name)
{

    x_variable = 1;
    jet_index = 100001;

}

//  Used in:
//  scripts/upg_perf_analyze_btagd3pd.py:        eff=Efficiency("mcpart_eta","mcpart_pt","trk_mcpart_index","trk_mcpart_probability","trk_eta",xvar,800000)
Efficiency::Efficiency(std::string mc_eta_name, std::string mc_pt_name, std::string mc_index_name, std::string mc_prob_name, std::string eta_name, int x_var, double jetPtCut)  : 
    ObservableGroup("Efficiency"), 
    mcEta(mc_eta_name), mcPt(mc_pt_name), mc_index(mc_index_name), probability(mc_prob_name), 
    eta(eta_name)
{

    trackJetAssoc  = new TrackJetMatchCut("mcpart_eta", "mcpart_phi", "jet_antikt4truth_eta", "jet_antikt4truth_phi","jet_antikt4truth_pt","jet_antikt4truth_flavor_truth_BHadronpdg",-1.0,jetPtCut,false);
    x_variable = x_var;
    jet_index = -1;

    // Binning  1 = mc_gen_et
    //          2 = dR Jet gen particle
    //          3 = jet pt
    if(x_variable==3){
        m_binning = Binning("jet_antikt4truth_pt", 20, 0, 1200000, "jetPt", "Jet #p_{T}");
    } else if(x_variable==2){
      // Index is a dummy variable - bit of a hack
        m_binning = Binning(mc_eta_name, 20, 0, 0.1, "deltaRjet", "#Delta R(jet,track)");
    } else if (x_variable ==1) {
        m_binning = Binning(mc_eta_name, 20, 0, 2.5, "etaAbs", "#eta", true);
    } else {
        std::cerr << "Efficiency" << "Invalid x_variable:" <<  x_variable << std::endl;
    }
}

void Efficiency::Initialize()
{
  //std::cout << "Efficiency Binning " << GetBinning()->GetNBins() << " " << GetBinning()->GetMinimum() <<  " " << GetBinning()->GetMaximum() << std::endl;
    nNum.resize(GetBinning()->GetNBins());
    nDen.resize(GetBinning()->GetNBins());

    for(int l=0; l < GetBinning()->GetNBins(); l++){
        nDen[l] = 0;
        nNum[l] = 0;
    }

}

void Efficiency::AnalyzeEvent(UpgPerfAna::AnalysisStep step){
    // This is normally not used - gets called via ObservableGroup
    //cout << "  Efficiency::AnalyzeEvent " << std::endl;
    if (step!=UpgPerfAna::INITIAL) return;
    int mcN = mcEta.size();
    for(int k=0; k<mcN; k++){

        //if(mcPt[k]*.001>80/*GeV*/){ // for test only
        Analyze(step,k);
        //}
    }
}

/** Analyze a specific *truth particle* of index k
 * @param k truth particle index
 */
void Efficiency::Analyze(UpgPerfAna::AnalysisStep step, int k) {
    if (step!=UpgPerfAna::INITIAL) return;

    // Here "track" is actually the mcpart
    if(jet_index!=100001) jet_index = trackJetAssoc->AcceptAndMatchTrack(k);

    if(jet_index>=0) {

        bool isMatched = false;

        // Loop over tracks to look for one matched to this truth particle
        for(size_t iTrack=0; iTrack<mc_index.size(); iTrack++){

            if (mc_index[iTrack]==k) {

                //std::cout<<"iTrack: "<<iTrack<<" "<<"k: "<<k<<std::endl;

                bool passTrack = true;
                if (m_cuts) 
                    // Apply track quality cuts
                    passTrack = m_cuts->AcceptTrack(iTrack);

                // Require to pass track match quality
                if (passTrack && probability[iTrack]>0.5) {

                    isMatched = true;

                    break;
                }
            }
        }


        int bin=-1;
        if(x_variable==3){
        // Bin by jet_pt
            bin = GetBinning()->GetBinIndex( jet_index ) ;
            //bin = GetBinning()->GetBinIndex(jet_pt[jet_index]) ;
        } else if(x_variable==2){
            // Bin by deltaR(mcpart, jet)
            bin = GetBinning()->GetBinIndexValue( trackJetAssoc->DeltaR(k,jet_index) ) ;
        } else if (x_variable==1) {
            // bin by mcEta
            bin = GetBinning()->GetBinIndex( k ) ;
            //bin = GetBinning()->GetBinIndex( mcEta[k] ) ;
        } else {
        std::cerr << "Efficiency" << "Invalid x_variable:" <<  x_variable << std::endl;
        }

        if (bin>=0) {
            nDen[bin]++;
            if(isMatched==true){
                nNum[bin]++;
            }
        }
    }   // if jet_index>=0

}


void Efficiency::Finalize(){

    std::string title_effPlot, title_genTracks, title_recoTracks;

    if(x_variable>2.5){
        title_effPlot = "Track efficiency vs Jet P_{T}";
        title_genTracks = "Generated Tracks vs Jet P_{T}";
        title_recoTracks = "Reconstructed Tracks vs Jet P_{T}";
    }
    else if(x_variable>1.5){
        title_effPlot = "Track efficiency vs Delta R (track, jet)";
        title_genTracks = "Generated Tracks vs Delta R (track, jet)";
        title_recoTracks = "Reconstructed Tracks vs Delta R (track, jet)";
    }
    else{
        title_effPlot = "Track efficiency vs #eta";
        title_genTracks = "Generated Tracks vs #eta";
        title_recoTracks = "Reconstructed Tracks vs #eta";
    }

    int sumNum = 0, sumDen = 0;

    //float binLength = GetBinning()->GetBinWidth();
    int nbins = GetBinning()->GetNBins();

    double* x = new double[nbins];
    double* xerr = new double[nbins];
    double* y_effPlot = new double[nbins];
    double* yerr_effPlot = new double[nbins];
    double* y_genTracks = new double[nbins];
    double* yerr_genTracks = new double[nbins];
    double* y_recoTracks = new double[nbins];
    double* yerr_recoTracks = new double[nbins];

    for (int i=0; i< GetBinning()->GetNBins(); i++) {

        x[i] = GetBinning()->GetBinCenter(i);
        xerr[i] = GetBinning()->GetBinWidth(i)/2;
        if ((double)(nDen[i])<1.0) y_effPlot[i] = 0;
        else y_effPlot[i] = (double)(nNum[i])/(double)(nDen[i]);
        y_genTracks[i] = (double)(nDen[i]);
        y_recoTracks[i] = (double)(nNum[i]);
        yerr_effPlot[i] = (nDen[i]>0) ? sqrt((y_effPlot[i] * (1-y_effPlot[i])) / ((double)nDen[i])) : 0;
        yerr_genTracks[i] = (nDen[i]>0) ? sqrt((double)nDen[i]) : 0.;
        yerr_recoTracks[i] = (nNum[i]>0) ? sqrt((double)nNum[i]) : 0.;
        sumNum += nNum[i];
        sumDen += nDen[i];
        //std::cout << "EffPlot " << i << " " << nbins << " " << x[i] << " " << y_effPlot[i] << " " << xerr[i] << " " << yerr_effPlot[i] << std::endl;
    }

    effPlot = MakeGraphErrors("efficiencyPlot", nbins, x, y_effPlot, xerr, yerr_effPlot, m_binning, "#epsilon = #frac{rec trks}{true trks}");
    genTracks = MakeGraphErrors("genTracks", nbins, x, y_genTracks, xerr, yerr_genTracks, m_binning);
    recoTracks = MakeGraphErrors("recoTracks", nbins, x, y_recoTracks, xerr, yerr_recoTracks, m_binning);

    cout << "total num eff: " << sumNum << endl;
    cout << "total den eff: " << sumDen << endl;

    delete[] x;
    delete[] xerr;
    delete[] y_effPlot;
    delete[] yerr_effPlot;
    delete[] y_genTracks;
    delete[] y_recoTracks;
    delete[] yerr_genTracks;
    delete[] yerr_recoTracks;

}

void Efficiency::StorePlots()
{
    effPlot->Write();
    genTracks->Write();
    recoTracks->Write();
}

int Efficiency::GetNTrks()
{
    return mcEta.size();
}

void Efficiency::setmcEta(VectorBranch<float> branch)
{
    mcEta = branch;
}

void Efficiency::setmcPt(VectorBranch<float> branch)
{
    mcPt = branch;
}

void Efficiency::setmc_index(VectorBranch<int> branch)
{
    mc_index = branch;
}

void Efficiency::setprobability(VectorBranch<float> branch)
{
    probability = branch;
}

void Efficiency::seteta(VectorBranch<float> branch)
{
    eta = branch;
}
