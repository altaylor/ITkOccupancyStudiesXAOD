#include "InDetUpgradePerformanceAnalysis/PixelDiskPerEventOccupancy.h"
#include "InDetUpgradePerformanceAnalysis/SensorInfoBase.h"

#include "TGraphErrors.h"
#include "TString.h"
#include "TMultiGraph.h"
#include "TH2D.h"
#include "math.h"


PixelDiskPerEventOccupancy::PixelDiskPerEventOccupancy(/*float chipLength*/) :
  PerEventOccupancyObservable("PixelDiskPerEventOccupancy","pixdisk"),
  //userChipLength(chipLength),
  pixClus_x("pixClus_x"), pixClus_y("pixClus_y"), pixClus_z("pixClus_z"),
  pixClus_size("pixClus_size"), pixClus_layer("pixClus_layer"), 
  pixClus_bec("pixClus_bec"), pixClus_eta_module("pixClus_eta_module"),
  pixClus_phi_module("pixClus_phi_module"),
  pixClus_detElementId("pixClus_detElementId"),
  //pixClus_side("pixClus_side"),
  pixClus_locX("pixClus_locX"),
  pixClus_locY("pixClus_locY")
{}


void PixelDiskPerEventOccupancy::Initialize( ) {

  if (sensor == 0 ) {
    std::cerr << "ERROR " << __FUNCTION__ << " : No geometry infomation (sensor=0) " << std::endl;
  }

  // Get n_regions (Number of unique rings)
  n_disks = sensor->getValue( "n"+moduleType );
  // Count total number of rings, and assing an ID to each ring
  n_regions=0;
  // Loop over disks
  for (int iDisk=0; iDisk < n_disks; iDisk++) {
    const SensorInfoBase* diskInfo = sensor->get(moduleType,iDisk);
    int n_rings = diskInfo->getValue("nring");
    // Loop over rings
    for (int iRing=0; iRing<n_rings; iRing++) {
      for (int iSide=0; iSide < 2; iSide++) {
        int id = getRingId( (iSide*4)-2, iDisk, iRing);
        //std::cout << " side " << iSide << " disk " << iDisk << " ring " << iRing << " id " << id << " idx " << n_regions << std::endl;
        ring_indices[id] = n_regions;
        ring_ids[n_regions] = id;
        n_regions++;
      }
    }
  }

  // Base class is too barrel centric. Re-factor..
  PerEventOccupancyObservable::Initialize();
}

// Called as part of EventInit - fills plots for last event
void PixelDiskPerEventOccupancy::fillHists( std::vector< PerElementCounters > &counters, ElemType elem ) {

  // Chips not used yet for this study
  if (elem==CHIP) return;

  for (int ring=0; ring < n_regions; ring++) {
    // Skip if there are no sensor_counters (probably first event)
    if (counters[ ring ].size() ==0) continue;
    // These are floats because in normal occupancy mode they are averaged ver many events
    float maxClus=0; float maxHits=0; double maxFlu=0; float maxOcc=0; float maxClus_hits=0; float maxFluClus=0; float maxHits_clus=0;
    // Hist to stroe distribution to find mean.max
    //const char* elem_str = (elem==CHIP) ? "chip" : "sensor";
    int side, disk, loc_ring;
    getRingCoords(ring, side, disk, loc_ring);
    TString name = Form("ev%i_side%i_disk%i_ring%i", nEvents, side, disk, loc_ring);
    TString title = Form("Event %i Side %i, Disk %i, Ring %i", nEvents, side, disk, ring);
    TH1D *h_clus =     new TH1D((name+"_clus").Data(), (title+" - clusters").Data(),     100, 0.5, 100.5); 
    TH1D *h_hits =     new TH1D((name+"_hits").Data(), (title+" - hits").Data(),         750, 0.5, 750.5);
    TH1D *h_occ =      new TH1D((name+"_occ").Data(),  (title+" - occupancy").Data(),    500, 0, 100);
    TH1D *h_flu =      new TH1D((name+"_flu").Data(),  (title+" - hits/mm2").Data(),      200, 0, 2);
    TH1D *h_flu_clus = new TH1D((name+"_flu_clus").Data(),  (title+" - clusters/mm2").Data(),  50, 0, 0.1);
    h_clus->StatOverflows(kTRUE);
    h_hits->StatOverflows(kTRUE);
    h_occ->StatOverflows(kTRUE);
    h_flu->StatOverflows(kTRUE);
    h_flu_clus->StatOverflows(kTRUE); 
    // Loop over sensor_counters from last event to find maximum no. clusters in this event
    PerElementCounters::iterator counter_it = counters[ ring ].begin();
    PerElementCounters::iterator maxHits_counter;
    PerElementCounters::iterator maxClus_counter;
    //std::cout << " Start of new event, checking " << sensor_counters[ ring ].size() << " sensor_counters " << std::endl;
    for (;counter_it!=counters[ ring ].end();++counter_it) {
      // Calculate variables
      float hits = (*counter_it).second.hits();
      float occ = (*counter_it).second.occupancy();
      float clus = (*counter_it).second.cluster();
      float area = (*counter_it).second.area();
      double flu = hits / area;
      double fluClus = clus / area;
      // Check that counter doesn't have more than one detElementId
      // Ignore if counter_it does
      if ( (*counter_it).second.detElementIds.size() > 1 ) {
        printf("ERROR: counter has more than one detElementId!\n");
        printf( " Id %lli clusters: %.1f hits: %.1f detElementIds size: %i", (*counter_it).first, clus, hits, (int) (*counter_it).second.detElementIds.size());
        OccupancyObservable::ElementList::iterator el_it = (*counter_it).second.detElementIds.begin();
        for(;el_it!=(*counter_it).second.detElementIds.end(); ++el_it) printf(" %lli", (*el_it));
        printf("\n");
        continue;
      }
      // Fill hists
      h_clus->Fill(clus);
      h_hits->Fill(hits);
      h_occ->Fill(occ*100);
      h_flu->Fill(flu);
      h_flu_clus->Fill(fluClus);
      fillHist( getHistId(ring, elem, ALL_CLUS) , clus );
      fillHist( getHistId(ring, elem, ALL_HITS) , hits );
      fillHist( getHistId(ring, elem, ALL_FLU) ,  flu );
      fillHist( getHistId(ring, elem, ALL_FLU_CLUS) , fluClus );
      fillHist( getHistId(ring, elem, ALL_OCC) , occ*100 );
      // Get maxima
      if (clus > maxClus) {
        maxClus = clus;
        maxClus_hits = hits;
        maxClus_counter = counter_it;
      }
      if (hits > maxHits) {
        maxHits = hits;
        maxHits_clus = clus;
        maxHits_counter = counter_it;
      }
      if (occ > maxOcc) maxOcc = occ;
      if (flu > maxFlu) maxFlu = flu;
      if (fluClus > maxFluClus) maxFluClus = fluClus;

      if (hits > (*counter_it).second.totalPixel()) {
        //debugLargeClusters(&(*counter_it).second, (*counter_it).first, layer);
      }
    }

    // Fill hists
    fillHist( getHistId(ring, elem, MAX_HITS) , maxHits );
    fillHist( getHistId(ring, elem, MAX_HITS_CLUS) , maxHits_clus );
    fillHist( getHistId(ring, elem, MAX_OCC) , maxOcc *100);
    fillHist( getHistId(ring, elem, MAX_CLUS) , maxClus );
    fillHist( getHistId(ring, elem, MAX_CLUS_HITS) , maxClus_hits );
    fillHist( getHistId(ring, elem, MAX_FLU) , maxFlu );
    fillHist( getHistId(ring, elem, MAX_FLU_CLUS) , maxFluClus );

    fillHist( getHistId(ring, elem, MEAN_HITS) ,       h_hits->GetMean());
    fillHist( getHistId(ring, elem, MEAN_OCC) ,        h_occ->GetMean());
    fillHist( getHistId(ring, elem, MEAN_CLUS) ,       h_clus->GetMean());
    fillHist( getHistId(ring, elem, MEAN_FLU) ,        h_flu->GetMean());
    fillHist( getHistId(ring, elem, MEAN_FLU_CLUS) ,   h_flu_clus->GetMean());

    fillHist( getHistId(ring, elem, NPC_HITS) ,       find90pc(h_hits));
    fillHist( getHistId(ring, elem, NPC_OCC) ,        find90pc(h_occ));
    fillHist( getHistId(ring, elem, NPC_CLUS) ,       find90pc(h_clus));
    fillHist( getHistId(ring, elem, NPC_FLU) ,        find90pc(h_flu));
    fillHist( getHistId(ring, elem, NPC_FLU_CLUS) ,   find90pc(h_flu_clus));

    // Delete all sensor_counters
    counters[ ring ].clear();

    // Delete temporaries
    bool save_all = false;
    if (save_all) {
      extra_hists.push_back(h_clus);
      extra_hists.push_back(h_hits);
      if (h_occ) delete h_occ;
      if (h_flu) delete h_flu;
      if (h_flu_clus) delete h_flu_clus;
    } else {
      if (h_clus) delete h_clus;
      if (h_hits) delete h_hits;
      if (h_occ) delete h_occ;
      if (h_flu) delete h_flu;
      if (h_flu_clus) delete h_flu_clus;
    }
  } 
}



void PixelDiskPerEventOccupancy::Analyze(UpgPerfAna::AnalysisStep step, int k) {
  // increase event sensor_counters and find max occupancy for last event during "EVENT" step
  if (EventInit(step,k)) return;

  //std::cout << "pixClus_size.size() " << pixClus_size.size()  << std::endl;
  //std::cout << "pixClus_size[0] " << pixClus_size[0]  << std::endl;
  //std::cout << "pixClus_size[k] " << pixClus_size[k]  << std::endl;
  //pixClus_size.Get().Print();

  // enter occupancys in sensor_counters    
  if (step!=UpgPerfAna::INITIAL || pixClus_bec[k]==0) {
    return;
  }

  int loc_ring=pixClus_eta_module[k];
  int disk=pixClus_layer[k];
  int ring = getRingIndex(pixClus_bec[k], disk, loc_ring);
  int phiModule=pixClus_phi_module[k];
  int side=(pixClus_bec[k]+2)/4;

  nClus_layer[ring]++;

  ElementId sensorId = getElementId(disk, ring, phiModule, side);

  int size=pixClus_size[k]; // size in number of strips
  float locX=pixClus_locX[k];
  //float locY=pixClus_locY[k];

  //printf(" loc_ring %i ring_idx %i disk %i phiModule %i side %i sensorId %lli locX %.1f size %i \n", 
  //         loc_ring, ring, disk, phiModule, side, sensorId, locX, size );

   // loc_ring 1 ring 3 disk 0 phiModule 30 side 0 sensorId 57056 locX 14.7 size 1

  fillHist( getHistId( ring, CLUSTER, ALL_HITS) , size );

  PerElementCounters &counters = getCounters( ring );


  // Find counter for sensor, and create if non-existant
  PerElementCounters::iterator counter = counters.find( sensorId );
  if (counter == counters.end()) {
    PerEventCounter c= PerEventCounter(0., 100.);
    const SensorInfoBase* diskInfo = sensor->get(moduleType,disk);
    if (!diskInfo) std::cerr << "Cannot find sensor info for disk " << disk << ", crash soon" << std::endl;
    const SensorInfoBase* ringInfo = diskInfo->get("ring",loc_ring);
    if (!ringInfo) std::cerr << "Cannot find ring info for ring " << loc_ring << ", crash soon" << std::endl;
    c.setArea(ringInfo->getFloat("area")); 
    // Gives number of channels for this counter
    // Each counter corresponds to one "segment" of strips (ie 4 of these per module side for short strips, 2 for long)
    // perModule also corresponds to each segment
    // Separate sensor_counters for each side, so no factors of two here
    c.setTotalPixel(ringInfo->getValue("perModule") );
    //c.increaseEvent();
    counters[ sensorId ] = c;
    counter = counters.find( sensorId );
  }
  //printf("Recording cluster %.3f %.3f %.3f %i %llu\n", x,y,z,size,pixClus_detElementId[k]);
  (*counter).second.add(locX, size, pixClus_detElementId[k]);

}



int PixelDiskPerEventOccupancy::GetNTrks() {
  //std::cout<<"pixClus_n="<<pixClus_x.size()<<std::endl;
  return pixClus_x.size();
}


void PixelDiskPerEventOccupancy::defineHists() {

  defineHistLayers( LAYER, ALL_CLUS,           "layer_n_clus",                       500,  0,    200e3     );

  defineHistLayers( SENSOR, MAX_CLUS,       "sensor_max_clus",                     300,  0.5,    300.5     );
  defineHistLayers( SENSOR, MAX_HITS,       "sensor_max_hits",                     400,  0.5,    800.5     );
  defineHistLayers( SENSOR, MAX_OCC,        "sensor_max_occ",                      250,  0,    5     );
  defineHistLayers( SENSOR, MAX_FLU,        "sensor_max_flu",                      300,  0,    1.5     );
  defineHistLayers( SENSOR, MAX_FLU_CLUS,   "sensor_max_flu_clus",                 500,  0,    0.5    );
  defineHistLayers( SENSOR, MAX_CLUS_HITS,  "sensor_max_clus_hits",                500,  0.5,    500.5     );
  defineHistLayers( SENSOR, MAX_HITS_CLUS,  "sensor_max_hits_clus",                150,  0.5,    150.5     );

  defineHistLayers( SENSOR, MEAN_CLUS,       "sensor_mean_clus",                    100,  0.5,    100.5      );
  defineHistLayers( SENSOR, MEAN_HITS,       "sensor_mean_hits",                    300,  0.5,    300.5      );
  defineHistLayers( SENSOR, MEAN_OCC,        "sensor_mean_occ",                     200,  0,    1       );
  defineHistLayers( SENSOR, MEAN_FLU,        "sensor_mean_flu",                     200,  0,    0.5     );
  defineHistLayers( SENSOR, MEAN_FLU_CLUS,   "sensor_mean_flu_clus",                200,  0,    0.2);

  defineHistLayers( SENSOR, NPC_CLUS,       "sensor_90pc_clus",                    200,  0.5,    10.5       );
  defineHistLayers( SENSOR, NPC_HITS,       "sensor_90pc_hits",                    200,  0.5,    20.5      );
  defineHistLayers( SENSOR, NPC_OCC,        "sensor_90pc_occ",                     200,  0,    1      );
  defineHistLayers( SENSOR, NPC_FLU,        "sensor_90pc_flu",                     200,  0,    0.01     );
  defineHistLayers( SENSOR, NPC_FLU_CLUS,   "sensor_90pc_flu_clus",                200,  0,    0.005    );

  defineHistLayers( SENSOR, ALL_CLUS,       "sensor_all_clus",                    200,  0.5,    200.5     );
  defineHistLayers( SENSOR, ALL_HITS,       "sensor_all_hits",                    750,  0.5,    750.5     );
  defineHistLayers( SENSOR, ALL_OCC,        "sensor_all_occ",                     100,  0,    5     );
  defineHistLayers( SENSOR, ALL_FLU,        "sensor_all_flu",                     500,  0,    1.0     );
  defineHistLayers( SENSOR, ALL_FLU_CLUS,   "sensor_all_flu_clus",                300,  0,    0.3    );

  defineHistLayers( CLUSTER, ALL_HITS,      "cluster_all_hits",                    400,  0.5,    400.5     );

}

int PixelDiskPerEventOccupancy::getRingIndex( const int bec, const int disk, const int ring) {
  int id = getRingId(bec, disk, ring) ;
  std::map<int,int>::const_iterator it = ring_indices.find( id );
  if (it != ring_indices.end()) {
    return (*it).second;
  } else {
    std::cout << "Cannot find disk: bec= " << bec << " disk=" << disk << " ring=" << ring << std::endl;
    return -1;
  }
}

// ring is a unique identifier for a given ring, just used in this code
// loc_ring is the "local" ring identifier - ie 0/1/2. Need to specify side/disk/loc_ring to uniqueley identify an element
void PixelDiskPerEventOccupancy::getRingCoords(const int ring, int &side, int &disk, int &loc_ring) {
  std::map<int,int>::const_iterator it = ring_ids.find( ring );
  if (it == ring_ids.end()) {
    std::cout << "Cannot find ring with index " << ring << std::endl;
  }
  int id = (*it).second;
  //std::cout << " getRingCoords " << id << std::endl;
  loc_ring = (id>>6);
  side = (id>>3) & 0x1;
  disk = id & 0x7;
  //std::cout << " side " << side << " disk " << disk << " loc_ring " << loc_ring << std::endl;
}

void PixelDiskPerEventOccupancy::defineHistLayers(ElemType elem, PlotVariable var, TString name, int nbins, float min, float max) {
  for (int ring=0; ring < n_regions; ring++) {
    int side, disk, loc_ring;
    getRingCoords(ring, side, disk, loc_ring);
    TString this_name = Form("side%i_disk%i_ring%i_%s", side, disk, loc_ring, name.Data());
    defineHist( getHistId(ring, elem, var), this_name, nbins, min, max);
  }
}
