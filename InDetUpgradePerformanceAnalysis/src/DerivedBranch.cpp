#include "InDetUpgradePerformanceAnalysis/DerivedBranch.h"
#include "InDetUpgradePerformanceAnalysis/TreeHouse.h"

#include <iostream>

using namespace std;


template < class observableT >
DerivedBranch<observableT>::DerivedBranch(const std::string & out_branch_name,
    const std::string & formula)
  : Observable("calc_"+out_branch_name),
  m_formula(("calc_"+out_branch_name).c_str(), formula.c_str(), TreeHouse::Current()->GetChain()), 
  //m_formula_str(formula),
  m_out_branch(out_branch_name, m_outVec), m_tree_num(0)
{
}

template < class observableT >
DerivedBranch<observableT>::DerivedBranch( const DerivedBranch<observableT> &other) 
  : Observable( other.Name() ),
    m_formula(other.Name().c_str(), other.m_formula.GetExpFormula(), TreeHouse::Current()->GetChain()),
    m_out_branch(other.m_out_branch.Name()),
   m_tree_num(0)
{
}

template < class observableT >
int DerivedBranch<observableT>::GetNTrks(){
  //std::cout << "In GetNTrks for " << m_out_branch.Name() << std::endl;
  //std::cout << m_formula.GetTree() << std::endl;
  //std::cout << TreeHouse::Current()->GetChain()->GetTree()  << std::endl;
  //std::cout << TreeHouse::Current()->GetChain()->GetFile()  << std::endl;
  //std::cout << TreeHouse::Current()->GetChain()->GetFile()->GetName()  << std::endl;
  //std::cout << TreeHouse::Current()->GetChain()->GetTreeNumber()  << std::endl;
  if (m_tree_num != TreeHouse::Current()->GetChain()->GetTreeNumber()) Notify();

  // Check if TTreeFormula is pointing at the current tree 
  // Might be some better way to do this than checking every single time, 
  //  however GetNTrks() is public so could be called before AnalyzeEvent,
  //  for example by ObservableGroup

  // Find size of vector from the TTreeFormula
  //return 0;
  return m_formula.GetNdata();
}

template < class observableT >
void DerivedBranch<observableT>::Initialize()
{
  Notify();

  //Register input branches with TreeHouse
  for (int iBranch=0; iBranch < m_formula.GetNcodes(); iBranch++) {
    //std::cout << Name() << " " << m_formula.GetNcodes() << " " << iBranch << std::endl;
    //std::cout << m_formula.GetLeaf(iBranch) << std::endl;
    //std::cout << m_formula.GetLeaf(iBranch)->GetName() << std::endl;
    std::string branch = m_formula.GetLeaf(iBranch)->GetName();
    TreeHouse::Current()->GetBranchId(branch);
  }
  // Unset m_tree_num to force calling Notify before first event
  // Leaf list changes after Initialize() method (fue to friend tree branches?)
  m_tree_num = -1;

}

template < class observableT >
void DerivedBranch<observableT>::Analyze(UpgPerfAna::AnalysisStep step, int i) 
{
  if (step==UpgPerfAna::EVENT) return;
  //std::cout << "In Analyze for " << m_out_branch.Name() << " " << i << std::endl;
  // here the calculation takes place
  this->m_outVec[i]= (float) m_formula.EvalInstance(i);
}

template < class observableT >
void DerivedBranch<observableT>::StorePlots()
{}

template < class observableT >
void DerivedBranch<observableT>::Notify()
{
  // Set formula to point to current tree
  //std::cout << "In Notify() for " << m_out_branch.Name() << std::endl;
  m_tree_num = TreeHouse::Current()->GetChain()->GetTreeNumber();
  m_formula.SetTree( TreeHouse::Current()->GetChain()->GetTree() );
  m_formula.Notify();
}

template < class observableT >
void DerivedBranch<observableT>::InitializeEvent(UpgPerfAna::AnalysisStep ) {
  //std::cout << "In InitializeEvent for " << m_out_branch.Name() << " STEP " << step << std::endl;
  // Resize vector for output
  // DELETE ME!
  /*
  if (m_formula.GetTree() != TreeHouse::Current()->GetChain()->GetTree()) Notify();
  for (int iBranch=0; iBranch < m_formula.GetNcodes(); iBranch++) {
    //std::cout << Name() << " " << m_formula.GetNcodes() << " " << iBranch << std::endl;
    //std::cout << m_formula.GetLeaf(iBranch) << std::endl;
    //std::cout << m_formula.GetLeaf(iBranch)->GetName() << std::endl;
    std::cout << iBranch << " " << m_formula.GetLeaf(iBranch);
    std::cout << " " << m_formula.GetLeaf(iBranch)->GetName() << std::endl;
  }
  */
  //Notify();
  int n_trks = GetNTrks();
  m_outVec.resize(n_trks);
}


template class DerivedBranch<float>;
