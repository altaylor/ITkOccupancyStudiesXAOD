#include "InDetUpgradePerformanceAnalysis/TreeHouse.h"
#include "TROOT.h"
#include "TFile.h"
#include "TDirectory.h"

#include <iostream>
#include <fstream>

using namespace std;

TreeHouse * TreeHouse::current =0;

TreeHouse::TreeHouse(TChain * c)
  : chain(c),nmax(-1),m_friend(0), m_friend_file(0)
{
  current=this;
}

TTree * TreeHouse::AddFriend()
{
  if (!m_friend) {
    // Creates friend tree in memory - uses a LOT of memory in some jobs
    //gROOT->cd();
    char * tmpname = tmpnam(NULL);
    printf ("Writing friend tree to temporary file  %s\n", tmpname);
    TDirectory * old_dir = gDirectory;
    m_friend_file = new TFile( tmpname, "CREATE" );
    m_friend_file->cd();
    // Name given to tree must match name in helper.py
    m_friend=new TTree("friend", "friend");
    m_friend->SetDirectory(m_friend_file);
    chain->AddFriend(m_friend);
    old_dir->cd();
  }
  return m_friend;
}

TreeHouse::~TreeHouse()
{
  if (m_friend_file) {
    m_friend_file->Write();
    TString name = m_friend_file->GetName();
    m_friend_file->Close();
    remove( name );
  }
}


template <class V>
size_t TreeHouse::AddBranch(const std::string name,V & var)
{
  BMap::iterator it=bmap.find(name);
  if (it!=bmap.end()) {
    std::cout << " ERROR branch "<< name <<" does already exist at "<<it->second<<std::endl;
    return it->second;
  }
  else {
    // create friend tree if not existing and add branch
    // cout << "add branch " << name << &var << " " << branch << endl;
    TBranch *branch=AddFriend()->Branch(name.c_str(), &var);  // "var/I"
    size_t id=branches.size();
    bmap[name]=id;
    branches.push_back(&var);
    metabranches.push_back(branch);
    return id;
  }
}

void TreeHouse::Fill()
{
  if (m_friend) m_friend->Fill();
}

void TreeHouse::SelectBranches()
{
  chain->SetBranchStatus("*", 0);//activation of the branches in the chain
  for(BMap::iterator itr=bmap.begin(); itr!=bmap.end(); itr++){
    chain->SetBranchStatus(itr->first.c_str(), 1);
    chain->SetBranchAddress(itr->first.c_str(), &branches[itr->second]);
    //std::cout << "Setting branch address: " << itr->first << " " << itr->second <<  " " << &branches[itr->second] << std::endl;
  }
  chain->SetCacheSize(500e6);
  chain->SetCacheLearnEntries(100);
}


void* TreeHouse::GetBranch(std::string name){
  return branches[bmap[name]];
}


size_t TreeHouse::GetBranchId(std::string name){
  BMap::iterator it=bmap.find(name);
  if (it!=bmap.end()) {
    return it->second;
  }
  else{
    size_t id=branches.size();
    bmap[name]=id;
    branches.push_back(0);
    metabranches.push_back(chain->GetBranch(name.c_str()));
    return id;
  }
}

void TreeHouse::AddFile(const TString & fname) {
   if(fname.Contains("root", TString::kIgnoreCase)){
    if (nmax!=-1 && chain->GetEntries() >= nmax) return;
    std::cout<<fname<<std::endl;
    chain->Add(fname);
  }
  else {
    // part to parse txt file
    ifstream inputFile(fname);
    while (!inputFile.eof()) {
      std::string line;
      inputFile>>line;
      std::cout<<line<<std::endl;
      chain->Add(line.c_str());
      if (nmax!=-1 && chain->GetEntries() >= nmax) break;
    }
  } 
}


std::ostream & operator<<(std::ostream & s, const TreeHouse & th)
{
  s<<" TreeHouse \n";
  s<<" ----------------------------------- \n";
  s<<" chain entries : ";
  if (th.chain) s<<th.chain->GetEntries();
  s<<std::endl;
  s<<" nmax : "<<th.nmax<<std::endl;
  s<<" bmap size : "<<th.bmap.size()<<std::endl;
  s<<" branches  : "<<th.branches.size()<<std::endl;
  return s;
}

template size_t TreeHouse::AddBranch< std::vector<float> >(const std::string name,std::vector<float> & var);
template size_t TreeHouse::AddBranch< std::vector<double> >(const std::string name,std::vector<double> & var);
template size_t TreeHouse::AddBranch< std::vector<int> >(const std::string name,std::vector<int> & var);
template size_t TreeHouse::AddBranch< int >(const std::string name, int & var);
template size_t TreeHouse::AddBranch< float >(const std::string name, float & var);
//template size_t TreeHouse::AddBranch< double >(const std::string name, double & var);
template size_t TreeHouse::AddBranch(const std::string name,std::vector<char> & var);
template size_t TreeHouse::AddBranch(const std::string name,std::vector<ULong64_t> & var);
template size_t TreeHouse::AddBranch(const std::string name,std::vector<std::vector<float> > & var);
template size_t TreeHouse::AddBranch(const std::string name,std::vector<std::vector<ULong64_t> > & var);
