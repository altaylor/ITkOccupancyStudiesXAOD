#include "InDetUpgradePerformanceAnalysis/OccupancyObservable.h"

#include "TH2D.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TMultiGraph.h"

#include <iostream>

OccupancyObservable::OccupancyObservable(const std::string & name1, const std::string & mType) :
  Observable(name1), moduleType(mType),  ndisk(99)
{}

void OccupancyObservable::Initialize() 
{
  m_histos.push_back(new TH2D(Form("%s_clus_map2d",moduleType.c_str()),"Clusters distribution",240,-3100.,3100.,160,0.,1100.));
  m_histos.push_back(new TH2D(Form("%s_hit_map2d",moduleType.c_str()),"Hit distritbution",240,-3100.,3100.,160,0.,1100.));
  m_histos.push_back(new TH2D(Form("%s_occ_map2d",moduleType.c_str()),"Occupancy distribution",240,-3100.,3100.,160,0.,1100.));
  m_histos.push_back(new TH2D(Form("%s_flu_map2d",moduleType.c_str()),"Hit density distribution",240,-3100.,3100.,160,0.,1100.));
  m_histos.push_back(new TH2D(Form("%s_clus_map2d_detail",moduleType.c_str()),"Clusters distribution",120,-1750.,1750.,80,0.,350.));
  m_histos.push_back(new TH2D(Form("%s_hit_map2d_detail",moduleType.c_str()),"Hit distribution",120,-1750.,1750.,80,0.,350.));
  m_histos.push_back(new TH2D(Form("%s_occ_map2d_detail",moduleType.c_str()),"Occupancy distribution",120,-1750.,1750.,80,0.,350.));
  m_histos.push_back(new TH2D(Form("%s_flu_map2d_detail",moduleType.c_str()),"Hit density distribution",120,-1750.,1750.,80,0.,350.));
  m_histos.push_back(new TH1D(Form("%s_clus_size_all",moduleType.c_str()),"Cluster size - all clusters",50, 0, 50));
  m_histos.push_back(new TH1D(Form("%s_clus_size_primary",moduleType.c_str()),"Cluster size - primary clusters",50, 0, 50));
  m_histos.push_back(new TH1D(Form("%s_clus_size_secondary",moduleType.c_str()),"Cluster size - secondary clusters",50, 0, 50));
  m_histos.push_back(new TH1D(Form("%s_clus_size_unmatched",moduleType.c_str()),"Cluster size - unmatched clusters",50, 0, 50));

  for (size_t i=0;i<CLUS_SIZE_ALL;++i) {
    m_histos[i]->GetXaxis()->SetTitle("z [mm]");
    m_histos[i]->GetYaxis()->SetTitle("r [mm]");
    m_histos[i]->SetStats(kFALSE);
  }
  for (size_t i=CLUS_SIZE_ALL;i<END_HISTOS;++i) {
    m_histos[i]->GetXaxis()->SetTitle("Events");
    m_histos[i]->GetYaxis()->SetTitle("Cluster Size (No. Hits)");
  }
}

bool OccupancyObservable::EventInit(UpgPerfAna::AnalysisStep step, int k) {
  if (step==UpgPerfAna::EVENT) {
    // intialize only
    LayerIdCounters::iterator it = counters.begin();
    for (;it!=counters.end();++it) {
      (*it).second.increaseEvent();
    }
    if (k!=0) {
      std::cout<<"WARNING step=UpgPerfAna::EVENT, but k!=0"<<std::endl; 
    }
    return true;
  }
  return false;
}

void OccupancyObservable::AnalyzeEvent(UpgPerfAna::AnalysisStep step) {
  Analyze(UpgPerfAna::EVENT,0);
  for (int i=0; i<GetNTrks() ; ++i) {
    Analyze(step, i);
  }
}

//void OccupancyObservable::Analyze(UpgPerfAna::AnalysisStep, int);
//int OccupancyObservable::GetNTrks();
//void OccupancyObservable::Finalize();
void OccupancyObservable::StorePlots()
{
  // save histos and graphs
  for (std::vector<TH1*>::iterator it = m_histos.begin(); 
      it!=m_histos.end(); ++it) {
    if ( (*it)->GetEntries()>0)   (*it)->Write();
    else {
      std::cout<<"histogram "<<(*it)->GetName()<<" does not contain entries, and is not written out"<<std::endl;    }
  }
  for (std::vector<TGraph*>::iterator it = m_graphs.begin(); 
      it!=m_graphs.end(); ++it) {
    (*it)->Write();
  }
  for (std::vector<TMultiGraph*>::iterator it = m_multigraphs.begin(); 
      it!=m_multigraphs.end(); ++it) {
    (*it)->Write();
  }

}
OccupancyObservable::~OccupancyObservable()
{
  // delete histos and graphs
}

TGraphErrors * OccupancyObservable::CreateGraph(int npoints, const char * fmt, const std::string & n, int i) 
{
  TGraphErrors * g=new TGraphErrors(npoints);
  int k=i%ndisk;
  int j=i/ndisk;
  g->SetName(Form(fmt,n.c_str(),i));
  if (n=="pixlayer" || n=="sctlayer") 
    g->SetTitle(Form("Layer %d",i));
  else if (n=="pixdisk" || n=="sctdisk") 
    g->SetTitle(Form("Disk %d",k));
  else 
    g->SetTitle(g->GetName());

  // std::cout<<" CreateGraph "<<g->GetName()<<std::endl;
  // std::cout<<"             #"<<g->GetTitle()<<"#"<<std::endl;
  // std::cout<<"             #"<<n<<"#"<<k<<","<<j<<std::endl;

  //  g->SetName(Form("occ_%s_%d",moduleType.c_str(),i));
  //  g->SetName(buffer);
  g->SetMarkerStyle(20+j);
  g->SetLineStyle(1+j);
  // g->SetLineWidth(3)
  g->SetMarkerColor(1+k);
  g->SetLineColor(1+k);

  m_graphs.push_back(g);
  return g;
}

TMultiGraph * OccupancyObservable::CreateMultiGraph(const char * fmt, const std::string & n)
{
  TMultiGraph *mg = new TMultiGraph();
  mg->SetName(Form(fmt,n.c_str()));

  m_multigraphs.push_back(mg);  
  return mg;
}


// For each bin, multply by factor then divide by number of clusters in that bin
// We divide by number of clusters because the occupancies are calculated properly in OccupancyObservable
// but then in order to fill the occupancy map we loop over clusters to dicsover where the modules are
// Of course this leads to double counting so have to divide by ocupancy
void OccupancyObservable::NormalizeOccupancyMap(OHisto clus_id, OHisto occ_id, float factor)
{
  TH2D * h_clus = (TH2D*)m_histos[clus_id];
  TH2D * h_occ = (TH2D*)m_histos[occ_id];
  TAxis * xaxis = h_clus->GetXaxis();
  TAxis * yaxis = h_clus->GetYaxis();
  if (h_occ->GetXaxis()->GetNbins()!=xaxis->GetNbins() ||
      h_occ->GetYaxis()->GetNbins()!=yaxis->GetNbins() ) {
    std::cout<<"ERROR bining mismatch!"<<std::endl;
    return;
  }
  // TODO add xmin,xmax, etc. checks!

  // TODO check binning consistency
  for (int i=1; i<=xaxis->GetNbins();++i) {
    for (int j=1; j<=yaxis->GetNbins();++j) {
      double o=factor*h_occ->GetBinContent(i,j);
      if (o>0.) {
        double c=h_clus->GetBinContent(i,j);
        if (c<1) {
          printf("ERROR - c<1. Should not happen! c=%e, coords: i j %i %i Histograms: %s %s\n", c, i, j, h_occ->GetName(), h_clus->GetName());
          printf("        h_occ bins %i %i h_clus bins %i %i\n", h_occ->GetXaxis()->GetNbins(), h_occ->GetYaxis()->GetNbins(), h_clus->GetXaxis()->GetNbins(), h_clus->GetYaxis()->GetNbins());
        }
        //printf("NormalizeOccupancyMap Occ %.5f Clusters %.4f NewOcc %.5f\n", o, c, o/c);
        h_occ->SetBinContent(i,j,o/c);
      }
    }
  }
}



// ======================================================================

OccupancyObservable::Counters::Counters() :
  sum_evnt(0),sum_x(0.),sum_y(0.),sum_z(0.),sum_r(0.),f_area(0.),
  sum_clus(0),sum_clussize(0),sum_clussize2(0),
  tot_pix(0)
{}

float OccupancyObservable::Counters::occupancy() const
{
  //std::cout<<"sum_evnt="<<sum_evnt<<std::endl;
  return float(sum_clussize)/(float(tot_pix) *float(sum_evnt));
}

double fsqr(int x) { double a = (double)x; return a*a; }
double fsqr(long x) { double a = (double)x; return a*a; }

float OccupancyObservable::Counters::occupancyError() const
{
  // TODO: check error formula
  float n=float(sum_clus);
  if (n>2.) {
    // N.E - I don't get the n*n 
    //double var=n*n/(n-1) * (sum_clussize2 - 1./n *fsqr(sum_clussize));
    double var=1/(n-1) * (sum_clussize2 - 1./n *fsqr(sum_clussize));
    return sqrt(var/n)/float(tot_pix * sum_evnt);
  }
  else {
    return occupancy();
  }
}

float OccupancyObservable::Counters::hits() const
{
  //std::cout<<"sum_evnt="<<sum_evnt<<std::endl;
  return float(sum_clussize)/float(sum_evnt);
}

float OccupancyObservable::Counters::hitsError() const
{
  // TODO: check error formula
  float n=float(sum_clus);
  if (n>2.) {
    double var=n*n/(n-1) * (sum_clussize2 - 1./n *fsqr(sum_clussize));
    return sqrt(var/n)/float(sum_evnt);
  }
  else {
    return hits();
  }
}


float OccupancyObservable::Counters::cluster() const
{
  //std::cout<<"sum_evnt="<<sum_evnt<<std::endl;
  return float(sum_clus)/float(sum_evnt);
}

float OccupancyObservable::Counters::clusterError() const
{
  // TODO: check error formula  
  float n=float(sum_clus);
  if (n>2.) {
    return sqrt(n)/float(sum_evnt);
  }
  else {
    return cluster();
  }
}
