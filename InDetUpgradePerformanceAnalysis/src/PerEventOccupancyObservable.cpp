
#include "InDetUpgradePerformanceAnalysis/PerEventOccupancyObservable.h"
#include "InDetUpgradePerformanceAnalysis/SensorInfoBase.h"

#include "TGraphErrors.h"
#include "TString.h"
#include "TMultiGraph.h"
#include "TH2D.h"
#include "math.h"


PerEventOccupancyObservable::PerEventOccupancyObservable(const std::string & name1, const std::string & mType ) :
  Observable(name1),
  moduleType(mType),  
  n_regions(0),
  nEvents(0)
{}

void PerEventOccupancyObservable::defineHist(long id, TString name, int nbins, float min, float max) {
  m_hmap[id] =  new TH1D( name, name, nbins, min, max );
  m_hmap[id]->StatOverflows(kTRUE);
}

void PerEventOccupancyObservable::fillHist(long id, double var, double weight) {
  std::map<long, TH1*>::iterator it = m_hmap.find(id);
  if ( it == m_hmap.end()) {
    std::cerr << "ERROR - Histogram not found" << id << std::endl;
  } else {
    (*it).second->Fill(var, weight);
  }
}

void PerEventOccupancyObservable::defineHistLayers(ElemType elem, PlotVariable var, TString name, int nbins, float min, float max) {
  for (int layer=0; layer < n_regions; layer++) {
    TString this_name("layer");
    this_name += layer;
    this_name += "_";
    this_name += name;
    defineHist( getHistId(layer, elem, var), this_name, nbins, min, max);
  }
}

long PerEventOccupancyObservable::getHistId(int layer, ElemType elem, PlotVariable  var, int ring) {
  // var = (id>>11) 
  // ring = (id>>8) & 0x2
  // layer = (id>>4) & 0xF
  // elem = id & 0xF
  long id =  (var<<11) + (ring<<8) + (layer<<4) + elem;
  return id;
}


void PerEventOccupancyObservable::Initialize() {
  // create standard histograms
  std::cout << Name() << " : n_regions=" << n_regions << std::endl;

  nClus_layer.resize(n_regions, 0);
  sensor_counters.resize(n_regions);
  chip_counters.resize(n_regions);
  m_histos.resize(n_regions);
  m_histos_chip.resize(n_regions);

  defineHists();

}

// Called at the start of every event
// Calculate the min/max rates for the previous event and clear counters
bool PerEventOccupancyObservable::EventInit(UpgPerfAna::AnalysisStep step, int k) {

  if (step==UpgPerfAna::EVENT) {
    nEvents += 1;
    // Fill hists for sensors
    fillHists( sensor_counters, SENSOR );
    // Fill hists for chips
    fillHists( chip_counters, CHIP );
    for (int layer=0; layer < n_regions; layer++) {
      // Fill counter for total numer of clusters per layer
      fillHist( getHistId( layer, LAYER, ALL_CLUS) , nClus_layer[layer] );
    }
    // Reset counter for this event
    nClus_layer.resize(n_regions, 0);

    // Sanity check
    if (k!=0) {
      std::cout<<"WARNING step=UpgPerfAna::EVENT, but k!=0"<<std::endl; 
    }
    return true;
  }
  return false;
}

void PerEventOccupancyObservable::Finalize() {

}

void PerEventOccupancyObservable::StorePlots() {
  for (std::map<long, TH1*>::iterator it = m_hmap.begin(); it != m_hmap.end(); it++) {
    if ( (*it).second->GetEntries()>0)   {
      (*it).second->Write();
    } else {
      std::cout<<"histogram "<<(*it).second->GetName()<<" does not contain entries, and is not written out"<<std::endl;
    }
  }

  for (std::vector<TH1*>::iterator it = extra_hists.begin(); 
      it!=extra_hists.end(); ++it) {
    if ( (*it)->GetEntries()>0)   {
      (*it)->Write();
    } else {
      std::cout<<"histogram "<<(*it)->GetName()<<" does not contain entries, and is not written out"<<std::endl;
    }
  }
}

double PerEventOccupancyObservable::find90pc(TH1D* hist) {
  double integral = hist->Integral(0, hist->GetNbinsX()+1); // include overflows
  for (int iBin=1; iBin <= hist->GetNbinsX(); iBin++) {
    if ( hist->Integral(0, iBin) / integral >= 0.9 ) {
      return hist->GetBinCenter( iBin );
    }
  }
  return -999;
}

// Get the vector of counters for a given region
PerEventOccupancyObservable::PerElementCounters& PerEventOccupancyObservable::getCounters( int region, ElemType elem ) {
  std::vector< PerElementCounters > *collection=0;
  if (elem == CHIP) collection = &chip_counters;
  else if (elem == SENSOR) collection = &sensor_counters;
  else std::cerr << "Cannot provide PerElementCounters for element type " << elem << std::endl;
  if ((size_t)region >= collection->size()) std::cerr << "Cannot find region " << region << " in PerElementCounters collection for element " << elem << std::endl;
  return collection->at(region);
}

PerEventOccupancyObservable::PerEventCounter::PerEventCounter() :
  f_area(0.),
  sum_clussize(0),
  tot_pix(0),
  elem_start(0.), elem_end(100.)
{}

PerEventOccupancyObservable::PerEventCounter::PerEventCounter(double elem_start, double elem_end) :
  f_area(0.),
  sum_clussize(0),
  tot_pix(0),
  elem_start(elem_start), elem_end(elem_end)
{}

float PerEventOccupancyObservable::PerEventCounter::occupancy() const
{
  //std::cout<<"sum_evnt="<<sum_evnt<<std::endl;
  return float(sum_clussize)/(float(tot_pix) );
}

float PerEventOccupancyObservable::PerEventCounter::occupancyError() const
{
  return 0;
  /*
  // TODO: implement
  float n=float(sum_clus);
  if (n>2.) {
  // N.E - I don't get the n*n 
  //double var=n*n/(n-1) * (sum_clussize2 - 1./n *fsqr(sum_clussize));
  double var=1/(n-1) * (sum_clussize2 - 1./n *fsqr(sum_clussize));
  return sqrt(var/n)/float(tot_pix * sum_evnt);
  }
  else {
  return occupancy();
  }
  */
}

float PerEventOccupancyObservable::PerEventCounter::hits() const
{
  //std::cout<<"sum_evnt="<<sum_evnt<<std::endl;
  return float(sum_clussize);
}

float PerEventOccupancyObservable::PerEventCounter::hitsError() const
{
  return 0;
  // TODO: implement
  /*
     float n=float(sum_clus);
     if (n>2.) {
     double var=n*n/(n-1) * (sum_clussize2 - 1./n *fsqr(sum_clussize));
     return sqrt(var/n)/float(sum_evnt);
     }
     else {
     return hits();
     }
     */
}


float PerEventOccupancyObservable::PerEventCounter::cluster() const
{
  //std::cout<<"sum_evnt="<<sum_evnt<<std::endl;
  return float(clus_sizes.size());
}

float PerEventOccupancyObservable::PerEventCounter::clusterError() const
{
  return 0;
  // TODO: implement
  /*
     float n=float(sum_clus);
     if (n>2.) {
     return sqrt(n)/float(sum_evnt);
     }
     else {
     return cluster();
     }
     */
}


