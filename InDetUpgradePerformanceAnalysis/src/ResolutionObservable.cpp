#include "TMath.h"
#include "InDetUpgradePerformanceAnalysis/ResolutionObservable.h"
#include <vector>
#include <cmath>
#include "TH1.h"
#include "TBranch.h"
#include "TTree.h"
#include "TChain.h"
#include "TFile.h"
#include "TGraphErrors.h"
#include "TDirectory.h"
#include <string>
#include <iostream>
#include <sstream>
#include "InDetUpgradePerformanceAnalysis/InDetUpgradeAnalysis.h"

// Helper function to wrap values bigger/smaller than pi
// over the disconuity
float wrap_pi(float value) {
  if(value>TMath::Pi()){ 
    value = 2*TMath::Pi()-value;
  } else if(value<(-TMath::Pi())){ 
    value = -2*TMath::Pi()-value; 
  }
  return value;
}


using namespace std;

    template < class observableT >
ResolutionObservable<observableT>::ResolutionObservable(const std::string & name1, double pt, const std::string & name2, std::string indexstring, Binning binning, const std::string & name3, std::string indexstring_2)
    : Observable(name1+"_"+name2+"_"+name3),// InitialisBinning, e base class
    m_binning(binning), // Initialise BinningTool
    varname(name1), m_pt(pt), 
    m_doFit(false), m_maxRangeFinal(-1), m_doDoubleGaussian(false),
    mc_index(indexstring), 
    mc_index_2(indexstring_2), rec_var(name1), 
    MC_var(name2), add_var(name3)
{

        minh=-1.01;  maxh=1.01;
        nbinshisto=102;

}

template < class observableT >
int ResolutionObservable<observableT>::GetNTrks(){
    /*
       std::cout << "varname="<< varname << std::endl;
       std::cout << "name="<<eta.Name() << std::endl;
       std::cout << "size="<<eta.size() << std::endl;
       */
    return rec_var.size();
}

template < class observableT >
void ResolutionObservable<observableT>::Initialize() {
    DefinePlots();
}

template < class observableT >
void ResolutionObservable<observableT>::DefinePlots(){


    //std::cout << "ResolutionObservable<observableT>::DefinePlots()" << std::endl;
    //std::cout << "GetBinning() " <<GetBinning()  << std::endl;
    //std::cout << "GetBinning() " <<GetBinning()  << std::endl;
    //std::cout << "GetNBins() " << GetBinning()->GetNBins()  << std::endl;
    int count=0;
    cd("initial");

    for(int i=0; i < GetBinning()->GetNBins(); i++,count++){
        TH1D *histo_i = new TH1D(Form("%s_%s_%i",nameobs.c_str(), m_binning.name().c_str(), count), 
                Form("distribution of %s in bin %i ", nameobs.c_str(), count),  
                nbinshisto, minh, maxh);
        histo_i->StatOverflows();
        initialPlots.push_back(histo_i); 
        outlier_width.push_back(0);
        outlier_mean.push_back(0);
        sumw.push_back(0);
        sumwx.push_back(0);
        sumwx2.push_back(0);
    }

    cd("..");
}

template < class observableT >
void ResolutionObservable<observableT>::DefineFinalPlots(){

    cd("final");
    double *rms_final =  new double[GetBinning()->GetNBins()];
    double *mean =  new double[GetBinning()->GetNBins()];

    std::cout << "Making final plots for " << this->Name() << std::endl;

    for(int i=0; i < GetBinning()->GetNBins(); i++){

      double rms_full = initialPlots[i]->GetRMS();
      double mean_full = initialPlots[i]->GetMean(); // Need to get mean before changing limits to catch outliers
      double integral = initialPlots[i]->Integral(); // Need to get mean before changing limits to catch outliers
      // Calculate RMS (a la root) using only +1 -> -1. Canot use hist for this as want unbinned version
      double rms_hist = sqrt(fabs(sumwx2[i]/sumw[i] - pow(sumwx[i]/(sumw[i]),2)));
      double mean_hist = sumwx[i]/sumw[i];
      // If at least 96% of the points are in the hist (-1 to 1), use the RMS without overflow
      // range of the final plot to avoid problems with one or two outliers
      // making the range far too wide and subsequent fits/RMS not being determined
      if (integral > 0.97 * initialPlots[i]->GetEntries()) {
        rms_final[i] = rms_hist;
        mean[i] = mean_hist;
        //rms_final[i] = min(rms_hist, rms);
      } else {
        rms_final[i] = rms_full;
        mean[i] = mean_full;
      }
        //std::cout << initialPlots[i]->GetName() << " " << rms_full << " " << rms_hist << " " << rms_final[i] << " " << mean_full << " " << mean_hist << " " << mean[i] << std::endl;
    }

    for(int i=0; i < GetBinning()->GetNBins();i++){

      double rms = rms_final[i];
      double rms_interp;

      // calculate RMS by interpolating neighbouring points
      if (i==0) {
        rms_interp = 2*rms_final[i+1] - rms_final[i+2];
      } else if ( i==GetBinning()->GetNBins()-1) {
        rms_interp = 2*rms_final[i-1] - rms_final[i-1];
      } else {
        rms_interp = (rms_final[i-1] + rms_final[i+1] ) /2;
      }
    // If this is very different from the RMS for this bin, chalk it up as psychological
    // and use the interporlated one
      //std::cout << "Interpolated RMS: " << rms_interp << " not " << rms_final[i] << std::endl;
      //if ( (rms_interp>0) && (rms_final[i]) > (10.*rms_interp) ) {
      //  rms = rms_interp;
      //std::cout << "Using Interpolated " << std::endl;
      //}

        maxh = mean[i] + rms*5;
        minh = mean[i] - rms*5;
        
      // If min/ax wider than specified maximum, force back
      if ( m_maxRangeFinal>0 && maxh>m_maxRangeFinal) {
          maxh = m_maxRangeFinal;
          minh = -1*m_maxRangeFinal;
      }

        TH1D *histo_c = new TH1D(Form("%s_%s_%i",nameobs.c_str(), m_binning.name().c_str(), i), 
                Form("distribution of %s in bin %i ", nameobs.c_str(), i), 
                nbinshisto*10, minh, maxh);
        TH1D *histo_o = new TH1D(Form("%s_outliers_%s_%i",nameobs.c_str(), m_binning.name().c_str(), i), 
                Form("outliers distribution of %s in bin %i ", nameobs.c_str(), i), 
                nbinshisto, minh, maxh);
        histo_c->StatOverflows();
        histo_o->StatOverflows();
        controlPlots.push_back(histo_c); 
        outlierPlots.push_back(histo_o); 
        outlier_width[i] = maxh;
        outlier_mean[i] = mean[i];

        printf("%s mean: %.4f rms %.4f outlier width %.4f maxh %.4f minh %.4f \n" , histo_c->GetName(), mean[i], rms, outlier_width[i], maxh, minh);
        histo_c->Print();
    }
    cd("..");
}

    template < class observableT >
void ResolutionObservable<observableT>::Analyze(UpgPerfAna::AnalysisStep flag, int i)
{

    // First event of the CONTROL step need to define control plots
    if ((flag == UpgPerfAna::CONTROL) && (controlPlots.size() ==0) ) {
        DefineFinalPlots();
    }

    FillPlots(flag,i);
}

template < class observableT >
void ResolutionObservable<observableT>::FillPlots(UpgPerfAna::AnalysisStep flag, int i/*referred to the iteration we are in*/){

    int bin = GetBinning()->GetBinIndex(i);
    if (bin>=0) {
        float variable = EvaluateVariable(i, varname);
        if (variable>1.e6) return;
        if (flag == UpgPerfAna::INITIAL) {
          //if (variable < -1.0) std::cout << " UNDERFLOW " << Name() << " " << variable << std::endl;
            initialPlots[bin]->Fill(variable);
            // Calculate out own stats for -1 -> 1 range (most difference observables)
            if (fabs(variable)<1.) {
            sumw[bin]+=1;
            sumwx[bin]+=variable;
            sumwx2[bin]+=variable*variable;
            }
        }
        else if(flag == UpgPerfAna::CONTROL) {
            if (fabs(outlier_mean[bin]-variable) > outlier_width[bin]) {
              //std::cout << controlPlots[bin]->GetName() << " filling outlier variable " << variable << " mean " << outlier_mean[bin] << " width " << outlier_width[i] << std::endl;
                outlierPlots[bin]->Fill(variable);
            } else {
                controlPlots[bin]->Fill(variable);
            }
        }
    }
}

/*
   double ResolutionObservable<observableT>::GetEta(int bin){

   double binLength = (etaMax - etaMin)/nEtaBins;
   double halfBin = binLength/2;
   double etaValue = (etaMin + (binLength*bin)) + halfBin;
   return etaValue;
   }
   */

template < class observableT >
void ResolutionObservable<observableT>::Finalize(){

    // Define control plots if they do not exist.
    // They should already exist by this point, but if
    // no events have passed the cuts they will not
    if (controlPlots.size() ==0 ) {
        DefineFinalPlots();
    }

    // fill profiles

    int nbins = GetBinning()->GetNBins();
    double* x = new double[nbins]; 
    double* xerr = new double[nbins]; 
    double* y_res = new double[nbins];  
    double* yerr_res = new double[nbins]; 
    double* y_bias = new double[nbins];  
    double* yerr_bias = new double[nbins]; 
    double* y_bias_init = new double[nbins];  
    double* yerr_bias_init = new double[nbins]; 
    double* y_sum = new double[nbins];  
    double* yerr_sum = new double[nbins]; 
    double* y_frac = new double[nbins]; 
    double* yerr_frac = new double[nbins]; 
    double* y_tail = new double[nbins]; 
    double* yerr_tail = new double[nbins]; 

    //cout <<" filling profile " << nameobs << endl;

    for (int i=0; i<nbins; i++) {

        // Get X and X error
        x[i] = GetBinning()->GetBinCenter(i);
        xerr[i] = GetBinning()->GetBinWidth(i)/2;

        // Set resolution point
        ResolutionResult res = GetResolution(controlPlots[i]);
        y_res[i] = res.res;
        if(m_pt>0) y_res[i] =  y_res[i] / m_pt;
        yerr_res[i] = res.reserr;
        if(m_pt>0) yerr_res[i] =  yerr_res[i] / m_pt;

        // Set bias graph point
        y_bias[i] = res.bias;
        yerr_bias[i] = res.biaserr;

        // Set bias graph point (with outliers)
        y_bias_init[i] = initialPlots[i]->GetMean();
        yerr_bias_init[i] = (initialPlots[i]->GetMeanError());

        // Set sum outliers graph point
        y_sum[i] = outlierPlots[i]->IntegralAndError(0, outlierPlots[i]->GetNbinsX()+1, yerr_sum[i]);
        //yerr_sum[i] = (outlierPlots[i]->GetMeanError());

        // Set outlier fraction graph point
        y_frac[i] = ((double)outlierPlots[i]->GetEntries()) / ((double)initialPlots[i]->GetEntries()); 
        // Use binomial error formula, since outlier fraction is N(pass some cut)/N(all)
        yerr_frac[i] = sqrt((y_frac[i] * (1-y_frac[i])) / (initialPlots[i]->GetEntries())) ;

        // Set tail fraction graph point
        y_tail[i] = 1.0 - controlPlots[i]->Integral( controlPlots[i]->FindBin(res.bias-2*res.res), controlPlots[i]->FindBin(res.bias+2*res.res) ) / controlPlots[i]->Integral() ;
        // Use binomial error formula, since outlier fraction is N(pass some cut)/N(all)
        yerr_tail[i] = sqrt((y_tail[i] * (1-y_tail[i])) / (controlPlots[i]->GetEntries())) ;

    }

    // Make graph of resolutions
    resPlot = MakeGraphErrors(nameobs+"_res", nbins, x, y_res, xerr, yerr_res, m_binning);
    biasPlot = MakeGraphErrors(nameobs+"_bias", nbins, x, y_bias, xerr, yerr_bias, m_binning);
    biasPlot_init = MakeGraphErrors(nameobs+"_bias_withOutliers", nbins, x, y_bias_init, xerr, yerr_bias_init, m_binning);
    fracPlot = MakeGraphErrors(nameobs+"_outlierFrac", nbins, x, y_frac, xerr, yerr_frac, m_binning);
    tailPlot = MakeGraphErrors(nameobs+"_tailFrac", nbins, x, y_tail, xerr, yerr_tail, m_binning);
    sumPlot = MakeGraphErrors(nameobs+"_outlierSum", nbins, x, y_sum, xerr, yerr_sum, m_binning);

    delete[] x;
    delete[] xerr;
    delete[] y_res;
    delete[] yerr_res;
    delete[] y_bias;
    delete[] yerr_bias;
    delete[] y_bias_init;
    delete[] yerr_bias_init;
    delete[] y_sum;
    delete[] yerr_sum;
    delete[] y_frac;
    delete[] yerr_frac;
}

  template < class observableT >
ResolutionResult ResolutionObservable<observableT>::GetResolution(TH1* hist)
{

  ResolutionResult res;

  if (m_doFit) {
    double rms = hist->GetRMS();
    if (m_doDoubleGaussian) {
      // Prefit to get guess for paramaters of first two gaussains
      TF1 *g1 = new TF1("g1", "gaus", -1*rms, rms);
      TF1 *g2 = new TF1("g2", "gaus");
      hist->Fit(g1, "R");
      hist->Fit(g2);
      TF1* total = new TF1("total", "gaus(0)+gaus(3)+gaus(6)" );
      Double_t par[9];
      par[6]=0; par[7]=0;
      g1->GetParameters(&par[0]);
      g2->GetParameters(&par[3]);
      // If the central gaussian is much narrower than the RMS
      // set the initial width of the last gaussian to the RMS 
      // so it can model the tail
      // If the central gaussian is similar to the RMS, don't
      // want to do this otherwise third gaussian will compete with first
      if ( g1->GetParameter(2) < 0.2*rms ) {
        par[8] = rms;
      } else {
        par[8] = 0;
      }
      // If the prefits for the restricted and full range are the same
      // bump one up a bit to help the fit along
      if (par[5]<1.2*par[2]) {
        par[5] = par[2]*1.2;
      }
      total->SetParameters(par);

      double nmax = hist->Integral()/10;
      total->SetParLimits(0,0, nmax);
      total->SetParLimits(3,0, nmax);
      total->SetParLimits(6,0, nmax);

      total->SetParLimits(2,0, 10);
      total->SetParLimits(5,0, 10);
      total->SetParLimits(8,0, 1e6);

      hist->Fit("total");
      // figure out which gaussian ended up as the core one
      int iCore;
      double rms_1 = fabs(total->GetParameter(2)) ;
      double rms_2 = fabs(total->GetParameter(5)) ;
      double rms_3 = fabs(total->GetParameter(8)) ;
      if ( (rms_1<rms_2||rms_2==0) && (rms_1<rms_3||rms_3==0) && rms_1!=0) {
        iCore = 0;
      } else if ( (rms_2<rms_3||rms_3==0) && rms_2!=0) {
        iCore = 3;
      } else {
        iCore = 6;
      }
      // need to fabs as sometimes Minuit figs -ve for the sigma (since is squared in gaus)
      res.bias = fabs(total->GetParameter(iCore+1));
      res.res = fabs(total->GetParameter(iCore+2));
      res.biaserr = fabs(total->GetParError(iCore+1));
      res.reserr = fabs(total->GetParError(iCore+2));
      if (total->GetNDF() !=0) res.chi2 = total->GetChisquare()/total->GetNDF();

    } else {
      double mean = hist->GetMean();
      double fit_range_low = mean - 2 * rms;
      double fit_range_high = mean + 2 * rms;
      TF1 *f1 = new TF1("f1", "gaus", fit_range_low, fit_range_high);
      f1->GetName(); // Avoid compiler warnings
      TFitResultPtr fit = hist->Fit("f1", "RS");
      if ((int) fit==0) {
        res.bias = fit->Parameter(1);
        res.res = fit->Parameter(2);
        res.biaserr = fit->ParError(1);
        res.reserr = fit->ParError(2);
        if (fit->Ndf() !=0) res.chi2 = fit->Chi2()/fit->Ndf();
        //std::cout << "Mean: " << res.bias << " +/- " << res.biaserr << " Sigma: " << res.res << " +/- " << res.reserr << " Chi2/ndf " << res.chi2 << std::endl;
      } else {
        std::cout << "ERROR Fit failed! Dropping back to RMS" << std::endl;
      }
    }
  } else {
    // Default - use RMS / simple mean
    //
    double rms = hist->GetRMS();
    double last_rms = 1e6;
    for (int i=0; i<10; i++) {
      double mean = hist->GetMean();
      hist->GetXaxis()->SetRangeUser(mean-5*rms, mean+5*rms);
      last_rms = rms;
      rms = hist->GetRMS();
      if (fabs(last_rms -rms)/rms < 0.01) break;
    }
    res.res = rms;
    res.reserr = hist->GetRMSError();
    res.bias = hist->GetMean();
    res.biaserr = hist->GetMeanError();
    res.chi2=-1;
  }

  return res;
}

    template < class observableT >
void ResolutionObservable<observableT>::StorePlots()
{

    cd("initial");
    for(int i=0; i<GetBinning()->GetNBins(); i++){
        SafeWrite((TObject*) initialPlots[i]);
    }
    cd("..");

    cd("final");
    for(int i=0; i<GetBinning()->GetNBins(); i++){
        SafeWrite((TObject*) controlPlots[i]);
        //SafeWrite((TObject*) outlierPlots[i]);
    }
    cd("..");

    SafeWrite((TObject*) resPlot);
    SafeWrite((TObject*) biasPlot);
    SafeWrite((TObject*) biasPlot_init);
    SafeWrite((TObject*) fracPlot);
    SafeWrite((TObject*) tailPlot);
    SafeWrite((TObject*) sumPlot);
}

template class ResolutionObservable<float>;
template class ResolutionObservable<int>;
