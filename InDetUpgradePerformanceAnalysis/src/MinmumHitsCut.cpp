#include "InDetUpgradePerformanceAnalysis/MinimumHitsCut.h"
#include "TText.h"

MinimumHitsCut::MinimumHitsCut(int value, const std::string & pixHitsbranch, const std::string & sctHitsbranch)
  : Cuts(Form("%s+%s >= %d",pixHitsbranch.c_str(),sctHitsbranch.c_str(),value)),nPixHits(pixHitsbranch),nSCTHits(sctHitsbranch),minHits(value) 
{}

bool MinimumHitsCut::AcceptTrack(int i)
{
  return nPixHits[i]+nSCTHits[i] >= minHits;
}
