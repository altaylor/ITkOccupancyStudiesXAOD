
#include "GaudiKernel/DeclareFactoryEntries.h"

#include "../MyAlg.h"

DECLARE_ALGORITHM_FACTORY( MyAlg )

DECLARE_FACTORY_ENTRIES( InDetUpgradePerformanceAnalysis ) 
{
  DECLARE_ALGORITHM( MyAlg );
}
