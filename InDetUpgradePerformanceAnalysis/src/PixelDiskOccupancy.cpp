#include "InDetUpgradePerformanceAnalysis/PixelDiskOccupancy.h"
#include "InDetUpgradePerformanceAnalysis/SensorInfoBase.h"

#include "TGraphErrors.h"
#include "TMultiGraph.h"
#include "TH2D.h"


PixelDiskOccupancy::PixelDiskOccupancy() :
  DiskOccupancy("PixelDiskOccupancy","pixdisk"),
  pixClus_x("pixClus_x"), pixClus_y("pixClus_y"), pixClus_z("pixClus_z"),
  pixClus_size("pixClus_size"), pixClus_layer("pixClus_layer"), 
  pixClus_bec("pixClus_bec"), pixClus_eta_module("pixClus_eta_module"),
  pixClus_detElementId("pixClus_detElementId")
 {}

void PixelDiskOccupancy::Initialize() {
  // create standard histograms
  OccupancyObservable::Initialize();

  // prepare counters for each etaID and disk
  if (sensor != 0 ) {
    ndisk = sensor->getValue("npixdisk");
    for (int i=0; i<ndisk; ++i) {
      const SensorInfoBase* disk = sensor->get(moduleType,i);
      int nring = disk->getValue("nring");
      //      std::cout<<"disk="<<i<<" nring="<<nring<<std::endl;
      for (int j=0; j<nring; ++j) {
	const SensorInfoBase* ring = disk->get("ring",j);
	Counters c=Counters();
	float area = ring->getFloat("area");
	std::cout<<"disk="<<i<<" iring="<<j<<" area="<<area<<std::endl;
	c.setArea(ring->getValue("nmodule")*area);
	c.setTotalPixel(ring->getValue("total"));
	counters[ std::make_pair(i,j) ] = c;
	counters[ std::make_pair(i+ndisk,j) ] = c;
      }
    }
  }
}


void PixelDiskOccupancy::Analyze(UpgPerfAna::AnalysisStep step, int k) {
  // increase event counters during "EVENT" step
  if (OccupancyObservable::EventInit(step,k)) return;
  
  // enter occupancys in counters
  if (step==UpgPerfAna::INITIAL && pixClus_bec[k]!=0){  // discs only
    int module=pixClus_eta_module[k];
    int layer=pixClus_layer[k];
    LMPair layer_module = std::make_pair(layer,module);
    if (pixClus_bec[k]==-2) {
      layer_module=std::make_pair(layer+ndisk,module);
    }
    float x=pixClus_x[k];
    float y=pixClus_y[k];
    float z=pixClus_z[k];
    float s=pixClus_size[k];
    float r=sqrt(x*x+y*y);
    counters[layer_module].add(x,y,z,s,pixClus_detElementId[k]);
    // fill cluster and hit maps
    m_histos[CLUS_MAP]->Fill(z,r);
    m_histos[CLUS_MAP_DETAIL]->Fill(z,r);
    static_cast<TH2*>(m_histos[HIT_MAP])->Fill(z,r,s);
    static_cast<TH2*>(m_histos[HIT_MAP_DETAIL])->Fill(z,r,s);
  }
}

int PixelDiskOccupancy::GetNTrks() {
  //std::cout<<"pixClus_n="<<pixClus_x.size()<<std::endl;
  return pixClus_x.size();
}
