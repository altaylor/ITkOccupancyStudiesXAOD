#include "InDetUpgradePerformanceAnalysis/MinMaxCutWithIndex.h"

#include <sstream>
#include <cstdio>

std::string strf(float value) {
  std::string s;
  std::stringstream sstr;
  sstr<<value;
  sstr>>s;
  return s;
}

MinCutWithIndex::MinCutWithIndex(const std::string & varname, const std::string & indexname, float value) 
  : Cuts(strf(value)+"<"+varname+":"+indexname),
    var(varname),
    index(indexname),
    minvar(value)
{}

bool MinCutWithIndex::AcceptTrack(int i) 
{
  int j = index[i];
  if (j<0) {
     std::printf(" ERROR: j<0 in cut %s for i=%i j=%i\n", Name().c_str(), i, j);
    return false;
  } 
  return minvar <= var[j];
}

MaxCutWithIndex::MaxCutWithIndex(const std::string & varname, const std::string & indexname, float value) 
  : Cuts(varname+":"+indexname+"<"+strf(value)),
    var(varname),
    index(indexname),
    maxvar(value)
{}

bool MaxCutWithIndex::AcceptTrack(int i) 
{
  int j = index[i];
  return var[j] < maxvar;
}

RangeCutWithIndex::RangeCutWithIndex(const std::string & varname, const std::string & indexname, float vmin, float vmax) 
  : Cuts(strf(vmin)+"<"+varname+":"+indexname+"<"+strf(vmax)),
    var(varname),
    index(indexname),
    minvar(vmin),
    maxvar(vmax)
{}

bool RangeCutWithIndex::AcceptTrack(int i) 
{
  int j = index[i];
  return  ( minvar <= var[j]) && (var[j] < maxvar);
}
