#include "InDetUpgradePerformanceAnalysis/AllCuts.h"
#include "InDetUpgradePerformanceAnalysis/Observable.h"

AllCuts::AllCuts()
  : Cuts("AllCuts")
{}

bool AllCuts::AcceptEvent()
{
  for (size_t i=0; i<allcuts.size(); ++i) {
    if (!allcuts[i]->AcceptEvent()) return false;
  }
  return true;
}

bool AllCuts::AcceptTrack(int k)
{
  for (size_t i=0; i<allcuts.size(); ++i) {
    // Count returns it's argument, but also increments a counter
    if (!allcuts[i]->Count(allcuts[i]->AcceptTrack(k))) return false;
  }
  return true;
}

void AllCuts::Add(Cuts* cut)
{
  allcuts.push_back(cut);
}

AllCuts::~AllCuts()
{
  for (size_t i=0; i<allcuts.size(); ++i) {
    delete allcuts[i];
  }
  allcuts.clear();
}


std::ostream & operator<<(std::ostream & s, const Cuts* cut)
{
  int & l=Observable::output_level;
  if (cut) {
    s<<Spaces(l)<<"Cut : "<<cut->Name()<<" "<<cut->m_naccept<<"/"<<cut->m_ntotal<<std::endl;
    const AllCuts* acut = dynamic_cast<const AllCuts*>(cut);
    if (acut) {
      l++;
      for (size_t i=0; i<acut->allcuts.size(); ++i) 
	s<<acut->allcuts[i];
      l--;
    }
  }
  return s;
}
