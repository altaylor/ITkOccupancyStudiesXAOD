# echo "cleanup WorkArea WorkArea-00-00-00 in /afs/cern.ch/user/a/altaylor/UpgradeITK_V2/InnerDetector/InDetPerformance"

if ( $?CMTROOT == 0 ) then
  setenv CMTROOT /cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.20.0/CMT/v1r25p20140131
endif
source ${CMTROOT}/mgr/setup.csh
set cmtWorkAreatempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if $status != 0 then
  set cmtWorkAreatempfile=/tmp/cmt.$$
endif
${CMTROOT}/${CMTBIN}/cmt.exe cleanup -csh -pack=WorkArea -version=WorkArea-00-00-00 -path=/afs/cern.ch/user/a/altaylor/UpgradeITK_V2/InnerDetector/InDetPerformance  $* >${cmtWorkAreatempfile}
if ( $status != 0 ) then
  echo "${CMTROOT}/${CMTBIN}/cmt.exe cleanup -csh -pack=WorkArea -version=WorkArea-00-00-00 -path=/afs/cern.ch/user/a/altaylor/UpgradeITK_V2/InnerDetector/InDetPerformance  $* >${cmtWorkAreatempfile}"
  set cmtcleanupstatus=2
  /bin/rm -f ${cmtWorkAreatempfile}
  unset cmtWorkAreatempfile
  exit $cmtcleanupstatus
endif
set cmtcleanupstatus=0
source ${cmtWorkAreatempfile}
if ( $status != 0 ) then
  set cmtcleanupstatus=2
endif
/bin/rm -f ${cmtWorkAreatempfile}
unset cmtWorkAreatempfile
exit $cmtcleanupstatus

